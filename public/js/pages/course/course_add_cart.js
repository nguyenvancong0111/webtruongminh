const addCartCourseVUE = new Vue({
    el: '#block_course_add_cart',
    data: {
        filter: 0,
        index: 0,
        optionValue: JSON.parse(course_option),
        price: price
    },
    mounted: function () {
        this.filter = this.optionValue.length > 0 ? this.optionValue[0].id : 0
        this.price = this.optionValue.length > 0 ? this.optionValue[this.index].price : price
    },

    methods: {

        changeFilter: function(e, index, filter){
            this.filter = filter
            this.index = index
            this.price = this.optionValue.length > 0 ? this.optionValue[index].price : price
        },

        formatPrice: function(value) {
            let val = (value/1).toFixed(0).replace('.', ',');
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        },
        add_cart: function (e, course_id, slug = '', quan, gocart = 0, route = '', route_redirect = '') {
            var vm = this;
            if(quan > 0) {
                $.ajax({
                    type: 'POST',
                    url: route,
                    data: {
                        _token:ENV.token,
                        id:course_id,
                        filter: vm.filter,
                        slug: slug,
                        quan:quan,
                    },
                    dataType: 'json',
                }).done(function(json) {
                    if (json.error == 1) {
                        Swal.fire({
                            title: 'Thông báo',
                            text: json.msg,
                            type: 'warning',
                            confirmButtonText: 'Đồng ý',
                            confirmButtonColor: '#f37d26',
                        });
                    } else {
                        if(gocart == 0) {
                            // $('.qty-rece').html(json.data.number)
                            // $('.qty-cart-show').html(json.data.number)
                            // $('.qty-cart-show').show()
                            // $('.qty-rece').html(json.data.number);
                            $('.cart-header .cart-link .cart-count').html(json.data.number);
                            // $('#icon-cart-pop').addClass('bounce-3');
                            // $('#icon-cart-pop').addClass('cart-buyed');
                            Swal.fire({
                                title: 'Thông báo',
                                text: 'Thêm vào giỏ hàng thành công',
                                type: 'success',
                                confirmButtonText: 'Đồng ý',
                                confirmButtonColor: '#006ac5',
                            }).then((result) => {
                                // shop.reload();
                            });
                        }else {
                            window.location.href = route_redirect;
                        }

                    }
                });
            }else {
                Swal.fire({
                    title: 'Thông báo',
                    text: 'Hãy chọn số lượng',
                    type: 'Warning',
                    confirmButtonText: 'Đồng ý',
                    confirmButtonColor: '#f37d26',
                }).then((result) => {
                    // shop.reload();
                });
            }
        }
    }
})