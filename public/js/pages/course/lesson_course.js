const lessonCourseVUE = new Vue({
    el: '#block_lesson_course',
    data: {
        fetchURL: lesson_course_url,
        lessonGroup: [],
    },
    mounted: async function () {
        await this.load();
    },

    methods: {
        load: async function(){
            await $.get(this.fetchURL).then(json => {
                this.lessonGroup = this.lessonGroup.concat(json.data);
            });
        },
    }
})