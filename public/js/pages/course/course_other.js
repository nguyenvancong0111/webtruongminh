const otherCourseVUE = new Vue({
    el: '#block_course_other',
    data: {
        fetchURL: course_other_url,
        courseOther: [],
    },
    mounted: async function () {
        await this.load();
    },

    methods: {
        load: async function(){
            await $.get(this.fetchURL).then(json => {
                this.courseOther = this.courseOther.concat(json.data);
            });
        },
    }
})