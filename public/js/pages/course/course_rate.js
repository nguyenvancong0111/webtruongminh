const rateCourseVUE = new Vue({
    el: '#block_course_rate',
    data: {
        viewRate: view_rating_url,
        calRate: cal_rating_url,
        postRate: create_rating_url,
        rateViews: [],
        avg: {
            avg_rate: 0,
            avg_star: 5,
            item: {1: 0, 2: 0, 3: 0, 4: 0, 5: 0},
            total_rate: 0
        },
        pagination: {
            count: 0,
            current_page: 0,
            links: [],
            per_page: 0,
            total: 0,
            total_pages: 0,
        },
    },
    mounted: async function () {
        await this.loadCalcultor();
        await this.loadList(1);
    },

    methods: {
        loadList: async function(page){
            await $.get(this.viewRate, {
                page: page
            }).then(json => {
                this.rateViews = json.data;
                this.pagination = json.meta.pagination;
            });
        },
        loadCalcultor: async function(){
            await $.get(this.calRate).then(json => {
                this.avg = json.avg
            });
        },
        loadMore: async function(){
            await this.loadList(this.pagination.current_page + 1);
        },

        createRate: function(e){
            var vm = this;
            var form = $('#rate-form');
            var data = $(form).serializeArray();
            var fd = new FormData();
            if (data.length > 0) {
                $.each(data, function(key, value) {
                    fd.append(value.name, value.value);
                })
            }
            $('#rate-form input').removeAttr('style').addClass('reverse');
            $('#rate-form textarea').removeAttr('style').addClass('reverse');
            $('.error-message').html('');
            $.ajax({
                type: 'POST',
                url: vm.postRate,
                data: fd,
                contentType: false,
                processData: false,
                success: function(json){
                    vm.loadList();
                    Swal.fire(
                        '',
                        json.data.mess,
                        'success'
                    ).then(() => {
                        $('input[name="rating"]').prop('checked', false);
                        $('#rate-form textarea').val('')
                    });

                },
                error: function(json){
                    Object.keys(json.responseJSON.errors).forEach(item => {
                        $(`#error_${item}`).text(json.responseJSON.errors[item][0]);
                    })
                },
            }).done(function(json) {

            });
        },
    }
})