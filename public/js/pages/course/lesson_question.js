const questionAnswerLessonVUE = new Vue({
    el: '#block_lesson_question_answer',
    data: {
        QAUrl: JSON.parse(QAUrl),
        auth: JSON.parse(auth),
        questionViews: [],
        pagination: {
            count: 0,
            current_page: 0,
            links: [],
            per_page: 0,
            total: 0,
            total_pages: 0,
        },
    },
    mounted: async function () {
        await this.loadList(1);
    },

    methods: {
        loadList: async function(page){
            await $.get(this.QAUrl.view_question_url, {
                page: page
            }).then(json => {
                this.questionViews = this.questionViews.concat(json.data);
                this.pagination = json.meta.pagination;
            });
        },
        loadMore: async function(){
            await this.loadList(this.pagination.current_page + 1);
        },

        createQuestion: function(e){
            var vm = this;
            var form = $('#question-lesson-form');
            var data = $(form).serializeArray();
            var fd = new FormData();
            if (data.length > 0) {
                $.each(data, function(key, value) {
                    fd.append(value.name, value.value);
                })
            }
            fd.append('type', 2);
            $('#question-lesson-form textarea').removeAttr('style').addClass('reverse');
            $('.error-message').html('');
            $.ajax({
                type: 'POST',
                url: vm.QAUrl.create_question_url,
                data: fd,
                contentType: false,
                processData: false,
                success: function(json){
                    if (json.error === 0){
                        $.get(vm.QAUrl.view_question_url, {
                            page: 1
                        }).then(res => {
                            vm.questionViews = res.data;
                            vm.pagination = res.meta.pagination;
                        });

                        Swal.fire(
                            json.data.title,
                            json.data.mess,
                            'success'
                        ).then(() => {
                            $('#question-lesson-form textarea').val('')
                        });
                    }else{
                        Swal.fire(
                            json.code.title,
                            json.code.mess,
                            'error'
                        )
                    }
                },
                error: function(json){
                    Object.keys(json.responseJSON.errors).forEach(item => {
                        $(`#error_${item}`).text(json.responseJSON.errors[item][0]);
                    })
                },
            }).done(function(json) {

            });
        },
        createAnswer: function(e, question, index, dom){
            var vm = this;
            var answer = $(dom+'-'+question).val();
            if(typeof answer != ''){
                var fd = new FormData();
                fd.append('type', 2);
                fd.append('authType', vm.auth.type);
                fd.append('authID', vm.auth.authID);
                fd.append('id', question)
                fd.append('answer', answer)
                fd.append('_token', ENV.token)

                $.ajax({
                    type: 'POST',
                    url: vm.QAUrl.create_answer_url,
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function(json){
                        if (typeof vm.questionViews[index] != 'undefined'){
                            vm.questionViews[index].answer = vm.questionViews[index].answer.concat(json.data)
                        }
                        $(dom+'-'+question).val('')
                    },
                    error: function(json){
                        Swal.fire(
                            'Thất bại',
                            json.data.mess,
                            'error'
                        )
                    },
                }).done(function(json) {

                });
            }


        },
    }
})