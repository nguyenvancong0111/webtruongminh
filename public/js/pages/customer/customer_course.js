const courseCustomerVUE = new Vue({
    el: '#block_customer_course',
    data: {
        fetchURL: customer_course_url,
        activeCourseURL: active_customer_course_url,
        customerCourse: [],
        contentData: {
            quizzes: null,
            paginate: null
        },
        CustomerQuiz: {
            list: {},
            paginate: {},
        },
        api_get_exam_by_customer: api_get_exam_by_customer,
    },
    mounted: async function () {
        await this.load();

    },
    methods: {
        load: async function () {
            await $.get(this.fetchURL).then(json => {
                this.customerCourse = this.customerCourse.concat(json.data);
            });
        },
        activeCourse: function (id, course_id, key) {
            $('#code_active_error').empty().css('display', 'none')
            $.post(this.activeCourseURL, {
                cusCID: id,
                course_id: parseInt(course_id),
                code: $('#code_active').val()
            }).then(json => {
                if (typeof json.error != 'undefined' && json.error == 1) {
                    $('#code_active_error').html(json.code.mess).css('display', 'block')
                } else {
                    this.$set(this.customerCourse, key, json.data)
                }
            });
        },
        changePage: async function (n) {
            const quiz = this;
            if (n > 0 && n < quiz.contentData.paginate.pagination.total_pages + 1) {
                quiz.getExam(n);
            }
        },
        loadDataQuiz : function(e, object, dom, page = 1){
            var dome = dom != null ? dom : e.target;
            if ($(dome).hasClass('loaded') == false) {
                $.get(this.api_get_exam_by_customer, {
                    object: object,
                    page: page
                }).then(json => {
                    if (typeof this.CustomerQuiz.list[object] != 'undefined') {
                        this.CustomerQuiz.list[object] = this.CustomerQuiz.list[object].concat(json.data)
                    } else {
                        this.$set(this.CustomerQuiz.list, object, json.data);
                    }
                    this.$set(this.CustomerQuiz.paginate, object, json.meta.pagination);
                });
                $(dome).addClass('loaded')
            }
        },

    }
})