const couponsCustomerVUE = new Vue({
    el: '#block_customer_coupons',
    data: {
        fetchURL: customer_coupons_url,
        customerCoupons: [],
    },
    mounted: async function () {
        await this.load();
    },

    methods: {
        load: async function(){
            await $.get(this.fetchURL).then(json => {
                this.customerCoupons = this.customerCoupons.concat(json.data);
            });
        },
        copyCode: function (e) {
            var dom = e.target;
            const selection = window.getSelection();
            const range = document.createRange();
            range.selectNodeContents(dom);
            selection.removeAllRanges();
            selection.addRange(range);

            try {
                document.execCommand('copy');
                selection.removeAllRanges();

                const mailId = dom.textContent;
                dom.textContent = 'Copied!';
                dom.classList.add('success');

                setTimeout(() => {
                    dom.textContent = mailId;
                    dom.classList.remove('success');
                }, 1000);
            } catch (e) {
                dom.textContent = 'Couldn\'t copy, hit Ctrl+C!';
                dom.classList.add('error');

                setTimeout(() => {
                    errorMsg.classList.remove('show');
                }, 1200);
            }
        }
    }
})