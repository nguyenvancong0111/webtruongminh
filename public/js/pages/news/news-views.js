const viewsNewsVUE = new Vue({
    el: '#block_news_views',
    data: {
        fetchURL: news_views_url,
        newsViews: [],
    },
    mounted: async function () {
        await this.load();
    },

    methods: {
        load: async function(){
            await $.get(this.fetchURL).then(json => {
                this.newsViews = this.newsViews.concat(json.data);
            });
        },

    }
})