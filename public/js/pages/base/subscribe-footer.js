let app_api_footer_subscriber = new Vue({
    el: '#footerSaveSubscriber',
    data: {
        url_api_footer_subscriber: url_api_footer_subscriber,
        info: {
            email: ''
        },
        isSaving: false,
    },
    methods: {
        footerSaveSubscriber(evt) {
            app_api_footer_subscriber.isSaving = true;
            $.ajax({
                url: url_api_footer_subscriber,
                type: "POST",
                data: app_api_footer_subscriber.info,
                statusCode: {
                    422: function (response) {
                        const bugs = response.responseJSON.errors;
                        let html = '';
                        Object.keys(bugs).forEach(element => {
                            html += bugs[element][0] + '<BR>';
                        });
                        return Swal.fire({
                            title: noti.notice,
                            html: html,
                            showCloseButton: true,
                            icon: "warning"
                        });
                    },

                },
                success: function (jsonData) {
                    if (jsonData.success) {
                        app_api_footer_subscriber.info = {};
                        return Swal.fire({
                            title: noti.notice,
                            html: jsonData.message,
                            showCloseButton: true,
                            icon: "success"
                        });

                    }
                }
            }).always(function () {
                app_api_footer_subscriber.isSaving = false;
            });
        }
    }
});