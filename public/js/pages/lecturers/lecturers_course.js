const courseLecturersVUE = new Vue({
    el: '#block_lectures_course',
    data: {
        lecturersUrl: JSON.parse(lecturers_url),
        lecturersCourse: {
            participate: [],
            participatePaginate: '',
            created: [],
            createdPaginate: '',
        },
        lecturesRate: {
            list: {},
            avg: {},
            paginate:{}
        },
        lecturesQuestion:{
            list: {},
            paginate:{}
        },
        lecturersCustomer: {
            list: {},
            paginate: {},
        },
        lecturersCustomerQuiz: {
            list: {},
            paginate: {},
        }

    },
    mounted: async function () {
        await this.load();
        // await this.loadRateTab();
    },

    methods: {
        load: async function(){
            await $.get(this.lecturersUrl.course_participate).then(json => {
                this.lecturersCourse.participate = this.lecturersCourse.participate.concat(json.data);
                this.lecturersCourse.participatePaginate = json.meta.paginate_html;
            });
            await $.get(this.lecturersUrl.course_created).then(json => {
                this.lecturersCourse.created = this.lecturersCourse.created.concat(json.data);
                this.lecturersCourse.createdPaginate = json.meta.paginate_html;
            });
        },
        paginateParticipateClick: function (url) {
            $.get(url).then(json => {
                this.lecturersCourse.participate = json.data;
                this.lecturersCourse.participatePaginate = json.meta.paginate_html;
            });
        },
        paginateClick: function (url) {
            $.get(url).then(json => {
                this.lecturersCourse.created = json.data;
                this.lecturersCourse.createdPaginate = json.meta.paginate_html;
            });
        },
        loadRateTab:  function(object){
            var vm = this;
            vm.loadRateAvg(object);
            vm.loadRateList(object);
        },
        loadRateAvg: function(object){
            $.get(this.lecturersUrl.course_rateAvg, {
                object: object
            }).then(json => {
                this.$set(this.lecturesRate.avg, object, json.avg);
            });
        },
        loadRateList: function(object, page = 1){
            $.get(this.lecturersUrl.course_rate, {
                object: object,
                page: page
            }).then(json => {
                if (typeof this.lecturesRate.list[object] != 'undefined'){
                    this.lecturesRate.list[object] = this.lecturesRate.list[object].concat(json.data)
                }else{
                    this.$set(this.lecturesRate.list, object, json.data);
                }
                this.$set(this.lecturesRate.paginate, object, json.meta.pagination);
            });
        },
        loadMoreRate: async function(object){
            await this.loadRateList( object, this.lecturesRate.paginate[object].current_page + 1);
        },
        loadQuestionList: function(object, page = 1){
            $.get(this.lecturersUrl.course_question, {
                object: object,
                page: page,
                type: 1,
            }).then(json => {
                if (typeof this.lecturesQuestion.list[object] != 'undefined'){
                    this.lecturesQuestion.list[object] = this.lecturesQuestion.list[object].concat(json.data)
                }else{
                    this.$set(this.lecturesQuestion.list, object, json.data);
                }
                this.$set(this.lecturesQuestion.paginate, object, json.meta.pagination);
            });
        },
        loadMoreQuestion: function(object){
            this.loadQuestionList( object, this.lecturesQuestion.paginate[object].current_page + 1);
        },

        Reply: function(e, object, id, index, dom){
            var mess = $(dom+'_'+id).val();
            $.post(this.lecturersUrl.course_answer, {
                object: object,
                id: id,
                answer: mess,
                user_type: 1,
                type: 1,
            }).then(json => {
                if (typeof this.lecturesQuestion.list[object][index] != 'undefined'){
                    this.lecturesQuestion.list[object][index].answer = this.lecturesQuestion.list[object][index].answer.concat(json.data)
                }else{
                    this.$set(this.lecturesQuestion.list[object][index].answer, object, json.data);
                }
                $(dom+'_'+id).val('')
            });
        },

        loadCustomerList: function(object, page = 1){
            $.get(this.lecturersUrl.course_customer, {
                object: object,
                page: page
            }).then(json => {
                if (typeof this.lecturersCustomer.list[object] != 'undefined'){
                    this.lecturersCustomer.list[object] = this.lecturersCustomer.list[object].concat(json.data)
                }else{
                    this.$set(this.lecturersCustomer.list, object, json.data);
                }
                this.$set(this.lecturersCustomer.paginate, object, json.meta.pagination);
            });
        },

        loadCustomerQuiz: function(object, customer, page = 1){
            $.get(this.lecturersUrl.course_customer_quiz, {
                customer_id: customer,
                object: object,
                page: page
            }).then(json => {
                if (typeof this.lecturersCustomerQuiz.list[object] != 'undefined'){
                    this.lecturersCustomerQuiz.list[object] = this.lecturersCustomerQuiz.list[object].concat(json.data)
                }else{
                    this.$set(this.lecturersCustomerQuiz.list, object, json.data);
                }
                this.$set(this.lecturersCustomerQuiz.paginate, object, json.meta.pagination);
            });
        },
        loadMoreCustomer: function(object){
            this.loadCustomerList( object, this.lecturersCustomer.paginate[object].current_page + 1);
        },

        loadData: function (e, object, type, dom = null) {
            var vm = this;
            var dome = dom != null ? dom : e.target;
            switch (parseInt(type)) {
                default:
                    return "Nothing";
                case 1:
                    if ($(dome).hasClass('loaded') == false){
                        vm.loadRateTab(object)
                        $(dome).addClass('loaded')
                    }
                    break;
                case 2:
                    if ($(dome).hasClass('loaded') == false){
                        vm.loadQuestionList(object)
                        $(dome).addClass('loaded')
                    }
                    break;

                case 3:
                    if ($(dome).hasClass('loaded') == false){
                        vm.loadCustomerList(object)
                        $(dome).addClass('loaded')
                    }
                    break
            }
        },
        loadDataCourse: function(e, object, customer, dom){
            var vm = this;
            var dome = dom != null ? dom : e.target;
            if ($(dome).hasClass('loaded') == false){
                vm.loadCustomerQuiz(object, customer)
                $(dome).addClass('loaded')
            }
        }

    }
})
function paginateClick(url, dom){
    var domVue = courseLecturersVUE;
    if (typeof dom != 'undefined' && dom === 1){
        domVue.paginateParticipateClick(url)
    }else if (typeof dom != 'undefined' && dom === 2) {
        domVue.paginateClick(url)
    }
}