var app_cart = new Vue({
    // options content
    el: '#app_cart',
    data: {
        data_cart: JSON.parse(data_content),
        url_remove: remove_item_url,
        url_update: update_item_url,
        url_payCart: pay_cart,
        url_applyCoupons: apply_coupon,
        cart_items:[],
        total_items:0,
        total_cart:0,
        extra_fee:0,
        grand_total:0,
        member_discount:0,
        coupon_value: '',
        point_submit:0,
        point_to_money:0,
        data_dathang: JSON.parse(data_dathang),
        disabled_btn:false,
        ok_dk:false,
    },
    mounted(){
        this.load();
        // this.loader();

    },
    beforeMount: function() {

    },
    computed: {

    },
    updated: function(){

    },
    // watch:{
    //     'loading':function(){console.log(1)}
    // },
    methods:{
        noPay: function(){
            Swal.fire(
                'Thông báo',
                'Bạn không thể thanh toán đơn hàng 0 VNĐ',
                'warning'
            )
        },
        loader: function() {
            // setTimeout(function() {
            //     if($('#pb_loader').length > 0) {
            //         $('#pb_loader').removeClass('show');
            //     }
            // }, 700);
            $("#loadMe").modal({
                backdrop: "static", //remove ability to close modal with click
                keyboard: false, //remove option to close with keyboard
                show: true //Display loader!
            });
        },
        show_loader: function() {
            // if($('#pb_loader').length > 0) {
            //     $('#pb_loader').addClass('show');
            // }

            $("#loadMe").modal({
                backdrop: "static", //remove ability to close modal with click
                keyboard: false, //remove option to close with keyboard
                show: true //Display loader!
            });
        },
        hide_loader: function() {
            // setTimeout(function() {
                // if($('#pb_loader').length > 0) {
                //     $('#pb_loader').removeClass('show');
                // }
            // }, 700);
            setTimeout(function() {
                $("#loadMe").modal("hide");
            }, 500);
        },
        goTo:function(ele) {
            $([document.documentElement, document.body]).animate({
                scrollTop: $(ele).offset().top-150
            }, 500);
        },
        returnPros: function(filter_id) {
            return list_filter[filter_id];
        },
        payCart: function(e) {
            var vm = this;
            e.preventDefault();
            var form = $('#complete-form');
            var data = $(form).serializeArray();
            var fd = new FormData();
            if (data.length > 0){
                $.each(data, function (key, value) {
                    fd.append(value.name, value.value);
                })
            }
            if (vm.data_dathang.grand_total_coupon != 0){
                fd.append('coupon', vm.data_dathang.coupon_code)
            }
            $('#complete-form input').removeAttr('style').addClass('reverse');
            $('#complete-form textarea').removeAttr('style').addClass('reverse');
            $.ajax({
                type: 'POST',
                url: vm.url_payCart,
                data: fd,
                contentType: false,
                processData: false,
                statusCode: {
                    422: function(response) {
                        var json = JSON.parse(response.responseText);
                        if (json) {
                            $('#complete-form span.error-message').empty();
                            $.each(json.errors, function(key, val) {
                                $('#complete-form #' + key).removeClass('reverse').css('border-bottom', '1px solid red' );
                                $('#complete-form #' + key + '_error').html(val[0]).css('display', 'block')
                            });
                        }
                    },
                    200: function(response) {
                        if (response.error === 1) {
                            Swal.fire(
                                response.code.title,
                                response.code.mess,
                                'error'
                            )
                        } else {
                            shop.redirect(response.data.url)
                        }

                    }
                },
                beforeSend: function() {

                }
            })
        },
        load:function() {
            var vm = this;
            if(typeof vm.data_cart != 'undefined'){
                vm.cart_items = vm.data_cart.details
                vm.updatePriceToShow(vm.data_cart);
            }
        },
        updatePriceToShow: function(data){
            this.total_cart = data.total;
            this.total_items = data.number;

            // $('# total_cart_top').html(data.number);
            $('.cart-header .cart-link .cart-count').html(data.number);

        },
        applyCoupon: function(e){
            var vm = this;
            vm.data_dathang.coupon_code = $('#coupon_value').val();
            $.ajax({
                type: 'POST',
                url: vm.url_applyCoupons,
                data: {
                    _token:ENV.token,
                    coupons: vm.data_dathang.coupon_code
                },
                dataType: 'json',
            }).done(function(json) {
                if (json.error == 1) {
                    Swal.fire({
                        title: 'Thông báo',
                        text: json.msg,
                        type: 'warning',
                        confirmButtonText: 'Đồng ý',
                        confirmButtonColor: '#006ac5',
                    });
                } else {
                    vm.data_dathang.coupon_code = json.data.coupon;
                    vm.data_dathang.discount_coupon = json.data.dccoupon;
                    vm.data_dathang.grand_total_coupon = json.data.grand_total;
                    window.history.replaceState('', '', vm.updateURLParameter(window.location.href, 'coupons', json.data.coupon))
                }
            }).always(function(){
                vm.hide_loader();
            });
        },
        updateURLParameter: function(url, param, paramVal) {
            var TheAnchor = null;
            var newAdditionalURL = "";
            var tempArray = url.split("?");
            var baseURL = tempArray[0];
            var additionalURL = tempArray[1];
            var temp = "";

            if (additionalURL) {
                var tmpAnchor = additionalURL.split("#");
                var TheParams = tmpAnchor[0];
                TheAnchor = tmpAnchor[1];
                if (TheAnchor)
                    additionalURL = TheParams;

                tempArray = additionalURL.split("&");

                for (var i = 0; i < tempArray.length; i++) {
                    if (tempArray[i].split('=')[0] != param) {
                        newAdditionalURL += temp + tempArray[i];
                        temp = "&";
                    }
                }
            } else {
                var tmpAnchor = baseURL.split("#");
                var TheParams = tmpAnchor[0];
                TheAnchor = tmpAnchor[1];

                if (TheParams)
                    baseURL = TheParams;
            }

            if (TheAnchor)
                paramVal += "#" + TheAnchor;

            var rows_txt = temp + "" + param + "=" + paramVal;
            return baseURL + "?" + newAdditionalURL + rows_txt;
        },

        remove: function(e,index,item){
            var vm = this;
            e.preventDefault();
            Swal.fire({
                title: 'Thông báo',
                text: 'Bạn muốn loại bỏ khóa học ra khỏi giỏ hàng',
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#006ac5',
                cancelButtonColor: '#f36f21',
                confirmButtonText: 'Đồng ý',
                cancelButtonText: 'Hủy',
            }).then((result) => {
                vm.show_loader();
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        url: vm.url_remove,
                        data: {
                            _token:ENV.token,
                            index:index,
                            id: item.id,
                        },
                        dataType: 'json',
                    }).done(function(json) {
                        if (json.error == 1) {
                            Swal.fire({
                                title: 'Thông báo',
                                text: json.msg,
                                type: 'warning',
                                confirmButtonText: 'Đồng ý',
                                confirmButtonColor: '#006ac5',
                            });
                        } else {
                            vm.data_cart.details = json.data.details;
                            vm.updatePriceToShow(json.data);
                        }
                    }).always(function(){
                        vm.hide_loader();
                    });
                }else {
                    vm.hide_loader();
                }
            });
        },
        selectItem: function(e, index, item, dom){
            var vm = this;
            e.preventDefault();
            let val = 0;
            if ($(dom+index).is(":checked")){
                val = 1;
            }

            $.ajax({
                type: 'POST',
                url: vm.url_update,
                data: {
                    _token:ENV.token,
                    index:index,
                    id: item.id,
                    slug: item.slug,
                    val: val,
                    order_item_id: item.item_id,
                },
                dataType: 'json',
            }).done(function(json) {
                if (json.error == 1) {
                    Swal.fire({
                        title: 'Thông báo',
                        text: json.msg,
                        type: 'warning',
                        confirmButtonText: 'Đồng ý',
                        confirmButtonColor: '#006ac5',
                    });
                } else {
                    $('.cart-header .cart-link .cart-count').html(json.data.number);
                    vm.cart_items = json.data.details;
                    vm.updatePriceToShow(json.data);
                }
            }).always(function(){
                vm.hide_loader();
            });
        },

        formatPrice: function(value) {
            let val = (value/1).toFixed(0).replace('.', ',');
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        },
        formatNumberIndex: function (value) {
            if (parseInt(value) == 0 ){
                value = 0;
            }else if(parseInt(value) < 10){
                value = '0' + value;
            }
            return value;
        }
    }
});