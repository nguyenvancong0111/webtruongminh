const newsHomeVUE = new Vue({
    el: '#block_news',
    data: {
        fetchURL: news_url,
        news: [],
    },
    mounted: async function () {
        await this.load();
        this.buildSwiper();
    },

    methods: {
        load: async function(){
            await $.get(this.fetchURL).then(json => {
                this.news = this.news.concat(json.data);
            });
        },
        buildSwiper: function(){
            setTimeout(() => {
                if ($('#newshome-slide').length > 0) {
                    var swiperAdmissions = new Swiper('#newshome-slide', {
                        loop: 'true',
                        speed: 1200,
                        spaceBetween: 20,
                        slidesPerView: 3,
                        autoplay: {
                            delay: 2000,
                            disableOnInteraction: false,
                        },
                        pagination: {
                            el: '#newshome-slide-pagination',
                            clickable: true,
                        },
                        navigation: {
                            nextEl: '#newshome-slide-prev',
                            prevEl: '#newshome-slide-next',
                        },
                        breakpoints: {
                            310: {
                                slidesPerView: 1.2,
                                spaceBetween: 20,
                            },

                            570: {
                                slidesPerView: 2,
                                spaceBetween: 20,
                            },
                            992: {
                                slidesPerView: 2.2,
                                spaceBetween: 20,
                            },
                            1400: {
                                slidesPerView: 3,
                                spaceBetween: 20,
                            },
                        },
                    });
                }
            }, 100)
        }
    }
})