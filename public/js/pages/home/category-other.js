const categoryOtherHomeVUE = new Vue({
    el: '#block_category_other',
    data: {
        fetchURL: category_other_url,
        categoryOther: [],
    },
    mounted: async function () {
        await this.load();
    },

    methods: {
        load: async function(){
            await $.get(this.fetchURL).then(json => {
                this.categoryOther = this.categoryOther.concat(json.data);
            });
        },
    }
})
