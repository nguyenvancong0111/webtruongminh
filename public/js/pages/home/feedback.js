const feedbackHomeVUE = new Vue({
    el: '#block_feedback',
    data: {
        fetchURL: feedback_url,
        feedback: [],
    },
    mounted: async function () {
        await this.load();
    },

    methods: {
        load: async function(){
            await $.get(this.fetchURL).then(json => {
                this.feedback = this.feedback.concat(json.data);
            });
        },
    }
})