function alertChangeOrderState(url, messageContent='', messageTitle='Xác nhận!') {
  Swal.fire({
    title: $.i18n(messageTitle),
    text:  $.i18n(messageContent),
    showCloseButton: true,
    icon: "question"
  }).then( confirm => handleChangeStateOrderResult(confirm, url))
}
function reSendMail(url, order_item, course) {
    $.post(url, {
        order_item: order_item,
        course_id: course
    }).then(res => {
        if (res.error === 0){
            Swal.fire({
                title: 'Thông báo',
                text: res.data,
                showCloseButton: true,
                icon: "success"
            })
            /*.then(r => {
                const iframe = document.getElementById("iframeLoadData");
                iframe.contentWindow.location.reload();
            })*/
        }
    });
}
function activeNow(url, customer, course, code) {
    $.post(url, {
        customer_id: customer,
        course_id: course,
        code: code
    }).then(res => {
        if (res.error === 0){
            Swal.fire({
                title: 'Thông báo',
                text: res.data,
                showCloseButton: true,
                icon: "success"
            }).then(r => {
                location.reload();
            })
        }else{
            Swal.fire({
                title: 'Thất bại',
                text: res.data,
                showCloseButton: true,
                icon: "error"
            })
        }
    });
}
function handleChangeStateOrderResult(confirm, url) {
  if (confirm.isConfirmed) {
    $.post(url).then(res => {
      Swal.fire({
        title: $.i18n('Thông báo'),
        text:  $.i18n(res.message),
        showCloseButton: true,
        icon: "question"
      }).then(r => {
        location.reload();
      })
    });
  }
}
function handleChangeLawyer(url, messageContent='', messageTitle='Xác nhận!'){
    Swal.fire({
        title: $.i18n(messageTitle),
        text:  $.i18n(messageContent),
        showCloseButton: true,
        icon: "question"
    }).then( confirm => {
        $.post(url).then(res => {
            Swal.fire({
                title: $.i18n('Thông báo'),
                text:  $.i18n(res.msg),
                showCloseButton: true,
                icon: "question"
            }).then(r => {
                location.reload();
            })
        });
    });
}

$(document).ready(function () {
  let $userHandle = $('#user-handle')
  $userHandle.select2({
    // minimumInputLength: 3,
    ajax: {
      url: "/users/filter",
      dataType: 'json',
      type: "GET",
      data: function (query) {
        let term = query.term;
        if(typeof term === "undefined")
          term = '';
        return {
          search: `name:${term};email:${term};phone:${term}`
        }
      },
      processResults: function (data) {
        return {
            results: $.map(data.data, function (item) {
                return {
                    text: `${item.email} (${item.name})`,
                    id: item.id
                }
            })
        };
      }
    }
  });

  let checkHasUserId = hasQueryStr('user_id');
  if (checkHasUserId) {
    $userHandle.append(`<option value="${checkHasUserId}" selected="selected">${$('#user-hanleName').val()}</option>`);
    $userHandle.trigger('change');
  }
});

