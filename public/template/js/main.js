function actions__dataTabPopup() {
    $(
        ".navtab .navtab-item,.js--loyalty-login,.js--action-login,.js--action-forgot,.js--action-forgot-loyal,.js-action-submit"
    ).click(function () {
        let dataTab = $(this).data("tabpopup");

        // $('.navtab .navtab-item').removeClass('active');
        // $(this).addClass('active');

        $(".popup").fadeOut();
        $(".popup-content").removeClass("active");

        $("#" + dataTab).fadeIn();
        $("#" + dataTab)
            .find(".popup-content")
            .addClass("active");
    });
}

function backTop() {
    window.scroll({ top: 0, left: 0, behavior: "smooth" });
}

function scrollTopVisibility() {
    $(window).scroll(function () {
        var windowScrollTop = $(window).scrollTop();

        if (windowScrollTop > 500) $(".totop").show();
        else $(".totop").hide();
    });
}

// menu header
function mobileMenu() {
    const windowWidth = $(window).width();

    $(".menu-hamburger").click(function () {
        $(this).siblings(".menu-overlay").addClass("active");
        $(this).siblings(".menu-items").addClass("active");
    });

    if (windowWidth < 992) {
        $(".menu-parent i").click(function () {
            $(this)
                .toggleClass("active")
                .parent()
                .siblings(".menu-sub")
                .slideToggle();
        });
    }

    $(document).mouseup(function (e) {
        if (
            !$(".menu-items").is(e.target) &&
            $(".menu-items").has(e.target).length === 0
        ) {
            $(".menu-items").siblings(".menu-overlay").removeClass("active");
            $(".menu-items").removeClass("active");
        }
    });
}

function showinfoxhd() {
    if ($("#info_customer_electronic_bill").is(":checked")) {
        $("#info-xhd").show();
    } else {
        $("#info-xhd").hide();
    }
}

function searchHeader() {
    $(".js-trigger-search").click(function () {
        $(".search-form .container").toggleClass("active");
        // $('.search-form .box__input-search input').val('Nhập từ khóa tìm kiếm...');
    });
    $(".js-close-form").click(function () {
        $(".search-form .container").removeClass("active");
        // $('.search-form .box__input-search input').val('Nhập từ khóa tìm kiếm...');
    });

    $(document).mouseup(function (e) {
        if (
            !$(".search-form form").is(e.target) &&
            $(".search-form form").has(e.target).length === 0
        ) {
            $(".search-form .container").removeClass("active");
        }
    });
    // $('.search-form .box__input-search input').focusin(function () {
    //   $(this).val(' ');
    // });
}

function clickTabActive(classAction, dataAttr, classRemove) {
    $(classAction).click(function () {
        let dataTab = $(this).data(dataAttr);

        $(classAction).removeClass("active");
        $(this).addClass("active");

        $(classRemove).fadeOut(1);

        $("#" + dataTab).fadeIn();
    });
}

function headerScrollSticky() {
    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        var headerHeight = $(".header").innerHeight();

        if (scrollTop) {
            $(".header").addClass("scroll");
            $("main").css("margin-top", headerHeight);
        } else {
            $(".header").removeClass("scroll");
            $("main").css("margin-top", 0);
        }
    });
}

function showhidePasswordInput() {
    $(".js-password-action").click(function () {
        var inputCheck = $(this).parents(".js-toggle-password").find("input");

        if (inputCheck.attr("type") == "password") {
            inputCheck.attr("type", "text");

            $(this).parent().find(".js-password-action").removeClass("active");
            $(this).parent().find(".show").addClass("active");
        } else {
            inputCheck.attr("type", "password");

            $(this).parent().find(".js-password-action").removeClass("active");
            $(this).parent().find(".hide").addClass("active");
        }
    });
}

function openPopup() {
    $(".js--btn-popup").click(function () {
        let dataBtn = $(this).data("popup");

        $("#" + dataBtn).fadeIn();
        $("#" + dataBtn)
            .find(".popup-content")
            .addClass("active");
    });
}

function toggleTable(holder) {
    $(holder).on("click", function () {
        var parentsRow = $(this).parent();

        if (parentsRow.hasClass("opened") == true) {
            // is open
            parentsRow.removeClass("opened");
        } else {
            $(holder).removeClass("opened");
            parentsRow.addClass("opened");
        }
    });
}

function closePopup() {
    $(document).mouseup(function (e) {
        if (
            !$(".style--radius-border").is(e.target) &&
            $(".style--radius-border").has(e.target).length === 0
        ) {
            $(".popup").fadeOut();
            $(".popup-content").removeClass("active");
        }
    });

    $(".js-close-popup").click(function () {
        $(".popup").fadeOut();
        $(".popup-content").removeClass("active");
    });
}

function clickUploadAva(holder) {
    $("#upload:hidden").trigger("click");
}

function btnToggle__popupUpdate(holder) {
    $(holder).toggleClass("active");

    if ($(holder).hasClass("active")) $(holder).text("Thu gọn");
    else $(holder).text("Xem chi tiết lịch sử tiến độ");

    $(".block--info-details .details-ticket").slideToggle();
}
$(document).ready(function () {
    mobileMenu();
    headerScrollSticky();
    actions__dataTabPopup();
    searchHeader();
    showhidePasswordInput();
    openPopup();
    closePopup();
    scrollTopVisibility();
});
