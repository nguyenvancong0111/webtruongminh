var quiz = new Vue({
    el: '#list-quiz-course-customer',
    data: {
        urlFetchData: fetchURL,
        contentData: {
            quizzes: null,
            paginate: null
        },
        course_id: course_id,
        api_get_exam_by_course: api_get_exam_by_course,
        callback: {

        }
    },
    mounted: function() {
        // const top = this;
        // $.get(this.urlFetchData.news_top).then(function (json) {
        //     if (json.data.length > 0) {
        //         top.contentData.newsTopBig = json.data[0];
        //         top.contentData.newsTopSmall = json.data.slice(1);

        //     } else {
        //         top.contentData.newsTopBig = null;
        //         top.contentData.newsTopSmall = null;
        //     }
        // })
        console.log(11111);
        this.getExam(1);

    },
    methods: {
        getExam: function(n) {
            const quiz = this;
            $.ajax({
                url: quiz.api_get_exam_by_course,
                data: {
                    course_id: quiz.course_id,
                    page: n
                },
                method: 'get',
                success: function(data) {
                    quiz.contentData.quizzes = data.data;
                    quiz.contentData.paginate = data.meta;
                }
            })
        },
        changePage: async function(n) {
            const quiz = this;
            if (n > 0 && n < quiz.contentData.paginate.pagination.total_pages + 1) {
                quiz.getExam(n);
            }
        },
    }
});