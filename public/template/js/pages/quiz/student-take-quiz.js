var question = new Vue({
    el: '#list-question-for-exam',
    data: {
        url: JSON.parse(urlQuiz),
        fil: JSON.parse(fil),
        exam: JSON.parse(data_exam),

        questions: [],
        number_question: -1,
        start_time: null,
        end_time: null,
        time: Number(timeout),
        time_out: Number(timeout),
        time_use: 0,
        timeView: 0,
        timerEnabled: false,
        isActive: false,
        is_red: false
    },
    watch: {
        timerEnabled(value) {
            if (value) {
                setTimeout(() => {
                    this.convertSecondsTime(this.time--)
                    this.time_use ++
                }, this.time_out * 10);
            }
        },
        time: {
            handler(value) {
                if (value > 0 && this.timerEnabled) {
                    if(value < 10){
                        this.isActive = true
                    }
                    setTimeout(() => {
                        this.convertSecondsTime(this.time--);
                        this.time_use ++
                    }, this.time_out * 10);
                }else if(value === 0){
                    this.isActive = false;
                    this.is_red = true;
                    this.convertSecondsTime(0);
                }
            },
            immediate: true
        }
    },
    mounted: function() {
        this.convertSecondsTime(this.time)
        this.getQuestion();
    },

    methods: {
        getQuestion: function(n) {
            const vm = this;
            $.ajax({
                url: vm.url.question,
                data: {
                    exam_id: vm.fil.exam
                },
                method: 'get',
                success: function(json) {
                    if (json.data.length > 0) {
                        vm.questions = json.data;
                    }
                }
            })
        },

        startQuiz: function(e){
            this.number_question = 0;
            this.timerEnabled = true;
            this.getTimeNow(true);
        },
        next_quiz: function(){
            this.number_question = this.number_question++
        },
        prev_quiz: function(){
            this.number_question = this.number_question--
        },

        submitExam: function() {
            this.getTimeNow(false);
            this.timerEnabled = false;
            $("#loading").modal({
                backdrop: "static", //remove ability to close modal with click
                keyboard: false, //remove option to close with keyboard
                show: true //Display loader!
            });
            const vm = this;
            // clearTimeout(question.endtime);
            var answer_check = {};

            $.each(vm.questions, function (key, val) {
                var ans = [];
                if (typeof val.answer != 'undefined' && val.answer.length > 0){
                    $.each(val.answer, function (k, v) {
                        if (typeof v.checkbox != 'undefined' && v.checkbox == true){
                            ans.push(v.id);
                        }
                        if (typeof v.radio != 'undefined' && v.radio == 1){
                            ans.push(v.id);
                        }
                    })
                    answer_check[val.id] = ans.length > 0 ? ans : [0];
                }
            })

            $.ajax({
                url: vm.url.submit,
                type: 'POST',
                data: {
                    lesson: vm.exam.lesson_id,
                    next_lesson: vm.exam.next_lesson,
                    data: answer_check,
                    exam: vm.fil.exam,
                    rework: vm.exam.rework,
                    times: vm.exam.times,
                    customer: vm.fil.customer,
                    course: vm.fil.course,
                    start_time: vm.start_time,
                    end_time: vm.end_time,
                    time_use: vm.time_use,
                },
                statusCode: {
                    200: function(response) {
                        if (response.error === 1) {
                            Swal.fire(
                                response.code.title,
                                response.code.mess,
                                'error'
                            )
                        } else {
                            shop.redirect(response.data.url)
                        }

                    }
                },
                beforeSend: function(){

                },
                complete: function(response){
                    $("#loading").modal("hide");
                    if (response.error === 0){
                        shop.reload();
                    }else{
                        Swal.fire(
                            response.code.title,
                            response.code.mess,
                            'error'
                        )
                    }
                },
                success: (json) => {

                }
            })
        },

        convertSecondsTime(seconds){
            this.timeView = new Date(seconds * 1000).toISOString().substr(11, 8)
        },
        getTimeNow: function(start) {
            const today = new Date();
            const date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
            const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            const date_time = date +' '+ time;
            if (start){
                this.start_time = date_time;
            }else{
                this.end_time = date_time;
            }
        },
        playVideo: function () {
            $('.player__content').addClass('d-none');
            $('.player__embed').removeClass('d-none').addClass('d-block');
        },
        radioChange: function (e, q, i) {
            console.log(q, i)
            var vm = this;
            $.each(vm.questions, function (k){
                console.log(vm.questions)
                if (k == i){
                    vm.$set(vm.questions[q].answer[k], 'radio', 1)
                }else{
                    vm.$delete(vm.questions[q].answer[k], 'radio')
                }
            })
        }

    }
});