// import ProductTag from "./ProductTag/ProductTag.vue";
import SearchBar from './SearchBar/SearchBar.vue';
import CollectionTable from './CollectionTable.vue';


export default {
    name: 'CollectionList',
    components: { SearchBar, CollectionTable },
    data() {
        return {};
    },
    computed: {},
    mounted() {},

    methods: {

    },
}; // End class