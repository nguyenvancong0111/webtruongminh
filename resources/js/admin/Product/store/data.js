export default {
    namespaced: true,
    state: {
        content: {
            image: '',
            image_url: '',
            status: 2,
            data_status: 2,
            color: '',
            new_old: 1,
            sort_order: 0,
            code: '',
            is_home: 0,
            hot: 0,
        },
        arrID_variants_add: {},
    },
    mutations: {
        contentData(state, payloadData) {
            state.content = payloadData;
        },
        deleteArrIDVariantAdd(state, value) {
            delete state.arrID_variants_add[value];
            state.arrID_variants_add = Object.assign({}, state.arrID_variants_add, state.arrID_variants_add);
        },
        addVariantID(state, value) {
            // state.arrID_variants_add.push(value.product_variant_id);
            const keyArr = value.product_variant_id;
            const productOptionInsert = value.productOptionInsert;
            state.arrID_variants_add[keyArr] = productOptionInsert;

            // console.log(productOptions);
        },
        updateStatus(state, value) {
            state.content.status = value;
        },
        updateData_status(state, value) {
            state.content.data_status = value;
        },
        updateColor(state, value) {
            state.content.color = value;
        },
        updateNew_old(state, value) {
            state.content.new_old = value;
        },
        updateIs_home(state, value) {
            state.content.is_home = value;
        },
        updateHot(state, value) {
            state.content.hot = value;
        },
        setSort_order(state, value) {
            state.content.sort_order = value;
        },
        setCode(state, value) {
            state.content.code = value;
        },
        setImg(state, value) {
            state.content.image = value;
        },
        setImgUrl(state, value) {
            alert(1);
            console.log(value);
            state.content.image_url = value;
        }
    },
    actions: {
        contentData({commit}, content) {
            commit('contentData', content);
        },
        updateVideoLink({commit}, content) {
            commit('updateVideoLink', content);
        },
        updateManufacturer({commit}, content) {
            commit('updateManufacturer', content);
        },
        updateStatus({commit}, content) {
            commit('updateStatus', content);
        },
        updateData_status({commit}, content) {
            commit('updateData_status', content);
        },
        updateColor({commit}, content) {
            commit('updateColor', content);
        },
        updateNew_old({commit}, content) {
            commit('updateNew_old', content);
        },
        updateIs_home({commit}, content) {
            commit('updateIs_home', content);
        },
        updateHot({commit}, content) {
            commit('updateHot', content);
        },
        setSort_order({ commit }, content) {
            commit('setSort_order', content);
        },
        setCode({ commit }, content) {
            commit('setCode', content);
        },
        setImg({commit}, content) {
            commit('setImg', content);
        },
        setImgUrl({commit}, content) {
            commit('setImgUrl', content);
        },
        setDataPrdOrigin({commit}, content) {
            commit('setDataPrdOrigin', content);
        },
        setPrimaryCategoryId({commit}, content) {
            commit('setPrimaryCategoryId', content);
        },
        setFreeship({commit}, content) {
            commit('setFreeship', content);
        },
        addVariantID({commit}, content) {
            commit('addVariantID', content);
        },
        deleteArrIDVariantAdd({commit}, content) {
            commit('deleteArrIDVariantAdd', content);
        },
    },
    getters: {},
};
