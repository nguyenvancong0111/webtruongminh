<?php

namespace App\Ship\Middlewares\Http;

use Apiato\Core\Foundation\FunctionLib;
use Illuminate\Foundation\Http\Middleware\TrimStrings as Middleware;
use Closure;

class TrimStrings extends Middleware
{

    /**
     * The names of the attributes that should not be trimmed.
     *
     * @var array
     */
    protected $except = [
        'password',
        'password_confirmation',
    ];


    protected $allow_tags = [
        '<iframe>',
        '<a>', '<h1>', '<h2>', '<h3>', '<h4>', '<h5>', '<h6>',
        '<p>', '<div>', '<span>', '<ul>', '<li>', '<ol>','<table>', '<td>', '<th>', '<tr>', '<tbody>',
        '<strong>', '<b>', '<i>', '<u>', '<em>', '<strike>', '<code>', '<mark>', '<hr>', '<br>', '<img>'
    ];

    public function handle($request, Closure $next)
    {
        $env = FunctionLib::appEnv();
        $input = $request->all();
        if ($env == 'admin') {
            array_walk_recursive($input, function (&$input, $key) {
                $input = trim($input);
                $allowScript = $this->allowScriptForInputName();
                if (!in_array($key, $allowScript)) {
                    $input = $this->strip_tags_attributes($input, $this->allow_tags);
                }
            });
        } else {

            array_walk_recursive($input, function (&$input) {
                $input = trim($input);
                $input = strip_tags($input);
                $input = str_replace("&#032;", " ", $input);
                $input = str_replace(chr(0xCA), "", $input);  //Remove sneaky spaces
                $input = str_replace(array("<!--", "-->", "/<script/i", ">", "<", '"', "/\\\$/", "/\r/", "!", "'", "=", "+"), array("", "", "&#60;script", "&gt;", "&lt;", "&quot;", "&#036;", "", "&#33;", "&#39;", "", " "), $input);
                $input = preg_replace("/\\\(?!&amp;#|\?#)/", "&#092;", $input);
            });
        }
        $request->merge($input);

        return parent::handle($request, $next);
    }

    function strip_tags_attributes($sSource, $aAllowedTags = array(), $aDisabledAttributes = array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavaible', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragdrop', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterupdate', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmoveout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload'))
    {
        if (empty($aDisabledAttributes)) return strip_tags($sSource, implode('', $aAllowedTags));
        $str = strip_tags($sSource, implode('', $aAllowedTags));

        FunctionLib::set('aDisabledAttributes', $aDisabledAttributes);
        return preg_replace_callback(
            '/<(.*?)>/i',
            function ($matches) {
                $aDisabledAttributes = FunctionLib::get('aDisabledAttributes', []);
                if (!empty($aDisabledAttributes)) {
                    foreach ($matches as $match) {
                        return preg_replace(
                            [
                                //'/javascript:[^\"\']*/i',
                                '/(' . implode('|', $aDisabledAttributes) . ')[ \\t\\n]*=[ \\t\\n]*[\"\'][^\"\']*[\"\']/i',
                                '/\s+/'
                            ],
                            [/*'',*/
                                '', ' '
                            ],
                            stripslashes($match)
                        );
                    }
                }
            },
            $str
        );
    }

    private function allowScriptForInputName()
    {
        return ['ga', 'gtag_noscript', 'gtag','script_head','script_body'];
    }
}
