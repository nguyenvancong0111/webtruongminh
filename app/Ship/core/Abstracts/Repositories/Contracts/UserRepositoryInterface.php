<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 23:30:10
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-10 23:35:27
 * @ Description: Happy Coding!
 */

namespace Apiato\Core\Abstracts\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

interface UserRepositoryInterface extends RepositoryInterface
{
}
