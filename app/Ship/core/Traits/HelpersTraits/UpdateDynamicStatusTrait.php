<?php

namespace App\Ship\core\Traits\HelpersTraits;

trait UpdateDynamicStatusTrait
{
    public function updateDynamicStatus(string $field,int $id, int $status,string $id_alias_field = '') :? bool
    {
        if (!empty($field) && $id > 0) {
            return $this->repository->getModel()->where($id_alias_field ? $id_alias_field : 'id', $id)->update([$field => $status]);
        }

        return false;
    }
}
