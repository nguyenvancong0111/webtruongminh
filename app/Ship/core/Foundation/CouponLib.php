<?php
namespace App\Ship\core\Foundation;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Discount\Actions\Getters\GetCodeByCodeAction;
use App\Containers\Discount\Models\DiscountCode;

class CouponLib
{
    public static function calcCoupon($coupon,$total,$cart_content) {
        $checking['category'] = [];//app(ProductService::class)->getAllCateFromPrdIds($cart_content['itm_ids']);
        $checking['course'] = $cart_content['itm_ids'];
        $grand_total = 0;
        $dccoupon = 0;
        $coupon = self::checking_coupon_by_cus($coupon, $checking);
        if (!empty($coupon)){
            if(!empty($coupon['passing'])) {
                $dccoupon = ($coupon['type']=='percent' ? ceil((($total*$coupon['discount']/100)/1000)*1000) : $coupon['discount']);
                $grand_total = $dccoupon <= $total ? $total - $dccoupon : 0;
            }elseif(!empty($coupon['passing'])) {
                $total = 0;
                $flip_arr = FunctionLib::replace_key_by_val($cart_content['details'],'id');
                foreach($coupon['passing'] as $item) {
                    $total += $flip_arr[$item]['price']*$flip_arr[$item]['quan'];//($coupon['type']=='percent' ? ceil((($flip_arr[$item]['price']*$coupon['discount']/100)/1000)*1000) : $coupon['discount']);
                }

                $dccoupon = ($coupon['type']=='percent' ? ceil((($total*$coupon['discount']/100)/1000)*1000) : $coupon['discount']);
                $grand_total = $dccoupon <= $total ? $total - $dccoupon : 0;
            }
            return ['grand_total' => $grand_total,'dccoupon' => $dccoupon, 'coupon' => $coupon['coupon_code']];
        }
        return false;
    }

    /// $for_admin : Bỏ qua check hạn sử dụng,  bỏ qua check đã sử dụng
    /// Trong nghiep vụ admin sẽ tự xử lý
    public static function checking_coupon_by_cus($coupon = '', $checking = [], $customer_id = 0, $for_admin = false) {
        // check coupon hệ thống trước
        $coupon_sys = self::check_coupon_sys($coupon, $checking);
        if($coupon_sys != false) {
            return $coupon_sys;
        }
        return false;
    }

    // Đây là hàm chcek coupon do hệ thông sinh ra
    // trong menu MGG hệ thống
    private static function check_coupon_sys($coupon = '',$checking = []) {
        $coupon = app(GetCodeByCodeAction::class)->skipCache(true)->run($coupon);
        if(!empty($coupon)){
            $data_return = [];

            // check coupon ap dung cho danh muc hoac san pham
            $passing = self::checkObjCoupon($coupon, $checking);

            $end = strtotime($coupon->end_date);
            $start = strtotime($coupon->start_date);

            // Check xem coupon còn hạn hay ko, đã sử dụng hay chưa
            if( $start < time() &&  $end > time() && $passing && $coupon->quantity > 0) {
                $data_return = $data_return + [
                        'type' => $coupon->discount_type,// Mặc định coupon hệ thống là dạng Phần trăm
                        'discount' => $coupon->discount_value,
                        'coupon_code' => $coupon->code,
                        'passing' => is_array($passing) ? $passing : [],
                    ];
                return $data_return;
            }
        }
        return false;
    }

    private static function checkObjCoupon(DiscountCode $coupon, $checking){
        if($coupon->product_use != '*') {
            if(empty($checking) || !isset($checking[$coupon->product_use]) || empty($checking[$coupon->product_use])){
                return false;
            }
            $compare = array_column($coupon->course->toArray(), 'id');

            $prd_discount = [];
            foreach($checking[$coupon->product_use] as $item) {
                if(in_array($item,$compare)) {
                    $prd_discount[] = $item;
                }
            }

            if(!empty($prd_discount)) {
                return $prd_discount;
            }

            return false;

        }
        return true;
    }




    public static function tickVoucherUsed($coupon = '',$customer_id = 0){
        $coupon = Coupon::couponCanUse($coupon);
        if(!empty($coupon)){
            $coupon->update(['quantity' => DB::raw('quantity - 1'), 'used_times' => DB::raw('used_times + 1')]);
            return $coupon;
        }
        return false;
    }


}
