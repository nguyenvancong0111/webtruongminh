<?php
namespace Apiato\Core\Foundation;

use Apiato\Core\Abstracts\Exceptions\Exception;
use App\Core\ImageTemplate\CourseCoverLarge;
use App\Core\ImageTemplate\CourseCoverSmall;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File as FacadesFile;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

class MyStorage
{
    private static $limits = [
        'minute' => 1,
        '5minutes' => 5,
        '10minutes' => 10,
        'hour' => 60,
        '5hours' => 300,
        '12hours' => 720,
        'day' => 1440,
        '2days' => 2880,
        'week' => 10080,
        'month' => 43200,
        'year'  => 525600,
    ];
    public static function getDefaultDisk(){
        return self::getDisk(config('flysystem.default'));
    }

    public static function getDisk($disk){
        return Flysystem::connection($disk);
    }

    public static function get_video_stream_link($disk, $path, $options){
        $version = array_get($options, 'version', 'origin');
        if(app()->environment() == 'production'){
            $storage_stream_config = config('flysystem.connections.' . $disk);
            if(!array_get($storage_stream_config, 'video_streaming_support', false)){
                return [];
            }else{
                try{
                    $closure = array_get($storage_stream_config, 'get_video_stream_closure');
                    $stream = call_user_func($closure,  $path, null, null );
                    if($version == 'hd'){
                        $stream['rel'] = '720';
                        $stream['label'] = 'HD';
                        $stream['title'] = 'HD';
                    }elseif($version == 'sd'){
                        $stream['rel'] = '360';
                        $stream['label'] = 'SD';
                        $stream['title'] = 'SD';
                    }else{
                        $stream['rel'] = '1000';
                        $stream['label'] = 'Origin';
                        $stream['title'] = 'Origin';
                    }

                    return $stream;
                }catch (\Exception $ex){
                    return [];
                }

            }
        }elseif(in_array($disk, ['public', 'tmp', 'local'])){
            $stream = [
                'src' => route('api.video.stream',['id' => $options['id'], 'version' => $version]),
                'type' => 'video/mp4',
                'label' => strtoupper($version),
                'title' => strtoupper($version),
            ];
            return $stream;
        }
        return "";
    }

    public static function get_document_download_link($disk, $path, $options){
        if(in_array($disk, ['public', 'tmp', 'local']) && isset($options['secure']) && $options['secure'] == true && isset($options['document_id'])) {
            $id = $options['document_id'];
            return route('web.course.fileView', ['id' => $id, 'token' => self::makeRequestToken($id, '10minutes')]);
        }
        return "";
    }

    public static function removeFromDisk($disk, $path){
        $disk = self::getDisk($disk);
        if($disk->has($path) && $disk->get($path)->isFile()){
            return $disk->delete($path);
        }
        return true;
    }

    public static function defaultValidUploadFile(UploadedFile $file, $type){
        $return = ['valid' => false, 'message' => 'Error'];
        if(is_string($type) && in_array($type,['video', 'document', 'audio','image'])){
            $allowed_size = [config('flysystem.max_size.' . $type)];
            $allowed_ext = config('flysystem.exts.' . $type);
        }elseif(is_array($type)){
            if(empty($type['allowed_size'])){
                $allowed_size = [config('flysystem.max_upload_size')];
            }elseif(is_array($type['allowed_size'])){
                $allowed_size = $type['allowed_size'];
            }else{
                $allowed_size = [intval($type['allowed_size'])];
            }

            if(empty($type['allowed_ext'])){
                $allowed_ext = [config('flysystem.upload_exts_default')];
            }elseif(is_array($type['allowed_ext'])){
                $allowed_ext = $type['allowed_ext'];
            }else{
                $allowed_ext = explode(',',$type['allowed_ext']);
            }
        }else{
            return $return;
        }

        if(count($allowed_size) == 1){
            $min_size = 0;
            $max_size = $allowed_size[0];
        }else{
            $min_size = $allowed_size[0];
            $max_size = $allowed_size[1];
        }
        $ext = $file->getClientOriginalExtension();
        $size = $file->getSize();
        if(!in_array(strtolower($ext), $allowed_ext)){
            $return['message'] = trans('validation.invalid_extension', ['exts' => implode(',', $allowed_ext)]);
            return $return;
        }
        if($size < $min_size || $size > $max_size){
            $return['message'] = trans_choice('validation.invalid_size',
                $min_size,
                ['min_size' => round($min_size/1024,3), 'max_size' => round($max_size/1024,3)]);
            return $return;
        }
        return ['valid' => true, 'message' => 'ok'];
    }

    public static function saveUploadedFile(UploadedFile $file, $disk_name, $file_path){
        $disk = self::getDisk($disk_name);
        if(!Storage::exists($file_path)){
            try {
                $fil = $disk->writeStream($file_path, fopen($file->getRealPath(), 'rb'));
                return ['status' => $fil, 'message' => 'Upload thành công', 'dir' => $file_path];
            }catch (Exception $e){
                return ['status' => false, 'message' => $e->getMessage()];
            }
        }
        return ['status' => false, 'message' => 'File already exists'];

    }

    public static function responseFromDisk($disk_name, $path, $custom_name = "", $ext_auto = true){
        $response = new StreamedResponse();
        $disk = self::getDisk($disk_name);
        try{
            $response->setCallBack(function() use($disk, $path)
            {
                echo $disk->get($path)->read();
            });
            $path_info = pathinfo($path);
            $file_name = $custom_name == "" ? $path_info['filename'] : $custom_name;
            if($ext_auto){
                $file_name = $file_name . "." . $path_info['extension'];
            }
            $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $file_name);
            $response->headers->set('Content-Disposition',$disposition);
            return $response;
        }catch (\Exception $ex){
            abort(404);
        }

    }

    public static function local_stream($path, $object, $user){
        return [
            'type' => 'application/x-mpegURL',
            'src' => url('/') . $path . '/master.m3u8',
        ];
    }
    public static function video_stream($path){
        return [
            'type' => 'application/x-mpegURL',
            'rel' => '720',
            'label' => 'HD',
            'title' => 'HD',
//            'src' => url('/') . $path . '/master.m3u8',
            'src' => storage_path('app').$path
        ];
    }

    public static function makeRequestToken($request = '', $time = '5minutes'){
        if(isset(self::$limits[$time])){
            $time = self::$limits[$time];
        }else{
            $time = intval($time);
        }
        $token = md5(\FunctionLib::guid(env('APP_KEY', "edus365")));
        Cache::add($token, $request, $time);
        return $token;
    }
}