<?php

namespace App\Containers\Product\UI\WEB\Requests;

use App\Containers\Product\Data\Transporters\DefaultTransporter;
use App\Ship\Parents\Requests\Request;

/**
 * Class AjaxStoreProductVariantRequest.
 */
class AjaxStoreProductVariantRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = DefaultTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'product_id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            // 'product_id' => 'required|numeric',
            'price' => 'nullable',
            'sku' => 'required|unique:product_variants,sku,NULL,product_variant_id,deleted_at,NULL',
            'stock' => 'nullable|numeric|min:0',
            'status' => 'required|numeric|min:1|max:2',

            'choice' => 'required|array',
            'choice.optionIds' => 'required|array',
            'choice.optionIds.*' => 'required|numeric',

            'choice.optionValueIds' => 'required|array',
            'choice.optionValueIds.*' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'sku.unique' => 'Sku đã được sử dụng trùng'
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
