<?php

namespace App\Containers\Product\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Containers\Product\UI\WEB\Requests\FilterProductRequest;
use App\Containers\Product\UI\WEB\Requests\AjaxStoreProductVariantRequest;
use App\Containers\Product\UI\WEB\Requests\AjaxStoreUpdateProductVariantRequest;
use App\Containers\Product\UI\WEB\Requests\AjaxDeleteProductVariantRequest;
use App\Containers\Product\UI\WEB\Requests\AjaxFindVariantByProductIdRequest;
use App\Containers\Product\UI\WEB\Requests\AjaxFindVariantsByProductIdRequest;
use App\Containers\Product\Enums\ProductSync;

/**
 * Class ProductDescController
 *
 * @package App\Containers\Product\UI\WEB\Controllers
 */
class AjaxProductVariantController extends WebController
{
    use ApiResTrait;

    public function ajaxFindVariantByProductId(AjaxFindVariantByProductIdRequest $request)
    {
        $productsVariant = Apiato::call('Product@ProductVariant\FindVariantByProductIdAction', [
            $request->productId,
            [
                'product_option_values',
                'product_option_values.option.desc',
                'product_option_values.option_value.desc'
            ]
        ]);
        return $this->sendResponse($productsVariant);
    }

    public function ajaxVariantsByIDs(AjaxFindVariantsByProductIdRequest $request)
    {
        $productsVariant = Apiato::call('Product@ProductVariant\GetVariantByIdsAction', [
            $request->ids, $request->productId
        ]);
        return $this->sendResponse($productsVariant);
    }

    public function ajaxStoreProductVariant(AjaxStoreProductVariantRequest $request)
    {
        $transporter = $request->toTransporter();
        if (isset($transporter->product_id)) {
            if (ProductSync::SYNC && !empty($transporter->product_id)) {
                $product = Apiato::call('Product@Admin\GetProductByIdAction', [$transporter->product_id]);
                $arrayPrd = $product->toArray();
                if (array_key_exists(ProductSync::KEY_ISSET_SYNC, $arrayPrd) && empty($arrayPrd[ProductSync::KEY_ISSET_SYNC])) {
                    $productsVariant = Apiato::call('Product@ProductVariant\StoreProductVariantAction', [
                        $transporter,
                        [
                            'product_option_values',
                            'product_option_values.option.desc',
                            'product_option_values.option_value.desc'
                        ]
                    ]);
                    return $this->sendResponse($productsVariant, __('Tạo biến thể thành công'));
                } else {
                    return $this->sendResponse('Error', __('Không thể tạo biến thể do đã đồng bộ ' . ProductSync::NAME_SYNC));
                }
            } else {
                $productsVariant = Apiato::call('Product@ProductVariant\StoreProductVariantAction', [
                    $transporter,
                    [
                        'product_option_values',
                        'product_option_values.option.desc',
                        'product_option_values.option_value.desc'
                    ]
                ]);
                return $this->sendResponse($productsVariant, __('Tạo biến thể thành công'));
            }
        } else {
            return $this->sendResponse('Error', __('Tạo biến thể không thành công'));
        }
    }

    public function ajaxStoreUpdateProductVariant(AjaxStoreUpdateProductVariantRequest $request)
    {
        $transporter = $request->toTransporter();
        if (isset($transporter->product_id)) {
            if (ProductSync::SYNC && !empty($transporter->product_id)) {
                $product = Apiato::call('Product@Admin\GetProductByIdAction', [$transporter->product_id]);
                $arrayPrd = $product->toArray();
                if (array_key_exists(ProductSync::KEY_ISSET_SYNC, $arrayPrd) && empty($arrayPrd[ProductSync::KEY_ISSET_SYNC])) {
                    $productsVariant = Apiato::call('Product@ProductVariant\StoreProductVariantAction', [
                        $transporter,
                        [
                            'product_option_values',
                            'product_option_values.option.desc',
                            'product_option_values.option_value.desc'
                        ]
                    ]);
                    return $this->sendResponse($productsVariant, __('Sửa biến thể thành công'));
                } else {
                    return $this->sendResponse('Error', __('Không thể sửa biến thể do đã đồng bộ ' . ProductSync::NAME_SYNC));
                }
            } else {
                $productsVariant = Apiato::call('Product@ProductVariant\StoreProductVariantAction', [
                    $transporter,
                    [
                        'product_option_values',
                        'product_option_values.option.desc',
                        'product_option_values.option_value.desc'
                    ]
                ]);
                return $this->sendResponse($productsVariant, __('Sửa biến thể thành công'));
            }
        }
    }

    public function ajaxDeleteProductVariant(AjaxDeleteProductVariantRequest $request)
    {
        $transporter = $request->toTransporter();
        $productsVariant = Apiato::call('Product@ProductVariant\DeleteProductVariantAction', [$transporter]);
        return $this->sendResponse($productsVariant, __('Xóa biến thể thành công'));
    }
} // End class
