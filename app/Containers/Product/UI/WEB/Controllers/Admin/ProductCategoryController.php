<?php

namespace App\Containers\Product\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Containers\Product\UI\WEB\Requests\AjaxFindProductByCateId;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;

/**
 * Class ProductDescController
 *
 * @package App\Containers\Product\UI\WEB\Controllers
 */
class ProductCategoryController extends WebController
{
  use ApiResTrait;

  public function ajaxGetPrdByIdCate(AjaxFindProductByCateId $request)
  {
    $transporter = $request->toTransporter();
    $limit = 20;
    $offset = '';
    $products = Apiato::call('Product@Admin\FindProductByIdCateAction', [
      $transporter->cate_id,$limit,$offset,$transporter->name
    ]);
    $cate = Apiato::call('Category@GetCategoryByIdAction', [
      $transporter->cate_id
    ]);
    return $this->sendResponse(['products' => $products,'cate' => $cate]);
  }

  public function ajaxGetPrdByIdCateForOffset(AjaxFindProductByCateId $request)
  {
    $transporter = $request->toTransporter();
    $limit = 20;
    $products = Apiato::call('Product@Admin\FindProductByIdCateAction', [
      $transporter->cate_id,$limit,$transporter->offset,$transporter->name
    ]);
    return $this->sendResponse($products);
  }
} // End class
