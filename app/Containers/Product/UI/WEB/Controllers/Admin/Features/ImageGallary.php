<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 23:15:50
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-06 22:58:20
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\WEB\Controllers\Admin\Features;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\StringLib;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Models\ProductImage;
use App\Containers\Product\UI\API\Requests\UploadImgPrdGalleryRequest;
use App\Containers\Product\UI\WEB\Requests\Admin\ChangeImgPosGalleryRequest;
use App\Containers\Product\UI\WEB\Requests\Admin\RemoveImgGalleryRequest;
use App\Containers\Product\UI\WEB\Requests\AjaxGetProductRequest;

trait ImageGallary
{

    public function uploadImgGallery(UploadImgPrdGalleryRequest $request)
    {
        if ($request->hasFile('Filedata')) {
            $image = $request->file('Filedata');
            if ($image->isValid()) {
                $title = basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension());
                $fname = Apiato::call('File@UploadImageAction', [$request, 'Filedata', $title, StringLib::getClassNameFromString(Product::class)]);
                if (!empty($fname) && !$fname['error']) {
                    $imgGallery = new ProductImage();
                    $imgGallery->object_id = $request->object_id;
                    $imgGallery->image = $fname['fileName'];
                    //                    $imgGallery->size = $image->getClientSize();
                    //                    $imgGallery->type = $image->getClientMimeType();
                    $imgGallery->type = $request->type;
                    //                    $imgGallery->changed = time();
                    // $imgGallery->user_id = \Auth::id();
                    //                    $imgGallery->uname = \Auth::user()->user_name;
                    //                    $imgGallery->lang = $request->lang;
                    $imgGallery->sort = ProductImage::getSortInsert($request->lang);
                    $imgGallery->save();

                    if (empty($imgGallery->object_id)) {
                        return FunctionLib::ajaxRespond(true, 'Hoàn thành', ['id' => $imgGallery->id]);
                    } else {
                        return FunctionLib::ajaxRespond(true, 'Hoàn thành', ['images' => Apiato::call('Product@Admin\GetProductGalleryAction', [$request->object_id])]);
                    }
                }
                return FunctionLib::ajaxRespond(false, 'Upload ảnh thất bại!');
            }
            return FunctionLib::ajaxRespond(false, 'File không hợp lệ!');
        }
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy ảnh!');
    }

    public function ajaxItemImgUpdate(UploadImgPrdGalleryRequest $request)
    {
        $data = $request->editImage;
        if ($data['id'] > 0) {
            $cur = ProductImage::find($data['id']);
            if ($cur) {
                $cur->sort = (int) $data['sort'];
                $cur->save();
                return FunctionLib::ajaxRespond(true, 'Hoàn thành', $cur);
            }
        }
        return FunctionLib::ajaxRespond(false, 'Dữ liệu không chính xác');
    }

    public function uploadImgsGallery(UploadImgPrdGalleryRequest $request)
    {
        if ($request->hasFile('Filedata')) {
            $image = $request->file('Filedata');
            if ($image->isValid()) {
                $title = basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension());
                $fname = Apiato::call('File@UploadImageAction', [$request, 'Filedata', $title, StringLib::getClassNameFromString(Product::class)]);
                if (!empty($fname) && !$fname['error']) {
                    $imgGallery = new ProductImage();
                    $imgGallery->object_id = $request->object_id;
                    $imgGallery->image = $fname['fileName'];
                    $imgGallery->created = time();
                    $imgGallery->type = 'product';
                    //                    $imgGallery->changed = time();
                    // $imgGallery->user_id = \Auth::id();
                    //                    $imgGallery->uname = \Auth::user()->user_name;
                    //                    $imgGallery->lang = $request->lang;
                    // $imgGallery->sort = ProductImage::getSortInsert($request->lang);
                    $imgGallery->save();
                    if (empty($imgGallery->object_id)) {
                        return FunctionLib::ajaxRespond(true, 'Hoàn thành', ['fileName' =>  $imgGallery->image]);
                    } elseif ($request->object_id == 'NaN') {
                        return FunctionLib::ajaxRespond(true, 'Hoàn thành', ['fileName' =>  $imgGallery]);
                    } else {
                        return FunctionLib::ajaxRespond(true, 'Hoàn thành', ['images' => Apiato::call('Product@Admin\GetProductGalleryAction', [$request->object_id])]);
                    }
                }
                return FunctionLib::ajaxRespond(false, 'Upload ảnh thất bại!');
            }
            return FunctionLib::ajaxRespond(false, 'File không hợp lệ!');
        }
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy ảnh!');
    }

    public function ajaxImgData(AjaxGetProductRequest $request)
    {
        $transporter = $request->toTransporter();
        if ($transporter->product_id > 0) {
            $productImgs = Apiato::call('Product@Admin\GetProductGalleryAction', [$transporter->product_id]);
            if (!empty($productImgs)) {
                return FunctionLib::ajaxRespond(true, 'Hoàn thành', $productImgs);
            }
            return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
        }
    }

    public function ajaxItemChangePos(ChangeImgPosGalleryRequest $request)
    {
        if ($request->id > 0 && $request->next > 0 && $request->type != '') {
            $next = ProductImage::find($request->next);
            $cur = ProductImage::find($request->id);
            if ($next && $cur) {
                $cur->sort = $request->type == 'left' ? ($next->sort + 1) : ($next->sort - 1);
                $cur->save();
                return FunctionLib::ajaxRespond(true, 'Hoàn thành');
            }
        }
        return FunctionLib::ajaxRespond(false, 'Dữ liệu không chính xác');
    }

    public function ajaxItemImgDel(RemoveImgGalleryRequest $request)
    {
        if ($request->product_id > 0) {
            $data = ProductImage::where('id', $request->img_id)->where('object_id', $request->product_id)->where('type', 'product')->first();
            if ($data) {
                $data->delete();
                return FunctionLib::ajaxRespond(true, 'Hoàn thành', ['images' => Product::getImageGallery($request->product_id, 'product')]);
            }
        }
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }
}
