<?php

namespace App\Containers\Product\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\StringLib;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Models\ProductImage;
use App\Containers\Product\UI\WEB\Requests\Admin\CreateProductRequest;
use App\Containers\Product\UI\WEB\Requests\Admin\FindProductByIdRequest;
use App\Containers\Product\UI\WEB\Requests\Admin\GetAllProductsRequest;
use App\Containers\Product\UI\WEB\Requests\Admin\UpdateProductRequest;
use App\Containers\Product\UI\API\Requests\UploadImgPrdGalleryRequest;
use App\Containers\Product\UI\WEB\Requests\Admin\ChangeImgPosGalleryRequest;
use App\Containers\Product\UI\WEB\Requests\Admin\RemoveImgGalleryRequest;
use App\Containers\Product\UI\WEB\Requests\AjaxGetProductRequest;
use App\Containers\Product\UI\WEB\Controllers\Admin\Features\ImageGallary;
use App\Containers\Product\UI\WEB\Controllers\Admin\Features\UpdateSomeStatus;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Facades\View;

class Controller extends AdminController
{
    use ApiResTrait;
    use ImageGallary, UpdateSomeStatus;

    public function __construct()
    {
        $this->title = 'Sản phẩm';
        parent::__construct();
    }

    public function index(GetAllProductsRequest $request)
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Sản phẩm', $this->form == 'list' ? '' : route('admin_product_home_page'));
        View::share('breadcrumb', $this->breadcrumb);

        $products = Apiato::call('Product@Admin\GetAllProductsAction', [$request->all(), ['with_relationship' => ['categories', 'categories.desc','collections', 'collections.desc']], $limit = 20]);
        $products->load([
            'product_variants.product_option_values',
            'product_variants.product_option_values.option.desc',
            'product_variants.product_option_values.option_value.desc',
        ]);

        $categories = Apiato::call('Category@Admin\GetAllCategoriesAction', [['cate_type' => CategoryType::PRODUCT]]);

        $collections = Apiato::call('Collection@Admin\GetAllCollectionAction', [false,false,['desc']]);

        return view('product::admin.index', [
            'search_data' => $request,
            'data' => $products,
            'categories' => $categories,
            'collections' => $collections,
        ]);
    }

    public function edit(FindProductByIdRequest $request)
    {
        $this->showEditForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Sản phẩm', $this->form == 'list' ? '' : route('admin_product_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Thông tin sản phẩm');
        View::share('breadcrumb', $this->breadcrumb);

        return view('product::admin.edit', [
            'input' => $request->all()
        ]);
    }

    public function add(CreateProductRequest $request)
    {
        $this->showEditForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Sản phẩm', $this->form == 'list' ? '' : route('admin_product_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Thông tin sản phẩm');
        View::share('breadcrumb', $this->breadcrumb);
        return view('product::admin.edit', [
            'input' => [
                'id' => 'undefined',
            ]
        ]);
    }

    public function update(UpdateProductRequest $request)
    {
        try {
            $tranporter = $this->uploadImage(new DataTransporter($request), $request, 'image', 'products', StringLib::getClassNameFromString(Product::class));

            $product = Apiato::call('Product@Admin\SaveProductAction', [$tranporter]);

            if ($product) {
                return redirect()->route('admin_product_edit_page', ['id' => $product->id])->with('status', 'Sản phẩm đã được cập nhật');
            }
        } catch (Exception $e) {
            throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function delete(FindProductByIdRequest $request)
    {
        try {
            Apiato::call('Product@Admin\DeleteProductAction', [$request]);
            return redirect()->route('admin_product_home_page')->with('status', 'Sản phẩm đã được xóa');

        } catch (Exception $e) {
            // throw $e;
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
        }
    }

    public function uploadImgGallery(UploadImgPrdGalleryRequest $request)
    {
        if ($request->hasFile('Filedata')) {
            $image = $request->file('Filedata');
            if ($image->isValid()) {
                $title = basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension());
                $fname = Apiato::call('File@UploadImageAction', [$request, 'Filedata', $title, StringLib::getClassNameFromString(Product::class)]);
                if (!empty($fname) && !$fname['error']) {
                    $imgGallery = new ProductImage();
                    $imgGallery->object_id = $request->object_id;
                    $imgGallery->image = $fname['fileName'];
                    //                    $imgGallery->size = $image->getClientSize();
                    //                    $imgGallery->type = $image->getClientMimeType();
                    $imgGallery->type = $request->type;
                    //                    $imgGallery->changed = time();
                    // $imgGallery->user_id = \Auth::id();
                    //                    $imgGallery->uname = \Auth::user()->user_name;
                    //                    $imgGallery->lang = $request->lang;
                    $imgGallery->sort = ProductImage::getSortInsert($request->lang);
                    $imgGallery->save();

                    if (empty($imgGallery->object_id)) {
                        return FunctionLib::ajaxRespond(true, 'Hoàn thành', ['id' => $imgGallery->id]);
                    } else {
                        return FunctionLib::ajaxRespond(true, 'Hoàn thành', ['images' => Apiato::call('Product@Admin\GetProductGalleryAction', [$request->object_id])]);
                    }
                }
                return FunctionLib::ajaxRespond(false, 'Upload ảnh thất bại!');
            }
            return FunctionLib::ajaxRespond(false, 'File không hợp lệ!');
        }
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy ảnh!');
    }

    public function ajaxGetIdsPrd(GetAllProductsRequest $request)
    {
        $limit = !isset($request->limit) || empty($request->limit) ? 100 : $request->limit;
        $products = Apiato::call('Product@Admin\GetAllProductsAction', [$request->all(), ['with_relationship' => ['categories', 'categories.desc']], $limit]);
        $products->load([
            'product_variants.product_option_values',
            'product_variants.product_option_values.option.desc',
            'product_variants.product_option_values.option_value.desc',
        ]);
        if ($products) {
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $products);
        }
        return FunctionLib::ajaxRespond(false, 'Dữ liệu không chính xác');
    }

    public function ajaxItemImgUpdate(UploadImgPrdGalleryRequest $request)
    {
        $data = $request->editImage;
        if ($data['id'] > 0) {
            $cur = ProductImage::find($data['id']);
            if ($cur) {
                $cur->sort = (int)$data['sort'];
                $cur->link = $data['link'];
                $cur->save();
                return FunctionLib::ajaxRespond(true, 'Hoàn thành', $cur);
            }
        }
        return FunctionLib::ajaxRespond(false, 'Dữ liệu không chính xác');
    }

    public function uploadImgsGallery(UploadImgPrdGalleryRequest $request)
    {
        if ($request->hasFile('Filedata')) {
            $image = $request->file('Filedata');
            if ($image->isValid()) {
                $title = basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension());
                $fname = Apiato::call('File@UploadImageAction', [$request, 'Filedata', $title, StringLib::getClassNameFromString(Product::class)]);
                if (!empty($fname) && !$fname['error']) {
                    $imgGallery = new ProductImage();
                    $imgGallery->object_id = $request->object_id;
                    $imgGallery->image = $fname['fileName'];
                    $imgGallery->created = time();
                    $imgGallery->type = 'product';
                    //                    $imgGallery->changed = time();
                    // $imgGallery->user_id = \Auth::id();
                    //                    $imgGallery->uname = \Auth::user()->user_name;
                    //                    $imgGallery->lang = $request->lang;
                    // $imgGallery->sort = ProductImage::getSortInsert($request->lang);
                    $imgGallery->save();
                    if (empty($imgGallery->object_id)) {
                        return FunctionLib::ajaxRespond(true, 'Hoàn thành', ['fileName' => $imgGallery->image]);
                    } elseif ($request->object_id == 'NaN') {
                        return FunctionLib::ajaxRespond(true, 'Hoàn thành', ['fileName' => $imgGallery]);
                    } else {
                        return FunctionLib::ajaxRespond(true, 'Hoàn thành', ['images' => Apiato::call('Product@Admin\GetProductGalleryAction', [$request->object_id])]);
                    }
                }
                return FunctionLib::ajaxRespond(false, 'Upload ảnh thất bại!');
            }
            return FunctionLib::ajaxRespond(false, 'File không hợp lệ!');
        }
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy ảnh!');
    }

    public function ajaxImgData(AjaxGetProductRequest $request)
    {
        $transporter = $request->toTransporter();
        if ($transporter->product_id > 0) {
            $productImgs = Apiato::call('Product@Admin\GetProductGalleryAction', [$transporter->product_id]);
            if (!empty($productImgs)) {
                return FunctionLib::ajaxRespond(true, 'Hoàn thành', $productImgs);
            }
            return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
        }
    }

    public function ajaxItemChangePos(ChangeImgPosGalleryRequest $request)
    {
        if ($request->id > 0 && $request->next > 0 && $request->type != '') {
            $next = ProductImage::find($request->next);
            $cur = ProductImage::find($request->id);
            if ($next && $cur) {
                $cur->sort = $request->type == 'left' ? ($next->sort + 1) : ($next->sort - 1);
                $cur->save();
                return FunctionLib::ajaxRespond(true, 'Hoàn thành');
            }
        }
        return FunctionLib::ajaxRespond(false, 'Dữ liệu không chính xác');
    }

    public function ajaxItemImgDel(RemoveImgGalleryRequest $request)
    {
        if ($request->product_id > 0) {
            $data = ProductImage::where('id', $request->img_id)->where('object_id', $request->product_id)->where('type', 'product')->first();
            if ($data) {
                $data->delete();
                return FunctionLib::ajaxRespond(true, 'Hoàn thành', ['images' => Product::getImageGallery($request->product_id, 'product')]);
            }
        } else {
            $data = ProductImage::where('id', $request->img_id)->where('type', 'product')->first();
            $data->delete();
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $request->img_id);
        }
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function ajaxSaveProductDetail(AjaxGetProductRequest $request)
    {
        $transporter = $request->toTransporter();
        $transporter = $transporter->toArray();
         try {
               $prd = Apiato::call('Product@Admin\SaveProductAction', [$transporter]);
               return FunctionLib::ajaxRespond(true, 'Hoàn thành', $prd);
         } catch (\Throwable $e) {
               return FunctionLib::ajaxRespond(false, 'Không hợp lệ!');
         }
    }

    public function ajaxProductDataByID(AjaxGetProductRequest $request)
    {
        $transporter = $request->toTransporter();
        if ($transporter->product_id > 0) {
            $product = Apiato::call('Product@Admin\GetProductByIdAction', [$transporter->product_id, ['all_desc', 'manufacturer']])->toArray();

            // $product['stockStatus'] = Apiato::call('General@StockStatus\StockStatusListingAction', [[['status', '>', -1]], 0])->toArray();
            unset($product['categories'], $product['all_desc']);
            if (!empty($product)) {
                return FunctionLib::ajaxRespond(true, 'Hoàn thành', $product);
            }
            return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
        }

    }

    public function ajaxStockStatus()
    {
        $stock = Apiato::call('General@StockStatus\StockStatusListingAction', [[['status', '>', -1]], 0])->toArray();
        if (!empty($stock)) {
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $stock);
        }
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function ajaxProductByID(AjaxGetProductRequest $request)
    {
        $transporter = $request->toTransporter();
        if ($transporter->product_id > 0) {
            $product = Apiato::call('Product@ProductDesc\FindProducDescByProductIdAction', [
                [
                    ['product_id', '=', $transporter->product_id],
                    // ['language_id', '=', $transporter->lang_id]
                ]
            ]);
            $productDescItem = $product->toArray();
            foreach ($productDescItem as $key => $value) {
                $prd[$value['language_id']] = $value;
            }
            if ($prd) {
                return FunctionLib::ajaxRespond(true, 'Hoàn thành', $prd);
            }
        }
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function ajaxGetLang()
    {
        $langs = Apiato::call('Localization@GetAllLanguageDBAction', [[]]);
        if ($langs) {
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $langs);
        }
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function editProductFrame(int $id)
    {
        return view('product::admin.product-edit-vue-frame', [
            'product_id' => $id
        ]);
    }

} // End class
