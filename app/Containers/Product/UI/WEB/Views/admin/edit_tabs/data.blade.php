<div class="tab-pane" id="data">
    <div class="tabbable">

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="input-sku">SKU</label>
            <div class="col-sm-6">
                <input type="text" name="sku" value="{{ old('sku', @$data['sku']) }}" placeholder="SKU" id="input-sku"
                    class="form-control">
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="input-global_price">Giá thị trường<span
                    class="text-danger">*</span></label>
            <div class="col-sm-6">
                <input type="text" name="global_price"
                    value="{{ old('global_price', \FunctionLib::numberFormat(@$data['global_price'])) }}"
                    placeholder="Giá thị trường sản phẩm" id="input-global_price" class="form-control"
                    onkeypress="return shop.numberOnly()" onkeyup="shop.mixMoney(this)" onfocus="this.select()">
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="input-price">Giá bán<span
                    class="text-danger">*</span>
                <span class="d-block small text-danger">(Vui lòng nhập 0 nếu muốn để giá LIÊN HỆ)</span>
            </label>
            <div class="col-sm-6">
                <input type="text" name="price"
                    value="{{ old('price', \FunctionLib::numberFormat(@$data['price'])) }}" placeholder="Giá sản phẩm"
                    id="input-price" class="form-control" onkeypress="return shop.numberOnly()"
                    onkeyup="shop.mixMoney(this)" onfocus="this.select()">
            </div>
        </div>

        {{-- <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="input-price">Số lượng <span class="d-block small text-danger">(Nếu sp có sl đi theo thuộc tính thì bỏ trống)</span></label>
            <div class="col-sm-6">
                <input type="text" name="quantity" value="{{ old('quantity',\FunctionLib::numberFormat(@$data['quantity'])) }}" placeholder="Số lượng sản phẩm"
                       id="input-quantity" class="form-control"
                       onkeypress="return shop.numberOnly()" onkeyup="shop.mixMoney(this)" onfocus="this.select()">
            </div>
        </div> --}}

        {{-- <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="input-price">Số lượng tối thiểu đặt hàng <span class="d-block small text-danger">(Khi đặt hàng, phải chọn tối thiểu sl này)</span></label>
            <div class="col-sm-6">
                <input type="text" name="minimum" value="{{ old('minimum',\FunctionLib::numberFormat(@$data['minimum'])) }}" placeholder="Đặt hàng tối thiểu"
                       id="input-minimum" class="form-control"
                       onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()">
            </div>
        </div> --}}

        {{-- <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="subtract_stock">Trừ tồn kho <span class="d-block small text-danger">(Đặt hàng thành công có trừ tồn kho hay ko)</span></label>
            <div class="col-sm-6">
            <select class="form-control{{ $errors->has('subtract_stock') ? ' is-invalid' : '' }}" name="subtract_stock" id="subtract_stock">
                <option value="1" {{@$data['subtract_stock'] == 1 ? 'selected' : ''}}>Có</option>
                <option value="0" {{@$data['subtract_stock'] == 0 ? 'selected' : ''}}>Không</option>
            </select>
            </div>
        </div> --}}

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="stock_status_id">Trạng thái kho</label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('stock_status_id') ? ' is-invalid' : '' }}"
                    name="stock_status_id" id="stock_status_id">
                    <option value="">-- Chọn Trạng thái --</option>
                    @foreach ($stock_status as $item)
                        <option {{ @$data['stock_status_id'] == $item->stock_status_id ? 'selected' : '' }}
                            value="{{ $item->stock_status_id }}">
                            {{ $item->name ?? '<!Chưa nhập ngôn ngữ mặc định>' }}
                        </option>
                    @endforeach
                    <option {{ @$data['stock_status_id'] == 0 ? 'selected' : '' }} value="0"
                        class="font-weight-bold text-success">Còn Hàng</option>
                    <option {{ @$data['stock_status_id'] == -1 ? 'selected' : '' }} value="-1"
                        class="font-weight-bold text-danger">Hết Hàng</option>
                </select>
            </div>
        </div>

        {{-- <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="shipping_required">Vận chuyển <span class="d-block small text-danger">(Sản phẩn yêu cầu phải sử dụng dịch vụ vận chuyển)</span></label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('shipping_required') ? ' is-invalid' : '' }}" name="shipping_required" id="shipping_required">
                    <option value="1" {{@$data['shipping_required'] == 1 ? 'selected' : ''}}>Có</option>
                    <option value="0" {{@$data['shipping_required'] == 0 ? 'selected' : ''}}>Không</option>
                </select>
            </div>
        </div> --}}

        {{-- <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="date_available">Ngày hiệu lực <span class="d-block small text-danger">(Ngày mà sản phẩm chính thức được bày bán)</span></label>
            <div class="col-sm-6">
                <input type="text" name="date_available" readonly value="{{ old('date_available',@$data['date_available'] ? Carbon\Carbon::parse($data['date_available'])->format('d/m/Y H:i') : '') }}" placeholder="dd/mm/YY H:i"
                       id="date_available" class="form-control">
            </div>
        </div> --}}

        {{-- <div class="row form-group align-items-center"> --}}
        {{-- <label class="col-sm-2 control-label text-right mb-0" for="length">Kích thước <span class="d-block small text-danger">(Mặc định cm [D x R X C])</span></label> --}}
        {{-- <div class="col-sm-3"> --}}
        {{-- <input type="text" name="length" value="{{ old('length',\FunctionLib::numberFormat(@$data['length']))}}" placeholder="Dài" --}}
        {{-- id="length" class="form-control" --}}
        {{-- onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()"> --}}
        {{-- </div> --}}

        {{-- <div class="col-sm-3"> --}}
        {{-- <input type="text" name="width" value="{{ old('width',\FunctionLib::numberFormat(@$data['width']))}}" placeholder="Rộng" --}}
        {{-- id="width" class="form-control" --}}
        {{-- onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()"> --}}
        {{-- </div> --}}

        {{-- <div class="col-sm-3"> --}}
        {{-- <input type="text" name="height" value="{{ old('height',\FunctionLib::numberFormat(@$data['height']))}}" placeholder="Cao" --}}
        {{-- id="height" class="form-control" --}}
        {{-- onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()"> --}}
        {{-- </div> --}}
        {{-- </div> --}}

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="weight">Trọng lượng <span
                    class="d-block small text-danger">(Mặc định gram)</span></label>
            <div class="col-sm-3">
                <input type="text" name="weight"
                    value="{{ old('weight', \FunctionLib::numberFormat(@$data['weight'])) }}"
                    placeholder="Trọng lượng" id="weight" class="form-control" onkeypress="return shop.numberOnly()"
                    onkeyup="mixMoney(this)" onfocus="this.select()">
            </div>
        </div>

        {{-- <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="status">Phân loại <span class="d-block small text-danger">(dòng sản phẩm)</span></label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" id="type">
                    @foreach ($producttype as $k => $t)
                        <option value="{{ $k }}"{{@$data['type'] == $k ? ' selected' : ''}}>{{ $t['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div> --}}

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="status">Trạng thái <span
                    class="d-block small text-danger">(Hiển thị hay không)</span></label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status"
                    id="status">
                    <option value="1" {{ @$data['status'] == 1 ? 'selected' : '' }}>Ẩn</option>
                    <option value="2" {{ @$data['status'] == 2 ? 'selected' : '' }}>Hiển thị</option>
                </select>
            </div>
        </div>

        {{-- <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="sort_order">Sắp xếp <span class="d-block small text-danger">(Vị trí, càng nhỏ thì càng lên đầu)</span></label>
            <div class="col-sm-3">
                <input type="text" name="sort_order" value="{{ old('sort_order',\FunctionLib::numberFormat(@$data['sort_order']))}}" placeholder="Vị trí sắp xếp"
                       id="sort_order" class="form-control"
                       onkeypress="return shop.numberOnly()" onfocus="this.select()">
            </div>
        </div> --}}
    </div>
</div>

@push('js_bot_all')
    <script>
        $('#date_available').datetimepicker({
            format: 'd/m/Y H:i',
        });

    </script>
@endpush
