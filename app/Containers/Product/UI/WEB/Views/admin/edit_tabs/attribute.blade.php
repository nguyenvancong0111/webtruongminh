<div class="tab-pane" id="attribute">
    <div class="tabbable">
        <table id="attribute" class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <td class="text-left">Nhóm</td>
                <td class="text-left">Chi tiết</td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            @if(isset($attributes))
            @foreach($attributes as $k => $product_attribute)
            <tr id="attribute-row{{ $k }}">
                <td class="text-left" style="width: 40%;">
                    <input type="text" name="product_attribute[{{ $k }}][name]" value="{{ $product_attribute['name'] }}" placeholder="Tên nhóm" class="input-attr form-control"/>
                    <input type="hidden" name="product_attribute[{{ $k }}][attribute_id]" value="{{ $product_attribute['attribute_id'] }}"/>
                </td>
                <td class="text-left">
                    @foreach($langs as $it_lang)
                    <div class="input-group">
                        <span class="input-group-addon"><img src="/admin/img/lang/{{$it_lang['image']}}" title="{{$it_lang['name']}}"></span>
                        <textarea name="product_attribute[{{ $k }}][product_attribute_description][{{ $it_lang['language_id'] }}][text]" rows="5" placeholder="Chi tiết" class="form-control">{{ isset($product_attribute['desc'][$it_lang['language_id']]) ? $product_attribute['desc'][$it_lang['language_id']]['text'] : '' }}</textarea>
                    </div>
                    @endforeach
                </td>
                <td class="text-right"><button type="button" onclick="$('#attribute-row{{ $k }}').remove();" data-toggle="tooltip" title="Xóa" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
            </tr>
            @endforeach
            @endif
            </tbody>

            <tfoot>
            <tr>
                <td colspan="2"></td>
                <td class="text-right"><button type="button" onclick="addAttribute();" data-toggle="tooltip" title="Thêm mới" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>

@push('js_bot_all')
    <script>
        $(document).ready(function(){
            autocomplete_attr('.input-attr');
        });

        var attribute_row = {{ isset($attributes) && count($attributes) > 0 ? count($attributes) : 0}};

        function addAttribute() {
            html = '<tr id="attribute-row' + attribute_row + '">';
            html += '  <td class="text-left" style="width: 20%;"><input id="" type="text" name="product_attribute[' + attribute_row + '][name]" value="" placeholder="Tên nhóm" class="input-attr form-control" /><input type="hidden" name="product_attribute[' + attribute_row + '][attribute_id]" value="" /></td>';
            html += '  <td class="text-left">';
            @foreach($langs as $it_lang)
            html += '<div class="input-group"><span class="input-group-addon"><img src="/admin/img/lang/{{$it_lang['image']}}" title="{{$it_lang['name']}}"></span><textarea name="product_attribute[' + attribute_row + '][product_attribute_description][{{ $it_lang['language_id'] }}][text]" rows="5" placeholder="Chi tiết" class="form-control"></textarea></div>';
            @endforeach
            html += '  </td>';
            html += '  <td class="text-right"><button type="button" onclick="$(\'#attribute-row' + attribute_row + '\').remove();" data-toggle="tooltip" title="Xóa" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#attribute tbody').append(html);

            // attributeautocomplete(attribute_row);

            autocomplete_attr('#attribute-row' + attribute_row + ' input.input-attr');

            attribute_row++;
        }

        function autocomplete_attr(ele) {
            $(ele).autocomplete({
                classes: {
                    "ui-autocomplete": "dropdown-menu"
                },
                'source': function(request, response) {
                    $.ajax({
                        url: '/admin/ajax/attribute/getAttributes' ,
                        dataType: 'json',
                        data: {name:request.term,_token:ENV.token},
                        success: function(json) {
                            response($.map(json.data.data, function(item) {
                                return {
                                    label: item['fullname'],
                                    value: item['attribute_id'],
                                    abc:item['name']
                                }
                            }));
                        }
                    });
                },
                'select': function(event, ui) {
                    var item = ui.item;
                    $(this).val(item.abc);
                    $(this).siblings('input').val(item.value);
                    return false;
                }
            });
        }
    </script>
@endpush