<div class="tab-pane" id="links">
    <div class="tabbable">
        {{-- <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="manufacturer_id">Nhà cung cấp</label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('manufacturer_id') ? ' is-invalid' : '' }}" name="manufacturer_id" id="manufacturer_id">
                    <option value="">--Chọn--</option>
                    @if(@$data->manufacturer_id > 0)
                        <option value="{{@$data->manufacturer_id}}" selected>{!! @$data->manufacturer->name !!}</option>
                    @endif
                </select>
            </div>
        </div> --}}

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="input-category"><span data-toggle="tooltip" title="Bộ lọc">Danh mục</span></label>
            <div class="col-sm-6">
                <input type="text" placeholder="Danh mục" id="input-category" class="form-control" />
            </div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <div id="product_category" class="row mx-0 list-group flex-row">
                    @if(isset($categories) && !empty($categories))
                    @foreach($categories as $item)
                        <a href="#" id="category-{{$item->category_id}}" class="col-3 list-group-item list-group-item-action">
                            <i class="fa fa-close"></i> {!! $item->path ? $item->path.' > '.$item->name : $item->name  !!}
                            <input type="hidden" name="product_category[]" value="{{$item->category_id}}" />
                        </a>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>

        <div class="row form-group align-items-center mt-3">
            <label class="col-sm-2 control-label text-right mb-0" for="input-filter"><span data-toggle="tooltip" title="Bộ lọc">Bộ lọc</span></label>
            <div class="col-sm-6">
                <input type="text" placeholder="Bộ lọc" id="input-filter" class="form-control" />
            </div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <div id="product_filter" class="row mx-0 list-group flex-row">
                    @if(isset($data->filters))
                    @foreach($data->filters as $item)
                        <a href="#" id="filter-{{$item->filter_id}}" class="col-3 list-group-item list-group-item-action">
                            <i class="fa fa-close"></i> {!! $item->group .' > '.$item->name  !!}
                            <input type="hidden" name="product_filter[]" value="{{$item->filter_id}}" />
                        </a>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@push('js_bot_all')
    <script>
        $("#manufacturer_id").select2({
            width: '100%',
            tags: true,
            ajax: {
                url: "/admin/ajax/manufacturer/getManufacturers",
                dataType: 'json',
                delay: 800,
                data: function (params) {
                    return {
                        name: params.term, // search term
                        page: params.page,
                        _token: ENV.token
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.data.data,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
        });

        $('input#input-filter').autocomplete({
            classes: {
                "ui-autocomplete": "dropdown-menu"
            },
            'source': function(request, response) {
                $.ajax({
                    global: false,
                    url: "{{route('api_filter_get_all')}}" ,
                    dataType: 'json',
                    headers:ENV.headerParams,
                    data: {name:request.term,_token:ENV.token},
                    success: function(json) {
                        response($.map(json.data, function(item) {
                            return {
                                label: item.name,
                                value: item.filter_id
                            }
                        }));
                    }
                });
            },
            'select': function(event, ui) {
                var item = ui.item;

                $('#filter-' + item.value).remove();

                var html = '<a href="#" id="filter-'+item.value +'" class="col-3 list-group-item list-group-item-action">\n' +
                    '                        <i class="fa fa-close"></i> '+item.label+'\n' +
                    '                        <input type="hidden" name="product_filter[]" value="'+item.value+'" />\n' +
                    '                    </a>';

                $('#product_filter').append(html);

                $('input#input-filter').val('');
                return false;
            }
        });

        $('input#input-category').autocomplete({
            classes: {
                "ui-autocomplete": "dropdown-menu"
            },
            'source': function(request, response) {
                $.ajax({
                    global: false,
                    url: "{{route('api_category_get_all')}}" ,
                    headers:ENV.headerParams,
                    dataType: 'JSON',
                    data: {type:'select2',name:request.term,_token:ENV.token},
                    success: function(json) {
                        response($.map(json.data, function(item) {
                            return {
                                label: item.text,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            'select': function(event, ui) {
                var item = ui.item;

                $('#category-' + item.value).remove();

                var html = '<a href="#" id="category-'+item.value +'" class="col-3 list-group-item list-group-item-action">\n' +
                    '                        <i class="fa fa-close"></i> '+item.label+'\n' +
                    '                        <input type="hidden" name="product_category[]" value="'+item.value+'" />\n' +
                    '                    </a>';

                $('#product_category').append(html);

                $('input#input-category').val('');
                return false;
            }
        });

        $('#product_category,#product_filter').delegate('.fa-close', 'click', function() {
            $(this).parent().remove();
        });
    </script>
@endpush