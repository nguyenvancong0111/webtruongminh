@if($product_option['type'] == 'file' )
    <div class="form-group" style="display: none;">
        <label class="col-sm-2 control-label text-right mb-0" for="input-value{{ $k_prd_opt }}">Nội dung tùy chọn</label>
        <div class="col-sm-10">
            <input type="text" name="product_option[{{ $k_prd_opt }}][value]" value="{{ $product_option['value'] }}" placeholder="Nội dung tùy chọn" id="input-value{{ $k_prd_opt }}" class="form-control"/>
        </div>
    </div>
@endif