<div id="product-admin-filters">
    <div v-for="cate in filter_cate_not_price">
        <h5>@{{ cate . title }}</h5>
        <div class="">
            <span class="m-2" v-for="filter in cate.filters">
                <label v-bind:for="'filter_' + filter.id">
                    <input type="checkbox" v-bind:id="'filter_' + filter.id" v-bind:checked="filter.checked == 1"
                        v-bind:name="'filters_[]'" v-bind:value="filter.id">
                    @{{ filter . title }}
                </label>
            </span>
        </div>
    </div>
    @include('BackEnd::layouts.loader')
</div>

@push('js_bot_all')
    <script>
        {{-- var mount = '{!! json_encode($mount) !!}'; --}}
        {{-- var storage = '{!! json_encode($storage) !!}'; --}}
        var filter_cate_not_price = '{!! json_encode($filter_cate_not_price) !!}';

    </script>
    {!! \FunctionLib::addMedia('admin/features/product/product_filters.js') !!}
@endpush
