<div id="product-admin-prices">
    <div v-for="cate in filter_cate_price">
        <h5>@{{ cate . title }}</h5>
        <div class="">
            <span class="m-2">
                <label v-bind:for="'filter_0_cate'+cate.id">
                    <input type="radio" v-bind:id="'filter_0_cate'+cate.id" checked v-model="cate.checked"
                        v-bind:name="'filter_cate_[' + cate.id+']'" v-bind:value="0">
                    Không chọn
                </label>
            </span>
            <span class="m-2" v-for="filter in cate.filters">
                <label v-bind:for="'filter_' + filter.id">
                    <input type="radio" v-bind:id="'filter_' + filter.id" v-model="cate.checked"
                        v-bind:name="'filter_cate_[' + cate.id+']'" v-bind:value="filter.id">
                    @{{ filter . title }}
                </label>
            </span>
        </div>
    </div>
    <div>
        <button class="btn btn-outline-info" @click="filter_click($event)">Chọn</button>
    </div>

    <h4>Danh sách giá và số lượng</h4>
    <div v-if="filter_cate_price.length > 0" class="row" v-for="(item, index_price ) in filter_prices">
        <div class="col-lg-12">
            <p>
                <input type="hidden" name="filter_price_ids[]" v-bind:value="item.key_price">
                <span v-for="abc in item.obj" class="badge badge-success m-1 font-lg">@{{ abc . title }}</span>
            </p>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label for="price">Giá hiển thị</label>
                <input type="text" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" id="price"
                    name="prices[]" v-bind:value="item.price" required onkeypress="return shop.numberOnly()"
                    onkeyup="mixMoney(this)" onfocus="this.select()">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label for="priceStrike">Giá gạch</label>
                <input type="text" class="form-control{{ $errors->has('priceStrike') ? ' is-invalid' : '' }}"
                    id="priceStrike[]" name="priceStrikes[]" v-bind:value="item.price_strike"
                    onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" required onfocus="this.select()">
            </div>
        </div>
        <div class="col-sm-6 d-flex">
            <div class="form-group mr-3" v-for="wh in item.warehouse">
                <div>
                    <label for="unit">@{{ wh . title }}</label>
                    <input v-bind:value="wh.amount" type="text" class="form-control" id="quantity"
                        v-bind:name="'quantity['+index_price+']['+wh.id+']'">
                </div>
            </div>
        </div>
        <div class="col-sm-2">
            <button class="btn btn-danger" @click="remove_price($event, item);">Xóa</button>
        </div>
    </div>
    @include('BackEnd::layouts.loader')
</div>

@push('js_bot_all')
    <script>
        {{-- var mount = '{!! json_encode($mount) !!}'; --}}
        var warehouse_ = '{!! json_encode($warehouse) !!}';
        var filter_cate_price = '{!! json_encode($filter_cate_price) !!}';
        var filter_prices = '{!! isset($product_prices['data']) ? json_encode($product_prices['data']) : '' !!}';

    </script>
    {!! \FunctionLib::addMedia('admin/features/product/product_prices.js') !!}
@endpush
