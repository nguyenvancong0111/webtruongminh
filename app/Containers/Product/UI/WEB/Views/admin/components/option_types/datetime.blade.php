@if($product_option['type'] == 'datetime' )
    <div class="form-group row align-items-center">
        <label class="col-sm-2 control-label text-right mb-0" for="input-value{{ $k_prd_opt }}">Nội dung tùy chọn</label>
        <div class="col-sm-10">
            <div class="input-group datetime">
                <input type="text" name="product_option[{{ $k_prd_opt }}][value]" value="{{ $product_option['value'] }}" placeholder="Nội dung tùy chọn" data-date-format="YYYY-MM-DD HH:mm" id="input-value{{ $k_prd_opt }}" class="form-control"/> <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
        </div>
    </div>
@endif