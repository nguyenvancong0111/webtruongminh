@extends('BackEnd::layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if (old_blade('editMode'))
                {!! Form::open(['url' => route('admin.' . $key . '.edit.post', $stock_status_id), 'files' => true]) !!}
            @else
                {!! Form::open(['url' => route('admin.' . $key . '.add.post'), 'files' => true]) !!}
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif
            <div class="card card-accent-primary">
                <div class="card-header">
                    <i class="fa fa-pencil"></i>THÔNG TIN CƠ BẢN
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs">
                                @foreach ($langs as $it_lang)
                                    <li class="nav-item">
                                        <a class="nav-link {{ $loop->first ? 'active' : '' }}" data-toggle="tab"
                                            href="#lang_{{ $it_lang['language_id'] }}" role="tab" aria-controls="website"
                                            aria-expanded="true"><i class="icon-globe"></i> {{ $it_lang['name'] }}</a>
                                    </li>
                                @endforeach
                            </ul>

                            <div class="tab-content p-0">
                                @foreach ($langs as $it_lang)
                                    <div class="tab-pane {{ $loop->first ? 'active' : '' }}"
                                        id="lang_{{ $it_lang['language_id'] }}" role="tabpanel" aria-expanded="true">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="title">Tên<span class="small text-danger">(
                                                            {{ $it_lang['name'] }} )</span></label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><img
                                                                src="/admin/img/lang/{{ $it_lang['image'] }}"
                                                                title="{{ $it_lang['name'] }}"></span>
                                                        <input type="text" class="form-control"
                                                            name="name[{{ $it_lang['language_id'] }}]"
                                                            id="name_{{ $it_lang['language_id'] }}"
                                                            value="{{ old('name.' . $it_lang['language_id'], @$data[$it_lang['language_id']]->name) }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb-3">
                <button type="submit" id="submit-filter" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i>
                    Thêm mới</button>
                &nbsp;&nbsp;
                <a class="btn btn-sm btn-danger" href="{{ redirect()->back()->getTargetUrl() }}"><i
                        class="fa fa-ban"></i> Hủy bỏ</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@stop

@section('js_bot')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    {!! \FunctionLib::addMedia('admin/js/library/ckeditor/ckeditor.js') !!}
    <script>
        $("#attribute_group_id").select2({
            tags: true,
        });

    </script>
@stop
