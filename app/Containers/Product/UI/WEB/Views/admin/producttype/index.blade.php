@extends('BackEnd::layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5">
                <h1>Quản trị {{ $site_title }}</h1>
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            {!! Form::open(['url' => route('admin.' . $key), 'method' => 'get', 'id' => 'searchForm']) !!}
            <div class="card card-accent-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-bookmark-o"></i></span>
                                <input type="text" name="name" class="form-control" placeholder="Tên"
                                    value="{{ $search_data->name }}">
                            </div>
                        </div>
                        {{-- <div class="form-group col-sm-3"> --}}
                        {{-- <div class="input-group"> --}}
                        {{-- <span class="input-group-addon"><i class="fa fa-language"></i></span> --}}
                        {{-- <select id="lang" name="lang" class="form-control"> --}}
                        {{-- <option value="">-- Chọn ngôn ngữ --</option> --}}
                        {{-- @foreach ($langs as $k => $v) --}}
                        {{-- <option value="{{ $k }}" @if ($search_data->lang == $k) selected="selected" @endif>{{ $v['name'] }}</option> --}}
                        {{-- @endforeach --}}
                        {{-- </select> --}}
                        {{-- </div> --}}
                        {{-- </div> --}}
                        {{-- <div class="form-group col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                <select id="status" name="status" class="form-control">
                                    <option value="">-- Chọn trạng thái --</option>
                                    <option value="2"{{ $search_data->status == 1 ? ' selected="selected"' : '' }}>Đang hiển thị</option>
                                    <option value="1"{{ $search_data->status == 2 ? ' selected="selected"' : '' }}>Đang ẩn</option>
                                    <option value="-1"{{ $search_data->status == -1 ? ' selected="selected"' : '' }}>Đã xóa</option>
                                </select>
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                    <button type="button" class="btn btn-sm btn-primary"
                        onclick="window.location.href='{{ route('admin.' . $key) }}'"><i class="fa fa-recycle"></i>
                        Reset</button>
                </div>
            </div>
            {!! Form::close() !!}

            <div class="card card-accent-primary">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Danh sách
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th width="55">ID</th>
                                <th>Tên</th>
                                <th>Tham số</th>
                                @if (\FunctionLib::can($permission, 'edit') || \FunctionLib::can($permission, 'delete'))
                                    <th width="55" class="text-center">Lệnh</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $k => $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ @$item->name ?? '!<Chưa nhập nội dung>' }}</td>
                                    <td>type={{ $item->id }}</td>
                                    <td align="center">
                                        @if (\FunctionLib::can($permission, 'edit'))
                                            <a href="{{ route('admin.' . $key . '.edit', $item->id) }}"
                                                class="btn text-primary"><i class="icon-pencil icons"></i></a>
                                        @endif
                                        @if (\FunctionLib::can($permission, 'delete'))
                                            <a href="{{ route('admin.' . $key . '.delete', $item->id) }}"
                                                class="btn text-danger" onclick="return confirm('Bạn muốn xóa ?')"><i
                                                    class="icon-trash icons"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @if (empty($data) || $data->isEmpty())
                        <h4 align="center">Không tìm thấy dữ liệu phù hợp</h4>
                    @else
                        <div class="pull-right">Tổng cộng: {{ $data->count() }} bản ghi / {{ $data->lastPage() }} trang
                        </div>
                        {!! $data->links('BackEnd::layouts.pagin') !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
