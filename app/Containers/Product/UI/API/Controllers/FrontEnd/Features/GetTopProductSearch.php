<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-12 15:21:11
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Product\Enums\ProductStatus;
use App\Containers\Product\UI\API\Requests\FrontEnd\GetTopProductSearchHomeRequest;
use App\Containers\Product\UI\API\Transformers\FrontEnd\ProductListTransformer;
use App\Containers\PromotionCampaign\Actions\FrontEnd\ApplyCampaignForProductListAction;

trait GetTopProductSearch
{
    public function getTopProductSearch(
        GetTopProductSearchHomeRequest $request,
        ApplyCampaignForProductListAction $applyCampaignForProductListAction
    ) {
        $skipPagination=true;
        $limit=12;
        if(isset($request->view_all)){
            $skipPagination=false;
            $limit=20;
            
        }
        $currentPage=1;
        if(isset($request->page)){
            $currentPage=$request->page;
        }
        $products = $this->productListingAction->run(
            [
                'is_home' => ProductStatus::IS_HOME,
                'is_top_searching' => ProductStatus::IS_TOP_SEARCHING,
                'inRandomOrder' => false, // Nếu để random thì orderBy ko còn được áp dụng
            ], // filters
            [
                'sort_order' => 'asc',
                'updated_at' => 'desc',
                'created_at' => 'desc'
            ], // orderBy
            $limit, // limit
            $skipPagination, // Skip pagination
            $this->currentLang, // curent lang
            [
                'with_relationship' => [
                    'manufacturer',
                    'specialTags' => function($q){
                        $q->mustHaveDesc($this->currentLang->language_id);
                    },
                    'specialTags.desc' => function($q){
                        $q->activeLang($this->currentLang->language_id);
                    }
                ]
                ],
                $currentPage   
        );

        $applyCampaignForProductListAction->run($products);

        return $this->transform($products, ProductListTransformer::class, [], [], 'product');
    }
}
