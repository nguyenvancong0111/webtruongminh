<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-09 18:07:50
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Controllers\FrontEnd\Features;

use Apiato\Core\Transformers\Serializers\PureArraySerializer;
use App\Containers\Collection\Actions\FrontEnd\GetProductByCollectionAction;
use App\Containers\Product\UI\API\Requests\FrontEnd\GetProductByCollectionRequest;
use App\Containers\Product\UI\API\Transformers\FrontEnd\ProductListTransformer;
use App\Containers\PromotionCampaign\Actions\FrontEnd\ApplyCampaignForProductListAction;

trait GetProductByCollection
{
    public function getProductByCollection(
        GetProductByCollectionRequest $request,
        ApplyCampaignForProductListAction $applyCampaignForProductListAction
    ) {
        $products = app(GetProductByCollectionAction::class)
            ->descSpecialOfffer($this->currentLang)
            ->descTags($this->currentLang)
            ->run([
                'collectionId' => [$request->collection_id]
            ], [
                'product.manufacturer',
                'product.specialOffers',
                'product.specialOffers.desc',
                'product.specialTags',
                'product.specialTags.desc'
            ], $this->currentLang, true, 20);

        $applyCampaignForProductListAction->run($products);
        // dd($products);
        // return $products;
        return $this->transform($products, ProductListTransformer::class, [], [], 'product');
    }
}
