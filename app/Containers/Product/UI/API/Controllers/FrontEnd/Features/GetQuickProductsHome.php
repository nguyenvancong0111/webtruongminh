<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-12 15:20:58
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Product\Enums\ProductStatus;
use App\Containers\Product\UI\API\Requests\FrontEnd\GetQuickProductsHomeRequest;
use App\Containers\Product\UI\API\Transformers\FrontEnd\ProductListTransformer;
use App\Containers\PromotionCampaign\Actions\FrontEnd\ApplyCampaignForProductListAction;

trait GetQuickProductsHome
{
    public function getQuickProductsHome(
        GetQuickProductsHomeRequest $request,
        ApplyCampaignForProductListAction $applyCampaignForProductListAction
    ) {
        $products = $this->productListingAction->run(
            [
                'is_home' => ProductStatus::IS_HOME,
                'isQuick' => ProductStatus::IS_QUICK,
                // 'inRandomOrder' => true, // Nếu để random thì orderBy ko còn được áp dụng
            ], // filters
            [
                'sort_order' => 'asc',
                'updated_at' => 'desc',
                'created_at' => 'desc'
            ], // orderBy
            16, // limit
            true, // Skip pagination
            $this->currentLang, // curent lang
            [
                'with_relationship' => [
                    'manufacturer',
                    'specialTags' => function($q){
                        $q->mustHaveDesc($this->currentLang->language_id);
                    },
                    'specialTags.desc' => function($q){
                        $q->activeLang($this->currentLang->language_id);
                    }
                ]
            ]
        );

        $applyCampaignForProductListAction->run($products);

        return $this->transform($products, ProductListTransformer::class, [], [], 'product');
    }
}
