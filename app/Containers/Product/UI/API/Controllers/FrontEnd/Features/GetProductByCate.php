<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-12 19:46:14
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Category\Enums\CategoryType;
use App\Containers\Product\Enums\ProductStatus;
use App\Containers\Product\UI\API\Requests\FrontEnd\GetProductByCateRequest;
use App\Containers\Product\UI\API\Transformers\FrontEnd\ProductListTransformer;
use App\Containers\PromotionCampaign\Actions\FrontEnd\ApplyCampaignForProductListAction;

trait GetProductByCate
{
    public function getProductByCate(
        GetProductByCateRequest $request,
        ApplyCampaignForProductListAction $applyCampaignForProductListAction
    ) {
        $arrCates = $this->getCategoryByIdAction->run(
            CategoryType::PRODUCT,
            $request->cate_id,
            $this->currentLang,
            true
        );

        $conditions = [
            'cate_ids' => $arrCates
        ];

        isset($request['isHot']) ? $conditions['isHot'] = $request['isHot'] ? ProductStatus::IS_HOT : ProductStatus::ZERO : false;
        isset($request['isHome']) ? $conditions['is_home'] = $request['isHome'] ? ProductStatus::IS_HOME : ProductStatus::ZERO : false;
        isset($request['isPromotion']) ? $conditions['isPromotion'] = $request['isPromotion'] ? ProductStatus::IS_PROMOTION : ProductStatus::NON_PROMOTION : false;
// dd($conditions['isHot']);
        $products = $this->productListingAction->run(
            $conditions, // $filters
            ['created_at' => 'DESC'], // $orderby
            18, // $Limit
            true, // $skipPagination
            $this->currentLang, // $currentLang
            [
                'with_relationship' => [
                    'manufacturer',
                    'specialTags' => function($q){
                        $q->mustHaveDesc($this->currentLang->language_id);
                    },
                    'specialTags.desc' => function($q){
                        $q->activeLang($this->currentLang->language_id);
                    }
                ]
            ]
        );

        $applyCampaignForProductListAction->run($products);

        return $this->transform($products, ProductListTransformer::class, [], [], 'product');
    }
}
