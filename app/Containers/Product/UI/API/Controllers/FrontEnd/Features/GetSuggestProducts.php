<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-12 19:46:00
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Product\Enums\ProductStatus;
use App\Containers\Product\UI\API\Requests\FrontEnd\GetSuggestProductsRequest;
use App\Containers\Product\UI\API\Transformers\FrontEnd\ProductListTransformer;
use App\Containers\PromotionCampaign\Actions\FrontEnd\ApplyCampaignForProductListAction;

trait GetSuggestProducts
{
    public function getSuggestProducts(
        GetSuggestProductsRequest $request,
        ApplyCampaignForProductListAction $applyCampaignForProductListAction
    ) {
        $currentPage=1;
        $inRandomOrder=false;
        if(isset($request->page)){
            $currentPage=$request->page;
        }
        $products = $this->productListingAction->run(
            [
                'isHot' => ProductStatus::IS_HOT,
                // 'inRandomOrder' => $inRandomOrder, // Nếu để random thì orderBy ko còn được áp dụng
            ], // filters
            [
                
                'updated_at' => 'desc',
                'created_at' => 'desc',
                'sort_order' => 'asc',
            ], // orderBy
            10, // limit
            false, // Skip pagination
            $this->currentLang, // curent lang
            [
                'with_relationship' => [
                    'manufacturer',
                    'specialTags' => function($q){
                        $q->mustHaveDesc($this->currentLang->language_id);
                    },
                    'specialTags.desc' => function($q){
                        $q->activeLang($this->currentLang->language_id);
                    }
                ]
                ],
                $currentPage
        );
// dd($products);
        $applyCampaignForProductListAction->run($products);

        return $this->transform($products, ProductListTransformer::class, [], [], 'product');
    }
}
