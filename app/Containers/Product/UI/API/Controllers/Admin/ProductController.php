<?php

namespace App\Containers\Product\UI\API\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Product\UI\API\Requests\LoadAllProductRequest;
use App\Containers\Product\UI\API\Transformers\AllProductsTransformer;
use App\Ship\Parents\Controllers\ApiController;

class ProductController extends ApiController
{
    public function loadAll(LoadAllProductRequest $request) {
        $products = Apiato::call('Product@Admin\GetAllProductsAction', [$request, ['with_relationship' => ['categories', 'categories.desc']],1]);
        return $this->transform($products,AllProductsTransformer::class,[], [], 'product');
    }
}
