<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-07 13:09:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-07 13:10:14
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Transformers;

use App\Ship\Parents\Transformers\Transformer;

class AllProductsTransformer extends Transformer
{

    /**
     * @param $token
     *
     * @return  array
     */
    public function transform($prd)
    {
        $response = [
            'id' => $prd->id,
            'title' => $prd->desc->name,
            'desc' => $prd->desc->short_description,
            'price' => $prd->price
        ];

        return $response;
    }
}
