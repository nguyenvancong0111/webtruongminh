<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-09 01:30:53
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-05 10:48:28
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Transformers\FrontEnd\Detail;

use App\Ship\Parents\Transformers\Transformer;

class ProductOptionsTransfomer extends Transformer
{
    protected $defaultIncludes = [
        'product_option_values'
    ];

    public function transform($option)
    {
        return [
            'product_option_id' => $option['product_option_id'],
            'option_id' => $option['option_id'],
            'name' => $option['name'],
            'show_color' => $option['show_image'],
        ];
    }

    public function includeProductOptionValues($option)
    {
        $optionValues = $option['product_option_value'];
        // dd($optionValues);

        return $this->collection($optionValues, new ProductOptionValueTransformer, 'product_option_values');
    }
}
