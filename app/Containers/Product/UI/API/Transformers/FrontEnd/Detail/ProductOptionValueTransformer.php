<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-09 01:30:53
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-16 18:28:20
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Transformers\FrontEnd\Detail;

use Apiato\Core\Foundation\ImageURL;
use App\Ship\Parents\Transformers\Transformer;

class ProductOptionValueTransformer extends Transformer
{
    public function transform($option)
    {
        $images = json_decode($option['images']) ?? [];
        if ($images && !empty($images)) {
            foreach ($images as &$img) {
                $img = (array)$img;
                /*$img['image_original'] = ImageURL::getImageUrl(@$img['name'], 'product', 'original');
                $img['image_small'] = ImageURL::getImageUrl(@$img['name'], 'product', 'small');
                $img['image_medium'] = ImageURL::getImageUrl(@$img['name'], 'product', 'mediumx3');*/
                $img['image_social'] = ImageURL::getImageUrl(@$img['name'], 'product', 'social');
            }
        }

        return [
            'option_id' => $option['option_id'],
            'product_option_value_id' => $option['product_option_value_id'],
            'option_value_id' => $option['option_value_id'],
            'product_variant_id' => $option['product_variant_id'],
            'name' => @$option['name'] ?? (isset($option['option_value']['desc']['name']) ? $option['option_value']['desc']['name'] : ''),
            'color' => $option['color'],
            // 'price' => $option['price'],
            'images' => $images,
        ];
    }
}
