<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-09 01:30:53
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-06 16:39:22
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Transformers\FrontEnd\Detail;

use App\Containers\Product\UI\API\Transformers\FrontEnd\ProductListTransformer;

class ProductCollectionTransformer extends ProductListTransformer
{
    public function transform($collection)
    {
        $prd=$collection->product;
        $response = parent::transform($prd);
        return $response;
    }
    public function includeSpecialTags($collection)
    {
        $prd=$collection->product;
        return parent::includeSpecialTags($prd);
    }

    public function includeBrand($collection) {
        $prd=$collection->product;
        return parent::includeBrand($prd);
    }
}
