<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-09 01:30:53
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-15 10:44:04
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Transformers\FrontEnd\Detail;

use Apiato\Core\Foundation\FunctionLib;
use App\Ship\Parents\Transformers\Transformer;

class ProductVariantsTransformer extends Transformer
{
    protected $defaultIncludes = [
        'product_option_values'
    ];

    public function transform($variant)
    {
        $response = [
            'product_variant_id' => $variant->product_variant_id,
            'product_id' => $variant->product_id,
            'sku' => $variant->sku,
            'price' => $variant->price,
            'price_formated' => FunctionLib::priceFormat($variant->price),
            'global_price' => $variant->global_price,
            'global_price_formated' => FunctionLib::priceFormat($variant->global_price),
            // 'price_final' => $variant->price_final,
            'status' => (int)$variant->status,
            'status_text' => $variant->status_text,
            'status_type' => $variant->status_type,
            'stock' => (int)$variant->stock,
            'is_root' => (int)$variant->is_root,
            'discount' => 100 - (@$variant->price > 0 && @$variant->global_price > 0 ? round($variant->price/$variant->global_price*100) : 100) ,
        ];

        return $response;
    }

    public function includeProductOptionValues($variant)
    {
        $optionValues = $variant->product_option_values;

        // dd($optionValues);

        return $this->collection($optionValues, new ProductOptionValueTransformer,'product_option_values');
    }

}
