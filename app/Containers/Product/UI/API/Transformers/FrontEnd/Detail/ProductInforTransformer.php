<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-09 01:30:53
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-12 15:24:03
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Transformers\FrontEnd\Detail;

use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\ImageURL;
use App\Containers\BaseContainer\UI\API\Transformers\FrontEnd\BaseCategoryTransformer;
use App\Containers\Manufacturer\UI\API\Transformers\FrontEnd\ManufacturerTransformer;
use App\Containers\Product\UI\API\Transformers\FrontEnd\ProductListTransformer;
use App\Containers\Tags\UI\API\Transformers\FrontEnd\SpecialTagsTransfomer;
use App\Ship\Parents\Transformers\Transformer;

class ProductInforTransformer extends Transformer
{
    protected $defaultIncludes = [
        'variants',
        //'suggests',
        //'topbuy',
        //'categories',
        //'special_tags',
        //'brand',
        //'similar'
    ];

    public function transform($prd)
    {
        // dd(array_rand($arrOffers));
        $response = [
            // "brand" => [
            //     'id' => @$prd->manufacturer->manufacturer_id,
            //     'name' => @$prd->manufacturer->name,
            //     'image' => !empty(@$prd->manufacturer->image) ? ImageURL::getImageUrl($prd->manufacturer->image,'manufacturer','small') : '',
            //     'link_brand'=>route('web_manufacturer_page',['manufacturer_id'=>@$prd->manufacturer->manufacturer_id,'slug'=>\Str::slug(@$prd->manufacturer->name)])
            // ],
            "ProductID" => $prd->id,
            "Name" => $prd->desc->name,
            "Slug" => $prd->desc->slug,
            //"ProductURL" => routeFrontEndFromOthers('web_product_detail_page', ['slug' => $prd->desc->slug, 'id' => $prd->id]),
            //"ProductImage" => ImageURL::getImageUrl($prd->image, 'product', 'mediumx2'),
            //"product_image_medium" => ImageURL::getImageUrl($prd->image, 'product', 'mediumx2'),
            //"product_image_small" => ImageURL::getImageUrl($prd->image, 'product', 'small'),
            "Price" => $prd->price,
            "PriceFormated" => FunctionLib::numberFormat($prd->price),
            "OldPrice" => $prd->global_price,
            "OldPriceFormated" => FunctionLib::numberFormat($prd->global_price),
            "discount" => isset($prd->discount) ? $prd->discount : (100 - (@$prd->price > 0 && @$prd->global_price > 0 ? round($prd->price / $prd->global_price * 100) : 100)),
            //"isHot" => $prd->hot ? true : false,
            //"isHome" => $prd->is_home ? true : false,
            //"isNew" => $prd->is_new ? true : false,
            //"isTopSearch" => $prd->is_top_searching ? true : false,
            //"isFreeShip" => $prd->shipping_required ? false : true,
            //'isQuick' => $prd->is_quick ? true : false,
            //"totalSold" => FunctionLib::numberFormat((int)@$prd->purchased_count),
            //"special_offer" => $prd->relationLoaded('specialOffers') ? $prd->specialOffers->pluck('desc')->toArray() : [],
            // "specialTags" => [
            //     [
            //         "img" => "/template/images/ic-tro-gia.png",
            //         "name" => "Trả góp",
            //         "bgColor" => "#f34343",
            //         "specialType" => false
            //     ]
            // ],
            //"rating" => $prd->avg_rating,
            //"coupons" => [],
            //"campaign" => isset($prd->campaign) ? $prd->campaign : null
        ];

        return $response;
    }

    public function includeVariants($prd)
    {
        return !empty($prd->variants) ? $this->collection($prd->variants, new ProductVariantsTransformer, 'variants') : $this->null();
    }

    public function includeSuggests($prd)
    {
        return !empty($prd->suggests) ? $this->collection($prd->suggests, new ProductListTransformer, 'suggests') : $this->null();
    }

    public function includeTopbuy($prd)
    {
        return !empty($prd->topBuy) ? $this->collection($prd->topBuy, new ProductListTransformer, 'topbuy') : $this->null();
    }

    public function includeCategories($prd)
    {
        return !empty($prd->categories) ? $this->collection($prd->categories, new BaseCategoryTransformer, 'categories') : $this->null();
    }

    public function includeSpecialTags($prd)
    {
        return ($prd->relationLoaded('specialTags') && !empty($prd->specialTags)) ? $this->collection($prd->specialTags, new SpecialTagsTransfomer, 'special_tags') : $this->null();
    }

    public function includeBrand($prd)
    {
        return ($prd->relationLoaded('manufacturer') && !empty($prd->manufacturer)) ? $this->item($prd->manufacturer, new ManufacturerTransformer, 'brand') : $this->null();
    }

    public function includeSimilar($prd)
    {
        // dd($prd->similar);
        return !empty($prd->similar) ? $this->collection($prd->similar, new ProductListTransformer, 'similar') : $this->null();
    }
}
