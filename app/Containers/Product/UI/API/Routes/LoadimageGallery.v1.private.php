<?php

/**
 * @apiGroup           Category
 * @apiName            getCategories
 * @api                {get} /v1/category/getCategories Get All Catagories
 *
 * @apiVersion         1.0.0
 * @apiPermission      Authenticated User
 *
 * @apiUse             GeneralSuccessMultipleResponse
 */
Route::group(
[
    'middleware' => [
        'auth:api',
    ],
],
function () use ($router) {
    $router->post('product/loadImgGallery', [
        'as' => 'api_product_load_img_gallery',
        'uses'       => 'Admin\Controller@loadImgGallery'
    ]);

    $router->post('product/uploadImgGallery', [
        'as' => 'api_product_upload_img_gallery',
        'uses'       => 'Admin\Controller@uploadImgGallery'
    ]);
});
