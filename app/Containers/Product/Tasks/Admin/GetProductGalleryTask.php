<?php

namespace App\Containers\Product\Tasks\Admin;

use App\Containers\Product\Data\Repositories\ProductImageRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class GetProductGalleryTask.
 */
class GetProductGalleryTask extends Task
{

    protected $repository;

    public function __construct(ProductImageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($product_id, $type = 'product', $json = false)
    {
        $images = $this->repository->getModel()->where('type', $type)->where('object_id', $product_id)->orderByRaw('sort asc')->get();
        // $images = $this->repository->getModel()->where('type', $type)->where('object_id', $product_id)->orderByRaw('sort desc,created desc')->get();
        $data = [];
        if(!$images->isEmpty()) {
            foreach ($images as $image) {
                $tmp = $image->toArray();
                $tmp['img'] = $image->image;
                $tmp['image_sm'] = $image->getImageUrl('small');
                $tmp['image_md'] = $image->getImageUrl('mediumx2');
                $tmp['image'] = $image->getImageUrl('largex2');
                $tmp['image_org'] = $image->getImageUrl();
                array_push($data, $tmp);
            }
        }
        return $json ? json_encode($data) : $data;
    }
}
