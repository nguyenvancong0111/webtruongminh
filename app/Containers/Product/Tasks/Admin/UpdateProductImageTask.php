<?php

namespace App\Containers\Product\Tasks\Admin;

use App\Containers\Product\Data\Repositories\ProductImageRepository;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class UpdateProductImageTask.
 */
class UpdateProductImageTask extends Task
{

    protected $repository;

    public function __construct(ProductImageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($img_upload_for_add,int $id)
    {
        if (!empty($img_upload_for_add)) {
            $this->repository->getModel()->whereIn('id', $img_upload_for_add)->update(['object_id' => $id]);
        }
    }
}
