<?php

namespace App\Containers\Product\Tasks\ProductCollection;

use App\Containers\Product\Models\Product;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SaveProductCategoryTask.
 */
class SaveProductCollectionTask extends Task
{
    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($product_collection,Product $current_product_model)
    {
        if (!empty($product_collection)) {
            if (is_array($product_collection)) {
               $current_product_model->collections()->sync($product_collection);
            } else {
                $current_product_model->collections()->attach($product_collection);
            }
        }else {
            $current_product_model->collections()->sync([]);
        }
    }
}
