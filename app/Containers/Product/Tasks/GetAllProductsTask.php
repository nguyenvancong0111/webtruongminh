<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-18 10:22:36
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-17 10:03:27
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Tasks;

use App\Containers\Product\Data\Criterias\ByBrandsCriteria;
use App\Containers\Product\Data\Criterias\ByCollectionsCriteria;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Containers\Product\Data\Criterias\ByCategoriesCriteria;
use App\Containers\Product\Data\Criterias\ByFilterGroupsCriteria;
use App\Containers\Product\Data\Criterias\ByIsHomeCriteria;
use App\Containers\Product\Data\Criterias\ByIsHotCriteria;
use App\Containers\Product\Data\Criterias\ByIsNewCriteria;
use App\Containers\Product\Data\Criterias\ByIdSyncCriteria;
use App\Containers\Product\Data\Criterias\ByIsPromotionCriteria;
use App\Containers\Product\Data\Criterias\ByIsQuickCriteria;
use App\Containers\Product\Data\Criterias\ByIsSaleCriteria;
use App\Containers\Product\Data\Criterias\ByIsShippingRequiredCriteria;
use App\Containers\Product\Data\Criterias\ByIsTopSearchingCriteria;
use App\Containers\Product\Data\Criterias\ByTypeCriteria;
use App\Containers\Product\Data\Criterias\LikeNameCriteria;
use App\Ship\Criterias\Eloquent\ByCreatedCriteria;
use App\Ship\Criterias\Eloquent\ByDateAvailableCriteria;
use App\Ship\Criterias\Eloquent\ByIdCriteria;
use App\Ship\Criterias\Eloquent\ByIdsCriteria;
use App\Ship\Criterias\Eloquent\ByStatusCriteria;
use App\Ship\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;
use App\Ship\Criterias\Eloquent\ThisBetweenSomethingsCriteria;
use App\Ship\Criterias\Eloquent\ThisNotInThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class GetAllProductssTask.
 */
class GetAllProductsTask extends Task
{

    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($external_data, $defaultLanguage = null, $skipPagination = false, $limit = 10, $filters = [], int $currentPage = 1): iterable
    {
//        dd($currentPage);
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;
        if (isset($filters['status']) && $filters['status'] != '') {
            $conds[] = ['status', $filters['status']];
        } else {
            $conds[] = ['status', '>', 0];
        }
        $this->repository->with(array_merge(['categories','categories.desc',
            'desc' => function ($query) use ($language_id) {
                $query->select('id', 'product_id', 'name', 'slug','short_description'/* 'meta_title'*/);
                $query->activeLang($language_id);
            },
            /*'specialOffers.desc' => function ($query) use ($language_id) {
                $query->select('id', 'special_offer_id', 'name');
                $query->activeLang($language_id);
            }*/
        ], isset($external_data['with_relationship']) ? $external_data['with_relationship'] : []))->where($conds);

        $this->repository->whereHas('desc', function ($q) {
            $q->where('slug', '!=', '')->whereNotNull('slug');
        });

        if (isset($filters['cate_ids']) && is_array($filters['cate_ids']) && !empty($filters['cate_ids'][0]) ) {
            $this->repository->whereHas('categories', function (Builder $q) use ($filters) {
                $q->whereIn('category.category_id', $filters['cate_ids']);
            });
        }

        if (isset($filters['name'])) {
            $this->repository->whereHas('desc', function (Builder $query) use ($filters, $language_id) {
                $query->activeLang($language_id);
                $query->where('name', 'like', '%'.$filters['name'].'%');
            });
        }
        $this->repository->whereHas('desc', function ($q) {
            $q->where('slug', '!=', '')->whereNotNull('slug');
        });
        // $externalData['with_relationship'] = array_merge(@$externalData['with_relationship'] ?? [],[
        //     'specialOffers.desc'
        // ]);
        // DB::enableQueryLog();
        // $this->repository->limit($limit);
        // dd(DB::getQueryLog());
        $this->repository->orderByRaw('id DESC');
        return $skipPagination ? ($limit == 0 ? $this->repository->all() : $this->repository->limit($limit)) : $this->repository->paginate($limit);
    }

    public function exceptProductIds(array $ids): self
    {
        $this->repository->pushCriteria(new ThisNotInThatCriteria('products.id', $ids));
        return $this;
    }

    public function inRandomOrder(): self
    {
        $this->repository->inRandomOrder();
        return $this;
    }

//    public function orderBy(array $orderBy = [])
//    {
//        foreach ($orderBy as $field => $direction) {
//            $this->repository->orderBy($field, $direction);
//        }
//        return $this;
//    }

    public function ordereByCreated(): self
    {
        $this->repository->pushCriteria(new OrderByCreationDateDescendingCriteria());
        return $this;
    }

    public function equalId($id): self
    {
        $this->repository->pushCriteria(new ByIdCriteria($id));
        return $this;
    }

    public function equalIds(array $ids): self
    {
        $this->repository->pushCriteria(new ByIdsCriteria($ids));
        return $this;
    }

    public function equalSyncId($sync_id): self
    {
        $this->repository->pushCriteria(new ByIdSyncCriteria($sync_id));
        return $this;
    }

    /**
     *
     */
    public function withVariantByIds(array $ids): self
    {
        // $this->repository->pushCriteria(new );
        return $this;
    }

    public function equalStatus($status): self
    {
        $this->repository->pushCriteria(new ByStatusCriteria($status));
        return $this;
    }

    public function greaterThanStatus($status): self
    {
        $this->repository->pushCriteria(new ThisOperationThatCriteria('status', $status, '>'));
        return $this;
    }

    public function createdAt($created_at, $operation = '<='): self
    {
        $this->repository->pushCriteria(new ByCreatedCriteria($created_at, $operation));
        return $this;
    }

    public function isHome($isHome): self
    {
        $this->repository->pushCriteria(new ByIsHomeCriteria($isHome));
        return $this;
    }

    public function isTopSearching($isTopSearching): self
    {
        $this->repository->pushCriteria(new ByIsTopSearchingCriteria($isTopSearching));
        return $this;
    }

    public function isNew($isNew): self
    {
        $this->repository->pushCriteria(new ByIsNewCriteria($isNew));
        return $this;
    }

    public function isHot($isHot): self
    {
        $this->repository->pushCriteria(new ByIsHotCriteria($isHot));
        return $this;
    }

    public function isSale($isSale): self
    {
        $this->repository->pushCriteria(new ByIsSaleCriteria($isSale));
        return $this;
    }

    public function isPromotion($value): self
    {
        $this->repository->pushCriteria(new ByIsPromotionCriteria($value));
        return $this;
    }

    public function isShippingRequired($value): self
    {
        $this->repository->pushCriteria(new ByIsShippingRequiredCriteria($value));
        return $this;
    }

    public function isQuick($isQuick): self
    {
        $this->repository->pushCriteria(new ByIsQuickCriteria($isQuick));
        return $this;
    }

    public function equalType($type): self
    {
        $this->repository->pushCriteria(new ByTypeCriteria($type));
        return $this;
    }

    public function dateAvailable($date_available, $operation = '<='): self
    {
        $this->repository->pushCriteria(new ByDateAvailableCriteria($date_available, $operation));
        return $this;
    }

    public function likeName($name): self
    {
        $this->repository->pushCriteria(new LikeNameCriteria($name));
        return $this;
    }

    public function byCategories($cat_ids): self
    {
        $this->repository->pushCriteria(new ByCategoriesCriteria($cat_ids));
        return $this;
    }

    public function byCollection($collection_ids): self
    {
        $this->repository->pushCriteria(new ByCollectionsCriteria($collection_ids));
        return $this;
    }


    public function byFilterGroupsCriteria(array $group_filter_ids): self
    {
        $this->repository->pushCriteria(new ByFilterGroupsCriteria($group_filter_ids));
        return $this;
    }

    public function byRating(int $ratingStar): self
    {
        if ($ratingStar > 0) {
            $this->repository->pushCriteria(new ThisOperationThatCriteria('avg_rating', '>=', $ratingStar));
        }
        return $this;
    }

    public function byBrand(array $brands): self
    {
        if (!empty($brands)) {
            $this->repository->pushCriteria(new ByBrandsCriteria($brands));
        }
        return $this;
    }

    public function betweenPrice(array $prices = []): self
    {
        if (!empty($prices) && count($prices) <= 2) {
            $this->repository->pushCriteria(new ThisBetweenSomethingsCriteria('price', (float)@$prices[0], (float)@$prices[1]));
        }
        return $this;
    }

    public function byTotalSold(int $min): self
    {
        $this->repository->pushCriteria(new ThisOperationThatCriteria('purchased_count', $min, '>='));
        return $this;
    }
}
