<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-03 13:48:24
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-10 10:44:48
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Tasks;

use App\Containers\Category\Enums\CategoryStatus;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Containers\Product\Models\Product;
use App\Ship\Parents\Tasks\Task;

class GetProductByIdForApiTask extends Task
{
    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    public function run(int $productId, $withExtraData): ?Product
    {
        $language_id = $this->currentLang ? $this->currentLang->language_id : 1;

        $withRelationships = [];
        if ($withExtraData) {
            $withRelationships = [
                'desc' => function ($query) use ($language_id) {
                    $query->activeLang($language_id);
                },
                'images',
                'categories' => function ($query) use ($language_id) {
                    $query->where('status', CategoryStatus::ACTIVE);
                },
                'categories.desc' => function ($query) use ($language_id) {
                    $query->activeLang($language_id);
                },
                //'manufacturer',
                /*'specialOffers.desc' => function ($query) use ($language_id) {
                    $query->select('id', 'special_offer_id', 'name');
                    $query->whereHas('language', function ($q) use ($language_id) {
                        $q->where('language_id', $language_id);
                    });
                }*/
            ];
        }

        $data = $this->repository->with($withRelationships)->where('id', $productId);

        return $data->first();
    }
}
