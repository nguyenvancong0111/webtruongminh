<?php

namespace App\Containers\Product\Task;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Tasks\GetProductByIdSlugTask;
use App\Ship\Parents\Actions\Action;
use Illuminate\Database\Eloquent\Builder;
use App\Ship\Parents\Tasks\Task;


class GetAllProductsByCateTask extends Task
{
    protected $getProductByIdSlugTask;

    public function __construct(GetProductByIdSlugTask $getProductByIdSlugTask)
    {
        $this->getProductByIdSlugTask = $getProductByIdSlugTask;
        parent::__construct();
    }

    public function run($filters = [],$orderBy = 'created_at DESC',$limit = 20,$noPageinate = false,$external_data = [], $current_page = 1)
    {
        if (isset($filters['id']) && $filters['id'] != '') {
            $conds[] = ['id', $filters['id']];
        } else {
            if (isset($filters['status']) && $filters['status'] != '') {
                $conds[] = ['status', $filters['status']];
            } else {
                $conds[] = ['status', '=', 2];
            }

            if (isset($filters['type']) && $filters['type'] != '') {
                $conds[] = ['type', $filters['type']];
            } else {
                $conds[] = ['type', '=', 1];
            }

             if (isset($filters['is_home']) && $filters['is_home'] != '') {
                $conds[] = ['is_home', $filters['is_home']];
            } 
        }


        $data = $this->model::with(array_merge(['desc' => function ($query) {
            $query->select('category_id', 'language_id', 'name');
            $query->activeLang($this->defaultLang);
        }], isset($external_data['with_relationship']) ? $external_data['with_relationship'] : []));

        if (isset($filters['name']) && !empty($filters['name'])) {
            $data->whereHas('desc', function (Builder $query) use ($filters) {
                $query->where('name', 'like', '%' . $filters['name'] . '%');
            });
        }
        if (isset($filters['cate_ids']) && is_array($filters['cate_ids'])) {
            $data->whereIn('category.category_id', $filters['cate_ids']);
        }
        if (isset($filters['cate_item']) && $filters['cate_item'] != '') {
            $data->where('category.category_id','!=', $filters['cate_item']);
        }

        $data->where($conds);
        $data->orderByRaw($orderBy);

        return $noPageinate ? $data->limit($limit)->get()->toArray() : $data->paginate($limit);
    }
}
