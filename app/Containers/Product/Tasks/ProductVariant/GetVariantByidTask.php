<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-13 10:15:44
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-17 14:32:38
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Tasks\ProductVariant;

use App\Containers\Product\Data\Repositories\ProductVariantRepository;
use App\Containers\Product\Enums\ProductVariantStatus;
use App\Ship\Parents\Tasks\Task;

class GetVariantByidTask extends Task
{
  protected $repository;

  public function __construct(ProductVariantRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(int $productId = 0, int $variantId = 0, int $quantity = 0)
  {
    $productsVariant = $this->repository->where([
      ['product_id', '=', $productId]
    ]);

    if ($variantId == 0) {

      if ($quantity > 0) {
        $productsVariant->where('stock', '>=', $quantity);
      }

      // $productsVariant->inRandomOrder();
      // $productsVariant->where('is_root', ProductVariantStatus::IS_ROOT);
    } else {
      $productsVariant->where('product_variant_id', '=', $variantId);
    }

    $productsVariant->with([
      // 'product_option_values',
      'product_option_values.option.desc',
      'product_option_values.option_value.desc'
    ]);

    return $productsVariant->first();
  }
}
