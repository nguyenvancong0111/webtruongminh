<?php

namespace App\Containers\Product\Tasks\ProductVariant;

use App\Containers\Product\Data\Repositories\ProductOptionValueRepository;
use App\Containers\Product\Data\Repositories\ProductVariantRepository;
use App\Containers\Product\Models\ProductVariant;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Ship\Parents\Tasks\Task;

class DeleteProductVariantTask extends Task
{
    protected $repository,$productOptionValueRepository;

    public function __construct(ProductVariantRepository $repository, ProductOptionValueRepository $productOptionValueRepository)
    {
        $this->repository = $repository;
        $this->productOptionValueRepository = $productOptionValueRepository;
    }

    public function run(array $whereCond=[]): int
    {
      $productVariant = $this->repository->deleteWhere($whereCond);
      $productOptionValue = $this->productOptionValueRepository->deleteWhere($whereCond);

      return $productVariant;
    }

} // End class
