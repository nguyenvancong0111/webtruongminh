<?php

namespace App\Containers\Product\Tasks\ProductVariant;

use App\Containers\Product\Data\Repositories\ProductOptionValueRepository;
use App\Containers\Product\Data\Repositories\ProductVariantRepository;
use App\Containers\Product\Models\ProductVariant;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Collection;

class StoreProductVariantOptionValueTask extends Task
{
    protected $repository;
    protected $productOptionValueRepository;

    public function __construct(ProductVariantRepository $repository, ProductOptionValueRepository $productOptionValueRepository)
    {
      $this->repository = $repository;
      $this->productOptionValueRepository = $productOptionValueRepository;
    }

    public function run(ProductVariant $productVariant, array $inputChoice=[], Collection $productOptions, $imageVarials=[], $old_product_option_values=[])
    {
      $productOptionsKeyByOptId = $productOptions->keyBy('option_id')->toArray();
      if(!empty($old_product_option_values)){
        foreach ($old_product_option_values as $key => $value) {
          # code...
          $check = 0;
          foreach ($inputChoice['optionIds'] as $k => $v) {
              if($value['option_id']== $v &&  $value['option_value_id'] == $inputChoice['optionValueIds'][$k]){
                $check = 1 ;
                break;
              }
          }
          if($check==0){
              $count=$this->productOptionValueRepository->where('product_option_value_id',$value['product_option_value_id'])->count();
              if($count>0)
              $this->productOptionValueRepository->find($value['product_option_value_id'])->delete();
          }
        }
      }
      foreach ($inputChoice['optionValueIds'] as $index => $optionValueId) {
        $optionId = $inputChoice['optionIds'][$index];
        $checkIsset = $this->productOptionValueRepository->where([
              ['product_option_id', $productOptionsKeyByOptId[$optionId]['product_option_id']],
              ['product_id',(int) $productVariant->product_id],
              ['option_id',$optionId],
              ['option_value_id',$optionValueId],
              ['product_variant_id',$productVariant->product_variant_id],
          ])->first();
          // dd($checkIsset,$productOptionsKeyByOptId[$optionId]['product_option_id'],$productVariant->product_id,$optionId,$optionValueId,$productVariant->product_variant_id);
          if(empty($checkIsset)){
            $productOptionValueArray[] = [
              'product_option_id' => $productOptionsKeyByOptId[$optionId]['product_option_id'],
              'product_id' =>(int) $productVariant->product_id,
              'option_id' => $optionId,
              'option_value_id' => $optionValueId,
              'product_variant_id' => $productVariant->product_variant_id,
              'images' => json_encode(isset($imageVarials[$index]) ? $imageVarials[$index] : []),
            ];
          }else{
            if(isset($imageVarials[$index])){
              $dataUpdate['images'] = json_encode($imageVarials[$index]);
              $productOptionVariant = $this->productOptionValueRepository->update($dataUpdate, $checkIsset->product_option_value_id);
            }
          }
      }
      if(isset($productOptionValueArray)){
        return $this->productOptionValueRepository->insert($productOptionValueArray);
      }
      else
        return true;
    }

    // public function with(array $with=[]) {
    //   $this->repository->pushCriteria(new WithCriteria($with));
    // }
} // End class
