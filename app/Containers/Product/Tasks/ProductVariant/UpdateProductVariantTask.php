<?php

namespace App\Containers\Product\Tasks\ProductVariant;

use App\Containers\Product\Data\Repositories\ProductVariantRepository;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use DB;
class UpdateProductVariantTask extends Task
{

    protected $repository;

    public function __construct(ProductVariantRepository $repository, ProductRepository $repositoryPrd)
    {
        $this->repository = $repository;
        $this->repositoryPrd = $repositoryPrd;
    }

    public function run( $warehouse, int $quantity,$product_variants_id,int $statusOld, int $product_id)
    {
        try {
            $countAdd=0;
            if($warehouse->type == 'In'){
                // Chuyển trạng thái từ nhập hoàn thành sang hủy nhập
                if($statusOld==2 && $warehouse->status !=2){
                    $countAdd = -$quantity;
                }elseif($statusOld==-1 && $warehouse->status == 2){
                    $countAdd = $quantity;
                }elseif($statusOld==1 && $warehouse->status == 2){
                  $countAdd = $quantity;
                }elseif($statusOld==0 && $warehouse->status == 2){
                    $countAdd = $quantity;
                }
            }else{
                if($statusOld==2 && $warehouse->status !=2){
                    $countAdd = $quantity;
                }elseif($statusOld==-1 && $warehouse->status == 2){
                    $countAdd = -$quantity;
                }elseif($statusOld==1 && $warehouse->status == 2){
                  $countAdd = -$quantity;
                }elseif($statusOld==0 && $warehouse->status == 2){
                    $countAdd = -$quantity;
                }
            }
            if(!empty($product_variants_id)){
                $result = $this->repository->where('product_variant_id', $product_variants_id)
                ->update([
                'stock'=> DB::raw('stock+'.$countAdd)
                ]);
            }
            // else{
            //     $result =  $this->repositoryPrd->where('id', $product_id)
            //     ->update([
            //     'stock'=> DB::raw('stock+'.$countAdd)
            //     ]);
            // }

            return $result;
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
