<?php

namespace App\Containers\Product\Tasks\ProductVariant;

use App\Containers\Product\Data\Repositories\ProductVariantRepository;
use App\Containers\Product\Models\ProductVariant;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Arr;

class StoreProductVariantTask extends Task
{
    protected $repository;

    public function __construct(ProductVariantRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $productVariantData=[]): ProductVariant
    {
      if(isset($productVariantData['product_variant_id']) && !empty($productVariantData['product_variant_id'])){
        $dataUpdate = Arr::except($productVariantData, ['product_variant_id']);
        $productVariant = $this->repository->update($dataUpdate, $productVariantData['product_variant_id']);
        return $productVariant;
      }
      else
        $productVariant = $this->repository->create($productVariantData);
      return $productVariant;
    }

    // public function with(array $with=[]) {
    //   $this->repository->pushCriteria(new WithCriteria($with));
    // }
} // End class
