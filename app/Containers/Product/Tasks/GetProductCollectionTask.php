<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-03 13:48:24
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-01 11:36:58
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Tasks;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Containers\Product\Models\Product;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetProductCollectionTask extends Task
{

    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(string $select = '*',$collection_id, Language $currentLang, $orderBy = ['created_at' => 'desc','sort_order' => 'asc'], $where, $limit = false, $offset = false)
    {
        $language_id = $currentLang ? $currentLang->language_id : 1;

        if (!empty($orderBy)) {
            foreach ($orderBy as $columm => $item) {
                if (!empty($item) && $item != 'null') {
                    $this->repository->orderBy($columm, $item);
                }
            }
        } else {
            foreach ($orderBy as $columm => $item) {
                $this->repository->orderBy($columm, $item);
            }
        }

        if (!empty($where)) {
            foreach ($where as $columm => $item) {
                if (!empty($item) && $item != 'null' && $columm != 'name') {
                    $this->repository->where($columm, $item);
                }
            }
        }

        $this->repository->whereHas('collections', function ($query) use ($collection_id) {
            if ($collection_id > 0) {
                $query->where('collection_id', $collection_id);
            }

            if(is_array($collection_id)){
                $query->whereIn('collection_id', $collection_id);
            }
               $query->where('status', 1);
        });

        $this->repository->whereHas('desc', function ($query) use ($where) {
            if (!empty($where['name'])) {
                $query->where('name', 'like', '%' . $where['name'] . '%')
                    ->orWhere('short_description', 'like', '%' . $where['name'] . '%');
             }
        });

        $data = $this->repository->select($select)->where('status',2)->with([
            'desc' => function ($query) use ($language_id) {
                $query->activeLang($language_id);
            },
            'images' => function($query){
                $query->orderBy('sort','desc')->limit(1);
             }
        ]);

        if (!empty($offset)) {
            return $data->skip($offset)->take($limit)->get();
        }

        return !empty($limit) ? $data->paginate($limit) : $data->get();
    }
}
