<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-23 21:17:58
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-24 16:43:19
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Tasks\Product;

use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteProductTask extends Task
{
    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $id)
    {
        try {
            return $this->repository->update(['status' => -1],$id);
        } catch (Exception $e) {
            // throw $e;
            throw $e;
        }
    }
}
