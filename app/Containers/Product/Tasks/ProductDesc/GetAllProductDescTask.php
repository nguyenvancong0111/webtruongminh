<?php

namespace App\Containers\Product\Tasks\ProductDesc;

use App\Containers\Product\Data\Repositories\ProductDescRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;

/**
 * Class GetAllProductDescTask.
 */
class GetAllProductDescTask extends Task
{

    protected $repository;

    public function __construct(ProductDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($product_id, $defaultLanguage = null)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('product_id', $product_id));
        $wery = $this->repository->all()->keyBy('language_id')->toArray();

        return $wery;
    }
}
