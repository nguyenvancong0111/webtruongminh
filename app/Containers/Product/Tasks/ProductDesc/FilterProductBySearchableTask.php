<?php

namespace App\Containers\Product\Tasks\ProductDesc;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Product\Data\Repositories\ProductDescRepository;

class FilterProductBySearchableTask extends Task
{

  protected $repository;

  public function __construct(ProductDescRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run()
  {
    return $this->repository->scopeQuery(function ($query) {
      return $query->orderBy('id', 'DESC')
                   ->take(20);
    })->all();
  }
}
