<?php

namespace App\Containers\Product\Tasks\ProductFilter;

use App\Containers\Product\Models\Product;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SaveProductFilterTask.
 */
class SaveProductFilterTask extends Task
{
    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($product_filter, Product $current_product_model)
    {
        if (!empty($product_filter)) {
            if (is_array($product_filter)) {
                $current_product_model->filters()->sync($product_filter);
            } else {
                $current_product_model->filters()->attach($product_filter);
            }
        } else {
            $current_product_model->filters()->sync([]);
        }
    }
}
