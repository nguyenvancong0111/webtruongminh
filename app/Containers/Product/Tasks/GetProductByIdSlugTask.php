<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-03 13:48:24
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-01 11:36:58
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Tasks;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Containers\Product\Models\Product;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetProductByIdSlugTask extends Task

{
    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $productId, string $slug, Language $currentLang): ?Product
    {
        $language_id = $currentLang ? $currentLang->language_id : 1;


        $data = $this->repository->with([
            'desc' => function ($query) use ($language_id) {
                $query->activeLang($language_id);
            },
            'images' => function ($query) {
                $query->orderBy('sort','desc');
            },
            'comments',
            'collections' => function ($query) {
                $query->where('status', 1);
            },
            'categories' => function ($query) {
                $query->where('status', 2);
            }
            /*'specialOffers.desc' => function ($query) use ($language_id) {
                $query->select('id', 'special_offer_id', 'name');
                $query->whereHas('language', function ($q) use ($language_id) {
                    $q->where('language_id', $language_id);
                });
            }*/
        ])->where('id', $productId);

        $data->whereHas('desc', function (Builder $query) use ($slug) {
            $query->where('slug', '=', $slug);
        });

        return $data->first();
    }
}
