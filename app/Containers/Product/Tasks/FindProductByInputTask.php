<?php

namespace App\Containers\Product\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Product\Data\Repositories\ProductDescRepository;
use Exception;

class FindProductByInputTask extends Task
{
    protected $repository;

    public function __construct(ProductDescRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(string $input, int $limit)
    {
        try {
            return $this->repository->with(['product' => function ($query) {
                $query->where('status', 2);
                $query->select('id', 'price', 'type', 'status', 'image')
                    ->with(['categories' => function ($q) {
                        $q->with('desc');
                    }]);
            }])->where('language_id', 1)->where('name', 'like', '%' . $input . '%')->limit($limit)->get();
        } catch (Exception $th) {
            throw $th;
        }
    }
}
