<?php

namespace App\Containers\Product\Tasks;

use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Ship\Criterias\Eloquent\OrderByFieldCriteria;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Ship\Parents\Tasks\Task;

class GetTopProductDashboardTask extends Task
{

    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
      // return $this->repository->limit(10)->get();
    }

    // public function with(array $with=[]) {
    //   $this->repository->pushCriteria(new WithCriteria($with));
    // }

    public function selectFields(array $column=['*']) {
      $this->repository->pushCriteria(new SelectFieldsCriteria($column));
    }

    public function orderByField(array $orderBy=[]) {
      $orderByKey = key($orderBy);
      $orderByValue = $orderBy[$orderByKey];
      $this->repository->pushCriteria(new OrderByFieldCriteria($orderByKey, $orderByValue));
    }
}
