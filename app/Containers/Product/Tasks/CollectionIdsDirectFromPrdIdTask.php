<?php

namespace App\Containers\Product\Tasks;

use App\Containers\Collection\Data\Repositories\CollectionDetailRepository;
use App\Containers\Product\Data\Repositories\ProductCategoryRepository;
use App\Containers\Product\Data\Repositories\ProductCollectionRepository;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisInThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class CateIdsDirectFromPrdIdTask.
 */
class CollectionIdsDirectFromPrdIdTask extends Task
{

    protected $repository;

    public function __construct(CollectionDetailRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return  mixed
     */
    public function run($product_id)
    {
        if(is_array($product_id)) {
            $this->repository->pushCriteria(new ThisInThatCriteria('object_id',$product_id));
        }else {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('object_id',$product_id));
        }

        $this->repository->pushCriteria(new SelectFieldsCriteria(['collection_id']));

        $result = $this->repository->all();

        if($result && !$result->isEmpty()) {
            return $result->pluck('collection_id')->toArray();
        }

        return [];
    }
}
