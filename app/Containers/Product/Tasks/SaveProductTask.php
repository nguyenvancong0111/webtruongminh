<?php

namespace App\Containers\Product\Tasks;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Containers\Product\Data\Repositories\ProductVariantRepository;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;
use App\Containers\Product\Enums\ProductSync;

/**
 * Class SaveProductTask.
 */
class SaveProductTask extends Task
{

    protected $repository;

    public function __construct(ProductRepository $repository, ProductVariantRepository $repositoryVariantRepository)
    {
        $this->repository = $repository;
        $this->repositoryVariantRepository = $repositoryVariantRepository;
    }

    /**
     * @return  mixed
     */
    public function run(array $data, $product_id)
    {

        try {
            $dataUpdate = Arr::except($data, ['product_description', 'img_upload_for_add', 'product_category', '_token', '_headers']);
            $dataUpdate['status'] = intval(str_replace([',', '.'], '', empty($dataUpdate['status']) && $dataUpdate['status']==null? 1 : $dataUpdate['status'] ));
            $dataUpdate['data_status'] = intval(str_replace([',', '.'], '', empty($dataUpdate['data_status']) && $dataUpdate['data_status']==null? 1 :$dataUpdate['data_status'] ));

            if(isset($product_id) && !empty($product_id) && $product_id!= 'NaN'){
                $product = $this->repository->update($dataUpdate, $product_id);
            }else{
                $product = $this->repository->create($dataUpdate);
            }
            return $product;

        }catch(Exception $e) {
            throw $e;
        }
    }
}
