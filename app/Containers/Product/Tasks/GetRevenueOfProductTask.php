<?php

namespace App\Containers\Product\Tasks;

use App\Containers\Order\Models\OrderItem;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Facades\DB;

class GetRevenueOfProductTask extends Task
{

    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $productId)
    {
      $result = OrderItem::where('product_id', $productId)
                         ->selectRaw("SUM(price*quantity) as total_revenue, DATE(orders.created_at) as date")
                         ->join('orders', 'orders.id', '=', 'order_items.order_id')
                         ->groupByRaw('date')
                         ->get()
                         ->keyBy('date');
      return $result;
    }
}
