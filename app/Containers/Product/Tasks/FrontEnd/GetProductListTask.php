<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 01:33:06
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-05 15:52:43
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Tasks\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\News\Data\Repositories\NewsRepository;
use App\Containers\News\Enums\ServiceStatus;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetProductListTask extends Task
{

    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run(
        array $filters = [],
        array $orderBy = ['created_at' => 'desc', 'id' => 'desc'],
        int $limit = 20,
        bool $skipPagination = false,
        Language $currentLang = null,
        array $externalData = [],
        int $currentPage = 1
    ): iterable {
        $language_id = $currentLang ? $currentLang->language_id : 1;
        if (isset($filters['status']) && $filters['status'] != '') {
            $conds[] = ['status', $filters['status']];
        } else {
            $conds[] = ['status', '>', 0];
        }
        $this->repository->with(array_merge(['categories','categories.desc',
            'desc' => function ($query) use ($language_id) {
                $query->select('id', 'product_id', 'name', 'slug','short_description'/* 'meta_title'*/);
                $query->activeLang($language_id);
            },
            /*'specialOffers.desc' => function ($query) use ($language_id) {
                $query->select('id', 'special_offer_id', 'name');
                $query->activeLang($language_id);
            }*/
        ], isset($external_data['with_relationship']) ? $externalData['with_relationship'] : []))->where($conds);

        $this->repository->whereHas('desc', function ($q) {
            $q->where('slug', '!=', '')->whereNotNull('slug');
        });
        if (isset($filters['cate_ids']) && is_array($filters['cate_ids']) && !empty($filters['cate_ids'][0]) ) {
            $this->repository->whereHas('categories', function (Builder $q) use ($filters) {
                $q->whereIn('category.category_id', $filters['cate_ids']);
            });
        }

        if (isset($filters['name'])) {
            $this->repository->whereHas('desc', function (Builder $query) use ($filters, $language_id) {
                $query->activeLang($language_id);
                $query->where('name', 'like', '%'.$filters['name'].'%');
            });
        }

        if(isset($filters['reject_id'])){
            if(is_array($filters['reject_id'])){
                $this->repository->whereNotIn('id', $filters['reject_id']);
            }else{
                $this->repository->where('id', '!=' ,$filters['reject_id']);
            }
        }

        foreach ($orderBy as $column => $direction) {
            $this->repository->orderBy($column, $direction);
        }

        // \DB::enableQueryLog();

        return $skipPagination ? $this->repository->limit($limit) : $this->repository->paginate($limit);
        // dd(\DB::getQueryLog());
    }
}
