<?php

namespace App\Containers\Product\Actions\ProductDesc;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class FilterProductBySearchableAction extends Action
{
  public function run(DataTransporter $transporter, array $with = [], array $column = ['*'])
  {
    $products = Apiato::call('Product@ProductDesc\FilterProductBySearchableTask',  [], [
      ['with' => [$with]],
      ['selectFields' => [$column]]
    ]);

    return $products;
  }
}
