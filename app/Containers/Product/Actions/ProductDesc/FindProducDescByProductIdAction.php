<?php

namespace App\Containers\Product\Actions\ProductDesc;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class FindProducDescByProductIdAction extends Action
{
  public function run(array $where=[])
  {
    $productDesc = Apiato::call('Product@ProductDesc\FindProducDescByProductIdTask',  [$where]);
    return $productDesc;
  }
}
