<?php

namespace App\Containers\Product\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class GetAllProductsAction.
 *
 */
class FindProductByIdCateAction extends Action
{

    /**
     * @return mixed
     */
    public function run(int $id_category ,int $limit, $offset='', $inputName='')
    {
        // return $products;
        $data = Apiato::call('Product@ProductCategory\FindProductByIdCateTask',[$id_category,Apiato::call('Localization@GetDefaultLanguageTask'),$limit,$offset,$inputName]);

        return $data;
    }
}
