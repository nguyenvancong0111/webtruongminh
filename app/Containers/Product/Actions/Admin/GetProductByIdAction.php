<?php

namespace App\Containers\Product\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class GetProductByIdAction.
 *
 */
class GetProductByIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run(int $product_id, array $with=[])
    {
        $data = Apiato::call('Product@Admin\GetProductByIdTask', [
            $product_id,
            Apiato::call('Localization@GetDefaultLanguageTask'),
            ['with_relationship' => $with]
        ]);

        $data->setRelation('all_desc', $data->all_desc->keyBy('language_id'));

        return $data;
    }
}
