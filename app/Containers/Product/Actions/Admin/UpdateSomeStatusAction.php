<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 23:29:32
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-21 17:43:43
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\Admin;

use App\Ship\Parents\Actions\Action;


class UpdateSomeStatusAction extends Action
{
    public function run(string $field,int $id, int $status) :? bool
    {
        $result = $this->call('Product@Admin\UpdateSomeStatusTask',[
            $field,
            $id,
            $status
        ]);

        $this->clearCache();

        return $result;
    }
}
