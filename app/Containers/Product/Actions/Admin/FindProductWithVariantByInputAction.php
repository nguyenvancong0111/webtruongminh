<?php

namespace App\Containers\Product\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class GetAllProductsAction.
 *
 */
class FindProductWithVariantByInputAction extends Action
{

    /**
     * @return mixed
     */
    public function run(string $input ,int $limit)
    {
        $products = $this->call('Product@FindProductWithVariantByInputTask', [$input,$limit]);

        return $products;
    }
}
