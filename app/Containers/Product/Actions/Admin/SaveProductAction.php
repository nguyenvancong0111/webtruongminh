<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-04-15 16:08:17
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-21 17:44:10
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\Admin;

use App\Containers\Product\Models\Product;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class SaveProductAction.
 *
 */
class SaveProductAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
      $product = $this->call('Product@SaveProductTask', [$data['product_data'],@$data['product_id']]);
      if ($product) {

            $original_desc = $this->call('Product@ProductDesc\GetAllProductDescTask', [$product->id]);

            $this->call('Product@ProductDesc\SaveProductDescTask', [$data, $original_desc, $product->id]);

            if (isset($data['product_category'])) {
              $this->call('Product@ProductCategory\SaveProductCategoryTask', [$data['product_category'],$product]);
            }

          if (isset($data['product_collection'])) {
              $this->call('Product@ProductCollection\SaveProductCollectionTask', [$data['product_collection'],$product]);
          }

            if(!empty($data['fileList']) && $data['product_id'] == 'NaN'){
                $img=[];
                foreach ($data['fileList'] as $key => $value) {
                    # code...
                    $img[] =  $value['id'];
                }
                $this->call('Product@Admin\UpdateProductImageTask', [$img,$product->id]);
            }

          $this->clearCache();

          return $product;
      }
    }
}
