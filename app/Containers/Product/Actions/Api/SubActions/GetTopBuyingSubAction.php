<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-25 23:31:45
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-17 10:06:04
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\Api\SubActions;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Enums\ProductStatus;
use App\Containers\Product\Tasks\GetAllProductsTask;
use App\Ship\Parents\Actions\SubAction;

class GetTopBuyingSubAction extends SubAction
{
    protected $criterias = [];
    protected $exceptProductIds = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function run(?array $categoryIds, int $limit = 3, Language $currentLang): ?iterable
    {
        if (!empty($categoryIds)) {
            $this->criterias = array_merge($this->criterias, [
                ['byCategories' => [$categoryIds]],
                ['inRandomOrder' => []],
                ['isSale' => [ProductStatus::IS_SALE]]
            ]);

            $suggestProduct = app(GetAllProductsTask::class)
                ->byCategories($categoryIds)
                ->activeStatus(ProductStatus::ACTIVE)
                ->inRandomOrder()
                ->byTotalSold(50);

            if(!empty($this->exceptProductIds)) {
                $suggestProduct->exceptProductIds($this->exceptProductIds);
            }

                // ->isSale(ProductStatus::IS_SALE)
            $suggestProduct = $suggestProduct->run([], $currentLang, true, $limit);

            return (!empty($suggestProduct) && !$suggestProduct->IsEmpty()) ? $suggestProduct : collect([]);
        }
        return null;
    }

    public function exceptProductIds(array $ids): self
    {
        $this->exceptProductIds = $ids;
        return $this;
    }
}
