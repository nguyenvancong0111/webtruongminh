<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-04 14:34:00
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\ProductOption;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Localization\Models\Language;
use App\Containers\Product\Tasks\ProductOption\GetProductOptionsTask;
use App\Ship\Parents\Actions\Action;

class GetProductOptionsAction extends Action
{
    public $getProductOptionsTask;

    public function __construct(GetProductOptionsTask $getProductOptionsTask)
    {
        $this->getProductOptionsTask = $getProductOptionsTask;
        parent::__construct();
    }

    public function run($productId, Language $currentLang = null)
    {
        return $this->remember(function () use ($productId, $currentLang) {
            return $this->getProductOptionsTask->run(
                $productId,
                !empty($currentLang) ? $currentLang : Apiato::call('Localization@GetDefaultLanguageTask')
            );
        }, null, [], 0, true);
    }
}
