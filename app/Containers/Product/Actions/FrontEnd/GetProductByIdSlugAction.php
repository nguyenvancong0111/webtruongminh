<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-03 13:46:27
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-01 11:35:53
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Tasks\GetProductByIdSlugTask;
use App\Ship\Parents\Actions\Action;

class GetProductByIdSlugAction extends Action
{
    protected $getProductByIdSlugTask;

    public function __construct(GetProductByIdSlugTask $getProductByIdSlugTask)
    {
        $this->getProductByIdSlugTask = $getProductByIdSlugTask;
        parent::__construct();
    }

    public function run(int $productId, string $slug, Language $currentLang = null)
    {
        return $this->remember(function() use($productId,$slug,$currentLang){
            $data = $this->getProductByIdSlugTask->run(
                $productId,
                $slug,
                $currentLang
            );

            return $data;
        },null,[],0,$this->skipCache);
    }
}
