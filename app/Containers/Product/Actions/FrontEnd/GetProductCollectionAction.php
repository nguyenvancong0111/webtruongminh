<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-03 13:46:27
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-01 11:35:53
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Tasks\GetProductCollectionTask;
use App\Ship\Parents\Actions\Action;

class GetProductCollectionAction extends Action
{
    protected $getProductCollectionTask;

    public function __construct(GetProductCollectionTask $getProductCollectionTask)
    {
        $this->getProductCollectionTask = $getProductCollectionTask;
        parent::__construct();
    }

    public function run(string $select = "*",$collection_id, Language $currentLang = null,array $orderBy = ['created_at' => 'desc','sort_order' => 'asc'], array $where = [], $limit = false, $offset = false)
    {
        return  $this->getProductCollectionTask->run(
                $select,
                $collection_id,
                $currentLang,
                $orderBy,
                $where,
                $limit,
                $offset
            );
    }
}
