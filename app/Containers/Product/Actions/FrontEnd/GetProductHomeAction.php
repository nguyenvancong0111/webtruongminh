<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-08 21:54:52
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-12 19:49:44
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\FrontEnd;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Localization\Models\Language;
use App\Containers\Product\Enums\ProductStatus;
use App\Ship\Parents\Actions\Action;

class GetProductHomeAction extends Action
{
    public function run(array $filters, array $orderBy = ['created_at' => 'DESC', 'id' => 'DESC'], int $limit = 10, bool $skipPagination = false, Language $currentLang = null, array $externalData = [], int $currentPage = 1): iterable {

        return $this->remember(function () use ($filters, $orderBy, $limit, $skipPagination, $currentLang, $externalData, $currentPage) {
            $criterias = [];
            if (isset($filters['id']) && $filters['id'] != '') {
                $criterias[] = ['equalId' => [$filters['id']]];
            }

            $criterias[] = ['equalStatus' => [ProductStatus::ACTIVE]];

            $this->detectCriterias($filters, $orderBy, $criterias);

            // dd($filters, $criterias);

            $products = Apiato::call('Product@GetAllProductsTask',
                [
                    $externalData,
                    $currentLang,
                    $skipPagination,
                    $limit
                ],
                array_merge([
                    'addRequestCriteria'
                ], $criterias)
            );

            return $products;
        }, null, [5], 0, $this->skipCache);
    }

    public function detectCriterias(array $filters, array $orderBy, array &$criterias): void
    {
        (isset($filters['equalIds']) && $filters["equalIds"]) ? $criterias[] = ['equalIds' => [$filters["equalIds"]]] : false;

        (isset($filters['is_home']) && $filters['is_home']) ? $criterias[] = ['isHome' => [ProductStatus::IS_HOME]] : false;

        (isset($filters['is_top_searching']) && $filters['is_top_searching']) ? $criterias[] = ['isTopSearching' => [ProductStatus::IS_TOP_SEARCHING]] : false;

        (isset($filters['isQuick']) && $filters['isQuick']) ? $criterias[] = ['isQuick' => [ProductStatus::IS_QUICK]] : false;

        isset($filters['isShippingRequired']) ? $criterias[] = ['isShippingRequired' => [$filters['isShippingRequired']]] : false;

        isset($filters['isPromotion']) ? $criterias[] = ['isPromotion' => [$filters['isPromotion']]] : false;

        (isset($filters['is_new'])) ? $criterias[] = ($filters['is_new'] == 'no_get' ? ['isNew' => [ProductStatus::ZERO]] : ['isNew' => [ProductStatus::IS_NEW]]) : false;

        (isset($filters['isHot'])) ? $criterias[] = ($filters['isHot'] == 'no_get' ? ['isHot' => [ProductStatus::ZERO]] : ['isHot' => [ProductStatus::IS_HOT]]) : false;

        (isset($filters['isSale'])) ? $criterias[] = ($filters['isSale'] == 'no_get' ? ['isSale' => [ProductStatus::ZERO]] : ['isSale' => [ProductStatus::IS_SALE]]) : false;

        (isset($filters['type']) && $filters['type'] > -1) ? $criterias[] = ['equalType' => [$filters['type']]] : false;

        (isset($filters['likeName']) && !empty($filters['likeName'])) ? $criterias[] = ['likeName' => [$filters['likeName']]] : false;
        // $conds[] = ['date_available', '<=', Carbon::now()];

        // $data = $data->with(array_merge(['desc' => function ($query) {
        //     $query->select('id', 'product_id', 'name', 'short_description', 'meta_title');
        //     $query->activeLang($this->defaultLang);
        // }], isset($external_data['with_relationship']) ? $external_data['with_relationship'] : []));

        // $data->where($conds);

        isset($filters['cate_ids']) && is_array($filters['cate_ids']) && !empty($filters['cate_ids']) ? $criterias[] = ['byCategories' => [$filters['cate_ids']]] : false;

        isset($filters['groupFilters']) && is_array($filters['groupFilters']) ? $criterias[] = ['byFilterGroupsCriteria' => [$filters['groupFilters']]] : false;

        $criterias[] = isset($filters['inRandomOrder']) ? ['inRandomOrder' => []] :  ['orderBy' => [$orderBy]];

        isset($filters['byRating']) && !empty($filters['byRating']) ? $criterias[] = ['byRating' => [$filters['byRating']]] : false;

        isset($filters['byBrand']) && !empty($filters['byBrand']) ? $criterias[] = ['byBrand' => [$filters['byBrand']]] : false;

        isset($filters['betweenPrice']) && !empty($filters['betweenPrice']) ? $criterias[] = ['betweenPrice' => [$filters['betweenPrice']]] : false;
    }
}
