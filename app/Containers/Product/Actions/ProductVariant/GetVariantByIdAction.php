<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-14 12:32:05
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-17 14:28:57
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\ProductVariant;

use App\Containers\Product\Actions\ProductVariant\SubActions\GetVariantByIdSubAction;
use App\Ship\Parents\Actions\Action;

class GetVariantByIdAction extends Action
{
    public function run(int $productId = 0, int $variantId, int $quantity)
    {
        return app(GetVariantByIdSubAction::class)->run($productId, $variantId, $quantity);
    }
}
