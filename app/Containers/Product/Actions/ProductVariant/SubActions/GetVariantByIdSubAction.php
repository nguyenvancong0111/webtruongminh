<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-14 18:27:45
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-17 14:29:21
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\ProductVariant\SubActions;

use App\Containers\Product\Tasks\ProductVariant\GetVariantByidTask;
use App\Ship\Parents\Actions\SubAction;

class GetVariantByIdSubAction extends SubAction
{
    public function run(int $productId = 0, int $variantId, int $quantity = 0)
    {
        return app(GetVariantByidTask::class)->run($productId, $variantId, $quantity);
    }
}
