<?php

namespace App\Containers\Product\Actions\ProductVariant;

use App\Ship\Parents\Actions\Action;
use App\Containers\Product\Actions\ProductVariant\SubActions\FindVariantByProductIdSubAction;

class FindVariantByProductIdAction extends Action
{
    public function run(int $productId=0, array $with=[], int $isset_root = 0)
    {
      return app(FindVariantByProductIdSubAction::class)->run($productId,$with,$isset_root);
    }
}
