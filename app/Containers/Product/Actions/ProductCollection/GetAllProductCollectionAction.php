<?php

namespace App\Containers\Product\Actions\ProductCollection;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class GetProductByIdAction.
 *
 */
class GetAllProductCollectionAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $transporter, array $with=[])
    {
      $collection = Apiato::call('Product@ProductCollection\GetAllProductCollectionTask', [], [
        ['filterByName' => [$transporter]],
        ['with' => [$with]]
      ]);
      return $collection;
    }
}

