<?php

/**
 * Created by PhpStorm.
 * Filename: ProductOptionValue.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 9/20/20
 * Time: 15:19
 */

namespace App\Containers\Product\Models;

use App\Containers\Option\Models\Option;
use App\Containers\Option\Models\OptionValue;
use App\Ship\core\Traits\HelpersTraits\DateTrait;
use App\Ship\Parents\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductOptionValue extends Model
{
    use DateTrait, SoftDeletes;

    protected $table = 'product_option_value';
    protected $primaryKey = 'product_option_value_id';
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function option()
    {
        return $this->belongsTo(Option::class, 'option_id', 'id');
    }

    public function option_value()
    {
        return $this->belongsTo(OptionValue::class, 'option_value_id', 'id');
    }
} // End class
