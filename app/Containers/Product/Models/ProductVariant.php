<?php

namespace App\Containers\Product\Models;

use App\Containers\Product\Enums\ProductVariantStatus;
use App\Containers\Product\Traits\DateTrait;
use App\Containers\Product\Traits\MoneyTrait;
use App\Ship\Parents\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductVariant extends Model
{
    use MoneyTrait,
        DateTrait,
        SoftDeletes;

    protected $table = 'product_variants';
    protected $appends = ['global_price_text', 'status_text', 'status_type'];
    protected $primaryKey = 'product_variant_id';
    protected $guarded = [];

    protected $attributes = [];

    protected $hidden = [];

    protected $casts = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $saleStatusText = [
        ProductVariantStatus::ON_SALE => 'Đang bán',
        ProductVariantStatus::NOT_ON_SALE => 'Ngừng bán',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'productvariants';

    public function product_option_values()
    {
        return $this->hasMany(ProductOptionValue::class, 'product_variant_id', 'product_variant_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function getPriceFinalAttribute(): float
    {
        $finalPrice = 0;
        $productOptionValues = $this->product_option_values;
        if ($productOptionValues->isNotEmpty()) {
            foreach ($productOptionValues as $prodOptValue) {
                switch ($prodOptValue->price_prefix) {
                    case '+':
                        $finalPrice += $prodOptValue->price;
                        break;

                    case '-':
                        $finalPrice -= $prodOptValue->price;
                        break;

                    default:
                        // code
                        break;
                }
            }
        }

        return $this->price + $finalPrice;
    }

    public function getStatusTextAttribute(): string
    {
        return $this->saleStatusText[$this->status];
    }

    public function getStatusTypeAttribute(): string
    {
        $statusText = $this->status == ProductVariantStatus::ON_SALE ? 'success'
            : 'warning';
        return $statusText;
    }

} // End class
