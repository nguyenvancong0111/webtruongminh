<?php

/**
 * Created by PhpStorm.
 * Filename: ProductDesc.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 7/23/20
 * Time: 10:38
 */

namespace App\Containers\Product\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Localization\Models\Language;

class ProductDesc extends Model
{
    protected $table = 'product_description';
    public $timestamps = false;

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function language()
    {
        return $this->hasOne(Language::class, 'language_id', 'language_id');
    }

    public function product_variants()
    {
        return $this->hasMany(ProductVariant::class, 'product_id', 'product_id');
    }
}
