<?php
 /**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-01-31 18:03:27
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-04-15 18:19:22
 * @ Description: Happy Coding!
 */


namespace App\Containers\Product\Models;

use App\Ship\Parents\Models\Model;

class ProductAttribute extends Model
{
    protected $table = 'product_attribute';
    protected $primaryKey = ['product_id','attribute_id','language_id'];
}