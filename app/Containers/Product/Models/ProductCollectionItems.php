<?php
/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2020-10-06 21:44:19
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-04-15 18:19:05
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Models;

use App\Ship\Parents\Models\Model;

class ProductCollectionItems extends Model
{
    protected $table = 'product_collection_items';
    
    public function product()
    {
        return $this->hasOne(Product::class,'id','product_id')->where('status',2);
    }
}