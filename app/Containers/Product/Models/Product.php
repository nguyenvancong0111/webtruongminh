<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-01-31 18:03:27
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-17 17:00:23
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Models;

use App\Containers\Collection\Models\Collection;
use App\Containers\Collection\Models\CollectionDetail;
use App\Containers\Comment\Models\Comment;
use App\Ship\Parents\Models\Model;
use App\Containers\Filter\Models\Filter;
use App\Containers\Option\Models\Option;
use App\Containers\Order\Traits\PriceTrait;
use App\Containers\Category\Models\Category;
use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\Localization\Models\Language;
use App\Containers\Manufacturer\Models\Manufacturer;
use Apiato\Core\Foundation\ImageURL;
use Apiato\Core\Foundation\UrlLib;
use App\Containers\Product\Enums\ProductStatus;
use App\Containers\PromotionCampaign\Models\PromotionCampaignProduct;
use App\Containers\SpecialOffer\Enums\SpecialOfferStatus;
use App\Containers\SpecialOffer\Models\SpecialOffer;
use App\Containers\SpecialOffer\Models\SpecialOfferProduct;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Containers\Customer\Models\CustomerWishList;
use App\Containers\Tags\Enums\TagCate;
use App\Containers\Tags\Enums\TagStatus;
use App\Containers\Tags\Enums\TagType;
use App\Containers\Tags\Models\TagDetails;
use App\Containers\Tags\Models\Tags;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Product extends Model
{
    // use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;
    public $unit = 'VNĐ';

    use PriceTrait;
    use SoftDeletes;

    protected $table = 'products';

    protected $appends = [
        'image_url',
        'image_site',
        'link'
    ];

    const KEY = 'product';

    const KEY_COOKIE_PRDS_HISTORY = 'COOKIE_HISTORY_PRODUCT_';

    protected $fillable = [
        'image',
        'sku',
        'price',
        'global_price',
        'stock_status_id',
        'manufacturer_id',
        'weight',
        'status',
        'eshop_product_id',
        'draft_name',
        'avg_rating',
        'rating_count',
        'shipping_required',
        'is_sale',
        'hot',
        'is_quick',
        'is_home',
        'is_promotion',
        'primary_category_id',
        'color',
        'data_status',
        'sort_order',
        'new_old',
        'code',
    ];

    public static $orderClauseText = [
        1 => 'Hot',
        2 => 'Bán chạy',
        3 => 'Mới',
        4 => 'Giá thấp đến cao',
        5 => 'Giá cao đến thấp'
    ];

    public static $orderClause = [
        1 => 'products.is_new desc, products.created desc', // san pham hot
        2 => 'products.is_sale desc, products.created desc', // san pham ban chay
        3 => 'products.created desc', // san pham moiw
        4 => 'products.price asc,products.created desc', //gia thap den dao
        5 => 'products.price desc,products.created desc', // gias cao den thap
    ];

    //dong san pham
    public static $productType = [
        0 => 'Hiện đại',
        1 => 'Luxury',
    ];

    public function isDeleted(): bool
    {
        return $this->status == ProductStatus::DELETE;
    }

    public function isActive(): bool
    {
        return $this->status == ProductStatus::ACTIVE;
    }

    static function getTypeName($type = -1)
    {
        return isset(self::$productType[$type]) ? self::$productType[$type] : '';
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class, 'object_id', 'id')->where('type', 'product')->orderBy('sort');
    }

    public function getImageUrl($size = 'original')
    {
        return ImageURL::getImageUrl($this->image, self::KEY, $size);
    }

 
    public function getImageHoverAttribute($image= '', $size = 'original')
    {
        return ImageURL::getImageUrl($image, self::KEY, $size);
    }


    public function getImageSiteAttribute($size = 'mediumx1')
    {
        return ImageURL::getImageUrl($this->image, self::KEY, $size);
    }

    public function getLinkAttribute()
    {
        $slug = !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name);
        if (!empty($slug)) {
            return route('web.product.detail', ['slug' => $slug, 'id' => $this->id]);
        } else {
            return null;
        }
    }

    public static function getImageGallery($hotel_id = 0, $type = "hotel", $json = false)
    {
        $images = ProductImage::where('type', $type)->where('object_id', $hotel_id)->orderByRaw('sort desc,created desc')->get();
        $data = [];
        foreach ($images as $image) {
            $tmp = $image->toArray();
            $tmp['img'] = $image->image;
            $tmp['image_sm'] = $image->getImageUrl('hotel_small');
            $tmp['image_md'] = $image->getImageUrl('hotel_preview');
            $tmp['image'] = $image->getImageUrl('hotel_large');
            $tmp['image_org'] = $image->getImageUrl();
            array_push($data, $tmp);
        }
        return $json ? json_encode($data) : $data;
    }

    public function getLinkDetail(bool $isAdmin = false)
    {
        if ($isAdmin) {
            return route('admin_product_edit_page', ['id' => $this->id]);
        }
        return routeFrontEndFromOthers('web_product_detail_page', ['slug' => $this->desc->slug, 'id' => $this->id]);
    }

    public function lang()
    {
        $lang = config('app.locales');
        return isset($lang[$this->lang]) ? $lang[$this->lang] : 'vi';
    }

    public function price_format($strike = false)
    {
        $price = $strike ? $this->global_price : $this->price;
        return $price > 0 ? number_format($price, 0, '1', '.') . ' ' . $this->unit : '';
    }

    public function price_format_nounit($strike = false, $unit = true)
    {
        $price = $strike ? $this->priceStrike : $this->price;
        return $price > 0 ? ($unit ? FunctionLib::priceFormat($price, 'VNĐ') : number_format($price)) : '';
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function desc()
    {
        return $this->hasOne(ProductDesc::class, 'product_id', 'id');
    }

    public function all_desc()
    {
        return $this->hasMany(ProductDesc::class, 'product_id', 'id');
    }

    public function languages()
    {
        return $this->belongsToMany(Language::class, ProductDesc::getTableName(), 'product_id', 'language_id');
    }

    public function manufacturer()
    {
        return $this->hasOne(Manufacturer::class, 'manufacturer_id', 'manufacturer_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, ProductCategory::getTableName(), 'product_id', 'category_id');
        //->where('status',CategoryStatus::ACTIVE);
    }

    public function categoriesIntroduct()
    {
        return $this->hasOne(ProductCategory::class, 'product_id', 'id');

    }

    public function collections()
    {
        return $this->belongsToMany(Collection::class, CollectionDetail::getTableName(), 'object_id', 'collection_id');
        //->where('status',CategoryStatus::ACTIVE);
    }

    public function filters()
    {
        return $this->belongsToMany(Filter::class, ProductFilter::getTableName(), 'product_id', 'filter_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'product_id','id')->where('status', 2);
    }

    public function options()
    {
        return $this->belongsToMany(Option::class, 'product_option', 'product_id', 'option_id')->withPivot(['product_option_id']);
    }

    public function getImageUrlAttribute()
    {
        return UrlLib::isUrl($this->image) ? $this->image : ImageURL::getImageUrl($this->image, self::KEY, '');
    }

    public function product_variants()
    {
        return $this->hasMany(ProductVariant::class, 'product_id', 'id');
    }

    public function justOneVariant()
    {
        return $this->hasOne(ProductVariant::class, 'product_id', 'id');
    }

    public function sumStockVariant()
    {
        return $this->hasOne(ProductVariant::class, 'product_id', 'id');
    }

    public function specialOffers()
    {
        return $this->belongsToMany(SpecialOffer::class, SpecialOfferProduct::getTableName(), 'product_id', 'special_offer_id')
            ->where('status', SpecialOfferStatus::ACTIVE)
            ->where('offer_time_start', '<=', time())
            ->where('offer_time_end', '>=', time());
    }

    public function specialTags()
    {
        return $this->belongsToMany(Tags::class, TagDetails::getTableName(), 'object_id', 'tag_id')
            ->where('status', TagStatus::ACTIVE)
            ->where('cate', TagCate::LABEL)
            ->where('type', TagType::PRODUCT);
    }

    public function promotion_campaign()
    {
        return $this->hasOne(PromotionCampaignProduct::class, 'product_id', 'id');
    }

    public function wishList()
    {
        if (isset(auth()->guard(config('auth.guard_for.frontend'))->user()->id)) {
            $count = $this->hasOne(CustomerWishList::class, 'product_id', 'id')->where('customer_id', auth()->guard(config('auth.guard_for.frontend'))->user()->id)->count();
            return $count > 0 ? true : false;
        } else {
            return false;
        }
    }

    public function routeProductDetail()
    {
        $slug = !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name);
        if (!empty($slug)) {
            return route('web.product.detail', ['slug' => $slug, 'id' => $this->id]);
        } else {
            return null;
        }
    }

    public function getDiscountAttribute()
    {
        $percent = 100 - (@$this->price > 0 && @$this->global_price > 0 ? round($this->price / $this->global_price * 100) : 100);
        return ($percent > 0 ? sprintf('-%s%s', $percent, '%') : null);
    }

    public static function saveProductAfterView($id = 0, $views = 0)
    {
        $viewedProductIds = Cookie::get(Product::KEY_COOKIE_PRDS_HISTORY, []);
        $viewedProductIds = !empty($viewedProductIds) ? unserialize($viewedProductIds) : [];

        $xxx = [];
        if (count($viewedProductIds) < 50) {
            $xxx[] = $id;
        }
        $xxx = array_merge($xxx, $viewedProductIds);

        $xxx = serialize(array_unique($xxx));
        Cookie::queue(Product::KEY_COOKIE_PRDS_HISTORY, $xxx, 60 * 24 * 365);
    }

    public static function productHistory($limit = 6)
    {
        $viewedProductIds = Cookie::get(Product::KEY_COOKIE_PRDS_HISTORY, []);
        $viewedProductIds = !empty($viewedProductIds) ? unserialize($viewedProductIds) : [];
        if (!empty($viewedProductIds)) {
            $rawOrder = DB::raw(sprintf('FIELD(id, %s)', implode(',', $viewedProductIds)));

            return self::where([
                ['status', ProductStatus::ACTIVE],
            ])
                ->select('id', 'image', 'price', 'global_price')
                ->whereIn('id', $viewedProductIds)
                ->with(['desc' => function ($query) {
                    $query->select('id', 'product_id', 'language_id', 'name', 'slug');
                    $query->activeLang();
                }])
                ->orderByRaw($rawOrder)
                ->limit($limit)->get();
        }
        return [];
    }

} // End class
