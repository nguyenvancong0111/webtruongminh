<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2020-10-06 14:40:52
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-17 17:00:37
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Models;

use App\Ship\Parents\Models\Model;
use Apiato\Core\Foundation\Facades\ImageURL;
use App\Models\Product\ProductCollection;
use App\Models\System\Language;

class ProductCollectionDesc extends Model
{
    protected $table = 'product_collection_description';

    public function product()
    {
        return $this->hasOne(ProductCollection::class, 'id', 'product_collection_id');
    }

    public function language()
    {
        return $this->hasOne(Language::class, 'language_id', 'language_id');
    }

    public function setImageAttribute($value)
    {
        return $this->attributes['image'] = ImageURL::getImageUrl($value);
    }
}
