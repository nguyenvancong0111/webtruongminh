<?php
/**
 * Created by PhpStorm.
 * Filename: ProductFilter.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 8/26/20
 * Time: 00:55
 */

namespace App\Containers\Product\Models;

use App\Ship\Parents\Models\Model;

class ProductFilter extends Model
{
    protected $table = 'product_filter';
    protected $primaryKey = ['product_id','filter_id'];
}