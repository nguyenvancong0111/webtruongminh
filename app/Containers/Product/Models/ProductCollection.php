<?php
/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2020-10-06 14:40:30
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-01 10:31:36
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Models;

use App\Ship\Parents\Models\Model;

class ProductCollection extends Model
{
    protected $table = 'product_collection';

    const POSITIONS = [
        'popular' => 'Ưa chuộng',
        'main_product' => 'Sản phẩm chính',
        'bottom_home' => 'Phía cuối trang chủ',
        'bottom_product' => "Phía cuối chi tiết sản phẩm",
    ];

    public function desc()
    {
        return $this->hasOne(ProductCollectionDesc::class,'product_collection_id','id');
    }

    public function all_desc(){
        return $this->hasMany(ProductCollectionDesc::class,'product_collection_id','id');
    }

    public function items()
    {
        return $this->hasMany(ProductCollectionItems::class,'product_collection_id','id');
    }

    public function sixItems() {
        // return $this->belongsToMany(Product::class,ProductCollectionItems::getTableName(),'product_collection_id','product_id')->nPerGroup('product_id',6,[['status','=',1]]);
        return $this->hasMany(ProductCollectionItems::class,'product_collection_id','id')->nPerGroup('product_collection_id',6,[['status','=',1]]);
    }

    public function twelItems() {
        return $this->hasMany(ProductCollectionItems::class,'product_collection_id','id')->nPerGroup('product_id',12);
    }

    public function products(){
        return $this->belongsToMany(Product::class,ProductCollectionItems::getTableName(),'product_collection_id','product_id')->where(ProductCollectionItems::getTableName().'.status', '>', 0);
    }

    
} // End class
