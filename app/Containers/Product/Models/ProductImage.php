<?php
/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-01-31 18:03:27
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-04-15 19:54:46
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Models;

use Apiato\Core\Foundation\Facades\ImageURL;
use App\Ship\Parents\Models\Model;

class ProductImage extends Model
{
    // use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;
    protected $table = 'product_img';
    public $timestamps = false;
    public static $step = 1;

    protected $appends = [
        'img',
        'imglarge',
        'image_site'
    ];

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'objet_id')->where('type', 'product');
    }

    public function getImageUrl($size = 'original')
    {
        return ImageURL::getImageUrl($this->image, Product::KEY, $size);
    }

    public function getImageSiteAttribute($size = 'original')
    {
        return ImageURL::getImageUrl($this->image,Product::KEY, $size);
    }


    public static function getSortInsert($lang = 'vi')
    {
        return self::max('sort') + self::$step;
    }

    public function getImgAttribute()
    {
        return $this->getImageUrl();
    }

    public function getImglargeAttribute()
    {
        return $this->getImageUrl('large');
    }
}