<?php

namespace App\Containers\Product\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

/**
 * Class ProductOptionRepository.
 */
class ProductOptionRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Product';

    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    public function updateOnDuplicateInsert(array $productOptionInputArray=[]) {
      $keys = array_keys(Arr::first($productOptionInputArray));
      if (!empty($productOptionInputArray)) {
        $sql = sprintf('INSERT INTO %s (%s) VALUES ', $this->getModel()->getTable(), implode(', ', $keys));
        foreach ($productOptionInputArray as $productOptColumn => $productOpt) {
          $productOptDataArray = array_values($productOpt);
          $sql .= sprintf(' (%s),', implode(',', $productOptDataArray));
        }

        $sql = rtrim($sql, ',');
        $sql .= " ON DUPLICATE KEY UPDATE product_id=VALUES(product_id), option_id=VALUES(option_id), value=VALUES(value), required=VALUES(required)";
        return DB::statement($sql);
      }
    }
} // End class
