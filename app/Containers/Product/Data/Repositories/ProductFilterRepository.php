<?php

namespace App\Containers\Product\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ProductFilterRepository.
 */
class ProductFilterRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Product';

    /**
     * @var array
     */
    protected $fieldSearchable = [];
}
