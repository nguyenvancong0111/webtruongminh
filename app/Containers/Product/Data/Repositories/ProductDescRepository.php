<?php

namespace App\Containers\Product\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ProductDescRepository.
 */
class ProductDescRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Product';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        'name' => 'LIKE',
        'product_id' => '',
        'language_id',
        'meta_title' => 'LIKE',
        'meta_description' => 'LIE'
    ];

    public function boot() {
      $this->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
    }
}
