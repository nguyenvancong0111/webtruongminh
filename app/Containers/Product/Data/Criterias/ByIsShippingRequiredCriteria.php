<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-02 16:48:57
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class ByIsShippingRequiredCriteria extends Criteria
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where('shipping_required', $this->value);
    }
}
