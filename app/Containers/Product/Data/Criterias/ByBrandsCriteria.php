<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-12 19:57:23
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class ByBrandsCriteria extends Criteria
{
    private $brandIds;

    public function __construct($brandIds)
    {
        $this->brandIds = $brandIds;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->whereIn('manufacturer_id',$this->brandIds);
    }
}
