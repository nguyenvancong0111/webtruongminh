<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-11 12:42:18
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class ByCollectionsCriteria extends Criteria
{
    private $cate_ids;

    public function __construct($collection_ids)
    {
        $this->collection_ids = $collection_ids;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->whereHas('collections', function (Builder $q) {
            $q->whereIn('collections.id', $this->collection_ids);
        });
    }
}
