<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-06 13:36:46
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-06 13:40:23
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class ByIsSaleCriteria extends Criteria
{
    private $isSale;

    public function __construct($isSale)
    {
        $this->isSale = $isSale;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where('is_sale', $this->isSale);
    }
}
