<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-01 16:45:14
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class WithVariantByIdCriteria extends Criteria
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->with(['justOneVariant' => function ($q) {
            $q->where('variant_id', $this->id);
        }]);
    }
}
