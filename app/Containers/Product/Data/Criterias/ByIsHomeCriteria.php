<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-06-23 21:08:24
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-09 04:03:03
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class ByIsHomeCriteria extends Criteria
{
    private $isHome;

    public function __construct($isHome)
    {
        $this->isHome = $isHome;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where('is_home', $this->isHome);
    }
}
