<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-11 12:41:55
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-11 19:35:21
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class ByFilterGroupsCriteria extends Criteria
{
    protected $group_filter_ids;

    public function __construct(array $group_filter_ids)
    {
        $this->group_filter_ids = $group_filter_ids;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        if($this->group_filter_ids && !empty($this->group_filter_ids)) {
            foreach ($this->group_filter_ids as $itm_group) {
                $model->whereHas('filters', function (Builder $q) use ($itm_group) {
                    $q->where( function (Builder $q2) use ($itm_group) {
                        $i = 0;
                        foreach($itm_group as $itm_filter) {
                            if($i == 0) {
                                $q2->where('filter.filter_id', (int)$itm_filter);
                            }else {
                                $q2->orWhere('filter.filter_id', (int)$itm_filter); 
                            }
                            $i++;
                        }
                    });
                });
            }
        }

        return $model;
    }
}
