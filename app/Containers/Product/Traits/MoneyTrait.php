<?php

namespace App\Containers\Product\Traits;

use Apiato\Core\Foundation\FunctionLib;

trait MoneyTrait {
  public function getGlobalPriceTextAttribute() {
    return FunctionLib::priceFormat($this->global_price);
  }
}
