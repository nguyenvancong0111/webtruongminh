<?php

namespace App\Containers\LinkRedirect\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Containers\Category\Models\Category;
use App\Containers\Course\Models\Course;
use App\Containers\Exam\Models\ExamQuestion;
use App\Containers\Level\Models\Level;
use App\Ship\Parents\Models\Model;

class LinkRedirect extends Model
{
    protected $table = 'link_redirect';
    protected $fillable = ['old_link', 'new_link', 'code'];
    public $timestamps = true;

}
