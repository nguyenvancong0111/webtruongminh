<?php
Route::group(
    [
        'prefix' => 'link-redirect',
        'namespace' => '\App\Containers\LinkRedirect\UI\WEB\Controllers\Admin',
        'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
        'middleware' => [
            'auth:admin',
        ],
    ],
    function () use ($router) {
        $router->get('/', [
            'as'   => 'admin_link_redirect_home_page',
            'uses' => 'Controller@index',
        ]);

        $router->post('/store', [
            'as'   => 'admin_link_redirect_store',
            'uses' => 'Controller@store',
        ]);
    }
);
