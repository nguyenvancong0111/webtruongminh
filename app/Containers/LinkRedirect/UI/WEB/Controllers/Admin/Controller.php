<?php

namespace App\Containers\LinkRedirect\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\StringLib;
use App\Containers\LinkRedirect\Actions\LinkRedirectListingAction;
use App\Containers\LinkRedirect\Actions\StoreLinkRedirectAction;
use App\Containers\LinkRedirect\UI\WEB\Requests\GetAllLinkRedirectRequest;
use App\Containers\LinkRedirect\UI\WEB\Requests\StoreLinkRedirectRequest;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use Exception;

class Controller extends AdminController
{
    // use UpdateSomeStatus;

    public function __construct()
    {
        $this->title = 'Link Redirect';
        parent::__construct();
    }
    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllLinkRedirectRequest $request)
    {
        try {

            $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Link Redirect', $this->form == 'list' ? '' : route('admin_link_redirect_home_page'));
            \View::share('breadcrumb', $this->breadcrumb);

            $data = app(LinkRedirectListingAction::class)->skipCache(true)->run($request->all(), 0, true, 1);
            $data = $this->convertData($data);

            return view('linkredirect::admin.index', [
                'data' => json_encode($data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE),
                'urlLoad' => json_encode([
                    'createdOrUpdate' => route('admin_link_redirect_store'),
                ])
            ]);
        } catch (Exception $e) {
            dd($e->getMessage(), $e->getFile());
        }
    }


    public function store(StoreLinkRedirectRequest $request)
    {
        try {
            app(StoreLinkRedirectAction::class)->skipCache(true)->run($request->all());
            return \FunctionLib::ajaxRespondV2(true, 'success', ['title' => 'Thành công', 'mess' => 'Nội dung đã được cập nhật']);
        } catch (\Exception $e) {
            return \FunctionLib::ajaxRespondV2(false, 'error', ['title' => 'Thât bại', 'mess' => 'Đã có lỗi xảy ra. Vui lòng thử lại !']);
        }
    }


    private function convertData($data){
        $data_return = [];
        if ($data->isNotEmpty()){
            foreach ($data as $itm){
                $data_return [] = [
                    'id' => $itm->id,
                    'old_link' => $itm->old_url,
                    'new_link' => $itm->new_url,
                    'code' => $itm->code,
                ];
            }

        }
        return $data_return;
    }
}
