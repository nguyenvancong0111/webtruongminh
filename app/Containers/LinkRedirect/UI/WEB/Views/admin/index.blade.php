@extends('basecontainer::admin.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5">
                <h1>{{ $site_title }}</h1>
            </div>

            <div class="card card-accent-primary" id="link_redirect">
                <div class="card-body p-0">
                    <table class="table table-bordered table-striped ">
                        <thead>
                        <tr>
                            <th width="55">#</th>
                            <th>Old Link</th>
                            <th>New Link <small class="text-danger">(Ưu tiên)</small></th>
                            <th width="200">Code </th>
                            <th width="55">Lệnh</th>
                        </tr>
                        </thead>
                        <tbody>

                            <tr v-for="(item, key) in data.linkRedirect">
                                <input type="hidden" v-bind:name="'linkRedirect['+key+'][id]'" v-model="item.id" class="form-control js-input" >
                                <td v-html="Number(key) + 1"></td>
                                <td>
                                    <input type="text" v-bind:name="'linkRedirect['+key+'][old_link]'" v-model="item.old_link" class="form-control" >
                                </td>
                                <td>
                                    <input type="text" v-bind:name="'linkRedirect['+key+'][old_link]'" v-model="item.new_link" class="form-control" >
                                </td>
                                <td>
                                    <select class="form-control" v-bind:name="'linkRedirect['+key+'][code]'" v-model="item.code">
                                        <option :value="404">404</option>
                                        <option :value="301">301</option>
                                    </select>
                                </td>
                                <td>
                                    <span class="btn btn-danger js-input" @click="trashLinkRedirect(key)" style="cursor: pointer"><i class="fa fa-trash"></i></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="row mt-2 mb-2">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-4"><a v-if="typeof data.linkRedirect != 'undefined' && data.linkRedirect.length > 0" class="btn btn-outline-info  w-100" @click="CreateOrUpdate"  style="cursor: pointer">Cập nhật</a></div>
                        <div class="col-sm-4">
                            <a class="btn btn-outline-info  w-100" @click="addLinkRedirect"  style="cursor: pointer">Thêm Link</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js_bot_all')
    <script>
        const url = '{!! $urlLoad !!}';
        var link_redirect_data = '{!! isset($data) && !empty($data) ? addslashes($data) : '' !!}';

        var linkRedirect = new Vue({
            el: '#link_redirect',
            data() {
                return {
                    data: {
                        URL: JSON.parse(url),
                        linkRedirect: link_redirect_data != '' ? JSON.parse(link_redirect_data) : [],
                    }
                }
            },
            methods: {
                addLinkRedirect: function () {
                    var val = {old_link: '', new_link: '', code: 404 };
                    return this.data.linkRedirect.push(val);
                },
                trashLinkRedirect: function (key) {
                    if(this.data.linkRedirect.length > 1) {
                        this.data.linkRedirect.splice(key, 1);
                    }else {
                        this.data.linkRedirect.splice(key, 1);
                        var val = {old_link: '', new_link: '', code: 404 };
                        return this.data.linkRedirect.push(val);
                    }
                },

                CreateOrUpdate: function(){
                    $.post(this.data.URL.createdOrUpdate, {
                        link: this.data.linkRedirect
                    }).then(response => {
                        if (response.error === 1) {
                            Swal.fire(
                                response.code.title,
                                response.code.mess,
                                'error'
                            )
                        } else {
                            Swal.fire(
                                response.data.title,
                                response.data.mess,
                                'success'
                            )
                            shop.reload();
                        }
                    });
                },
            },
        });
    </script>
@endpush
