<?php

namespace App\Containers\LinkRedirect\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\LinkRedirect\Tasks\LinkRedirectListingTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class LinkRedirectListingAction.
 *
 */
class LinkRedirectListingAction extends Action
{

    /**
     * @return mixed
     */
    public function run(array $filters = [], int $limit = 10, bool $noPaginage = true, int $current_page = 1)
    {
        return $this->remember(function () use($filters, $limit, $noPaginage, $current_page)  {
            $data = app(LinkRedirectListingTask::class)->run(
                $filters, $limit, $noPaginage, $current_page
            );

            return $data;

        },null,[4]);
    }
}
