<?php

namespace App\Containers\LinkRedirect\Actions;

use App\Containers\LinkRedirect\Tasks\SaveLinkRedirectTask;
use App\Ship\Parents\Actions\Action;

/**
 * Class StoreLinkRedirectAction.
 *
 */
class StoreLinkRedirectAction extends Action
{

    /**
     * @return mixed
     */
    public function run( $data)
    {
        $link = app(LinkRedirectListingAction::class)->skipCache(true)->run(
            [], 0, true, 1
        );

        app(SaveLinkRedirectTask::class)->run($data, $link->keyBy('id')->toArray());
        $this->clearCache();

    }
}
