<?php

namespace App\Containers\LinkRedirect\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\LinkRedirect\Tasks\FindLinkRedirectTask;
use App\Containers\LinkRedirect\Tasks\LinkRedirectListingTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class FindLinkRedirectAction.
 *
 */
class FindLinkRedirectAction extends Action
{

    /**
     * @return mixed
     */
    public function run(array $filters = [])
    {
        $data = app(FindLinkRedirectTask::class)->run($filters);

        return $data;

    }
}
