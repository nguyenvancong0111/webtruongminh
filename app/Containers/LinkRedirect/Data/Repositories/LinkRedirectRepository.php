<?php

namespace App\Containers\LinkRedirect\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class LinkRedirectRepository
 */
class LinkRedirectRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
