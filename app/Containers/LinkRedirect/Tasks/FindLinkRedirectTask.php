<?php

namespace App\Containers\LinkRedirect\Tasks;

use App\Containers\LinkRedirect\Data\Repositories\LinkRedirectRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class FindLinkRedirectTask.
 */
class FindLinkRedirectTask extends Task
{

    protected $repository;

    public function __construct(LinkRedirectRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($filters = [])
    {

        if (isset($filters['current_link']) && !empty($filters['current_link'])){
            $data = $this->repository;
            $data = $data->where('old_url', '=', $filters['current_link']);
            return $data->first();
        }
        return null;
    }
}
