<?php

namespace App\Containers\LinkRedirect\Tasks;

use App\Containers\LinkRedirect\Data\Repositories\LinkRedirectRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SaveLinkRedirectTask.
 */
class SaveLinkRedirectTask extends Task
{

    protected $repository;

    public function __construct(LinkRedirectRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data, $original_link)
    {
        try {
            $link = $data['link'];
            $arr_id = array_column($original_link, 'id');

            $update_item = [];
            $insert_item = [];
            if (!empty($link)){
                foreach ($link as $key => $item){
                    if (isset($item['id']) && !empty($item['id']) && isset($original_link[$item['id']])){
                        unset($arr_id[array_search((int)$item['id'], $arr_id)]);
                        $update_item[$item['id']] = [
                            'old_url' => $item['old_link'],
                            'new_url' => $item['new_link'],
                            'code' => $item['code'],
                        ];
                    }else{
                        $insert_item[] = [
                            'old_url' => $item['old_link'],
                            'new_url' => $item['new_link'],
                            'code' => $item['code'],
                        ];
                    }
                }
                if (!empty($update_item)) {
                    $this->repository->updateMultiple($update_item);
                }

                if (!empty($insert_item)) {
                    $this->repository->getModel()->insert($insert_item);
                }

                if(!empty($arr_ans_id)){
                    $this->repository->whereIn('id', $arr_id)->delete();
                }
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
