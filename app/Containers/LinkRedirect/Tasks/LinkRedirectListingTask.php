<?php

namespace App\Containers\LinkRedirect\Tasks;

use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\LinkRedirect\Data\Repositories\LinkRedirectRepository;
use App\Containers\Questions\Data\Repositories\QuestionsRepository;
use App\Containers\Questions\Enums\QuestionStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class LinkRedirectListingTask.
 */
class LinkRedirectListingTask extends Task
{

    protected $repository;

    public function __construct(LinkRedirectRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($filters = [], $limit = 20, $noPaginate = true, $current_page = 1)
    {
        $data = $this->repository;
        if (isset($filters['old_url']) && !empty($filters['old_url'])) {
            $data->where('old_url', 'like', '%' . $filters['old_url'] . '%');
        }
        if (isset($filters['new_url']) && !empty($filters['new_url'])) {
            $data->where('new_url', 'like', '%' . $filters['new_url'] . '%');
        }


        return $noPaginate ? ( $limit != 0 ? $data->limit($limit) : $data->get()) :  $data->paginate($limit);
    }
}
