<?php

namespace App\Containers\Specialoffer\Tasks\SpecialofferDes;
use Illuminate\Support\Str;
use App\Containers\Specialoffer\Data\Repositories\SpecialofferDescriptionRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Apiato\Core\Foundation\Facades\Apiato;

class SaveSpecialofferDesTask extends Task
{

    protected $repository;

    public function __construct(SpecialofferDescriptionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id='', array $data)
    {
            if(!empty($data)){
              foreach ($data as $key => $item) {
                if(!empty($item) && isset($item['id'])){
                    $this->repository->getModel()->where('special_offer_id',  $id)->where('language_id',$item['language_id'])->update($item);
                }elseif(!empty($item) && !isset($item['id'])){
                  $item['special_offer_id'] = $id;
                  $checkIsset =  $this->repository->getModel()->where('special_offer_id',  $id)->where('language_id',$item['language_id'])->first();
                  if(empty($checkIsset))
                    $this->repository->getModel()->insert($item);
                  else{
                    $this->repository->getModel()->where('special_offer_id',  $id)->where('language_id',$item['language_id'])->update($item);
                  }
                }
              }
            }

    }
}
