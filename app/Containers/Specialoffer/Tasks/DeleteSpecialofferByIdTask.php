<?php

namespace App\Containers\Specialoffer\Tasks;

use App\Containers\Specialoffer\Data\Repositories\SpecialofferRepository;
use App\Containers\Specialoffer\Data\Repositories\SpecialofferProductRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteSpecialofferByIdTask extends Task
{

    protected $repository;

    public function __construct(SpecialofferRepository $repository, SpecialofferProductRepository $repositoryPrd)
    {
        $this->repository = $repository;
        $this->repositoryPrd = $repositoryPrd;
    }

    public function run($id)
    {
        try {
            $this->repositoryPrd->where('special_offer_id',$id)->delete();
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
