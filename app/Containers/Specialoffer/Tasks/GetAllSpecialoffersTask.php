<?php

namespace App\Containers\Specialoffer\Tasks;

use App\Containers\Specialoffer\Data\Repositories\SpecialofferRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetAllSpecialoffersTask extends Task
{

    protected $repository;

    public function __construct(SpecialofferRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($filters, $perPage, $skipPagination = false, $external_data = ['with_relationship' => ['all_desc']], $orderBy = 'desc')
    {
        $this->repository->with(array_merge($external_data['with_relationship'],['desc','special_offer_product']));
        if(isset($filters['name']) && $filters['name']!= ''){
            $this->repository->whereHas('desc' , function (Builder $query) use($filters){
                $query->where('name', 'like', '%' . $filters['name'] . '%');
            });
        }
        if(isset($filters['id']) && $filters['id']!= ''){
            $this->repository->where('id', $filters['id']);
        }
        $this->repository->orderBy('id', $orderBy);
        return $skipPagination ?  $this->repository->get()  : $this->repository->paginate($perPage);
    }
}
