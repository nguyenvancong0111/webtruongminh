<?php

namespace App\Containers\Specialoffer\UI\WEB\Controllers;

use App\Containers\Specialoffer\UI\WEB\Requests\GetAllSpecialoffersRequest;
use App\Containers\Specialoffer\UI\WEB\Requests\UploadImgSpecialofferRequest;
use App\Containers\Specialoffer\UI\WEB\Requests\SpecialofferListRequest;
use App\Containers\Specialoffer\UI\WEB\Requests\SaveSpecialoffersRequest;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\AdminController;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\StringLib;
use App\Containers\Specialoffer\Models\Specialoffer;
use App\Containers\Specialoffer\UI\API\Requests\FindSpecialofferByIdRequest;

class Controller extends AdminController
{

    public function index()
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Special Offer', $this->form == 'list' ? '' : route('admin_specialoffer_home_page',['']));
        \View::share('breadcrumb', $this->breadcrumb);

        return view('specialoffer::admin.index',[]);
    }

    public function ajaxSpecialofferList(SpecialofferListRequest $request){
        $skipPagination = false;
        $perPage = 10;
        try {
            $specialoffer = Apiato::call('Specialoffer@GetAllSpecialoffersAction', [$request->fillters, $perPage, $skipPagination, []]);
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $specialoffer);
        } catch (\Throwable $th) {
            throw $th;
            return FunctionLib::ajaxRespond(true, 'Lỗi');
        }
    }

    public function ajaxSpecialofferByID (FindSpecialofferByIdRequest $request){
        $dataAlldes = [];
        try {
            $specialoffer = Apiato::call('Specialoffer@FindSpecialofferByIdAction', [$request->id]);
            foreach ($specialoffer->all_desc as  $value) {
                $dataAlldes[$value['language_id']] = $value;
            }
            $specialoffer->all_desc = $dataAlldes;
            $type = new Specialoffer;
            $specialoffer->type = $type->getTypeSpecialoffer();
            $specialoffer->offer_time_start = $specialoffer->offer_time_start * 1000;
            $specialoffer->offer_time_end = $specialoffer->offer_time_end * 1000;
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $specialoffer);
        } catch (\Throwable $th) {
            return FunctionLib::ajaxRespond(true, 'Lỗi');

        }

    }

    public function ajaxImgUpload(UploadImgSpecialofferRequest $request){
        if ($request->hasFile('Filedata')) {
            $image = $request->file('Filedata');
        if ($image->isValid()) {
            $title = basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension());
            $fname = Apiato::call('File@UploadImageAction', [$request, 'Filedata', $title, StringLib::getClassNameFromString(Specialoffer::class)]);
            if (!empty($fname) && !$fname['error']) {
                    $link= \ImageURL::getImageUrl(@$fname['fileName'], 'specialoffer', 'slide');
                    $fname['link']=$link;
                    return FunctionLib::ajaxRespond(true, 'ok',$fname);
            }
            return FunctionLib::ajaxRespond(false, 'Upload ảnh thất bại!');
        }
        return FunctionLib::ajaxRespond(false, 'File không hợp lệ!');
    }
    return FunctionLib::ajaxRespond(false, 'Không tìm thấy ảnh!');
    }

    public function ajaxSaveSpecialoffer(SaveSpecialoffersRequest $request){
        // dd($request->all());
        try {
            $specialofferData =  Apiato::call('Specialoffer@SaveSpecialofferByIdAction',[$request->id,$request->data,$request->collectionPickProduct]);
            return FunctionLib::ajaxRespond(true, 'Thành công',$specialofferData);
         } catch (\Throwable $th) {
            return FunctionLib::ajaxRespond(false, 'Lỗi');
         }
    }

    public function ajaxDeleteSpecialoffer(GetAllSpecialoffersRequest $request){
        if(isset($request->id)){
          try {
            $specialofferData =  Apiato::call('Specialoffer@DeleteSpecialofferByIdAction',[$request->id]);

            return FunctionLib::ajaxRespond(true, 'Thành công',$specialofferData);
          } catch (\Throwable $th) {
            return FunctionLib::ajaxRespond(false, 'Lỗi', $th);
          }
        }
        return FunctionLib::ajaxRespond(false, 'Lỗi');
    }

    public function ajaxDataSpecialoffer(SaveSpecialoffersRequest $request){
        $transporter = $request->toTransporter();
        if ($transporter->id > 0) {
            $specialoffer = Apiato::call('specialoffer@GetSpecialofferPrdDataAction', [$transporter]);
            return FunctionLib::ajaxRespond(true, 'Thành công',$specialoffer);

        }
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function ajaxGetTypeList(){
        $type = new Specialoffer;
        $type = $type->getTypeSpecialoffer();
        return FunctionLib::ajaxRespond(true, 'Thành công',$type);
    }
}
