<?php
Route::group(
[
    'prefix' => 'specialoffer',
    'namespace' => '\App\Containers\Specialoffer\UI\WEB\Controllers',
    'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
    'middleware' => [
        'auth:admin',
    ],
],function () use ($router) {
    $router->get('/{abc}', [
      'as'   => 'admin_specialoffer_home_page',
      'uses' => 'Controller@index',
  ]);
    $router->get('/', [
      'as'   => 'admin_specialoffer_home_page',
      'uses' => 'Controller@index',
  ]);

});
