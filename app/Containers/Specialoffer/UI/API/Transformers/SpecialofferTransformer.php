<?php

namespace App\Containers\Specialoffer\UI\API\Transformers;

use App\Containers\Specialoffer\Models\Specialoffer;
use App\Ship\Parents\Transformers\Transformer;

class SpecialofferTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Specialoffer $entity
     *
     * @return array
     */
    public function transform(Specialoffer $entity)
    {
        $response = [
            'object' => 'Specialoffer',
            'id' => $entity->getHashedKey(),
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
