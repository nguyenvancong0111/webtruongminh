<?php

namespace App\Containers\Specialoffer\UI\API\Controllers;

use App\Containers\Specialoffer\UI\API\Requests\CreateSpecialofferRequest;
use App\Containers\Specialoffer\UI\API\Requests\DeleteSpecialofferRequest;
use App\Containers\Specialoffer\UI\API\Requests\GetAllSpecialoffersRequest;
use App\Containers\Specialoffer\UI\API\Requests\FindSpecialofferByIdRequest;
use App\Containers\Specialoffer\UI\API\Requests\UpdateSpecialofferRequest;
use App\Containers\Specialoffer\UI\API\Transformers\SpecialofferTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Specialoffer\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateSpecialofferRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createSpecialoffer(CreateSpecialofferRequest $request)
    {
        $specialoffer = Apiato::call('Specialoffer@CreateSpecialofferAction', [$request]);

        return $this->created($this->transform($specialoffer, SpecialofferTransformer::class));
    }

    public function findSpecialofferById(FindSpecialofferByIdRequest $request)
    {
        $specialoffer = Apiato::call('Specialoffer@FindSpecialofferByIdAction', [$request]);

        return $this->transform($specialoffer, SpecialofferTransformer::class);
    }

    public function getAllSpecialoffers(GetAllSpecialoffersRequest $request)
    {
        $specialoffers = Apiato::call('Specialoffer@GetAllSpecialoffersAction', [$request]);

        return $this->transform($specialoffers, SpecialofferTransformer::class);
    }

    public function updateSpecialoffer(UpdateSpecialofferRequest $request)
    {
        $specialoffer = Apiato::call('Specialoffer@UpdateSpecialofferAction', [$request]);

        return $this->transform($specialoffer, SpecialofferTransformer::class);
    }

    public function deleteSpecialoffer(DeleteSpecialofferRequest $request)
    {
        Apiato::call('Specialoffer@DeleteSpecialofferAction', [$request]);

        return $this->noContent();
    }
}
