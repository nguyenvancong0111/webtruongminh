<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-01 11:43:48
 * @ Description: Happy Coding!
 */

namespace App\Containers\Specialoffer\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class SpecialOfferStatus extends BaseEnum
{

}
