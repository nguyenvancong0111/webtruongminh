<?php

namespace App\Containers\Specialoffer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class SaveSpecialofferByIdAction extends Action
{
    public function run($id,$data,$pickProduct)
    {
      $specialoffer =  Apiato::call('Specialoffer@SaveSpecialofferTask', [$id,$data,$pickProduct]);
      if($id!=='undefined'){
        Apiato::call('Specialoffer@SpecialofferDes\SaveSpecialofferDesTask', [$id,$data['all_desc']]);
      }else{
        Apiato::call('Specialoffer@SpecialofferDes\SaveSpecialofferDesTask', [$specialoffer,$data['all_desc']]);
      }
      if(!empty($pickProduct)){
        $this->call('Specialoffer@SpecialofferProduct\SaveSpecialofferProductTask', [$pickProduct, $id!=='undefined' ? $id : $specialoffer]);
      }
      $this->clearCache();

      return $specialoffer;

    }
}
