<?php

namespace App\Containers\Specialoffer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetSpecialofferPrdDataAction extends Action
{
    public function run($request)
    {   $data=[];
        $specialoffer = Apiato::call('Specialoffer@SpecialofferProduct\GetSpecialofferPrdTask', [$request]);
        foreach ($specialoffer as $key => $task){
            $specialoffer[$key]['categories'] = Apiato::call('Category@GetCategoryByIdAction', [Apiato::call('Product@CateIdsDirectFromPrdIdAction', [$task['id']])]);
            if(isset($request->cate_id) && !empty($request->cate_id)){
                foreach ($specialoffer[$key]['categories'] as $k => $v){
                    # code..
                    if($v->category_id == $request->cate_id){
                        $data[$key] = $specialoffer[$key];
                    }
                } 
            }
        }
        if(isset($request->cate_id) && !empty($request->cate_id))
            return $data;
        return $specialoffer;
    }
}
