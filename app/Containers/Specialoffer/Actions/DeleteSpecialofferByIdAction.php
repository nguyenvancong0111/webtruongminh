<?php

namespace App\Containers\Specialoffer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteSpecialofferByIdAction extends Action
{
    public function run($id)
    {
      $this->clearCache();
      return Apiato::call('Specialoffer@DeleteSpecialofferByIdTask', [$id]);
    }
}
