<?php

namespace App\Containers\Specialoffer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllSpecialoffersAction extends Action
{
    public function run($filter, $perPage = 11, $skipPagination = true, $external_data = [])
    {
        if (empty($external_data)) {
            $specialoffers = Apiato::call('Specialoffer@GetAllSpecialoffersTask', [$filter, $perPage, $skipPagination]);
        } else {
            $specialoffers = Apiato::call('Specialoffer@GetAllSpecialoffersTask', [$filter, $perPage, $skipPagination, $external_data]);
        }

        return $specialoffers;
    }
}
