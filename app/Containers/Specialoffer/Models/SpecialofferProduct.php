<?php

namespace App\Containers\Specialoffer\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Product\Models\ProductDesc;
use App\Containers\Product\Models\Product;

class SpecialofferProduct extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];
    protected $guarded = [];  
    public $timestamps = false;
    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $table = 'special_offer_product';
    protected $primaryKey = 'id';
    public function desc_product(){
        return $this->hasOne(ProductDesc::class,'product_id','product_id');
    }
    public function product(){
        return $this->hasOne(Product::class,'id','product_id');
    }
}
