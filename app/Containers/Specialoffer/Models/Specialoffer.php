<?php

namespace App\Containers\Specialoffer\Models;

use App\Ship\Parents\Models\Model;

class Specialoffer extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $table = 'special_offer';
    protected $primaryKey = 'id';

    const TYPE = [
        'price' => 'Giảm số tiền cụ thể trực tiếp vào giá bán',
        // 'voucher_accessory' => 'Tặng phiếu mua hàng phụ kiện',
        'refund' => 'Hoàn tiền',
    ];
    public function getTypeSpecialoffer(){
        return self::TYPE;
    }
    public function desc(){
        return $this->hasOne(SpecialofferDescription::class,'special_offer_id','id');
    }
    public function all_desc(){
        return $this->hasMany(SpecialofferDescription::class,'special_offer_id','id');
    }
    public function special_offer_product(){
        return $this->hasMany(SpecialofferProduct::class,'special_offer_id','id');
    }
}
