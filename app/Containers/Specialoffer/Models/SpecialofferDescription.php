<?php

namespace App\Containers\Specialoffer\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class SpecialofferDescription extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $table = 'special_offer_description';
    protected $primaryKey = 'id';

    public function language()
    {
        return $this->hasOne(Language::class,'language_id','language_id');
    }
}
