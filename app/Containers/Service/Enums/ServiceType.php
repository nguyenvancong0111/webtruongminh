<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-04 22:57:21
 * @ Description: Happy Coding!
 */

namespace App\Containers\Service\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class ServiceType extends BaseEnum
{
    /**
     * Dịch vụ trực tuyến
     */
    const ONLINE = 1;

    /**
     * Dịch vụ trực tiếp
     */
    const OFFLINE = 2;

    const TEXT = [
        self::ONLINE => 'Dịch vụ tư vấn trực tuyến',
        self::OFFLINE => 'Dịch vụ tư vấn trực tiếp',
    ];

}
