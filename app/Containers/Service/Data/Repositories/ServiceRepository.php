<?php

namespace App\Containers\Service\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ServiceRepository.
 */
class ServiceRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Service';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'status',
        'published',
        'views',
        'created_at',
        'desc.name',
        'desc.language_id'
    ];
}
