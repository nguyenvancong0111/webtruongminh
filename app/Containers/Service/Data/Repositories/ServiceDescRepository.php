<?php

namespace App\Containers\Service\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ServiceDescRepository.
 */
class ServiceDescRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Service';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'news_id',
        'language_id',
        'name',
        'tag',
    ];
}
