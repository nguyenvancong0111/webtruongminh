<?php

namespace App\Containers\Service\Tasks;

use App\Containers\Service\Data\Repositories\ServiceRepository;
use App\Ship\Parents\Tasks\Task;
/**
 * Class FindNewsByIdTask.
 */
class FindServiceByIdTask extends Task
{

    protected $repository;

    public function __construct(ServiceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($service_id,$defaultLanguage = null, $external_data = ['with_relationship' => ['all_desc']])
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;

        $data = $this->repository->with(array_merge($external_data['with_relationship'],[]))->find($service_id);

        return $data;
    }
}