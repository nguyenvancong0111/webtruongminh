<?php

namespace App\Containers\Service\Tasks;

use App\Containers\Service\Data\Repositories\ServiceDescRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class DeleteNewsDescTask.
 */
class DeleteServiceDescTask extends Task
{

    protected $repository;

    public function __construct(ServiceDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($service_id)
    {
        try {
            $this->repository->getModel()->where('service_id', $service_id)->delete();
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
