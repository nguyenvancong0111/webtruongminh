<?php

namespace App\Containers\Service\Tasks;

use App\Containers\Service\Data\Repositories\ServiceRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class CreateNewsTask.
 */
class CreateServiceTask extends Task
{

    protected $repository;

    public function __construct(ServiceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {

        try {
            $dataUpdate = Arr::except($data->toArray(), ['service_description', '_token', '_headers']);
            $dataUpdate['author'] = auth()->guard('admin')->user()->name;

            $service = $this->repository->create($dataUpdate);


            return $service;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
