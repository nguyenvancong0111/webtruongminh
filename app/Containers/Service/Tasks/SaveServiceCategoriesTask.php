<?php

namespace App\Containers\Service\Tasks;

use App\Containers\Service\Data\Repositories\ServiceRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class SaveNewsCategoriesTask.
 */
class SaveServiceCategoriesTask extends Task
{

    protected $repository;

    public function __construct(ServiceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data,$news)
    {
        try {
            $news_category = $data['service_category'] ?? [];

            if (!empty($news_category)) {
                // dd($news,$news_category->toArray());
                // if (is_array($news_category)) {
                    $news->categories()->sync($news_category->toArray());
                // } else {
                //     $news->categories()->attach($news_category);
                // }
            }else {
                $news->categories()->sync([]);
            }
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
