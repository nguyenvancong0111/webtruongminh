<?php

namespace App\Containers\Service\Tasks;

use App\Containers\Service\Data\Repositories\ServiceDescRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class GetAllNewsDescTask.
 */
class GetAllServiceDescTask extends Task
{

    protected $repository;

    public function __construct(ServiceDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($service_id)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('service_id', $service_id));
        $wery = $this->repository->all()->keyBy('language_id')->toArray();

        return $wery;
    }
}
