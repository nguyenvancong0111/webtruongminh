<?php

namespace App\Containers\Service\Tasks;

use App\Containers\Service\Data\Repositories\ServiceRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class DeleteNewsTask.
 */
class DeleteServiceTask extends Task
{

    protected $repository;

    public function __construct(ServiceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($news_id)
    {
        try {
            $this->repository->update(['status' => -1],$news_id);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
