<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-05 12:37:37
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-14 16:45:25
 * @ Description: Happy Coding!
 */

namespace App\Containers\Service\Tasks;

use App\Containers\Localization\Models\Language;
use App\Containers\Service\Data\Repositories\ServiceRepository;
use App\Containers\Service\Models\Service;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetServiceBySlugIdTask extends Task
{

    protected $repository;

    public function __construct(ServiceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run(int $serviceId, string $slug, Language $currentLang = null): ?Service
    {
        $language_id = $currentLang ? $currentLang->language_id : 1;

        $data = $this->repository->where('status','=', 2)->with([
            'desc' => function($query) use($language_id) {
                $query->activeLang($language_id);
            }]);
        $data->whereHas('desc', function (Builder $query) use ($slug) {
            $query->where('slug', '=', $slug);
        });
        // $this->repository->pushCriteria(new ThisEqualThatCriteria('path_id',$category_id));

        return $data->where('id',$serviceId)->first();
    }
}
