<?php

namespace App\Containers\Service\Tasks;

use App\Containers\Service\Data\Repositories\ServiceRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class SaveNewsTask.
 */
class SaveServiceTask extends Task
{

    protected $repository;

    public function __construct(ServiceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data->toArray(), ['service_description', '_token', '_headers']);
            $dataUpdate['author_updated'] = auth()->guard('admin')->user()->name;
            $news = $this->repository->update($dataUpdate, $data->id);

            return $news;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
