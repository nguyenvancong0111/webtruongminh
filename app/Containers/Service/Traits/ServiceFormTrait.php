<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-03 17:13:07
 * @ Description: Happy Coding!
 */

namespace App\Containers\Service\Traits;

use App\Containers\Order\Enums\OrderStatus;
use App\Containers\Service\Enums\ServiceForm;
use App\Containers\Service\Enums\ServiceType;
use App\Containers\Settings\Enums\PaymentStatus;

trait ServiceFormTrait
{
  public function isHour(): bool
  {
    return $this->type == ServiceForm::HOUR;
  }

  public function isCase(): bool
  {
    return $this->type == ServiceForm::CASE;
  }

  public function getServiceFormText(): string
  {
    $text = isset(ServiceForm::TEXT[$this->type]) ? ServiceForm::TEXT[$this->type] : '';
    return $text ? $text : 'Không xác định';
  }

    public function getPopupForm(): string
    {
        if ($this->isHour()) {
            return 'popupTimeline';
        } elseif ($this->isCase()) {
            return 'popupTimelineCase';
        }
        else {
            return 'text-light';
        }
    }
} // End class
