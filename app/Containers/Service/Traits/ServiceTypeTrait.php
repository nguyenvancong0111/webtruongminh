<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-03 17:13:07
 * @ Description: Happy Coding!
 */

namespace App\Containers\Service\Traits;

use App\Containers\Order\Enums\OrderStatus;
use App\Containers\Service\Enums\ServiceType;
use App\Containers\Settings\Enums\PaymentStatus;

trait ServiceTypeTrait
{
  public function isOnline(): bool
  {
    return $this->type == ServiceType::ONLINE;
  }

  public function isOffile(): bool
  {
    return $this->type == ServiceType::OFFLINE;
  }

  public function getServiceTypeText(): string
  {
    $text = isset(ServiceType::TEXT[$this->type]) ? ServiceType::TEXT[$this->type] : '';
    return $text ? $text : 'Không xác định';
  }
} // End class
