<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 01:35:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-14 16:44:26
 * @ Description: Happy Coding!
 */

namespace App\Containers\Service\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\News\Models\News;
use App\Containers\News\Tasks\GetNewsBySlugIdTask;
use App\Containers\Service\Models\Service;
use App\Containers\Service\Tasks\GetServiceBySlugIdTask;
use App\Ship\Parents\Actions\Action;

class GetServiceBySlugIdAction extends Action
{
    protected $getServiceBySlugIdTask;
    public function __construct(GetServiceBySlugIdTask $getServiceBySlugIdTask)
    {
        parent::__construct();
        $this->getServiceBySlugIdTask = $getServiceBySlugIdTask;
    }
    public function run(int $serviceId, string $slug, Language $currentLang = null): ?Service
    {
//        return $this->remember(function () use ($serviceId, $slug, $currentLang) {
            return $this->getServiceBySlugIdTask->run($serviceId, $slug, $currentLang);
//        });
    }
}
