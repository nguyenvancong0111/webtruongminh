<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 01:35:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-05 15:51:55
 * @ Description: Happy Coding!
 */

namespace App\Containers\Service\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetServiceRecentlyAction extends Action
{
    public function run($type, array $filter = [], int $limit = 20, bool $isPanination = false, Language $currentLang = null, array $orderBy = ['created_at' => 'desc', 'id' => 'desc']): iterable {
//        return $this->remember(function () use ($type, $filter, $limit, $isPanination, $currentLang,$orderBy) {
            return $this->call('Service@FrontEnd\GetServiceRecentlyTask', [
                $type,
                $filter,
                $limit,
                $isPanination,
                $currentLang,
                $orderBy

            ]);
//        }, null, [5]);
    }
}
