<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 01:35:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-14 16:44:26
 * @ Description: Happy Coding!
 */

namespace App\Containers\Service\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\Service\Models\Service;
use App\Containers\Service\Tasks\FrontEnd\GetServiceListTask;
use App\Ship\Parents\Actions\Action;

class GetServiceByArrIdAction extends Action
{
    protected $getServiceListTask;
    public function __construct(GetServiceListTask $getServiceListTask)
    {
        parent::__construct();
        $this->getServiceListTask = $getServiceListTask;
    }
    public function run(array $filters = [], array $orderBy = ['created_at' => 'desc', 'id' => 'desc'], int $limit = 20, bool $skipPagination = false, Language $currentLang = null, array $externalData = [], int $currentPage = 1)
    {
//        return $this->remember(function () use ($filters, $orderBy, $limit, $skipPagination, $currentLang, $externalData, $currentPage) {
            return $this->getServiceListTask->run($filters, $orderBy, $limit, $skipPagination, $currentLang, $externalData, $currentPage);
//        });
    }
}
