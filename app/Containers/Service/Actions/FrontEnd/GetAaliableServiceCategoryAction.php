<?php

namespace App\Containers\Service\Actions\FrontEnd;

use App\Ship\Parents\Actions\Action;
use App\Containers\Localization\Models\Language;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAaliableServiceCategoryAction extends Action
{
    public function run(array $with = [], array $orderBy = [], $selectFields = ['*'],int $pagination = 12 ,Language $currentLang = null,array $withNew = [], array $where = [], array $notWhere = [])
    {
        return Apiato::call('Service@FrontEnd\GetAvailableServiceCategoryTask', [$pagination,$currentLang,$withNew,$where,$notWhere],
            [
                ['orderBy' => [$orderBy]],
                ['with' => [$with]],
                ['mobile' => [false]],
                ['selectFields' => [$selectFields],$currentLang]
            ]
        );
    }
}
