<?php

namespace App\Containers\Service\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class DeleteServiceAction.
 *
 */
class DeleteServiceAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        Apiato::call('Service@DeleteServiceTask', [$data->id]);
        // Apiato::call('News@DeleteNewsDescTask', [$data->id]);

        $this->clearCache();
    }
}
