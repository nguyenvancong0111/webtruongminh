<?php

namespace App\Containers\Service\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class CreateNewsAction.
 *
 */
class CreateServiceAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $service = Apiato::call(
            'Service@CreateServiceTask',
            [
                $data
            ]
        );

        if ($service) {
            Apiato::call('Service@SaveServiceDescTask', [
                $data,
                [],
                $service->id
            ]);
//            Apiato::call('Service@SaveServiceCategoriesTask',[$data,$service]);
        }

        $this->clearCache();

        return $service;
    }
}
