<?php

namespace App\Containers\Service\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class FindNewsByIdAction.
 *
 */
class FindServiceByIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($news_id, $withData = [])
    {
        $data = Apiato::call('Service@FindServiceByIdTask', [
            $news_id,
            Apiato::call('Localization@GetDefaultLanguageTask'),
            $external_data = ['with_relationship' => array_merge(['all_desc'], $withData)]
        ]);

        if ($data) {
            $data->setRelation('all_desc', $data->all_desc->keyBy('language_id'));
        }

        return $data;
    }
}
