<?php

namespace App\Containers\Service\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class UpdateServiceAction.
 *
 */
class UpdateServiceAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $service = Apiato::call(
            'Service@SaveServiceTask',
            [
                $data
            ]
        );

        if($service) {
            $original_desc = Apiato::call('Service@GetAllServiceDescTask', [$service->id]);
            Apiato::call('Service@SaveServiceDescTask', [
                $data,
                $original_desc,
                $service->id
            ]);
//            Apiato::call('Service@SaveServiceCategoriesTask',[$data,$service]);
        }

        $this->clearCache();

        return $service;
    }
}
