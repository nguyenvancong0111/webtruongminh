<?php
/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2020-10-01 15:45:43
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-02-11 17:29:07
 * @ Description:
 */

namespace App\Containers\Service\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class ServiceDesc extends Model {
    protected $table = 'service_description';

    public function language()
    {
        return $this->hasOne(Language::class,'language_id','language_id');
    }
}