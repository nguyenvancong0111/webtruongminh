<?php

namespace App\Containers\Service\Models;

use Apiato\Core\Foundation\Facades\ImageURL;
use App\Containers\Category\Enums\CategoryStatus;
use App\Containers\Category\Models\Category;
use App\Containers\Service\Traits\ServiceFormTrait;
use App\Ship\Parents\Models\Model;
use Illuminate\Support\Str;

class Service extends Model
{
    use ServiceFormTrait;
    protected $table = 'service';

    protected $appends = ['format_date'];

    protected $fillable = [
        'status',
        'consultancy_costs',
        'deposit_cost',
        'consultation_time',
        'image',
        'icon',
        'icon_hover',
        'sort_order',
        'author',
        'author_updated',
        'type',
        'created_at',
        'is_home',
        'cate',
        'month'
    ];

    public function category()
    {
        return $this->hasOne(Category::class, 'category_id', 'cate');
    }

    public function getImageUrl($size = 'original')
    {
        return ImageURL::getImageUrl($this->image, 'service', $size);
    }

    public function desc()
    {
        return $this->hasOne(ServiceDesc::class, 'service_id', 'id');
    }

    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', ServiceDesc::class, 'service_id', 'id');
    }

    public function getFormatDateAttribute()
    {
        return ($this->created_at) ? $this->created_at->format('d.m.Y') : null;
    }

    public function link()
    {
        $slug = !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name);
        if (!empty($slug)) {
            switch($this->type){
                case 2:
                    return route('web.services.offline_detail', ['slug_service_offline' => $slug, 'id_service_offline' => $this->id]);
                    break;
                case 1:
                    return route('web.services.online_detail', ['slug_service_online' => $slug, 'id_service_online' => $this->id]);
                    break;
            }
        } else {
            return false;
        }
    }

}

