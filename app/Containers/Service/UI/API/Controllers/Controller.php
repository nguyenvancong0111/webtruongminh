<?php

namespace App\Containers\Service\UI\API\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Service\UI\API\Requests\UpdateServiceRequest;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Response;

/**
 * Class Controller.
 */
class Controller extends ApiController
{
    public function updateService(UpdateServiceRequest $request) {
        $tranporter = new DataTransporter($request);
        $service = Apiato::call('Service@UpdateServiceAction', [$tranporter]);

        if ($service) {
            return FunctionLib::ajaxRespondV2(true,'Success');
        }
        return FunctionLib::ajaxRespondV2(false, 'Fail',[],Response::HTTP_NOT_ACCEPTABLE);
    }
}
