<?php

/**
 * @apiGroup           Category
 * @apiName            getCategories
 * @api                {get} /v1/category/getCategories Get All Catagories
 *
 * @apiVersion         1.0.0
 * @apiPermission      Authenticated User
 *
 * @apiUse             GeneralSuccessMultipleResponse
 */

$router->post('service/updateService', [
    'as' => 'api_service_edit',
    'uses'       => 'Controller@updateService',
    'middleware' => [
        'auth:api',
    ],
]);
