<div class="tab-pane" id="data">
    <div class="tabbable">
        {{-- <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="published">Ngày hiệu lực <span class="d-block small text-danger">(Ngày mà bài viết chính thức được đăng)</span></label>
            <div class="col-sm-6">
                <input type="text" name="published" readonly value="{{ old('published',@$data['published'] ? Carbon\Carbon::parse($data['published'])->format('d/m/Y H:i') : '') }}" placeholder="dd/mm/YY H:i"
                       id="published" class="form-control">
            </div>
        </div> --}}

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="status">Hiển thị ở trang chủ <span
                    class="d-block small text-danger">(Hiển thị hay không)</span></label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('is_home') ? ' is-invalid' : '' }}" name="is_home" id="status">
                    <option value="0" {{@$data['is_home'] == 0 ? 'selected' : ''}}>Ẩn</option>
                    <option value="2" {{@$data['is_home'] == 2 ? 'selected' : ''}}>Hiển thị</option>
                </select>
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="status">Trạng thái <span
                    class="d-block small text-danger">(Hiển thị hay không)</span></label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" id="status">
                    <option value="1" {{@$data['status'] == 1 ? 'selected' : ''}}>Ẩn</option>
                    <option value="2" {{@$data['status'] == 2 ? 'selected' : ''}}>Hiển thị</option>
                </select>
            </div>
        </div>
        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="type">Danh mục <span class="d-block small text-danger"></span></label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('cate') ? ' is-invalid' : '' }}" name="cate" id="cate">
                    <option value="">--- Lựa chọn --- </option>
                    @foreach($cate as $key => $item)
                        <option value="{{ $item->category_id }}" @if($item->category_id == (@$data['cate'] ?? null)) selected @endif>
                            {{ $item->name }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="type">Phân loại <span class="d-block small text-danger"></span></label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" id="type">
                    @foreach($typeOptions as $key=>$type)
                        <option value="{{ $key }}" @if($key == (@$data['type'] ?? null)) selected @endif>
                            {{ $type }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="type">Chi phí tư vấn <span class="d-block small text-danger"> VNĐ/Tháng - Có giá trị hiển thị </span></label>
            <div class="col-sm-6">
                <input type="text" name="consultancy_costs" value="{{ old('consultancy_costs', @$data['consultancy_costs'])}}" placeholder="" id="consultancy_costs" class="form-control" >
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="type">Chi phí <span class="d-block small text-danger"> Là giá trị thanh toán của dịch vụ: Đặt cọc, giá dịch vụ </span></label>
            <div class="col-sm-6">
                <input type="text" name="deposit_cost" value="{{ old('deposit_cost', \Apiato\Core\Foundation\FunctionLib::numberFormat(@$data['deposit_cost']))}}" placeholder="" id="deposit_cost" class="form-control" onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()">
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="type">Thời gian tư vấn <span class="d-block small text-danger"> Giờ </span></label>
            <div class="col-sm-6">
                <input type="text" name="consultation_time" value="{{ old('consultation_time', \Apiato\Core\Foundation\FunctionLib::numberFormat(@$data['consultation_time']))}}" placeholder="" id="consultation_time" class="form-control" onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()">
            </div>
        </div>
        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="type">Thời gian tư vấn <span class="d-block small text-danger"> Tháng </span></label>
            <div class="col-sm-6">
                <input type="text" name="month" value="{{ old('month', \Apiato\Core\Foundation\FunctionLib::numberFormat(@$data['month']))}}" placeholder="" id="month" class="form-control" onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()">
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="sort_order">Sắp xếp <span
                    class="d-block small text-danger">(Vị trí, càng nhỏ thì càng lên đầu)</span></label>
            <div class="col-sm-3">
                <input type="text" name="sort_order" value="{{ old('sort_order', \Apiato\Core\Foundation\FunctionLib::numberFormat(@$data['sort_order']))}}" placeholder="Vị trí sắp xếp" id="sort_order" class="form-control" onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()">
            </div>
        </div>
    </div>
</div>

@push('js_bot_all')
    <script>
        $('#published').datetimepicker({format: 'd/m/Y H:i',});
    </script>
@endpush
