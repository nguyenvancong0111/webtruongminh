<?php

namespace App\Containers\Service\UI\WEB\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateServiceRequest.
 *
 */
class StoreServiceRequest extends Request
{

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles'       => 'admin',
        'permissions' => 'create-service',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            // 'id'           => 'required|exists:service,id',
//            'service_category' => 'required'
            'service_description*name' => 'required',
            'image' => 'bail|mimes:jpeg,jpg,png,gif',
        ];

    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
//            'id.required'  => 'ID không tồn tại',
//            'id.exists'  => 'ID không tồn tại',
//            'service_category.required' => 'Danh mục không được bỏ trống'
            'service_description.1.name' => 'Tên Tiếng Việt không được bỏ trống',
            'service_description.2.name' => 'Tên Tiếng Anh không được bỏ trống',
            'service_description.3.name' => 'Tên Tiếng Trung không được bỏ trống',
            'image.mimes' => 'Ảnh đại diện không đúng định dạng (jpeg, jpg, png, gif)',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
