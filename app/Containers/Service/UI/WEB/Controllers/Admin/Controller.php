<?php

namespace App\Containers\Service\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Category\Actions\Admin\GetAllCategoriesAction;
use App\Containers\Service\Models\Service;
use App\Containers\Service\UI\WEB\Controllers\Admin\Features\UpdateSomeStatus;
use App\Containers\Service\UI\WEB\Requests\Admin\FindServiceRequest;
use App\Containers\Service\UI\WEB\Requests\CreateServiceRequest;
use App\Containers\Service\UI\WEB\Requests\EditServiceRequest;
use App\Containers\Service\UI\WEB\Requests\StoreServiceRequest;
use App\Containers\Service\UI\WEB\Requests\UpdateServiceRequest;
use App\Containers\Service\UI\WEB\Requests\GetAllServiceRequest;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Request;
use App\Containers\Service\Enums\ServiceType;

class Controller extends AdminController
{
    use UpdateSomeStatus;

    public function __construct()
    {
        $this->title = 'Dịch vụ';
        parent::__construct();
    }
    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllServiceRequest $request)
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Dịch Vụ ', $this->form == 'list' ? '' : route('admin_service_home_page'));
        \View::share('breadcrumb', $this->breadcrumb);

        $Service = Apiato::call('Service@ServiceListingAction', [$request->all(),['id' => 'desc'], $this->perPage, false,Apiato::call('Localization@GetDefaultLanguageAction'),[],$request->page ?? 1]);

        return view('service::admin.index', [
            'search_data' => $request,
            'data' => $Service,
            'typeOptions'=> ServiceType::TEXT,
        ]);
    }

    public function edit(Request $request)
    {
        $this->showEditForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Dịch Vụ', $this->form == 'list' ? '' : route('admin_service_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Chỉnh sửa thông tin');
        \View::share('breadcrumb', $this->breadcrumb);

        $Service = Apiato::call('Service@Admin\FindServiceByIdAction', [$request->id, []]);
        $cate = app(GetAllCategoriesAction::class)->skipCache(true)->run(
            ['cate_type' => 1, 'status' => 2], 10, true, 1
        );
        return view('service::admin.edit', [
            'data' => $Service,
            'typeOptions'=> ServiceType::TEXT,
            'cate' => $cate
        ]);

        return $this->notfound($request->id);
    }

    public function add(Request $request)
    {
        $this->showAddForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Dịch Vụ', $this->form == 'list' ? '' : route('admin_service_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Thêm mới');
        \View::share('breadcrumb', $this->breadcrumb);

        $cate = app(GetAllCategoriesAction::class)->skipCache(true)->run(
            ['cate_type' => 1, 'status' => 2], 10, true, 1
        );
        return view('service::admin.edit', [
            'typeOptions'=> ServiceType::TEXT,
            'cate' => $cate
        ]);
    }

    public function update(UpdateServiceRequest $request)
    {

        try {
            $tranporter = new DataTransporter($request);

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'service', StringLib::getClassNameFromString(Service::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }
            if (isset($request->icon)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'icon', 'icon-service', StringLib::getClassNameFromString(Service::class)]);
                if (!$image['error']) {
                    $tranporter->icon = $image['fileName'];
                }
            }
            if (isset($request->icon_hover)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'icon_hover', 'icon-hover-service', StringLib::getClassNameFromString(Service::class)]);
                if (!$image['error']) {
                    $tranporter->icon_hover = $image['fileName'];
                }
            }

            if (isset($request->deposit_cost)){
                $tranporter->deposit_cost = intval(str_replace([',', '.'], '', $request->deposit_cost));
            }
            if (isset($request->consultation_time)){
                $tranporter->consultation_time = intval(str_replace([',', '.'], '', $request->consultation_time));
            }

            $Service = Apiato::call('Service@UpdateServiceAction', [$tranporter]);

            if ($Service) {
                return redirect()->route('admin_service_edit_page', ['id' => $Service->id])->with('status', 'Dịch vụ đã được cập nhật');
            }
        } catch (\Exception $e) {
            throw $e;
            if (isset($images) && isset($image['error']))
              return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
            else
              return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . isset($image) && isset($image['msg']) ? $image['msg'] : '']);
        }
    }

    public function create(StoreServiceRequest $request)
    {
        try {
            $tranporter = new DataTransporter($request);

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'image-service', StringLib::getClassNameFromString(Service::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }
            if (isset($request->icon)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'icon', 'icon-service', StringLib::getClassNameFromString(Service::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }
            if (isset($request->icon_hover)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'icon_hover', 'icon-hover-service', StringLib::getClassNameFromString(Service::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }

            if (isset($request->deposit_cost)){
                $tranporter->deposit_cost = intval(str_replace([',', '.'], '', $request->deposit_cost));
            }
            if (isset($request->consultation_time)){
                $tranporter->consultation_time = intval(str_replace([',', '.'], '', $request->consultation_time));
            }

            $Service = Apiato::call('Service@CreateServiceAction', [$tranporter]);
            if ($Service) {
                return redirect()->route('admin_service_home_page')->with('status', 'Dịch vụ đã được thêm mới');
            }
        } catch (\Exception $e) {
          if (isset($image['error']))
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
          else
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . @$image['msg']]);
        }
    }

    public function delete(FindServiceRequest $request)
    {
        try {
            Apiato::call('Service@DeleteServiceAction', [$request]);
        } catch (\Exception $e) {
            // throw $e;
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
        }
    }


}
