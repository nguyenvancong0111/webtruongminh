<?php

/**
 * Created by PhpStorm.
 * Filename: Manufacturer.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 8/19/20
 * Time: 15:06
 */

namespace App\Containers\Manufacturer\Models;

use App\Ship\Parents\Models\Model;

class ManufacturerCategory extends Model
{
    protected $table = 'manufacturer_category';

    protected $primaryKey = 'manufacturer_id';

    public function category()
    {
        return $this->hasOne(Category::class, 'category_id', 'category_id');
    }

    public function manufacturer()
    {
        return $this->hasOne(Manufacturer::class, 'manufacturer_id', 'manufacturer_id');
    }
}
