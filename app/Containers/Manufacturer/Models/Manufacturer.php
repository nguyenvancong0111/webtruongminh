<?php

/**
 * Created by PhpStorm.
 * Filename: Manufacturer.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 8/19/20
 * Time: 15:06
 */

namespace App\Containers\Manufacturer\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Manufacturer\Models\ManufacturerCategory;
use App\Containers\Category\Models\Category;

class Manufacturer extends Model
{
    protected $fillable = [
        'name',
        'image',
        'sort_order',
        'primary_category_id',
    ];

    protected $table = 'manufacturer';

    protected $primaryKey = 'manufacturer_id';

    public function categories()
    {
        return $this->belongsToMany(Category::class, ManufacturerCategory::getTableName(), 'manufacturer_id', 'category_id');
    }
}
