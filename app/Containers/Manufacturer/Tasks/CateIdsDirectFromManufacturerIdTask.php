<?php

namespace App\Containers\Manufacturer\Tasks;

use App\Containers\Manufacturer\Data\Repositories\ManufacturerCategoryRepository;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisInThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class CateIdsDirectFromPrdIdTask.
 */
class CateIdsDirectFromManufacturerIdTask extends Task
{

    protected $repository;

    public function __construct(ManufacturerCategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return  mixed
     */
    public function run($manufacturer_id)
    {
        if(is_array($manufacturer_id)) {
            $this->repository->pushCriteria(new ThisInThatCriteria('manufacturer_id',$manufacturer_id));
        }else {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('manufacturer_id',$manufacturer_id));
        }

        $this->repository->pushCriteria(new SelectFieldsCriteria(['category_id']));

        $result = $this->repository->all();

        if($result && !$result->isEmpty()) {
            return $result->pluck('category_id')->toArray();
        }

        return [];
    }
}
