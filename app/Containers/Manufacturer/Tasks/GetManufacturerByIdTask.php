<?php

namespace App\Containers\Manufacturer\Tasks;

use App\Containers\Manufacturer\Data\Repositories\ManufacturerRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class GetManufacturerByIdTask extends Task
{

    protected $repository;

    public function __construct(ManufacturerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->where('manufacturer_id',$id)->first();
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
