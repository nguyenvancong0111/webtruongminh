<?php

namespace App\Containers\Manufacturer\Tasks;
use Illuminate\Support\Str;
use App\Containers\Manufacturer\Data\Repositories\ManufacturerRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Arr;
use Exception;

class SaveManufacturerTask extends Task
{

    protected $repository;

    public function __construct(ManufacturerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id='', array $data)
    {
        try {
            $customerData = Arr::except($data, ['updated_at', 'manufacturer_id', 'created_at']);
            if($id!='undefined'){
                return $this->repository->update($customerData,$id);
            }
            else{
                return $this->repository->create($customerData);
            }


        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
