<?php

namespace App\Containers\Manufacturer\UI\WEB\Controllers;

use App\Containers\Manufacturer\UI\WEB\Requests\GetAllManufacturersRequest;
use App\Containers\Manufacturer\UI\WEB\Requests\ManufacturerListRequest;
use App\Containers\Manufacturer\UI\WEB\Requests\ManufacturerByIDRequest;
use App\Containers\Manufacturer\UI\WEB\Requests\UploadImgManufacturerRequest;
use App\Containers\Manufacturer\UI\WEB\Requests\ManufacturerDeleteRequest;
use App\Ship\Parents\Controllers\AdminController;
use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Manufacturer\Models\Manufacturer;

/**
 * Class Controller
 *
 * @package App\Containers\Manufacturer\UI\WEB\Controllers
 */
class Controller extends AdminController
{
    /**
     * Show all entities
     *
     * @param GetAllManufacturersRequest $request
     */
    public function __construct()
    {
        $this->title = 'Quản trị thương hiệu';
        parent::__construct();
    }

    public function index()
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Thương hiệu', $this->form == 'list' ? '' : route('admin_manufacturer_home_page',['']));
        \View::share('breadcrumb', $this->breadcrumb);

        return view('manufacturer::admin.index', [
        ]);
    }

    public function ajaxManufacturerList(ManufacturerListRequest $request){
        try {
            $perPage = $request->perPage ?? 15;
            $notPaginate = $request->noPagi ?? 0;
            $manufacturer = Apiato::call('Manufacturer@GetAllManufacturersAction', [$request->fillters, $perPage, $notPaginate]);
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $manufacturer);
        } catch (\Throwable $th) {
            return FunctionLib::ajaxRespond(false, 'Xảy ra lỗi');
        }
    }

    public function ajaxManufacturerByID(ManufacturerByIDRequest $request){
        $manufacturer = Apiato::call('Manufacturer@GetManufacturerByIdAction', [$request->id]);
        return FunctionLib::ajaxRespond(true, 'Hoàn thành', $manufacturer);
    }

    public function ajaxSaveManufacturer(ManufacturerListRequest $request){

        // try {
            $manufacturerData =  Apiato::call('Manufacturer@SaveManufacturerByIdAction',[$request->id,$request->data,$request->manufacturerCategory]);
            return FunctionLib::ajaxRespond(true, 'Thành công',$manufacturerData);
        //  } catch (\Throwable $th) {
        //     return FunctionLib::ajaxRespond(false, 'Lỗi');
        //  }
    }

    public function ajaxImgUpload(UploadImgManufacturerRequest $request){
        if ($request->hasFile('Filedata')) {
            $image = $request->file('Filedata');
        if ($image->isValid()) {
            $title = basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension());
            $fname = Apiato::call('File@UploadImageAction', [$request, 'Filedata', $title, StringLib::getClassNameFromString(Manufacturer::class)]);
            if (!empty($fname) && !$fname['error']) {
                    $link= \ImageURL::getImageUrl(@$fname['fileName'], 'manufacturer', 'largex2');
                    $fname['link']=$link;
                    return FunctionLib::ajaxRespond(true, 'ok',$fname);
            }
            return FunctionLib::ajaxRespond(false, 'Upload ảnh thất bại!');
        }
        return FunctionLib::ajaxRespond(false, 'File không hợp lệ!');
    }
    return FunctionLib::ajaxRespond(false, 'Không tìm thấy ảnh!');
    }

    public function ajaxDeleteManufacturer(ManufacturerDeleteRequest $request){
        if(isset($request->id)){
            $manufacturerData =  Apiato::call('Manufacturer@DeleteManufacturerByIdAction',[$request->id]);
            if(!empty($manufacturerData)){
                return FunctionLib::ajaxRespond(true, 'Thành công',$manufacturerData);
            }else{
                return FunctionLib::ajaxRespond(false, 'Không tìm thấy');
            }
        }
        return FunctionLib::ajaxRespond(false, 'Lỗi');
    }

    public function ajaxCateDataByID(ManufacturerDeleteRequest $request){
        $transporter = $request->toTransporter();
        if ($transporter->id > 0) {
         $cate = Apiato::call('Category@GetCategoryByIdAction', [Apiato::call('Manufacturer@CateIdsDirectFromManufacturerIdAction', [$transporter->id])]);
         $cateHis = [];
         foreach($cate as $key => $item){
          $cateHis[$key] = $item->category_id;
         }
         if(!empty($cate)){
              return FunctionLib::ajaxRespond(true, 'Hoàn thành', $cateHis);
          }
          return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
        }
    }
}
