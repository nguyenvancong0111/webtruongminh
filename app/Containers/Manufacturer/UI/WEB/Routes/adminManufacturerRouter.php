<?php
Route::group(
[
    'prefix' => 'manufacturer',
    'namespace' => '\App\Containers\Manufacturer\UI\WEB\Controllers',
    'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
    'middleware' => [
        'auth:admin',
    ],
],function () use ($router) {
    $router->get('/{abc}', [
      'as'   => 'admin_manufacturer_home_page',
      'uses' => 'Controller@index',
  ]);
    $router->get('/', [
      'as'   => 'admin_manufacturer_home_page',
      'uses' => 'Controller@index',
  ]);

});
