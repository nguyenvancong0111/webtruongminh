<?php

use Illuminate\Support\Facades\Route;

Route::namespace('\App\Containers\Manufacturer\UI\WEB\Controllers')->prefix('admin/ajax')->group(function () {
  Route::prefix('manufacturer')->group(function () {
    Route::any('/controller/manufacturerList', [
      'as' => 'admin.manufacturers.listManufacturers',
      'uses' => 'Controller@ajaxManufacturerList',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/manufacturerByID/{id}',[
      'as' => 'admin.manufacturers.controller.manufacturerByID',
      'uses' => 'Controller@ajaxManufacturerByID',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/{id}/manufacturerDelete',[
      'as' => 'admin.manufacturers.controller.manufacturerDelete',
      'uses' => 'Controller@ajaxDeleteManufacturer',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/{id}/getDataByID',[
      'as' => 'admin.manufacturers.controller.getDataByID',
      'uses' => 'Controller@ajaxGetDataByID',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::get('/{id}/controller/cateData', [
      'as' => 'admin.manufacturers.controller.cateData',
      'uses' => 'Controller@ajaxCateDataByID',
      'middleware' => [
          'auth:admin',
      ],
    ]);
    Route::POST('/controller/{id}/manufacturerSave',[
      'as' => 'admin.manufacturers.controller.manufacturerSave',
      'uses' => 'Controller@ajaxSaveManufacturer',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::POST('/controller/imgUpload',[
      'as' => 'admin.manufacturers.controller.imgUpload',
      'uses' => 'Controller@ajaxImgUpload',
       'middleware' => [
        'auth:admin',
      ]
    ]);
  });
});
