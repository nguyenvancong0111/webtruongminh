<?php

namespace App\Containers\Manufacturer\UI\API\Controllers;

use App\Containers\Manufacturer\UI\API\Requests\CreateManufacturerRequest;
use App\Containers\Manufacturer\UI\API\Requests\DeleteManufacturerRequest;
use App\Containers\Manufacturer\UI\API\Requests\GetAllManufacturersRequest;
use App\Containers\Manufacturer\UI\API\Requests\FindManufacturerByIdRequest;
use App\Containers\Manufacturer\UI\API\Requests\UpdateManufacturerRequest;
use App\Containers\Manufacturer\UI\API\Transformers\ManufacturerTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Manufacturer\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateManufacturerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createManufacturer(CreateManufacturerRequest $request)
    {
        $manufacturer = Apiato::call('Manufacturer@CreateManufacturerAction', [$request]);

        return $this->created($this->transform($manufacturer, ManufacturerTransformer::class));
    }

    /**
     * @param FindManufacturerByIdRequest $request
     * @return array
     */
    public function findManufacturerById(FindManufacturerByIdRequest $request)
    {
        $manufacturer = Apiato::call('Manufacturer@FindManufacturerByIdAction', [$request]);

        return $this->transform($manufacturer, ManufacturerTransformer::class);
    }

    /**
     * @param GetAllManufacturersRequest $request
     * @return array
     */
    public function getAllManufacturers(GetAllManufacturersRequest $request)
    {
        $manufacturers = Apiato::call('Manufacturer@GetAllManufacturersAction', [$request]);

        return $this->transform($manufacturers, ManufacturerTransformer::class);
    }

    /**
     * @param UpdateManufacturerRequest $request
     * @return array
     */
    public function updateManufacturer(UpdateManufacturerRequest $request)
    {
        $manufacturer = Apiato::call('Manufacturer@UpdateManufacturerAction', [$request]);

        return $this->transform($manufacturer, ManufacturerTransformer::class);
    }

    /**
     * @param DeleteManufacturerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteManufacturer(DeleteManufacturerRequest $request)
    {
        Apiato::call('Manufacturer@DeleteManufacturerAction', [$request]);

        return $this->noContent();
    }
}
