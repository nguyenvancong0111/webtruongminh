<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-24 09:54:00
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-09 18:04:13
 * @ Description: Happy Coding!
 */

namespace App\Containers\Manufacturer\UI\API\Transformers\FrontEnd;

use Apiato\Core\Foundation\ImageURL;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Manufacturer\Models\Manufacturer;
use App\Ship\Parents\Transformers\Transformer;

class ManufacturerTransformer extends Transformer
{
    protected $defaultIncludes = [];

    protected $availableIncludes = [];

    public function transform(Manufacturer $entity)
    {
        $response = [
            'name' => $entity->name,
            'manufacturer_id' => $entity->manufacturer_id,
            'image' => ImageURL::getImageUrl($entity->image,'manufacturer','large'),
            'link' => routeFrontEndFromOthers('web_manufacturer_page',['slug' => StringLib::slug($entity->name),'manufacturer_id' => $entity->manufacturer_id])
        ];

        return $response;
    }
}
