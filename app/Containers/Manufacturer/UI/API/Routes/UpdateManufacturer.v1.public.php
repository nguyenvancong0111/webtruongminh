<?php

/**
 * @apiGroup           Manufacturer
 * @apiName            updateManufacturer
 *
 * @api                {PATCH} /v1/manufacturers/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('manufacturers/{id}', [
    'as' => 'api_manufacturer_update_manufacturer',
    'uses'  => 'Controller@updateManufacturer',
    'middleware' => [
      'auth:api',
    ],
]);
