<?php

/**
 * @apiGroup           Manufacturer
 * @apiName            findManufacturerById
 *
 * @api                {GET} /v1/manufacturers/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('manufacturers/{id}', [
    'as' => 'api_manufacturer_find_manufacturer_by_id',
    'uses'  => 'Controller@findManufacturerById',
    'middleware' => [
      'auth:api',
    ],
]);
