<?php

namespace App\Containers\Manufacturer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Manufacturer\Tasks\GetManufacturerByIdTask;

class GetManufacturerByIdAction extends Action
{
    public function run($manufacturer_id)
    {
      $manufacturer = app(GetManufacturerByIdTask::class)->run($manufacturer_id);
        return $manufacturer;
    }
}
