<?php

namespace App\Containers\Manufacturer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteManufacturerByIdAction extends Action
{
    public function run($id)
    {
      $this->clearCache();
      return Apiato::call('Manufacturer@DeleteManufacturerByIdTask', [$id]);
    }
}
