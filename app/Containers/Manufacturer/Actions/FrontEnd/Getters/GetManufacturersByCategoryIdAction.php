<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-11-12 18:27:06
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-12 18:44:24
 * @ Description: Happy Coding!
 */

namespace App\Containers\Manufacturer\Actions\FrontEnd\Getters;

use App\Containers\Localization\Models\Language;
use App\Containers\Manufacturer\Tasks\Getters\GetManufacturersByCategoryIdTask;
use App\Ship\Parents\Actions\Action;

class GetManufacturersByCategoryIdAction extends Action
{
    protected $getManufacturersByCategoryIdTask;
    
    public function __construct(GetManufacturersByCategoryIdTask $getManufacturersByCategoryIdTask)
    {
        $this->getManufacturersByCategoryIdTask = $getManufacturersByCategoryIdTask;
        parent::__construct();
    }

    public function run($categoryIds, Language $currentLang = null)
    {
        return $this->remember(function () use ($categoryIds,$currentLang) {
            return $this->getManufacturersByCategoryIdTask->currentLang($currentLang)->categoryIds($categoryIds)->run();
        }, null, [], 0, $this->skipCache);
    }
}
