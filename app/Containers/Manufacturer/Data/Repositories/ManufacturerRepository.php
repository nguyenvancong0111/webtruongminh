<?php

namespace App\Containers\Manufacturer\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ManufacturerRepository
 */
class ManufacturerRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
