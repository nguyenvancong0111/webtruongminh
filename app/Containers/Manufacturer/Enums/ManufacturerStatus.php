<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-12 17:33:23
 * @ Description: Happy Coding!
 */

namespace App\Containers\Manufacturer\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class ManufacturerStatus extends BaseEnum
{
}
