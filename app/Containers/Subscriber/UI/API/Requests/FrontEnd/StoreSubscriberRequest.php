<?php


namespace App\Containers\Subscriber\UI\API\Requests\FrontEnd;

use App\Ship\Parents\Requests\Request;
use App\Containers\Subscriber\UI\API\Rules\StoreSubscriberRule;

class StoreSubscriberRequest extends Request
{
    protected $access = [
        'permissions' => '',
        'roles' => '',
    ];

    protected $decode = [];

    protected $urlParameters = [];

    public function rules()
    {
        return [
            'email' => ['required', 'unique:subscribers', 'max:191', new StoreSubscriberRule()],
        ];
    }

    public function messages()
    {
        return [
            'email.required' => __('email_required'),
            'email.unique' => __('email_unique'),
        ];
    }

    public function authorize()
    {
        return true;
    }
}
