<?php
/** @var Route $router */
$router->group([
  'prefix' => 'subscriber',
  'namespace' => '\App\Containers\Subscriber\UI\WEB\Controllers\Admin',
  'middleware' => [
    'auth:admin'
  ],
  'domain' => sprintf('admin.%s', parse_url(config('app.url'))['host'])
], function () use ($router) {
  $router->get('/', [
    'as' => 'admin.subscriber.index',
    'uses'  => 'SubscriberController@index'
  ]);

  $router->delete('/{id}', [
    'as' => 'admin.subscriber.delete',
    'uses'  => 'SubscriberController@delete'
  ]);
});



