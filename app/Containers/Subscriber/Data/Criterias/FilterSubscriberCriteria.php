<?php

namespace App\Containers\Subscriber\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Carbon\Carbon;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class FilterSubscriberCriteria extends Criteria
{
    private $queryParams;

    public function __construct(array $queryParams=[])
    {
        $this->queryParams = $queryParams;
    }
    
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        if (!empty($this->queryParams['keyword'])) { 
            $keyword = trim($this->queryParams['keyword']);
            $model = $model->where('email', 'LIKE', '%'.$keyword.'%');
        }

        if (!empty($this->queryParams['start_date'])) {
            $createdAtFormat = Carbon::createFromFormat('d/m/Y', $this->queryParams['start_date'])
                                     ->format('Y-m-d');
            $model = $model->whereDate('created_at', '>=', $createdAtFormat);
        }
        if (!empty($this->queryParams['end_date'])) {
            $createdAtFormat = Carbon::createFromFormat('d/m/Y', $this->queryParams['end_date'])
                                     ->format('Y-m-d');
            $model = $model->whereDate('created_at', '<=', $createdAtFormat);
        }
        return $model;
    }
}
