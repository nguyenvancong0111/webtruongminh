<?php

namespace App\Containers\Subscriber\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class SubscriberRepository
 */
class SubscriberRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
