<?php

namespace App\Containers\Subscriber\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindSubscriberByIdAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Subscriber@FindSubscriberByIdTask', [$request->id]);
    }
}
