<?php

namespace App\Containers\Subscriber\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Subscriber\Models\Subscriber;
use Illuminate\Support\Facades\Cookie;

class CreateSubscriberAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->all();

        $subscriber = Apiato::call('Subscriber@CreateSubscriberTask', [$data]);
        return $subscriber;
    }
}
