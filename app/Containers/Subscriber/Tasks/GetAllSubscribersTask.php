<?php

namespace App\Containers\Subscriber\Tasks;

use App\Containers\Subscriber\Data\Criterias\FilterSubscriberCriteria;
use App\Containers\Subscriber\Data\Repositories\SubscriberRepository;
use App\Ship\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;
use App\Ship\Parents\Tasks\Task;

class GetAllSubscribersTask extends Task
{

    protected $repository;

    public function __construct(SubscriberRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        $this->repository->pushCriteria(new OrderByCreationDateDescendingCriteria());
        return $this->repository->paginate(20);
    }

    public function filterSubscribers($request)
    {
        $this->repository->pushCriteria(new FilterSubscriberCriteria($request->all()));
    }
}
