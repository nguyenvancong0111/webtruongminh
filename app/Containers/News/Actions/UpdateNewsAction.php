<?php

namespace App\Containers\News\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class UpdateServiceAction.
 *
 */
class UpdateNewsAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $news = Apiato::call(
            'News@SaveNewsTask',
            [
                $data
            ]
        );

        if($news) {
            $original_desc = Apiato::call('News@GetAllNewsDescTask', [$news->id]);
            Apiato::call('News@SaveNewsDescTask', [
                $data,
                $original_desc,
                $news->id
            ]);
            Apiato::call('News@SaveNewsCategoriesTask',[$data,$news]);
        }

        $this->clearCache();

        return $news;
    }
}
