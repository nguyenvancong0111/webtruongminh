<?php

namespace App\Containers\News\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateViewItemAction extends Action
{
    public function run(int $id, array $data)
    {
        $data = Apiato::call('News@UpdateViewItemTask', [$id, $data]);
        return $data;
    }
}
