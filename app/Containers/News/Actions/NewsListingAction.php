<?php

namespace App\Containers\News\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class ServiceListingAction.
 *
 */
class NewsListingAction extends Action
{

    /**
     * @return mixed
     */
    public function run(array $filters,$orderBy = ['created_at' => 'desc','id' => 'desc'], $limit = 10, $noPaginage = true,$language_id, $external_data = [],$current_page)
    {
        return $this->remember(function () use($filters, $orderBy, $limit, $noPaginage, $language_id, $external_data,$current_page)  {
            
            $searchConds = [];
            
            $data = Apiato::call(
                'News@NewsListingTask',
                [
                    $searchConds,
                    $orderBy,
                    $limit,
                    $noPaginage,
                    Apiato::call('Localization@GetDefaultLanguageTask'),
                    $external_data,
                    $filters['page'] ?? 1
                ]
            );

            return $data;

        },null,[4]);
    }
}
