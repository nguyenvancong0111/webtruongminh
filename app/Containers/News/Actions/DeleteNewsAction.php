<?php

namespace App\Containers\News\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class DeleteServiceAction.
 *
 */
class DeleteNewsAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        Apiato::call('News@DeleteNewsTask', [$data->id]);
        // Apiato::call('News@DeleteNewsDescTask', [$data->id]);

        $this->clearCache();
    }
}
