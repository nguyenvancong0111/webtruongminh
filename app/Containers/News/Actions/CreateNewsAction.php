<?php

namespace App\Containers\News\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class CreateNewsAction.
 *
 */
class CreateNewsAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $news = Apiato::call(
            'News@CreateNewsTask',
            [
                $data
            ]
        );

        if ($news) {
            Apiato::call('News@SaveNewsDescTask', [
                $data,
                [],
                $news->id
            ]);
            Apiato::call('News@SaveNewsCategoriesTask',[$data,$news]);
        }

        $this->clearCache();

        return $news;
    }
}
