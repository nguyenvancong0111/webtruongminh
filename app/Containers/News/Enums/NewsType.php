<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-04 22:57:21
 * @ Description: Happy Coding!
 */

namespace App\Containers\News\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class NewsType extends BaseEnum
{
    /**
     * Tin thường
     */
    const NORMAL = 'normal';
    
    /**
     * Tin khuyến mại
     */
    const PROMOTION ='promotion' ;

    /**
     * Tin sự kiện
     */
    const EVENT = 'event';

    

    const TEXT = [
        self::NORMAL => 'Tin thường',
        self::PROMOTION => 'Khuyến mãi',
        self::EVENT => 'Sự kiện',
    ];
}
