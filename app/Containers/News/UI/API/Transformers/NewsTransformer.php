<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-07 13:09:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-07 13:10:14
 * @ Description: Happy Coding!
 */

namespace App\Containers\News\UI\API\Transformers;

use App\Containers\News\Models\News;
use App\Ship\Parents\Transformers\Transformer;

class NewsTransformer extends Transformer
{
    /**
     * @param $token
     *
     * @return  array
     */
    public function transform(News $entity)
    {
        $response = [
            'id' => $entity->id,
            'desc' => $entity->relationLoaded('desc') ? $entity->desc : $this->null(),
            'image' => $entity->getImageUrl(),
            'views' => $entity->views,
            'image_medium' => $entity->getImageUrl('medium'),
            'image_450x270' => $entity->getImageUrl('450x270'),
            'link' => $entity->link(),
            'created_at' => $entity->created_at->format('d/m/Y'),
            'created' => $entity->buildDate(),
            'start_at' => $entity->start_at ? $entity->start_at->format('d/m/Y') : '',
            'end_at' => $entity->end_at ? $entity->end_at->format('d/m/Y') : '',
            'end_at_countdown' => $entity->end_at ? $entity->end_at->format('m/d/Y') : '',
            'cate_desc' => $entity->relationLoaded('categories') ? $entity->categories : $this->null(),
            'cateName' => $entity->generateCatName(),
        ];
        return $response;
    }
}
