<?php

namespace App\Containers\News\UI\API\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\FunctionLib;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use App\Containers\News\Actions\FrontEnd\GetNewsListAction;
use App\Containers\News\UI\API\Requests\GetNewsRequest;
use App\Containers\News\UI\API\Requests\UpdateNewsRequest;
use App\Containers\News\UI\API\Transformers\NewsTransformer;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Response;

/**
 * Class Controller.
 */
class Controller extends BaseApiFrontController
{
    public function updateNews(UpdateNewsRequest $request) {
        $tranporter = new DataTransporter($request);
        $news = Apiato::call('News@UpdateNewsAction', [$tranporter]);

        if ($news) {
            return FunctionLib::ajaxRespondV2(true,'Success');
        }
        return FunctionLib::ajaxRespondV2(false, 'Fail',[],Response::HTTP_NOT_ACCEPTABLE);
    }

    public function getNews(GetNewsRequest $request, GetNewsListAction $getNewsListAction){
        $news = $getNewsListAction->run($request->all(), ['sort_order' => 'asc', 'id' => 'desc'], $request->get('limit', 3), $request->get('skipPagination', false), $this->currentLang, ['categories', 'categories.desc']);
        return $this->transform($news, new NewsTransformer());
    }
    public function getNewsIntroduce(GetNewsRequest $request){
        $news = app(GetNewsListAction::class)->skipCache(true)->run(
            ['status' => 2], ['id' => 'desc', 'sort_order' => 'asc'], 4, true, $this->currentLang, []
        );
        return $this->transform($news, new NewsTransformer());
    }

    public function getNewsHome(GetNewsRequest $request){
        $news = app(GetNewsListAction::class)->skipCache(true)->run(
            ['status' => 2, 'is_home' => 2], ['sort_order' => 'asc', 'id' => 'desc'], 3, true, $this->currentLang, []
        );
        return $this->transform($news, new NewsTransformer());
    }

    public function getNewsLoadMore(GetNewsRequest $request){
        $news = app(GetNewsListAction::class)->skipCache(true)->run(
            ['status' => 2, 'reject_id' => isset($request->reject) && !empty($request->reject) ? explode(',', $request->reject) : []], ['sort_order' => 'asc', 'id' => 'desc'], 12, false, $this->currentLang, [], isset($request->page) && !empty($request->page) ? $request->page : 1
        );
        return $this->transform($news, new NewsTransformer());

    }
    public function getNewsRelated(GetNewsRequest $request){
        $news = app(GetNewsListAction::class)->skipCache(true)->run(
            ['status' => 2, 'reject_id' => isset($request->reject) && !empty($request->reject) ? [(int)$request->reject] : []], ['sort_order' => 'asc', 'id' => 'desc'], 4, true, $this->currentLang, [], 1
        );
        return $this->transform($news, new NewsTransformer());
    }
    public function getNewsViews(GetNewsRequest $request){
        $news = app(GetNewsListAction::class)->skipCache(true)->run(
            ['status' => 2, 'reject_id' => isset($request->reject) && !empty($request->reject) ? [(int)$request->reject] : []], ['views' => 'desc', 'id' => 'desc'], 6, true, $this->currentLang, [], 1
        );

        return $this->transform($news, new NewsTransformer());
    }


}
