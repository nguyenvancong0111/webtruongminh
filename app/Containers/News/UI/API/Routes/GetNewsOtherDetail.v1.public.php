<?php

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

$router->get('news-views', [
    'as' => 'api_news_views',
    'uses'       => 'Controller@getNewsViews',
    'prefix' => app(CheckSegmentLanguageAction::class)->run(),
    'middleware' => [
        'api',
    ],
]);
$router->get('news-related', [
    'as' => 'api_news_related',
    'uses'       => 'Controller@getNewsRelated',
    'prefix' => app(CheckSegmentLanguageAction::class)->run(),
    'middleware' => [
        'api',
    ],
]);
