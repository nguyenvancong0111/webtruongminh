<?php

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

$router->get('news-home', [
    'as' => 'api_news_list_home',
    'uses'       => 'Controller@getNewsHome',
    'prefix' => app(CheckSegmentLanguageAction::class)->run(),
    'middleware' => [
        'api',
    ],
]);
