<?php

namespace App\Containers\News\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\StringLib;
use App\Containers\News\Models\News;
use App\Containers\News\UI\WEB\Controllers\Admin\Features\UpdateSomeStatus;
use App\Containers\News\UI\WEB\Requests\Admin\FindNewsRequest;
use App\Containers\News\UI\WEB\Requests\CreateNewsRequest;
use App\Containers\News\UI\WEB\Requests\EditNewsRequest;
use App\Containers\News\UI\WEB\Requests\StoreNewsRequest;
use App\Containers\News\UI\WEB\Requests\UpdateNewsRequest;
use App\Containers\News\UI\WEB\Requests\GetAllNewsRequest;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Request;
use App\Containers\News\Enums\NewsType;

class Controller extends AdminController
{
    use UpdateSomeStatus;

    public function __construct()
    {
        $this->title = 'Nhân Viên';
        parent::__construct();
    }
    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllNewsRequest $request)
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Nhân Viên', $this->form == 'list' ? '' : route('admin_news_home_page'));
        \View::share('breadcrumb', $this->breadcrumb);
// dd(1);
        $news = Apiato::call('News@NewsListingAction', [$request->all(),['created_at' => 'desc'], $this->perPage, false,Apiato::call('Localization@GetDefaultLanguageAction'),[],$request->page ?? 1]);

        return view('news::admin.index', [
            'search_data' => $request,
            'data' => $news,
            'typeOptions'=> NewsType::TEXT,
            'categories' => Apiato::call('Category@Admin\GetAllCategoriesAction', [['cate_type' => 2]])
        ]);
    }

    public function edit(Request $request)
    {
        $this->showEditForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Nhân Viên', $this->form == 'list' ? '' : route('admin_news_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Chỉnh sửa thông tin');
        \View::share('breadcrumb', $this->breadcrumb);

        $news = Apiato::call('News@Admin\FindNewsByIdAction', [$request->id, ['activeCategories', 'activeCategories.desc']]);

        return view('news::admin.edit', [
            'data' => $news,
            'typeOptions'=> NewsType::TEXT
        ]);

        return $this->notfound($request->id);
    }

    public function add(Request $request)
    {
        $this->showAddForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Nhân Viên', $this->form == 'list' ? '' : route('admin_news_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Thêm mới');
        \View::share('breadcrumb', $this->breadcrumb);
        return view('news::admin.edit');
    }

    public function update(UpdateNewsRequest $request)
    {

        try {
            $tranporter = new DataTransporter($request);

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'news', StringLib::getClassNameFromString(News::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }

            $news = Apiato::call('News@UpdateNewsAction', [$tranporter]);

            if ($news) {
                return redirect()->route('admin_news_edit_page', ['id' => $news->id])->with('status', 'Nhân Viên đã được cập nhật');
            }
        } catch (\Exception $e) {
            throw $e;
            if (!$image['error'])
              return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
            else
              return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . $image['msg']]);
        }
    }

    public function create(StoreNewsRequest $request)
    {
        try {
            $tranporter = new DataTransporter($request);

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'news', StringLib::getClassNameFromString(News::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }

            $news = Apiato::call('News@CreateNewsAction', [$tranporter]);
            if ($news) {
                return redirect()->route('admin_news_home_page')->with('status', 'Nhân Viên đã được thêm mới');
            }
        } catch (\Exception $e) {
          if (!isset($image['error']))
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
          else
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . $image['msg']]);
        }
    }

    public function delete(FindNewsRequest $request)
    {
        try {
            Apiato::call('News@DeleteNewsAction', [$request]);
        } catch (\Exception $e) {
            // throw $e;
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
        }
    }


}
