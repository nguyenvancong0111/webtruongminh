<?php

namespace App\Containers\News\UI\WEB\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class UpdateNewsRequest.
 *
 */
class UpdateNewsRequest extends Request
{

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles'       => 'admin',
        'permissions' => 'edit-news',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'id'           => 'required|exists:news,id',
            'news_description.*.name' => 'required',
            'image' => 'image|mimes:jpeg,jpg,png,gif',
            'news_category' => 'required'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required'  => 'ID không tồn tại',
            'id.exists'  => 'ID không tồn tại',
            'news_category.required' => 'Danh mục không được bỏ trống',
            'news_description.1.name.required' => 'Tên không được bỏ trống',
            'news_description.2.name.required' => 'Tên Tiếng Anh không được bỏ trống',
            'news_description.3.name.required' => 'Tên Tiếng Trung không được bỏ trống',
            'image.image' => 'Ảnh đại diện không đúng định dạng (jpeg, jpg, png, gif)',
            'image.mimes' => 'Ảnh đại diện không đúng định dạng (jpeg, jpg, png, gif)',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
