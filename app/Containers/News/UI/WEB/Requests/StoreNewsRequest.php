<?php

namespace App\Containers\News\UI\WEB\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateNewsRequest.
 *
 */
class StoreNewsRequest extends Request
{

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles'       => 'admin',
        'permissions' => 'create-news',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            // 'id'           => 'required|exists:news,id',
//            'news_category' => 'required'
            'news_description*name' => 'required',
            'image' => 'bail|required|image|mimes:jpeg,jpg,png,gif',
        ];

    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
//            'id.required'  => 'ID không tồn tại',
//            'id.exists'  => 'ID không tồn tại',
//            'news_category.required' => 'Danh mục không được bỏ trống'
            'news_description.1.name' => 'Tên Tiếng Việt không được bỏ trống',
            'news_description.2.name' => 'Tên Tiếng Anh không được bỏ trống',
            'news_description.3.name' => 'Tên Tiếng Trung không được bỏ trống',
            'image.required' => 'Vui lòng Upload ảnh đại diện',
            'image.mimes' => 'Ảnh đại diện không đúng định dạng (jpeg, jpg, png, gif)',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
