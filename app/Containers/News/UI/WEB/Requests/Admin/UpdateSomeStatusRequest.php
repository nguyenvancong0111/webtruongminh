<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 23:23:36
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-01 00:43:06
 * @ Description: Happy Coding!
 */

namespace App\Containers\News\UI\WEB\Requests\Admin;

use App\Ship\Parents\Requests\Request;

class UpdateSomeStatusRequest extends Request
{

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => 'edit-news',
        'roles'       => 'admin',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'field'
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'id'           => 'required|exists:news,id',
            'field' => ['required'],
            'status' => ['required','numeric'],
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required'  => 'ID không được trống',
            'id.exists'  => 'ID không tồn tại',
            'field.required' => 'Tên trường cần update không được thiếu',
            'status.required' => 'Trạng thái không thể trống',
            'status.numeric' => 'Trạng thái chỉ có thể là chữ số',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
