<div class="tab-pane" id="image">
    <div class="tabbable">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="image">Ảnh đại diện</label>
                    <input type="file" id="image" name="image" class="dropify form-control {{ $errors->has('image') ? ' is-invalid' : '' }}" data-show-remove="false" data-default-file="{{ \ImageURL::getImageUrl(old('image',@$data['image']), 'news', 'original')    }}">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="image">Ảnh album</label>
                    <input type="file" id="image" name="image[]" multiple class="dropify form-control {{ $errors->has('image') ? ' is-invalid' : '' }}" data-show-remove="false" data-default-file="{{ \ImageURL::getImageUrl(old('image',@$data['image']), 'news', 'original')    }}">
                </div>
            </div>
        </div>
    </div>
</div>