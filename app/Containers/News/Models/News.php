<?php

namespace App\Containers\News\Models;

use Apiato\Core\Foundation\Facades\ImageURL;
use App\Containers\Category\Enums\CategoryStatus;
use App\Containers\Category\Models\Category;
use App\Ship\Parents\Models\Model;
use Illuminate\Support\Str;

class News extends Model
{
    protected $table = 'news';

    protected $appends = ['format_date'];

    protected $fillable = [
        'status',
        'image',
        'sort_order',
        'author',
        'author_updated',
        'type',
        'created_at',
        'is_home',
        'views'
    ];

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'cat_id');
    }

    public function getImageUrl($size = 'original')
    {
        return ImageURL::getImageUrl($this->image, 'news', $size);
    }

    public function desc()
    {
        return $this->hasOne(NewsDesc::class, 'news_id', 'id');
    }

    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', NewsDesc::class, 'news_id', 'id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, NewsCategory::getTableName(), 'news_id', 'category_id')->where('category.status', '!=', CategoryStatus::DELETE);
    }

    public function activeCategories()
    {
        return $this->belongsToMany(Category::class, NewsCategory::getTableName(), 'news_id', 'category_id')->where('category.status', '!=', CategoryStatus::DELETE);
    }

    public function getFormatDateAttribute()
    {
        return ($this->created_at) ? $this->created_at->format('d.m.Y') : null;
    }

    public function link()
    {
        $slug = !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name);
        if (!empty($slug)) {
            return route('web.news.detail', ['slug_news' => $slug, 'id' => $this->id]);
        } else {
            return 'javascript:;';
        }
    }
    public function generateCatName(){
        if($this->relationLoaded('categories')){
            return implode(', ', $this->categories->map(function($item) {
                return $item->desc->name;
            })->toArray());
        }
        return null;
    }

    public function buildDate(){
        $return['day'] = $this->created_at->format('d');
        $return['month'] = $this->created_at->format('m');
        $return['year'] = $this->created_at->format('Y');
        return $return;
    }


}

