<?php

namespace App\Containers\News\Tasks;

use App\Containers\News\Data\Repositories\NewsRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class SaveNewsTask.
 */
class SaveNewsTask extends Task
{

    protected $repository;

    public function __construct(NewsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data->toArray(), ['news_description', 'news_category', '_token', '_headers']);
            $dataUpdate['author_updated'] = auth()->guard('admin')->user()->name;
            $news = $this->repository->update($dataUpdate, $data->id);

            return $news;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
