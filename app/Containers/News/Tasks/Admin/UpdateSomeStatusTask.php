<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 23:26:46
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-01 00:34:25
 * @ Description: Happy Coding!
 */

namespace App\Containers\News\Tasks\Admin;

use App\Containers\News\Data\Repositories\NewsRepository;
use App\Ship\core\Traits\HelpersTraits\UpdateDynamicStatusTrait;
use App\Ship\Parents\Tasks\Task;

class UpdateSomeStatusTask extends Task
{
    use UpdateDynamicStatusTrait;

    protected $repository;

    public function __construct(NewsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(string $field,int $id, int $status) :? bool
    {
        return $this->updateDynamicStatus($field,$id,$status);
    }
}
