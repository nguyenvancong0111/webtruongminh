<?php

namespace App\Containers\News\Tasks;

use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\News\Data\Repositories\NewsRepository;
use App\Containers\News\Enums\NewsStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class NewsListingTask.
 */
class NewsListingTask extends Task
{

    protected $repository;

    public function __construct(NewsRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($filters = [], $orderBy = ['created_at' => 'desc', 'id' => 'desc'], $limit = 20, $noPaginate = true, $defaultLanguage = null, $external_data = [], $current_page = 1)
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;

       
        $this->repository->with(['desc' => function ($query) use ($language_id) {
            $query->select('id', 'news_id', 'language_id', 'name', 'short_description');
            $query->activeLang($language_id);
        }]);
        

        foreach ($orderBy as $column => $direction) {
            $this->repository->orderBy($column, $direction);
        }

        // \DB::enableQueryLog();
        return $noPaginate ? $this->repository->limit($limit) :  $this->repository->paginate($limit);
        // dd(\DB::getQueryLog());
    }
}
