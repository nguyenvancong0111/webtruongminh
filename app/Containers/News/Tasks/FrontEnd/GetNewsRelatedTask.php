<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 01:33:06
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-05 15:52:43
 * @ Description: Happy Coding!
 */

namespace App\Containers\News\Tasks\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\News\Data\Repositories\NewsRepository;
use App\Containers\News\Enums\NewsStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisInThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

class GetNewsRelatedTask extends Task
{

    protected $repository;

    public function __construct(NewsRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run(array $category = [], array $filters = [], int $limit = 20, bool $isPanination = false, Language $currentLang = null, array $orderBy = ['created_at' => 'desc', 'id' => 'desc'], int $current_page = 1): iterable {
        $language_id = $currentLang ? $currentLang->language_id : 1;

        $this->repository->pushCriteria(new ThisEqualThatCriteria('status', NewsStatus::ACTIVE));

//        if(!empty($type)){
//            $this->repository->pushCriteria(new ThisEqualThatCriteria('type', $type));
//        }
//        else{
//            $this->repository->pushCriteria(new ThisInThatCriteria('type',[ServiceType::PROMOTION,ServiceType::EVENT] ));
//        }
        $this->repository->with(['desc' => function ($query) use ($language_id) {
            $query->select('id', 'news_id', 'language_id', 'name', 'short_description', 'slug');
            $query->activeLang($language_id);
        }, 'categories', 'categories.desc' => function($q) use ($language_id){
            $q->select('category_id', 'language_id', 'name', 'slug');
            $q->activeLang($language_id);
        }]);


        if (!empty($category)){
            $this->repository->whereHas('categories', function (Builder $q) use ($category) {
                $q->whereIn('category.category_id', $category);
            });
        }

        if (!empty($filters['arr_id_skip'])){
            $this->repository->whereNotIn('id',$filters['arr_id_skip'] );
        }

//        $this->repository->whereRaw("(DATEDIFF('".Carbon::now()."',created_at)<30)");

        foreach ($orderBy as $column => $direction) {
            $this->repository->orderBy($column, $direction);
        }
//         \DB::enableQueryLog();

        return $isPanination ?  $this->repository->paginate($limit): $this->repository->limit($limit);
        // dd(\DB::getQueryLog());
    }
}
