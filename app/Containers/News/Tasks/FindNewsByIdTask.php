<?php

namespace App\Containers\News\Tasks;

use App\Containers\News\Data\Repositories\NewsRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class FindNewsByIdTask.
 */
class FindNewsByIdTask extends Task
{

    protected $repository;

    public function __construct(NewsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($news_id,$defaultLanguage = null, $external_data = ['with_relationship' => ['all_desc']])
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;

        $data = $this->repository->with(array_merge($external_data['with_relationship'],[ 'categories', 'categories.desc' => function ($query) use($language_id) {
            $query->activeLang($language_id);
        }]))->find($news_id);

        return $data;
    }
}