<?php

namespace App\Containers\News\Tasks;

use Apiato\Core\Foundation\Facades\StringLib;
use App\Containers\News\Data\Repositories\NewsDescRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SaveNewsDescTask.
 */
class SaveNewsDescTask extends Task
{

    protected $repository;

    public function __construct(NewsDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data, $original_desc, $news_id, $edit_id = null)
    {
        $news_description = isset($data['news_description']) ? (array)$data['news_description'] : null;
        if (is_array($news_description) && !empty($news_description)) {
            $updates = [];
            $inserts = [];
            foreach ($news_description as $k => $v) {
                if (isset($original_desc[$k])) {
                    $updates[$original_desc[$k]['id']] = [
                        'name' => $v['name'],
                        'slug' => isset($v['slug']) && $v['slug'] != '' ? StringLib::slug($v['slug']) : ( $k == 3 ? StringLib::slug($v['name'], '-', 'cn') : StringLib::slug($v['name'])),
                        'short_description' => $v['short_description'],
                        'description' => $v['description'],
                        'meta_title' => $v['meta_title'],
                        'meta_description' => $v['meta_description'],
                        'meta_keyword' => $v['meta_keyword'],
                    ];
                } else {
                    $inserts[] = [
                        'news_id' => $news_id,
                        'language_id' => $k,
                        'name' => $v['name'],
                        'slug' => isset($v['slug']) && $v['slug'] != '' ? StringLib::slug($v['slug']) : ( $k == 3 ? StringLib::slug($v['name'], '-', 'cn') : StringLib::slug($v['name'])),
                        'short_description' => $v['short_description'],
                        'description' => $v['description'],
                        'meta_title' => $v['meta_title'],
                        'meta_description' => $v['meta_description'],
                        'meta_keyword' => $v['meta_keyword'],
                    ];
                }
            }
            if (!empty($updates)) {
                $this->repository->updateMultiple($updates);
            }

            if (!empty($inserts)) {
                $this->repository->getModel()->insert($inserts);
            }
        }
    }

    public function to_slug($string, $separator = '-') {
        $re = "/(\\s|\\".$separator.")+/mu";
        $str = @trim($string);
        $subst = $separator;
        $result = preg_replace($re, $subst, $str);

        return $result;
    }
}
