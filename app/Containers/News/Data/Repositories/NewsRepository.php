<?php

namespace App\Containers\News\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class NewsRepository.
 */
class NewsRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'News';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'status',
        'published',
        'views',
        'created_at',
        'desc.name',
        'desc.language_id'
    ];
}
