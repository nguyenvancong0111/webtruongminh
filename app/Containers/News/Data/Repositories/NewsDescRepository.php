<?php

namespace App\Containers\News\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ServiceDescRepositoryDescRepository.
 */
class NewsDescRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'News';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'news_id',
        'language_id',
        'name',
        'tag',
    ];
}
