<?php

namespace App\Containers\Log\Models;

use App\Ship\Parents\Models\Model;

class ApiLog extends Model
{
    protected $table = 'api_log';

    protected $guarded = [];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [
        'current_object_authen' => 'array',
        'request' => 'array',
        'response' => 'array'
    ];  

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'apilogs';
}
