<?php

namespace App\Containers\Log\Tasks;

use App\Containers\Log\Data\Repositories\ApiLogRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class StoreApiLogTask extends Task
{

    protected $repository;

    public function __construct(ApiLogRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
