<?php

namespace App\Containers\Log\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetApiLogsAction extends Action
{
    public function run(Request $request)
    {
        $apiLogs = Apiato::call('Log@GetApiLogsTask', [], []);
        return $apiLogs;
    }
}
