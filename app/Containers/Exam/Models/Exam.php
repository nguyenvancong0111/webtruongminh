<?php

namespace App\Containers\Exam\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Containers\Category\Models\Category;
use App\Containers\Course\Models\Course;
use App\Containers\Course\Models\CourseExam;
use App\Containers\Customer\Models\Customer;
use App\Containers\Customer\Models\CustomerExam;
use App\Containers\Questions\Models\Questions;
use App\Ship\Parents\Models\Model;
use Illuminate\Support\Str;

class Exam extends Model
{
    protected $table = 'exam';

    protected $primaryKey = 'id';

    protected $fillable = [
        'title',
        'short_description',
        'time',
        'rework',
        'point',
        'image',
        'sort_order',
        'status',
        'view_result'
    ];

    public function category()
    {
        return $this->hasOne(Category::class, 'category_id', 'category_id');
    }
    public function course()
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }
    public function exam_course(){
        return $this->belongsToMany(Course::class, CourseExam::getTableName(), 'exam_id', 'course_id');
    }
    public function exam_handing(){
        return $this->hasOne(CustomerExam::class, 'exam_id', 'id');
    }
    public function genExam()
    {
        return $this->belongsToMany(Questions::class, ExamQuestion::getTableName(), 'exam_id', 'question_id')->withPivot('exam_id', 'question_id');
    }

    public function exam_filter(){
        return $this->hasMany(ExamFilterQuestion::class, 'exam_id', 'id');
    }
    public function exam_question(){
        return $this->hasMany(ExamQuestion::class, 'exam_id', 'id');
    }

    public function getImage($size = "small")
    {
        return ImageURL::getImageUrl($this->image, 'exam', $size);
    }
    public function link()
    {
        return 'javascript:;';
    }
    public function customer()
    {
        return $this->hasOne(CustomerExam::class, 'exam_id', 'id');
    }
    // public function customer()
    // {
    //     return $this->belongsToMany(Customer::class, CustomerExam::getTableName(), 'news_id', 'category_id');
    // }
}
