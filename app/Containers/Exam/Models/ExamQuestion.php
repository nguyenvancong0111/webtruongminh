<?php

/**
 * Created by PhpStorm.
 * Filename: ExamGroup.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 8/12/20
 * Time: 14:40
 */

namespace App\Containers\Exam\Models;

use App\Containers\Questions\Enums\QuestionStatus;
use App\Containers\Questions\Models\Questions;
use App\Models\Category\CategoryExamGroup;
use App\Ship\Parents\Models\Model;

class ExamQuestion extends Model
{
    protected $table = 'exam_question';
    public $timestamp = false;
    public function getQuestion()
    {
        return $this->hasOne(Questions::class, 'id', 'question_id')
            ->where(Questions::getTableName() . '.status', QuestionStatus::ACTIVE);
    }
}
