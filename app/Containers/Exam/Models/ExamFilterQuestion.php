<?php

/**
 * Created by PhpStorm.
 * Filename: ExamGroup.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 8/12/20
 * Time: 14:40
 */

namespace App\Containers\Exam\Models;

use App\Containers\Category\Models\Category;
use App\Containers\Level\Models\Level;
use App\Containers\Questions\Enums\QuestionStatus;
use App\Containers\Questions\Models\Questions;
use App\Models\Category\CategoryExamGroup;
use App\Ship\Parents\Models\Model;

class ExamFilterQuestion extends Model
{
    protected $table = 'exam_filter_question';
    public $timestamp = false;
    public function level()
    {
        return $this->hasOne(Level::class, 'id', 'level_id');
    }
    public function category()
    {
        return $this->hasOne(Category::class, 'category_id', 'category_id');
    }
}
