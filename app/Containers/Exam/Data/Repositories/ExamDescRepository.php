<?php

namespace App\Containers\Exam\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ExamDescRepository.
 */
class ExamDescRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Exam';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'filter_id',
        'language_id',
        'filter_group_id',
        'name',
    ];
}
