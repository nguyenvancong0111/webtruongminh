<?php

namespace App\Containers\Exam\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ExamRepository.
 */
class ExamRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Exam';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'filter_id',
        'filter_group_id',
        'status',
        'created_at',
        'desc.name',
        'desc.language_id'
    ];
}
