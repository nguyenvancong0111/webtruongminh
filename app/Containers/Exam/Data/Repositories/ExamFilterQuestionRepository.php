<?php

namespace App\Containers\Exam\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ExamGroupDescRepository.
 */
class ExamFilterQuestionRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Exam';

    /**
     * @var array
     */
    
}
