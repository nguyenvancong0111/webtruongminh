<?php

namespace App\Containers\Exam\UI\API\Transformers;

use App\Ship\Parents\Transformers\Transformer;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

/**
 * Class ExamCourseCustomerTransformer.
 *
 */
class ExamCourseCustomerTransformer extends Transformer
{
    /**
     * @param $token
     *
     * @return  array
     */
    public function transform($exam)
    {
        $response = [
            'id' => $exam->id,
            'name' => $exam->title,
            'view_result' => $exam->view_result == 1 && $exam->relationLoaded('exam_handing') && $exam->exam_handing && $exam->exam_handing->times == $exam->exam_handing->rework  ? true: false,
            'link_view' => $exam->relationLoaded('exam_handing') && $exam->exam_handing  && $exam->view_result == 1 ? route('web.lecturers.view_quiz', ['slug_exam' => Str::slug($exam->title), 'id_exam' => $exam->id, 'course_id' => $exam->exam_handing->course_id, 'customer_id' => $customer_id]) : 'javacript:;',
            'point' => $exam->point,
            'image' => $exam->getImage(),
            'short_description' => $exam->short_description,
            'score' => $exam->relationLoaded('exam_handing') && $exam->exam_handing ? $exam->exam_handing->score : 0,
        ];

        return $response;
    }
}
