<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-20 03:49:45
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\UI\API\Transformers;

use App\Ship\Parents\Transformers\Transformer;
use Illuminate\Support\Facades\Config;

/**
 * Class ExamTransformer.
 *
 */
class ExamTransformer extends Transformer
{

    /**
     * @param $token
     *
     * @return  array
     */
    public function transform($exam)
    {
        $response = [
            'id' => $exam->id,
            'name' =>   $exam->desc->name ?? '',
            'short_description' =>   $exam->desc->short_description,
            'point' => $exam->point,
            'score' => $exam->score,
            'image' => $exam->getImage(),
            'link' => $exam->link(),
            // 'language_id' => $exam->desc->language_id
        ];

        return $response;
    }
}
