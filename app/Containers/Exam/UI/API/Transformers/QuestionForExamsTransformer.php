<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-20 03:49:45
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\UI\API\Transformers;

use App\Containers\Questions\UI\API\Transformers\AnswerTransformer;
use App\Ship\Parents\Transformers\Transformer;
use Illuminate\Support\Facades\Config;

/**
 * Class ExamTransformer.
 *
 */
class QuestionForExamsTransformer extends Transformer
{

    protected array $defaultIncludes = [
        'answer',

    ];
    /**
     * @param $token
     *
     * @return  array
     */
    public function transform($question)
    {
        $response = [
            'question_id' => $question->question_id,
            'content' => $question->getQuestion ? $question->getQuestion->desc->content : '',
            'type_answer' =>  $question->getQuestion ? $question->getQuestion->type_answer : '',
        ];

        return $response;
    }
    public function includeAnswer($question)
    {
        if ($question->relationLoaded('getQuestion') && !empty($question->getQuestion)) {
            if (!empty($question->getQuestion->answer)) {
                return $this->collection($question->getQuestion->answer, new AnswerTransformer) ;
            }
        }
    }
}
