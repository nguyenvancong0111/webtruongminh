<?php

$router->get('exam/getExams', [
    'as' => 'api_exam_by_course_id',
    'uses'       => 'Controller@getExamsByCourseId',
    'middleware' => [
        // 'auth:api',
    ],
]);
$router->get('exam/getQuestionForExams', [
    'as' => 'api_get_question_for_exam',
    'uses'       => 'Controller@getQuestionForExams',
    'middleware' => [
        // 'auth:api',
    ],
]);
$router->post('submit-exam', [
    'as' => 'api_submit_question_for_exam',
    'uses'       => 'Controller@submitQuestionForExams',
    'middleware' => [
        // 'auth:api',
    ],
]);
$router->get('exam/getExams', [
    'as' => 'api_exam_by_customer_id',
    'uses'       => 'Controller@getExamsByCustomerId',
    'middleware' => [
        // 'auth:api',
    ],
]);
$router->get('exam/course-customer', [
    'as' => 'api_exam_by_course_customer',
    'uses'       => 'Controller@getExamsByCourseCustomer',
    'middleware' => [
        // 'auth:api',
    ],
]);
