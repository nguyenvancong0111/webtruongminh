<?php

Route::group(
    [
        'middleware' => [
             'api',
        ],
        'prefix' => '/exam',
    ],
    function () use ($router) {
        $router->any('/questions', [
            'as' => 'api_question_exam',
            'uses'       => 'Controller@getQuestionInExam'
        ]);

    });