<?php

/**
 * @apiGroup           Exam
 * @apiName            getExam
 * @api                {get} /v1/exam/getExam Get All Exam
 *
 * @apiVersion         1.0.0
 * @apiPermission      Authenticated User
 *
 * @apiUse             GeneralSuccessMultipleResponse
 */

$router->get('exam/getExam', [
    'as' => 'api_filtergroup_get_all',
    'uses' => 'Controller@getExam',
    'middleware' => [
        'auth:admin',
    ],
]);
