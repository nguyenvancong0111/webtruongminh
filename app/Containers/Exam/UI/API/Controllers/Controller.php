<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:19:15
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\UI\API\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Course\Actions\FrontEnd\FindCourseByIdAction;
use App\Containers\Course\Actions\FrontEnd\FindCustomerLessonViewAction;
use App\Containers\Course\Actions\FrontEnd\GetCourseBySlugIdAction;
use App\Containers\Course\Actions\FrontEnd\UpdateCustomerLessonViewAction;
use App\Containers\Course\Events\UpdateCustomerCoursePercentEvent;
use App\Containers\Customer\Actions\UpdateCustomerExamAction;
use App\Containers\Exam\Actions\FrontEnd\FindExamIdAction as FrontEndFindExamIdAction;
use App\Containers\Exam\Actions\FrontEnd\GetQuestionOfExamAction;
use App\Containers\Exam\Actions\GetExamsByCourseCustomerAction;
use App\Containers\Exam\Actions\GetExamsByCourseIdAction;
use App\Containers\Exam\Actions\GetExamsByCustomerIdAction;
use App\Containers\Exam\Tasks\GetExamsByCourseIdTask;
use App\Containers\Exam\UI\API\Requests\GetAllExamRequest;
use App\Containers\Exam\UI\API\Requests\GetExamByCourseIDRequest;
use App\Containers\Exam\UI\API\Requests\GetQuestionForExamRequest;
use App\Containers\Exam\UI\API\Requests\SubmitQuestionForExamRequest;
use App\Containers\Exam\UI\API\Transformers\ExamCourseCustomerTransformer;
use App\Containers\Exam\UI\API\Transformers\ExamSelect2Transformer;
use App\Containers\Exam\UI\API\Transformers\ExamTransformer;
use App\Containers\Exam\UI\API\Transformers\ExamsSelect2Transformer;
use App\Containers\Exam\UI\API\Transformers\QuestionForExamsTransformer;
use App\Containers\Questions\Actions\Admin\ListQuestionInExamAction;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Str;

class Controller extends ApiController
{
    public function getExam(GetAllExamRequest $request)
    {
        $transporter = new DataTransporter([
            'name' => $request->name
        ]);
        $filterGroups = Apiato::call('Exam@ExamListingAction', [$transporter, ['id' => 'DESC'], ['course']]);
        $type = $request->type;
        if ($type && $type == 'select2') {
            return $this->transform($filterGroups, ExamSelect2Transformer::class, [], [], 'filtergroups');
        } else {
            return $this->transform($filterGroups, ExamTransformer::class, [], [], 'filtergroups');
        }
    }
    public function getExamsByCourseId(GetExamByCourseIDRequest $request)
    {
        // dd($request->all());
        try {
            $data = app(GetExamsByCourseIdAction::class)->run($request->course_id, [], $request->limit);
            // dd($data);
            return $this->transform($data, ExamTransformer::class, [], [], '');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    public function getExamsByCustomerId(GetExamByCourseIDRequest $request)
    {
        // dd($request->all());
        try {
            $data = app(GetExamsByCustomerIdAction::class)->run($request->customer_id, $request->limit);
            // dd($data);
            return $this->transform($data, ExamTransformer::class, [], [], '');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    public function getExamsByCourseCustomer(GetExamByCourseIDRequest $request)
    {
        try {
            $data = app(GetExamsByCourseCustomerAction::class)->skipCache(true)->run(
                $request->customer_id, $request->object, ['status' => 2], 4, ['sort_order' => 'asc'], true, isset($request->page) && !empty($request->page) ? $request->page : 1
            );
            $data_return = [];
            if ($data->isNotEmpty()){
                foreach ($data as $item){
                    $data_return[] = [
                        'id' => $item->id,
                        'name' => $item->title,
                        'view_result' => $item->view_result == 1 && $item->relationLoaded('exam_handing') && $item->exam_handing && $item->exam_handing->times == $item->exam_handing->rework  ? true: false,
                        'link_view' => $item->relationLoaded('exam_handing') && $item->exam_handing  && $item->view_result == 1 ? route('web.lecturers.view_quiz', ['slug_exam' => Str::slug($item->title), 'id_exam' => $item->id, 'course_id' => $item->exam_handing->course_id, 'customer_id' => $request->customer_id]) : 'javacript:;',
                        'point' => $item->point,
                        'image' => $item->getImage(),
                        'short_description' => $item->short_description,
                        'score' => $item->relationLoaded('exam_handing') && $item->exam_handing ? $item->exam_handing->score : 0,
                    ];
                }
            }

            return \FunctionLib::ajaxRespondV2(true, 'success', $data_return);
            /*return $this->transform($data, new ExamCourseCustomerTransformer());*/

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getQuestionInExam(GetQuestionForExamRequest $request){
        $question_ans = app(ListQuestionInExamAction::class)->skipCache(true)->run((int)$request->exam_id, ['answer:id,question_id,title,sort']);
        $question_ans = $this->convertQuestionAnswerForExam($question_ans);

        return \FunctionLib::ajaxRespondV2(true, 'success', $question_ans);
    }
    public function submitQuestionForExams(SubmitQuestionForExamRequest $request)
    {
        if(isset($request->exam) && !empty($request->exam)){
            $exam = app(FrontEndFindExamIdAction::class)->run($request->exam, []);
            if ($exam){
                $question_ans = app(ListQuestionInExamAction::class)->skipCache(true)->run((int)$request->exam, ['answer:id,question_id,title,sort,radio,checkbox']);
                $score = $this->scoreAnswerForExam($exam, $question_ans, $request->data);
                $data_update = [
                    'start_time' => $request->start_time,
                    'end_time' => $request->end_time,
                    'handling_time' => $request->time_use,
                    'score' => $score['score'],
                    'count_true' => $score['count_true'],
                    'result' => json_encode($request->data),
                    'times' => (int)$request->times
                ];
                if ($request->times == 1){
                    $data_update['rework'] = $request->rework;
                }
                $customer_ex = app(UpdateCustomerExamAction::class)->skipCache(true)->run(
                    ['customer_id' => (int)$request->customer, 'course_id' => (int)$request->course, 'exam_id' => (int)$request->exam],
                    $data_update
                );


                $clv = app(FindCustomerLessonViewAction::class)->skipCache(true)->run(
                    ['customer_id' => (int)$request->customer, 'course_id' => (int)$request->course, 'lesson_id' => (int)$request->lesson]
                );
                if (empty($clv)){
                    $course = app(FindCourseByIdAction::class)->skipCache(true)->run($request->course);
                    $percent = 100 / $course->lessons;
                    app(UpdateCustomerLessonViewAction::class)->skipCache(true)->run(
                        ['customer_id' => (int)$request->customer, 'course_id' => (int)$request->course, 'lesson_id' =>(int)$request->lesson], ['percent' => $percent, 'time_view' => 0]
                    );

                    UpdateCustomerCoursePercentEvent::dispatch((int)$request->customer, (int)$request->course);
                }

                return \FunctionLib::ajaxRespondV2(true, 'success', ['url' => route('web.quiz.success', ['slug_q' => Str::slug($exam->title), 'id_q' => $exam->id, 'course_id_q' => $request->course, 'lesson_id_q' => $request->lesson, 'next_lesson' => $request->next_lesson])]);
            }
            return \FunctionLib::ajaxRespondV2(false, 'Error', ['title' => 'Lỗi', 'mess' => 'Vui lòng thử lại']);
        }
    }

    private function convertQuestionAnswerForExam($data){
        $data_return = [];
        if ($data->isNotEmpty()){
            foreach ($data as $itm){
                $answer = [];
                if ($itm->answer->isNotEmpty()){
                    foreach ($itm->answer as $ans){
                        $answer[] = [
                            'id' => $ans->id,
                            'title' => $ans->title,
                            'sort' => $ans->sort
                        ];
                    }
                }
                usort($answer, function ($a, $b){
                    return $a['sort'] > $b['sort'];
                });

                $data_return [] = [
                    'id' => $itm->id,
                    'question_type' => $itm->question_type,
                    'type' => (int)$itm->type,
                    'question' => $itm->question,
                    'image' => \ImageURL::getImageUrl($itm->image, 'questions', '700x400'),
                    'video' => \ImageURL::getFileUrl($itm->video, 'questions', 'video'),
                    'audio' => \ImageURL::getFileUrl($itm->audio, 'questions', 'audio'),
                    'answer' => $answer

                ];
            }

        }

        return  $data_return;
    }

    private function scoreAnswerForExam($exam, $data, $answer){
        $score = 0;
        $count_true = 0;
        if ($data->isNotEmpty()){
            $score_ques = (int)$exam->point / count($data);

            $data = $data->toArray();

            foreach ($data as $itm){
                if (isset($answer[$itm['id']])){
                    $ans = array_map('intval', $answer[$itm['id']]);
                    $ex_ans = [];
                    foreach ($itm['answer'] as $i){
                        if ($i['radio'] == 1){
                            $ex_ans[] = (int)$i['id'];
                        }
                        if ($i['checkbox'] == 1){
                            $ex_ans[] = (int)$i['id'];
                        }
                    }
                    $arr_check = array_diff($ans, $ex_ans);
                    if (empty($arr_check)){
                        $score += $score_ques;
                        $count_true ++;
                    }
                }
            }
        }

        return  ['score' => $score, 'count_true' => $count_true];
    }

}
