<?php

use Illuminate\Support\Facades\Route;

Route::namespace('\App\Containers\Exam\UI\WEB\Controllers\Admin')->prefix('admin/ajax')->group(function () {
  Route::prefix('filters')->group(function () {
    Route::get('/getListExam', [
      'as' => 'admin.exam.getListExam',
      'uses' => 'Controller@ajaxGetListExam',
      'middleware' => [
      'auth:admin',
      ]
    ]);
    Route::get('/get-question', [
      'as' => 'admin.exam.get.question',
      'uses' => 'Controller@ajaxGetQuestion',
      'middleware' => [
      'auth:admin',
      ]
    ]);
  });
});
