<?php
Route::group(
    [
        'prefix' => 'exam',
        'namespace' => '\App\Containers\Exam\UI\WEB\Controllers\Admin',
        'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
        'middleware' => [
            'auth:admin',
        ],
    ],
    function () use ($router) {
        $router->get('/', [
            'as'   => 'admin_exam_home_page',
            'uses' => 'Controller@index',
        ]);

        $router->get('/edit/{id}', [
            'as'   => 'admin_exam_edit_page',
            'uses' => 'Controller@edit',
        ]);

        $router->post('/edit/{id}', [
            'as'   => 'admin_exam_edit_page',
            'uses' => 'Controller@update',
        ]);

        $router->get('/add', [
            'as'   => 'admin_exam_add_page',
            'uses' => 'Controller@add',
        ]);

        $router->post('/add', [
            'as'   => 'admin_exam_add_page',
            'uses' => 'Controller@create',
        ]);
        $router->post('/update-status/{id}', [
            'as'   => 'admin_exam_update_status',
            'uses' => 'Controller@updateStatus',
        ]);
        $router->delete('/{id}', [
            'as' => 'admin_exam_delete',
            'uses'       => 'Controller@deleteExam',
        ]);
    }
);
