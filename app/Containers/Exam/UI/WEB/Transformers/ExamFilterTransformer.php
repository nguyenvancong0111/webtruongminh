<?php

namespace App\Containers\Exam\UI\WEB\Transformers;

use App\Containers\Questions\UI\API\Transformers\AnswerTransformer;
use App\Ship\Parents\Transformers\Transformer;
use Illuminate\Support\Facades\Config;

/**
 * Class QuestionForExamsTransformer.
 *
 */
class ExamFilterTransformer extends Transformer
{

    /**
     * @param $token
     *
     * @return  array
     */
    public function transform($data)
    {
        $response = [
            'id' => $data->id,
            'category' => $data->category_id,
            'level' => $data->level_id,
            'course' => $data->course_id,
            'number' => $data->number
        ];
        return $response;
    }

}
