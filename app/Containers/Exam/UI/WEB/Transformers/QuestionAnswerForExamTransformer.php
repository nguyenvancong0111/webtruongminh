<?php

namespace App\Containers\Exam\UI\WEB\Transformers;

use App\Containers\Questions\UI\API\Transformers\AnswerTransformer;
use App\Ship\Parents\Transformers\Transformer;
use Illuminate\Support\Facades\Config;

/**
 * Class QuestionForExamsTransformer.
 *
 */
class QuestionAnswerForExamTransformer extends Transformer
{

    /**
     * @param $token
     *
     * @return  array
     */
    public function transform($data)
    {
        $answer = $data->relationLoaded('answer') ? $this->transformAnswer($data->answer) : [];
        $response = [
            'id' => $data->id,
            'question' => $data->question,
//            'answer' => $answer
        ];
        return $response;
    }

    public function transformAnswer($data){
        $data_return = [];
        foreach ($data as $itm){
            $data_return [] = [
                'id' => $itm->id,
                'title' => $itm->title,
            ];
        }
        return $data_return;
    }
}
