<?php

namespace App\Containers\Exam\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\Exam\UI\WEB\Requests\CreateExamRequest;
use App\Containers\Exam\UI\WEB\Requests\FindExamRequest;
use App\Containers\Exam\UI\WEB\Requests\GetExamAjaxRequest;
use App\Containers\Exam\UI\WEB\Requests\GetAllExamsRequest;
use App\Containers\Exam\UI\WEB\Requests\UpdateExamRequest;
use App\Containers\Exam\UI\WEB\Requests\GetExamDataByID;
use App\Containers\Exam\UI\WEB\Requests\GetQuestionExamRequest;
use App\Containers\Exam\UI\WEB\Transformers\ExamFilterTransformer;
use App\Containers\Exam\UI\WEB\Transformers\ExamQuestionTransformer;
use App\Containers\Exam\UI\WEB\Transformers\QuestionAnswerForExamTransformer;
use App\Containers\Questions\Actions\Admin\GetQuestionForExamAction;
use App\Containers\Questions\Actions\Admin\ListQuestionInExamAction;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Course\Actions\Admin\FindCourseByIdAction;
use App\Containers\Exam\Actions\GetQuestionOfExamAction;
use App\Containers\Exam\Actions\SaveExamFilterQuestionAction;
use App\Containers\Exam\Actions\SaveExamQuestionAction;
use App\Containers\Exam\Enums\ExamStatus;
use App\Containers\Exam\Models\Exam;

class Controller extends AdminController
{
    public function __construct()
    {
        $this->title = 'Đề thi';
        parent::__construct();
    }
    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllExamsRequest $request)
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Đề thi', $this->form == 'list' ? '' : route('admin_filter_home_page'));
        view()->share('breadcrumb', $this->breadcrumb);

        $exam = Apiato::call('Exam@ExamListingAction', [new DataTransporter($request), ['id' => 'desc'], ['course.desc']]);
//        dd($exam);
        if ($request->course_id) {
            $course = app(FindCourseByIdAction::class)->run($request->course_id);
        } else {
            $course = null;
        }
        return view('exam::admin.index', [
            'search_data' => $request,
            'data' => $exam,
            'course' => $course
        ]);
    }

    public function edit(FindExamRequest $request)
    {
        $this->showEditForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Đề thi', $this->form == 'list' ? '' : route('admin_exam_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Chỉnh sửa thông tin');
        view()->share('breadcrumb', $this->breadcrumb);
        $exam = Apiato::call('Exam@FindExamIdAction', [$request->id, ['exam_filter']]);
        if (!empty($exam)){
            $exam_filter = $this->transform($exam->exam_filter, new ExamFilterTransformer())['data'];

            $questions = app(ListQuestionInExamAction::class)->skipCache(true)->run($exam->id, ['answer', 'category:category_id', 'category.desc:category_id,name', 'level:id', 'level.desc']);
            $data_convert = $this->convertQuestionAnswerForExam($questions);
            $data_questions = $data_convert['data'];

            return view('exam::admin.edit', [
                'data' => $exam,
                'exam_criteria' => json_encode($exam_filter),
                'exam_questions' => json_encode($data_questions),
                'urlLoad' => json_encode([
                    'category' => route('api_category_get_all2'),
                    'level' => route('get_ajax_level'),
                    'renderExam' => route('admin.exam.get.question')
                ])
            ]);
        }
        return $this->notfound($request->id);
    }

    public function add(Request $request)
    {
        $this->showAddForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Đề thi', $this->form == 'list' ? '' : route('admin_exam_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Thêm mới');
        view()->share('breadcrumb', $this->breadcrumb);
        return view('exam::admin.edit', [
            'urlLoad' => json_encode([
                'category' => route('api_category_get_all2'),
                'level' => route('get_ajax_level'),
                'renderExam' => route('admin.exam.get.question')
            ])
        ]);
    }

    public function update(UpdateExamRequest $request)
    {
        try {

            // dd($request->all());
            $tranporter = new DataTransporter($request);
            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'exam', StringLib::getClassNameFromString(Exam::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }
            // dd($tranporter->toArray());
            $tranporter->number_question = $request->count ? count($request->count) : 0;
            $tranporter->rework = $request->rework ? intval(str_replace([',', '.'], '', $request->rework)) : 1;

            $exam = Apiato::call('Exam@UpdateExamGroupAction', [$tranporter]);

            if ($exam) {
                return redirect()->route('admin_exam_edit_page', ['id' => $exam->id])->with('status', 'Exam đã được cập nhật');
            }
        } catch (\Exception $e) {
            // throw $e;
            dd($e->getMessage());
            $this->throwExceptionViaMess($e);
        }
    }

    public function create(CreateExamRequest $request)
    {
        try {
            $tranporter = new DataTransporter($request);
            // dd($request->toArray()['question']);
            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'exam', StringLib::getClassNameFromString(Exam::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }
            $tranporter->number_question = $request->count ? count($request->count) : 0;
            $exam = Apiato::call('Exam@CreateExamGroupAction', [$tranporter]);

            if ($exam) {
                return redirect()->route('admin_exam_home_page')->with('status', 'Exam đã được thêm mới');
            }
        } catch (\Exception $e) {
            // throw $e;
            dd($e->getMessage(), $e->getLine(), $e->getFile());
            $this->throwExceptionViaMess($e);
        }
    }

    public function deleteExam(FindExamRequest $request)
    {
        try {
            Apiato::call('Exam@DeleteExamGroupAction', [$request]);
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }
    public function updateStatus(FindExamRequest $request)
    {
        try {
            Apiato::call(
                'Exam@UpdateStatusExamAction',
                [
                    $request->id,
                    $request->status
                ]
            );
            return FunctionLib::ajaxRespondV2(true, 'Success');
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }
    // public function ajaxGetListExam(GetExamAjaxRequest $request)
    // {
    //     try {
    //         $dataExam = [];
    //         $dataExamSort = [];
    //         $filterGroups = Apiato::call('Exam@ExamGroupListingAction', [new DataTransporter($request), 200, true])->toArray();
    //         foreach ($filterGroups as $key => $value) {
    //             foreach ($value['exam'] as $items) {
    //                 $dataExam[$items['filter_group_id']][$items['sort_order']]['text'] = $value['desc']['name'] . ' > ' . $items['desc']['name'];
    //                 $dataExam[$items['filter_group_id']][$items['sort_order']]['id'] = $items['filter_id'];
    //                 $dataExam[$items['filter_group_id']][$items['sort_order']]['sort_order'] = $items['sort_order'] ?? 0;
    //                 $dataExam[$items['filter_group_id']][$items['sort_order']]['short_description'] =  $value['desc']['short_description'] ?? '';
    //             }
    //             ksort($dataExam[$items['filter_group_id']]);
    //         }
    //         foreach ($dataExam as $key => $value) {
    //             foreach ($value as $k => $v) {
    //                 array_push($dataExamSort, $v);
    //             }
    //         }
    //         return FunctionLib::ajaxRespond(true, 'Hoàn thành', $dataExamSort);
    //     } catch (\Exception $e) {
    //         // throw $e;
    //         return FunctionLib::ajaxRespond(false, 'Xảy ra lỗi');
    //     }
    // }
    public function ajaxExamDataByID(GetExamDataByID $request)
    {
        try {
            $dataExam = Apiato::call('Exam@GetExamsByPrdIdAction', [$request->product_id]);
            $data = [];
            foreach ($dataExam as $key => $item) {
                $data[$key] = $item->filter_id;
            }
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $data);
        } catch (\Exception $e) {
            return FunctionLib::ajaxRespond(false, 'Xảy ra lỗi');
        }
    }
    public function ajaxGetQuestion(GetQuestionExamRequest $request)
    {
        $data = [];
        $rejectID = [];
        if (isset($request->filter) && !empty($request->filter)){
            foreach ($request->filter as $item){
                if(isset($item['number']) && !empty($item['number']) && is_numeric((int)$item['number'])){
                    $fil = [];
                    if ((int)$item['category'] > 0){
                        $fil['category_id'] = (int) $item['category'];
                    }
                    if ((int)$item['level'] > 0){
                        $fil['level_id'] = (int) $item['level'];
                    }
                    $quesion = app(GetQuestionForExamAction::class)->skipCache(true)->run(
                        $fil, (int)$item['number'], $rejectID
                    );
                    $data_convert = $this->convertQuestionAnswerForExam($quesion);
                    $data = array_merge($data, $data_convert['data']);
                    $rejectID = array_merge($rejectID, $data_convert['rejectID']);
                }
            }
        }
        return FunctionLib::ajaxRespond(true, 'Success', ['data' => $data, 'reject' => $rejectID]);
//        $data = $request->data;
//        $ided = $request->ided;
//        $question = app(GetQuestionForExamAction::class)->run($data, $ided);
//        $html = view('exam::admin.list-question', ['data' => $question])->render();
//        // dd($html);
//        return FunctionLib::ajaxRespond(true, 'Hoàn thành', $html);
    }

    private function convertQuestionAnswerForExam($data){
        $data_return = [];
        $arr_id = [];
        if ($data->isNotEmpty()){
            foreach ($data as $itm){
                $answer = [];
                if ($itm->answer->isNotEmpty()){
                    foreach ($itm->answer as $ans){
                        $answer[] = [
                            'id' => $ans->id,
                            'title' => $ans->title,
                        ];
                    }
                }

                $criteria = [
                    'category' => !empty($itm->category) && !empty($itm->category->desc) ? $itm->category->desc->name : '',
                    'level_name' => !empty($itm->level) && !empty($itm->level->desc) ? $itm->level->desc->name : '',
                    'level_short_desc' => !empty($itm->level) && !empty($itm->level->desc) ? $itm->level->desc->short_desc : '',
                ];

                $arr_id[] = $itm->id;
                $data_return [] = [
                    'id' => $itm->id,
                    'question' => $itm->question,
                    'criteria' => $criteria,
                    'answer' => $answer
                ];
            }

        }
        return ['data' => $data_return, 'rejectID' => $arr_id];
    }
}
