@extends('basecontainer::admin.layouts.default')

@section('content')
    <div class="row" id="exam_criteria">
        <div class="col-sm-12">
            @if (isset($editMode) && $editMode)
                {!! Form::open(['url' => route('admin_exam_edit_page', $data->id), 'files' => true]) !!}
            @else
                {!! Form::open(['url' => route('admin_exam_add_page'), 'files' => true]) !!}
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif
            <div class="card card-accent-primary">
                <div class="card-header">
                    <i class="fa fa-pencil"></i> THÔNG TIN CƠ BẢN
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="tab-content p-0">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="title">Tiêu đề</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="title" id="title" value="{{ old('title', @$data->title) }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="short_description">Mô tả ngắn</label>
                                            <div class="input-group">
                                                <textarea rows="5" class="form-control" name="short_description" id="short_description" required>{!! old('short_description', @$data->short_description) !!}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="title">Thời gian làm bài (giây)</label>
                                            <input type="text" class="form-control" name="time" id="time" value="{{ old('time', @$data->time) }}" required onkeypress="return shop.numberOnly()" onkeyup="shop.mixMoney(this)">
                                        </div>
                                        <div class="form-group">
                                            <label for="title">Điểm bài thi</label>
                                            <input type="text" class="form-control" name="point" id="point" value="{{ old('point', @$data->point) }}" required onkeypress="return shop.numberOnly()" onkeyup="shop.mixMoney(this)">
                                        </div>

                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="image">Ảnh đại diện</label>
                                            <input type="file" id="image" name="image" class="dropify form-control {{ $errors->has('image') ? ' is-invalid' : '' }}" data-show-remove="false" data-default-file="{{ \ImageURL::getImageUrl(old('image', @$data['image']), 'exam', 'original') }}">
                                        </div>
                                        <div class="form-group d-flex">
                                            <div class="mr-4 w-50">
                                                <label for="status">Trạng thái <span class="small text-danger">(Hiển thị hay không)</span></label>
                                                <select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" id="status">
                                                    <option value="1"{{ @$data['status'] == 1 ? 'selected' : '' }}>Ẩn</option>
                                                    <option value="2"{{ @$data['status'] == 2 ? 'selected' : '' }}>Hiển thị
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="w-50">
                                                <label for="status">Xem kết quả <span class="small text-danger">(có hay không)</span></label>
                                                <select class="form-control{{ $errors->has('view_result') ? ' is-invalid' : '' }}" name="view_result" id="view_result" >
                                                    <option value="0"{{ @$data['view_result'] == 0 ? 'selected' : '' }}>Ẩn</option>
                                                    <option value="1"{{ @$data['view_result'] == 1 ? 'selected' : '' }}>Cho phép</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group d-flex">
                                            <div class="mr-4 w-50">
                                                <label for="title">Cho phép làm lại <small class="text-danger">(Số lần)</small></label>
                                                <input type="text" class="form-control" name="rework" id="rework" value="{{ old('rework', @$data->rework) }}" required onkeypress="return shop.numberOnly()" onkeyup="shop.mixMoney(this)">
                                            </div>
                                            <div class="w-50">
                                                <label for="title">Sắp xếp</label>
                                                <input type="text" class="form-control" name="sort_order" id="sort_order" value="{{ old('sort_order', @$data->sort_order) }}" required onkeypress="return shop.numberOnly()" onkeyup="shop.mixMoney(this)">
                                            </div>
                                        </div>
                                        <div class="form-group">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-accent-primary" >
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <i class="fa fa-pencil"></i> Tiêu chí tạo đề
                        </div>
                    </div>
                </div>
                <div class="tab-content p-0">
                    <div class="row pl-1 pr-1">
                        <div class="col-sm-12">
                            <div v-for="(item, key) in data.examCriteria">
                                <input type="hidden" v-bind:name="'exam_criteria['+key+'][id]'" v-model="item.id" class="form-control js-input" >
                                <div class="row mt-3">
                                    <div class="col-sm-1"></div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <select class="form-control" v-bind:name="'exam_criteria['+key+'][category]'" v-model="item.category">
                                                <option :value="0">--- Danh mục ---</option>
                                                <option v-for="cate in data.category" :value="cate.id" v-html="cate.text"></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <select class="form-control" v-bind:name="'exam_criteria['+key+'][level]'" v-model="item.level">
                                                <option :value="0">--- Level ---</option>
                                                <option v-for="lv in data.level" :value="lv.id" v-html="lv.name" ></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <input type="text" v-bind:name="'exam_criteria['+key+'][number]'" v-model="item.number" class="form-control js-input"placeholder="Số lượng" >
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="input-group-append">
                                            <span class="btn btn-danger js-input" @click="trashCri(key)" style="cursor: pointer"><i class="fa fa-trash"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2 mb-2">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-4">
                                    <a class="btn btn-outline-success  w-100" @click="createExam"  style="cursor: pointer">Tạo đề</a>
                                </div>
                                <div class="col-sm-4">
                                    <a class="btn btn-outline-info  w-100" @click="addCri"  style="cursor: pointer">Thêm tiêu chí</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-accent-primary">
                <div class="card-header">
                    <i class="fa fa-pencil"></i> Danh sách câu hỏi trong đề thi
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-striped table-responsive-md">
                        <thead>
                            <tr>
                                <th width="55" align="center">#</th>
                                <th>Nội dung</th>
                                <th width="300">Tiêu chí</th>
                                <th width="55" class="text-center">Lệnh</th>
                            </tr>
                        </thead>
                        <tbody id="list-question">
                            <template v-if="typeof data.question != 'undefined' && data.question.length > 0">
                                <tr v-for="(item, index) in data.question">
                                    <td v-html="Number(index) + Number(1)"></td>
                                    <td>
                                        <p class="mb-0"><strong>Câu hỏi: </strong>&nbsp;<span class="" v-html="item.question"></span></p>
                                        <p class="mb-0"><strong>Câu trả lời: </strong></p>
                                        <template v-for="(ans, key) in item.answer" >
                                            <span class="ml-4" v-html="Number(key) + Number(1) + '. '+ ans.title"></span>
                                            <br>
                                        </template>
                                    </td>
                                    <td>
                                        <p><strong>Danh mục: </strong>&nbsp;<span class="" v-html="item.criteria.category"></span></p>
                                        <p><strong>Level: </strong>&nbsp;<span class="" v-html="item.criteria.level_name+' - <small>('+item.criteria.level_short_desc+')</small>'"></span></p>
                                    </td>
                                    <td>
                                        <span class="btn btn-danger js-input" style="cursor: pointer"><i class="fa fa-trash"></i></span>
                                    </td>
                                    <input type="hidden" v-bind:name="'question[]'" v-model="item.id" class="form-control js-input" >
                                </tr>
                            </template>
                            <template v-else>
                                <tr>
                                    <td colspan="4">
                                        <p class="text-center m-0">Empty Data</p>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="mb-3">
                <button type="submit" id="submit-exam" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i>Cập nhật</button>
                &nbsp;&nbsp;
                <a class="btn btn-sm btn-danger" href="{{ redirect()->back()->getTargetUrl() }}"><i class="fa fa-ban"></i> Hủy bỏ</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@push('js_bot_all')
    <script !src="">
        const url = '{!! $urlLoad !!}';
        var exam_criteria_data = '{!! isset($exam_criteria) && !empty($exam_criteria) ? addslashes($exam_criteria) : '' !!}';
        var exam_question_data = '{!! isset($exam_questions) && !empty($exam_questions) ? addslashes($exam_questions) : '' !!}';

        var examCriteriaVue = new Vue({
            el: '#exam_criteria',
            data() {
                return {
                    data: {
                        URL: JSON.parse(url),
                        category: [],
                        level: [],
                        course: [],
                        question: exam_question_data != '' ? JSON.parse(exam_question_data) : [],
                        rejectID: [],
                        examCriteria: exam_criteria_data != '' ? JSON.parse(exam_criteria_data) : [{level: 0, category: 0, course: 0, number: 0}],
                    }
                }
            },
            mounted: function(){
                this.loadCate();
                this.loadLevel();
            },
            methods: {
                addCri: function () {
                    var exCr = {level: 0, category: 0, course: 0, number: 0};
                    return this.data.examCriteria.push(exCr);
                },
                trashCri: function (key) {
                    if(this.data.examCriteria.length > 1) {
                        this.data.examCriteria.splice(key, 1);
                    }else {
                        this.data.examCriteria.splice(key, 1);
                        var exCr = {level: 0, category: 0, course: 0, number: 0};
                        return this.data.examCriteria.push(exCr);
                    }
                },

                createExam: function(){
                    $.get(this.data.URL.renderExam, {
                        filter: this.data.examCriteria
                    }).then(json => {
                        this.data.question = json.data.data;
                        this.data.rejectID = json.data.reject;
                    });
                },

                loadCate: function () {
                    $.get(this.data.URL.category).then(json => {
                        this.data.category = this.data.category.concat(json.data);
                    });
                },
                loadLevel: function () {
                    $.get(this.data.URL.level).then(json => {
                        this.data.level = this.data.level.concat(json.data);
                    });
                }
            },
        });
    </script>
@endpush
