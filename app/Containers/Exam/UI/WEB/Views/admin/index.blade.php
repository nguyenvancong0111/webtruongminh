@extends('basecontainer::admin.layouts.default')

@section('right-breads')
    {!! $data->appends($search_data->toArray())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5">
                <h1>Quản trị {{ $site_title }}</h1>
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            {!! Form::open(['url' => route('admin_exam_home_page'), 'method' => 'get', 'id' => 'searchForm']) !!}

            <div class="card card-accent-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i class="fa fa-bookmark-o"></i></span></div>
                                <input type="text" name="name" class="form-control" placeholder="Tên"
                                    value="{{ $search_data->name }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                    <a href="{{ route('admin_exam_home_page') }}" class="btn btn-sm btn-primary"><i
                            class="fa fa-refresh"></i> Reset</a>
                    <a href="{{ route('admin_exam_add_page') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>
                        Thêm mới</a>
                </div>
            </div>
            {!! Form::close() !!}

            <div class="card card-accent-primary">
                <div class="card-body p-0">
                    <table class="table table-bordered table-striped table-responsive-md">
                        <thead>
                            <tr>
                                <th width="55">ID</th>
                                <th>Tiêu đề</th>
                                <th>Thông tin</th>
                                <th>Hình ảnh</th>
                                <th width="55" class="text-center">Lệnh</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $k => $item)
                                <tr>
                                    {{-- <td>{{++$k}}</td> --}}
                                    <td>{{ $item->id }}</td>
                                    <td>
                                        <b>{{ @$item->title ?? '!<Chưa nhập nội dung>' }}</b>
                                        <div class="mt-2 font-sm"><i>{{ @$item->short_description }}</i></div>
                                    </td>

                                    <td>
                                        <ul>
                                            <li>Sắp xếp: {{ @$item->sort_order }}</li>
                                            <li>Số câu hỏi: {{ @$item->totalQ }}</li>
                                            <li>Thời gian làm bài: {{ @$item->time }}(Giây)</li>
                                            <li>Điểm: {{ @$item->point }}</li>
                                            <li>Thời gian tạo: {!! $item->created_at ? Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s') : '---' !!} </li>
                                        </ul>
                                    </td>
                                    <td class="text-center">
                                        <img src="{{ $item->getImage() }}" width="100"/>
                                    </td>

                                    <td align="center">
                                        @if ($item->status == 2)
                                            <a href="javascript:void(0)"
                                                data-route="{{ route('admin_exam_update_status', $item->id) }}"
                                                class="text-primary"
                                                onclick="admin.updateStatus(this,{{ $item->id }},1)"
                                                title="Đang hiển thị, Click để ẩn"><i class="fa fa-eye"></i></a>
                                        @else
                                            <a href="javascript:void(0)"
                                                data-route="{{ route('admin_exam_update_status', $item->id) }}"
                                                class="text-danger"
                                                onclick="admin.updateStatus(this,{{ $item->id }}, 2)"
                                                title="Đang ẩn, Click để hiển thị"><i class="fa fa-eye"></i></a>
                                        @endif
                                        <a href="{{ route('admin_exam_edit_page', $item->id) }}"
                                            class="btn text-primary"><i class="fa fa-pencil"></i></a>
                                        <a data-href="{{ route('admin_exam_delete', $item->id) }}"
                                            class="btn text-danger" onclick="admin.delete_this(this)"><i
                                                class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @include('basecontainer::admin.inc.bottom-pager-infor')
                </div>
            </div>
        </div>
    </div>
@stop
@push('js_bot_all')
    <script type="text/javascript">
        $(document).ready(function() {

            $("#course_id").select2({
                width: '100%',
                tags: true,
                ajax: {
                    url: "{{ route('get_ajax_course') }}",
                    headers: ENV.headerParams,
                    dataType: 'JSON',
                    delay: 800,
                    data: function(params) {
                        return {
                            status: 2,
                            name: params.term, // search term
                            page: params.page,
                            _token: ENV.token
                        };
                    },
                    processResults: function(data, params) {
                        return {
                            results: $.map(data.data, function(item) {
                                return {
                                    text: item.desc.name,
                                    id: item.id
                                }
                            })
                        };

                    },
                    cache: true
                },
            });
        })
    </script>
@endpush
