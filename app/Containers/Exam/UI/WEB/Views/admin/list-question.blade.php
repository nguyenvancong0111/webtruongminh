@if (!empty($data))
   
    @foreach ($data as $k => $val)
        @foreach ($val as $item)
            <tr id="question-{{ $item->id }}">
                <td>{{ $item->id }}
                    <input type="hidden" value="{{ $item->id }}" name="question[id][]" class="id-question">
                    <input type="hidden" value="{{ $item->id }}" name="count[]">

                </td>
                <td>
                    <b>{{ @$item->desc->name ?? '!<Chưa nhập nội dung>' }}</b>
                </td>
                <td></td>
                <td>
                    <input type="text" name="question[order][]" value="{{ $item->id }}"
                        class="form-control number" />
                </td>
                <td align="center">
                    <button type="button" onclick="removeQuestion({{ $item->id }});" data-toggle="tooltip"
                        title="Xóa" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                </td>
            </tr>
         
        @endforeach
    @endforeach
@endif
