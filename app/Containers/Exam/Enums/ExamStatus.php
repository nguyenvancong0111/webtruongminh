<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-12 16:29:36
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class ExamStatus extends BaseEnum
{
    
}
