<?php

namespace App\Containers\Exam\Events\Handlers;

use App\Containers\BaseContainer\Events\Handlers\BaseFrontEventHandler;
use App\Containers\Customer\Actions\UpdateCustomerExamAction;
use App\Containers\Exam\Events\CreateOrUpdateCustomerExamEvent;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateOrUpdateCustomerExamHandler extends BaseFrontEventHandler /*implements ShouldQueue*/
{
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(CreateOrUpdateCustomerExamEvent $event)
    {
        if ($event->customer != 0){
            //update Lesson
            app(UpdateCustomerExamAction::class)->skipCache(true)->run(
                $event->filters, $event->data
            );
        }
    }
}
