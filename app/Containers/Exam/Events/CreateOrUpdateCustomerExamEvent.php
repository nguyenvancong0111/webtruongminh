<?php

namespace App\Containers\Exam\Events;

use App\Ship\Parents\Events\Event;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateOrUpdateCustomerExamEvent extends Event /*implements ShouldQueue*/
{
    public $filters;
    public $data;
    public $customer;
    public function __construct($customer, $filters, $data)
    {
        $this->filters = $filters;
        $this->data = $data;
        $this->customer = $customer;

    }

    public function handle()
    {
        // Do some handling here
    }

    public function broadcastOn()
    {
        return [];
    }
}
