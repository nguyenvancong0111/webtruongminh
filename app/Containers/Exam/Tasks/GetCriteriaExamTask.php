<?php

namespace App\Containers\Exam\Tasks;

use App\Containers\Exam\Data\Repositories\ExamFilterQuestionRepository;
use App\Ship\Criterias\Eloquent\OrderByFieldCriteria;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

class GetCriteriaExamTask extends Task
{

    protected $repository;

    public function __construct(ExamFilterQuestionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($exam_id)
    {
        $wery = $this->repository->pushCriteria(new ThisEqualThatCriteria('exam_id', $exam_id));
        return $wery->get()->toArray();
    }
}
