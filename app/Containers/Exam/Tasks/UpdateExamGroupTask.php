<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-18 11:51:48
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Tasks;

use App\Containers\Exam\Data\Repositories\ExamRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;


class UpdateExamGroupTask extends Task
{

    protected $repository;

    public function __construct(ExamRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data->toArray(), ['exam_criteria', 'question', '_token', '_headers']);
            // dd($data->toArray());
            $cate = $this->repository->update($dataUpdate, $data->id);

            return $cate;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
