<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 11:43:03
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Tasks;

use App\Containers\Exam\Data\Repositories\ExamDescRepository;
use App\Ship\Parents\Tasks\Task;

class SaveExamGroupDescTask extends Task
{

    protected $repository;

    public function __construct(ExamDescRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data, $original_desc, $exam_id, $edit_id = null)
    {
        $description = isset($data['description']) ? (array)$data['description'] : null;
        if (is_array($description) && !empty($description)) {
            $updates = [];
            $inserts = [];

            foreach ($description as $k => $v) {
                if (isset($original_desc[$k])) {
                    $updates['data'][$original_desc[$k]['filter_group_id'] . '_' . $k] = [
                        'name' => $v['name'],
                        'short_desc' => $v['short_description'],
                    ];

                } else {
                    $inserts[] = [
                        'language_id' => $k,
                        'exam_id' => $exam_id,
                        'name' => $v['name'],
                        'short_desc' => $v['short_description'],
                    ];
                }
            }
            if (!empty($updates)) {
                $this->repository->updateMultipleConditions($updates);
            }

            if (!empty($inserts)) {
                $this->repository->getModel()->insert($inserts);
            }
        }
    }
}
