<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-12 12:08:26
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Tasks;

use App\Containers\Exam\Data\Repositories\ExamRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisInThatCriteria;
use App\Ship\Parents\Tasks\Task;

class GetExamsByCourseIdTask extends Task
{
    protected $repository;

    public function __construct(ExamRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($course_id, $order_by, $limit = 4, $hasPage = true, $customer_id = null)
    {
       
        if ($customer_id) {
            $external_data['with_relationship'] = [
                'customer' => function ($qr) use ($customer_id) {
                    $qr->select('*');
                    $qr->where('customer_id', $customer_id);
                }
            ];
        }
      
        $this->repository->with(array_merge(['desc'], isset($external_data['with_relationship']) ? $external_data['with_relationship'] : []));
        
        $this->repository->pushCriteria(new ThisEqualThatCriteria('course_id', $course_id));
        // dd($this->repository->take($limit)->get());
        foreach ($order_by as $column => $direction) {
            $this->repository->orderBy($column, $direction);
        }
    
        //        dd($data->get());
        return $hasPage ?  $this->repository->paginate($limit) : $this->repository->take($limit)->get();
    }
}
