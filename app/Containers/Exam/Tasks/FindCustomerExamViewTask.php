<?php

namespace App\Containers\Exam\Tasks;

use App\Containers\Customer\Data\Repositories\CustomerExamRepository;
use App\Ship\Parents\Tasks\Task;
/**
 * Class FindCustomerExamViewTask.
 */
class FindCustomerExamViewTask extends Task
{

    protected $repository;

    public function __construct(CustomerExamRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($filter)
    {
        $data = $this->repository;
        if (is_array($filter) && !empty($filter)){
            foreach ($filter as $key => $val){
                $data = $data->where($key, $val);
            }
        }
        return $data->first();
    }
}