<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 11:43:03
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Tasks;

use App\Containers\Exam\Data\Repositories\ExamQuestionRepository;
use App\Ship\Parents\Tasks\Task;

class SaveExamQuestionTask extends Task
{

    protected $repository;

    public function __construct(ExamQuestionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data, $exam_id)
    {
        $data = $data->toArray()['question'];
        $insert = [];
        if(!empty($data)){
            foreach ($data as $val) {
                $insert[] = [
                    'exam_id' => $exam_id,
                    'question_id' => $val
                ];
            }
        }
        $this->repository->getModel()->where(['exam_id' => $exam_id])->delete();
        if (!empty($insert)){
            $this->repository->getModel()->insert($insert);
        }
    }
}
