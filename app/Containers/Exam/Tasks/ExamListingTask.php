<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-08 09:58:41
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:15:43
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Tasks;

use App\Containers\Exam\Data\Repositories\ExamRepository;
use App\Containers\Exam\Enums\ExamStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class ExamListingTask extends Task
{

    protected $repository;

    public function __construct(ExamRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($filters = [], $orderBy = ['sort_order' => 'asc', 'id' =>  'DESC'], $with = [], $defaultLanguage = null, $skipPagination = false, $limit = 10)
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;
        if (isset($filters['status']) && !empty($filters['status'])) {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('status', $filters['course_id']));
        }
        if (isset($filters['course_id']) && !empty($filters['course_id'])) {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('course_id', $filters['course_id']));
        }
        $this->repository->with(array_merge([], $with));

        if (isset($filters['name']) && $filters['name']) {
            $this->repository->where('title', 'like', '%' . $filters['name'] . '%');
        }
        foreach ($orderBy as $column => $direction) {
            $this->repository->orderBy($column, $direction);
        }
        $this->repository->withCount('exam_question as totalQ');
        return !$skipPagination ?
            $this->repository->paginate($limit) : ($limit == 0 ? $this->repository->get() : $this->repository->take($limit)->get());
    }
}
