<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-12 12:08:26
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Tasks;

use App\Containers\Exam\Data\Repositories\ExamRepository;
use App\Containers\Exam\Models\Exam;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

class GetExamsByCustomerIdTask extends Task
{
    protected $repository;

    public function __construct(ExamRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($customer_id, $limit = 4, $hasPage = true)
    {
       
        $data = Exam::leftJoin('customer_exam', 'customer_exam.exam_id', 'exam.id')->where('customer_exam.customer_id', $customer_id)->with(['desc']);
        return $hasPage ?  $data->paginate($limit) : $data->take($limit)->get();
    }
}
