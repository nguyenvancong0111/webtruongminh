<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:16:33
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Tasks;

use App\Containers\Exam\Data\Repositories\ExamRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

class FindExamIdTask extends Task
{

    protected $repository;

    public function __construct(ExamRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, $external_data = [])
    {
        $this->repository->with(array_merge([], isset($external_data) ? $external_data : []));
        $this->repository->withCount('exam_question as totalQ');

        $this->repository->pushCriteria(new ThisEqualThatCriteria('id', $id));

        return $this->repository->first();
    }
}
