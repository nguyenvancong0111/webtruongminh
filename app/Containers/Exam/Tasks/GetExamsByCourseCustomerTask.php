<?php

namespace App\Containers\Exam\Tasks;

use App\Containers\Exam\Data\Repositories\ExamRepository;
use App\Containers\Exam\Models\Exam;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetExamsByCourseCustomerTask extends Task
{
    protected $repository;

    public function __construct(ExamRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $customer_id, $course_id, $filter = [], $limit = 4, $order_by = ['created_at' => 'DESC', 'id' => 'DESC'], $skipPagigate = true, $current_page = 1)
    {
        $this->repository->with(['exam_handing' => function($query)  use($customer_id){
            $query->where('customer_id', $customer_id);
        }]);
        $this->repository->whereHas('exam_handing', function (Builder $qr) use($customer_id, $course_id){
           $qr->where('customer_exam.customer_id', '=', $customer_id);
        });
        if(isset($filter['status']) && !empty($filter['status'])){
            $this->repository->pushCriteria(new ThisEqualThatCriteria('status', $filter['status']));
        }
        $this->repository->whereHas('exam_course', function (Builder $query) use ($course_id){
            if (is_array($course_id) && !empty($course_id)){
                $query->whereIn('course.id', $course_id);
            }elseif (!is_array($course_id)){
                $query->where('course.id', (int)$course_id);
            }
        });

        foreach ($order_by as $key => $val){
            $this->repository->orderBy($key, $val);
        }

        return $skipPagigate ? $this->repository->limit($limit) : $this->repository->paginate($limit);

    }
}
