<?php

namespace App\Containers\Exam\Tasks;

use App\Containers\Exam\Data\Repositories\ExamQuestionRepository;
use App\Ship\Criterias\Eloquent\OrderByFieldCriteria;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

class GetQuestionOfExamTask extends Task
{

    protected $repository;

    public function __construct(ExamQuestionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($exam_id, $limit, $hasPage = false, $with = [])
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('exam_id', $exam_id));
        $this->repository->pushCriteria(new OrderByFieldCriteria('sort_order', 'asc'));
        $wery = $this->repository->with([]);
        return $hasPage ? $wery->paginate($limit) : ($limit == 0 ? $wery->get() : $wery->take($limit)->get());
    }
}
