<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-20 03:47:48
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Tasks;

use App\Containers\Exam\Data\Repositories\ExamRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

class UpdateStatusExamTask extends Task
{
    protected $repository;

    public function __construct(ExamRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($exam_id, $status)
    {
        try {
            $cate = $this->repository->where('id', $exam_id)->update(['status' => $status]);
            return $cate;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
