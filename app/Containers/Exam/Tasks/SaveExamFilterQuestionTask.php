<?php

namespace App\Containers\Exam\Tasks;

use App\Containers\Exam\Data\Repositories\ExamFilterQuestionRepository;
use App\Ship\Parents\Tasks\Task;

class SaveExamFilterQuestionTask extends Task
{

    protected $repository;

    public function __construct(ExamFilterQuestionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data, $original_exF, $exam_id)
    {
        $data = $data->toArray()['exam_criteria'];
        $arr_id = array_column($original_exF, 'id');

        $update_item = [];
        $insert_item = [];
        if (!empty($data)){
            foreach ($data as $key => $item){
                if (!empty($item['id']) && isset($original_answer[$item['id']])){
                    unset($arr_id[array_search((int)$item['id'], $arr_id)]);
                    $update_item[$item['id']] = [
                        'exam_id' => $exam_id,
                        'category_id' => $item['category'],
                        'level_id' => $item['level'],
                        /*'course_id' => $item['course'],*/
                        'number' => $item['number']
                    ];
                }else{
                    $insert_item[] = [
                        'exam_id' => $exam_id,
                        'category_id' => $item['category'],
                        'level_id' => $item['level'],
                        /*'course_id' => $item['course'],*/
                        'number' => $item['number']
                    ];
                }
            }

            if (!empty($update_item)) {
                $this->repository->updateMultiple($update_item);
            }

            if (!empty($insert_item)) {
                $this->repository->getModel()->insert($insert_item);
            }

            if(!empty($arr_id)){
                $this->repository->whereIn('id', $arr_id)->delete();
            }

        }
    }
}
