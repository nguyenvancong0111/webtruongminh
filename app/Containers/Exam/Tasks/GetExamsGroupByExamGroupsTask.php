<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-12 12:08:26
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Tasks;

use App\Containers\Exam\Data\Repositories\ExamGroupRepository;
use App\Ship\Criterias\Eloquent\ThisInThatCriteria;
use App\Ship\Parents\Tasks\Task;

class GetExamsGroupByExamTask extends Task
{
    protected $repository;

    public function __construct(ExamGroupRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($filterGroupIds = [], $defaultLanguage = null, $orderBy = ['sort_order' => 'ASC', 'filter_group_id' => 'DESC'])
    {
        $languageId = $defaultLanguage ? $defaultLanguage->language_id : 1;

        $external_data['with_relationship'] = [
            'exam' => function ($qr) {
                $qr->select('filter_id', 'filter_group_id', 'sort_order','value_range');
            }, 'exam.desc' => function ($query) use ($languageId) {
                $query->select('filter_id', 'name');
                $query->activeLang($languageId);
            }
        ];
        $this->repository->with(array_merge(['desc' => function ($query) use ($languageId) {
            $query->activeLang($languageId);
        }], isset($external_data['with_relationship']) ? $external_data['with_relationship'] : []));

        $this->repository->pushCriteria(new ThisInThatCriteria('filter_group_id', $filterGroupIds));

        foreach ($orderBy as $column => $direction) {
            $this->repository->orderBy($column, $direction);
        }
        //        dd($data->get());
        return $this->repository->all();
    }
}
