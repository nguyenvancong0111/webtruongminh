<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:15:23
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Tasks;

use App\Containers\Exam\Data\Repositories\ExamRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteExamGroupTask extends Task
{

    protected $repository;

    public function __construct(ExamRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            $this->repository->getModel()->where('id', $id)->delete();
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
