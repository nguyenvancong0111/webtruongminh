<?php

namespace App\Containers\Exam\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class FindCustomerExamViewAction.
 *
 */
class FindCustomerExamViewAction extends Action
{

    /**
     * @return mixed
     */
    public function run($filters)
    {
        $data = Apiato::call('Exam@FindCustomerExamViewTask', [$filters]);
        return $data;
    }
}
