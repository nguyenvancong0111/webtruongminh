<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:12:31
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Actions;

use App\Ship\Parents\Actions\Action;

class GetExamByNameAction extends Action
{
    public function run($name,$limit)
    {
        if ($name) {
            $exam['name'] = $name;
        }
        $data = $this->call(
            'Exam@GetExamForChoosingTask',
            [
                '',
                $exam ?? [],
                [],
                $limit,
                $this->call('Localization@GetDefaultLanguageTask')
            ]
        );

        return $data;
    }
}
