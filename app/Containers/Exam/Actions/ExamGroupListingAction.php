<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:13:50
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

class ExamGroupListingAction extends Action
{
    public function run(DataTransporter $filters, $limit = 10, $skipPagination = false)
    {
        $data = Apiato::call('Exam@ExamGroupListingTask', [
            $filters,
            [
                'sort_order' => 'DESC', 'filter_group_id' =>  'DESC'],
                Apiato::call('Localization@GetDefaultLanguageTask'),
                $skipPagination,
                $limit
            ]
        );

        return $data;
    }
}
