<?php

namespace App\Containers\Exam\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Exam\Tasks\GetExamsByCourseCustomerTask;
use App\Ship\Parents\Actions\Action;


class GetExamsByCourseCustomerAction extends Action
{
    public function run(int $customer_id, $course_id, $filter = [], $limit = 4, $order_by = ['created_at' => 'DESC', 'id' => 'DESC'], $skipPagigate = true, $current_page = 1)
    {
        $data = app(GetExamsByCourseCustomerTask::class)->run(
            $customer_id, $course_id, $filter, $limit, $order_by, $skipPagigate, $current_page
        );

        return $data;
    }
}
