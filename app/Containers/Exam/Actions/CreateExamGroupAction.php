<?php

namespace App\Containers\Exam\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Exam\Tasks\SaveExamFilterQuestionTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

class CreateExamGroupAction extends Action
{
    public function run(DataTransporter $data)
    {
        $exam = Apiato::call('Exam@CreateExamGroupTask', [$data]);
        if ($exam) {
            //create exam criteria
            app(SaveExamFilterQuestionTask::class)->run($data, [], $exam->id);

            //remove exam question old and insert new
            app(SaveExamQuestionAction::class)->run($data, $exam->id);
        }

        $this->clearCache();

        return $exam;
    }
}
