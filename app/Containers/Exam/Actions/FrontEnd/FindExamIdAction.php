<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:12:54
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Actions\FrontEnd;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

class FindExamIdAction extends Action
{
    public function run($id, $external_data = [])
    {
        $data = Apiato::call(
            'Exam@FindExamIdTask',
            [
                $id, $external_data
            ]
        );



        return $data;
    }
}
