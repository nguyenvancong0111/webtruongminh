<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-12 12:10:04
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Exam\Tasks\GetCriteriaExamTask;
use App\Containers\Exam\Tasks\SaveExamFilterQuestionTask;
use App\Ship\Parents\Actions\Action;

class UpdateExamGroupAction extends Action
{

    public function run($data)
    {
        $exam = Apiato::call(
            'Exam@UpdateExamGroupTask',
            [
                $data
            ]
        );
        if ($exam) {
            //create exam criteria
            $original_exam_criteria = app(GetCriteriaExamTask::class)->run($exam->id);
            app(SaveExamFilterQuestionTask::class)->run($data, $original_exam_criteria, $exam->id);
            //remove exam question old and insert new
            app(SaveExamQuestionAction::class)->run($data, $exam->id);
        }

        $this->clearCache();

        return $exam;
    }
}
