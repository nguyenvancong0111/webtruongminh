<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:12:16
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;


class GetExamsByCustomerIdAction extends Action
{
    public function run($customer_id, $limit = 4, $hasPage = true)
    {
        $data = Apiato::call(
            'Exam@GetExamsByCustomerIdTask',
            [
                $customer_id,
              
                $limit,
                $hasPage
            ]
        );

        return $data;
    }
}
