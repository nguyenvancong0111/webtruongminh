<?php


namespace App\Containers\Exam\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

class ExamListingAction extends Action
{
    public function run(DataTransporter $filters, $order = ['id' => 'DESC'], $with = [], $limit = 15, $skipPagination = false) {
        $data = Apiato::call('Exam@ExamListingTask',
            [$filters, $order, $with, Apiato::call('Localization@GetDefaultLanguageTask'), $skipPagination, $limit]
        );

        return $data;
    }
}
