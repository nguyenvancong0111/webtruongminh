<?php

namespace App\Containers\Exam\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

class FindExamIdAction extends Action
{
    public function run($id, $external_data=[])
    {
        $data = Apiato::call(
            'Exam@FindExamIdTask',
            [
                $id, $external_data
            ]
        );
        return $data;
    }
}
