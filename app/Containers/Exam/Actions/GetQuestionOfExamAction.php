<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:12:31
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Actions;

use App\Ship\Parents\Actions\Action;

class GetQuestionOfExamAction extends Action
{
    public function run($exam_id, $limit)
    {

        $data = $this->call(
            'Exam@GetQuestionOfExamTask',
            [
                $exam_id,
                $limit,
            ]
        );

        return $data;
    }
}
