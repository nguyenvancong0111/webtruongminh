<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-12 12:09:48
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

class SaveExamFilterQuestionAction extends Action
{
    public function run($data, $id)
    {
        $exam = Apiato::call('Exam@SaveExamFilterQuestionTask', [
            $data,
            $id
        ]);

        $this->clearCache();

        return $exam;
    }
}
