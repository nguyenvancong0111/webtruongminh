<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-10 13:11:40
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-12 12:09:01
 * @ Description: Happy Coding!
 */

namespace App\Containers\Exam\Actions;

use App\Containers\Exam\Tasks\GetExamsGroupByExamTask;
use App\Ship\Parents\Actions\Action;

class GetExamsGroupByExamAction extends Action
{
    public $getExamsGroupByExamTask;

    public function __construct(GetExamsGroupByExamTask $getExamsGroupByExamTask)
    {
        $this->getExamsGroupByExamTask = $getExamsGroupByExamTask;
        parent::__construct();
    }

    public function run($filterGroupIds = [], $defaultLanguage = null, $orderBy = ['sort_order' => 'ASC', 'filter_group_id' => 'DESC'])
    {
        return $this->remember(function () use ($filterGroupIds, $defaultLanguage, $orderBy) {
            return $this->getExamsGroupByExamTask->run($filterGroupIds, $defaultLanguage, $orderBy);
        });
    }
}
