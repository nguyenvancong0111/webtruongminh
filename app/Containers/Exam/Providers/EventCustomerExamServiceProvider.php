<?php

namespace App\Containers\Exam\Providers;

use App\Containers\Exam\Events\CreateOrUpdateCustomerExamEvent;
use App\Containers\Exam\Events\Handlers\CreateOrUpdateCustomerExamHandler;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventCustomerExamServiceProvider extends ServiceProvider
{
    protected $listen = [
        CreateOrUpdateCustomerExamEvent::class => [
            CreateOrUpdateCustomerExamHandler::class
        ],
    ];

    public function boot() {
        parent::boot();
    }
}
