<?php

namespace App\Containers\AppSpace\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\StringLib;
use App\Containers\AppSpace\Models\AppSpace;
use App\Containers\AppSpace\Models\AppSpaceDesc;
use App\Containers\AppSpace\UI\WEB\Requests\CreateAppSpaceRequest;
use App\Containers\AppSpace\UI\WEB\Requests\DeleteAppSpaceRequest;
use App\Containers\AppSpace\UI\WEB\Requests\GetAllAppSpacesRequest;
use App\Containers\AppSpace\UI\WEB\Requests\UpdateAppSpaceRequest;
use App\Containers\AppSpace\UI\WEB\Requests\StoreAppSpaceRequest;
use App\Containers\AppSpace\UI\WEB\Requests\EditAppSpaceRequest;
use App\Containers\AppSpace\UI\WEB\Requests\UpdateStatusAppSpaceRequest;
use App\Containers\Banner\Models\Banner;
use App\Ship\core\Foundation\BladeHelper;
use App\Ship\Parents\Controllers\AdminController;
use Apiato\Core\Foundation\Facades\Apiato;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class Controller extends AdminController
{
    protected $containerName = 'appspace';

    public function __construct()
    {
        $this->title = 'Kho ứng dụng';
        $this->imageField = 'image';
        $this->imageKey = $this->containerName;
        $this->actions = [
            'list' => 'AppSpace@GetAllAppSpacesAction',
            'create' => 'AppSpace@CreateAppSpaceAction',
            'edit' => 'AppSpace@FindAppSpaceByIdAction',
            'update' => 'AppSpace@UpdateAppSpaceAction',
            'delete' => 'AppSpace@DeleteAppSpaceAction',
        ];
        $this->routes = [
            'list' => "admin.$this->containerName.index",
            'create' => "admin.$this->containerName.create",
            'store' => "admin.$this->containerName.store",
            'edit' => "admin.$this->containerName.edit",
            'update' => "admin.$this->containerName.update",
            'destroy' => "admin.$this->containerName.destroy",
            'status' => "admin.$this->containerName.update-status",
        ];
        View::share('routes', $this->routes);
        View::share('containerName', $this->containerName);

        parent::__construct();
    }

    public function index(GetAllAppSpacesRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $request->merge(['scopeAdminBaseSearch' => true]);
        $data = Apiato::call($this->actions['list'], [$request, $this->perPage, null, ['desc']]);

        return view("$this->containerName::Admin.index", [
            'request' => $request,
            'data' => $data,
            //'types' => config('appspace-container.type'),
        ]);
    }

    public function create(CreateAppSpaceRequest $request)
    {
        $this->showAddForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['add', $this->title, $this->routes['list']]);

        return view("$this->containerName::Admin.form", [
            //'types' => config('appspace-container.type'),
        ]);
    }

    public function store(StoreAppSpaceRequest $request)
    {
        try {
            $tranporter = $request->all();

            if(isset($request->delete_image)){
                $tranporter['image'] = '';
            }

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'appspace', StringLib::getClassNameFromString(AppSpace::class)]);
                if (!$image['error']) {
                    $tranporter['image'] = $image['fileName'];
                }
            }

            if(isset($request->delete_image_two)){
                $tranporter['image_two'] = '';
            }

            if (isset($request->image_two)) {
                $imageTwo = Apiato::call('File@UploadImageAction', [$request, 'image_two', 'appspace', StringLib::getClassNameFromString(AppSpace::class)]);
                if (!$imageTwo['error']) {
                    $tranporter['image_two'] = $imageTwo['fileName'];
                }
            }

            $data = Apiato::call('AppSpace@CreateAppSpaceAction', [$tranporter]);

            if ($data) {
                return redirect()->route('admin.appspace.index')->with('status', 'Kho ứng dụng đã được thêm mới');
            }

        } catch (\Exception $e) {
            $this->throwExceptionViaMess($e);
        }
    }

    public function edit($id, EditAppSpaceRequest $request)
    {
        try {
            $this->showEditForm();
            Apiato::call('BaseContainer@CreateBreadcrumbAction', ['edit', $this->title, $this->routes['list']]);

            $editedObj = Apiato::call($this->actions['edit'], [$id, ['all_desc']]);

            return view("$this->containerName::Admin.form", [
                'data' => $editedObj,
                //'types' => config('appspace-container.type'),
            ]);
        } catch (Exception $e) {
            return $this->notfound($id);
        }
    }

    public function update(UpdateAppSpaceRequest $request)
    {
        try {
            $tranporter = $request->all();

            if(isset($request->delete_image)){
                $tranporter['image'] = '';
            }

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'appspace', StringLib::getClassNameFromString(AppSpace::class)]);
                if (!$image['error']) {
                    $tranporter['image'] = $image['fileName'];
                }
            }

            if(isset($request->delete_image_two)){
                $tranporter['image_two'] = '';
            }

            if (isset($request->image_two)) {
                $imageTwo = Apiato::call('File@UploadImageAction', [$request, 'image_two', 'appspace', StringLib::getClassNameFromString(AppSpace::class)]);
                if (!$imageTwo['error']) {
                    $tranporter['image_two'] = $imageTwo['fileName'];
                }
            }

            $data = Apiato::call('AppSpace@UpdateAppSpaceAction', [$tranporter]);

            return redirect()->to(route( $this->routes['edit'], [$data->id]))->with('status', 'Cập nhật kho ứng dụng thành công');

        } catch (\Exception $e) {
            $this->throwExceptionViaMess($e);
        }
    }

    public function destroy($id, DeleteAppSpaceRequest $request)
    {
        DB::beginTransaction();
        try {
            Apiato::call($this->actions['delete'], [$id]);

            DB::commit();
            return redirect()->route($this->routes['list'])->with('status', 'Xóa bản ghi thành công!');
        } catch (Exception $e) {
            DB::rollBack();
            return $this->throwExceptionViaMess($e);
        }
    }

    public function updateStatus(UpdateStatusAppSpaceRequest $request)
    {
        DB::beginTransaction();
        try {
            Apiato::call('AppSpace@UpdateStatusAppSpaceAction', [$request]);

            DB::commit();
            return FunctionLib::ajaxRespondV2(true, BladeHelper::STATUS_TEXT[$request->status] . ' bản ghi thành công!');
        } catch (Exception $e) {
            DB::rollBack();
            return $this->throwExceptionViaMess($e);
        }
    }
}
