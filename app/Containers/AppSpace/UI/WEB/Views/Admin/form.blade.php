@extends('basecontainer::admin.layouts.default')

@section('content')
    @if (isset($editMode) && $editMode)
        {!! Form::open(['url' => route($routes['update'], $data->id), 'files' => true]) !!}
        <input type="hidden" name="id" value="{{$data->id}}"/>
        @method('PUT')
    @else
        {!! Form::open(['url' => route($routes['store']), 'files' => true]) !!}
    @endif

    <div class="row">
        <div class="col-12">
            @include('basecontainer::admin.inc.alert')
        </div>
    </div>

    <div class="row">
        <div class="col-7">
            <div class="card card-accent-success">
                <div class="card-header">
                    <i class="fa fa-pencil-square-o"></i> {{__('Thông tin chung')}}
                </div>
                <div class="card-body">
                    <div class="tabbable boxed parentTabs">
                        <div class="tab-content">
                            @include("$containerName::Admin.edit_tabs.general")
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-5">
            <div class="row">
                <div class="col-12">
                    <div class="card card-accent-info">
                        <div class="card-header">
                            <i class="fa fa-deviantart"></i> {{__('Chi tiết')}}
                        </div>
                        <div class="card-body">
                            <div class="tabbable boxed parentTabs">
                                <div class="tab-content">
                                    @include("$containerName::Admin.edit_tabs.detail")
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 mt-2">
                    <div class="card card-accent-warning">
                        <div class="card-header">
                            <i class="fa fa-image"></i> {{__('Hình ảnh')}}
                        </div>
                        <div class="card-body">
                            <div class="tabbable boxed parentTabs">
                                <div class="tab-content">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="image">{{ $imgTittle ?? 'Hình ảnh 1' }}</label>
                                                <input type="file" id="image" name="image" accept="image/*"
                                                       class="form-control pt-1 {{ $errors->has('image') ? 'is-invalid' : '' }}">
                                                @if(!empty($data->image))
                                                    <div class="holderImg mt-2">
                                                        <img width="100" class="oldImg img-thumbnail" src="{{ $data->getImageUrl('small') }}">

                                                        <input type="hidden" name="delete_image" class="delete_image">
                                                        <a class="btn-danger btn-sm ml-3 mt-0 text-decoration-none"
                                                           href="javascript:void(0)"
                                                           onclick="$(this).closest('.holderImg').find('.delete_image').val('1');$(this).closest('.holderImg').find('.oldImg').remove();$(this).remove();"
                                                           title="Xóa ảnh"><i class="fa fa-trash"></i> {{__('Xóa')}}</a>
                                                    </div>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <label for="image_two">{{ $imgTittle ?? 'Hình ảnh 2' }}</label>
                                                <input type="file" id="image_two" name="image_two" accept="image/*"
                                                       class="form-control pt-1 {{ $errors->has('image_two') ? 'is-invalid' : '' }}">
                                                @if(!empty($data->image_two))
                                                    <div class="holderImg mt-2">
                                                        <img width="100" class="oldImg img-thumbnail" src="{{ $data->getImageUrlTwo('small') }}">

                                                        <input type="hidden" name="delete_image_two" class="delete_image_two">
                                                        <a class="btn-danger btn-sm ml-3 mt-0 text-decoration-none"
                                                           href="javascript:void(0)"
                                                           onclick="$(this).closest('.holderImg').find('.delete_image_two').val('1');$(this).closest('.holderImg').find('.oldImg').remove();$(this).remove();"
                                                           title="Xóa ảnh"><i class="fa fa-trash"></i> {{__('Xóa')}}</a>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @include("basecontainer::admin.inc.form-btn-submit")

    {!! Form::close() !!}
@stop

@section('js_bot')
    <script type="text/javascript">
        $("ul.nav-tabs a").click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

    </script>
@stop
