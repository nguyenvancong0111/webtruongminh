<div class="tab-pane active" id="data">
    <div class="tabbable">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="status">Trạng thái <small class="text-danger">(Hiển thị hay không)</small></label>
                    <div class="input-group">
                        <select id="status" name="status"
                                class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}">
                            @if(!empty(\App\Ship\core\Foundation\BladeHelper::STATUS_TEXT))
                                @foreach(\App\Ship\core\Foundation\BladeHelper::STATUS_TEXT as $statusKey => $statusText)
                                    <option value="{{$statusKey}}" {{ old('status', @$data->status) == $statusKey ? 'selected' : '' }}>{{$statusText}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="sort_order">Sắp xếp <small class="text-danger">(Vị trí, càng nhỏ thì càng lên
                            đầu)</small></label>
                    <div class="input-group">
                        <input type="number" class="form-control {{ $errors->has('sort_order') ? 'is-invalid' : '' }}"
                               min="0" placeholder="0"
                               name="sort_order" id="sort_order"
                               value="{{ old('sort_order', @$data->sort_order) }}">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>