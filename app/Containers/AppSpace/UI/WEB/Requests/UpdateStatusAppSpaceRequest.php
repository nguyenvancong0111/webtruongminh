<?php

namespace App\Containers\AppSpace\UI\WEB\Requests;

use App\Containers\AppSpace\Models\AppSpace;
use App\Ship\core\Foundation\BladeHelper;
use App\Ship\Parents\Requests\Request;

class UpdateStatusAppSpaceRequest extends Request
{
    protected $access = [
        'permissions' => 'appspace-edit',
        'roles' => 'admin',
    ];

    protected $decode = [];

    protected $urlParameters = [
        'id',
    ];

    public function rules()
    {
        return [
            'id' => ['required', sprintf('exists:%s,id', AppSpace::TABLE_NAME)],
            'status' => ['required', 'in:' . implode(',', array_keys(BladeHelper::STATUS_TEXT))],
        ];
    }

    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
