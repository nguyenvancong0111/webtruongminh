<?php

namespace App\Containers\AppSpace\UI\WEB\Requests;

use App\Containers\AppSpace\Models\AppSpace;
use App\Ship\Parents\Requests\Request;

class UpdateAppSpaceRequest extends Request
{
    protected $access = [
        'permissions' => 'appspace-edit',
        'roles' => 'admin',
    ];

    protected $decode = [
        // 'id',
    ];

    protected $urlParameters = [
        // 'id',
    ];

    public function rules()
    {
        return [
            'id' => ['required', sprintf('exists:%s,id', AppSpace::TABLE_NAME)],

            'description.*.name' => 'bail|required|string|max:255',
            'description.*.short_description' => 'bail|nullable|string|max:255',
            'description.*.description' => 'bail|nullable|string|max:500',
            'description.*.slug' => 'bail|nullable|string|max:255',

            'status' => 'bail|nullable|numeric|in:1,2',
            'sort_order' => 'bail|nullable|numeric|min:0',

            'image' => 'bail|nullable|mimes:jpeg,jpg,png|max:15360',
            'image_two' => 'bail|nullable|mimes:jpeg,jpg,png|max:15360',
        ];
    }

    public function attributes()
    {
        return [
            'description.1.name' => 'Tên kho ứng dụng',
            'description.2.name' => 'Tên giai đoạn tiếng anh',
            'description.3.name' => 'Tên giai đoạn tiếng trung',
            'description.*.short_description' => 'Mô tả ngắn',
            'description.*.description' => 'Nội dung',
            'description.*.slug' => 'Đường dẫn',

            'status' => 'Trạng thái',
            'sort_order' => 'Sắp xếp',

            'image' => 'Hình ảnh 1',
            'image_two' => 'Hình ảnh 2',
        ];
    }

    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
