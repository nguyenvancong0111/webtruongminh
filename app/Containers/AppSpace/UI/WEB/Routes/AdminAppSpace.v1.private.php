<?php

/** @var Route $router */
$router->group([
    'middleware' => ['auth:admin'],
    'namespace' => '\App\Containers\AppSpace\UI\WEB\Controllers\Admin',
    'domain' => sprintf('admin.%s', parse_url(config('app.url'))['host']),

], function () use ($router) {
    $router->resource('/app-space', 'Controller')->names([
        'index' => 'admin.appspace.index',
        'create' => 'admin.appspace.create',
        'store' => 'admin.appspace.store',
        'edit' => 'admin.appspace.edit',
        'update' => 'admin.appspace.update',
        'destroy' => 'admin.appspace.destroy',
    ])->except(['show']);

    $router->post('/app-space/update-status/{id}', ['as' => 'admin.appspace.update-status', 'uses' => 'Controller@updateStatus'])->where(['id' => '[0-9]+']);
});
