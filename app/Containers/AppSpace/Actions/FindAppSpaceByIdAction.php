<?php

namespace App\Containers\AppSpace\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class FindAppSpaceByIdAction extends Action
{
    public function run($id, array $withData = [], array $conditions = [], $selectFields = ['*'])
    {
        return Apiato::call('AppSpace@FindAppSpaceByIdTask', [$id], [
            ['with' => [$withData]],
            ['where' => [$conditions]],
            ['selectFields' => [$selectFields]]
        ]);
    }
}
