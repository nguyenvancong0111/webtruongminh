<?php

namespace App\Containers\AppSpace\Actions;

use App\Containers\AppSpace\Data\Repositories\AppSpaceDescRepository;
use App\Containers\AppSpace\Models\AppSpace;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class CreateAppSpaceAction extends Action
{
    public function run($request)
    {
        $data = Arr::only($request, [
            'description', 'status', 'sort_order', 'image', 'type','image_two'
        ]);
        $data['created_by'] = auth()->id();

        $objCreated = Apiato::call('AppSpace@CreateAppSpaceTask', [Arr::except($data, ['description'])]);

        if ($objCreated) {
            // Create Desc
            Apiato::call('BaseContainer@Admin\SaveModelDescTask', [app(AppSpaceDescRepository::class), $data, [], $objCreated->id, 'app_space_id']);

            // Create Log
            Apiato::call('User@CreateUserLogSubAction', [
                $objCreated->id,
                [],
                $objCreated->load('all_desc')->toArray(),
                'Tạo Kho ứng dụng',
                AppSpace::class
            ]);
        }

        $this->clearCache();

        return $objCreated;
    }
}
