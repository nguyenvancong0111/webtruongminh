<?php

namespace App\Containers\AppSpace\Actions;

use App\Containers\AppSpace\Data\Repositories\AppSpaceDescRepository;
use App\Containers\AppSpace\Models\AppSpace;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class UpdateAppSpaceAction extends Action
{
    public function run($request)
    {
        $data = Arr::only($request, [
            'id', 'description', 'status', 'sort_order', 'image', 'image_two',
        ]);

        $data['updated_by'] = auth()->id();

        $objBefore = Apiato::call('AppSpace@FindAppSpaceByIdTask', [$data['id']], [['with' => [['all_desc']]]]);
        $objUpdated = Apiato::call('AppSpace@UpdateAppSpaceTask', [$data['id'], Arr::except($data, ['description'])]);

        if ($objUpdated) {
            $originalDesc = Apiato::call('BaseContainer@Admin\GetAllModelDescTask', [app(AppSpaceDescRepository::class), $objUpdated->id, 'app_space_id']);

            Apiato::call('BaseContainer@Admin\SaveModelDescTask', [app(AppSpaceDescRepository::class), $data, $originalDesc, $objUpdated->id, 'app_space_id']);

            // Create Log
            Apiato::call('User@CreateUserLogSubAction', [
                $objUpdated->id,
                $objBefore->toArray(),
                $objUpdated->load('all_desc')->toArray(),
                'Sửa Giai kho ứng dụng',
                AppSpace::class
            ]);
        }

        $this->clearCache();

        return $objUpdated;
    }
}
