<?php

namespace App\Containers\AppSpace\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\AppSpace\Models\AppSpace;
use App\Ship\core\Foundation\BladeHelper;
use App\Ship\Parents\Actions\Action;

class UpdateStatusAppSpaceAction extends Action
{
    public function run($request)
    {
        $objUpdated = Apiato::call('AppSpace@UpdateAppSpaceTask', [$request->id, ['status' => $request->status]]);

        Apiato::call('User@CreateUserLogSubAction', [
            $request->id,
            [],
            $objUpdated->toArray(),
            (BladeHelper::STATUS_TEXT[$request->status] ?? 'N/A') . ' Kho ứng dụng',
            AppSpace::class
        ]);

        $this->clearCache();

        return $objUpdated;
    }
}
