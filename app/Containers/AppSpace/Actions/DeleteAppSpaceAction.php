<?php

namespace App\Containers\AppSpace\Actions;

use App\Containers\AppSpace\Data\Repositories\AppSpaceDescRepository;
use App\Containers\AppSpace\Models\AppSpace;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteAppSpaceAction extends Action
{
    public function run($id)
    {
        $deleted = Apiato::call('AppSpace@DeleteAppSpaceTask', [$id]);

        if ($deleted) {
            Apiato::call('BaseContainer@Admin\DeleteModelDescTask', [app(AppSpaceDescRepository::class), $id, 'app_space_id']);

            Apiato::call('User@CreateUserLogSubAction', [
                $id,
                [],
                [],
                'Xóa Giai kho ứng dụng',
                AppSpace::class
            ]);

            return $deleted;
        }

        $this->clearCache();

        return false;
    }
}
