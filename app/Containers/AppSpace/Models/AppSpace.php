<?php

namespace App\Containers\AppSpace\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Ship\core\Traits\HelpersTraits\LangTrait;
use App\Ship\Parents\Models\Model;

class AppSpace extends Model
{
    use LangTrait;

    const TABLE_NAME = 'app_space';
    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'status', 'sort_order', 'image', 'image_two', 'created_by', 'updated_by',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $resourceKey = 'app_space';

    /*relationship*/
    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', AppSpaceDesc::class, 'app_space_id', 'id');
    }

    public function desc()
    {
        return $this->hasOne(AppSpaceDesc::class, 'app_space_id', 'id');
    }

    /*function */
    public function getImageUrl($size = 'small')
    {
        return ImageURL::getImageUrl($this->image, 'appspace', $size);
    }

    public function getImageUrlTwo($size = 'small')
    {
        return ImageURL::getImageUrl($this->image_two, 'appspace', $size);
    }

}
