<?php

namespace App\Containers\AppSpace\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class AppSpaceDesc extends Model
{
    const TABLE_NAME = 'app_space_description';
    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'app_space_id', 'language_id', 'short_description', 'title', 'slug',
    ];


    public function language()
    {
        return $this->hasOne(Language::class, 'language_id', 'language_id');
    }

}
