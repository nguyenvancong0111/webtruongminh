<?php

namespace App\Containers\AppSpace\Tasks;

use App\Containers\BaseContainer\Traits\ScopeAdminBaseSearchTrait;
use App\Containers\BaseContainer\Traits\ScopeFEAvailableDataTrait;
use App\Containers\AppSpace\Data\Repositories\AppSpaceRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllAppSpacesTask extends Task
{
    use ScopeAdminBaseSearchTrait;
//    use ScopeFEAvailableDataTrait;

    protected $repository;

    public function __construct(AppSpaceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($request, $limit = 20)
    {
        return $this->returnDataByLimit($limit);
    }

}
