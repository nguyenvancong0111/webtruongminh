<?php

namespace App\Containers\AppSpace\Tasks;

use App\Containers\AppSpace\Data\Repositories\AppSpaceRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateAppSpaceTask extends Task
{
    protected $repository;

    public function __construct(AppSpaceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        } catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
