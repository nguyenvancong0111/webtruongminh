<?php

namespace App\Containers\AppSpace\Tasks;

use App\Containers\AppSpace\Data\Repositories\AppSpaceRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteAppSpaceTask extends Task
{
    protected $repository;

    public function __construct(AppSpaceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            $examCategory = $this->repository->find($id);

            return $examCategory->delete();
        } catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
