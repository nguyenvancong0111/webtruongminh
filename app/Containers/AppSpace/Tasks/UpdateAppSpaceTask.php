<?php

namespace App\Containers\AppSpace\Tasks;

use App\Containers\AppSpace\Data\Repositories\AppSpaceRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateAppSpaceTask extends Task
{
    protected $repository;

    public function __construct(AppSpaceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        } catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
