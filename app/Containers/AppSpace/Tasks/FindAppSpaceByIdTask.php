<?php

namespace App\Containers\AppSpace\Tasks;

use App\Containers\AppSpace\Data\Repositories\AppSpaceRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindAppSpaceByIdTask extends Task
{
    protected $repository;

    public function __construct(AppSpaceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        } catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
