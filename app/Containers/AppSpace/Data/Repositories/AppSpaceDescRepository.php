<?php

namespace App\Containers\AppSpace\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class AppSpaceDescRepository extends Repository
{
    protected $container = 'AppSpace';

    protected $fieldSearchable = [
        'id',
        'app_space_id',
        'language_id',
        'slug',
        'title',
        'short_description',
    ];

}
