<?php

namespace App\Containers\AppSpace\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class AppSpaceRepository extends Repository
{
    protected $container = 'AppSpace';

    protected $fieldSearchable = [
        'id',
        'status',
        'created_at',
    ];

}
