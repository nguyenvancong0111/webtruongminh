<?php

namespace App\Containers\Localization\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Collection;


class GetAllLanguageDBAction extends Action
{

    /**
     * @return  \Illuminate\Support\Collection
     */
    public function run($filters): Collection
    {
        return $this->remember(function () use($filters){
            $localizations = Apiato::call('Localization@GetAllLanguageDBTask', [$filters]);

            return $localizations;
        });
    }
}
