<?php

namespace App\Containers\Inventory\UI\WEB\Controllers;

use App\Containers\Inventory\UI\WEB\Requests\CreateInventoryRequest;
use App\Containers\Inventory\UI\WEB\Requests\DeleteInventoryRequest;
use App\Containers\Inventory\UI\WEB\Requests\GetAllInventoriesRequest;
use App\Containers\Inventory\UI\WEB\Requests\FindInventoryByIdRequest;
use App\Containers\Inventory\UI\WEB\Requests\UpdateInventoryRequest;
use App\Containers\Inventory\UI\WEB\Requests\StoreInventoryRequest;
use App\Containers\Inventory\UI\WEB\Requests\EditInventoryRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Inventory\UI\WEB\Controllers
 */
class InventoryController extends WebController
{
    /**
     * Show all entities
     *
     * @param GetAllInventoriesRequest $request
     */
    public function index(GetAllInventoriesRequest $request)
    {
        $inventories = Apiato::call('Inventory@GetAllInventoriesAction', [$request]);

        // ..
    }

    /**
     * Show one entity
     *
     * @param FindInventoryByIdRequest $request
     */
    public function show(FindInventoryByIdRequest $request)
    {
        $inventory = Apiato::call('Inventory@FindInventoryByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateInventoryRequest $request
     */
    public function create(CreateInventoryRequest $request)
    {
        // ..
    }

    /**
     * Add a new entity
     *
     * @param StoreInventoryRequest $request
     */
    public function store(StoreInventoryRequest $request)
    {
        $inventory = Apiato::call('Inventory@CreateInventoryAction', [$request]);

        // ..
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditInventoryRequest $request
     */
    public function edit(EditInventoryRequest $request)
    {
        $inventory = Apiato::call('Inventory@GetInventoryByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateInventoryRequest $request
     */
    public function update(UpdateInventoryRequest $request)
    {
        $inventory = Apiato::call('Inventory@UpdateInventoryAction', [$request]);

        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteInventoryRequest $request
     */
    public function delete(DeleteInventoryRequest $request)
    {
         $result = Apiato::call('Inventory@DeleteInventoryAction', [$request]);

         // ..
    }
}
