<?php

namespace App\Containers\Inventory\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class InventoryRepository
 */
class InventoryRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
