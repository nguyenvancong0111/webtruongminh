<?php

namespace App\Containers\Inventory\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteInventoryAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Inventory@DeleteInventoryTask', [$request->id]);
    }
}
