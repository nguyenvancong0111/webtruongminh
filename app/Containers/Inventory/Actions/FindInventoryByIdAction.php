<?php

namespace App\Containers\Inventory\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindInventoryByIdAction extends Action
{
    public function run(Request $request)
    {
        $inventory = Apiato::call('Inventory@FindInventoryByIdTask', [$request->id]);

        return $inventory;
    }
}
