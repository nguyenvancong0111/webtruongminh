<?php

namespace App\Containers\Inventory\Tasks;

use App\Containers\Inventory\Data\Repositories\InventoryRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllInventoriesTask extends Task
{

    protected $repository;

    public function __construct(InventoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
