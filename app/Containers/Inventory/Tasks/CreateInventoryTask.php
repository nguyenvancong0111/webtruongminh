<?php

namespace App\Containers\Inventory\Tasks;

use App\Containers\Inventory\Data\Repositories\InventoryRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateInventoryTask extends Task
{

    protected $repository;

    public function __construct(InventoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
