<?php

namespace App\Containers\Inventory\Tasks;

use App\Containers\Inventory\Data\Repositories\InventoryRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteInventoryTask extends Task
{

    protected $repository;

    public function __construct(InventoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
