<?php

namespace App\Containers\Order\Tasks;

use App\Containers\News\Data\Repositories\NewsRepository;
use App\Ship\core\Traits\HelpersTraits\UpdateDynamicStatusTrait;
use App\Ship\Parents\Tasks\Task;

class UpdateSomeFieldTask extends Task
{
    use UpdateDynamicStatusTrait;

    protected $repository;

    public function __construct(NewsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $id, string $field, $val) :? bool
    {
        return $this->updateDynamicStatus($id, $field, $val);
    }
}
