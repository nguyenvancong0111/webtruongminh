<?php

namespace App\Containers\Order\Tasks;

use App\Containers\Order\Data\Repositories\OrderItemLogsRepository;
use App\Ship\Parents\Tasks\Task;

class OrderItemAddLogTask extends Task
{

    protected $repository;

    public function __construct(OrderItemLogsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data = [])
    {
        $result = $this->repository->create($data);

        return $result;
    }
}
