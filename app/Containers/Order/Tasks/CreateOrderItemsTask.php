<?php

namespace App\Containers\Order\Tasks;

use App\Containers\Order\Data\Repositories\OrderItemRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Exception;

class CreateOrderItemsTask extends Task
{
    protected $items = [], $orderId = 0;
    protected $repository;

    public function __construct(OrderItemRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        try {
            $dataCreate = $this->convertItems();

            $this->repository->getModel()->insert($dataCreate);

        } catch (Exception $e) {
            throw $e;
            // throw new CreateResourceFailedException();
        }
    }

    public function setItems(array $items): self
    {
        $this->items = $items;
        return $this;
    }

    public function setOrderId(int $orderId): self
    {
        $this->orderId = $orderId;
        return $this;
    }

    private function convertOptions(array $options)
    {
        return json_encode($options);
    }

    private function convertItems()
    {
        $dataCreate = [];
        foreach ($this->items as $item) {
            $dataCreate[] = [
                'order_id' => $this->orderId,
                'course_id' => $item['id'],
                'name' => $item['name'],
                'slug' => $item['slug'],
                'price' => $item['price'],
                'quantity' => $item['quan'],
                'time' => isset($item['opt']['opt']) ? $item['opt']['opt']['time'] : 0,
                /*'month' => $item['opt']['month'],*/
                'img' => $item['opt']['img_or'],
                'opts' => $this->convertOptions($item['opt']),
                'created_at' => Carbon::now(),
            ];
        }

        return $dataCreate;
    }
}
