<?php

namespace App\Containers\Order\Tasks;

use App\Containers\Localization\Models\Language;
use App\Containers\Order\Data\Criterias\FilterOrderItemCriteria;
use App\Containers\Order\Data\Repositories\OrderItemRepository;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Order\Data\Criterias\FilterOrderLogsCriteria;

class GetOrderItemByOrderIDTask extends Task
{

    protected $repository;

    public function __construct(OrderItemRepository $repository)
    {
        $this->repository = $repository;

    }

    public function run($orderID)
    {
        return $this->repository->where('order_id', $orderID)->get();
    }
} // End class
