<?php

namespace App\Containers\Order\Tasks;

use App\Containers\Localization\Models\Language;
use App\Containers\Order\Data\Criterias\FilterOrderItemCriteria;
use App\Containers\Order\Data\Repositories\OrderItemRepository;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Order\Data\Criterias\FilterOrderLogsCriteria;

class FindOrderItemByIDTask extends Task
{

    protected $repository;

    public function __construct(OrderItemRepository $repository)
    {
        $this->repository = $repository;

    }

    public function run()
    {
        return $this->repository->first();
    }

    public function filterOrderItems($transporter) {
      $this->repository->pushCriteria(new FilterOrderItemCriteria($transporter));
    }
} // End class
