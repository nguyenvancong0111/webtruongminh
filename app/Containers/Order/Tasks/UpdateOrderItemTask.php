<?php

namespace App\Containers\Order\Tasks;

use App\Containers\Order\Data\Repositories\OrderItemRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateOrderItemTask extends Task
{

    protected $repository;

    public function __construct(OrderItemRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw $exception;
        }
    }
}
