<?php

namespace App\Containers\Order\Tasks;

use App\Containers\Localization\Models\Language;
use App\Containers\Order\Data\Criterias\FilterOrderItemCriteria;
use App\Containers\Order\Data\Repositories\OrderItemRepository;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Order\Data\Criterias\FilterOrderLogsCriteria;

class GetAllOrderItemsByCustomerIDTask extends Task
{

    protected $repository;

    public function __construct(OrderItemRepository $repository)
    {
        $this->repository = $repository;

    }

    public function run(array $filters = [], Language $currentLang = null, bool $noPaginate = true, int $limit = 12, int $currentPage = 1)
    {
        if (!empty($filters['customer_id'])) {
            $this->repository->whereHas('order', function ($query) use ($filters) {
                $query->where('orders.customer_id', $filters['customer_id']);
            });
        }
        $this->repository->orderBy('created_at', 'DESC');
        return $noPaginate ? $this->repository->limit($limit) : $this->repository->paginate($limit);
    }

    public function filterOrderItems($transporter) {
      $this->repository->pushCriteria(new FilterOrderItemCriteria($transporter));
    }
} // End class
