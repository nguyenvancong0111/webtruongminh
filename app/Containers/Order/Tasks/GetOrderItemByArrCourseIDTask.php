<?php

namespace App\Containers\Order\Tasks;

use App\Containers\Localization\Models\Language;
use App\Containers\Order\Data\Criterias\FilterOrderItemCriteria;
use App\Containers\Order\Data\Repositories\OrderItemRepository;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Order\Data\Criterias\FilterOrderLogsCriteria;
use Illuminate\Database\Eloquent\Builder;

class GetOrderItemByArrCourseIDTask extends Task
{

    protected $repository;

    public function __construct(OrderItemRepository $repository)
    {
        $this->repository = $repository;

    }

    public function run($arrID)
    {
        $this->repository->whereIn('course_id', $arrID);
        $this->repository->with(['order' => function($q){
            $q->where('payment_status', 0);
            $q->select('id', 'customer_id', 'payment_status');
        }]);
        $this->repository->whereHas('order', function (Builder $q){
           $q->where('orders.payment_status', 0);
        });
        $this->repository->select('id', 'order_id', 'course_id', 'price');
        return $this->repository->get()->toArray();
    }
} // End class
