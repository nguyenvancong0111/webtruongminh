<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 12:15:29
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-01 10:36:34
 * @ Description: Happy Coding!
 */

namespace App\Containers\Order\Tasks;

use App\Containers\Order\Data\Repositories\OrderRepository;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindOrderByCodeTask extends Task
{
    protected $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(string $token)
    {
        try {
            return $this->repository->where('code', $token)->first();
        } catch (Exception $exception) {
            throw $exception;
            // throw new NotFoundException();
        }
    }

    public function selectFields(array $column = ['*']): self
    {
        $this->repository->pushCriteria(new SelectFieldsCriteria($column));
        return $this;
    }
}
