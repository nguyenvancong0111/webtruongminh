<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 17:28:48
 * @ Description: Happy Coding!
 */

namespace App\Containers\Order\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class OrderItemTypeAction extends BaseEnum
{
    /**
     * Tiếp nhận hồ sơ
     */
    const RECEIVE = 1;

    /**
     * Đang xử lý
     */
    const PROCESSING = 2;

    /**
     * Hoàn thành
     */
    const COMPLETE = 3;


    const TEXT = [
        self::RECEIVE => 'Tiếp nhận hồ sơ',
        self::PROCESSING => 'Đang xử lý',
        self::COMPLETE => 'Hoàn thành',
    ];
}
