<?php

namespace App\Containers\Order\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class OrderPaymentStatus extends BaseEnum
{
    /**
     * Đơn lỗi thanh toán Online
     */
    const ERROR_PAY = -2;
    /**
     * Đơn chưa thanh toán
     */
    const NO_PAY = -1;

    /**
     * Đơn đã thanh toán - OnePay
     */
    const SUCCESS_PAY = 0;

    const TEXT = [
        self::ERROR_PAY => 'Lỗi thanh toán',
        self::NO_PAY => 'Chưa thanh toán',
        self::SUCCESS_PAY => 'Đã thanh toán',
    ];
}
