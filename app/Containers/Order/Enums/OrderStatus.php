<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 17:28:48
 * @ Description: Happy Coding!
 */

namespace App\Containers\Order\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class OrderStatus extends BaseEnum
{
    /**
     * Đơn hủy
     */
    const CANCEL = -1;

    /**
     * Đơn hàng mới
     */
    const NEW_ORDER = 1;

    /**
     * Đã tiếp nhận
     */
    const ASSIGNED = 7;

    /**
     * Đơn hoàn thành
     */
    const DONE = 999;

    /**
     * @deprecated
     *
     * Đã thanh toán
     */
    const PAID = 4;

    const TEXT = [
        self::NEW_ORDER => 'Chờ xác nhận',
        self::ASSIGNED => 'Đã tiếp nhận',
        self::DONE => 'Thành công',
        self::CANCEL => 'Đã hủy bỏ',
    ];
}
