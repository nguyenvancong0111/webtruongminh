<?php

namespace App\Containers\Order\Models;

use App\Containers\Order\Traits\OrderCustomerTrait;
use App\Containers\Order\Traits\OrderDeliveryTrait;
use App\Containers\Order\Traits\OrderLocationTrait;
use App\Ship\Parents\Models\Model;
use App\Containers\User\Models\User;
use App\Containers\Order\Traits\PriceTrait;
use App\Containers\Order\Traits\PaymentTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Containers\Order\Traits\OrderScopeTrait;
use App\Containers\Order\Traits\OrderStatusTrait;
use App\Containers\Order\Traits\OrderPaymentTrait;
use App\Containers\Settings\Models\DeliveryType;
use App\Containers\Settings\Models\PaymentType;
use App\Ship\core\Traits\HelpersTraits\DateTrait;

class ShopCart extends Model
{

    protected $table = 'shopcart';


    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
} // End class
