<?php

namespace App\Containers\Order\Models;

use App\Containers\Customer\Models\Customer;
use App\Containers\Order\Traits\OrderItemTypeActionTrait;
use App\Ship\Parents\Models\Model;

class OrderItemLogs extends Model
{
    use OrderItemTypeActionTrait;
  protected $table = 'order_item_logs';
  protected $guarded = [];

  protected $attributes = [];

  protected $hidden = [];

  protected $casts = [];

  protected $dates = [
    'created_at',
    'updated_at',
  ];

  /**
   * A resource key to be used by the the JSON API Serializer responses.
   */
  protected $resourceKey = 'orderitemlogs';

    public  function lawyer(){
        return $this->hasOne(Customer::class, 'id', 'lawyer_id');
    }
} // End class
