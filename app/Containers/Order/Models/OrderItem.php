<?php

namespace App\Containers\Order\Models;

use App\Containers\Course\Models\Course;
use App\Containers\Customer\Models\Customer;
use App\Containers\Customer\Models\CustomerCourse;
use App\Containers\Order\Traits\OrderItemTrait;
use App\Containers\Order\Traits\OrderItemTypeActionTrait;
use App\Containers\Order\Traits\OrderItemTypeTrait;
use App\Containers\Product\Models\Product;
use App\Containers\Service\Models\Service;
use App\Ship\Parents\Models\Model;

class OrderItem extends Model
{
    use OrderItemTrait, OrderItemTypeTrait, OrderItemTypeActionTrait;

    protected $table = 'order_items';

    protected $appends = [
      'price_currency',
      'total_price_product_currency'
    ];

    protected $fillable = [];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'orderitems';

    public function course() {
      return $this->belongsTo(Course::class, 'course_id', 'id');
    }

    public function order(){
        return $this->hasOne(Order::class, 'id', 'order_id');
    }

    public function activeCode(){
        return $this->hasOne(CustomerCourse::class, 'order_item_id', 'id');
    }

    public function itemLogs(){
        return $this->hasMany(OrderItemLogs::class, 'order_item_id', 'id')->orderByRaw('id DESC, type_action ASC');
    }

    public function lawyer(){
        return $this->hasOne(Customer::class, 'id', 'lawyer_id');
    }

} // End class
