<?php

namespace App\Containers\Order\UI\WEB\Controllers;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Customer\Actions\AttachCustomerCourseAction;
use App\Containers\Order\Actions\GenActiveCodeOrderItemsAction;
use App\Containers\Order\Actions\GetOrderItemByArrIDAction;
use App\Containers\Order\Actions\GetOrderItemByOrderIDAction;
use App\Containers\Order\Actions\OrderItemAddLogAction;
use App\Containers\Order\Actions\UpdateOrderItemAction;
use App\Containers\Order\Events\FrontEnd\Events\OrderSuccessEvent;
use App\Containers\Order\Models\Order;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Order\Actions\AcceptOrderAction;
use App\Containers\Order\Actions\DeliveriedOrderAction;
use App\Containers\Order\Actions\DeliveringOrderAction;
use App\Containers\Order\Actions\ExportOrderAction;
use App\Containers\Order\Actions\MarkCancelOrderAction;
use App\Containers\Order\Actions\MarkFinishOrderAction;
use App\Containers\Order\Actions\MarkPaidOrderAction;
use App\Containers\Order\Actions\MarkRefundOrderAction;
use App\Containers\Order\Actions\MarkWaitingPaymentOrderAction;
use App\Containers\Order\Actions\UnAcceptOrderAction;
use App\Containers\Order\Events\Admin\Events\RollbackProductStockEvent;
use App\Ship\Parents\Controllers\WebController;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Containers\Order\UI\WEB\Requests\AcceptOrderRequest;
use App\Containers\Order\UI\WEB\Requests\GetAllOrdersRequest;
use App\Containers\Order\UI\WEB\Requests\FindOrderByIdRequest;
use App\Containers\Order\UI\WEB\Requests\MarkPaidOrderRequest;
use App\Containers\Order\UI\WEB\Requests\UnAcceptOrderRequest;
use App\Containers\Order\UI\WEB\Requests\MarkCancelOrderRequest;
use App\Containers\Order\UI\WEB\Requests\MarkFinishOrderRequest;
use App\Containers\Order\UI\WEB\Requests\MarkRefundOrderRequest;
use App\Containers\Order\Events\Admin\Events\SubtractionProductStockEvent;
use App\Containers\Order\UI\WEB\Requests\DeliveriedOrderRequest;
use App\Containers\Order\UI\WEB\Requests\DeliveringOrderRequest;
use App\Containers\Order\UI\WEB\Requests\ExportOrderRequest;
use App\Containers\Order\UI\WEB\Requests\MarkWaitingPaymentOrderRequest;
use App\Containers\Order\UI\WEB\Requests\ResendOrderMailRequest;

use App\Ship\Transporters\DataTransporter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use function GuzzleHttp\Psr7\uri_for;


/**
 * Class OrderProcessController
 *
 * @package App\Containers\Order\UI\WEB\Controllers
 */
class OrderProcessController extends WebController
{
    use ApiResTrait;


    /**
     * Đánh dấu đơn hàng là đã thanh toán
     *
     * @param MarkPaidOrderRequest $request
     */
    public function markPaidOrder(MarkPaidOrderRequest $request)
    {
        $request->merge(['payment_time' => date(now()), 'is_admin_accept' => auth()->guard(config('auth.guard_for.admin'))->id()]);
        $order = app(MarkPaidOrderAction::class)->run($request->toArray());
        if ($order->payment_status == 0) {
            //Create Customer Course
            $customer_course = app(GenActiveCodeOrderItemsAction::class)->skipCache(true)->run($order);
            app(AttachCustomerCourseAction::class)->skipCache(true)->run($order->customer, $customer_course);
            //send mail
            OrderSuccessEvent::dispatch($order->id);

        }

        $msg = __('Đánh dấu là đơn hàng đã thanh toán thành công');
        return $this->orderProcessResponse($order, $msg, $request);
    }

    private function orderProcessResponse(Order $order, string $msg = '', $request)
    {
        if ($request->ajax()) {
            return $this->sendResponse($order, $msg);
        }
        return redirect()->back()->with([
            'flash_level' => 'success',
            'flash_message' => $msg
        ]);
    }
    private function makeCodeOrder($number_of_characters = 10)
    {

        $permitted_chars = '0123456789';
        $input_length = strlen($permitted_chars);
        $random_string = '';

        for ($i = 0; $i < $number_of_characters; $i++) {
            $random_string .= $permitted_chars[mt_rand(0, $input_length - 1)];
        }
        return strtoupper($random_string);
    }
} // End class
