<?php

namespace App\Containers\Order\UI\WEB\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Customer\Actions\FrontEnd\ActiveCustomerCourseAction;
use App\Containers\Customer\Actions\FrontEnd\CheckCustomerCourseActiveAction;
use App\Containers\Customer\Actions\GetAllCustomersAction;
use App\Containers\Order\Actions\GetOrderItemByArrIDAction;
use App\Containers\Order\Enums\OrderPaymentStatus;
use App\Containers\Order\Events\Admin\Events\ReSendActivecodeMailEvent;
use App\Containers\Order\UI\WEB\Requests\ActiveCourseRequest;
use App\Containers\Order\UI\WEB\Requests\ResendActiveCodeMailRequest;
use App\Ship\Parents\Exceptions\Exception;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\BaseContainer\Actions\CreateBreadcrumbAction;
use App\Containers\Order\Actions\CreateOrderAction;
use App\Containers\Order\Actions\GetAllOrdersAction;
use App\Containers\Order\Enums\OrderStatus;
use App\Ship\Parents\Controllers\WebController;
use App\Ship\Parents\Controllers\AdminController;
use App\Containers\Order\UI\WEB\Requests\EditOrderRequest;
use App\Containers\Order\UI\WEB\Requests\StoreOrderRequest;
use App\Containers\Order\UI\WEB\Requests\CreateOrderRequest;
use App\Containers\Order\UI\WEB\Requests\DeleteOrderRequest;
use App\Containers\Order\UI\WEB\Requests\UpdateOrderRequest;
use App\Containers\Order\UI\WEB\Requests\GetAllOrdersRequest;
use App\Containers\Order\UI\WEB\Requests\FindOrderByIdRequest;
use App\Containers\Settings\Enums\PaymentStatus;
use Illuminate\Support\Facades\Auth;

/**
 * Class Controller
 *
 * @package App\Containers\Order\UI\WEB\Controllers
 */
class OrderController extends AdminController
{
    public function __construct()
    {
        $this->title = 'Đơn hàng';
        $method = request()->route()->getActionMethod();
        if (in_array($method, ['edit', 'show', 'update', 'delete'])) {
            $this->dontUseShareData = true;
        }

        parent::__construct();
    }

    /**
     * Show all entities
     *
     * @param GetAllOrdersRequest $request
     */
    public function index(GetAllOrdersRequest $request)
    {
        $filters = $request->all();
        app(CreateBreadcrumbAction::class)->run('list', $this->title, 'admin.orders.index');

        $orders = app(GetAllOrdersAction::class)->skipCache()->run(
            $filters,
            ['customer:id,fullname,email,phone', 'logs' => function ($query) {
                $query->where('action_key', 'email_admin')->select('id', 'order_id');
            }
            ],

            ['id', 'customer_id', 'fullname', 'email', 'phone', 'code', 'total_price', 'fee_shipping', 'created_at', 'status', 'user_id', 'payment_status', 'payment_type', 'delivery_type', 'coupon_code', 'coupon_value'], 20,
            $request->page ?? 1
        );

//     dd($orders);

        $users = collect([]);
        if (isset($filters['user_id']) && $filters['user_id']) {
            $users = Apiato::call('User@GetAllUserNoLimitAction', [
                [
                    ['id', '=', $filters['user_id']]
                ],

                ['id', 'name', 'email']
            ]);
        }
        return view('order::index', [
            'orders' => $orders,
            'input' => $filters,
            'ordersType' => OrderPaymentStatus::TEXT,
            'users' => $users
        ]);
    }

    /**
     * Show one entity
     *
     * @param FindOrderByIdRequest $request
     */
    public function show(FindOrderByIdRequest $request)
    {
        $order = Apiato::call('Order@FindOrderByIdAction', [
            $request->id,
            ['orderItems', 'orderItems.activeCode', 'customer:id,fullname,email', 'notes']
        ]);


        return view('order::show', [
            'order' => $order,
            'input' => $request->all()
        ]);
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateOrderRequest $request
     */
    public function create(CreateOrderRequest $request)
    {
        // ..
    }

    /**
     * Add a new entity
     *
     * @param StoreOrderRequest $request
     */
    public function store(StoreOrderRequest $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $order = app(CreateOrderAction::class)->run($data);

        // ..
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditOrderRequest $request
     */
    public function edit(EditOrderRequest $request)
    {
        $order = Apiato::call('Order@GetOrderByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateOrderRequest $request
     */
    public function update(UpdateOrderRequest $request)
    {
        $order = Apiato::call('Order@UpdateOrderAction', [$request]);

        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteOrderRequest $request
     */
    public function delete(DeleteOrderRequest $request)
    {
        $result = Apiato::call('Order@DeleteOrderAction', [$request]);

        // ..
    }

    public function reSendMail(ResendActiveCodeMailRequest $request){
        try {
            ReSendActivecodeMailEvent::dispatch($request->order_item, $request->course_id);
            return \FunctionLib::ajaxRespondV2(true, 'Success', 'Mã kích hoạt đã được gửi');
        }catch (Exception $e){
            return \FunctionLib::ajaxRespondV2(false, 'Error', $e->getMessage());
        }
    }

    public  function activeCourse(ActiveCourseRequest $request){
        $filter = [
            'customer_id' => (int)$request->customer_id,
            'course_id' => (int)$request->course_id
        ];
        if (isset($request->code) && !empty($request->code)){
            $filter['active_code'] = $request->code;
        }
        $check = app(CheckCustomerCourseActiveAction::class)->skipCache(true)->run($filter);
        if ($check){
            $customer_course = app(ActiveCustomerCourseAction::class)->skipCache(true)->run(
                ['id' => $check->id, 'customer_id' => (int)$request->customer_id, 'course_id' => (int)$request->course_id, 'active_code' => $check->active_code],
                ['is_active' => 1, 'start_time' => date(now())], []
            );
//            dd($customer_course);

            if ($customer_course && $customer_course->is_active == 1){
                return \FunctionLib::ajaxRespondV2(true, 'Success', 'Kích hoạt thành công');
            }
            return \FunctionLib::ajaxRespondV2(false, 'Error', 'Kích hoạt thất bại');
        }
        return \FunctionLib::ajaxRespondV2(false, 'Error', 'Học viên chưa thanh toán');
    }
}
