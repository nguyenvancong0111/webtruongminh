@extends('basecontainer::admin.layouts.default_for_iframe')
@section('content')
    <div class="row" id="sectionContent">
        <div class="col-12">
            <div class="card mb-0">
                <div class="card-header d-flex">
                    <button class="btn btn-link">Thông tin đơn hàng: <strong>{{ $order->code }}</strong></button>
                    <button type="button" class="btn btn-secondary ml-auto mr-2" onclick='return closeFrame()'>Đóng lại</button>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="" class="font-weight-bold h5">Thông tin Đơn</label>
                                <p><span class="font-weight-bold"> ID: </span>{{ $order->id }}</p>
                                <p><span class="font-weight-bold"> Mã đơn: </span>{{ $order->code }}</p>
                                <p><span class="font-weight-bold"> Hình thức thanh toán: </span>{{ $order->getPaymentTypeText() }}</p>

                                <p><span class="font-weight-bold">Trạng thái thanh toán: </span>
                                    <span class="text-{{ $order->payment_status == 0 ? 'success' : 'danger' }}">{{ $order->PaymentStatusText() }}</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="" class="font-weight-bold h5">Thông tin Học viên</label>
                                <p><span class="font-weight-bold">Họ và Tên:</span> {{$order->customer->fullname}}</p>
                                <p><span class="font-weight-bold">Email:</span> {{$order->customer->email}}</p>
                                <p><span class="font-weight-bold">Số điện thoại:</span> {{$order->customer->phone}}</p>
                                <p><span class="font-weight-bold">Đặt hàng lúc:</span> {{$order->created_at}}</p>
                            </div>
                        </div>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr class="table-info">
                                <th>#</th>
                                <th>Khóa học</th>
                                <th>Tùy chọn</th>
                                <th>Thành tiền</th>
                                @if($order->payment_status == 0)
                                <th>Mã / Tình trạng</th>
                                @endif
                            </tr>
                            </thead>

                            <tbody>
                            @if ($order->orderItems->isNotEmpty())
                                @foreach ($order->orderItems as $k => $orderItem)
                                    <tr>
                                        <td>
                                            <span class="d-block">{{$loop->index  + 1}}</span>
                                        </td>
                                        <td>
                                            <p>{{ $orderItem->name }}</p>
                                        </td>
                                        <td>
                                            @php
                                                $opt = json_decode($orderItem->opts, true);
                                            @endphp
                                            @if(isset($opt['opt']))
                                                <p>{!! implode(': ', ['<strong>'.$opt['opt']['title_gr'].'</strong>', $opt['opt']['title']]) !!}</p>
                                            @endif
                                        </td>
                                        <td>{{\Apiato\Core\Foundation\FunctionLib::priceFormat( $orderItem->price) }}</td>
                                        @if($order->payment_status == 0)
                                            <td>
                                                @if(!empty($orderItem->activeCode))
                                                    <p><span class="font-weight-bold">Mã kích hoạt: </span> {{$orderItem->activeCode->active_code}}</p>
                                                    <p><span class="font-weight-bold">Tình trạng: </span> {!!$orderItem->activeCode->is_active == 1 ? '<span class="text-success">Đã kích hoạt</span' : '<span class="text-danger">Chưa kích hoạt</span>' !!}</p>
                                                    @if($orderItem->activeCode->is_active == 0)
                                                        <a href="javascript:;" class="btn btn-outline-success" title="Gửi lại mã kích hoạt" onclick="reSendMail('{{route('admin.orders.re_send_mail')}}', '{{$orderItem->id}}', {{$orderItem->course_id}})"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>
                                                        <a href="javascript:;" class="btn btn-outline-secondary" title="Kích hoạt ngay" onclick="activeNow('{{route('admin.orders.active_course')}}', {{$order->customer->id}}, {{$orderItem->course_id}}, '{{$orderItem->activeCode->active_code}}')"><i class="fa fa-unlock" aria-hidden="true"></i></a>
                                                    @endif
                                                @else
                                                    <p class="text-danger w-100 justify-content-center d-flex font-weight-bold">NaN</p>
                                                @endif
                                            </td>
                                        @endif

                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.Card-body -->
            </div><!-- /.End card -->
        </div>
    </div>
@endsection
