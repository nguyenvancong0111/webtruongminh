<tr class="font-sm {{ $order->isCancelOrder() ? 'table-danger' : '' }}"
    ondblclick="return loadIframe(this, '{{ route('admin.orders.show', ['id' => $order->id]) }}')">
    <td class="py-0">
        <p class="mb-1">
            ID:
            <a href="javascript:void(0)" class="font-italic"
                onclick="return loadIframe(this, '{{ route('admin.orders.show', ['id' => $order->id]) }}')">
                #{{ $order->id }}
            </a>
        </p>
        <p class="mb-1">Mã đơn: <em><b>{{ $order->code }}</b></em></p>
        <p class="mb-1">
            @if ($order->logs->isNotEmpty())
                <img src="{{ asset('admin/img/discount/email.png') }}" title="Đã gửi mail cho khách">
            @endif

            @if (!empty($order->coupon_code))
                <img src="{{ asset('admin/img/discount/discount-money.png') }}" data-toggle="tooltip" data-placement="bottom" title="Sử dụng mã giảm giá: {{$order->coupon_code}}">
            @endif
        </p>
    </td>
    <td class="py-1">
        <p class="mb-1"><i class="fa fa-user"></i> &nbsp;{{ ucwords($order->fullname) }}</p>

        @if (!empty($order->email))
            <p class="mb-1"><b>@</b> <a href="mailto:{{ $order->email }}">{{ $order->email }}</a></p>
        @endif

        @if (!empty($order->phone))
            <p class="mb-1"><i class="fa fa-mobile"></i> &nbsp;&nbsp;{{ $order->phone }}</p>
        @endif
    </td>
    <td>
        <p class="mb-1"><i class="fa fa-money" aria-hidden="true"></i> {{ \FunctionLib::priceFormat($order->fee_shipping + $order->total_price)}}</p>
    </td>
    <td class="text-center py-0">{{ $order->created_at }}</td>
    <td class="py-0 mb-1">
        <p class="mb-1"><i class="fa fa-credit-card" title="Hình thức thanh toán" aria-hidden="true"></i>
            {{ $order->getPaymentTypeText() }}</p>
        <p class="mb-1">
            <strong class="{{$order->getPaymentStatusCssClass()}}">
                <i class="fa fa-credit-card-alt" aria-hidden="true"></i>
                {{ $order->PaymentStatusText() }}
            </strong>

        </p>
    </td>
    <td class="py-0 text-center">
        <div class="btn-group">
            @dropdown('Chọn')
            <div class="dropdown-menu">
                <input type="hidden" class="customer-hidden" value='@bladeJson($order)'>

                <a class="dropdown-item" href="javascript:void(0)"
                    onclick="return loadIframe(this, '{{ route('admin.orders.show', ['id' => $order->id]) }}')">
                    Chi tiết đơn hàng
                </a>

                @if($order->payment_type == 2 && $order->payment_status == -1)
                    <a class="dropdown-item" href="javascript:void(0)" onclick="alertChangeOrderState('{!! route('admin.orders.process.confirm-paid', ['id' => $order->id]) !!}', 'Đánh dấu là đơn hàng đã thanh toán');">
                        Xác nhận thanh toán
                    </a>
                @endif
            </div>
        </div>
    </td>
</tr>
