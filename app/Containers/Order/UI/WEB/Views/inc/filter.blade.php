<li class="nav-item ml-auto">
    <a class="nav-link"
       href="javascript:void(0)"
       data-toggle="modal" data-target="#modalOrderFilter">
        <i class="fa fa-filter"></i> Bộ lọc
    </a>
</li>

<!-- Modal -->
<div class="modal fade" id="modalOrderFilter" role="dialog" aria-labelledby="modalOrderFilterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg mw-90" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Bộ lọc đơn hàng</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="{{ route('admin.orders.index') }}" method="GET">
                <input type="hidden" name="is_filter" value=1>
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="">Mã đơn/ID đơn</label>
                                <input type="text" name="order_id" class="form-control" value="{{ $input['order_id'] }}">
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label for="">Trạng thái thanh toán</label>
                                <select name="payment_status" class="select2  form-control" style="width: 100%;">
                                    <option value="" selected>---Chọn---</option>
                                    @foreach ($ordersType as $key => $val)
                                        <option value="{{ $key }}" @if($key == ($input['payment_status'] ?? '')) selected @endif>
                                            {{ $val }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="">Ngày thao tác</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="">Từ ngày</span>
                                    </div>
                                    <input type="text"
                                           name="from_date"
                                           class="form-control"
                                           value="{{ $input['from_date'] ?? '' }}"
                                           id="date_timepicker_start">

                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="">đến</span>
                                    </div>

                                    <input type="text"
                                           name="to_date"
                                           class="form-control"
                                           value="{{ $input['to_date'] ?? '' }}"
                                           id="date_timepicker_end">
                                </div>
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label for="">Thông tin khách</label>
                                <input type="text" name="keyword" class="form-control"
                                       placeholder="Họ tên, Email hoặc SĐT" value="{{ $input['keyword'] ?? '' }}">
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-body -->

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Lọc đơn</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div>
</div>
