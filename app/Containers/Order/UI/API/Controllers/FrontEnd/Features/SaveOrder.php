<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-21 12:34:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-01 20:54:44
 * @ Description: Happy Coding!
 */

namespace App\Containers\Order\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Customer\Actions\CusGetAddressBookByIdAction;
use App\Containers\Customer\Models\Customer;
use App\Containers\Order\Actions\CreateOrderAction;
use App\Containers\Order\UI\API\Requests\FrontEnd\SaveOrderRequest;
use App\Containers\Payment\Actions\ProcessPaymentAction;
use App\Containers\Payment\Gateway\PaymentsGateway;
use App\Containers\ShoppingCart\Actions\FrontEnd\GetContentCartAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\RemoveAllSelectedCartItemAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\StoreCartAction;
use App\Containers\Order\Events\FrontEnd\Events\OrderSuccessEvent;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

trait SaveOrder
{
  public function saveOrder(SaveOrderRequest $request)
  {
    $cart = app(GetContentCartAction::class)->currentLang($this->currentLang)->instance()->onlySelected()->run();

    if (!isset($cart['count_selected']) || $cart['count_selected'] <= 0) {
      return $this->sendError('', Response::HTTP_NOT_ACCEPTABLE, __('site.hientaikhongcosanphamnaotronggio'));
    }

    // dd($cart);

    $data = array_merge(
      [
        'customer_id' => $this->user->id,
        'payment_type' => $request->payment_method,
        'delivery_type' => $request->delivery_method,
        'fee_shipping' => 0,
        'note' => $request->order_note,
        'coupon_code' => isset($cart['coupon']['coupons'][0]) ? $cart['coupon']['coupons'][0]['code'] : '',
        'coupon_value' => isset($cart['coupon']['value']) ? $cart['coupon']['value'] : 0,
        'total_price' => $cart['total'],
        'fee_shipping' => isset($cart['delivery']['value']) ? $cart['delivery']['value'] : 0,
      ],
      $this->convertAddress($this->user, $request->address_id),
    );

    $order = app(CreateOrderAction::class)->setData($data)->setItems($cart['items'])->run();

    if($order->isNewOrder()){
        OrderSuccessEvent::dispatch($order->id);
    }

    $cart = app(RemoveAllSelectedCartItemAction::class)->currentLang($this->currentLang)->instance()->run();

    app(StoreCartAction::class)->currentLang($this->currentLang)->instance()->setIdentify($this->user->id)->run();

    $transaction = App::make(PaymentsGateway::class)->charge($order);
    if ($transaction) {
      if ($transaction->id) {
        return $this->sendResponse([
          'url' => $transaction->data
        ]);
      }
    }

    return $this->sendResponse([
      'cart' => $cart,
      'token' => $order->token_tracking,
      'url' => route('web_checkout_complete', ['token' => $order->token_tracking])
    ]);
  }

  public function convertAddress(Customer $customer, int $addressId)
  {
    $address = app(CusGetAddressBookByIdAction::class)->run($customer->id, $addressId);

    return [
      'fullname' => $address->name,
      'phone' => $address->phone,
      'email' => $customer->email,
      'province_id' => $address->province_id,
      'district_id' => $address->district_id,
      'ward_id' => $address->ward_id,
      'address' => $address->address,
      'address_id' => $address->id
    ];
  }

  public function calcDelivery()
  {
  }
}
