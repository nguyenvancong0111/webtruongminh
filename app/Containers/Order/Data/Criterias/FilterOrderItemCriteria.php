<?php

namespace App\Containers\Order\Data\Criterias;

use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class FilterOrderItemCriteria extends Criteria
{
    private $transporter;

    public function __construct($transporter)
    {
        $this->transporter = $transporter;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        if (!empty($this->transporter->id)) {
            $model = $model->where('id', $this->transporter->id);
        }

        if(isset($this->transporter->course_id) && !empty($this->transporter->course_id)){
            $model = $model->where('course_id', $this->transporter->course_id);
        }

        if (!empty($this->transporter->type)) {
            $model = $model->where('type', $this->transporter->type);
        }


        if (!empty($this->transporter->from_date)) {
            $fromDate = FunctionLib::getCarbonFromVNDate($this->transporter->from_date)->toDateTimeString();
            $model = $model->whereDate('created_at', '>=', $fromDate);
        }

        if (!empty($this->transporter->to_date)) {
            $toDate = FunctionLib::getCarbonFromVNDate($this->transporter->to_date)->addDay(1)->toDateTimeString();
            $model = $model->whereDate('created_at', '<', $toDate);
        }

        if (!empty($this->transporter->arr_id)) {
            $model = $model->whereIn('id', $this->transporter->arr_id);
        }

        $model->orderBy('created_at', 'ASC');
        return $model;
    }
} // End class
