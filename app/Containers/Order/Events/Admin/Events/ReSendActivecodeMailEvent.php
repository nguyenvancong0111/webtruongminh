<?php

namespace App\Containers\Order\Events\Admin\Events;

use App\Containers\Order\Actions\FindOrderItemByIDAction;
use App\Containers\Settings\Actions\GetAllSettingsAction;
use App\Ship\Parents\Events\Event;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Queue\SerializesModels;

class ReSendActivecodeMailEvent extends Event
{
    use SerializesModels;

    public $data;
    public $settings;

    public function __construct($order_item, $course_id)
    {
        $data = app(FindOrderItemByIDAction::class)->run(
            new DataTransporter(['id' => $order_item, 'course_id' => $course_id]), ['order:id,customer_id', 'order.customer:id,fullname,email', 'activeCode']
        );
        $data = $data->toArray();
        $data_event = [];
        if (!empty($data)){
            $data_event['item'] = [
                'id' => $data['id'],
                'course_id' => $data['course_id'],
                'name' => $data['name'],
                'slug' => $data['slug'],
                'price' => $data['price'],
                'image' => \ImageURL::getImageUrl($data['img'], 'course', '70x50'),

            ];
            $data_event['customer'] = !empty($data['order']) && !empty($data['order']['customer']) ? ['fullname' => $data['order']['customer']['fullname'], 'email' => $data['order']['customer']['email']] : [];
            $data_event['active_code'] = !empty($data['active_code']) && $data['active_code']['is_active'] == 0 ? $data['active_code']['active_code'] : '';
        }
        $this->data = $data_event;

        $this->settings = app(GetAllSettingsAction::class)->run('Array', true);
    }

    public function broadcastOn()
    {
        return [];
    }
}
