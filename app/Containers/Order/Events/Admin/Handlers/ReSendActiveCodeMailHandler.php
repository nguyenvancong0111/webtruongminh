<?php

namespace App\Containers\Order\Events\Admin\Handlers;

use App\Containers\BaseContainer\Events\Handlers\BaseFrontEventHandler;
use App\Containers\Order\Events\Admin\Events\ReSendActivecodeMailEvent;
use Illuminate\Support\Facades\Mail;

class ReSendActiveCodeMailHandler extends BaseFrontEventHandler
{
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(ReSendActivecodeMailEvent $event)
    {
        $messageBody = view('ubacademy::pc.customer.email.re-send-active-code', ['data' => $event->data, 'settings' => $event->settings])->render();

        Mail::html($messageBody, function ($message) use($event) {
            $message->to($event->data['customer']['email']);
            $message->subject('['.env('APP_NAME').'] Mã kích hoạt khóa học');
        });
    }
}
