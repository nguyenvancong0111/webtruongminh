<?php

namespace App\Containers\Order\Events\FrontEnd\Handlers;

use App\Containers\BaseContainer\Events\Handlers\BaseFrontEventHandler;
use App\Containers\Bizfly\Actions\Mail\StoreMailCronAction;
use App\Containers\Order\Events\FrontEnd\Events\OrderSuccessEvent;
use App\Containers\Order\Mails\OrderSuccessMail;
use App\Containers\Settings\Actions\GetAllSettingsAction;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class OrderSendMailSuccessAdminHandler extends BaseFrontEventHandler implements ShouldQueue
{
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(OrderSuccessEvent $event)
    {
        $messageBody = view('ubacademy::pc.checkout.email.order-admin', ['data' => $event->order, 'settings' => $event->settings, 'bank' => $event->bank])->render();
        Mail::html($messageBody, function ($message) use($event) {
            $message->to($event->settings['contact']['email_order']);
            $message->subject(sprintf('['.env('APP_NAME').'] Đơn hàng mới "%s"', $event->order->code));
        });
    }
}
