<?php

namespace App\Containers\Order\Events\FrontEnd\Handlers;

use App\Containers\BaseContainer\Events\Handlers\BaseFrontEventHandler;
use App\Containers\Order\Events\FrontEnd\Events\OrderSuccessEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class OrderSendMailSuccessCustomerHandler extends BaseFrontEventHandler implements ShouldQueue
{
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(OrderSuccessEvent $event)
    {
        $messageBody = view('ubacademy::pc.checkout.email.order-customer', ['data' => $event->order, 'settings' => $event->settings, 'bank' => $event->bank])->render();
        Mail::html($messageBody, function ($message) use($event) {
            $message->to($event->order->email);
            $message->subject(sprintf('['.env('APP_NAME').'] Đơn hàng "%s" đã được tiếp nhận', $event->order->code));
        });
    }
}
