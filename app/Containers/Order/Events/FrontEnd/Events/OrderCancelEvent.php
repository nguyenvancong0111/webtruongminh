<?php

namespace App\Containers\Order\Events\FrontEnd\Events;

use App\Containers\Bank\Actions\GetBankListAction;
use App\Containers\Localization\Actions\GetDefaultLanguageAction;
use App\Containers\Order\Actions\FindOrderByIdAction;
use App\Containers\Settings\Actions\GetAllSettingsAction;
use App\Ship\Parents\Events\Event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class OrderCancelEvent extends Event implements ShouldQueue
{
    use SerializesModels;

    public $orderId;
    public $order;
    public $settings;

    public function __construct(int $orderId)
    {
        $this->orderId = $orderId;
        $this->order = app(FindOrderByIdAction::class)->run($orderId,['orderItems']);
        $this->settings = app(GetAllSettingsAction::class)->run('Array', true);

    }

    public function broadcastOn()
    {
        return [];
    }
}
