<?php

namespace App\Containers\Order\Events\FrontEnd\Events;

use App\Containers\Bank\Actions\GetBankListAction;
use App\Containers\Localization\Actions\GetDefaultLanguageAction;
use App\Containers\Order\Actions\FindOrderByIdAction;
use App\Containers\Settings\Actions\GetAllSettingsAction;
use App\Ship\Parents\Events\Event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class OrderSuccessEvent extends Event implements ShouldQueue
{
    use SerializesModels;

    public $orderId;
    public $order;
    public $settings;
    public $bank;

    public function __construct(int $orderId)
    {
        $this->orderId = $orderId;
        $this->order = app(FindOrderByIdAction::class)->run($orderId,['orderItems', 'orderItems.activeCode:order_item_id,active_code']);
        $this->settings = app(GetAllSettingsAction::class)->run('Array', true);
        $this->bank = app(GetBankListAction::class)->skipCache(true)->run(
            ['status' => 2], ['sort_order' => 'ASC', 'id' => 'desc'], 20, true, app(GetDefaultLanguageAction::class)->run(), [], 1
        );
    }

    public function broadcastOn()
    {
        return [];
    }
}
