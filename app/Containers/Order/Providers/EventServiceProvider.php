<?php

namespace App\Containers\Order\Providers;

use App\Containers\Order\Events\Admin\Events\ReSendActivecodeMailEvent;
use App\Containers\Order\Events\Admin\Handlers\ReSendActiveCodeMailHandler;
use App\Containers\Order\Events\FrontEnd\Events\OrderSuccessEvent;
use App\Containers\Order\Events\FrontEnd\Handlers\OrderSendMailErrorAdminHandler;
use App\Containers\Order\Events\FrontEnd\Handlers\OrderSendMailSuccessCustomerHandler;
use App\Containers\Order\Events\FrontEnd\Handlers\OrderSendMailSuccessAdminHandler;
use App\Containers\Order\Events\FrontEnd\Events\OrderCancelEvent;
use App\Containers\Order\Events\FrontEnd\Handlers\OrderSendMailCancelHandler;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        // FrontEnd
        // Thành Công (thanh toán online, chuyển khoản)
        OrderSuccessEvent::class => [
            OrderSendMailSuccessCustomerHandler::class,
            OrderSendMailSuccessAdminHandler::class
        ],
        ReSendActivecodeMailEvent::class =>[
            ReSendActiveCodeMailHandler::class
        ],
        OrderCancelEvent::class => [
            OrderSendMailErrorAdminHandler::class
        ],
    ];

    public function boot() {
        parent::boot();
    }
}
