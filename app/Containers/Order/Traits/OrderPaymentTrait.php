<?php

namespace App\Containers\Order\Traits;

trait OrderPaymentTrait
{
  public function isPaymentStatusSuccess(): bool {
    return $this->payment_status == 0;
  }

  public function isPaymentOnline():bool {
      return $this->payment_type == 1;
  }
  public function isPaymentATM():bool{
      return $this->payment_type == 2;
  }

  public function getPaymentStatusText(): string {
    if ($this->isPaymentStatusSuccess()) {
      return 'Đã thanh toán';
    }
    return 'Chưa thanh toán';
  }

  public function getPaymentTypeText(): string {
      if ($this->isPaymentOnline()){
          return  'Thanh toán Online';
      }
      if ($this->isPaymentATM()){
          return 'Thanh toán chuyển khoản';
      }
  }

  public function getPaymentText(): string {
    return !empty($this->paymentType) ? $this->paymentType->desc->name : '';
  }
} // End class

