<?php

namespace App\Containers\Order\Traits;

use App\Containers\Service\Enums\ServiceType;

trait OrderItemTypeTrait
{
    public function isOnline(): bool
    {
        return $this->type == ServiceType::ONLINE;
    }

    public function isOffile(): bool
    {
        return $this->type == ServiceType::OFFLINE;
    }

    public function getServiceTypeText(): string
    {
        if ($this->isOnline()) {
            return 'Dịch vụ tư vấn trực tuyến (Online)';
        }
        if ($this->isOffile()){
            return 'Dịch vụ tư vấn trực tiếp (Offline)';
        }
        return 'Chưa xác định';
    }

    public function getClassServiceText(): string {
        if ($this->status_action == 3 || $this->time == $this->time_used ){
            return 'card-services--status-red';
        }
        return 'card-services--status-blue';
    }
} // End class


