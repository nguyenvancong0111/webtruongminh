<?php

namespace App\Containers\Order\Traits;

use App\Containers\Order\Enums\OrderPaymentStatus;

trait OrderPaymentStatusTrait
{
    public function isErrorPay(): bool
    {
        return $this->payment_status == OrderPaymentStatus::ERROR_PAY;
    }

    public function isNoPay(): bool
    {
        return $this->payment_status == OrderPaymentStatus::NO_PAY;
    }

    public function isSuccessPay(): bool
    {
        return $this->payment_status == OrderPaymentStatus::SUCCESS_PAY;
    }


    public function PaymentStatusText(): string
    {
        $text = isset(OrderPaymentStatus::TEXT[$this->payment_status]) ? OrderPaymentStatus::TEXT[$this->payment_status] : 'Lỗi hệ thống';
        return $text ? $text : 'Không xác định';
    }

    public function getPaymentStatusCssClass(): string
    {
        if ($this->isNoPay()) {
            return 'text-dark';
        } elseif ($this->isSuccessPay()) {
            return 'text-success';
        } elseif($this->isErrorPay()) {
            return 'text-danger';
        }else{
            return 'text-danger';
        }
    }
} // End class
