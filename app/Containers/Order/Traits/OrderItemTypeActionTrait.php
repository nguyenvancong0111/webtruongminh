<?php

namespace App\Containers\Order\Traits;

trait OrderItemTypeActionTrait
{
    public function isCancel(): bool {
        return $this->cancel == 1;
    }
    public function isWait(): bool
    {
        return $this->status_action == 0;
    }
    public function isReceive(): bool
    {
        return $this->status_action == 1;
    }
    public function isProcessing(): bool
    {
        return $this->status_action == 2;
    }
    public function isComplete(): bool
    {
        return $this->status_action == 3;
    }



    public function getItemTypeActionText(): string
    {
        if ($this->isCancel()){
            return 'Dịch vụ đã hủy';
        }
        if ($this->isReceive()) {
            return 'Tiếp nhận hồ sơ';
        }
        if ($this->isProcessing()){
            return 'Đang xử lý';
        }
        if ($this->isComplete()){
            return 'Hoàn Thành';
        }
        if($this->isWait()){
            return 'Chờ xử lý';
        }
        return 'Chưa xác định';
    }
} // End class


