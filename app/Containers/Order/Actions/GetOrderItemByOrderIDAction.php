<?php

namespace App\Containers\Order\Actions;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class GetOrderItemByOrderIDAction extends Action
{
  public function run($orderID)
  {
      $order_item = Apiato::call('Order@GetOrderItemByOrderIDTask', [$orderID]);
      return $order_item;
  }
}
