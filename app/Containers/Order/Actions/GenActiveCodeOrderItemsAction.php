<?php

namespace App\Containers\Order\Actions;

use App\Containers\Order\Models\Order;
use App\Ship\Parents\Actions\Action;
use Illuminate\Support\Str;

class GenActiveCodeOrderItemsAction extends Action
{
    public function run(Order $order)
    {
        $data_return = [];
        if ($order->orderItems->isNotEmpty()){
            foreach ($order->orderItems as $itm){
                $data_return[$itm->course_id] = [
                    'order_item_id' => $itm->id,
                    'active_code' => $this->genActiveCode($itm->name, $itm->course_id, $order->customer_id, 10),
                    'available_time' => $itm->time,
                    'is_active' => 0,
                ];
            }
        }
        return $data_return;
    }

    private function genActiveCode($course_name, $course_id, $customer_id, $length  = 10 ){
        $code = '';
        $y = explode(' ',$course_name);
        foreach($y AS $k){
            $code .= strtoupper(substr($k,0,1));
        }
        $permitted = str_replace('-', '', $code.$course_id.$customer_id);
        return sha1(uniqid('', true).Str::random($length).$permitted.microtime(true));
    }

}
