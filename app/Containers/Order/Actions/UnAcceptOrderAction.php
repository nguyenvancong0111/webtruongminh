<?php

namespace App\Containers\Order\Actions;

use App\Containers\Order\Enums\OrderStatus;
use App\Ship\Parents\Actions\Action;

class UnAcceptOrderAction extends Action
{
    protected $status = OrderStatus::NEW_ORDER;
    public function run(array $data)
    {
      return app(OrderHandlerProcessSubAction::class)->status($this->status)->run($data);
    }
}
