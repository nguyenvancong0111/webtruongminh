<?php

namespace App\Containers\Order\Actions;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class GetAllOrderItemsByCustomerIDAction extends Action
{
  public function run(DataTransporter $dataTransporter, array $with=[], array $column=['*'], array $filters = [],  Language $currentLang = null, bool $noPaginate = true, int $limit = 10, int $currentPage = 1)
  {
      $criteria = [
          ['filterOrderItems' => [$dataTransporter]],
          ['with' => [$with]],
          ['selectFields' => [$column]],
      ];
    $order_item = Apiato::call('Order@GetAllOrderItemsByCustomerIDTask', [$filters, $currentLang, $noPaginate, $limit, $currentPage], $criteria);
    return $order_item;
  }
}
