<?php

namespace App\Containers\Order\Actions;

use App\Containers\Onepay\Tasks\ChargeWithOnepayTask;
use App\Containers\Order\Models\Order;
use App\Ship\Parents\Actions\Action;
use Illuminate\Support\Facades\DB;

class PaymentTypeOrderAction extends Action
{
    protected $order;
    public function run()
    {
        return DB::transaction(function () {
            $response = app(ChargeWithOnepayTask::class)->charge($this->order);
            return $response;
        });
    }

    public function setOrders(Order $order): self
    {
        $this->order = $order;
        return $this;
    }
}
