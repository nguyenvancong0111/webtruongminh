<?php

namespace App\Containers\Order\Actions;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class FindOrderItemByIDAction extends Action
{
  public function run(DataTransporter $dataTransporter, array $with=[], array $column=['*'])
  {
      $criteria = [
          ['filterOrderItems' => [$dataTransporter]],
          ['with' => [$with]],
          ['selectFields' => [$column]],
      ];
    $order_item = Apiato::call('Order@FindOrderItemByIDTask', [], $criteria);
    return $order_item;
  }
}
