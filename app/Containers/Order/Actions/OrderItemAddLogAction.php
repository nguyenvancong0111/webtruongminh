<?php

namespace App\Containers\Order\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class OrderItemAddLogAction extends Action
{
    public function run($data = [])
    {
        $dataCreate = Arr::only($data, [ 'lawyer_id', 'order_item_id', 'order_item_type', 'type_action', 'time', 'date', 'content']);
        return Apiato::call('Order@OrderItemAddLogTask', [$dataCreate]);
    }
}
