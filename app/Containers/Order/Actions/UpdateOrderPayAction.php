<?php

namespace App\Containers\Order\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateOrderPayAction extends Action
{
    public function run($order_id, array $data)
    {
        $order = Apiato::call('Order@UpdateOrderTask', [$order_id, $data]);
        return $order;
    }
}
