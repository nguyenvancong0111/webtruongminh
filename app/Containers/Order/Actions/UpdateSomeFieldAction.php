<?php

namespace App\Containers\Order\Actions;

use App\Ship\Parents\Actions\Action;


class UpdateSomeFieldAction extends Action
{
    public function run(int $id, string $field, $val) :? bool
    {
        $result = $this->call('Order@UpdateSomeFieldTask',[ $id, $field, $val]);

        $this->clearCache();

        return $result;
    }
}
