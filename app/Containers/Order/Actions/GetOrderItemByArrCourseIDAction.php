<?php

namespace App\Containers\Order\Actions;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class GetOrderItemByArrCourseIDAction extends Action
{
  public function run(array $arrIDCourse)
  {
    $order_item = Apiato::call('Order@GetOrderItemByArrCourseIDTask', [$arrIDCourse], []);
    return $order_item;
  }
}
