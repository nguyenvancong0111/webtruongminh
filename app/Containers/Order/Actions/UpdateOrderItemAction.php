<?php

namespace App\Containers\Order\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateOrderItemAction extends Action
{
    public function run(int $id, array $data)
    {
        $order_item = Apiato::call('Order@UpdateOrderItemTask', [$id, $data]);
        return $order_item;
    }
}
