<?php

namespace App\Containers\Contact\Models;

use App\Containers\Product\Models\ProductVariant;
use App\Ship\core\Traits\HelpersTraits\DateTrait;
use App\Ship\Parents\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use DateTrait;
    use SoftDeletes;

    const TYPE_C = 'contact';
    const TYPE_H = 'home';

    protected $table = 'contact';

    public const KEY_COOKIE_REQUIRE_INFORMATION = '_KEY_COOKIE_REQUIRE_INFORMATION';

    protected $guarded = [];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'contacts';

    public static function contactTypeText()
    {
        return [
            self::TYPE_C => 'Ở trang liên hệ',
            self::TYPE_H => 'Form Liên Hệ Trang Chủ',
        ];
    }

    public static function typeContact(){
        return [
            1 => 'Tư vấn trực tuyến (online)',
            2 => 'Tư vấn trực tiếp (offline)'
        ];
    }


} // End class
