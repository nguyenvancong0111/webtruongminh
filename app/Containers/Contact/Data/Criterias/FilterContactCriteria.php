<?php

namespace App\Containers\Contact\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Carbon\Carbon;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class FilterContactCriteria extends Criteria
{
    private $queryParams;

    public function __construct(array $queryParams = [])
    {
        $this->queryParams = $queryParams;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        if (!empty($this->queryParams['keyword'])) {
            $keyword = trim($this->queryParams['keyword']);
            $model = $model->where(function ($q) use ($keyword) {
                $q->orWhere('fullname', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('email', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('phone', 'LIKE', '%' . $keyword . '%');
            });
        }

        if (!empty($this->queryParams['type'])) {
            if ($this->queryParams['type'] == 'contact'){
                $model = $model->where('type', '=', $this->queryParams['type']);
            }
            if ($this->queryParams['type'] == 'home_1'){
                $model = $model->where('service_type', '=', 1);
            }
            if ($this->queryParams['type'] == 'home_2'){
                $model = $model->where('service_type', '=', 2);
            }
        }

        if (!empty($this->queryParams['start_date'])) {
            $createdAtFormat = Carbon::createFromFormat('d/m/Y', $this->queryParams['start_date'])
                ->format('Y-m-d');
            $model = $model->whereDate('created_at', '>=', $createdAtFormat);
        }
        if (!empty($this->queryParams['end_date'])) {
            $createdAtFormat = Carbon::createFromFormat('d/m/Y', $this->queryParams['end_date'])
                ->format('Y-m-d');
            $model = $model->whereDate('created_at', '<=', $createdAtFormat);
        }

        return $model;
    }
}
