@extends('basecontainer::admin.layouts.default')

@section('right-breads')
    {!! $contacts->appends($request->all())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5"><h1>{{ $site_title }}</h1></div>

            @include('contact::Admin.filter')

            <div class="card card-accent-primary">
                <table class="table table-hover table-bordered mb-0" id="tableCustomer">
                    <thead>
                    <tr>
                        <th width="60" class="text-center">ID</th>
                        <th>Thông tin liên hệ</th>
                        <th>Thông tin</th>
                        <th>Ngày tạo</th>
                        <th width="60" class="text-center">Xóa</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($contacts as $contact)
                        <tr>
                            <td class="text-center">{{ $contact->id }}</td>
                            <td>
                                @if (!empty($contact->fullname))
                                    <span class="d-block">
                                            <i class="fa fa-user w-15"></i>&nbsp;&nbsp;&nbsp;{{ $contact->fullname }}
                                        </span>
                                @endif

                                @if (!empty($contact->email))
                                    <span class="d-block"><i class="fa fa-envelope-o w-15"></i>
                                        &nbsp;<a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a>
                                    </span>
                                @endif

                                @if (!empty($contact->phone))
                                    <span class="d-block"><i class="fa fa-mobile w-15"></i>
                                    &nbsp;&nbsp;&nbsp;<a href="tel:{{ $contact->phone }}" class="text-body">{{ $contact->phone }}</a>
                                    </span>
                                @endif
                            </td>
                            <td title="{{$contact->content}}">
                                <span class="d-block"><b>Lời nhắn:</b> {{ $contact->content }}</span>
                            </td>
                            <td>{{$contact->created_at}}</td>
                            <td class="text-center">
                                <input type="hidden" class="contact-info" value='@bladeJson($contact)'>
                                <a class="fa fa-trash text-danger cursor-pointer"
                                   data-href="{{ route('admin.contact.delete', $contact->id) }}"
                                   data-toggle="tooltip"
                                   data-original-title="Xóa liên hệ này"
                                   onclick="admin.delete_this(this);">
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @if ($contacts->isNotEmpty())
                    <div class="d-flex py-2">
                       <span class="ml-auto mr-3">
                            Tổng cộng: {{ $contacts->count() }}
                            bản ghi / {{ $contacts->lastPage() }} trang
                       </span>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
