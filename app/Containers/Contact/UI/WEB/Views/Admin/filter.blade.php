<form action="{{ route('admin.contact.index') }}" method="GET" id="searchForm">
    <div class="card card-accent-primary">
        <div class="card-body">
            <div class="row">
                <div class="form-group col-sm-3">
                    <div class="input-group">
                        <div class="input-group-prepend" title="Từ khóa">
                            <span class="input-group-text"><i class="fa fa-bookmark-o"></i></span>
                        </div>
                        <input type="text"
                               class="form-control"
                               name="keyword"
                               value="{{ $request->keyword }}"
                               placeholder="Họ tên / SĐT/ Email"
                        >
                    </div>
                </div>
                <div class="form-group col-6">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                        </div>
                        <input type="text" class="form-control dateptimeicker" name="start_date" autocomplete="off"
                               value="{{ $request->start_date }}" placeholder="{{ __('Từ ngày') }}">
                        <div class="input-group-append">
                            <span class="input-group-text">-</span>
                        </div>
                        <input type="text" class="form-control dateptimeicker" name="end_date" autocomplete="off"
                               value="{{ $request->end_date }}" placeholder="{{ __('Đến ngày') }}">
                    </div>
                </div>

            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                <i class="fa fa-search"></i> Tìm kiếm
            </button>

            <a class="btn btn-sm btn-primary ml-2"
               href="{{ route('admin.contact.index') }}">
                <i class="fa fa-refresh"></i> Hủy tìm
            </a>
        </div>
    </div>
</form>