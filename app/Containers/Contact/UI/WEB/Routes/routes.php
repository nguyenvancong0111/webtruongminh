<?php
/** @var Route $router */
$router->group([
  'prefix' => 'contacts',
  'namespace' => '\App\Containers\Contact\UI\WEB\Controllers\Admin',
  'middleware' => [
    'auth:admin'
  ],
  'domain' => sprintf('admin.%s', parse_url(config('app.url'))['host'])
], function () use ($router) {
  $router->get('/', [
    'as' => 'admin.contact.index',
    'uses'  => 'ContactController@index'
  ]);

  $router->delete('/{id}', [
    'as' => 'admin.contact.delete',
    'uses'  => 'ContactController@delete'
  ]);
});



