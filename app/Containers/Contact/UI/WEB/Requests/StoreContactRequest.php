<?php

namespace App\Containers\Contact\UI\WEB\Requests;

use App\Containers\Contact\Models\Contact;
use App\Containers\Contact\UI\WEB\Rules\PhoneNumberRule;
use App\Ship\core\Traits\HelpersTraits\SecurityTrait;
use App\Ship\Parents\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreContactRequest.
 */
class StoreContactRequest extends Request
{
    use SecurityTrait;

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    // protected $transporter = \App\Ship\Transporters\DataTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles' => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    public function rules()
    {
        if (!empty(request('product_variant_id')) && !empty(request('product_id'))) {
            $rules['phone'] = [
                'required',
                new PhoneNumberRule(),
                Rule::unique('contact')->where(function ($query) {
                    return $query->where([
                        ['deleted_at', '=', null],
                        ['type', '=', Contact::TYPE_V],
                        ['object_id', '=', request('product_variant_id')],
                    ]);
                })
            ];
        } else {
            $rules['name'] = ['required', 'min:3', 'max:100'];
            $rules['email'] = ['required', 'email:rfc,filter', 'max:100', 'unique:contact,email,NULL,id,deleted_at,NULL'];
            $rules['phone'] = ['required', new PhoneNumberRule()];
            $rules['message'] = ['required', 'max:500'];
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => __('name_required'),
            'name.min' => __('name_min', ['min' => 3]),
            'name.max' => __('name_max', ['max' => 191]),
            'email.required' => __('email_required'),
            'email.email' => __('email_email'),
            'email.unique' => __('email_unique'),
            'phone.required' => __('phone_required'),
            'phone.regex' => __('phone_regex'),
            'phone.unique' => __('phone_unique'),
            'message.required' => __('message_required'),
            'message.max' => __('message_max'),
        ];
    }

    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'name' => $this->cleanXSS($this->name),
            'email' => $this->cleanXSS($this->email),
            'phone' => $this->cleanXSS($this->phone),
            'message' => $this->cleanXSS($this->message),
        ]);
    }
}
