<?php

namespace App\Containers\Contact\UI\WEB\Controllers\Desktop;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Containers\Contact\UI\WEB\Requests\StoreContactRequest;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;

class ContactController extends WebController
{
    use ApiResTrait;

    public function store(StoreContactRequest $request)
    {
        $contact = Apiato::call('Contact@CreateContactAction', [$request]);
        return $this->sendResponse($contact, __('site.guithongtinquantamsanpham'));
    }
} // End class ContactController

