<?php

namespace App\Containers\Contact\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Contact\Models\Contact;
use App\Containers\Contact\UI\WEB\Requests\DeleteContactRequest;
use App\Containers\Contact\UI\WEB\Requests\GetAllContactsRequest;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Ship\Parents\Controllers\AdminController;
use Exception;

class ContactController extends AdminController
{
    use ApiResTrait;

    public function __construct()
    {
        $this->title = __('Liên hệ');
        parent::__construct();
    }

    public function index(GetAllContactsRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $contacts = Apiato::call('Contact@GetAllContactsAction', [
            $request,
            []
        ]);

        return view('contact::Admin.index', [
            'contacts' => $contacts,
            'request' => $request,
            'contactTypes' => Contact::contactTypeText(),
        ]);
    }

    public function delete(DeleteContactRequest $request)
    {
        try {
            Apiato::call('Contact@DeleteContactAction', [$request]);
        } catch (Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }
} // End class

