<?php

namespace App\Containers\Contact\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Contact\Models\Contact;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cookie;

class CreateContactAction extends Action
{
    public function run($data = [], $saveCookie = false)
    {

        $dataCreate = Arr::only($data, [ 'fullname', 'email', 'phone', 'title', 'content', 'type']);
        $contact = Apiato::call('Contact@CreateContactTask', [$dataCreate]);
        if ($saveCookie) {
            Cookie::queue(Contact::KEY_COOKIE_REQUIRE_INFORMATION, $contact);
        }

        return $contact;
    }
}
