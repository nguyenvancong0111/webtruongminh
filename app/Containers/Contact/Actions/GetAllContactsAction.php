<?php

namespace App\Containers\Contact\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllContactsAction extends Action
{
    public function run(Request $request, array $with=[])
    {
        return Apiato::call('Contact@GetAllContactsTask', [], [
            [ 'filterContacts' => [$request] ],
            [ 'with' => [$with] ]
        ]);
    }
}
