<?php

namespace App\Containers\SocialAuth\Tasks;

use Apiato\Core\Abstracts\Repositories\Contracts\UserRepositoryInterface;
use App\Ship\Parents\Tasks\Task;

/**
 * Class FindSocialUserTask.
 *
 * @author Mahmoud Zalt <mahmoud@zalt.me>
 */
class FindSocialUserTask extends Task
{

    protected $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $socialProvider
     * @param $socialId
     *
     * @return  mixed
     */
    public function run(string $socialProvider, string $socialId,string $email)
    {
        return $this->repository->findWhere([
            // 'social_provider' => $socialProvider,
            // 'social_id'       => $socialId,
            'email' => $email
        ])->first();
    }

}
