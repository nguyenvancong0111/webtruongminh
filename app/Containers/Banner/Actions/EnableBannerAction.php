<?php

namespace App\Containers\Banner\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Banner\Models\Banner;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class UpdateBannerAction.
 *
 */
class EnableBannerAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $banner = Apiato::call('Banner@EnableBannerTask', [$data->id]);
        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            ['status' => 1],
            ['status' => 2],
            'Hiển thị Banner',
            Banner::class
        ]);

        $this->clearCache();

        return $banner;
    }
}
