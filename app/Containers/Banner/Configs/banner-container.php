<?php

return [
    'status' => [
        'visible' => 1,
        'hidden' => 2,
        'old_delete' => -1
    ],

    'days_of_end_at_default' => 30,

    'positions' => [
        'big_home' => 'Banner Trang Chủ - 1920x754',
    ]
];
