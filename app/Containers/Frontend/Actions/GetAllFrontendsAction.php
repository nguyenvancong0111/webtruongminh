<?php

namespace App\Containers\Frontend\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllFrontendsAction extends Action
{
    public function run(Request $request)
    {
        $frontends = Apiato::call('Frontend@GetAllFrontendsTask', [], ['addRequestCriteria']);

        return $frontends;
    }
}
