<?php

namespace App\Containers\Frontend\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateFrontendAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $frontend = Apiato::call('Frontend@UpdateFrontendTask', [$request->id, $data]);

        return $frontend;
    }
}
