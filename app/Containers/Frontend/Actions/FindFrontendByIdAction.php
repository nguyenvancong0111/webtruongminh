<?php

namespace App\Containers\Frontend\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindFrontendByIdAction extends Action
{
    public function run(Request $request)
    {
        $frontend = Apiato::call('Frontend@FindFrontendByIdTask', [$request->id]);

        return $frontend;
    }
}
