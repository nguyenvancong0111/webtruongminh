<?php

namespace App\Containers\Frontend\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteFrontendAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Frontend@DeleteFrontendTask', [$request->id]);
    }
}
