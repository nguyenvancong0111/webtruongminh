<?php

namespace App\Containers\Frontend\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class FrontendRepository
 */
class FrontendRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
