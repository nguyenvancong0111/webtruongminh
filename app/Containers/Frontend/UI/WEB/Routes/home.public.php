<?php



/** @var Route $router */

Route::group(
    [
        'middleware' => [
            'htmloptimized',
            'Maintenance',
            'WebLocaleRedirect'
        ],
        // 'namespace' => 'Desktop\Home',
    ],
    function ($router) {
        $router->get('/', ['as'   => 'web.home.index', 'uses' => 'Controller@index',]);
        $router->get('/{slug}-nv{id}', ['as'   => 'nhanvien.deatil', 'uses' => 'Controller@detail',]);
    }
);
