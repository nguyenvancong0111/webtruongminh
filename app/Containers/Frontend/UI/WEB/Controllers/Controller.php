<?php

namespace App\Containers\Frontend\UI\WEB\Controllers;

use App\Containers\Frontend\UI\WEB\Requests\CreateFrontendRequest;
use App\Containers\Frontend\UI\WEB\Requests\DeleteFrontendRequest;
use App\Containers\Frontend\UI\WEB\Requests\GetAllFrontendsRequest;
use App\Containers\Frontend\UI\WEB\Requests\FindFrontendByIdRequest;
use App\Containers\Frontend\UI\WEB\Requests\UpdateFrontendRequest;
use App\Containers\Frontend\UI\WEB\Requests\StoreFrontendRequest;
use App\Containers\Frontend\UI\WEB\Requests\EditFrontendRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Http\Request;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;

/**
 * Class Controller
 *
 * @package App\Containers\Frontend\UI\WEB\Controllers
 */
class Controller extends BaseFrontEndController
{
    /**
     * Show all entities
     *
     * @param GetAllFrontendsRequest $request
     */
    public function index(Request $request)
    {
        $nhanvien = [];
        return $this->returnView('frontend', 'pc.home.index', [
            'nhanvien' => $nhanvien,
        ]);
    }

    public function detail(Request $request){
        $news = Apiato::call('News@FrontEnd\GetNewsBySlugIdAction', [$request->id, $request->slug_news, $this->currentLang]);
        if (!empty($news)){

            app(UpdateViewItemAction::class)->skipCache(true)->run($news->id, ['views' => $news->views + 1]);

            $arr_cate = [];
            if(!empty($news->categories)){
                $arr_cate = array_column($news->categories->toArray(), 'category_id');
            }
            $this->generateMetaTag($news, $news->desc->name);
            if (!empty($news->categories) && !empty($news->categories[0]->desc)){
                $this->frontBreadcrumb($news->categories[0]->desc->name, 'javascript:;', true);
            }
            $related_news =  Apiato::call('News@FrontEnd\GetNewsRelatedAction', [$arr_cate, ['arr_id_skip' => [$news->id]], 5, false, $this->currentLang, ['sort_order' => 'ASC'], 1]);
            return $this->returnView('frontend', 'home.detail', [
                'data' => $news,
            ]);
        }
        return abort(404);

    }
}
