@extends('basecontainer::frontend.pc.layouts.home')

@section('content')
{{--    @dd(1)--}}
    <main>
        <div class="d-block text-center my-3 my-lg-5">
            <img src="{{asset('template/images/404.jpg')}}" alt="">
            <p><a href="{{route('web.home.index')}}">Trở về trang chủ</a></p>
        </div>
    </main>
@endsection
