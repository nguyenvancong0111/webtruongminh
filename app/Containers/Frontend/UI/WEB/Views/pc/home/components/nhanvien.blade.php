<section class="tp-product-area pb-55">
    <div class="container">
        <div class="d-flex flex-column align-items-center">
            <h3 class="tp-section-title">Danh sách nhân viên</h3>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="tp-product-tab-content">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="new-tab-pane" role="tabpanel"
                            aria-labelledby="new-tab" tabindex="0">
                            <div class="row block-gaigoi">
                                @foreach ($nhanvien as $item)
                                    <div class="col-6 col-lg-4 col-xl-2 item-gaigoi">
                                        <div class="tp-product-item p-relative transition-3">
                                            <div class="tp-product-thumb p-relative fix m-img">
                                                <a
                                                    href="{{ route('nhanvien.deatil', ['slug' => $item->slug, 'id' => $item->id]) }}">
                                                    <img data-src="src="{{\ImageURL::getImageUrl($item->image, 'news', '400x600')}}" alt />
                                                </a>
                                                <div class="tp-product-badge">
                                                    <span class="product-hot"><i class="fa-solid fa-hot-tub-person"></i>
                                                        {{ $item->code }}</span>
                                                </div>
                                            </div>
                                            <div class="tp-product-content">
                                                <div class="tp-product-category"></div>
                                                <h3 class="tp-product-title">
                                                    <a
                                                        href="{{ route('nhanvien.deatil', ['slug' => $item->slug, 'id' => $item->id]) }}">
                                                        💕 {{ $item->name }} 💕
                                                    </a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
