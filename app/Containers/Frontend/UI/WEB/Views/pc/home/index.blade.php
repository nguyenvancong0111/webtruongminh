@extends('basecontainer::frontend.pc.layouts.home')

@section('content')
    <main>
        @include('frontend::pc.home.components.banner', ['data' => '$banner'])
        @include('frontend::pc.home.components.nhanvien', ['data' =>$nhanvien])
        @include('frontend::pc.home.components.infor')
    </main>
@endsection
