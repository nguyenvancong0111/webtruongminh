<?php

namespace App\Containers\Frontend\Tasks;

use App\Containers\Frontend\Data\Repositories\FrontendRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllFrontendsTask extends Task
{

    protected $repository;

    public function __construct(FrontendRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
