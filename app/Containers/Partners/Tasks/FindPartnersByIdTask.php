<?php

namespace App\Containers\Partners\Tasks;

use App\Containers\Partners\Data\Repositories\PartnersRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class FindBannerByIdTask.
 */
class FindPartnersByIdTask extends Task
{

    protected $repository;

    public function __construct(PartnersRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($partners_id, $defaultLanguage = 1, $external_data = ['with_relationship' => ['all_desc']])
    {
        
        $data = $this->repository->with(array_merge($external_data['with_relationship']))->find($partners_id);
        
        return $data;
    }
}