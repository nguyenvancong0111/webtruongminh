<?php

namespace App\Containers\Partners\Tasks;

use App\Containers\Partners\Data\Repositories\PartnersRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class CreateBannerTask.
 */
class CreatePartnersTask extends Task
{

    protected $repository;

    public function __construct(PartnersRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $data = Arr::except($data->toArray(), ['banner_description', '_token', '_headers']);
//            $data['position'] = is_array($data['position']) ? implode(',', $data['position']) : $data['position'];
//            $data['is_mobile'] = isset($data['is_mobile']) ? 1 : 0;
            if(!empty($data['position'])){
                $data['position'] = is_array($data['position']) ? implode(',', $data['position']) : $data['position'];
            }
            $partner = $this->repository->create($data);
            return $partner;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
