<?php

namespace App\Containers\Partners\Tasks;

use App\Containers\Partners\Data\Repositories\PartnersDescRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class GetAllPartnersDescTask.
 */
class GetAllPartnersDescTask extends Task
{

    protected $repository;

    public function __construct(PartnersDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($partner_id)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('partners_id', $partner_id));
        $wery = $this->repository->all()->keyBy('language_id')->toArray();

        return $wery;
    }
}
