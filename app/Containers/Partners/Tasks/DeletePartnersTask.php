<?php

namespace App\Containers\Partners\Tasks;

use App\Containers\Partners\Data\Repositories\PartnersRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteBannerTask.
 */
class DeletePartnersTask extends Task
{

    protected $repository;

    public function __construct(PartnersRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($id)
    {
        try {
            $this->repository->getModel()->where('id', $id)->update(
                [
                    'status' => -1
                ]
            );
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
