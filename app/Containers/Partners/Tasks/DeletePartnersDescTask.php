<?php

namespace App\Containers\Partners\Tasks;

use App\Containers\Partners\Data\Repositories\PartnersDescRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeletePartnersDescTask.
 */
class DeletePartnersDescTask extends Task
{

    protected $repository;

    public function __construct(PartnersDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($id)
    {
        try {
            $this->repository->getModel()->where('partners_id', $id)->delete();
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
