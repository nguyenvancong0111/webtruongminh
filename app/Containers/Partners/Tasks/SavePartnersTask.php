<?php

namespace App\Containers\Partners\Tasks;

use App\Containers\Partners\Data\Repositories\PartnersRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Arr;

/**
 * Class SaveBannerTask.
 */
class SavePartnersTask extends Task
{

    protected $repository;

    public function __construct(PartnersRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data->toArray(), ['banner_description', '_token', '_headers']);
            if(!empty($dataUpdate['position'])) {
                $dataUpdate['position'] = is_array($dataUpdate['position']) ? implode(',', $dataUpdate['position']) : $dataUpdate['position'];
            }
          //  $dataUpdate['is_mobile'] = isset($dataUpdate['is_mobile']) ? 1 : 0;

            $banner = $this->repository->update($dataUpdate, $data->id);
            return $banner;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}
