<?php

namespace App\Containers\Partners\Tasks;

use App\Containers\Partners\Data\Criterias\MobileCriteria;
use App\Containers\Partners\Data\Repositories\PartnersRepository;
use App\Ship\Parents\Tasks\Task;

class GetAvailablePartnersTask extends Task
{
    protected $repository;

    public function __construct(PartnersRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($orderBy = ['created_at' => 'desc', 'id' => 'desc'], $position = [], $where = [], $limit = 0)
    {
        foreach ($orderBy as $column => $direction) {
            $this->repository->orderBy($column, $direction);
        }

        if (!empty($position)) {
            foreach ($position as $column => $direction) {
                $this->repository->where($column, 'like', $direction);
            }
        }

        if (!empty($where)) {
            foreach ($where as $column => $direction) {
                $this->repository->where($column, $direction);
            }
        }

        if (empty($limit)) {
            return $this->repository->get();
        }

        return $this->repository->with('desc')->paginate($limit);
    }

    public function mobile(bool $isMobile = false)
    {
        $this->repository->pushCriteria(new MobileCriteria($isMobile));
    }
}
