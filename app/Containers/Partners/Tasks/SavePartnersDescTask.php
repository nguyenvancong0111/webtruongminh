<?php

namespace App\Containers\Partners\Tasks;

use App\Containers\Partners\Data\Repositories\PartnersDescRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SavePartnersDescTask.
 */
class SavePartnersDescTask extends Task
{

    protected $repository;

    public function __construct(PartnersDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data, $original_desc, $pertners_id, $edit_id = null)
    {
        $banner_description = isset($data['banner_description']) ? (array)$data['banner_description'] : null;
        if (is_array($banner_description) && !empty($banner_description)) {
            $updates = [];
            $inserts = [];
            foreach ($banner_description as $k => $v) {
                if (isset($original_desc[$k])) {
                    $updates[$original_desc[$k]['id']] = [
                        'name' => $v['name'],
                        'link' => $v['link'],
                        'short_description' => $v['short_description'],
                    ];
                    if(isset($data['image']) && !empty($data['image'])){
                        $updates[$original_desc[$k]['id']]['image'] = $data['image'];
                    }
                } else {
                    $inserts[] = [
                        'partners_id' => $pertners_id,
                        'language_id' => $k,
                        'name' => $v['name'],
                        'link' => $v['link'],
                        'short_description' => $v['short_description'],
                        'image' => $data['image'] ?? '',
                    ];
                }
            }
            if (!empty($updates)) {
                $this->repository->updateMultiple($updates);
            }

            if (!empty($inserts)) {
                $this->repository->getModel()->insert($inserts);
            }
        }
    }
}
