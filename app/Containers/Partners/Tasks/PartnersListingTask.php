<?php

namespace App\Containers\Partners\Tasks;

use App\Containers\Partners\Data\Criterias\AdminFilterCriteria;
use App\Containers\Partners\Data\Criterias\HasNameCriteria;
use App\Containers\Partners\Data\Criterias\OrderByCreatedCriteria;
use App\Containers\Partners\Data\Criterias\WithAllDescriptionCriteria;
use App\Containers\Partners\Data\Criterias\WithDescriptionCriteria;
use App\Containers\Partners\Data\Repositories\PartnersRepository;
use App\Ship\Parents\Tasks\Task;


/**
 * Class BannerListingTask.
 */
class PartnersListingTask extends Task
{

    protected $repository;

    public function __construct(PartnersRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($limit = 20){
        return $this->repository->paginate($limit);
    }

    public function adminFilter($request) {
        $this->repository->pushCriteria(new AdminFilterCriteria($request));
    }

    public function hasName($name) {
        $this->repository->pushCriteria(new HasNameCriteria($name));
    }

    public function withDescription($language_id) {
        $this->repository->pushCriteria(new WithDescriptionCriteria($language_id));
    }

    public function withAllDescription() {
        $this->repository->pushCriteria(new WithAllDescriptionCriteria());
    }

    public function ordereByCreated()
    {
        $this->repository->pushCriteria(new OrderByCreatedCriteria());
    }
}
