<?php

namespace App\Containers\Partners\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;


class FindPartnersByIdAction extends Action
{

    public function run($partners_id)
    {
        $data = Apiato::call('Partners@FindPartnersByIdTask', [
            $partners_id,
            Apiato::call('Localization@GetDefaultLanguageTask'),
            ['with_relationship' => ['all_desc']]
        ]);

        return $data;
    }
}
