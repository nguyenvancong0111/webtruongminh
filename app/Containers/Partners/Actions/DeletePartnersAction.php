<?php

namespace App\Containers\Partners\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Banner\Models\Partners;
use App\Ship\Parents\Actions\Action;

/**
 * Class DeleteBannerAction.
 *
 */
class DeletePartnersAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        Apiato::call('Partners@DeletePartnersTask', [$data->id]);
        Apiato::call('Partners@DeletePartnersDescTask', [$data->id]);

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            [],
            ['status' => -1],
            'Xóa Partners',
            Partners::class
        ]);
    }
}
