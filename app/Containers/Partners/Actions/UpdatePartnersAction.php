<?php

namespace App\Containers\Partners\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Partners\Models\Partners;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class UpdateBannerAction.
 *
 */
class UpdatePartnersAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $beforeData = Apiato::call('Partners@FindPartnersByIdTask', [$data->id]);
        $banner = Apiato::call('Partners@SavePartnersTask',[$data]);

        if($banner) {
            $original_desc = Apiato::call('Partners@GetAllPartnersDescTask', [$banner->id]);

            Apiato::call('Partners@SavePartnersDescTask', [
                $data,
                $original_desc,
                $banner->id
            ]);

            Apiato::call('User@CreateUserLogSubAction', [
                $banner->id,
                $beforeData->toArray(),
                $banner->toArray(),
                'Cập nhật Partners',
                Partners::class
            ]);
        }

        return $banner;
    }
}
