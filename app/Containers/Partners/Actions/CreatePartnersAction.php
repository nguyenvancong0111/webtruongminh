<?php

namespace App\Containers\Partners\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Banner\Models\Partners;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class CreateBannerAction.
 *
 */
class CreatePartnersAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $partner = Apiato::call('Partners@CreatePartnersTask',[$data]);

        if ($partner) {
            Apiato::call('Partners@SavePartnersDescTask', [$data, [], $partner->id]);

            $partner = Apiato::call('Partners@FindPartnersByIdTask',[$partner->id]);
            Apiato::call('User@CreateUserLogSubAction', [
                $partner->id,
                [],
                $partner->toArray(),
                'Tạo Partner',
                Partners::class
            ]);
        }

        return $partner;
    }
}
