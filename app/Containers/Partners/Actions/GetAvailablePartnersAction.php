<?php

namespace App\Containers\Partners\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

class GetAvailablePartnersAction extends Action
{
    public function run($orderBy = ['created_at' => 'desc', 'id' => 'desc'], $positions = [], $where = [], $limit = 0, $conditions = [])
    {
        return Apiato::call('Partners@GetAvailablePartnersTask', [$orderBy, $positions, $where, $limit], [
            ['with' => [$conditions]],
        ]);
    }
}
