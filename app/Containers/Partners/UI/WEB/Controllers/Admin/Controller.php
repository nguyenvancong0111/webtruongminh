<?php

namespace App\Containers\Partners\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Partners\Models\Partners;
use App\Containers\Partners\UI\WEB\Requests\Admin\FindPartnersRequest;
use App\Containers\Partners\UI\WEB\Requests\CreatePartnersRequest;
use App\Containers\Partners\UI\WEB\Requests\GetAllPartnersRequest;
use App\Containers\Partners\UI\WEB\Requests\UpdatePartnersRequest;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use Exception;

class Controller extends AdminController
{
    public function __construct()
    {
        $this->title = 'Partners';

        parent::__construct();

    }
    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllPartnersRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $partners = Apiato::call('Partners@PartnersListingAction', [new DataTransporter($request), $this->perPage]);


        $options = array_merge(['' => 'Chọn vị trí'], config('partners-container.positions'));

        return view('partners::admin.index', [
            'search_data' => $request,
            'data' => $partners,
            'options' => $options,
        ]);
    }

    public function edit(FindPartnersRequest $request)
    {
        $this->showEditForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['edit', $this->title, 'admin_partners_home_page']);

        try{
            $partners = Apiato::call('Partners@Admin\FindPartnersByIdAction', [$request->id]);

        }catch(Exception $e){
            return redirect()->route('admin_partners_home_page', ['id' => $request->id])->with('status', 'Có lỗi');
        }

        if(!empty($partners)) {
            return view('partners::admin.edit', [
                'data' => $partners,
                'positions' => config('partners-container.positions')
            ]);
        }

        return $this->notfound($request->id);
    }

    public function add()
    {
        $this->showAddForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['add', $this->title, 'admin_partners_home_page']);
        return view('partners::admin.edit', [
            'positions' => config('partners-container.positions')
        ]);
    }

    public function update(UpdatePartnersRequest $request)
    {
        try {
            $tranporter = new DataTransporter($request);
            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'partners', StringLib::getClassNameFromString(Partners::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }

            $news = Apiato::call('Partners@UpdatePartnersAction', [$tranporter]);

            if ($news) {
                return redirect()->route('admin_partners_edit_page', ['id' => $news->id])->with('status', 'Cập nhật partners thành công');
            }
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function create(CreatePartnersRequest $request)
    {
        try {
            $tranporter = new DataTransporter($request);

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'partners', StringLib::getClassNameFromString(Partners::class)]);

                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }
            $partner = Apiato::call('Partners@CreatePartnersAction', [$tranporter]);
            if ($partner) {
                return redirect()->route('admin_partners_home_page')->with('status', 'Partner đã được thêm mới');
            }
        } catch (\Exception $e) {
            // throw $e;
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function delete(FindPartnersRequest $request)
    {
        try {
            Apiato::call('Partners@DeletePartnersAction', [$request]);
        } catch (\Exception $e) {
            // throw $e;
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

}
