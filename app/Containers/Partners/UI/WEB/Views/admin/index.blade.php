@extends('basecontainer::admin.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5"><h1>{{ $site_title }}</h1></div>

            @if( count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            {!! Form::open(['url' => route('admin_partners_home_page'), 'method' => 'get', 'id' => 'searchForm']) !!}
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                                class="fa fa-bookmark-o"></i></span></div>
                                <input type="text" name="name" class="form-control" placeholder="Tiêu đề"
                                       value="{{ $search_data->name }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                    <a href="{{ route('admin_partners_home_page') }}" class="btn btn-sm btn-primary"><i
                                class="fa fa-plus"></i> Reset</a>
                    <a href="{{ route('admin_partners_add_page') }}" class="btn btn-sm btn-primary"><i
                                class="fa fa-plus"></i> Thêm mới</a>
                </div>
            </div>
            {!! Form::close() !!}

            <div class="card card-accent-info">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Danh sách
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        @if(empty($data) || $data->isEmpty())
                            <h4 align="center">Không tìm thấy dữ liệu phù hợp</h4>
                        @else
                            <table class="table table-bordered table-striped ">
                                <thead>
                                <tr>
                                    <th width="55">ID</th>
                                    <th>Tiêu đề</th>
                                    <th width="125">Ảnh</th>
                                    <th width="100">Trạng thái</th>
                                    <th width="100">Ngày tạo</th>
                                    <th width="55">Lệnh</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($data as $item)
                                    <tr>
                                        <td align="center">{{ $item->id }}</td>
                                        <td>
                                            <div class="mb-2">{{ @$item->desc->name }}</div>
                                            <div>
                                                <b>Vị trí:</b>
                                                @php($positions = $item->positions())
                                                @foreach($positions as $n)
                                                    {{ $n . ($loop->last?'':', ') }}
                                                @endforeach
                                            </div>
                                        </td>
                                        <td align="center">
                                            @if(isset($item->desc) && $item->desc->image != '')
                                                <img src="{{ $item->desc->getImageUrl('small') }}" width="100"/>
                                            @else
                                                ---
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->status == 1)
                                                <a href="javascript:" class="cat-item-news-rs">Hiển thị</a>
                                            @elseif($item->status == 2)
                                                <a href="javascript:" class="cat-item-news-rs">Ẩn</a>
                                            @else
                                                <a href="javascript:" class="cat-item-news-rs">Không xác định</a>
                                            @endif
                                        </td>
                                        <td align="center">{!! $item->created_at ? Carbon\Carbon::parse($item->created_at)->format("d/m/Y H:i:s") : '---' !!} </td>
                                        <td align="center">
                                            <a href="{{ route('admin_partners_edit_page', $item->id) }}"
                                               class="btn text-primary"><i class="fa fa-pencil"></i></a>
                                            <a data-href="{{ route('admin_partners_delete', $item->id) }}"
                                               class="btn text-danger" onclick="admin.delete_this(this);"><i
                                                        class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>


                            @include('basecontainer::admin.inc.bottom-pager-infor')
{{--                            {!! $data->links() !!}--}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop