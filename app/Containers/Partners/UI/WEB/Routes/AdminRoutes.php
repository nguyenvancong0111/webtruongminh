<?php
Route::group(
    [
        'prefix' => 'partners',
        'namespace' => '\App\Containers\Partners\UI\WEB\Controllers\Admin',
        'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
        'middleware' => [
            'auth:admin',
        ],
    ],
    function () use ($router) {
        $router->get('/', [
            'as'   => 'admin_partners_home_page',
            'uses' => 'Controller@index',
        ]);

        $router->get('/edit/{id}', [
            'as'   => 'admin_partners_edit_page',
            'uses' => 'Controller@edit',
        ]);

        $router->post('/edit/{id}', [
            'as'   => 'admin_pictures_edit_page',
            'uses' => 'Controller@update',
        ]);

        $router->get('/add', [
            'as'   => 'admin_partners_add_page',
            'uses' => 'Controller@add',
        ]);

        $router->post('/add', [
            'as'   => 'admin_partners_add_page',
            'uses' => 'Controller@create',
        ]);

        $router->delete('/delete/{id}', [
            'as' => 'admin_partners_delete',
            'uses'       => 'Controller@delete',
        ]);

    }
);
