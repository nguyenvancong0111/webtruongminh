<?php

namespace App\Containers\Partners\UI\WEB\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateBannerRequest.
 *
 */
class CreatePartnersRequest extends Request
{

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles'       => 'admin',
        'permissions' => 'partners-manager',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'image' => 'required|mimes:jpeg,jpg,png,gif',
//            'position' => 'required',
            'banner_description.*.name' => 'bail|required|string|max:255',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'image.required' => 'Vui lòng Upload ảnh',
            'image.mimes' => 'Ảnh không đúng định dạng (jpeg, jpg, png, gif)',
            'banner_description.1.name.required' => 'Tên đối tác tiếng việt',
            'banner_description.2.name.required' => 'Tên đối tác tiếng anh',
            'banner_description.3.name.required' => 'Tên đối tác tiếng trung',
            'banner_description.1.name.string' => 'Tên đối tác tiếng việt phải là một chuỗi',
            'banner_description.2.name.string' => 'Tên đối tác tiếng anh phải là một chuỗi',
            'banner_description.3.name.string' => 'Tên đối tác tiếng trung phải là một chuỗi',
            'banner_description.1.name.max' => 'Tên đối tác tiếng việt quá dài (tối đa 255 ký tự)',
            'banner_description.2.name.max' => 'Tên đối tác tiếng anh quá dài (tối đa 255 ký tự)',
            'banner_description.3.name.max' => 'Tên đối tác tiếng trung quá dài (tối đa 255 ký tự)',
        ];
    }

    public function attributes()
    {
        return [
            'banner_description.1.name.required' => 'Tên đối tác tiếng việt',
            'banner_description.2.name' => 'Tên đối tác tiếng anh',
            'banner_description.3.name' => 'Tên đối tác tiếng trung',

        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
