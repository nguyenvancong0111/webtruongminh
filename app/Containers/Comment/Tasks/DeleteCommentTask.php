<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteCommentTask.
 */
class DeleteCommentTask extends Task
{

    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($comment_id)
    {
        try {
          $data =  $this->repository->getModel()->where('id', $comment_id)->update(
                [
                    'status' => -1
                ]
            );

        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
