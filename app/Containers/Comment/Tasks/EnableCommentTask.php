<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteBannerTask.
 */
class EnableCommentTask extends Task
{

    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($id)
    {

        try {
            $this->repository->getModel()->where('id', $id)->update(
                [
                    'status' => 2
                ]
            );

        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
