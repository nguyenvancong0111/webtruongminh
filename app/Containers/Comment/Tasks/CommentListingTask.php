<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-07 20:44:45
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Criterias\AdminFilterCriteria;
use App\Containers\Comment\Data\Criterias\HasNameCriteria;
use App\Containers\Comment\Data\Criterias\OrderByCreatedCriteria;
use App\Containers\Comment\Data\Criterias\OrderBySortCriteria;
use App\Containers\Comment\Data\Criterias\WithAllDescriptionCriteria;
use App\Containers\Comment\Data\Criterias\WithDescriptionCriteria;
use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Ship\Criterias\Eloquent\OrderByFieldCriteria;
use App\Ship\Criterias\Eloquent\OrderByFieldsCriteria;
use App\Ship\Parents\Tasks\Task;

class CommentListingTask extends Task
{
    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($limit = 20)
    {
        return $this->repository->with(['course' => function($query){
            $query->with('desc');
        }, 'customer'])->paginate($limit);
    }

    public function adminFilter($request): self
    {
        $this->repository->pushCriteria(new \App\Containers\Comment\Data\Criterias\AdminFilterCriteria($request));
        return $this;
    }

    public function hasName($name): self
    {
        $this->repository->pushCriteria(new \App\Containers\Comment\Data\Criterias\HasNameCriteria($name));
        return $this;
    }

    public function withDescription($language_id): self
    {
        $this->repository->pushCriteria(new \App\Containers\Comment\Data\Criterias\WithDescriptionCriteria($language_id));
        return $this;
    }

    public function withAllDescription(): self
    {
        $this->repository->pushCriteria(new \App\Containers\Comment\Data\Criterias\WithAllDescriptionCriteria());
        return $this;
    }

    public function ordereByCreated(): self
    {
        $this->repository->pushCriteria(new OrderByFieldsCriteria([
            ['created_at','desc'],
        ]));
        return $this;
    }

}
