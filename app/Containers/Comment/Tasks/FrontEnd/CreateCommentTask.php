<?php

namespace App\Containers\Comment\Tasks\FrontEnd;

use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class CreateBannerTask.
 */
class CreateCommentTask extends Task
{

    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $data = $this->repository->create($data);

            return $data;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
