<?php

namespace App\Containers\Comment\Tasks\FrontEnd;

use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

class GetAllRateByCourseIdTask extends Task
{

    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($courseId, $with = [], $skipPaginate= false, $limit = 10, $currentPage = 1): iterable {

        $this->repository->pushCriteria(new ThisEqualThatCriteria('status', 2));
        $this->repository->pushCriteria(new ThisEqualThatCriteria('object_id', $courseId));
        $this->repository->with($with);
        $this->repository->orderBy('id', 'desc');
        return $skipPaginate ? $this->repository->paginate($limit) : $this->repository->get();
    }
}
