@extends('basecontainer::admin.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @if( count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            {!! Form::open(['url' => route('admin_comment_home_page'), 'method' => 'get', 'id' => 'searchForm']) !!}
            <div class="card card-accent-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-bookmark-o"></i></span></div>
                                <input type="text" name="name" class="form-control" placeholder="Tên, Email, Nội dung"
                                       value="{{ $search_data->name }}">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-calendar"></i></span></div>
                                <input type="text" name="time_from" class="datepicker form-control"
                                       placeholder="Thời gian đánh giá từ" autocomplete="off"
                                       value="{{ $search_data->time_from }}">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-calendar"></i></span></div>
                                <input type="text" name="time_to" class="datepicker form-control"
                                       placeholder="Thời gian đánh giá đến" autocomplete="off"
                                       value="{{ $search_data->time_to }}">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-list"></i></span></div>
                                <select id="status" name="status" class="form-control">
                                    <option value="">-- Chọn trạng thái --</option>
                                    <option value="2"{{ $search_data->status == 2 ? ' selected="selected"' : '' }}>Đang
                                        hiển thị
                                    </option>
                                    <option value="1"{{ $search_data->status == 1 ? ' selected="selected"' : '' }}>Đang
                                        ẩn
                                    </option>
                                    <option value="-1"{{ $search_data->status == -1 ? ' selected="selected"' : '' }}>Đã
                                        xóa
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                    <a href="{{ route('admin_comment_home_page') }}" class="btn btn-sm btn-primary"><i
                            class="fa fa-refresh"></i> Reset</a>
                </div>
            </div>
            {!! Form::close() !!}

            <div class="card card-accent-primary">
                <div class="card-body p-0">
                    <table class="table table-bordered table-striped ">
                        <thead>
                        <tr>
                            <th width="55">ID</th>
                            <th width="20%">Học viên</th>
                            <th width="">Nội dung</th>
                            <th width="30%">Khóa học</th>
                            <th width="100">Thời gian đánh giá</th>
                            <th width="8%">Lệnh</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <td align="center">{{ $item->id }}</td>
                                <td>
                                    <p><strong>Họ và Tên: </strong> {{$item->customer->fullname}}</p>
                                    <p><strong>Email: </strong> {{$item->customer->email}}</p>
                                    <p><strong>Đánh giá: </strong>
                                        @for($i=0;$i<$item->rating;$i++)
                                            <label class="fa fa-star"></label>
                                        @endfor
                                    </p>
                                </td>
                                <td>{!! nl2br(@$item->message) !!}</td>
                                <td>
                                    @if(!empty($item->course))
                                        <a href="{{$item->course->link()}}" target="_blank" class="{{$item->status == 2 ? "text-success" : "text-danger"}}">{{$item->course->desc->name}}</a>
                                    @endif
                                </td>
                                <td align="center">{!! $item->created_at ? Carbon\Carbon::parse($item->created_at)->format("d/m/Y H:i:s") : '---' !!} </td>
                                <td align>
                                 @if($item->status > 0)
                                    <div>
                                        @if($item->status != 2)
                                        <a href="javascript:void(0)"
                                           data-route="{{route('admin_comments_enable', $item->id)}}"
                                           class="text-success"
                                           onclick="admin.updateStatus(this,{{$item->id}}, 2)"
                                           title="Đang ẩn, Click để hiển thị"><i class="fa fa-check">Duyệt</i></a>
                                        @endif
                                    </div>

                                    <div>
                                        @if($item->status != -1)
                                            <a data-href="{{ route('admin_comments_delete', $item->id) }}"
                                               href="javascript:void(0)"
                                               class="text-danger"
                                               title="Xóa"
                                               onclick="admin.delete_this(this);"><i
                                                    class="fa fa-trash"></i>Xóa</a>
                                        @endif
                                    </div>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    @include('basecontainer::admin.inc.bottom-pager-infor')
                </div>
            </div>
        </div>
    </div>
@stop
@push('js_bot_all')
    <script>
        $('.datepicker').datetimepicker({
            format: 'd/m/Y',
            timepicker: false
        });
    </script>
@endpush
