<?php

namespace App\Containers\Comment\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Comment\Models\Comment;
use App\Containers\Comment\UI\WEB\Requests\Admin\FindCommentRequest;
use App\Containers\Comment\UI\WEB\Requests\GetAllCommentrRequest;
use App\Ship\Parents\Controllers\AdminController;
use Exception;

class Controller extends AdminController
{
    public function __construct()
    {
        $this->title = 'Rating';

        parent::__construct();

    }
    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllCommentrRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $comments = Apiato::call('Comment@CommentListingAction', [$request, $this->perPage]);
//        dd($comments);
        return view('comment::admin.index', [
            'search_data' => $request,
            'data' => $comments,
        ]);
    }

    public function edit(FindCommentRequest $request)
    {
        $this->showEditForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['edit', $this->title, 'admin_comment_home_page']);

        try{
            $comment = Apiato::call('Comment@Admin\FindCommentByIdAction', [$request->id]);

        }catch(Exception $e){
            return redirect()->route('admin_comment_home_page', ['id' => $request->id])->with('status', 'Có lỗi');
        }
// dd(config('comment-container.positions'));
        return view('comment::admin.edit', [
            'data' => $comment,
            'positions' => config('comment-container.positions')
        ]);

        return $this->notfound($request->id);
    }

    public function add()
    {
        $this->showAddForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['add', $this->title, 'admin_comment_home_page']);

        return view('comment::admin.edit', [
            'positions' => config('comment-container.positions')
        ]);
    }

    public function update(UpdateCommentRequest $request)
    {
        try {
            $data =$request->except('image');

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'comment', StringLib::getClassNameFromString(Comment::class)]);
                if (!$image['error']) {
                    $data['image'] = $image['fileName'];
                }
            }

            $comment = Apiato::call('Comment@UpdateCommentAction', [$data]);

            if ($comment) {
                return redirect()->route('admin_comments_edit_page', ['id' => $comment->id])->with('status', 'Cập nhật comment thành công');
            }
        } catch (\Exception $e) {
            $this->throwExceptionViaMess($e);
        }
    }

    public function create(CreateCommentRequest $request)
    {
        try {
            $data =$request->except('image');

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'comment', StringLib::getClassNameFromString(Comment::class)]);
                if (!$image['error']) {
                    $data['image'] = $image['fileName'];
                }
            }

            $comment = Apiato::call('Comment@CreateCommentAction', [$data]);
            if ($comment) {
                return redirect()->route('admin_comment_home_page')->with('status', 'Comment đã được thêm mới');
            }
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function delete(FindCommentRequest $request)
    {
        try {
            Apiato::call('Comment@DeleteCommentAction', [$request]);
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function enable(FindCommentRequest $request){
        try {
            Apiato::call('Comment@EnableCommentAction', [$request]);
            return FunctionLib::ajaxRespondV2(true,'Success');
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function disable(FindCommentRequest $request){
        try {
            Apiato::call('Comment@DisableCommentAction', [$request]);
            return FunctionLib::ajaxRespondV2(true,'Success');
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }
}
