<?php

namespace App\Containers\Comment\UI\API\Controllers;

use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use App\Containers\Comment\Actions\FrontEnd\GetAllRateByCourseIdAction;
use App\Containers\Comment\UI\API\Requests\GetRateByCourseRequest;
use App\Containers\Comment\UI\API\Transformers\CalculatorRateCourseTransformer;
use App\Containers\Comment\UI\API\Transformers\RateCourseTransformer;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Response;

/**
 * Class Controller.
 */
class Controller extends BaseApiFrontController
{
    public function getListRate(GetRateByCourseRequest $request){
        $rate = app(GetAllRateByCourseIdAction::class)->skipCache(true)->run((int)$request->object, ['customer'], true, 5, isset($request->page) && !empty($request->page) ? (int)$request->page : 1);
        return $this->transform($rate, new RateCourseTransformer());
    }
    public function getRateByCourse(GetRateByCourseRequest $request){
        $rate = app(GetAllRateByCourseIdAction::class)->skipCache(true)->run((int)$request->object, [], false, 0, 1);
        $data = [
            'avg' => $this->calculatorRate($rate->toArray())
        ];
        return $data;
    }

    public function calculatorRate($data){
        $dataReturn = [
            'total_rate' => 0,
            'avg_rate' => 0,
            'avg_star' => 0,
            'item' => [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0]
        ];
        if (!empty($data)){
            $countRateItem = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0];
            $dataReturn['total_rate'] = count($data);
            $sum_rate = array_sum(array_column($data, 'rating'));
            $dataReturn['avg_rate'] = round($sum_rate/$dataReturn['total_rate'], 1);
            $dataReturn['avg_star'] = round($sum_rate/$dataReturn['total_rate'], 0 );

            foreach ($data as $item){
                switch ($item['rating']){
                    case 1:
                        $countRateItem[1] += 1;
                        break;
                    case 2:
                        $countRateItem[2] += 1;
                        break;
                    case 3:
                        $countRateItem[3] += 1;
                        break;
                    case 4:
                        $countRateItem[4] += 1;
                        break;
                    case 5:
                        $countRateItem[5] += 1;
                        break;
                }
            }
            for ($i = 1; $i <= 5; $i++){
                $dataReturn['item'][$i] = round($countRateItem[$i] / $dataReturn['total_rate'] * 100, 1);
            }
        }
        return $dataReturn;
    }
}

