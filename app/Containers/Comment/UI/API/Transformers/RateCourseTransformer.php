<?php

namespace App\Containers\Comment\UI\API\Transformers;

use App\Containers\Comment\Models\Comment;
use App\Ship\Parents\Transformers\Transformer;

class RateCourseTransformer extends Transformer
{
    /**
     * @param $token
     *
     * @return  array
     */
    public function transform(Comment $data)
    {
        $response = [
            'name' => $data->customer->fullname,
            'avatar' => \ImageURL::getImageUrl($data->customer->avatar, 'customer', 'small'),
            'rate' => $data->rating,
            'content' => $data->message,
            'created_at' => \FunctionLib::dateFormat($data->created_at, 'd/m/Y')
        ];
        return $response;
    }
}
