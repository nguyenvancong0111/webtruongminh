<?php

Route::group(
    [
        'middleware' => [
            'api',
        ],
        'prefix' => '/course/rating',
    ],
    function () use ($router) {
        $router->any('/', [
            'as' => 'api_rate_course',
            'uses' => 'Controller@getListRate'
        ]);
        $router->any('/calculator', [
            'as' => 'api_rate_cal_course',
            'uses' => 'Controller@getRateByCourse'
        ]);
    });