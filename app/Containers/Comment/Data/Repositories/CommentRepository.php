<?php

namespace App\Containers\Comment\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class CommentRepository.
 */
class CommentRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Comment';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'status',
        'name',
        'email',
        'product_id',
        'created_at',
        'update_at',
        'message',
        'rating',
    ];
}
