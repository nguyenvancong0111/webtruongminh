<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2020-10-04 13:58:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-10 16:23:34
 * @ Description:
 */

namespace App\Containers\Comment\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Containers\Comment\Enums\CommentStatus;
use App\Containers\Course\Models\Course;
use App\Containers\Customer\Models\Customer;
use App\Containers\Product\Models\Product;
use App\Ship\Parents\Models\Model;
use Carbon\Carbon;

class Comment extends Model
{
    protected $table = 'comment';

    protected $fillable = [
        'status',
        'object_id',
        'customer_id',
        'created_at',
        'update_at',
        'message',
        'rating'
    ];

    public function customer(){
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }

    public function course(){
        return $this->hasOne(Course::class,'id','object_id');
    }
}
