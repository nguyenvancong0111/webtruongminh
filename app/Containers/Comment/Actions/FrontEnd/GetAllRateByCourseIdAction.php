<?php

namespace App\Containers\Comment\Actions\FrontEnd;

use App\Ship\Parents\Actions\Action;

class GetAllRateByCourseIdAction extends Action
{
    public function run($courseId, $with = [], $skipPaginate= false, $limit = 10, $currentPage = 1): iterable {
        return $this->call('Comment@FrontEnd\GetAllRateByCourseIdTask', [$courseId, $with, $skipPaginate, $limit, $currentPage]);
    }
}
