<?php

namespace App\Containers\Comment\Actions\FrontEnd;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Comment\Models\Comment;
use App\Ship\Parents\Actions\Action;

/**
 * Class CreateBannerAction.
 *
 */
class CreateCommentAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        $data = Apiato::call('Comment@FrontEnd\CreateCommentTask',[$data]);

        if ($data) {

            Apiato::call('User@CreateUserLogSubAction', [
                $data->id,
                [],
                $data->toArray(),
                'Đánh giá',
                Comment::class
            ]);
        }
        $this->clearCache();
        return $data;


    }
}
