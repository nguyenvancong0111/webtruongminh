<?php

namespace App\Containers\Comment\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Comment\Models\Banner;
use App\Containers\Comment\Models\Comment;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class UpdateBannerAction.
 *
 */
class EnableCommentAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $banner = Apiato::call('Comment@EnableCommentTask', [$data->id]);
        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            ['status' => 1],
            ['status' => 2],
            'Hiển thị đánh gía',
            Comment::class
        ]);

        $this->clearCache();

        return $banner;
    }
}
