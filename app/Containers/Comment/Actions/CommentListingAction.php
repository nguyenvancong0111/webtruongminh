<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-07 20:38:13
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Comment\Tasks\CommentListingTask;
use App\Ship\Parents\Actions\Action;

class CommentListingAction extends Action
{
    public function run($filters, $limit = 10)
    {
        $defaultLanguage = Apiato::call('Localization@GetDefaultLanguageTask');

        $data = app(CommentListingTask::class)->adminFilter($filters)->ordereByCreated();

//        if (!empty($filters->name)) {
//            $data->hasName($filters->name);
//        }

        return $data->run($limit);
    }
}
