<?php


namespace App\Containers\Comment\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Comment\Models\Comment;
use App\Ship\Parents\Actions\Action;

/**
 * Class DeleteCommentAction.
 *
 */
class DeleteCommentAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {

        Apiato::call('Comment@DeleteCommentTask', [$data->id]);

        $this->clearCache();

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            [],
            ['status' => -1],
            'Xóa Comment',
            Comment::class
        ]);
    }
}
