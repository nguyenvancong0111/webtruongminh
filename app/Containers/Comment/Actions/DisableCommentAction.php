<?php

namespace App\Containers\Comment\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Comment\Models\Comment;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Exception;

/**
 * Class UpdateCommentAction.
 *
 */
class DisableCommentAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $comment = Apiato::call('Comment@DisableCommentTask', [$data->id]);

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            ['status' => 2],
            ['status' => 1],
            'Ẩn Comment',
            Comment::class
        ]);

        $this->clearCache();

        return $comment;
    }
}
