<div class="tab-pane" id="contact" role="tabpanel" aria-expanded="false">
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                {{-- <div class="col-sm-6">
                    <div class="form-group">
                        <label for="title">Email nhận thông tin đơn hàng</label>
                        <input type="text" class="form-control{{ $errors->has('email_order') ? ' is-invalid' : '' }}" id="email_order"
                               name="contact[email_order]" value="{{ old('contact.email_order', @$data['contact']['email_order']) }}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="title">Email liên hệ</label>
                        <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email_hn"
                               name="contact[email]" value="{{ old('contact.email', @$data['contact']['email']) }}">
                    </div>
                </div> --}}
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="title">Hotline</label>
                        <input type="text" class="form-control{{ $errors->has('hotline') ? ' is-invalid' : '' }}" id="hotline"
                               name="contact[hotline]" value="{{ old('contact.hotline', @$data['contact']['hotline']) }}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="title">Tel</label>
                        <input type="text" class="form-control{{ $errors->has('tel') ? ' is-invalid' : '' }}" id="tel"
                               name="contact[tel]" value="{{ old('contact.tel', @$data['contact']['tel']) }}">
                    </div>
                </div>
                {{-- <div class="col-sm-6">
                    <div class="form-group">
                        <label for="title">Url Map</label>
                        <input type="text" class="form-control{{ $errors->has('map_url') ? ' is-invalid' : '' }}" id="map_url"
                               name="contact[map_url]" value="{{ old('contact.map_url', @$data['contact']['map_url']) }}">
                    </div>
                </div> --}}
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="title">Địa chỉ</label>
                        <textarea class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" id="address"
                                  name="contact[address]" rows="5">{{ old('contact.address', @$data['contact']['address']) }}</textarea>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="title">Ifram Map</label>
                        <textarea class="form-control{{ $errors->has('map_iframe') ? ' is-invalid' : '' }}" id="map_iframe"
                                  rows="5"
                                  name="contact[map_iframe]">{{ old('contact.map_iframe', @$data['contact']['map_iframe'])}}</textarea>
                    </div>
                </div>
            </div>

        </div>
        {{-- <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="title">Công ty</label>
                        <input type="text" class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}" id="company" name="contact[company]" value="{{ old('contact.company', @$data['contact']['company']) }}" >
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="title">Đăng ký kinh doanh</label>
                        <input type="text" class="form-control{{ $errors->has('dkkd') ? ' is-invalid' : '' }}" id="dkkd" name="contact[dkkd]" value="{{ old('contact.dkkd', @$data['contact']['dkkd']) }}" >
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="title">Giấy chứng nhận</label>
                        <input type="text" class="form-control{{ $errors->has('gcn') ? ' is-invalid' : '' }}" id="gcn" name="contact[gcn]" value="{{ old('contact.gcn', @$data['contact']['gcn']) }}" >
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>