<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Container
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'types' => [
        '0' => 'Desktop (Tablet)',
        '1' => 'Mobile',
    ],

    'status' => [
        'visible' => 1,
        'hidden' => 2,
        'old_delete' => -1
    ],
    'positions' => [
        'partner_home' => 'Đối tác - Trang Chủ'
    ]
];
