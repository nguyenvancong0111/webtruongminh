<?php

namespace App\Containers\Feedback\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Feedback\Models\Feedback;
use App\Containers\Partners\Models\Partners;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class UpdateBannerAction.
 *
 */
class UpdateFeedbackAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $beforeData = Apiato::call('Feedback@FindFeedbackByIdTask', [$data->id]);
        $feedback = Apiato::call('Feedback@SaveFeedbackTask',[$data]);

        if($feedback) {
            $original_desc = Apiato::call('Feedback@GetAllFeedbackDescTask', [$feedback->id]);

            Apiato::call('Feedback@SaveFeedbackDescTask', [
                $data,
                $original_desc,
                $feedback->id
            ]);

            Apiato::call('User@CreateUserLogSubAction', [
                $feedback->id,
                $beforeData->toArray(),
                $feedback->toArray(),
                'Cập nhật Feedback',
                Feedback::class
            ]);
        }

        return $feedback;
    }
}
