<?php

namespace App\Containers\Feedback\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class PartnersListingAction.
 *
 */
class FeedbackListingAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $filters, $order_by = 'sort_order asc, id desc', $limit = 10)
    {
        if (!empty($filters->position)) {
            $filters->positions = is_array($filters->position) ? implode(',', $filters->position) : $filters->position;
        }
        $defaultLanguage = Apiato::call('Localization@GetDefaultLanguageTask');
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;

        $criterias = [
            'addRequestCriteria',
            ['adminFilter' => [$filters]],
            ['withDescription' => [$language_id]],
            ['ordereByCreated' => [$order_by]]
        ];

        if(!empty($filters->name)){
            $criterias[] = ['hasName' => [$filters->name]];
        }

        $data = Apiato::call(
            'Feedback@FeedbackListingTask',
            [
                $limit
            ],
            $criterias
        );

        return $data;
    }
}
