<?php

namespace App\Containers\Feedback\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Feedback\Models\Feedback;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class CreateBannerAction.
 *
 */
class CreateFeedbackAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $feedback = Apiato::call('Feedback@CreateFeedbackTask',[$data]);

        if ($feedback) {
            Apiato::call('Feedback@SaveFeedbackDescTask', [$data, [], $feedback->id]);

            $feedback = Apiato::call('Feedback@FindFeedbackByIdTask',[$feedback->id]);
            Apiato::call('User@CreateUserLogSubAction', [
                $feedback->id,
                [],
                $feedback->toArray(),
                'Tạo Feedback',
                Feedback::class
            ]);
        }

        return $feedback;
    }
}
