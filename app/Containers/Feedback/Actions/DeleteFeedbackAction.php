<?php

namespace App\Containers\Feedback\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Feedback\Models\Feedback;
use App\Ship\Parents\Actions\Action;

/**
 * Class DeleteBannerAction.
 *
 */
class DeleteFeedbackAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        Apiato::call('Feedback@DeleteFeedbackTask', [$data->id]);
        Apiato::call('Feedback@DeleteFeedbackDescTask', [$data->id]);

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            [],
            ['status' => -1],
            'Xóa Feedback',
            Feedback::class
        ]);
    }
}
