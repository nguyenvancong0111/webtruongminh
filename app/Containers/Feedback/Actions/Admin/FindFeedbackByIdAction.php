<?php

namespace App\Containers\Feedback\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;


class FindFeedbackByIdAction extends Action
{

    public function run($feedback_id)
    {
        $data = Apiato::call('Feedback@FindFeedbackByIdTask', [
            $feedback_id,
            Apiato::call('Localization@GetDefaultLanguageTask'),
            ['with_relationship' => ['all_desc']]
        ]);

        return $data;
    }
}
