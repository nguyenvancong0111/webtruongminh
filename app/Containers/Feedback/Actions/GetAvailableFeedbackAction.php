<?php

namespace App\Containers\Feedback\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

class GetAvailableFeedbackAction extends Action
{
    public function run($orderBy = ['created_at' => 'desc', 'id' => 'desc'], $positions = [], $where = [], $limit = 0, $conditions = [])
    {
        return Apiato::call('Feedback@GetAvailableFeedbackTask', [$orderBy, $positions, $where, $limit], [
            ['with' => [$conditions]],
        ]);
    }
}
