<?php

namespace App\Containers\Feedback\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class BannerRepository.
 */
class FeedbackRepository extends Repository
{
    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Feedback';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'status',
        'publish_at',
        'position',
        'views',
        'is_mobile',
        'created_at',
        'image',
    ];
}
