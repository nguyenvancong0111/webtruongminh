<?php

namespace App\Containers\Feedback\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class PartnersDescRepository.
 */
class FeedbackDescRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Feedback';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'news_id',
        'language_id',
        'name',
        'tag',
    ];
}
