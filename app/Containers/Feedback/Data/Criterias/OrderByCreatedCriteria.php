<?php

namespace App\Containers\Feedback\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

/**
 * Class OrderByCreatedCriteria.
 *
 */
class  OrderByCreatedCriteria extends Criteria
{
    protected $order_by;

    public function __construct($order_by)
    {
        $this->order_by = $order_by;
    }
    /**
     * @param                                                   $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        $order_by = $this->order_by;
        return $model->orderByRaw($order_by);
    }
}
