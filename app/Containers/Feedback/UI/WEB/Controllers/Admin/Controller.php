<?php

namespace App\Containers\Feedback\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Feedback\Models\Feedback;
use App\Containers\Feedback\UI\WEB\Requests\Admin\FindFeedbackRequest;
use App\Containers\Feedback\UI\WEB\Requests\CreateFeedbackRequest;
use App\Containers\Feedback\UI\WEB\Requests\GetAllFeedbackRequest;
use App\Containers\Feedback\UI\WEB\Requests\UpdateFeedbackRequest;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use Exception;

class Controller extends AdminController
{
    public function __construct()
    {
        $this->title = 'Feedback';

        parent::__construct();

    }
    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllFeedbackRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $feedback = Apiato::call('Feedback@FeedbackListingAction', [new DataTransporter($request), $this->perPage]);


        $options = array_merge(['' => 'Chọn vị trí'], config('feedback-container.positions'));

        return view('feedback::admin.index', [
            'search_data' => $request,
            'data' => $feedback,
            'options' => $options,
        ]);
    }

    public function edit(FindFeedbackRequest $request)
    {
        $this->showEditForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['edit', $this->title, 'admin_feedback_home_page']);

        try{
            $feedback = Apiato::call('Feedback@Admin\FindFeedbackByIdAction', [$request->id]);
        }catch(Exception $e){
            return redirect()->route('admin_feedback_home_page', ['id' => $request->id])->with('status', 'Có lỗi');
        }

        if(!empty($feedback)) {
            return view('feedback::admin.edit', [
                'data' => $feedback,
                'positions' => config('feedback-container.positions')
            ]);
        }

        return $this->notfound($request->id);
    }

    public function add()
    {
        $this->showAddForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['add', $this->title, 'admin_feedback_home_page']);
        return view('feedback::admin.edit', [
            'positions' => config('feedback-container.positions')
        ]);
    }

    public function update(UpdateFeedbackRequest $request)
    {
        try {
            $tranporter = new DataTransporter($request);
            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'feedback', StringLib::getClassNameFromString(Feedback::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }
            if (isset($request->background)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'background', 'feedback', StringLib::getClassNameFromString(Feedback::class)]);
                if (!$image['error']) {
                    $tranporter->background = $image['fileName'];
                }
            }
            $feedback = Apiato::call('Feedback@UpdateFeedbackAction', [$tranporter]);


            if ($feedback) {
                return redirect()->route('admin_feedback_edit_page', ['id' => $feedback->id])->with('status', 'Cập nhật feedback thành công');
            }
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function create(CreateFeedbackRequest $request)
    {
        try {
            $tranporter = new DataTransporter($request);

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'feedback', StringLib::getClassNameFromString(Feedback::class)]);

                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }
            if (isset($request->background)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'background', 'feedback-background', StringLib::getClassNameFromString(Feedback::class)]);
                if (!$image['error']) {
                    $tranporter->background = $image['fileName'];
                }
            }
            $feedback = Apiato::call('Feedback@CreateFeedbackAction', [$tranporter]);
            if ($feedback) {
                return redirect()->route('admin_feedback_home_page')->with('status', 'feedback đã được thêm mới');
            }
        } catch (\Exception $e) {
            // throw $e;
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function delete(FindFeedbackRequest $request)
    {
        try {
            Apiato::call('Feedback@DeleteFeedbackAction', [$request]);
        } catch (\Exception $e) {
            // throw $e;
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

}
