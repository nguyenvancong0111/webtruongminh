<?php

namespace App\Containers\Feedback\UI\WEB\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateBannerRequest.
 *
 */
class CreateFeedbackRequest extends Request
{

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles'       => 'admin',
        'permissions' => 'feedback-manager',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'image' => 'required|mimes:jpeg,jpg,png,gif',
//            'position' => 'required',
            'feedback_description.*.name' => 'bail|required|string|max:255',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'image.required' => 'Vui lòng Upload ảnh',
            'image.mimes' => 'Ảnh không đúng định dạng (jpeg, jpg, png, gif)',
            'feedback_description.1.name.required' => 'Tên đối tác tiếng việt',
        ];
    }

    public function attributes()
    {
        return [
            'banner_description.1.name.required' => 'Tên đối tác tiếng việt',
            'banner_description.2.name' => 'Tên đối tác tiếng anh',
            'banner_description.3.name' => 'Tên đối tác tiếng trung',

        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
