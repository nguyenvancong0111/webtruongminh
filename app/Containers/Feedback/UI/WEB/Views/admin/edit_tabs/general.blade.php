<div class="tab-pane active" id="general">
    <div class="tabbable">
        <ul class="nav nav-tabs nav-underline nav-underline-primary mb-3" role="tablist">
            @foreach($langs as $it_lang)
                <li class="nav-item">
                    <a class="nav-link {{$loop->first ? 'active' : ''}}" href="#lang_{{$it_lang['language_id']}}" ><i class="icon-globe"></i> {{$it_lang['name']}}</a>
                </li>
            @endforeach
        </ul>

        <div class="tab-content p-0">
            @foreach($langs as $it_lang)
                <div class="tab-pane {{$loop->first ? 'active' : ''}}" id="lang_{{$it_lang['language_id']}}">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="title">Tên <span class="small text-danger">( {{$it_lang['name']}} )</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class=" input-group-text"><img src="{{ asset('admin/img/lang/'.$it_lang['image']) }}" title="{{$it_lang['name']}}"></span></div>
                                    <input type="text" class="form-control {{ $errors->has("feedback_description.{$it_lang['language_id']}.name") ? 'is-invalid' : '' }}"
                                           name="feedback_description[{{$it_lang['language_id']}}][name]"
                                           id="name_{{$it_lang['language_id']}}"
                                           value="{{ old('feedback_description.'.$it_lang['language_id'].'.name',@$data['all_desc'][$it_lang['language_id']]['name']) }}" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="title">Chức vụ <span class="small text-danger">( {{$it_lang['name']}} )</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class=" input-group-text"><img src="{{ asset('admin/img/lang/'.$it_lang['image']) }}" title="{{$it_lang['name']}}"></span></div>
                                    <input type="text" class="form-control {{ $errors->has("feedback_description.{$it_lang['language_id']}.position") ? 'is-invalid' : '' }}"
                                           name="feedback_description[{{$it_lang['language_id']}}][position]"
                                           id="position_{{$it_lang['language_id']}}"
                                           value="{{ old('feedback_description.'.$it_lang['language_id'].'.position',@$data['all_desc'][$it_lang['language_id']]['position']) }}" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                        <div class="form-group">
                            <label for="description_{{$it_lang['language_id']}}">Nội dung <span class="small text-danger">( {{$it_lang['name']}} )</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><img src="{{ asset('admin/img/lang/'.$it_lang['image']) }}" title="{{$it_lang['name']}}"></span></div>
                                <textarea rows="5" cols="40" class="form-control" name="feedback_description[{{$it_lang['language_id']}}][short_description]" id="description_{{$it_lang['language_id']}}" >{!! old('feedback_description.'.$it_lang['language_id'].'.short_description',@$data['all_desc'][$it_lang['language_id']]['short_description'])  !!}</textarea>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>