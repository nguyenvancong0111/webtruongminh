<div class="tab-pane" id="image">
    <div class="tabbable">
        <div class="row">
            {{--<div class="col-sm-6">
                <div class="form-group">
                    <label for="background">Background</label>
                    <input type="file" id="background" name="background" class="dropify form-control {{ $errors->has('background') ? ' is-invalid' : '' }}" data-show-remove="false" data-default-file="{{ \ImageURL::getImageUrl(old('background',@$data['background']), 'feedback', 'original')    }}">
                </div>
            </div>--}}
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" id="image" name="image" class="dropify form-control {{ $errors->has('image') ? ' is-invalid' : '' }}" data-show-remove="false" data-default-file="{{ \ImageURL::getImageUrl(old('image',@$data['image']), 'feedback', 'original')    }}">
                </div>
            </div>
        </div>
    </div>
</div>