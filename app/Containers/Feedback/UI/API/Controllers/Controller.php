<?php

namespace App\Containers\Feedback\UI\API\Controllers;

use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use App\Containers\Feedback\Actions\FeedbackListingAction;
use App\Containers\Feedback\UI\API\Requests\GetFeedbackRequest;
use App\Containers\Feedback\UI\API\Transformers\FeedbackTransformer;
use App\Ship\Transporters\DataTransporter;

/**
 * Class Controller.
 */
class Controller extends BaseApiFrontController
{

    public function getFeedbackHome(GetFeedbackRequest $request){
        $news = app(FeedbackListingAction::class)->skipCache(true)->run(
            new DataTransporter(['status' => 1]), 'rate DESC, id desc', 3
        );
        return $this->transform($news, new FeedbackTransformer());
    }
}
