<?php

namespace App\Containers\Feedback\UI\API\Transformers;

use App\Containers\Feedback\Models\Feedback;
use App\Ship\Parents\Transformers\Transformer;

class FeedbackTransformer extends Transformer
{
    /**
     * @param $token
     *
     * @return  array
     */
    public function transform(Feedback $data)
    {
        $response = [
            'id' => $data->id,
            'desc' => $data->relationLoaded('desc') ? $data->desc : $this->null(),
            'image' => $data->getImageUrl(),
            'rate' => $data->rate
        ];
        return $response;
    }
}
