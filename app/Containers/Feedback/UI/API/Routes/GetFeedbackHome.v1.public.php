<?php

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

$router->get('feedback-home', [
    'as' => 'api_feedback_list_home',
    'uses'       => 'Controller@getFeedbackHome',
    'prefix' => app(CheckSegmentLanguageAction::class)->run(),
    'middleware' => [
        'api',
    ],
]);
