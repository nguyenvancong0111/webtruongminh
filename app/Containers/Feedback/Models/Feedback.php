<?php
/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2020-10-04 13:58:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2020-10-21 14:34:21
 * @ Description:
 */

namespace App\Containers\Feedback\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Containers\CommonTemplate\Traits\UnitTrait;
use App\Ship\core\Traits\HelpersTraits\LangTrait;
use App\Ship\Parents\Models\Model;

class Feedback extends Model
{
    protected $table = 'feedback';
    protected static $allAvailablePositions = [];

    protected $fillable = [
        'status',
        'sort_order',
        'position',
        'is_mobile',
        'image',
        'background',
        'rate'
    ];

    const SIZE = [
        'big_home' => ['width' => 1920, 'height' => 0],
        'big_introduce' => ['width' => 1920, 'height' => 0],
        'big_contact' => ['width' => 1920, 'height' => 0],
        // 'fae_home' => ['width' => 600, 'height' => 398],
    ];

    public static function getSize($type = 'big_home')
    {
        if (isset(self::SIZE[$type])) {
            return (object)self::SIZE[$type];
        }
        return false;
    }


    public function positions()
    {

        $configPositions = config('feedback-container.positions');
        $all = explode(',', $this->position);
        if (!empty($all)) {
            $tmp = [];
            foreach ($all as $a) {
                if (isset($configPositions[$a])) {
                    $tmp[$a] = $configPositions[$a];
                }
            }
            return $tmp;
        }
        return false;
    }

    public function desc()
    {
        return $this->hasOne(FeedbackDesc::class, 'feedback_id', 'id');
    }

    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', FeedbackDesc::class, 'feedback_id', 'id');
    }

    public function scopeAvailable($query, array $positions = [])
    {
        return $query->where('status', 2)->where(function ($query) use ($positions) {
            foreach ($positions as $position) {
                $query->orWhereRaw("LOCATE('{$position}', position) > 0");
            }
        });
    }

    public function getBannerLink(): string
    {
        if (!empty($this->desc->link)) {
            return $this->desc->link;
        }

        return route('contractors.verified');
    }

    public function getImageUrl($size = 'large')
    {
        return ImageURL::getImageUrl($this->image, 'feedback', $size);
    }

}