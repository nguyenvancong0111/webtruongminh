<?php

namespace App\Containers\Feedback\Tasks;

use App\Containers\Feedback\Data\Repositories\FeedbackDescRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class GetAllPartnersDescTask.
 */
class GetAllFeedbackDescTask extends Task
{

    protected $repository;

    public function __construct(FeedbackDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($partner_id)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('feedback_id', $partner_id));
        $wery = $this->repository->all()->keyBy('language_id')->toArray();

        return $wery;
    }
}
