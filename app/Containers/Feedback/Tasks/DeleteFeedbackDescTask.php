<?php

namespace App\Containers\Feedback\Tasks;

use App\Containers\Feedback\Data\Repositories\FeedbackDescRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeletePartnersDescTask.
 */
class DeleteFeedbackDescTask extends Task
{

    protected $repository;

    public function __construct(FeedbackDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($id)
    {
        try {
            $this->repository->getModel()->where('feedback_id', $id)->delete();
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
