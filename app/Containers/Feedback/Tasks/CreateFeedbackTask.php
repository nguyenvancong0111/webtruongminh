<?php

namespace App\Containers\Feedback\Tasks;

use App\Containers\Feedback\Data\Repositories\FeedbackRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class CreateBannerTask.
 */
class CreateFeedbackTask extends Task
{

    protected $repository;

    public function __construct(FeedbackRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $data = Arr::except($data->toArray(), ['feedback_description', '_token', '_headers']);
//            $data['position'] = is_array($data['position']) ? implode(',', $data['position']) : $data['position'];
//            $data['is_mobile'] = isset($data['is_mobile']) ? 1 : 0;
            if(!empty($data['position'])){
                $data['position'] = is_array($data['position']) ? implode(',', $data['position']) : $data['position'];
            }
            $feedback = $this->repository->create($data);
            return $feedback;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
