<?php

namespace App\Containers\Feedback\Tasks;

use App\Containers\Feedback\Data\Repositories\FeedbackRepository;
use App\Containers\Partners\Data\Criterias\MobileCriteria;
use App\Ship\Parents\Tasks\Task;

class GetAvailableFeedbackTask extends Task
{
    protected $repository;

    public function __construct(FeedbackRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($orderBy = ['created_at' => 'desc', 'id' => 'desc'], $position = [], $where = [], $limit = 0)
    {
        foreach ($orderBy as $column => $direction) {

           $this->repository->orderBy($column, $direction);
        }

        if (!empty($position)) {
            foreach ($position as $column => $direction) {
                $this->repository->where($column, 'like', $direction);
            }
        }

        if (!empty($where)) {
            foreach ($where as $column => $direction) {
                $this->repository->where($column, $direction);
            }
        }

        if (empty($limit)) {
            return $this->repository->get();
        }

        return $this->repository->with('desc')->paginate($limit);
    }

    public function mobile(bool $isMobile = false)
    {
        $this->repository->pushCriteria(new MobileCriteria($isMobile));
    }
}
