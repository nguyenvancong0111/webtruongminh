<?php

namespace App\Containers\Feedback\Tasks;

use App\Containers\Feedback\Data\Repositories\FeedbackRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteBannerTask.
 */
class DeleteFeedbackTask extends Task
{

    protected $repository;

    public function __construct(FeedbackRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($id)
    {
        try {
            $this->repository->getModel()->where('id', $id)->update(
                [
                    'status' => -1
                ]
            );
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
