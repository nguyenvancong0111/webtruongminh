<?php

namespace App\Containers\Feedback\Tasks;

use App\Containers\Feedback\Data\Repositories\FeedbackDescRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SavePartnersDescTask.
 */
class SaveFeedbackDescTask extends Task
{

    protected $repository;

    public function __construct(FeedbackDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data, $original_desc, $feedback_id, $edit_id = null)
    {
        $feedback_description = isset($data['feedback_description']) ? (array)$data['feedback_description'] : null;
        if (is_array($feedback_description) && !empty($feedback_description)) {
            $updates = [];
            $inserts = [];
            foreach ($feedback_description as $k => $v) {
                if (isset($original_desc[$k])) {
                    $updates[$original_desc[$k]['id']] = [
                        'name' => $v['name'],
                        'position' => $v['position'],
//                        'link' => $v['link'],
                        'short_description' => $v['short_description'],
                    ];
                    if(isset($data['image']) && !empty($data['image'])){
                        $updates[$original_desc[$k]['id']]['image'] = $data['image'];
                    }
                } else {
                    $inserts[] = [
                        'feedback_id' => $feedback_id,
                        'language_id' => $k,
                        'name' => $v['name'],
                        'position' => $v['position'],
//                        'link' => $v['link'],
                        'short_description' => $v['short_description'],
                    ];
                }
            }
            if (!empty($updates)) {
                $this->repository->updateMultiple($updates);
            }

            if (!empty($inserts)) {
                $this->repository->getModel()->insert($inserts);
            }
        }
    }
}
