<?php

namespace App\Containers\Feedback\Tasks;

use App\Containers\Feedback\Data\Repositories\FeedbackRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class FindBannerByIdTask.
 */
class FindFeedbackByIdTask extends Task
{

    protected $repository;

    public function __construct(FeedbackRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($feedback_id, $defaultLanguage = 1, $external_data = ['with_relationship' => ['all_desc']])
    {

        $data = $this->repository->with(array_merge($external_data['with_relationship']))->find($feedback_id);

        return $data;
    }
}