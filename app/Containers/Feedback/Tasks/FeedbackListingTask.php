<?php

namespace App\Containers\Feedback\Tasks;

use App\Containers\Feedback\Data\Repositories\FeedbackRepository;
use App\Containers\Feedback\Data\Criterias\AdminFilterCriteria;
use App\Containers\Feedback\Data\Criterias\HasNameCriteria;
use App\Containers\Feedback\Data\Criterias\OrderByCreatedCriteria;
use App\Containers\Feedback\Data\Criterias\WithAllDescriptionCriteria;
use App\Containers\Feedback\Data\Criterias\WithDescriptionCriteria;
use App\Ship\Parents\Tasks\Task;


/**
 * Class BannerListingTask.
 */
class FeedbackListingTask extends Task
{

    protected $repository;

    public function __construct(FeedbackRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($limit = 20){
        return $this->repository->paginate($limit);
    }

    public function adminFilter($request) {
        $this->repository->pushCriteria(new AdminFilterCriteria($request));
    }

    public function hasName($name) {
        $this->repository->pushCriteria(new HasNameCriteria($name));
    }

    public function withDescription($language_id) {
        $this->repository->pushCriteria(new WithDescriptionCriteria($language_id));
    }

    public function withAllDescription() {
        $this->repository->pushCriteria(new WithAllDescriptionCriteria());
    }

    public function ordereByCreated($order_by)
    {
        $this->repository->pushCriteria(new OrderByCreatedCriteria($order_by));
    }
}
