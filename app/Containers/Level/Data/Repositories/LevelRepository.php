<?php

namespace App\Containers\Level\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class LevelRepository
 */
class LevelRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
