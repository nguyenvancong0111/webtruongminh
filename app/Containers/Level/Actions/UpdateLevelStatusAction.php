<?php

namespace App\Containers\Level\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Level\Tasks\UpdateLevelStatusTask;
use App\Ship\Transporters\DataTransporter;

class UpdateLevelStatusAction extends Action
{
    public function run($level_id, $status)
    {
        return app(UpdateLevelStatusTask::class)->run($level_id, $status);
    }
}
