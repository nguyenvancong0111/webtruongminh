<?php

namespace App\Containers\Level\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteLevelAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Level@DeleteLevelTask', [$request->id]);
    }
}
