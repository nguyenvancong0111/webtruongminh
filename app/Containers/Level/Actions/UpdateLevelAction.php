<?php

namespace App\Containers\Level\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class UpdateLevelAction extends Action
{
    public function run(DataTransporter $data)
    {
        $level = Apiato::call('Level@UpdateLevelTask', [$data]);
        if ($level) {
            $original_desc = Apiato::call('Level@FindLevelByIdTask', [$level->id, ['all_desc']]);
            Apiato::call('Level@SaveLevelDescTask', [$data, $original_desc->all_desc->toArray(), $level->id]);

        }
        return $level;
    }
}
