<?php

namespace App\Containers\Level\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class CreateLevelAction extends Action
{
    public function run(DataTransporter $data)
    {
        $level = Apiato::call('Level@CreateLevelTask', [$data]);
        if ($level) {
            Apiato::call('Level@SaveLevelDescTask', [$data, [], $level->id]);
        }
        return $level;
    }
}
