<?php

namespace App\Containers\Level\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class GetAllLevelsAction extends Action
{
    public function run(DataTransporter $data, $with = ['desc'], $orderBy = ['created_at' => 'desc'], $hasPage = true, $limit = 13)
    {
        $levels = Apiato::call(
            'Level@GetAllLevelsTask',
            [$limit, $hasPage],
            [
                ['where' => [$data->toArray()]],
                ['with' => [$with]],
                ['orderBy' => [$orderBy]],
                ['name' => [$data->name]],


            ]
        );

        return $levels;
    }
}
