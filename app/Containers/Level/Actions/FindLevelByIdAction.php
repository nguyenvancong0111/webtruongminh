<?php

namespace App\Containers\Level\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindLevelByIdAction extends Action
{
    public function run($id, $with = ['all_desc'])
    {
        $level = Apiato::call('Level@FindLevelByIdTask', [$id, $with]);

        return $level;
    }
}
