<?php
Route::group(
    [
        'prefix' => 'level',
        'namespace' => '\App\Containers\Level\UI\WEB\Controllers',
        'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
        'middleware' => [
            'auth:admin',
        ],
    ],
    function () use ($router) {
        $router->get('/', [
            'as'   => 'admin_level_home_page',
            'uses' => 'Controller@index',
        ]);

        $router->get('/edit/{id}', [
            'as'   => 'admin_level_edit_page',
            'uses' => 'Controller@edit',
        ]);

        $router->post('/edit/{id}', [
            'as'   => 'admin_level_edit_page',
            'uses' => 'Controller@update',
        ]);

        $router->get('/add', [
            'as'   => 'admin_level_add_page',
            'uses' => 'Controller@create',
        ]);
        $router->post('/update-status', [
            'as'   => 'admin_level_update_status',
            'uses' => 'Controller@updateStatus',
        ]);
        $router->post('/add', [
            'as'   => 'admin_level_add_page',
            'uses' => 'Controller@store',
        ]);

        $router->delete('/{id}', [
            'as' => 'admin_level_delete',
            'uses'       => 'Controller@delete',
        ]);

        $router->get('/getlevelsAutocomplete', [
            'as'   => 'admin_level_get_autôcmplete',
            'uses' => 'Controller@getlevelsAutocomplete',
        ]);
        $router->get('/getAjaxLevel', [
            'as'   => 'get_ajax_level',
            'uses' => 'Controller@getAjaxLevel',
        ]);
    }
);
