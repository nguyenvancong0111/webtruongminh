<?php

namespace App\Containers\Level\UI\WEB\Controllers;

use App\Containers\Level\UI\WEB\Transformers\LevelTransformer;
use App\Containers\Level\UI\WEB\Requests\CreateLevelRequest;
use App\Containers\Level\UI\WEB\Requests\DeleteLevelRequest;
use App\Containers\Level\UI\WEB\Requests\GetAllLevelsRequest;
use App\Containers\Level\UI\WEB\Requests\FindLevelByIdRequest;
use App\Containers\Level\UI\WEB\Requests\UpdateLevelRequest;
use App\Containers\Level\UI\WEB\Requests\StoreLevelRequest;
use App\Containers\Level\UI\WEB\Requests\EditLevelRequest;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\AdminController;
use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\Level\Actions\UpdateLevelStatusAction;
use App\Containers\Level\UI\WEB\Requests\UpdateLevelStatusRequest;
use App\Ship\Transporters\DataTransporter;
use Exception;

/**
 * Class Controller
 *
 * @package App\Containers\Level\UI\WEB\Controllers
 */
class Controller extends AdminController
{
    public function __construct()
    {
        $this->title = 'Quản trị Level câu hỏi';
        parent::__construct();
    }
    /**
     * Show all entities
     *
     * @param GetAllLevelsRequest $request
     */
    public function index(GetAllLevelsRequest $request)
    {
        $dataTranporter = new DataTransporter($request);
        if (!isset($dataTranporter->status)) {
            $dataTranporter->status = [1, 2];
        }
        $levels = Apiato::call('Level@GetAllLevelsAction', [$dataTranporter]);
        // dd($levels);
        return view('level::admin.index', [
            'data' => $levels,
            'search_data' => $request
        ]);

        // ..
    }

    /**
     * Show one entity
     *
     * @param FindLevelByIdRequest $request
     */
    public function show(FindLevelByIdRequest $request)
    {
        $level = Apiato::call('Level@FindLevelByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateLevelRequest $request
     */
    public function create(CreateLevelRequest $request)
    {
        $this->showAddForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Tùy chọn', $this->form == 'list' ? '' : route('admin_level_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Thêm mới');
        view()->share('breadcrumb', $this->breadcrumb);

        return view('level::admin.edit', []);
    }
    /**
     * Add a new entity
     *
     * @param StoreLevelRequest $request
     */
    public function store(StoreLevelRequest $request)
    {
        // dd($request);
        try {
            $dataTranporter = new DataTransporter($request);
            $level = Apiato::call('Level@CreateLevelAction', [$dataTranporter]);
            if ($level) {
                return redirect()->route('admin_level_home_page')->with('status', 'Đã được thêm mới');
            } else {
                return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
            }
        } catch (Exception $e) {
            dd($e->getMessage(), $e->getFile());
        }


        // ..
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditLevelRequest $request
     */
    public function edit(EditLevelRequest $request)
    {
        $level = Apiato::call('Level@FindLevelByIdAction', [$request->id]);
        $this->showEditForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Tùy chọn', $this->form == 'list' ? '' : route('admin_level_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Chỉnh sửa');
        view()->share('breadcrumb', $this->breadcrumb);

        return view('level::admin.edit', ['data' => $level]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateLevelRequest $request
     */
    public function update(UpdateLevelRequest $request)
    {
        // $level = Apiato::call('Level@UpdateLevelAction', [$request]);

        try {
            $dataTranporter = new DataTransporter($request);
            $level = Apiato::call('Level@UpdateLevelAction', [$dataTranporter]);
            if ($level) {
                return redirect()->route('admin_level_edit_page', ['id' => $level->id])->with('status', 'Đã được cập nhật');
            } else {
                return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
            }
        } catch (Exception $e) {
            dd($e->getMessage(), $e->getFile());
        }
        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteLevelRequest $request
     */
    public function delete(DeleteLevelRequest $request)
    {
        $result = Apiato::call('Level@DeleteLevelAction', [$request]);

        // ..
    }
    public function updateStatus(UpdateLevelStatusRequest $request)
    {
        $result = app(UpdateLevelStatusAction::class)->run($request->id, $request->status);
        // dd($request->all());
        if ($result) {
            return $this->sendResponse($result, 'Success');
        }
        return $this->sendError('Không update được dữ liệu!');
    }
    public function getAjaxLevel(GetAllLevelsRequest $request)
    {
        $dataTranporter = new DataTransporter($request);
        if (!isset($dataTranporter->status)) {
            $dataTranporter->status = 2;
        }
        $levels = Apiato::call('Level@GetAllLevelsAction', [$dataTranporter]);

        return $this->transform($levels, new LevelTransformer());
    }
}
