@extends('basecontainer::admin.layouts.default')

@section('right-breads')
    {!! $data->appends($search_data->toArray())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5">
                <h1>{{ $site_title }}</h1>
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            {!! Form::open(['url' => route('admin_level_home_page'), 'method' => 'get', 'id' => 'searchForm']) !!}
            <div class="card card-accent-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i class="fa fa-bookmark-o"></i></span></div>
                                <input type="text" name="name" class="form-control" placeholder="Tên"
                                    value="{{ $search_data->name }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                    <a href="{{ route('admin_level_home_page') }}" class="btn btn-sm btn-primary"><i
                            class="fa fa-refresh"></i> Reset</a>
                    @if ($user->can('add-level'))
                        <a href="{{ route('admin_level_add_page') }}" class="btn btn-sm btn-primary"><i
                                class="fa fa-plus"></i> Thêm mới</a>
                    @endif

                </div>
            </div>
            {!! Form::close() !!}

            <div class="card card-accent-primary">
                <div class="card-body p-0">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="50">ID</th>
                                <th >Nội dung</th>
                                <th width="100">Sắp xếp</th>
                                <th width="25">Lệnh</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $k => $item)
                                <tr>
                                    <td class="text-center">{{ @$item->id }}</td>
                                    <td>
                                        <p><strong>Tiêu đề: </strong> {{ @$item->desc->name }}</p>
                                        <p><strong>Mô tả: </strong> {{ @$item->desc->short_desc }}</p>
                                    </td>
                                    <td class="text-center">{{ @$item->sort_order }}</td>
                                    <td align="center">
                                        @if($user->can('edit-level'))
                                            @if ($item->status == 2)
                                                <a href="javascript:void(0)" data-route="{{ route('admin_level_update_status', ['field' => 'status']) }}" class=" btn text-primary" onclick="admin.updateStatus(this, {{ $item->id }}, 1,'id')" title="Đang hiển thị, Click để ẩn"><i class="fa fa-eye"></i></a>
                                            @else
                                                <a href="javascript:void(0)" data-route="{{ route('admin_level_update_status', ['field' => 'status']) }}" class=" btn text-danger" onclick="admin.updateStatus(this, {{ $item->id }},2, 'id')" title="Đang ẩn, Click để hiển thị"><i class="fa fa-eye"></i></a>
                                            @endif

                                            <a href="{{ route('admin_level_edit_page', $item->id) }}" lass="btn text-primary"><i class="fa fa-pencil"></i></a>
                                        @endif

                                        @if ($item->status != -1)
                                            <a data-href="{{ route('admin_level_delete', $item->id) }}" class="btn text-danger" onclick="admin.delete_this(this);"><i class="fa fa-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @include('basecontainer::admin.inc.bottom-pager-infor')
                </div>
            </div>
        </div>
    </div>
@stop
