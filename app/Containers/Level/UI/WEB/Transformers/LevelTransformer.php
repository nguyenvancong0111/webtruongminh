<?php

namespace App\Containers\Level\UI\WEB\Transformers;

use App\Ship\Parents\Transformers\Transformer;
use Illuminate\Support\Facades\Config;

/**
 * Class LevelTransformer.
 *
 */
class LevelTransformer extends Transformer
{

    /**
     * @param $token
     *
     * @return  array
     */
    public function transform($data)
    {
        $response = [
            'id' => $data->id,
            'name' =>   $data->desc->name ?? '',
            'short_description' =>   $data->desc->short_desc,
        ];

        return $response;
    }
}
