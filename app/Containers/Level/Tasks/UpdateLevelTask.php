<?php

namespace App\Containers\Level\Tasks;

use App\Containers\Level\Data\Repositories\LevelRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

class UpdateLevelTask extends Task
{

    protected $repository;

    public function __construct(LevelRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data->toArray(), ['desc', '_token', '_headers']);
            return $this->repository->update($dataUpdate, $data->id);
        } catch (Exception $exception) {
            dd($exception->getMessage(),$exception->getFile());
            throw new UpdateResourceFailedException();
        }
    }
}
