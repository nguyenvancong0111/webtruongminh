<?php

namespace App\Containers\Level\Tasks;

use App\Containers\Level\Data\Repositories\LevelRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteLevelTask extends Task
{

    protected $repository;

    public function __construct(LevelRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
