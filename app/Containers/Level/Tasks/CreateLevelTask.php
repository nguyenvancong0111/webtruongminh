<?php

namespace App\Containers\Level\Tasks;

use App\Containers\Level\Data\Repositories\LevelRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

class CreateLevelTask extends Task
{

    protected $repository;

    public function __construct(LevelRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run( $data)
    {
        try {
            $dataUpdate = Arr::except($data->toArray(), ['desc', '_token', '_headers']);

            return $this->repository->create($dataUpdate);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
