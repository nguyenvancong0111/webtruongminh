<?php

namespace App\Containers\Level\Tasks;

use App\Containers\Level\Data\Repositories\LevelRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisInThatCriteria;

class GetAllLevelsTask extends Task
{

    protected $repository;

    public function __construct(LevelRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    
    public function run($limit,  $hasPage)
    {
        return $hasPage ? $this->repository->paginate($limit) : ($limit == 0 ? $this->repository->get() : $this->repository->take($limit)->get());
    }
    public function name($name)
    {

        if (!empty($name) && $name != '') {
            $this->repository->whereHas('desc', function (Builder $query) use ($name) {

                $query->select('name', 'question_level_id');
                $query->where('name', 'like', '%' . $name . '%');
            });
        }
    }
    public function where($where = [])
    {
        $fillable = $this->repository->getModel()->getFillable();
        array_push($fillable, 'id');
        foreach ($where as $key => $val) {
            if (in_array($key, $fillable) && !empty($val)) {
                if (is_array($val)) {
                    $this->repository->pushCriteria(new ThisInThatCriteria($key, $val));
                } else {
                    $this->repository->pushCriteria(new ThisEqualThatCriteria($key, $val));
                }
            }
        }
    }

}
