<?php

namespace App\Containers\Level\Tasks;

use App\Containers\Level\Data\Repositories\LevelRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

class UpdateLevelStatusTask extends Task
{

    protected $repository;

    public function __construct(LevelRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, $status)
    {
        try {

            return $this->repository->where('id', $id)->update(['status' => $status]);
        } catch (Exception $exception) {
            dd($exception->getMessage(), $exception->getFile());
            throw new UpdateResourceFailedException();
        }
    }
}
