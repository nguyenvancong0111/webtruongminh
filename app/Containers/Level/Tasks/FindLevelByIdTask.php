<?php

namespace App\Containers\Level\Tasks;

use App\Containers\Level\Data\Repositories\LevelRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindLevelByIdTask extends Task
{

    protected $repository;

    public function __construct(LevelRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, $with)
    {
        try {
            return $this->repository->with($with)->find($id);
        } catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
