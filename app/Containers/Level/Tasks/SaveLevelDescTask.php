<?php

namespace App\Containers\Level\Tasks;

use Apiato\Core\Foundation\Facades\StringLib;
use App\Containers\Level\Data\Repositories\LevelDescRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SaveNewsDescTask.
 */
class SaveLevelDescTask extends Task
{

    protected $repository;

    public function __construct(LevelDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data, $original_desc, $level_id)
    {
        $desc = isset($data['desc']) ? (array)$data['desc'] : null;
        if (is_array($desc) && !empty($desc)) {
            $updates = [];
            $inserts = [];
            foreach ($desc as $k => $v) {
                if (isset($original_desc[$k])) {
                    $updates[$original_desc[$k]['id']] = [
                        'name' => isset($v['name']) ? $v['name'] : '',
                        'short_desc' => isset($v['short_desc']) ? $v['short_desc'] : '',
                    ];
                } else {
                    $inserts[] = [
                        'question_level_id' => $level_id,
                        'language_id' => $k,
                        'name' => $v['name'],
                        'short_desc' => $v['short_desc'],
                    ];
                }
            }
            if (!empty($updates)) {
                $this->repository->updateMultiple($updates);
            }

            if (!empty($inserts)) {
                $this->repository->getModel()->insert($inserts);
            }
        }
    }

    public function to_slug($string, $separator = '-')
    {
        $re = "/(\\s|\\" . $separator . ")+/mu";
        $str = @trim($string);
        $subst = $separator;
        $result = preg_replace($re, $subst, $str);

        return $result;
    }
}
