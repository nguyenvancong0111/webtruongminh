<?php

namespace App\Containers\Level\Models;

use App\Ship\core\Traits\HelpersTraits\HasManyKeyBy;
use App\Ship\Parents\Models\Model;

class Level extends Model
{
    protected $table = "question_level";
    protected $fillable = [
        'status',
        'type',
        'sort_order'
    ];

    protected $attributes = [];

    protected $hidden = [];

    protected $casts = [];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    public function desc()
    {
        return $this->hasOne(LevelDesc::class, 'question_level_id', 'id');
    }
    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', LevelDesc::class, 'question_level_id', 'id');
    }
    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'levels';
}
