<?php

namespace App\Containers\Category\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class CategoryRepository.
 */
class CategoryRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Category';

    public const ALL = 'ALL';
    public const NEWS = 'NEWS';
    public const PRODUCT = 'PRODUCT';

    public $TYPE = [
        'ALL' => -1,
        'PRODUCT' => 1,
        'NEWS' => 2
    ];
    public $currentType = -1;
    public $separator = '&nbsp;&nbsp;&gt;&nbsp;&nbsp;';

    public $categoryDesc, $categoryPath;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'status',
        'created_at',
        'desc.name',
        'desc.language_id'
    ];
}
