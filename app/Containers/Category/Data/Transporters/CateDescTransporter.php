<?php

namespace App\Containers\Category\Data\Transporters;

use App\Ship\Parents\Transporters\Transporter;

/**
 * Class CateDescTransporter
 *
 */
class CateDescTransporter extends Transporter
{

    /**
     * @var array
     */
    protected $schema = [
        'type' => 'object',
        'properties' => [
            'additionalProperties' => true,
        ],
        'required'   => [
            
        ],
        'default'    => [
           
        ]
    ];
}
