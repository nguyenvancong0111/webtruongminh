<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 23:29:32
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-08 23:31:14
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\Actions\Admin;

use App\Ship\Parents\Actions\Action;


class UpdateSomeStatusAction extends Action
{
    public function run(string $field,int $id, int $status, string $id_alias_field = '') :? bool
    {
        $result = $this->call('Category@Admin\UpdateSomeStatusTask',[
            $field,
            $id,
            $status,
            $id_alias_field
        ]);

        $this->clearCache();

        return $result;
    }
}
