<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 20:43:34
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-14 11:04:44
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\Actions;

use Apiato\Core\Foundation\StringLib;
use App\Containers\Category\Actions\SubActions\GetAllCategoriesForFrontEndSubAction;
use App\Containers\Category\Actions\SubActions\PresentFrontEndCategoriesSubAction;
use App\Containers\Category\Tasks\GetAllCategoriesForFrontEndTask;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetCategoryLikeSearchAction extends Action
{
    public $getAllCategoriesForFrontEndTask, $presentFrontEndCategoriesSubAction, $getAllCategoriesForFrontEndSubAction;

    public function __construct(
        GetAllCategoriesForFrontEndTask $getAllCategoriesForFrontEndTask,
        PresentFrontEndCategoriesSubAction $presentFrontEndCategoriesSubAction,
        GetAllCategoriesForFrontEndSubAction $getAllCategoriesForFrontEndSubAction
    )
    {
        parent::__construct();
        $this->getAllCategoriesForFrontEndTask = $getAllCategoriesForFrontEndTask;
        $this->presentFrontEndCategoriesSubAction = $presentFrontEndCategoriesSubAction;
        $this->getAllCategoriesForFrontEndSubAction = $getAllCategoriesForFrontEndSubAction;
    }

    public function run(?int $categoryType, ?string $keyword, Language $currentLang = null, bool $withParent = false): ?array
    {
        return $this->remember(function () use ($categoryType, $keyword, $currentLang, $withParent) {
            $pureCategories = $this->getAllCategoriesForFrontEndSubAction->run(
                ['cate_type' => $categoryType],
                $currentLang,
                true,
                'category.sort_order ASC,category.created_at DESC'
            );

            $treeCategories = $this->presentFrontEndCategoriesSubAction->run($pureCategories);

            $currentCategory = [];
            $currentCategory = $this->returnCateByKeyword($treeCategories, explode(' ', StringLib::lower($keyword)), $currentCategory);

            return [
                'current' => empty($currentCategory) ? $this->returnRandElements($treeCategories, array_rand($treeCategories, count($treeCategories))) : $currentCategory,
                'parents' => [], //array_reverse($parents),
            ];
        }, null, [], 0, $this->skipCache);
    }

    private function returnCateByKeyword(array $elements, array $keyword = [], &$branch = []): ?array
    {
        foreach ($elements as $element) {
            if (StringLib::contains(StringLib::lower(@$element['desc']['slug']), $keyword) || StringLib::contains(StringLib::lower(@$element['desc']['name']), $keyword)) {
                $tempElement = $element;
                $tempElement['sub'] = [];
                $branch[] = $tempElement;
            }

            if (!empty($element['sub'])) {
                $this->returnCateByKeyword($element['sub'], $keyword, $branch);
            }
        }

        return $branch;
    }

    public function findParents(array $input, int $id, array &$parents)
    {
        foreach ($input as $val) {
            if ($val['category_id'] == $id) {
                array_push($parents, $val);
                if (!empty($val['parent_id'])) {
                    $this->findParents($input, $val['parent_id'], $parents);
                }
                break;
            }
        }
    }

    private function returnRandElements(array $haystack, array $indexes)
    {
        $returnArr = [];
        foreach ($indexes as $idx) {
            $returnArr[] = $haystack[$idx];
        }

        return $returnArr;
    }
}
