<?php

namespace App\Containers\Category\Actions;

use App\Containers\Category\Actions\SubActions\GetAllCategoriesForFrontEndSubAction;
use App\Containers\Category\Actions\SubActions\PresentFrontEndCategoriesSubAction;
use App\Ship\Parents\Actions\Action;

class PresentFrontEndCategoryAction extends Action
{
    public $presentFrontEndCategoriesSubAction,$getAllCategoriesForFrontEndSubAction;
    public function __construct(
        PresentFrontEndCategoriesSubAction $presentFrontEndCategoriesSubAction,
        GetAllCategoriesForFrontEndSubAction $getAllCategoriesForFrontEndSubAction
    ) {
        parent::__construct();
        $this->presentFrontEndCategoriesSubAction = $presentFrontEndCategoriesSubAction;
        $this->getAllCategoriesForFrontEndSubAction = $getAllCategoriesForFrontEndSubAction;
    }
    public function run($filters = [], $currentLang, $limit = 10, $orderBy = 'category.sort_order,category.created_at desc', $skipPagination = false): ?array
    {
        $data = $this->getAllCategoriesForFrontEndSubAction->run(
            $filters,
            $currentLang,
            $skipPagination,
            $limit,
            $orderBy
        );
        $data =  $this->presentFrontEndCategoriesSubAction->run($data);

        return $data;
    }
}
