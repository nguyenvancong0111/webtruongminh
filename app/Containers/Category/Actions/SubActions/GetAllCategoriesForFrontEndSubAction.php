<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-25 23:31:45
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-05 23:39:38
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\Actions\SubActions;

use App\Containers\Category\Enums\CategoryStatus;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\Category\Tasks\GetAllCategoriesForFrontEndTask;
use App\Ship\Parents\Actions\SubAction;

class GetAllCategoriesForFrontEndSubAction extends SubAction
{
    protected $keyBy = null;

    public $getAllCategoriesForFrontEndTask;

    public function __construct(GetAllCategoriesForFrontEndTask $getAllCategoriesForFrontEndTask)
    {
        parent::__construct();
        $this->getAllCategoriesForFrontEndTask = $getAllCategoriesForFrontEndTask;
    }

    public function run(
        $filters = [],
        $currentLang,
        $skipPagination = false,
        $limit = 20,
        $orderBy = 'category.sort_order,category.category_id desc',
        $externalData = [],
        $currentPage = 1,
        $returnType = 'array'
    )
    {
        $orderBy = 'category.sort_order asc,category.category_id desc';
        return $this->remember(function () use ($filters, $currentLang, $limit, $orderBy, $skipPagination, $externalData, $currentPage, $returnType) {
            $filters = array_merge([
                'status' => CategoryStatus::ACTIVE,
                'cate_type' => CategoryType::COURSE
            ], $filters);
            $data = $this->getAllCategoriesForFrontEndTask->keyBy($this->keyBy);

            if (isset($filters['byParentId'])) {
                $data->byParentId($filters['byParentId']);
            }
            return $data->run($filters, $currentLang, $limit, $orderBy, $skipPagination, $externalData, $currentPage, $returnType);
        }, $this->customCacheKey, [5], 0, $this->skipCache);
    }

    public function keyBy(?string $keyBy): self
    {
        $this->keyBy = $keyBy;
        $this->customCacheKey = 'cccKeyBy';
        return $this;
    }
}
