<?php

namespace App\Containers\Category\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class GetAllProductsAction.
 *
 */
class GetAllFilterGroupAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $data = Apiato::call('Category@GetAllFilterGroupTask', [$data->id, $data->name ?? '', Apiato::call('Localization@GetDefaultLanguageTask')]);

        return $data;
    }
}
