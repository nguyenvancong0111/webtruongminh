<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 20:43:34
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-04 13:31:25
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\Actions;

use App\Containers\Category\Actions\SubActions\GetAllCategoriesForFrontEndSubAction;
use App\Containers\Category\Actions\SubActions\PresentFrontEndCategoriesSubAction;
use App\Containers\Category\Tasks\GetAllCategoriesForFrontEndTask;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetCategoryBySlugAction extends Action
{
    public $getAllCategoriesForFrontEndTask, $presentFrontEndCategoriesSubAction, $getAllCategoriesForFrontEndSubAction;

    public function __construct(
        GetAllCategoriesForFrontEndTask $getAllCategoriesForFrontEndTask,
        PresentFrontEndCategoriesSubAction $presentFrontEndCategoriesSubAction,
        GetAllCategoriesForFrontEndSubAction $getAllCategoriesForFrontEndSubAction
    )
    {
        parent::__construct();
        $this->getAllCategoriesForFrontEndTask = $getAllCategoriesForFrontEndTask;
        $this->presentFrontEndCategoriesSubAction = $presentFrontEndCategoriesSubAction;
        $this->getAllCategoriesForFrontEndSubAction = $getAllCategoriesForFrontEndSubAction;
    }

    public function run(int $categoryType, int $categoryId, string $slug = null, Language $currentLang = null, bool $withParent = false): ?array
    {
        return $this->remember(function () use ($categoryType, $categoryId, $slug, $currentLang, $withParent) {
            $pureCategories = $this->getAllCategoriesForFrontEndSubAction->run(
                ['cate_type' => $categoryType],
                $currentLang,
                true,
                'category.sort_order,category.created_at desc'
            );

            $treeCategories = $this->presentFrontEndCategoriesSubAction->run($pureCategories);

            $currentCategory = $this->returnCateBySlugId($treeCategories, $categoryId, $slug, $current);
            $parents = [];
            if ($withParent) {
                $this->findParents($pureCategories, $categoryId, $parents);
            }

            return [
                'current' => $currentCategory,
                'parents' => array_reverse($parents),
            ];
        }, null, [], 0, false);
    }

    private function returnCateBySlugId(array $elements, int $categoryId = 0, string $slug = null, &$branch = []): ?array
    {
        if (!empty($branch)) {
            return $branch;
        }

        foreach ($elements as $element) {
            if ($element['category_id'] == $categoryId && ((!empty($slug) && $element['desc']['slug'] == $slug) || empty($slug))) {
                $branch = $element;
            } elseif (!empty($element['sub'])) {
                $this->returnCateBySlugId($element['sub'], $categoryId, $slug, $branch);
            }
        }

        return $branch;
    }

    // Hàm này cần nghiên cứu lại :"))))~
    public function getParents(int $id, $pureArray, &$parents_id = [])
    {
        $breaking = false;

        if ($breaking) return $parents_id;

        foreach ($pureArray as $item) {
            if ($item['parent_id'] == 0) {
                $parents_id = [];
            }

            $parents_id[] = $item['category_id'];

            if ($item['category_id'] == $id) {
                $breaking = true;
            } elseif (!empty($item['sub'])) {
                $this->getParents($id, $item['sub'], $parents_id);
            }
        }

        return $parents_id;
    }

    public function findParents(array $input, int $id, array &$parents)
    {
        foreach ($input as $val) {
            if ($val['category_id'] == $id) {
                array_push($parents, $val);
                if (!empty($val['parent_id'])) {
                    $this->findParents($input, $val['parent_id'], $parents);
                }
                break;
            }
        }
    }
}
