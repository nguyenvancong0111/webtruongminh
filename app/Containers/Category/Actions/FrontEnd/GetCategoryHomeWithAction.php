<?php

namespace App\Containers\Category\Actions\FrontEnd;


use App\Containers\Category\Tasks\GetAllCategoriesForFrontEndTask;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetCategoryHomeWithAction extends Action
{
    protected $keyBy = null;

    public $getAllCategoriesForFrontEndTask;

    public function __construct(GetAllCategoriesForFrontEndTask $getAllCategoriesForFrontEndTask)
    {
        parent::__construct();
        $this->getAllCategoriesForFrontEndTask = $getAllCategoriesForFrontEndTask;
    }
    public function run(array $filters = [], string $orderBy = 'category.sort_order asc,category.category_id desc', int $limit = 4, bool $skipPagination = false, ?Language $currentLang = null, array $externalData = [], int $currentPage = 1): ?array
    {
        return $this->remember(function () use ($filters, $orderBy, $limit, $skipPagination, $currentLang, $externalData, $currentPage) {
            $data = $this->getAllCategoriesForFrontEndTask->keyBy($this->keyBy);

            return $data->run($filters, $currentLang, $limit, $orderBy, $skipPagination, $externalData, $currentPage);
        }, null, [], 0, $this->skipCache);
    }
    public function keyBy(?string $keyBy): self
    {
        $this->keyBy = $keyBy;
        $this->customCacheKey = 'cccKeyBy';
        return $this;
    }
}
