<?php

namespace App\Containers\Category\Actions\FrontEnd;

use App\Containers\Banner\Actions\SubActions\GetAvailableBannerByPositionSubAction;
use App\Containers\Category\Actions\SubActions\GetAllCategoriesForFrontEndSubAction;
use App\Containers\Category\Enums\CategoryStatus;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetAllCategoryAction extends Action
{
    protected $getAvailableBannerByPositionSubAction;
    public $getAllCategoriesForFrontEndSubAction;
    protected $currentLang;
    protected $isMobile;
    protected $limit = 20;
    protected $skipPagination = false;

    public function __construct(
        GetAvailableBannerByPositionSubAction $getAvailableBannerByPositionSubAction,
        GetAllCategoriesForFrontEndSubAction $getAllCategoriesForFrontEndSubAction
    )
    {
        $this->getAllCategoriesForFrontEndSubAction = $getAllCategoriesForFrontEndSubAction;
        $this->getAvailableBannerByPositionSubAction = $getAvailableBannerByPositionSubAction;
        parent::__construct();
    }

    public function run(?Language $currentLang = null,int $parentId = 0, bool $isMobile = false, $withData = [])
    {
        $this->currentLang = $currentLang;
        $this->isMobile = $isMobile;
        return $this->remember(function () use ($parentId, $withData) {
            return $this->getAllCategoriesForFrontEndSubAction->keyBy('category_id')->run(
                [
                    'cate_type' => CategoryType::NEWS,
                    'byParentId' => $parentId,
                ],
                $this->currentLang,
                $this->skipPagination,
                $this->limit,
                'category.sort_order ASC,category.category_id DESC',
                $withData,
                1,
                'object'
            );
        }, null, [], 0, $this->skipCache);
    }

    public function setLimit($limit = 20)
    {
        $this->limit = $limit;

        return $this;
    }

    public function skipPagination()
    {
        $this->skipPagination = true;

        return $this;
    }

}
