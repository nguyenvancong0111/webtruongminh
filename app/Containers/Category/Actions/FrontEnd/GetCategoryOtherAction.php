<?php

namespace App\Containers\Category\Actions\FrontEnd;

use App\Containers\Category\Actions\SubActions\GetAllCategoriesForFrontEndSubAction;
use App\Containers\Category\Enums\CategoryStatus;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetCategoryOtherAction extends Action
{
    public $getAllCategoriesForFrontEndSubAction;

    public function __construct(
        GetAllCategoriesForFrontEndSubAction $getAllCategoriesForFrontEndSubAction
    ) {
        parent::__construct();
        $this->getAllCategoriesForFrontEndSubAction = $getAllCategoriesForFrontEndSubAction;
    }

    public function run(array $filters = [], ?Language $currentLang = null, int $limit = 10): ?array
    {
        $data = $this->getAllCategoriesForFrontEndSubAction->run(
            $filters,
            $currentLang,
            true,
            $limit,
            'category.sort_order,category.category_id desc',
            [],
            1
        );

        return $data;
    }
}
