<?php

namespace App\Containers\Category\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetCategoriesWithNewsAction extends Action
{
    public function run(?Language $currentLang = null, int $limitCategory = 3, int $limitNews = 12)
    {
        $newsCategoryWithNews = app(GetAllCategoryAction::class)
            ->setLimit($limitCategory)
            ->skipCache()
            ->run($currentLang, 0, false, []);

        return $newsCategoryWithNews->map(function ($item) use ($currentLang, $limitNews) {
            return $item->setRelation('news', $item->withNewsLimit($currentLang, $limitNews, ['*']));
        });
    }
}
