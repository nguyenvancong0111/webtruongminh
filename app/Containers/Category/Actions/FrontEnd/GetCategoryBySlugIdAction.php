<?php

namespace App\Containers\Category\Actions\FrontEnd;

use App\Containers\Category\Actions\SubActions\GetAllCategoriesForFrontEndSubAction;
use App\Containers\Category\Actions\SubActions\PresentFrontEndCategoriesSubAction;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetCategoryBySlugIdAction extends Action
{
    public function run(int $categoryType, int $categoryId, $categorySlug, Language $currentLang = null)
    {
        return $this::call('Category@GetCategoryBySlugIdTask', [$categoryType, $categoryId, $categorySlug, $currentLang]);
    }

}
