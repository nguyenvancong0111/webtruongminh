<?php

namespace App\Containers\Category\Actions\FrontEnd;

use App\Ship\Parents\Actions\Action;

class GetAllCateChillByIDAction extends Action
{
    public function run(array $parrent_id = [], bool $collect = false, array $output = [])
    {
        return $this->remember(function () use ( $parrent_id, $collect, $output) {
            return $this->call('Category@GetAllCateChildsTask', [ $parrent_id, $collect, $output]);
        }, null, [], 0, $this->skipCache);
    }

}
