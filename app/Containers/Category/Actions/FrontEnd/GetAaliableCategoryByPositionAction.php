<?php

namespace App\Containers\Category\Actions\FrontEnd;

use App\Ship\Parents\Actions\Action;
use App\Containers\Localization\Models\Language;
use Illuminate\Database\Eloquent\Collection;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAaliableCategoryByPositionAction extends Action
{
    public function run(array $positions = [], bool $isMobile, array $with = [], array $orderBy = [], $selectFields = ['*'],int $limit = 9 ,Language $currentLang = null,array $withNew = [],array $where = [],array $whereNew = [])
    {
        return Apiato::call('Category@GetAvailableCategoryByPositionTask', [$positions,$limit,$currentLang,$withNew,$where,$whereNew],
            [
                ['orderBy' => [$orderBy]],
                ['with' => [$with]],
                ['mobile' => [$isMobile]],
                ['selectFields' => [$selectFields],$currentLang]
            ]
        );
    }
}
