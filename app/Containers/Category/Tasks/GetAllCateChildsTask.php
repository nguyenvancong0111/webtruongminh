<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-30 09:45:26
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\Tasks;

use App\Containers\Category\Data\Repositories\CategoryRepository;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Ship\Criterias\Eloquent\ThisInThatCriteria;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Arr;

class GetAllCateChildsTask extends Task
{

    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($cate_ids, $collect, &$output)
    {
        try {
            if($collect){
                return $this->getAllChildsCollect($cate_ids,$output);
            }else{
                return $this->getAllChilds($cate_ids,$output);
            }
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    public function getAllChildsCollect($cate_ids = [], &$output = [])
    {
        $i = 0;
        $this->repository->pushCriteria(new SelectFieldsCriteria(['category_id','parent_id', 'type']));
        $this->repository->pushCriteria(new ThisInThatCriteria('parent_id', $cate_ids));

        $result = $this->repository->with('desc')->all();

//        if (!empty($result) && !$result->IsEmpty()) {
//            $has_pid = [];
//            foreach ($result as $item) {
//                if ($item->parent_id != 0) {
//                    $has_pid[] = $item->parent_id;
//                }
//                $output[] = $item->category_id;
//            }
//            if (!empty($has_pid)) {
//                $this->getCateIds($has_pid, $output, $i);
//            }
//        }
        return $result;
    }

    public function getAllChilds($cate_ids = [], &$output = [])
    {
        $i = 0;
        $this->repository->pushCriteria(new SelectFieldsCriteria(['category_id','parent_id']));
        $this->repository->pushCriteria(new ThisInThatCriteria('parent_id', $cate_ids));

        $result = $this->repository->all();

        if (!empty($result) && !$result->IsEmpty()) {
            $has_pid = [];
            foreach ($result as $item) {
                if ($item->parent_id != 0) {
                    $has_pid[] = $item->parent_id;
                }
                $output[] = $item->category_id;
            }
            if (!empty($has_pid)) {
                $this->getCateIds($has_pid, $output, $i);
            }
        }
        return $output;
    }

    public function getCateIds($cate_ids = [], &$output = [], &$i)
    {
        // $this->repository->pushCriteria(new SelectFieldsCriteria(['category_id', 'parent_id']));
        // $this->repository->pushCriteria(new ThisInThatCriteria('parent_id', $output));
        $result =  $this->repository->getModel()->select('category_id','parent_id')->whereIn('parent_id', $output)->get();
        // $result = $this->repository->all();
        $i++;
        if (!empty($result)&& !$result->IsEmpty() && $i < 3) {
            foreach ($result as $item) {
                $output[] = $item->category_id;
                $this->getCateIds([$item->category_id], $output, $i);
            }
        }
    }
}
