<?php

namespace App\Containers\Category\Tasks;

use App\Containers\Category\Data\Repositories\CategoryRepository;
use App\Containers\Category\Models\CategoryDesc;
use App\Containers\Category\Models\CategoryPath;
use App\Containers\Localization\Models\Language;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Models\ProductCategory;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class GetCategoryByIdTask.
 */
class GetCategoryBySlugIdTask extends Task
{

    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return  mixed
     */
    public function run(int $categoryType, int $categoryId, $categorySlug, Language $currentLang = null)
    {
        $language_id = $currentLang ? $currentLang->language_id : 1;

        $data = $this->repository->where('status','=', 2)->with([
            'desc' => function($query) use($language_id) {
                $query->activeLang($language_id);
            }]);
        $data->whereHas('desc', function (Builder $query) use ($categorySlug) {
            $query->where('slug', '=', $categorySlug);
        });

        return $data->where('category_id',$categoryId)->where('type', $categoryType)->first();
    }
}