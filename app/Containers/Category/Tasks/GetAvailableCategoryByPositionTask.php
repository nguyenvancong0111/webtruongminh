<?php

namespace App\Containers\Category\Tasks;

use App\Containers\Category\Data\Repositories\CategoryRepository;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Tasks\Task;

class GetAvailableCategoryByPositionTask extends Task
{
    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $positions = [], int $limit = 9, Language $currentLang = null,array $withNew = [],array $where = [],array $whereNew = [])
    {
        $language_id = $currentLang->language_id ? $currentLang->language_id : 1;

        if(!empty($where)){
            foreach($where as $column => $field){
                $this->repository->where($column,$field);
            }
        }

        if(!empty($withNew)){
            $this->repository->with([
                'news' => function ($query) use ($limit, $language_id,$whereNew) {
                    $query->where('status', 2)->orderBy('sort_order', 'asc')->orderby('created_at','desc');
                    if(!empty($whereNew)){
                        foreach($whereNew as $column => $field){
                            $query->where($column,$field);
                        }
                    }

                },
                'news.desc' => function ($query) use ($language_id) {
                    $query->activeLang($language_id);
                }
            ]);
        }

        $data = $this->repository->with([
                'desc' => function ($query) use ($language_id) {
                    $query->where('language_id', $language_id);
                },
            ]
        )->available($positions)->get();

        return $data;
    }
}
