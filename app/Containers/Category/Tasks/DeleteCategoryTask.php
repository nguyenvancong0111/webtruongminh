<?php

namespace App\Containers\Category\Tasks;

use App\Containers\Category\Data\Repositories\CategoryRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class DeleteCategoryTask extends Task
{

    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $id)
    {
        try {

            $dataUpdate = ['status' => -1];
            $cate = $this->repository->update($dataUpdate, $id);

            return $cate;

        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
