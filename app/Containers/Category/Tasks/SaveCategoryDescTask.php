<?php

namespace App\Containers\Category\Tasks;

use Apiato\Core\Foundation\Facades\StringLib;
use Apiato\Core\Foundation\ImageURL;
use App\Containers\Category\Data\Repositories\CategoryDescRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use App\Ship\Parents\Requests\Request;

/**
 * Class SaveCategoryDescTask.
 */
class SaveCategoryDescTask extends Task
{

    protected $repository;

    public function __construct(CategoryDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($data, $original_desc, $category_id, $edit_id = null, Request $request)
    {
        $description = isset($data['category_description']) ? (array)$data['category_description'] : null;

        if (!empty($description) && is_array($description)) {

            $updates = [];
            $inserts = [];

            $id_after_insert = 0;

            foreach ($description as $k => $v) {
                $attrItem = [];

                if (!empty($v['item'])) {
                    $count = count(array_filter($v['item']['item_title']));

                    for ($i = 0; $i < $count; $i++) {

                        if (!empty($v['item']['item_image'][$i])) {

                            $request->request->add(['fileImage' => $v['item']['item_image'][$i]]);
                            $upload_image = $this->imageFile($request, 'fileImage', $v['item']['item_image'][$i], 'category');

                        } else {
                            $upload_image['fileName'] = !empty($v['item']['check_item_image'][$i]) ? $v['item']['check_item_image'][$i] : null;
                        }

                        $attrItem[] = [
                            'item_image' => $upload_image['fileName'],
                            'item_title' => $v['item']['item_title'][$i],
                            'item_iframe' => $v['item']['item_iframe'][$i],
                        ];

                    }
                }

                $arr_data = [
                    'name' => $v['name'],
                    'slug' => isset($v['slug']) && $v['slug'] != '' ? StringLib::slug($v['slug']) : ( $k == 3 ? StringLib::slug($v['name'], '-', 'cn') : StringLib::slug($v['name'])),
                    'description' => $v['description'],
                    'meta_title' => $v['meta_title'],
                    'meta_description' => $v['meta_description'],
                    'meta_keyword' => $v['meta_keyword'],
                    'item' => json_encode($attrItem),
                ];


                if (isset($original_desc[$k])) {
                    $updates['data'][$original_desc[$k]['category_id'] . '_' . $k] = $arr_data;

                    $updates['conds'][$original_desc[$k]['category_id'] . '_' . $k] = ['category_id' => $original_desc[$k]['category_id'], 'language_id' => $k];
                } else {
                    if (null !== $edit_id) {
                        $inserts[] = array_merge(['language_id' => $k, 'category_id' => $edit_id], $arr_data);
                    } else {
                        if ($id_after_insert == 0) {
                            // $id_after_insert = $this->categoryDesc::insertGetId(array_merge(['language_id' => $k],$arr_data));
                            $inserts[] = array_merge(['language_id' => $k, 'category_id' => $category_id], $arr_data);
                        } else {
                            $inserts[] = array_merge(['language_id' => $k, 'category_id' => $category_id], $arr_data);
                        }
                    }
                }
            }

            if (!empty($updates)) {
                $this->repository->updateMultipleConditions($updates);
            }
            if (!empty($inserts)) {
                $this->repository->getModel()->insert($inserts);
            }
        }
    }

    public function imageFile(Request $request, string $fileField = 'file', string $prefix = '', $folder_upload = 'pagefile')
    {
        $errorMsg = null;
        $fname = null;
//        if ($request->hasFile($fileField)) {

        $file = $request->$fileField;

        $allowedFileExtension = ['jpeg', 'jpg', 'png', 'gif'];
        $extension = $file->getClientOriginalExtension();

        if (in_array($extension, $allowedFileExtension) && $file->isValid()) {
            $fname = ImageURL::makeFileName(!empty($prefix) ? $prefix : $file->getClientOriginalName(), $extension);

            $storeResult = $file->storeAs($folder_upload, $fname, 'upload');

            if (!$storeResult) {
                $errorMsg = 'Upload file lên server thất bại!';
            }
        } else {
            $errorMsg = 'Upload file lên server sai định dạng!';
        }
//        }

        return ['error' => !empty($errorMsg), 'msg' => @$errorMsg ?: 'Success', 'fileName' => $fname];
    }
}
