<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 23:26:46
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-08 23:31:49
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\Tasks\Admin;

use App\Containers\Category\Data\Repositories\CategoryRepository;
use App\Ship\core\Traits\HelpersTraits\UpdateDynamicStatusTrait;
use App\Ship\Parents\Tasks\Task;

class UpdateSomeStatusTask extends Task
{
    use UpdateDynamicStatusTrait;
    
    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(string $field,int $id, int $status,string $id_alias_field = '') :? bool
    {
        return $this->updateDynamicStatus($field,$id,$status,$id_alias_field);
    }
}
