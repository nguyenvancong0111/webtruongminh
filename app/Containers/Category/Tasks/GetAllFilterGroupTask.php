<?php

namespace App\Containers\Category\Tasks;

use App\Containers\Category\Data\Repositories\CategoryPathRepository;
use App\Containers\Category\Models\CategoryFilterGroup;
use App\Containers\Filter\Models\FilterGroup;
use App\Containers\Filter\Models\FilterGroupDesc;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class GetAllFilterGroupTask.
 */
class GetAllFilterGroupTask extends Task
{

    protected $repository;

    public function __construct(CategoryPathRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($categoryIds = 0, $name = '', $defaultLanguage = null): ?Collection
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;
        $wery = DB::table(FilterGroup::getTableName(), 'f_g');
        $wery->selectRaw('f_g.filter_group_id,c_f.category_id,f_g_d.name,f_g_d.short_description');
        $wery->join(CategoryFilterGroup::getTableName() . ' as c_f', 'f_g.filter_group_id', '=', 'c_f.filter_group_id');
        $wery->leftJoin(FilterGroupDesc::getTableName() . ' as f_g_d', 'f_g_d.filter_group_id', '=', 'f_g.filter_group_id');

        if (is_array($categoryIds)) {
            $wery->whereIn('c_f.category_id', $categoryIds);
        } elseif ($categoryIds > 0) {
            $wery->where('c_f.category_id', $categoryIds);
        }

        if ($name != '') {
            $wery->where('f_g_d.name', 'LIKE', '%' . $name . '%');
        }
        $wery->where('f_g_d.language_id', $language_id);

        return $wery->get();
    }
}
