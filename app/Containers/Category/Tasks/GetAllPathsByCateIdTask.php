<?php

namespace App\Containers\Category\Tasks;

use App\Containers\Category\Data\Repositories\CategoryPathRepository;
use App\Ship\Criterias\Eloquent\OrderByFieldCriteria;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class GetAllPathsByCateIdTask.
 */
class GetAllPathsByCateIdTask extends Task
{

    protected $repository;

    public function __construct(CategoryPathRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($category_id)   {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('path_id',$category_id));
        $this->repository->pushCriteria(new OrderByFieldCriteria('level','asc'));

        $wery = $this->repository->all();

        return $wery;
    }
}
