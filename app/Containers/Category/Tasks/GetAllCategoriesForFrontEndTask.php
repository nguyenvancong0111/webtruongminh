<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-30 01:47:02
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-05 23:43:37
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\Tasks;

use App\Containers\Category\Data\Repositories\CategoryRepository;
use App\Containers\Category\Enums\CategoryStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;

use Illuminate\Database\Eloquent\Builder;

class GetAllCategoriesForFrontEndTask extends Task
{
    protected $keyBy = null;
    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(
        $filters = [],
        $defaultLanguage = null,
        $limit = 20,
        $orderBy = 'category.sort_order,category.category_id desc',
        $skipPagination = false,
        $externalData = [],
        $currentPage = 1,
        $returnType = 'array'
    )
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;

        if (isset($filters['id']) && $filters['id'] != '') {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('id', $filters['id']));
        }
        if (isset($filters['status']) && $filters['status'] != '') {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('category.status', $filters['status']));
        } else {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('category.status', CategoryStatus::ACTIVE));
        }

        if (isset($filters['cate_type'])) {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('category.type', $filters['cate_type']));
        }

        if (isset($filters['hot'])) {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('category.hot', CategoryStatus::HOT));
        }
        if (isset($filters['is_home'])) {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('category.is_home', $filters['is_home']));
        }

        if (isset($filters['show_freeship_page'])) {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('category.show_freeship_page', CategoryStatus::SHOW_FREESHIP_PAGE));
        }

        if (isset($filters['show_sale_page'])) {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('category.show_sale_page', CategoryStatus::SHOW_PROMOTION_PAGE));
        }

        $this->with(array_merge(['desc' => function ($query) use ($language_id) {
            $query->select('category_id', 'language_id', 'name', 'slug', 'description', 'meta_title', 'meta_description', 'meta_keyword');
            $query->activeLang($language_id);
        }], isset($externalData['with_relationship']) ? $externalData['with_relationship'] : []));

        if(isset($filters['count_course']) && $filters['count_course']){
            $this->repository->withCount(['course' =>function($q){
                $q->whereHas('count_item', function (Builder $query) {
                    $query->whereNotNull('course_id');
                });
            }]);
        }
        if (isset($filters['name']) && !empty($filters['name'])) {
            $this->repository->whereHas('desc', function (Builder $query) use ($filters) {
                $query->where('category_description.name', 'like', '%' . $filters['name'] . '%');
            });
        }

        if (isset($filters['slug']) && !empty($filters['slug'])) {
            $this->repository->whereHas('desc', function (Builder $query) use ($filters) {
                $query->where('category_description.slug', '=', $filters['slug']);
            });
        }

        if (isset($filters['cate_ids']) && is_array($filters['cate_ids']) && !empty($filters['cate_ids'])) {
            $this->repository->whereIn('category.category_id', $filters['cate_ids']);
        } elseif (isset($filters['cate_ids']) && !empty($filters['cate_ids'])) {
            $this->repository->where('category.category_id', $filters['cate_ids']);
        }
        if (isset($filters['reject']) && is_array($filters['reject']) && !empty($filters['reject'])){
            $this->repository->whereNotIn('category.category_id', $filters['reject']);
        }elseif (isset($filters['reject']) && !empty($filters['reject'])){
            $this->repository->where('category.category_id', '!=', $filters['reject']);
        }
        if (isset($filters['not_parent']) && $filters['not_parent']){
            $this->repository->where('category.parent_id', '!=', 0);
        }
        if (isset($filters['not_cate'])) {
            $this->repository->pushCriteria(new ThisOperationThatCriteria('category.category_id', $filters['not_cate'], '!='));
        }

        $orderBy ? $this->repository->orderByRaw($orderBy) : null;

        // dd($this->repository->toSql(), $filters);
        $this->repository = $limit === 1 ? $this->repository->first() : ($skipPagination ? $this->repository->limit($limit) : $this->repository->paginate($limit));

        switch ($returnType) {
            case 'object':
                return $this->keyBy !== null ? $this->repository->keyBy($this->keyBy) : $this->repository;
            case 'array':
            default:
                return $this->keyBy !== null ? $this->repository->keyBy($this->keyBy)->toArray() : $this->repository->toArray();
        }
    }

    /**
     * Chỉ lấy danh mục gốc có parent_id = 9;
     */
    public function byParentId(int $parentId = 0): self
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('parent_id', $parentId));
        return $this;
    }

    public function keyBy(?string $keyBy): self
    {
        $this->keyBy = $keyBy;
        return $this;
    }
}
