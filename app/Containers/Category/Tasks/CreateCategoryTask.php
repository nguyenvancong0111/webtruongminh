<?php

namespace App\Containers\Category\Tasks;

use App\Containers\Category\Data\Repositories\CategoryRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Arr;

class CreateCategoryTask extends Task
{

    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data, ['category_description', 'category_filter_group','_token', '_headers']);
            if(isset($dataUpdate['position']))
            $dataUpdate['position'] = is_array($data['position']) ? implode(',', $data['position']) : $data['position'];

            $cate = $this->repository->create($dataUpdate);
            return $cate;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
