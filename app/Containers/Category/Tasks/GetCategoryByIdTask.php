<?php

namespace App\Containers\Category\Tasks;

use App\Containers\Category\Data\Repositories\CategoryRepository;
use App\Containers\Category\Models\CategoryDesc;
use App\Containers\Category\Models\CategoryPath;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Models\ProductCategory;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class GetCategoryByIdTask.
 */
class GetCategoryByIdTask extends Task
{

    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return  mixed
     */
    public function run($category_id, $defaultLanguage = null)
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;
        $wery = DB::table($this->repository->getModel()->getTable(), 'c')->distinct();
        $wery->select('*');
        $wery->addSelect([
            'path' => DB::table(CategoryPath::getTableName(), 'cp')
                ->selectRaw("GROUP_CONCAT( cd1.NAME ORDER BY LEVEL SEPARATOR '" . $this->repository->separator . "' ) ")
                ->leftJoin(CategoryDesc::getTableName() . ' as cd1', function ($q) {
                    $q->on('cp.path_id', '=', 'cd1.category_id');
                    $q->on('cp.category_id', '!=', 'cp.path_id');
                })->whereRaw('cp.category_id = c.category_id AND cd1.language_id = ' . $language_id)
                ->groupBy('cp.category_id')
        ]);
        $wery->leftJoin(CategoryDesc::getTableName() . ' as cd2', 'c.category_id', '=', 'cd2.category_id');

        $if_arr = is_array($category_id);
        if($if_arr) {
            $wery->whereIn('c.category_id', $category_id);
        }else {
            $wery->where('c.category_id', $category_id);
        }

        $wery->where('cd2.language_id', $language_id);

        return $if_arr ? $wery->get() : $wery->first();
    }
}