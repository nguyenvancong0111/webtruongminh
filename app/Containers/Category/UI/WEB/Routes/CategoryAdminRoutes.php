<?php
Route::group(
    [
        'prefix' => 'categories',
        'namespace' => '\App\Containers\Category\UI\WEB\Controllers\Admin',
        'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
        'middleware' => [
            'auth:admin',
        ],
    ],
    function () use ($router) {
        $router->get('/', [
            'as'   => 'admin_category_home_page',
            'uses' => 'Controller@index',
        ]);

        $router->get('/edit/{id}', [
            'as'   => 'admin_category_edit_page',
            'uses' => 'Controller@edit',
        ]);

        $router->post('/edit/{id}', [
            'as'   => 'admin_category_edit_page',
            'uses' => 'Controller@update',
        ]);

        $router->get('/add', [
            'as'   => 'admin_category_add_page',
            'uses' => 'Controller@add',
        ]);

        $router->post('/add', [
            'as'   => 'admin_category_add_page',
            'uses' => 'Controller@create',
        ]);

        $router->delete('/{id}', [
            'as' => 'admin_category_delete',
            'uses'       => 'Controller@deleteCategory',
        ]);

        $router->post('/status/{field}', [
            'as'   => 'admin_category_change_status',
            'uses' => 'Controller@updateSomeStatus',
        ]);
    }
);
