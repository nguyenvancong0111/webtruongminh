<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 23:15:50
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-08 23:30:52
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\UI\WEB\Controllers\Admin\Features;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Category\UI\WEB\Requests\Admin\UpdateSomeStatusRequest;

trait UpdateSomeStatus
{
    public function updateSomeStatus(UpdateSomeStatusRequest $request) {
        $result = Apiato::call('Category@Admin\UpdateSomeStatusAction', [
            $request->field,
            $request->id,
            $request->status,
            $request->id_alias_field,
        ]);

        if($result) {
            return $this->sendResponse($result,'Success');
        }

        return $this->sendError('Không update được dữ liệu!');
    }
}
