<?php

namespace App\Containers\Category\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\StringLib;
use App\Containers\Category\Models\Category;
use App\Containers\Category\UI\WEB\Requests\Admin\CreateCategoryRequest;
use App\Containers\Category\UI\WEB\Requests\Admin\FindCategoryRequest;
use App\Containers\Category\UI\WEB\Requests\Admin\GetAllCategoriesRequest;
use App\Containers\Category\UI\WEB\Requests\Admin\UpdateCategoryRequest;
use App\Containers\Category\UI\API\Transformers\CategoriesSelect2Transformer;
use App\Containers\Product\UI\WEB\Requests\AjaxGetProductRequest;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Category\UI\WEB\Controllers\Admin\Features\UpdateSomeStatus;
use App\Containers\Category\UI\WEB\Requests\Admin\DeleteCategoryRequest;

class Controller extends AdminController
{
    use UpdateSomeStatus;

    public function __construct()
    {
        $this->title = 'Danh mục';
        parent::__construct();
    }
    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllCategoriesRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title, 'admin_category_home_page']);
        // dd($request->page);
        $categories = Apiato::call('Category@Admin\GetAllCategoriesAction', [$request->all(), 30, false, $request->page]);

        return view('category::Admin.index', [
            'search_data' => $request,
            'data' => $categories->appends($request->except('page')),
            'type' => Category::$type,
        ]);
    }

    public function edit(FindCategoryRequest $request)
    {
        $this->showEditForm();

        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['edit', $this->title, 'admin_category_home_page']);

        $transporter = new DataTransporter($request);
        $data = Apiato::call('Category@GetCategoryByIdAction', [$transporter->id]);

        if ($data) {
            $data->all_desc = Apiato::call('Category@GetAllCategoryDescAction', [$transporter]);
            /*$data->filter_groups = Apiato::call('Category@GetAllFilterGroupAction', [$transporter]);*/

            return view('category::Admin.edit', [
                'data' => $data,
                'type' => Category::$type,
            ]);
        }

        return $this->notfound($request->id);
    }

    public function add(CreateCategoryRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['create', $this->title, 'admin_category_home_page']);

        return view('category::Admin.edit', [
            'type' => Category::$type,
        ]);
    }

    public function update(UpdateCategoryRequest $request)
    {
        try {
            $tranporter = $request->all();
            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'category', StringLib::getClassNameFromString(Category::class)]);
                if (!$image['error']) {
                    $tranporter['image'] = $image['fileName'];
                }
            }

            if (isset($request->icon)) {
                $icon = Apiato::call('File@UploadImageAction', [$request, 'icon', 'category', StringLib::getClassNameFromString(Category::class)]);
                if (!$icon['error']) {
                    $tranporter['icon'] = $icon['fileName'];
                }
            }

            if (isset($request->icon2)) {
                $icon2 = Apiato::call('File@UploadImageAction', [$request, 'icon2', 'category', StringLib::getClassNameFromString(Category::class)]);
                if (!$icon2['error']) {
                    $tranporter['icon2'] = $icon2['fileName'];
                }
            }


            $cate = Apiato::call('Category@UpdateCategoryAction', [$tranporter,$request]);

            if ($cate) {
                return redirect()->route('admin_category_edit_page', ['id' => $cate->category_id])->with('status', 'Danh mục đã được cập nhật');
            }
        } catch (\Exception $e) {
            throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function create(CreateCategoryRequest $request)
    {
        try {
            $tranporter = $request->all();
            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'category', StringLib::getClassNameFromString(Category::class)]);
                if (!$image['error']) {
                    $tranporter['image'] = $image['fileName'];
                }
            }

            if (isset($request->icon)) {
                $icon = Apiato::call('File@UploadImageAction', [$request, 'icon', 'category', StringLib::getClassNameFromString(Category::class)]);
                if (!$icon['error']) {
                    $tranporter['icon'] = $icon['fileName'];
                }
            }

            if (isset($request->icon2)) {
                $icon2 = Apiato::call('File@UploadImageAction', [$request, 'icon2', 'category', StringLib::getClassNameFromString(Category::class)]);
                if (!$icon2['error']) {
                    $tranporter['icon2'] = $icon2['fileName'];
                }
            }
            $cate = Apiato::call('Category@CreateCategoryAction', [$tranporter,$request]);

            if ($cate) {
                return redirect()->route('admin_category_home_page', ['id' => $cate->category_id])->with('status', 'Danh mục đã được Thêm mới');
            }
        } catch (\Exception $e) {
//             throw $e;
//            $this->throwExceptionViaMess($e);
            return redirect()->route('admin_category_add_page')->withErrors(['errors' => 'Có lỗi xảy ra. Vui lòng thử lại']);
        }
    }

    public function deleteCategory(DeleteCategoryRequest $request)
    {
        try {
            Apiato::call('Category@Admin\DeleteCategoryAction', [$request->id]);
        } catch (\Exception $e) {
            throw $e;
            // $this->throwExceptionViaMess($e);
        }
    }

    public function ajaxCateDataByID(AjaxGetProductRequest $request){
        $transporter = $request->toTransporter();
        if ($transporter->product_id > 0) {
         $cate = Apiato::call('Category@GetCategoryByIdAction', [Apiato::call('Product@CateIdsDirectFromPrdIdAction', [$transporter->product_id])]);
         $cateHis = [];
         foreach($cate as $key => $item){
          $cateHis[$key] = $item->category_id;
         }
         if(!empty($cate)){
              return FunctionLib::ajaxRespond(true, 'Hoàn thành', $cateHis);
          }
          return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
        }
      }

    public function getAllCategories(GetAllCategoriesRequest $request)
    {
        $data = [
            'name'=> $request->queryString,
            'type' => 'select2',
            'cate_type'=> isset($request->cate_type) && !empty($request->cate_type) ? (int) $request->cate_type : 1
        ];
        $categories = Apiato::call('Category@Admin\GetAllCategoriesAction',[$data]);
        $type = 'select2';
        if($type && $type == 'select2') {
            return $this->transform($categories, CategoriesSelect2Transformer::class, [], [], 'category');
        }
    }
}
