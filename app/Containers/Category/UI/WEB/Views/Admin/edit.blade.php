@extends('basecontainer::admin.layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if (isset($editMode) && $editMode)
                {!! Form::open(['url' => route('admin_category_edit_page', $data->category_id), 'files' => true]) !!}
            @else
                {!! Form::open(['url' => route('admin_category_add_page'), 'files' => true]) !!}
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            @if (isset($editMode) && $editMode)
                {{-- <div class="mb-3">
                <a target="_blank" href="{{ route($data->type == 1 ? 'list_one_category' : 'news.cate',['cate_slug' => $data->name ? \Illuminate\Support\Str::slug($data->name) : 'xxx','cate_id'=> $data->category_id]) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> Xem</a>
            </div> --}}
            @endif
            <div class="card card-accent-primary">
                <div class="card-header">
                    <i class="fa fa-pencil-square-o"></i> THÔNG TIN
                </div>
                <div class="card-body">

                    <div class="tabbable boxed parentTabs">
                        <ul class="nav nav-tabs nav-underline nav-underline-primary mb-3">
                            <li class="nav-item">
                                <a class="nav-link active" href="#general"><i class="fa fa-empire"></i> Chung</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#data"><i class="fa fa-deviantart"></i> Thông tin chi tiết</a>
                            </li>

{{--                            <li class="nav-item list-item">--}}
{{--                                <a class="nav-link" href="#video"><i class="fa fa-deviantart"></i> Video</a>--}}
{{--                            </li>--}}

{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link" href="#image"><i class="fa fa-image"></i> Hình ảnh</a>--}}
{{--                            </li>--}}
                        </ul>

                        <div class="tab-content p-0">
                            @include('category::Admin.edit_tabs.general')

                            @include('category::Admin.edit_tabs.data')

                            @include('category::Admin.edit_tabs.item')

                            @include('category::Admin.edit_tabs.image')
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb-3">
                <button type="submit" name="submit" value="update" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Cập nhật</button>
                &nbsp;&nbsp;
                {{-- <button type="button" onclick="update_to_add();" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Cập nhật & Thêm mới</button>
                &nbsp;&nbsp; --}}
                <a class="btn btn-sm btn-danger" href="{{ route('admin_category_home_page') }}"><i class="fa fa-ban"></i>
                    Hủy bỏ</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('css')

@stop
@section('js_bot')
    {!! \FunctionLib::addMedia('admin/js/library/tinymce/tinymce.min.js') !!}
    <script type="text/javascript">
        $("ul.nav-tabs a").click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });

        shop.getCat = function(type, lang, def) {
            var html = '<option value="0">-- Chọn --</option>';
            shop.ajax_popup('category/get-cat', 'POST', {
                type: type,
                lang: lang
            }, function(json) {
                $.each(json.data, function(ind, value) {
                    html += '<option value="' + value.id + '"' + (def == value.id ? ' selected' : '') +
                        '>' + value.title + '</option>';
                    if (value.sub.length != 0) {
                        $.each(value.sub, function(k, sub) {
                            html += '<option value="' + sub.id + '"' + (def == sub.id ?
                                    ' selected' : '') + '> &nbsp;&nbsp;&nbsp; ' + sub.title +
                                '</option>';
                        });
                    }
                });
                $('#pid').html(html);
            });
        };

    </script>
@stop
