<div class="tab-pane" id="image">
    <div class="tabbable">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="image">Ảnh đại diện</label>
                    <input type="file" id="image" name="image" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}">
                    <div class="mt-2">
                        @if(!empty(@$data->image))
                            <div class="pull-right">
                                <img src="{{ \ImageURL::getImageUrl($data->image, 'category', 'avatar')}}" />
                            </div>
                        @endif
                    </div>
                </div>
            </div>
           <div class="col-sm-4">
               <div class="form-group">
                   <label for="icon">Icon</label>
                   <input type="file" id="icon" name="icon" class="form-control{{ $errors->has('icon') ? ' is-invalid' : '' }}">
                   <div class="mt-2">
                       @if(!empty(@$data->icon))
                           <div class="pull-right">
                               <img src="{{ \ImageURL::getImageUrl($data->icon, 'category', 'avatar')}}" />
                           </div>
                       @endif
                   </div>
               </div>
           </div>

{{--            <div class="col-sm-4">--}}
{{--                <div class="form-group">--}}
{{--                    <label for="icon2">Icon 2</label>--}}
{{--                    <input type="file" id="icon2" name="icon2" class="form-control{{ $errors->has('icon2') ? ' is-invalid' : '' }}">--}}
{{--                    <div class="mt-2">--}}
{{--                        @if(!empty(@$data->icon2))--}}
{{--                            <div class="pull-right">--}}
{{--                                <img src="{{ \ImageURL::getImageUrl($data->icon2, 'category', 'avatar')}}" />--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>
</div>