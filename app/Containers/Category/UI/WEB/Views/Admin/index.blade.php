@extends('basecontainer::admin.layouts.default')

@section('right-breads')
    {!! $data->appends($search_data->toArray())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5">
                <h1>{{ $site_title }}</h1>
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            {!! Form::open(['url' => route('admin_category_home_page'), 'method' => 'get', 'id' => 'searchForm']) !!}
            <div class="card card-accent-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                                class="fa fa-bookmark-o"></i></span></div>
                                <input type="text" name="name" class="form-control" placeholder="Tên danh mục"
                                       value="{{ $search_data->name }}">
                            </div>
                        </div>
                        <div class="form-group col-sm-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                                class="fa fa-navicon"></i></span></div>
                                <select id="type" name="cate_type" class="form-control">
                                    <option value="">-- Loại danh mục --</option>
                                    @foreach ($type as $k => $v)
                                        <option value="{{ $k }}"
                                                @if ($search_data->cate_type == $k) selected="selected" @endif>{{ $v }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                    <a href="{{ route('admin_category_home_page') }}" class="btn btn-sm btn-primary"><i
                                class="fa fa-refresh"></i> Reset</a>
                    @if($user->can('create-categories'))
                    <a href="{{ route('admin_category_add_page') }}" class="btn btn-sm btn-primary"><i
                                class="fa fa-plus"></i> Thêm mới</a>
                    @endif
                </div>
            </div>
            {!! Form::close() !!}

            <div class="card card-accent-primary">
                <div class="card-body p-0">
                    <table class="table table-bordered table-striped table-responsive-md">
                        <thead>
                        <tr>
                            <th width="55">ID</th>
                            <th>Tên danh mục</th>
                            <th width="55">Sort</th>
                            <th width="150">Loại danh mục</th>
                            <th width="55">Xem</th>
                            @if($user->can('update-categories'))
                            <th width="55">Sửa</th>
                            @endif
                            @if($user->can('delete-categories'))
                            <th width="55">Xóa</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <td>{{ $item->category_id }}</td>
                                <td>{!! $item->name !!}</td>
                                <td class="alert-secondary">{{ $item->sort_order ?? 0 }}</td>
                                <td>
                                    {{ \App\Containers\Category\Models\Category::$type[$item->type] }}
                                </td>
                                @php($linkCatByType = \App\Containers\Category\Models\Category::linkCatByType($item->type, $item->slug, $item->name, $item->category_id))
                                <td align="center">
                                    <a href="{{$linkCatByType}}" target="_blank" class="text-info" data-toggle="tooltip" data-placement="top" data-original-title="Xem danh mục"><i class="fa fa-chain"></i></a>
                                </td>
                                @if($user->can('update-categories'))
                                <td align="center">
                                    <a data-toggle="tooltip" data-original-title="Sửa"
                                       href="{{ route('admin_category_edit_page', $item->category_id) }}"
                                       class="text-primary"><i class="fa fa-pencil"></i></a></td>
                                @endif
                                @if($user->can('delete-categories'))
                                <td align="center">
                                    <a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Xóa"
                                       data-href="{{ route('admin_category_delete', $item->category_id) }}"
                                       class="text-danger" onclick="admin.delete_this(this)"><i
                                                class="fa fa-trash-o"></i></a>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    @include('basecontainer::admin.inc.bottom-pager-infor')
                </div>
            </div>
        </div>
        <!--/.col-->
    </div>
@endsection
