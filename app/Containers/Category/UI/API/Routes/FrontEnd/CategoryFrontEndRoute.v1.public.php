<?php

Route::group(
[
    'middleware' => [
         'api',
    ],
    'prefix' => '/category',
],
function () use ($router) {
    $router->any('/getAllShippingCategory', [
        'as' => 'api_list_category_shipping_page',
        'uses'       => 'FrontEnd\Controller@getAllShippingCategory'
    ]);

    $router->any('/getAllPromotionCategory', [
        'as' => 'api_list_category_promotion_page',
        'uses'       => 'FrontEnd\Controller@getAllPromotionCategory'
    ]);

    $router->any('/getAllHotCategory', [
        'as' => 'api_list_category_hot_page',
        'uses'       => 'FrontEnd\Controller@getAllHotCategory'
    ]);
    $router->any('/other-home', [
        'as' => 'api_list_category_other_home',
        'uses'       => 'FrontEnd\Controller@getCategoryOther'
    ]);

});