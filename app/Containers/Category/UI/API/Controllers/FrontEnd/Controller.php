<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-02 11:22:42
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-06 14:05:28
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\UI\API\Controllers\FrontEnd;

use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use App\Containers\Category\UI\API\Controllers\FrontEnd\Features\GetAllHotCategory;
use App\Containers\Category\UI\API\Controllers\FrontEnd\Features\GetAllPromotionCategory;
use App\Containers\Category\UI\API\Controllers\FrontEnd\Features\GetAllShippingCategory;
use App\Containers\Category\UI\API\Controllers\FrontEnd\Features\GetCategoryOther;

class Controller extends BaseApiFrontController
{
    use GetAllShippingCategory,
        GetAllPromotionCategory,
        GetAllHotCategory,
        GetCategoryOther;

    public function __construct()
    {
        parent::__construct();
    }
}
