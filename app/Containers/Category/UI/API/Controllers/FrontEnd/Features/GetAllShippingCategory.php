<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-02 11:25:23
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-03 22:26:12
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Category\Actions\FrontEnd\GetShippingPageCategoryAction;
use App\Containers\Category\UI\API\Requests\FrontEnd\GetAllCategoriesRequest;
use App\Containers\Category\UI\API\Transformers\FrontEnd\ShippingCategoryTransfomer;

trait GetAllShippingCategory
{
    public function getAllShippingCategory(GetAllCategoriesRequest $request)
    {
        $categories = app(GetShippingPageCategoryAction::class)->run($this->currentLang);

        return $this->transform($categories, new ShippingCategoryTransfomer(explode('_', $request->categoryId)), [], [], 'category');
    }
}
