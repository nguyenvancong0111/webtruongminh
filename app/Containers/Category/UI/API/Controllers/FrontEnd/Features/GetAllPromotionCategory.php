<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-02 11:25:23
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-02 16:07:41
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Category\Actions\FrontEnd\GetPromotionCategoryAction;
use App\Containers\Category\UI\API\Requests\FrontEnd\GetAllPromotionCategoryRequest;
use App\Containers\Category\UI\API\Transformers\FrontEnd\PromotionCategoryTransfomer;

trait GetAllPromotionCategory
{
    protected $data = [];
    public function getAllPromotionCategory(GetAllPromotionCategoryRequest $request)
    {
        $categories = app(GetPromotionCategoryAction::class)->skipCache()->run($this->currentLang,(int)$request->parentId,!empty($request->isIndex) ? (bool)$request->isIndex : true,$this->isMobile);

        // dd($categories);

        return $this->transform($categories, new PromotionCategoryTransfomer(explode('_', $request->categoryId)), [], [], 'category', null, false);
    }
}
