<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-02 11:25:23
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-06 14:05:14
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Category\Actions\FrontEnd\GetHotCategoryAction;
use App\Containers\Category\UI\API\Requests\FrontEnd\GetAllHotCategoryRequest;
use App\Containers\Category\UI\API\Transformers\FrontEnd\HotCategoryTransfomer;

trait GetAllHotCategory
{
    protected $data = [];
    public function getAllHotCategory(GetAllHotCategoryRequest $request)
    {
        $categories = app(GetHotCategoryAction::class)->skipCache()->run($this->currentLang);

        // dd($categories);

        return $this->transform($categories, new HotCategoryTransfomer(explode('_', $request->categoryId)), [], [], 'category', null, false);
    }
}
