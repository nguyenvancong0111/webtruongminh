<?php

namespace App\Containers\Category\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Category\Actions\FrontEnd\GetCategoryOtherAction;
use App\Containers\Category\UI\API\Requests\FrontEnd\GetAllHotCategoryRequest;
use App\Containers\Category\UI\API\Transformers\FrontEnd\OtherCategoryTransfomer;

trait GetCategoryOther
{
    protected $data = [];
    public function getCategoryOther(GetAllHotCategoryRequest $request)
    {
        $categories = app(GetCategoryOtherAction::class)->skipCache()->run(
            ['count_course' => true, 'not_parent' => 1, 'reject' => isset($request->reject) && !empty($request->reject) ? explode(',', $request->reject) : []], $this->currentLang, 6
        );

        return $this->transform($categories, new OtherCategoryTransfomer(explode('_', $request->categoryId)), [], [], 'category', null, false);
    }
}
