<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-02 11:43:56
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-20 17:37:05
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\UI\API\Transformers\FrontEnd;

use App\Containers\BaseContainer\UI\API\Transformers\FrontEnd\BaseCategoryTransformer;
use App\Containers\Category\UI\API\Transformers\FrontEnd\PromotionCategory\PromotionCateBannerTransfomer;

class PromotionCategoryTransfomer extends BaseCategoryTransformer
{
    public $selectedValues;

    protected $defaultIncludes = [
        'on_top_banners',
        'conner_banners'
    ];

    public function transform($category)
    {
        $response = parent::transform($category);

        $response['link'] = route('web_product_promotion_detail_cate',['slug' => $category['desc']['slug'],'id' => $category['category_id']]);

        return $response;
    }

    public function includeOnTopBanners($category)
    {
        return !empty($category['on_top_banners']) && !$category['on_top_banners']->IsEmpty() ? $this->collection($category['on_top_banners'], new PromotionCateBannerTransfomer(),'on_top_banners') : $this->null();
    }

    public function includeConnerBanners($category)
    {
        return !empty($category['conner_banners']) && !$category['conner_banners']->IsEmpty() ? $this->collection($category['conner_banners'], new PromotionCateBannerTransfomer('slide'),'conner_banners') : $this->null();
    }
}
