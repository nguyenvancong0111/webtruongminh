<?php

namespace App\Containers\Category\UI\API\Transformers;

use App\Ship\Parents\Transformers\Transformer;
use Illuminate\Support\Facades\Config;

/**
 * Class CategoriesSelect2Transformer.
 *
 */
class CategoriesSelect2Transformer extends Transformer
{

    /**
     * @param $token
     *
     * @return  array
     */
    public function transform($category)
    {
        $response = [
            'id' => $category->category_id,
            'text' => html_entity_decode($category->name),
            'description' => $category->description,
        ];

        return $response;
    }
}
