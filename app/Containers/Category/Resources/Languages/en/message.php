<?php 

return [
    'title' => 'FAQ',

    'id' => 'ID',
    'keyword' => 'Keyword',
    'faq_type' => 'FAQ\'s Type',
    'date_create' => 'Date Create',
    'display_status' => 'Display Status',
    'search' => 'Search',
    'reset' => 'Reset',

    'create' => 'Create FAQ',
    'update' => 'Update FAQ',

    'sort' => 'Sort',
    'question' => 'Question',
    'answer' => 'Answer',
    'action' => 'Action',
    'edit' => 'Edit FAQ',
    'delete' => 'Delete FAQ',
    'list' => 'FAQ List',
    'faq_manager' => 'FAQ Management',
    'show' => 'Show',
    'hidden' => 'Hidden',
    'owner' => 'Owner',
    'contractor' => 'Contractor',
    'no_data' => 'No Data',
    'choose_faq_type' => '---Choose FAQ\'s Type---',
    'choose_display_status' => '---Choose Display Status---',
    'keyword_input' => 'FAQ\'s keyword',
    'date_create_picker' => 'Date create',

    'total' => 'Total',
    'record' => 'record',
    'page' => 'page',

    'create_success' => 'Create FAQ successfully',
    'update_success' => 'Update FAQ successfully',
    'delete_success' => 'Delete FAQ successfully',
];