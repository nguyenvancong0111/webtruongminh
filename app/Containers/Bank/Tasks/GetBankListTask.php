<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 01:33:06
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-05 15:52:43
 * @ Description: Happy Coding!
 */

namespace App\Containers\bank\Tasks;

use App\Containers\Bank\Data\Repositories\BankRepository;
use App\Containers\Localization\Models\Language;
use App\Containers\News\Data\Repositories\NewsRepository;
use App\Containers\News\Enums\NewsStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class GetBankListTask extends Task
{

    protected $repository;

    public function __construct(BankRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run(
        array $filters = [],
        array $orderBy = ['created_at' => 'desc', 'id' => 'desc'],
        int $limit = 20,
        bool $skipPagination = false,
        Language $currentLang = null,
        array $externalData = [],
        int $currentPage = 1
    ): iterable {
        $language_id = $currentLang ? $currentLang->language_id : 1;
        if (isset($filters['status']) && $filters['status'] != '') {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('status', $filters['status']));
        }

        $this->repository->with(['desc' => function ($query) use ($language_id) {
            $query->select('id', 'bank_id', 'language_id', 'name', 'account', 'branch');
            $query->activeLang($language_id);
        }]);

        if (isset($filters['name']) && !empty($filters['name'])) {
            $this->repository->whereHas('desc', function (Builder $query) use ($filters) {
                $query->where('name', 'like', '%' . $filters['name'] . '%');
            });
        }

        foreach ($orderBy as $column => $direction) {
            $this->repository->orderBy($column, $direction);
        }

        // \DB::enableQueryLog();

        return $skipPagination ? $this->repository->limit($limit) : $this->repository->paginate($limit);
        // dd(\DB::getQueryLog());
    }
}
