<?php

namespace App\Containers\Bank\Tasks;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Bank\Data\Repositories\BankRepository;
use App\Containers\Bank\Enums\BankType;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Illuminate\Support\Arr;

/**
 * Class SaveBankTask.
 */
class SaveBankTask extends Task
{

    protected $repository;

    public function __construct(BankRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data, ['bank_description', '_token', '_headers']);
            $dataUpdate['is_mobile'] = isset($data['is_mobile']) ? BankType::MOBILE : BankType::DESKTOP;

            $bank = $this->repository->update($dataUpdate, $data['id']);
            return $bank;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}
