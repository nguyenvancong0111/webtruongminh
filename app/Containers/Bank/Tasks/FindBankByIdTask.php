<?php

namespace App\Containers\Bank\Tasks;

use App\Containers\Bank\Data\Repositories\BankRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class FindBankByIdTask.
 */
class FindBankByIdTask extends Task
{

    protected $repository;

    public function __construct(BankRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($bank_id, $defaultLanguage = 1, $external_data = ['with_relationship' => ['all_desc']])
    {

        $data = $this->repository->with(array_merge($external_data['with_relationship']))->find($bank_id);

        return $data;
    }
}