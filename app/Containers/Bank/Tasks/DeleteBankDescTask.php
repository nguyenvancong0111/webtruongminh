<?php

namespace App\Containers\Bank\Tasks;

use App\Containers\Bank\Data\Repositories\BankDescRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteBankDescTask.
 */
class DeleteBankDescTask extends Task
{

    protected $repository;

    public function __construct(BankDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($bank_id)
    {
        try {
            $this->repository->getModel()->where('bank_id', $bank_id)->delete();
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
