<?php

namespace App\Containers\Bank\Tasks;

use App\Containers\Bank\Data\Repositories\BankDescRepository;
use App\Containers\Bank\Data\Repositories\BankRepository;
use App\Containers\News\Data\Repositories\NewsDescRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class GetAllBankDescTask.
 */
class GetAllBankDescTask extends Task
{

    protected $repository;

    public function __construct(BankDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($bank_id)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('bank_id', $bank_id));
        $wery = $this->repository->all()->keyBy('language_id')->toArray();

        return $wery;
    }
}
