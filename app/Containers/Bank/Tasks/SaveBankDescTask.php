<?php

namespace App\Containers\Bank\Tasks;

use App\Containers\Bank\Data\Repositories\BankDescRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SaveBankDescTask.
 */
class SaveBankDescTask extends Task
{

    protected $repository;

    public function __construct(BankDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data, $original_desc, $bank_id, $edit_id = null)
    {
        $bank_description = isset($data['bank_description']) ? $data['bank_description'] : null;

        if (is_array($bank_description) && !empty($bank_description)) {
            $updates = [];
            $inserts = [];


            foreach ($bank_description as $k => $v) {
                if (isset($original_desc[$k])) {
                    $updates[$original_desc[$k]['id']] = [
                        'name' => $v['name'],
                        'account' => $v['account'],
                        'branch' => $v['branch']
                    ];
                } else {
                    $inserts[] = [
                        'bank_id' => $bank_id,
                        'language_id' => $k,
                        'name' => $v['name'],
                        'account' => $v['account'],
                        'branch' => $v['branch']
                    ];
                }

            }


            if (!empty($updates)) {
                $this->repository->updateMultiple($updates);
            }

            if (!empty($inserts)) {
                $this->repository->getModel()->insert($inserts);
            }
        }
    }
}
