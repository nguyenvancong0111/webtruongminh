<?php

namespace App\Containers\Bank\Tasks;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Bank\Data\Repositories\BankRepository;
use App\Containers\Bank\Enums\BankType;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class CreateBankTask.
 */
class CreateBankTask extends Task
{

    protected $repository;

    public function __construct(BankRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $data = Arr::except($data, ['bank_description', '_token', '_headers']);
            $data["publish_at"] = !empty($data['publish_at']) ? Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($data['publish_at'])) : Carbon::now();
            if(isset($data['position']))
            $data['position'] = is_array($data['position']) ? implode(',', $data['position']) : $data['position'];
            $data['is_mobile'] = isset($data['is_mobile']) ? BankType::MOBILE : BankType::DESKTOP;

            $bank = $this->repository->create($data);
            return $bank;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
