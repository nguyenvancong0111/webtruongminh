<?php

namespace App\Containers\Bank\Tasks;

use App\Containers\Bank\Data\Repositories\BankRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteBankTask.
 */
class EnableBankTask extends Task
{

    protected $repository;

    public function __construct(BankRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($bank_id)
    {
        try {
            $this->repository->getModel()->where('id', $bank_id)->update(
                [
                    'status' => 2
                ]
            );
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
