<?php

namespace App\Containers\Bank\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class BankDescRepository.
 */
class BankDescRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Bank';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'bank_id',
        'language_id',
        'name',
        'account',
        'branch',
    ];
}
