<?php

namespace App\Containers\Bank\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class BankRepository.
 */
class BankRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Bank';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'bank_number',
        'image',
        'status',
        'created_at',
        'updated_at',
    ];
}
