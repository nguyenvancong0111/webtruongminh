<?php

namespace App\Containers\Bank\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

/**
 * Class WithDescriptionCriteria.
 *
 *
 */
class WithDescriptionCriteria extends Criteria
{

    protected $language_id;

    public function __construct($language_id)
    {
        $this->language_id = $language_id;
    }

    /**
     * @param                                                   $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        $language_id = $this->language_id;

        return $model->with(['desc' => function ($query) use ($language_id){
            $query->select('id', 'bank_id', 'language_id', 'name', 'account', 'branch');
            $query->activeLang($language_id);
        }]);
    }
}
