<?php

namespace App\Containers\Bank\UI\WEB\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateBankRequest.
 *
 */
class CreateBankRequest extends Request
{

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles'       => 'admin',
        'permissions' => 'bank-manager',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
      return [
        'image' => 'bail|nullable|mimes:jpeg,jpg,png,gif',
      ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
      return [
        'image.mimes' => 'Ảnh không đúng định dạng (jpeg, jpg, png, gif)',
      ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
