<?php

namespace App\Containers\Bank\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Bank\Enums\BankType;
use App\Containers\Bank\Models\Bank;
use App\Containers\Bank\UI\WEB\Requests\Admin\FindBankRequest;
use App\Containers\Bank\UI\WEB\Requests\CreateBankRequest;
use App\Containers\Bank\UI\WEB\Requests\GetAllBankRequest;
use App\Containers\Bank\UI\WEB\Requests\UpdateBankRequest;
use App\Containers\Category\Actions\Admin\GetAllCategoriesAction;
use App\Ship\Parents\Controllers\AdminController;
use Exception;

class Controller extends AdminController
{
    public function __construct()
    {
        $this->title = 'Ngân hàng';

        parent::__construct();

        $categories = app(GetAllCategoriesAction::class)->run(['parent_id' => 0],1,true);

        view()->share('bankTypes', BankType::TEXT);

        view()->share('categories', $categories);
    }
    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllBankRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $banks = Apiato::call('Bank@BankListingAction', [$request, $this->perPage]);

        return view('bank::admin.index', [
            'search_data' => $request,
            'data' => $banks,
        ]);
    }

    public function edit(FindBankRequest $request)
    {
        $this->showEditForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['edit', $this->title, 'admin_bank_home_page']);

        try{
            $bank = Apiato::call('Bank@Admin\FindBankByIdAction', [$request->id]);

        }catch(Exception $e){
            return redirect()->route('admin_bank_home_page', ['id' => $request->id])->with('status', 'Có lỗi');
        }
// dd(config('bank-container.positions'));
        return view('bank::admin.edit', [
            'data' => $bank,
            'positions' => config('bank-container.positions')
        ]);

        return $this->notfound($request->id);
    }

    public function add()
    {
        $this->showAddForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['add', $this->title, 'admin_bank_home_page']);

        return view('bank::admin.edit', [
            'positions' => config('bank-container.positions')
        ]);
    }

    public function update(UpdateBankRequest $request)
    {
        try {

            $data =$request->except('image');

            if(isset($request['delete_image'])){
                $data['image'] = '' ;
            }

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'bank', StringLib::getClassNameFromString(Bank::class)]);
                if (!$image['error']) {
                    $data['image'] = $image['fileName'];
                }
            }

            $bank = Apiato::call('Bank@UpdateBankAction', [$data]);

            if ($bank) {
                return redirect()->route('admin_banks_edit_page', ['id' => $bank->id])->with('status', 'Cập nhật bank thành công');
            }
        } catch (\Exception $e) {
            $this->throwExceptionViaMess($e);
        }
    }

    public function create(CreateBankRequest $request)
    {
        try {
            $data =$request->except('image');

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'bank', StringLib::getClassNameFromString(Bank::class)]);
                if (!$image['error']) {
                    $data['image'] = $image['fileName'];
                }
            }

            $bank = Apiato::call('Bank@CreateBankAction', [$data]);
            if ($bank) {
                return redirect()->route('admin_bank_home_page')->with('status', 'Bank đã được thêm mới');
            }
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function delete(FindBankRequest $request)
    {
        try {
            Apiato::call('Bank@DeleteBankAction', [$request]);
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function enable(FindBankRequest $request){
        try {
            Apiato::call('Bank@EnableBankAction', [$request]);
            return FunctionLib::ajaxRespondV2(true,'Success');
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function disable(FindBankRequest $request){
        try {
            Apiato::call('Bank@DisableBankAction', [$request]);
            return FunctionLib::ajaxRespondV2(true,'Success');
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }
}
