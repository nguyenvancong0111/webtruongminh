<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-10 16:16:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-10 16:17:03
 * @ Description: Happy Coding!
 */

namespace App\Containers\Bank\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class BankStatus extends BaseEnum
{
}
