<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2020-10-04 13:58:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-10 16:23:34
 * @ Description:
 */

namespace App\Containers\Bank\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Containers\Bank\Enums\BankStatus;
use App\Ship\Parents\Models\Model;
use Carbon\Carbon;

class Bank extends Model
{
    protected $table = 'bank';

    protected $fillable = [
        'bank_number',
        'image',
        'status',
        'created_at',
        'updated_at',
        'sort_order'
    ];

    public function desc()
    {
        return $this->hasOne(BankDesc::class, 'bank_id', 'id');
    }

    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', BankDesc::class, 'bank_id', 'id');
    }

    public function scopeAvailable($query, array $positions = [])
    {
        return $query->where('status', BankStatus::ACTIVE)
            ->where(function ($query) use ($positions) {
                foreach ($positions as $position) {
                    $query->orWhereRaw("LOCATE('{$position}', position) > 0");
                }
            });
    }

    public function getImageUrl($size = 'large')
    {
        return ImageURL::getImageUrl($this->image, 'bank', $size);
    }

    public function getBankLink(): string
    {
        if (!empty($this->desc->link)) {
            return $this->desc->link;
        }

        return route('web_home_page');
    }
}
