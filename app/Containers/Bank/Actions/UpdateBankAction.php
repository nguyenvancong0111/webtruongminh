<?php

namespace App\Containers\Bank\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Bank\Models\Bank;
use App\Ship\Parents\Actions\Action;

class UpdateBankAction extends Action
{
    public function run($data)
    {
        $beforeData = Apiato::call('Bank@FindBankByIdTask', [$data['id']]);
        $bank = Apiato::call('Bank@SaveBankTask', [$data]);

        if ($bank) {
            $original_desc = Apiato::call('Bank@GetAllBankDescTask', [$bank->id]);
            Apiato::call('Bank@SaveBankDescTask', [
                $data,
                $original_desc,
                $bank->id
            ]);


            Apiato::call('User@CreateUserLogSubAction', [
                $bank->id,
                $beforeData->toArray(),
                $bank->toArray(),
                'Cập nhật Bank',
                Bank::class
            ]);
        }

        $this->clearCache();

        return $bank;
    }
}
