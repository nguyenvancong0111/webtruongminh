<?php

namespace App\Containers\Bank\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class FindBankByIdAction.
 *
 */
class FindBankByIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($bank_id)
    {
        $data = Apiato::call('Bank@FindBankByIdTask', [
            $bank_id,
            Apiato::call('Localization@GetDefaultLanguageTask'),
            ['with_relationship' => ['all_desc']]
        ]);
        return $data;
    }
}
