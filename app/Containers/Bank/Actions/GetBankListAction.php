<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-07 20:38:13
 * @ Description: Happy Coding!
 */

namespace App\Containers\Bank\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Bank\Tasks\BankListingTask;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetBankListAction extends Action
{
    public function run(
        array $filters = [],
        array $orderBy = ['created_at' => 'desc', 'id' => 'desc'],
        int $limit = 20,
        bool $skipPagination = false,
        Language $currentLang = null,
        array $externalData = [],
        int $currentPage = 1
    ): iterable {
        return $this->remember(function () use ($filters, $orderBy, $limit, $skipPagination, $currentLang, $externalData, $currentPage) {
            return $this->call('Bank@GetBankListTask', [
                $filters,
                $orderBy,
                $limit,
                $skipPagination,
                $currentLang,
                $externalData,
                $currentPage
            ]);
        }, null, [5]);
    }
}
