<?php

namespace App\Containers\Bank\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Bank\Models\Bank;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class UpdateBankAction.
 *
 */
class EnableBankAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $bank = Apiato::call('Bank@EnableBankTask', [$data->id]);
        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            ['status' => 1],
            ['status' => 2],
            'Hiển thị Bank',
            Bank::class
        ]);

        $this->clearCache();

        return $bank;
    }
}
