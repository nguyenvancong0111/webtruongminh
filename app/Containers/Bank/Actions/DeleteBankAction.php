<?php

namespace App\Containers\Bank\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Bank\Models\Bank;
use App\Ship\Parents\Actions\Action;

/**
 * Class DeleteBankAction.
 *
 */
class DeleteBankAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        Apiato::call('Bank@DeleteBankTask', [$data->id]);
        Apiato::call('Bank@DeleteBankDescTask', [$data->id]);

        $this->clearCache();

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            [],
            ['status' => -1],
            'Xóa Bank',
            Bank::class
        ]);
    }
}
