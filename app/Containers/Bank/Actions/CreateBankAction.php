<?php

namespace App\Containers\Bank\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Bank\Models\Bank;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class CreateBankAction.
 *
 */
class CreateBankAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        $bank = Apiato::call('Bank@CreateBankTask',[$data]);

        if ($bank) {
            Apiato::call('Bank@SaveBankDescTask', [$data, [], $bank->id]);

            Apiato::call('User@CreateUserLogSubAction', [
                $bank->id,
                [],
                $bank->toArray(),
                'Tạo Bank',
                Bank::class
            ]);
        }

        $this->clearCache();

        return $bank;
    }
}
