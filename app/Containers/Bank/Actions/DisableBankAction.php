<?php

namespace App\Containers\Bank\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Bank\Models\Bank;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Exception;

/**
 * Class UpdateBankAction.
 *
 */
class DisableBankAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $bank = Apiato::call('Bank@DisableBankTask', [$data->id]);

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            ['status' => 2],
            ['status' => 1],
            'Ẩn Bank',
            Bank::class
        ]);

        $this->clearCache();

        return $bank;
    }
}
