@extends('basecontainer::admin.layouts.default')

@section('right-breads')
    {!! $wards->appends($search_data->toArray())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="header">
            <div class="float-left">
                <h2> {{ __('location::admin.ward.breadcrumb') }} </h2>
            </div>
        </div>
        <div class="clearfix mb-2"></div>
        <form action="{{ route('location.ward_list') }}" method="GET">
            <div class="card card-accent-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i class="fa fa-bookmark-o"></i></span></div>
                                <input type="text" name="name" class="form-control" placeholder="Tiêu đề" value="{{ @$search_data->name }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                    {{-- <a class="btn btn-sm btn-primary" href="{{ route('location.ward_add') }}">
                        {{ __('location::admin.city.city_addButton_lable') }}
                    </a> --}}
                </div>
            </div>
        </form>
        <div class="card rounded-0">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Danh sách
            </div>
            <div class="card-body">
                <table class="table table-bordered table-striped" id="tableService">
                    <thead>
                        <tr>
                            {{-- <th style="width: 40px" class="text-center">
                                <input type="checkbox" id="checkAll" value="" name="checkAll"/>
                            </th> --}}
                            <th>
                                {{ __('location::admin.ward.name') }}
                            </th>
                            <th>
                                {{ __('location::admin.district.name') }}
                            </th>
                            <th>
                                {{ __('location::admin.action') }}
                            </th>
                        </tr>
                    </thead>
                    @forelse (@$wards ?? [] as $ward)
                        <tr>
                            {{-- <td>
                                <input type="checkbox" id="item_check_{{ $ward->id }}" value="{{ $ward->id }}" name="itemCheck[]"/>
                            </td> --}}
                            <td>
                                {{ @$ward->name }}
                            </td>
                            <td>
                                {{ @$ward->district->name }}
                            </td>
                            <td>
                                <div class="d-inline">
                                    <form action="{{ route('location.ward_delete', [
                                        'id' => $ward->id
                                    ]) }}" method="POST">
                                        @csrf
                                            <button type="submit" onclick="return confirm('{{ __('location::admin.ward.confirm') }}')" class="btn rounded-0">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>
                                    </form>
                                    <a href="{{ route('location.ward_edit', [
                                        'id' => $ward->id
                                    ]) }}" class="btn rounded-0">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">
                                {{ __('contractor::admin.list.not_found') }}
                            </td>
                        </tr>
                    @endforelse
                </table>
            </div>
            @if (@$wards->hasPages())
                <div class="card-footer">
                    {!! $wards->appends($search_data->toArray())->links('basecontainer::admin.inc.paginator')  !!}
                </div>
            @endif    
        </div>
    </div>
</div>
@endsection