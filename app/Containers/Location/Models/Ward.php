<?php

namespace App\Containers\Location\Models;

use App\Ship\Parents\Models\Model;

class Ward extends Model
{
    protected $table = '_geovnward';

    protected $fillable = [
        'name',
        'district_id'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = '_geovnward';

    public function district(){
        return $this->hasOne(District::class, 'id', 'district_id');
    }
}
