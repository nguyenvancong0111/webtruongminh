<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-10 12:13:57
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class SortOption extends BaseEnum
{
    /**
     * Phổ biến
     */
    const DEFAULT = 'default';
    /**
     * Bán chạy
     */
    const TOP_SELLER = 'top_seller';
    /**
     * MỚi nhất
     */
    const NEWEST = 'newest';
    /**
     * Giá thấp đến cao
     */
    const PRICE_ASC = 'price,asc';
    /**
     * Giá cao đến thấp
     */
    const PRICE_DESC = 'price,desc';

    const TEXT = [
        self::DEFAULT => 'Phổ biến',
        self::TOP_SELLER => 'Bán chạy',
        self::NEWEST => 'Mới cập nhật',
        self::PRICE_ASC => 'Giá từ thấp đến cao',
        self::PRICE_DESC => 'Giá từ cao đến thấp'
    ];

    const SORT_OPTIONS = [
        [
            "display_value" => self::TEXT[self::NEWEST],
            "query_value" => self::NEWEST,
            "selected" => false
        ],
        [
            "display_value" => self::TEXT[self::PRICE_ASC],
            "query_value" => self::PRICE_ASC,
            "selected" => false
        ],
        [
            "display_value" => self::TEXT[self::PRICE_DESC],
            "query_value" => self::PRICE_DESC,
            "selected" => false
        ]
    ];

    const ORDER_QUERY_CONDITIONS = [
        self::TOP_SELLER => [
            'sort_order' => 'asc',
            'is_sale' => 'asc',
            'purchased_count' => 'desc',
            'updated_at' => 'desc',
            'created_at' => 'desc'
        ],
        self::NEWEST => [
            'is_new' => 'asc',
            'updated_at' => 'desc',
            'created_at' => 'desc'
        ],
        self::PRICE_ASC => [
            'price' => 'asc',
            'updated_at' => 'desc',
            'created_at' => 'desc'
        ],
        self::PRICE_DESC => [
            'price' => 'desc',
            'updated_at' => 'desc',
            'created_at' => 'desc'
        ]
    ];

    public static function sortOption()
    {
        return [
            [
                "display_value" => __('site.moicapnhat'),
                "query_value" => self::NEWEST,
                "selected" => false
            ],
            [
                "display_value" => __('site.giathapdencao'),
                "query_value" => self::PRICE_ASC,
                "selected" => false
            ],
            [
                "display_value" => __('site.giacaodenthap'),
                "query_value" => self::PRICE_DESC,
                "selected" => false
            ]
        ];
    }
}
