<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-10 11:56:14
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class ItemShowOption extends BaseEnum
{
    /**
     * 60
     */
    const SIX = 60;

    /**
     * 40
     */
    const FOUR = 40;

    /**
     * 30
     */
    const THREE = 30;

    /**
     * 20
     */
    const TWO = 20;

    /**
     * 10
     */
    const ONE = 10;

    const TEXT = [
        self::SIX => '60',
        self::FOUR => '40',
        self::THREE => '30',
        self::TWO => '20',
        self::ONE => '10',
    ];

    const ITEM_SHOW_OPTIONS = [
        [
            "display_value" => self::TEXT[self::TWO],
            "query_value" => self::TWO,
            "selected" => false
        ],
        [
            "display_value" => self::TEXT[self::FOUR],
            "query_value" => self::FOUR,
            "selected" => false
        ],
        [
            "display_value" => self::TEXT[self::SIX],
            "query_value" => self::SIX,
            "selected" => false
        ]
    ];
}
