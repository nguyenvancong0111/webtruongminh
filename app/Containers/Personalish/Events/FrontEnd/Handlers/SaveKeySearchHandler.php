<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-21 14:44:04
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 11:09:58
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\Events\FrontEnd\Handlers;

use App\Containers\BaseContainer\Events\Handlers\BaseFrontEventHandler;
use App\Containers\Personalish\Events\FrontEnd\Events\SaveKeySearchEvent;
use App\Containers\Customer\Actions\FrontEnd\CustomerSearch\StoreCustomerSearchAction;
use Carbon\Carbon;

class SaveKeySearchHandler extends BaseFrontEventHandler
{
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(SaveKeySearchEvent $event)
    {
        $data=['key_search'=>$event->keySearch,'customer_id'=>$event->customer_id];

        app(StoreCustomerSearchAction::class)->run($data);
    }
}
