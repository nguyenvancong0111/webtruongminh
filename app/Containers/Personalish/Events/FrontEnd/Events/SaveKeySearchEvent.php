<?php

namespace App\Containers\Personalish\Events\FrontEnd\Events;

use App\Ship\Parents\Events\Event;
use Illuminate\Queue\SerializesModels;

class SaveKeySearchEvent extends Event
{
    use SerializesModels;

    public $keySearch;
    public $customer_id;
   

    public function __construct(string $keySearch='',int $customer_id)
    {
        $this->keySearch = $keySearch;
        $this->customer_id = $customer_id;
    }

    public function broadcastOn()
    {
        return [];
    }
}
