<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-12 19:41:08
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\Services;

use Apiato\Core\Foundation\StringLib;
use App\Containers\Localization\Models\Language;
use App\Containers\Manufacturer\Actions\FrontEnd\Getters\GetManufacturersByCategoryIdAction;
use App\Containers\Personalish\Enums\ItemShowOption;
use App\Containers\Personalish\Enums\RatingOption;
use App\Containers\Personalish\Enums\ServiceOption;
use App\Containers\Personalish\Enums\SortOption;
use App\Containers\Product\Actions\FrontEnd\ProductListingAction;

class ListingProductService
{
    public $productListingAction, $getManufacturersByCategoryIdAction;
    public $limit = 20;
    public $skipPagin = false, $skipCache = false;

    public function __construct(
        ProductListingAction $productListingAction,
        GetManufacturersByCategoryIdAction $getManufacturersByCategoryIdAction
    )
    {
        $this->productListingAction = $productListingAction;
        $this->getManufacturersByCategoryIdAction = $getManufacturersByCategoryIdAction;
    }

    public function skipPagin($skipPagin = true): self
    {
        $this->skipPagin = $skipPagin;
        return $this;
    }

    public function skipCache(bool $skipCache = true): self
    {
        $this->skipCache = $skipCache;
        return $this;
    }

    public function getListingProduct(array $filters, Language $currentLang, int $limit = 20, int $currentPage = 1)
    {
        $orderBy = $this->detectSortOption(@$filters['sortOption'] ?? 'top_seller');

        $filters = array_merge([
            // 'is_home' => ProductStatus::IS_HOME,
            // 'is_new' =1> ProductStatus::IS_NEW,
            // 'inRandomOrder' => true, // Nếu để random thì orderBy ko còn được áp dụng
            'orderBy' => true
        ], $filters);

        // dd($filters);

        $externalData = ['with_relationship' => [
            // 'promotion_campaign',
            'manufacturer',
            'specialTags' => function ($q) use ($currentLang) {
                $q->mustHaveDesc($currentLang->language_id);
            },
            'specialTags.desc' => function ($q) use ($currentLang) {
                $q->activeLang($currentLang->language_id);
            }
        ]];

        $products = $this->productListingAction->skipCache($this->skipCache)->run(
            $filters, // filters
            $orderBy ? $orderBy : [
                'sort_order' => 'asc',
                'updated_at' => 'desc',
                'created_at' => 'desc'
            ], // orderBy
            $limit, // limit
            $this->skipPagin, // Skip pagination
            $currentLang, // curent lang
            $externalData,
            $currentPage
        );

        return $products;
    }

    public function serviceOptions(int $selectedService = 0)
    {
        $serviceValues = ServiceOption::SERVICE_OPTIONS;

        $serviceFilters = [
            "collapsed" => 5,
            "display_name" => __('site.service'),
            "multi_select" => false,
            "query_name" => "service",
            "values" => $serviceValues
        ];

        if (!isset(ServiceOption::TEXT[$selectedService])) {
            return $serviceFilters;
        }

        $this->selectedFilter($serviceValues, $selectedService);

        $serviceFilters['values'] = $serviceValues;

        return $serviceFilters;
    }

    public function ratingOptions(int $selectedStar = 9)
    {
        $ratingValue = RatingOption::RATING_OPTIONS;

        $ratingFilters = [
            "collapsed" => 5,
            "display_name" => __('site.danhgia'),
            "multi_select" => false,
            "query_name" => "rating",
            "values" => $ratingValue
        ];

        if ($selectedStar < 1 || $selectedStar > 5) return $ratingFilters;

        $this->selectedFilter($ratingValue, $selectedStar);

        $ratingFilters['values'] = $ratingValue;

        return $ratingFilters;
    }

    public function productBrands(array $categoryIds, Language $currentLang, string $selected = '')
    {
        $brands = $this->getManufacturersByCategoryIdAction->run($categoryIds, $currentLang);

        $values = [];

        if (!empty($brands)) {
            foreach ($brands as $item) {
                $values[] = [
                    "display_value" => $item->name,
                    "query_value" => $item->manufacturer_id,
                    "selected" => false
                ];
            }
        }

        $brandFilters = [
            'group_id' => 'group_brand',
            "collapsed" => 5,
            "display_name" => __('site.thuonghieu'),
            "multi_select" => true,
            "query_name" => "brand",
            "values" => $values
        ];

        $this->selectedFilter($values, explode(',', $selected), false);

        $brandFilters['values'] = $values;

        return $brandFilters;
    }

    public function detectSortOption(string $sortOption)
    {
        return isset(SortOption::ORDER_QUERY_CONDITIONS[$sortOption]) ? SortOption::ORDER_QUERY_CONDITIONS[$sortOption] : '';
    }

    public function sortOptions(string $selectedOption = '')
    {
        $options = SortOption::sortOption();

        $this->selectedFilter($options, $selectedOption);

        return $options;
    }

    public function showingOptions(string $selectedOption = '')
    {
        $options = ItemShowOption::ITEM_SHOW_OPTIONS;

        $this->selectedFilter($options, $selectedOption);

        return $options;
    }

    public function selectedFilter(array &$queryValues, $selectedValue = '', bool $isDefaultZero = true): void
    {
        $picked = false;
        if (!empty($selectedValue)) {
            for ($i = 0; $i < count($queryValues); $i++) {
                if (is_array($selectedValue)) {
                    $queryValues[$i]['selected'] = in_array($queryValues[$i]['query_value'], $selectedValue);
                } else {
                    if ($queryValues[$i]['query_value'] == $selectedValue) {
                        $queryValues[$i]['selected'] = true;
                        $picked = true;
                        break;
                    }
                }
            }
        }

        if (!$picked && $isDefaultZero) {
            $queryValues[0]['selected'] = true;
        }
    }

    public function findFilters(array $arrayRequestData, string $startWith = 'option_', string $separator = ',')
    {
        $optionParams = array_filter($arrayRequestData, function ($v, $k) use ($startWith) {
            return StringLib::startsWith($k, $startWith) && $v != '';
        }, ARRAY_FILTER_USE_BOTH);

        if (!empty($optionParams)) {
            $optionParams = array_map(function ($item) use ($separator) {
                return $item !== '' ? array_unique(explode($separator, $item)) : [];
            }, $optionParams);
        }

        return $optionParams;
    }
}
