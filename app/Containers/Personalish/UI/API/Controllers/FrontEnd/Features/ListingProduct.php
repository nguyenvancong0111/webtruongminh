<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-14 22:35:46
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\UI\API\Controllers\FrontEnd\Features;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Category\Actions\FrontEnd\GetCategoryByIdAction;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\Filter\Actions\GetFiltersGroupByFilterGroupsAction;
use App\Containers\Filter\Enums\FilterType;
use App\Containers\Personalish\Actions\GetFilterForProductListingAction;
use App\Containers\Personalish\UI\API\Requests\FrontEnd\PersonalishListingProductRequest;
use App\Containers\Personalish\Services\ListingProductService;
use App\Containers\Personalish\UI\API\Transformers\FrontEnd\Filter\FilterGroupTransformer;
use App\Containers\Product\Enums\ProductStatus;
use App\Containers\Product\UI\API\Transformers\FrontEnd\ProductListTransformer;
use App\Containers\PromotionCampaign\Actions\FrontEnd\GetMostRecentlyCampaignOnProductListAction;
use App\Containers\PromotionCampaign\Actions\SubActions\CalcProductPriceByCampaignSubAction;
use Spatie\Fractalistic\ArraySerializer;
use App\Containers\Personalish\Events\FrontEnd\Events\SaveKeySearchEvent;
use Illuminate\Support\Facades\Cookie;

trait ListingProduct
{
    public $data = [];
    private $arrayRequestData, $conditions = [], $categoryIds = [], $filterParams = [];
    private $perPage = 9;

    public function listingProduct(
        PersonalishListingProductRequest $request,
        ListingProductService $listingProductService,
        CalcProductPriceByCampaignSubAction $calcProductPriceByCampaignSubAction
    )
    {
        $this->arrayRequestData = $request->all();

        if (!empty($this->arrayRequestData['pLimit'])) {
            $this->perPage = intval($this->arrayRequestData['pLimit']);
        }

        $this->data['block'] = [
            'code' => 'listing_products',
            'title' => 'Product Listing',
            'icon' => ''
        ];

        /**
         * Khởi tạo mảng với các dữ liệu được cache trong config.
         * Hiển thị ở box navigation phía trên list sản phẩm
         */
        $this->data['sort_options'] = $listingProductService->sortOptions(isset($this->arrayRequestData['sort_option']) ? $this->arrayRequestData['sort_option'] : '');
        //$this->data['item_show_options'] = $listingProductService->showingOptions(isset($this->arrayRequestData['show_option']) ? $this->arrayRequestData['show_option'] : '');

        // Khởi tạo mảng filters, hiển thị ở sidebar
        //$this->data['filters'][] = $listingProductService->serviceOptions(isset($this->arrayRequestData['service']) ? (int)$this->arrayRequestData['service'] : 0); // Khởi tạo mảng $filters
        //$this->data['filters'][] = $listingProductService->ratingOptions(isset($this->arrayRequestData['rating']) ? (int)$this->arrayRequestData['rating'] : 0); // Khởi tạo mảng $filters

        if (!empty($this->arrayRequestData['categoryIds'])) {
            $this->categoryIds = explode(',', $this->arrayRequestData['categoryIds']);
        } else {
            // Lấy toàn bộ id danh mục con của danh mục hiện tại
            $this->categoryIds = isset($this->arrayRequestData['categoryId']) && is_numeric($this->arrayRequestData['categoryId']) ? app(GetCategoryByIdAction::class)->run(CategoryType::PRODUCT, (int)$this->arrayRequestData['categoryId'], $this->currentLang, true) : [];
        }

        // dd($this->categoryIds,$this->arrayRequestData['categoryId']);
        if (!empty($this->categoryIds)) {
            // dd(isset($this->arrayRequestData['brand'])  ? $this->arrayRequestData['brand'] : 0);
            $this->data['filters'][] = $listingProductService->productBrands($this->categoryIds, $this->currentLang, isset($this->arrayRequestData['brand']) ? $this->arrayRequestData['brand'] : '');

            $this->detectFilters($listingProductService);
        }

        if (!empty($this->arrayRequestData['q']) && isset($this->user->id)) {
            SaveKeySearchEvent::dispatch($this->arrayRequestData['q'], $this->user->id);
        } else if (!empty($this->arrayRequestData['q'])) {
            $size = 0;
            $key = 0;
            $check = 0;
            $arrKeySearch = Cookie::get('arrKeySearch');
            if (!empty($arrKeySearch)) {
                $size = sizeof($arrKeySearch);
                $key = array_key_last($arrKeySearch) + 1;
            }
            // dd(array_key_first($arrKeySearch));
            if ($size == 10) {
                Cookie::queue(Cookie::forget('arrKeySearch[' . array_key_first($arrKeySearch) . ']'));
            }
            Cookie::queue('arrKeySearch[' . $key . ']', $this->arrayRequestData['q'], 40000);
        }

        $this->detectConditions();

        $this->buildProduct($listingProductService, $calcProductPriceByCampaignSubAction);

        return $this->data;
    }

    private function detectFilters(ListingProductService $listingProductService)
    {
        // Lấy toàng bộ Filter Groups của các ids danh mục vừa lấy phía trên
        $filterGroups = app(GetFilterForProductListingAction::class)->run($this->categoryIds, $this->currentLang);

        if (!empty($filterGroups) && !$filterGroups->IsEmpty()) {

            // Lọc các filter theo từng loại
            $this->filterParams = [
                FilterType::OPTION => $listingProductService->findFilters($this->arrayRequestData, FilterType::OPTION),
                FilterType::RANGE_OPTION => $listingProductService->findFilters($this->arrayRequestData, FilterType::RANGE_OPTION, '-')
            ];

            // Lấy về toàn bộ filters và được group by id của filter_group
            $filtersGroupByGroup = app(GetFiltersGroupByFilterGroupsAction::class)->run($filterGroups->pluck('filter_group_id')->toArray(), $this->currentLang);

            // Transform dữ liệu để push xuống client
            $filtersGroupByGroup = $this->transform($filtersGroupByGroup, new FilterGroupTransformer($this->filterParams), [], [], 'filters', new ArraySerializer(), false);

            // Array merge với mảng các tùy chọn lọc khi khời tạo hàm
            $this->data['filters'] = array_merge($this->data['filters'], $filtersGroupByGroup);
        }
    }

    private function detectConditions(): void
    {
        // Lấy về danh sách các sản phẩm dựa trên các điều kiên
        $conditions = [
            'cate_ids' => isset($this->arrayRequestData['q']) ? [] : $this->categoryIds,
            'groupFilters' => isset($this->filterParams[FilterType::OPTION]) ? $this->filterParams[FilterType::OPTION] : [],
            'betweenPrice' => isset($this->filterParams[FilterType::RANGE_OPTION]) ? array_shift($this->filterParams[FilterType::RANGE_OPTION]) : [],
            'byRating' => (int)@$this->arrayRequestData['rating'],
            'sortOption' => @$this->arrayRequestData['sort_option'] ?? '',
        ];

        isset($this->arrayRequestData['isQuick']) ? $conditions['isQuick'] = (int)$this->arrayRequestData['isQuick'] : false;

        isset($this->arrayRequestData['isShippingRequired']) ? $conditions['isShippingRequired'] = $this->arrayRequestData['isShippingRequired'] ? ProductStatus::SHIPPING_REQUIRED : ProductStatus::SHIPPING_NOT_REQUIRED : false;

        isset($this->arrayRequestData['isHot']) ? $conditions['isHot'] = $this->arrayRequestData['isHot'] ? ProductStatus::IS_HOT : ProductStatus::ZERO : false;

        isset($this->arrayRequestData['isSale']) ? $conditions['isSale'] = $this->arrayRequestData['isSale'] ? ProductStatus::IS_SALE : ProductStatus::ZERO : false;

        isset($this->arrayRequestData['brand']) ? $conditions['byBrand'] = explode(',', $this->arrayRequestData['brand']) : false;

        isset($this->arrayRequestData['isPromotion']) ? $conditions['isPromotion'] = $this->arrayRequestData['isPromotion'] ? ProductStatus::IS_PROMOTION : ProductStatus::NON_PROMOTION : false;

        isset($this->arrayRequestData['q']) && !empty($this->arrayRequestData['q']) ? $conditions['likeName'] = $this->arrayRequestData['q'] : false;

        $this->conditions = $conditions;

        // dd($this->conditions );
        // dd($this->arrayRequestData,$this->conditions );
    }

    private function buildProduct(ListingProductService $listingProductService, CalcProductPriceByCampaignSubAction $calcProductPriceByCampaignSubAction): void
    {
        $conditions = $this->conditions;
        // Nếu có collectionId thì chỉ lấy ds product của collection đó
        if (!empty($this->arrayRequestData['collectionId'])) {
            unset($conditions['cate_ids']);
            $collection = Apiato::call('Collection@FrontEnd\FindCollectionByIdAction', [
                $this->arrayRequestData['collectionId'], ['products:id'], $this->currentLang, [['status', '=', 1]], ['id']
            ]);

            $conditions['equalIds'] = $collection->products->pluck('id')->toArray();
            empty($conditions['equalIds']) && $conditions['equalIds'] = [0]; // nếu collection ko chọn sp nào thì cho products=[]
        } //

        $this->data['products'] = $listingProductService->skipCache()->getListingProduct($conditions, $this->currentLang, @$this->arrayRequestData['show_option'] ?? $this->perPage, request()->page || 0);

        /*$campaigns = app(GetMostRecentlyCampaignOnProductListAction::class)->run($this->data['products']->pluck('id')->toArray());
        foreach ($this->data["products"] as $product) {
            if (isset($campaigns[$product->id])) {
                $calcProductPriceByCampaignSubAction->run($campaigns[$product->id], $product);
            }
        }*/

        // Transform dữ liệu để push xuống client
        $this->data['products'] = $this->transform($this->data['products'], new ProductListTransformer, [], [], 'product_list');

        // Phân tách sản phẩm và Thông tin phần trang, không để gộp vào key products
        $this->data['pagination'] = isset($this->data['products']['meta']['pagination']) ? $this->data['products']['meta']['pagination'] : [];
        $this->data['products'] = isset($this->data['products']['data']) ? $this->data['products']['data'] : [];
    }
}
