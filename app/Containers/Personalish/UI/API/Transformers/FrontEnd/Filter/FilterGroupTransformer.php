<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-09 01:30:53
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-09 22:29:43
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\UI\API\Transformers\FrontEnd\Filter;

use Apiato\Core\Foundation\StringLib;
use App\Containers\Filter\Enums\FilterType;
use App\Ship\Parents\Transformers\Transformer;

class FilterGroupTransformer extends Transformer
{
    public $optionParams;
    protected $query_name = '';

    public function __construct($optionParams)
    {
        $this->optionParams = $optionParams;

        parent::__construct();
    }

    protected $defaultIncludes = [
        'values'
    ];

    public function transform($filterGroup)
    {
        $slugName = StringLib::slug($filterGroup->desc->name, '_');
        $returnData = [
            'group_id' => $filterGroup->filter_group_id,
            'collapsed' => 5,
            'display_name' => $filterGroup->desc->name,
            'query_name' => $this->query_name = FilterType::OPTION . '_' . $slugName,
            'multi_select' => true,
        ];

        if ($filterGroup->min || $filterGroup->max) {
            $this->query_name = FilterType::RANGE_OPTION . '_' . $slugName;

            $returnData['min'] = $filterGroup->min;
            $returnData['max'] = $filterGroup->max;
            // $currentRange = isset($this->optionParams[FilterType::RANGE_OPTION][$this->query_name][0]) ? explode('_',$this->optionParams[FilterType::RANGE_OPTION][$this->query_name][0]) : [];
            $currentRange = isset($this->optionParams[FilterType::RANGE_OPTION][$this->query_name]) ? $this->optionParams[FilterType::RANGE_OPTION][$this->query_name] : [];

            $returnData['current_min'] = isset($currentRange[0]) ? (int)$currentRange[0] : 0;
            $returnData['current_max'] = isset($currentRange[1]) && $currentRange[1] ? (int)$currentRange[1] : $filterGroup->max;

            $returnData['query_name'] = $this->query_name;
        }
        return $returnData;
    }

    public function includeValues($filterGroup)
    {
        $filters = $filterGroup->filter;
        $selectedValues = [];

        if (isset($this->optionParams[FilterType::OPTION][$this->query_name])) {
            $selectedValues[FilterType::OPTION] = $this->optionParams[FilterType::OPTION][$this->query_name];
        } elseif (isset($this->optionParams[FilterType::RANGE_OPTION][$this->query_name])) {
            $selectedValues[FilterType::RANGE_OPTION] = $this->optionParams[FilterType::RANGE_OPTION][$this->query_name];
        }

        return (!empty($filters) && !$filters->IsEmpty()) ? $this->collection($filters, new FilterTransformer($selectedValues), 'filter_group_value') : $this->null();
    }
}
