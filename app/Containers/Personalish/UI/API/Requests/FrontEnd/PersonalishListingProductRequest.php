<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-09 18:16:12
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-01 15:30:38
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\UI\API\Requests\FrontEnd;

use App\Ship\Parents\Requests\ApiRequest;

class PersonalishListingProductRequest extends ApiRequest
{

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles'       => '',
        'permissions' => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        
    ];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            // 'categoryId'           => 'required|exists:category,category_id',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'categoryId.required'  => 'ID không tồn tại',
            'categoryId.exists'  => 'ID không tồn tại',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }
}
