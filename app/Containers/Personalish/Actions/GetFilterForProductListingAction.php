<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-10 10:39:22
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-14 00:48:34
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\Actions;

use App\Containers\Category\Tasks\GetAllFilterGroupTask;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;
use Illuminate\Support\Collection;

class GetFilterForProductListingAction extends Action
{
    public $getAllFilterGroupTask;

    public function __construct(GetAllFilterGroupTask $getAllFilterGroupTask)
    {
        $this->getAllFilterGroupTask = $getAllFilterGroupTask;
        parent::__construct();
    }
    public function run(array $categoryIds, Language $currentLang): ?Collection
    {
        return $this->remember(function() use ($categoryIds, $currentLang){
            $data = $this->getAllFilterGroupTask->run($categoryIds, '', $currentLang);

            return $data;
        });
    }
}
