<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-13 13:33:42
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-07-13 16:33:57
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\Providers;

use App\Containers\Personalish\Events\FrontEnd\Events\SaveKeySearchEvent;
use App\Containers\Personalish\Events\FrontEnd\Handlers\SaveKeySearchHandler;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        SaveKeySearchEvent::class => [
            SaveKeySearchHandler::class
        ]
       
    ];

    public function boot() {
        parent::boot();
    }
}
