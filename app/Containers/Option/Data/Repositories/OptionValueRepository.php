<?php

namespace App\Containers\Option\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class OptionValueRepository.
 */
class OptionValueRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Option';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'option_id',
        'title',
        'sort_order',
        'created_at',
        'updated_at'
    ];
}
