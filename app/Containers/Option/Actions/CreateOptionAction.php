<?php

namespace App\Containers\Option\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Option\Tasks\SaveOptionValueTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class CreateOptionAction.
 *
 */
class CreateOptionAction extends Action
{

    /**
     * @return mixed
     */
    public function run( $data)
    {
        $option = Apiato::call(
            'Option@CreateOptionTask',
            [
                $data
            ]
        );

        if ($option) {
            app(SaveOptionValueTask::class)->run( $data, [], $option->id);
        }

        $this->clearCache();

        return $option;
    }
}
