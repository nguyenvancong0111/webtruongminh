<?php

namespace App\Containers\Option\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use App\Containers\Option\Tasks\SaveOptionValueTask;

/**
 * Class UpdateOptionAction.
 *
 */
class UpdateOptionAction extends Action
{

    /**
     * @return mixed
     */
    public function run( $data)
    {
        $question = Apiato::call(
            'Option@SaveOptionTask',
            [
                $data
            ]
        );

        if($question) {
            $original_opt = Apiato::call('Option@GetAllOptionValueTask', [$question->id]);
            $data_opt = [];
            if(!empty($original_opt)){
                foreach ($original_opt as $itm){
                    $data_opt[$itm->id] = $itm;
                }
            }
            app(SaveOptionValueTask::class)->run( $data, $data_opt,$question->id);
        }

        $this->clearCache();

        return $question;
    }
}
