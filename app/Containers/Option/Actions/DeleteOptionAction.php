<?php

namespace App\Containers\Option\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class DeleteOptionAction.
 *
 */
class DeleteOptionAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        Apiato::call('Option@DeleteOptionTask', [$data->id]);


        $this->clearCache();
    }
}
