<?php

namespace App\Containers\Option\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetOptionCourseListAction extends Action
{
    public function run(array $filters = [], array $orderBy = ['created_at' => 'desc', 'id' => 'desc'], array $with = []): iterable {
        return $this->call('Option@FrontEnd\GetOptionCourseListTask',
            [$filters, $orderBy, $with]);
    }
}
