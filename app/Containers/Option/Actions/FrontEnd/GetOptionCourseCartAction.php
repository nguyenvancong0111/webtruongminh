<?php

namespace App\Containers\Option\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetOptionCourseCartAction extends Action
{
    public function run(array $arr_courseID = [], array $with = []): iterable {
        return $this->call('Option@FrontEnd\GetOptionCourseCartTask', [$arr_courseID, $with]);
    }
}
