<?php

namespace App\Containers\Option\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\Option\Models\Option;
use App\Containers\Option\Tasks\GetOptionBySlugIdTask;
use App\Ship\Parents\Actions\Action;

class GetOptionBySlugIdAction extends Action
{
    protected $getOptionBySlugIdTask;
    public function __construct(GetOptionBySlugIdTask $getOptionBySlugIdTask)
    {
        parent::__construct();
        $this->getOptionBySlugIdTask = $getOptionBySlugIdTask;
    }
    public function run(int $questionId, string $slug, Language $currentLang = null): ?Option
    {
        return $this->remember(function () use ($questionId, $slug, $currentLang) {
            return $this->getOptionBySlugIdTask->run($questionId, $slug, $currentLang);
        });
    }
}
