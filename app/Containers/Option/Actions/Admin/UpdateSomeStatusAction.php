<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 23:29:32
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-01 00:34:51
 * @ Description: Happy Coding!
 */

namespace App\Containers\Questions\Actions\Admin;

use App\Ship\Parents\Actions\Action;


class UpdateSomeStatusAction extends Action
{
    public function run(string $field,int $id, int $status) :? bool
    {
        $result = $this->call('Question@Admin\UpdateSomeStatusTask',[
            $field,
            $id,
            $status
        ]);

        $this->clearCache();

        return $result;
    }
}
