<?php

namespace App\Containers\Option\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class FindOptionByIdAction.
 *
 */
class FindOptionByIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($option_id, $withData = [])
    {
        $data = Apiato::call('Option@FindOptionByIdTask', [$option_id, $external_data = ['with_relationship' => array_merge([], $withData)]]);

        return $data;
    }
}
