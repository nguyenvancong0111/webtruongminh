<?php
use Illuminate\Support\Facades\Route;

Route::prefix('admin')->group(function () {
  Route::namespace('\App\Containers\Option\UI\WEB\Controllers\Admin')->prefix('ajax/option')->group(function () {
    Route::get('/getOptionsAutocomplete', [
      'as' => 'admin.option.findAutocomplete',
      'uses' => 'Controller@findAutocomplete'
    ]);
  });
});
