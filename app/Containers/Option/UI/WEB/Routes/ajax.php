<?php
use Illuminate\Support\Facades\Route;

Route::namespace('\App\Containers\Option\UI\WEB\Controllers\Admin')->prefix('admin/ajax')->group(function () {
  Route::prefix('option')->group(function () {
    Route::get('/all', [
      'as' => 'admin.ajax.option.all',
      'uses' => 'AjaxOptionController@all'
    ]);
  });

});

