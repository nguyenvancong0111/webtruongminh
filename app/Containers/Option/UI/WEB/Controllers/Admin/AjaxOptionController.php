<?php

namespace App\Containers\Option\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Option\UI\WEB\Requests\AjaxGetAllOptionsRequest;
use App\Containers\Option\UI\WEB\Transformers\OptionsTransformer;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;

/**
 * Class AjaxOptionController
 *
 * @package App\Containers\Option\UI\WEB\Controllers
 */
class AjaxOptionController extends BaseApiFrontController
{
    use ApiResTrait;
    public function all(AjaxGetAllOptionsRequest $request)
    {
        $options = Apiato::call('Option@OptionListingAction', [$request->toArray(), [], 10, false, 1, ['with_relationship' => ['option_value']], isset($request->page) && !empty($request->page) ? $request->page : 1]);
        if ($options->isNotEmpty()){
            $course_option = Apiato::call('Course@GetAllOptionCourseByIdTask', [$request->course_id]);
        }
        return FunctionLib::ajaxRespondV2(true, 'success', $this->convertData($options, isset($course_option) ? $course_option :  []));
    }

    private function convertData($data, $course_option){
        $data_return = [];
        if($data->isNotEmpty()){
            foreach ($data as $item){
                $option_value = [];
                if ($item->option_value->isNotEmpty()){
                    foreach ($item->option_value as $itm){
                        $option_value[] = [
                            'id' => $itm->id,
                            'title' => $itm->title,
                            'value' => isset($course_option[$itm->id]) ? $course_option[$itm->id]['value'] : null,
                            'price' => isset($course_option[$itm->id]) ? $course_option[$itm->id]['price'] : null,
                            'is_active' => isset($course_option[$itm->id]) && $course_option[$itm->id]['is_active'] == 1 ? true : false
                        ];
                    }
                }
                $data_return[$item->id] = [
                    'id' => $item->id,
                    'name' => $item->name,
                    'option_values' => $option_value,
                ];
            }
        }
        return $data_return;
    }


} // End class
