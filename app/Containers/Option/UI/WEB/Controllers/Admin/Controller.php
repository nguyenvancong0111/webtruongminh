<?php

namespace App\Containers\Option\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\Option\UI\WEB\Requests\CreateOptionRequest;
use App\Containers\Option\UI\WEB\Requests\DeleteOptionRequest;
use App\Containers\Option\UI\WEB\Requests\FindOptionByKeywordRequest;
use App\Containers\Option\UI\WEB\Requests\GetAllOptionsRequest;
use App\Containers\Option\UI\WEB\Requests\OptionAutoCompleteRequest;
use App\Containers\Option\UI\WEB\Requests\UpdateOptionRequest;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Request;

class Controller extends AdminController
{
    use ApiResTrait;

    public function __construct()
    {
        $this->title = 'Tùy chọn cho biến thể';
        parent::__construct();
    }

    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllOptionsRequest $request)
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Tùy chọn', $this->form == 'list' ? '' : route('admin_option_home_page'));
        \View::share('breadcrumb', $this->breadcrumb);

        $options = Apiato::call('Option@OptionListingAction', [$request->toArray(), [], 10, false, $this->currentLang, [], isset($request->page) && !empty($request->page) ? $request->page : 1]);

        return view('option::admin.index', [
            'search_data' => $request,
            'data' => $options,
        ]);
    }

    public function edit(Request $request)
    {
        $this->showEditForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Options', $this->form == 'list' ? '' : route('admin_option_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Chỉnh sửa thông tin');
        \View::share('breadcrumb', $this->breadcrumb);

        $option = Apiato::call('Option@Admin\FindOptionByIdAction', [$request->id, ['option_value']]);

        $option_data = [];
        if ($option->option_value->isNotEmpty()) {
            foreach ($option->option_value as $opt) {
                $option_data[] = [
                    'id' => $opt->id,
                    'title' => $opt->title,
                    'sort' => $opt->sort_order,
                ];
            }
        }

        $option_data = !empty($option_data) ? json_encode($option_data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE) : null;
        return view('option::admin.edit', [
            'data' => $option,
            'option_data' => $option_data,
        ]);

    }

    public function add(Request $request)
    {
        $this->showAddForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Options', $this->form == 'list' ? '' : route('admin_option_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Thêm mới');
        \View::share('breadcrumb', $this->breadcrumb);
        return view('option::admin.edit');
    }

    public function update(UpdateOptionRequest $request)
    {
        try {
            $tranporter = new DataTransporter($request);
            $option = Apiato::call('Option@UpdateOptionAction', [$tranporter]);

            if ($option) {
                return redirect()->route('admin_option_edit_page', ['id' => $option->id])->with('status', 'Option đã được cập nhật');
            }
        } catch (\Exception $e) {
            throw $e;

        }
    }

    public function create(CreateOptionRequest $request)
    {
        try {
            $tranporter = new DataTransporter($request);
            $option = Apiato::call('Option@CreateOptionAction', [$tranporter]);
            if ($option) {
                return redirect()->route('admin_option_home_page')->with('status', 'Option đã được thêm mới');
            }
        } catch (\Exception $e) {
            $this->throwExceptionViaMess($e);
        }
    }

    public function deleteOption(DeleteOptionRequest $request)
    {
        try {
            Apiato::call('Option@DeleteOptionAction', [$request->id]);
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

} // End class
