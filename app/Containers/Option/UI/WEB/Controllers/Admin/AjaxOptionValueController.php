<?php

namespace App\Containers\Option\UI\WEB\Controllers\Admin;

use App\Containers\Option\UI\WEB\Requests\CreateOptionRequest;
use App\Containers\Option\UI\WEB\Requests\DeleteOptionRequest;
use App\Containers\Option\UI\WEB\Requests\GetAllOptionsRequest;
use App\Containers\Option\UI\WEB\Requests\FindOptionByIdRequest;
use App\Containers\Option\UI\WEB\Requests\UpdateOptionRequest;
use App\Containers\Option\UI\WEB\Requests\StoreOptionRequest;
use App\Containers\Option\UI\WEB\Requests\EditOptionRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Option\UI\WEB\Requests\AjaxGetAllOptionsValueRequest;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;

/**
 * Class AjaxOptionValueController
 *
 * @package App\Containers\Option\UI\WEB\Controllers
 */
class AjaxOptionValueController extends WebController
{
  use ApiResTrait;
  /**
   * Show all entities
   *
   * @param GetAllOptionsRequest $request
   */
  public function all(AjaxGetAllOptionsValueRequest $request)
  {
    $optionsValue = Apiato::call('Option@GetAllOptionsValueAction', [$request, ['desc']]);
    return $this->sendResponse($optionsValue);
  }
} // End class
