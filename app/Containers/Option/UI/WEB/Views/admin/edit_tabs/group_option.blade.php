<div class="tab-pane active" id="group_option">
    <div class="tabbable">
        <div class="row form-group align-items-center">
            <label class="col-sm-1 control-label text-right mb-0" for="type_option">Tên nhóm</label>
            <div class="col-sm-4">
                <input type="text" name="name" id="name" class="form-control" value="{{old('name', @$data->name)}}">
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-1 control-label text-right mb-0" for="type_option">Phân loại</label>
            <div class="col-sm-4">
                <select name="type" id="type" class="form-control">
                    <option value="1" selected>Áp dụng thời gian</option>
                </select>
            </div>
        </div>
        <hr>

        <!--option-->
        <div class="card card-accent-primary" id="option">
            <div class="card-header">
                <div class="row">
                    <div class="col-sm-6">
                        <i class="fa fa-pencil"></i> Options
                    </div>
                </div>
            </div>
            <div class="tab-content p-0">
                <div class="row pl-1 pr-1">
                    <div class="col-sm-12">
                        <div v-for="(item, key) in data.val">
                            <input type="hidden" v-bind:name="'option['+key+'][id]'" v-model="item.id" class="form-control js-input" >
                            <div class="row mt-3">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <input type="text" v-bind:name="'option['+key+'][title]'" v-model="item.title" class="form-control js-input" placeholder="Tên hiển thị">
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <input type="text" v-bind:name="'option['+key+'][sort]'" v-model="item.sort" class="form-control js-input"placeholder="Sort" >
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="input-group-append">
                                        <span class="btn btn-danger js-input" @click="trashOpt(key)" style="cursor: pointer"><i class="fa fa-trash"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2 mb-3">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-2">
                                <a class="btn btn-outline-info  w-100" @click="addOpt"  style="cursor: pointer">Thêm Option</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js_bot_all')
    <script>
        $('#published').datetimepicker({
            format: 'd/m/Y H:i',
        });

        // option
        var option_data = '{!! isset($option_data) && !empty($option_data) ? addslashes($option_data) : '' !!}';
        var optionVue = new Vue({
            el: '#option',
            data() {
                return {
                    data: {
                        val: option_data != '' ? JSON.parse(option_data) : [{title: '', value: '', sort: ''}],
                    }
                }
            },
            mounted(){},
            methods: {
                addOpt: function () {
                    var ans = {title: '', value: '', sort: ''};
                    return this.data.val.push(ans);
                },
                trashOpt: function (key) {
                    if(this.data.val.length > 1) {
                        this.data.val.splice(key, 1);
                    }else {
                        this.data.val.splice(key, 1);
                        var ans = {title: '', value: '', sort: ''};
                        return this.data.val.push(ans);
                    }
                },
            },
        });
    </script>
@endpush
