<?php

namespace App\Containers\Option\UI\WEB\Transformers;

use App\Containers\Option\Models\Option;
use App\Ship\Parents\Transformers\Transformer;

class OptionsTransformer extends Transformer
{
    /**
     * @param $token
     *
     * @return  array
     */
    public function transform(Option $entity)
    {
        $response[$entity->id] = [
            'id' => $entity->id,
            'type' => $entity->type,
            'name' => $entity->name,
            'option_value' => $entity->relationLoaded('option_value') ? $entity->option_value : $this->null(),
        ];
        return $response;
    }
}
