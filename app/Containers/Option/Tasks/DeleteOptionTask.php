<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteOptionTask.
 */
class DeleteOptionTask extends Task
{

    protected $repository;

    public function __construct(OptionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($option_id)
    {
        try {
            $this->repository->update(['status' => -1],$option_id);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
