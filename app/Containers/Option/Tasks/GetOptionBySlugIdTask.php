<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Localization\Models\Language;
use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Containers\Option\Models\Option;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetOptionBySlugIdTask extends Task
{

    protected $repository;

    public function __construct(OptionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run(int $optionId): ?Option
    {

        $data = $this->repository->where('status',2);

        return $data->where('id',$optionId)->first();
    }
}
