<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class FindOptionByIdTask.
 */
class FindOptionByIdTask extends Task
{

    protected $repository;

    public function __construct(OptionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($option_id, $external_data = ['with_relationship' => []])
    {
        try {
            $data = $this->repository->with($external_data['with_relationship'])->find($option_id);
        } catch (Exception $e) {
            //throw $th;
            dd($e->getMessage());
        }


        return $data;
    }
}
