<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Option\Data\Repositories\OptionValueRepository;
use App\Ship\Parents\Tasks\Task;

class SaveOptionValueTask extends Task
{

    protected $repository;

    public function __construct(OptionValueRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data, $original_option, $option_id)
    {
        $option = $data->option;

        $arr_opt_id = array_column($original_option, 'id');

        $update_item = [];
        $insert_item = [];
        if (!empty($option)){
            foreach ($option as $key => $item){
                if (!empty($item['id']) && isset($original_option[$item['id']])){
                    unset($arr_opt_id[array_search((int)$item['id'], $arr_opt_id)]);
                    $update_item[$item['id']] = [
                        'option_id' => $option_id,
                        'title' => $item['title'],
                        'sort_order' => $item['sort'],
                    ];
                }else{
                    $insert_item[] = [
                        'option_id' => $option_id,
                        'title' => $item['title'],
                        'sort_order' => $item['sort'],
                    ];
                }
            }

            if (!empty($update_item)) {
                $this->repository->updateMultiple($update_item);
            }

            if (!empty($insert_item)) {
                $this->repository->getModel()->insert($insert_item);
            }

            if(!empty($arr_opt_id)){
                $this->repository->whereIn('id', $arr_opt_id)->delete();
            }

        }
    }
}
