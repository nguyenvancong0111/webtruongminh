<?php

namespace App\Containers\Option\Tasks\FrontEnd;

use App\Containers\Course\Data\Repositories\CourseOptionRepository;
use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetOptionCourseCartTask extends Task
{

    protected $repository;

    public function __construct(CourseOptionRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run(array $arr_courseID = [], array $with = []): iterable {

        $data = $this->repository;
        $data = $data->where('is_active', '=', 1);
        if (!empty($arr_courseID)){
            $data = $data->whereIn('course_id', $arr_courseID);
        }
        if (!empty($with)){
            $data->with($with);
        }


        $data->whereHas('option_value.option', function (Builder $q) {
            $q->where('option.status', '=', 2);
        });

        $data = $data->orderBy('id', 'ASC');
        // \DB::enableQueryLog();

        return $data->get()->groupBy('course_id');
        // dd(\DB::getQueryLog());
    }
}
