<?php

namespace App\Containers\Option\Tasks\FrontEnd;

use App\Containers\Course\Data\Repositories\CourseOptionRepository;
use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetOptionCourseListTask extends Task
{

    protected $repository;

    public function __construct(CourseOptionRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run(array $filters = [], array $orderBy = ['created_at' => 'desc', 'id' => 'desc'], array $with = []): iterable {

        $data = $this->repository;
        if (!empty($filters)){
            foreach ($filters as $key => $value){
                $data = $data->where($key, $value);
            }
        }
        if (!empty($with)){
            $data->with($with);
        }


        $data->whereHas('option_value.option', function (Builder $q) {
            $q->where('option.status', '=', 2);
        });

        if (!empty($orderBy)){
            foreach ($orderBy as $k => $v){
                $data->orderBy($k, $v);
            }
        }

        // \DB::enableQueryLog();

        return $data->get();
        // dd(\DB::getQueryLog());
    }
}
