<?php

namespace App\Containers\Option\Tasks;

use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class OptionListingTask.
 */
class OptionListingTask extends Task
{

    protected $repository;

    public function __construct(OptionRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($filters = [], $orderBy = ['created_at' => 'desc', 'id' => 'desc'], $limit = 20, $noPaginate = true, $defaultLanguage = null, $external_data = [], $current_page = 1)
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;

        if (isset($filters['id']) && $filters['id'] != '') {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('id', $filters['id']));
        } else {
            if (isset($filters['status']) && $filters['status'] != '') {
                $this->repository->pushCriteria(new ThisEqualThatCriteria('status', $filters['status']));
            } else {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('status', -1, '>'));
            }
            if (isset($filters['time_from']) && !empty($filters['time_from']) && $filters['time_from'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('created_at', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['time_from'])), '>='));
            }
            if (isset($filters['time_to']) && !empty($filters['time_to']) && $filters['time_to'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('created_at', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['time_to'],true)), '<='));
            }

            if (isset($filters['publish_from']) && !empty($filters['publish_from']) && $filters['publish_from'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('published', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['publish_from'])), '>='));
            }
            if (isset($filters['publish_to']) && !empty($filters['publish_to']) && $filters['publish_to'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('published', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['publish_to'],true)), '<='));
            }
        }
        if (!empty($external_data) && isset($external_data['with_relationship'])){
            $this->repository->with(array_merge($external_data['with_relationship'], []));
        }

        if (isset($filters['name']) && !empty($filters['name'])) {
            $this->repository->whereHas('desc', function (Builder $query) use ($filters) {
                $query->where('name', 'like', '%' . $filters['name'] . '%');
            });
        }

        foreach ($orderBy as $column => $direction) {
            $this->repository->orderBy($column, $direction);
        }
        return $noPaginate ? $this->repository->take($limit)->get() :  $this->repository->paginate($limit);
    }
}
