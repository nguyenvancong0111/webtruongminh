<?php

namespace App\Containers\Option\Tasks\Admin;

use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Ship\core\Traits\HelpersTraits\UpdateDynamicStatusTrait;
use App\Ship\Parents\Tasks\Task;

class UpdateSomeStatusTask extends Task
{
    use UpdateDynamicStatusTrait;

    protected $repository;

    public function __construct(OptionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(string $field,int $id, int $status) :? bool
    {
        return $this->updateDynamicStatus($field,$id,$status);
    }
}
