<?php
namespace App\Containers\Option\Models;

use App\Containers\Course\Models\CourseOption;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class Option extends Model
{
    protected $table = 'option';

    protected $fillable = [
        'type',
        'name',
        'sort_order',
        'status'
    ];


    public function option_value()
    {
        return $this->hasMany(OptionValue::class, 'option_id', 'id');
    }

    public function course_option(){
        return $this->hasOne(CourseOption::class, 'option_id', 'id');
    }
}
