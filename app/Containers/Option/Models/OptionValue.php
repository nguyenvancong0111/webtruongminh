<?php
namespace App\Containers\Option\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class OptionValue extends Model
{
    protected $table = 'option_value';

    protected $appends = ['name', 'option_value_id'];

    protected $fillable = [
        'option_id',
        'title',
        'sort_order',
        'status',
        'created_at',
        'updated_at'
    ];


    public function option()
    {
        return $this->hasOne(Option::class, 'id', 'option_id');
    }

    public function getNameAttribute()
    {
        return $this->title;
    }
    public function getOptionValueIdAttribute()
    {
        return $this->id;
    }
} // End classs
