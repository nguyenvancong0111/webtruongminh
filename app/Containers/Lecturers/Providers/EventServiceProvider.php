<?php

namespace App\Containers\Lecturers\Providers;

use App\Containers\Lecturers\Events\ForgotPasswordEvent;
use App\Containers\Lecturers\Events\Handlers\ForgotPasswordEmailHandler;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        ForgotPasswordEvent::class => [
            ForgotPasswordEmailHandler::class
        ]
    ];

    public function boot() {
        parent::boot();
    }
}
