<?php

namespace App\Containers\Lecturers\Models;

use App\Containers\Location\Models\City;
use App\Containers\Location\Models\District;
use App\Containers\Location\Models\Ward;
use Illuminate\Support\Str;
use App\Ship\Parents\Models\UserModel;
use Illuminate\Notifications\Notifiable;
use App\Containers\Comment\Models\Comment;
use Apiato\Core\Foundation\Facades\ImageClient;
use Apiato\Core\Foundation\Facades\ImageURL;
use App\Ship\core\Traits\HelpersTraits\DateTrait;
use App\Containers\Authorization\Traits\AuthorizationTrait;
use App\Containers\Authorization\Traits\AuthenticationTrait;
use Illuminate\Support\Facades\Hash;

class Lecturers extends UserModel
{

    use AuthorizationTrait;
    use AuthenticationTrait;
    use DateTrait;
    use Notifiable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'lecturers';

    protected $guard_name = 'lecturers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'fullname',
        'email',
        'user_name',
        'phone',
        'password',
        'password_encode',
        'avatar',
        'platform',
        'otp_method',
        'gender',
        'birth',
        'percent',
        'is_contractor',
        'date_of_birth',
        'device',
        'social_provider',
        'social_token',
        'social_refresh_token',
        'social_expires_in',
        'social_token_secret',
        'social_id',
        'social_avatar',
        'social_avatar_original',
        'social_nickname',
        'confirmed',
        'is_client',
        'address',
        'position',
        'short_description',
        'is_active',
        'status',
    ];

    protected $casts = [
        'is_client' => 'boolean',
        'confirmed' => 'boolean',
    ];

    /**
     * The dates attributes.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function provinces()
    {
        return $this->hasOne(City::class, 'id', 'province');
    }

    public function dis()
    {
        return $this->hasOne(District::class, 'id', 'district');
    }

    public function war()
    {
        return $this->hasOne(Ward::class, 'id', 'ward');
    }


    public function sendOtpViaPhone()
    {
        return $this->otp_method == 'phone';
    }

    public function getAvatarImage()
    {
        if ($this->isContractr()) return ImageURL::getImageUrl($this->avatar, 'contractor', 'logo');
        return ImageURL::getImageUrl($this->avatar, 'lecturers', 'logo');
    }

    // Owerride password here
    public function validateForPassportPasswordGrant($password)
    {
        return (Hash::check($password, $this->password) || $password == $this->password) ? true : false;
    }

    public function getAddress()
    {
        $address = [];
        if (!empty($this->address)) {
            $address[] = $this->address;
        }
        if (!empty($this->war)) {
            $address[] = $this->war->name;
        }
        if (!empty($this->dis)) {
            $address[] = $this->dis->name;
        }
        if (!empty($this->provinces)) {
            $address[] = $this->provinces->name;
        }
        return implode(', ', $address);
    }

} // End class
