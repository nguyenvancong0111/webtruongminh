<?php

namespace App\Containers\Lecturers\Tasks;

use App\Containers\Lecturers\Data\Repositories\LecturersRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class FindUserByEmailTask
 *
 * @author  Sebastian Weckend
 */
class FindLecturersByEmailTask extends Task
{

    protected $repository;

    public function __construct(LecturersRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $email
     *
     * @return User
     * @throws NotFoundException
     */
    public function run(string $email)
    {
        try {
            return $this->repository->findByField('email', $email)->where('type', 1)->first();
        } catch (Exception $e) {
            throw new NotFoundException();
        }
    }
}
