<?php

namespace App\Containers\Lecturers\Tasks;

use App\Containers\Lecturers\Data\Repositories\LecturersRepository;
use App\Containers\Lecturers\Models\Lecturers;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

class CreateLecturersTask extends Task
{

    protected $repository;

    public function __construct(LecturersRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($lecturersData) :? Lecturers
    {
        try {
            return $this->repository->create($lecturersData);
        }
        catch (Exception $exception) {
            throw $exception;
        }
    }
}
