<?php

namespace App\Containers\Lecturers\Tasks;

use Exception;
use Illuminate\Support\Arr;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Containers\Lecturers\Data\Repositories\LecturersRepository;

class UpdateLecturersTask extends Task
{

    protected $repository;

    public function __construct(LecturersRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            $lecturersData = Arr::except($data, ['_token', 'roles_ids', 'permissions_ids', '_method']);
            $lecturers = $this->repository->update($lecturersData, $id);
            return $lecturers;
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
