<?php

namespace App\Containers\Lecturers\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Lecturers\Data\Repositories\LecturersRepository;
use App\Containers\Lecturers\Data\Criterias\FilterLecturersCriteria;
use App\Containers\User\Data\Criterias\WithRoleCriteria;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;

class GetAllLecturersTask extends Task
{

    protected $repository;

    public function __construct(LecturersRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $limit=15, $hasPagination = true)
    {
        if($hasPagination == false){
          return $this->repository->scopeQuery(function ($query) {
            return $query->orderBy('id', 'DESC');
          })->get();
        }
        return $this->repository->scopeQuery(function ($query) {
          return $query->orderBy('id', 'DESC');
        })->paginate($limit);
    }

    public function LecturersFilter($LecturersFilterTransporter) {
      $this->repository->pushCriteria(new FilterLecturersCriteria($LecturersFilterTransporter));
    }

    public function withRole() {
      $this->repository->pushCriteria(new WithRoleCriteria());
    }

    public function selectFields(array $column=['*']) {
      $this->repository->pushCriteria(new SelectFieldsCriteria($column));
    }
} // End class
