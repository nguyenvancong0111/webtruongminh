<?php

namespace App\Containers\Lecturers\Tasks;

use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Ship\Parents\Tasks\Task;

class GetArrIDCourseByLecturersIDTask extends Task
{

    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $filters = [])
    {
        $data = $this->repository;
        if (!empty($filters)){
            foreach ($filters as $key => $val){
                $data = $data->where($key, $val);
            }
        }

        return $data->pluck('id')->toArray();

    }
} // End class
