<?php

namespace App\Containers\Lecturers\Tasks;

use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllLecturersCourseTask extends Task
{

    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $filters = [], bool $skipPaginate = true, int $limit = 10, int $currentPage = 1)
    {
        $this->repository->with(['desc' => function($q){
            $q->select('id', 'course_id', 'name', 'short_description', 'slug');
        }, 'lecturers_course' => function($qc){
            $qc->where('status', 2);
            $qc->select('id', 'fullname');
        }]);
        $this->repository->withCount('count_item as lessons');
        if (!empty($filters)){
            foreach ($filters as $key => $val){
                $this->repository->where($key, $val);
            }
        }
        $this->repository->orderByRaw('sort_order ASC, id DESC');
        return $skipPaginate ? ($limit > 0 ? $this->repository->limit($limit) : $this->repository->all()) : $this->repository->paginate($limit);

    }
} // End class
