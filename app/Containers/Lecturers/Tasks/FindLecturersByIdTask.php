<?php

namespace App\Containers\Lecturers\Tasks;

use App\Containers\Lecturers\Data\Repositories\LecturersRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindLecturersByIdTask extends Task
{

    protected $repository;

    public function __construct(LecturersRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $with=[])
    {
        try {
            return $this->repository->with($with)->find($id);
        }
        catch (Exception $exception) {
            throw $exception;
        }
    }

    public function withCount(array $withCount = []){

        if(!empty($withCount)) $this->repository->withCount($withCount);
    }
}
