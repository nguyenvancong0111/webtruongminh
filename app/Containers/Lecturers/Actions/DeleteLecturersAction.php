<?php

namespace App\Containers\Lecturers\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteLecturersAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Lecturers@DeleteLecturersTask', [$request->id]);
    }
}
