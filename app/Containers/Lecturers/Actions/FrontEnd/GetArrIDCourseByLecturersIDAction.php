<?php

namespace App\Containers\Lecturers\Actions\FrontEnd;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetArrIDCourseByLecturersIDAction extends Action
{
    public function run(array $filters = [])
    {
        $arr_course = Apiato::call('Lecturers@GetArrIDCourseByLecturersIDTask', [$filters], []);

        return $arr_course;
    }
}
