<?php

namespace App\Containers\Lecturers\Actions\FrontEnd;

use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\ImageURL;
use App\Ship\Parents\Actions\Action;
use Carbon\Carbon;

class SaveProfileLecturersAction extends Action
{
    public function run($request)
    {
        $lecturers = auth()->guard(config('auth.guard_for.frontend'))->user();
        $lecturers->fullname = $request->fullname ?? $lecturers->fullname;
        if (!empty($request->password_new) && !empty($request->password) && $request->password_new != '') {
            $lecturers->password = bcrypt($request->password_new) ?? $lecturers->password;
        }
        $lecturers->gender = $request->gender ?? $lecturers->gender;
        $lecturers->date_of_birth = Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($request->date_of_birth)) ?? $lecturers->date_of_birth;
        if ($lecturers->phone == '' && $request->phone != '' && $lecturers->type_check != 2) {
            $lecturers->phone = $request->phone;
            if ($request->has('type_check')) {
                $lecturers->type_check = 2;
            }
        }
        // process image
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            if ($image->isValid()) {
                $fname = ImageURL::makeFileName($request->fullname, $image->getClientOriginalExtension());
                $image = ImageURL::upload($image, $fname, 'lecturers');
                if ($image) {
                    $lecturers->avatar = $fname;
                } else {
                    return redirect()->back()->withInput()->withErrors(['image' => 'Upload ảnh lên server thất bại!']);
                }
            } else {
                return redirect()->back()->withInput()->withErrors(['image' => 'Upload ảnh thất bại!']);
            }
        }

        $lecturers->save();

        return $lecturers;
    }
}
