<?php

namespace App\Containers\Lecturers\Actions\FrontEnd;

use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\ImageURL;
use App\Ship\Parents\Actions\Action;
use Carbon\Carbon;

class UpdateInfoLecturersAction extends Action
{
    public function run($data)
    {
        $lecturers = auth()->guard(config('auth.guard_for.lecturers'))->user();
        $lecturers->fullname = $data['fullname'] ?? $lecturers->fullname;
        $lecturers->gender = !empty($data['gender']) ? $data['gender'] : $lecturers->gender;
        $lecturers->email = $data['email'] ?? $lecturers->email;
        $lecturers->phone = $data['phone'] ?? $lecturers->phone;
        $lecturers->address = $data['address'] ?? $lecturers->address;
        /*$lecturers->province = $data['province'] ?? $lecturers->province;
        $lecturers->district = $data['district'] ?? $lecturers->district;
        $lecturers->ward = $data['ward'] ?? $lecturers->ward;*/
        $lecturers->date_of_birth = !empty($data['date_of_birth']) ? Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($data['date_of_birth'])) : $lecturers->date_of_birth;

        $lecturers->save();

        return $lecturers;
    }
}