<?php

namespace App\Containers\Lecturers\Actions\FrontEnd;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Lecturers\Events\ForgotPasswordEvent;
use App\Containers\Lecturers\Mails\LecturersForgotPasswordMail;
use App\Containers\Lecturers\Tasks\CreatePasswordResetTask;
use App\Containers\Lecturers\Tasks\FindLecturersByEmailTask;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

/**
 * Class ForgotPasswordAction
 *
 * @author  Mahmoud Zalt <mahmoud@zalt.me>
 */
class ForgotPasswordAction extends Action
{

    /**
     * @param \App\Ship\Transporters\DataTransporter $data
     */
    public function run(DataTransporter $data): void
    {
        $lecturers = app(FindLecturersByEmailTask::class)->run($data->email);


        // generate token, password
        $generate = app(CreatePasswordResetTask::class)->run($lecturers);



        // send email
        ForgotPasswordEvent::dispatch($generate, $lecturers);

        /*$messageBody = view('luatasm::pc.lecturers.email.forgot-password-passwordReset', ['password' => $generate['pass_reset']])->render();
        Mail::html($messageBody, function ($message) use($lecturers) {
            $message->from($lecturers->email, $lecturers->fullname);
            $message->to($lecturers->email);
            $message->subject('Quên mật khẩu');
        });
        // check for failures
        if (empty(Mail::failures())){
            DB::beginTransaction();
            try{
                Apiato::call('Lecturers@UpdateLecturersAction', [new DataTransporter(['id' => $lecturers->id, 'password' => $generate['pass_reset']])]);
                DB::commit();
            }catch(\Exception $e){
                throw $e;
                DB::rollBack();
            }
        }*/

    }
}
