<?php

namespace App\Containers\Lecturers\Actions\FrontEnd;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllLecturersCourseAction extends Action
{
    public function run(array $filters = [], bool $skipPaginate = true, int $limit = 10, int $currentPage = 1, array $with = [])
    {
        $lecturers_course = Apiato::call('Lecturers@GetAllLecturersCourseTask', [$filters, $skipPaginate, $limit, $currentPage], [[$with]]);

        return $lecturers_course;
    }
}
