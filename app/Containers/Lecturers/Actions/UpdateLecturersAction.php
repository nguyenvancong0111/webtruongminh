<?php

namespace App\Containers\Lecturers\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class UpdateLecturersAction extends Action
{
    public function run(DataTransporter $transporter)
    {
      if ($transporter->password) {
        $transporter->password = bcrypt($transporter->password);
        $transporter->password_encode = bcrypt($transporter->password);
      }

      $lecturersPostData = $transporter->toArray();
      if (empty($lecturersPostData['password'])){
          unset($lecturersPostData['password']);
      }
      $lecturers = Apiato::call('Lecturers@UpdateLecturersTask', [$transporter->id, $lecturersPostData]);
      return $lecturers;
    }
}
