<?php

namespace App\Containers\Lecturers\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class GetAllLecturersAction extends Action
{
    public function run(DataTransporter $transporter, $hasPagination = true ,array $withData=[], array $column=['*'])
    {
      $Lecturers = Apiato::call('Lecturers@GetAllLecturersTask', [$transporter->limit, $hasPagination], [
        'withRole',
        ['selectFields' => [$column]],
        ['LecturersFilter' => [$transporter]],
        ['with' => [$withData]]
      ]);

      return $Lecturers;
    }
}
