<?php

namespace App\Containers\Lecturers\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class FindLecturersByIdAction extends Action
{
    public function run(int $lecturersId, array $withData=[], array $withCount = [])
    {
        $lecturers = Apiato::call('Lecturers@FindLecturersByIdTask', [$lecturersId, $withData], [
            [
                'withCount' => [$withCount]
            ]
        ]);

        return $lecturers;
    }
}
