<?php

namespace App\Containers\Lecturers\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Lecturers\Models\Lecturers;
use App\Ship\Transporters\DataTransporter;

class StoreNewLecturersAction extends Action
{
    public function run(DataTransporter $transporter) :? Lecturers
    {
      $transporter->password = bcrypt($transporter->password);
      $lecturersData = $transporter->toArray();

      $lecturers = Apiato::call('Lecturers@CreateLecturersTask', [$lecturersData]);
      return $lecturers;
    }
}
