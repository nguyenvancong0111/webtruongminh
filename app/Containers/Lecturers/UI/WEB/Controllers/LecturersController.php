<?php

namespace App\Containers\Lecturers\UI\WEB\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\FunctionLib as FoundationFunctionLib;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Lecturers\Models\Lecturers;
use App\Containers\Lecturers\UI\WEB\Requests\CreateLecturersRequest;
use App\Containers\Lecturers\UI\WEB\Requests\DeleteLecturersRequest;
use App\Containers\Lecturers\UI\WEB\Requests\EditLecturersRequest;
use App\Containers\Lecturers\UI\WEB\Requests\FilterLecturersBySeachableRequest;
use App\Containers\Lecturers\UI\WEB\Requests\GetAllLecturersRequest;
use App\Containers\Lecturers\UI\WEB\Requests\UpdateLecturersRequest;
use App\Containers\News\Models\News;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;

/**
 * Class Controller
 *
 * @author  Mahmoud Zalt  <mahmoud@zalt.me>
 */
class LecturersController extends AdminController
{
    use ApiResTrait;

    public function __construct()
    {
        if (FoundationFunctionLib::isDontUseShareData(['edit', 'store', 'update', 'delete', 'create'])) {
            $this->dontUseShareData = false;
        }

        parent::__construct();
    }

    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllLecturersRequest $request)
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Giảng Viên');
        \View::share('breadcrumb', $this->breadcrumb);

        $transporter = new DataTransporter($request);
        $transporter->type = 2;
        $lecturers = Apiato::call('Lecturers@GetAllLecturersAction', [
            $transporter,
            true,
            [],
            ['id', 'email', 'phone', 'fullname', 'created_at']
        ]);

        if ($request->ajax()) {
            return $this->sendResponse($lecturers);
        }

        $roles = Apiato::call('Authorization@GetAllRolesAction', [new DataTransporter([]), 0, true, ['lecturers']]);
        return view('lecturers::index', [
            'lecturers' => $lecturers,
            'input' => $request->all(),
            'roles' => $roles,
        ]);
    }

    public function create()
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Giảng Viên', route('admin.lecturers.index'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Tạo mới');
        \View::share('breadcrumb', $this->breadcrumb);

        $roles = Apiato::call('Authorization@GetAllRolesAction', [new DataTransporter([]), 0, true, ['lecturers']]);

        return view('lecturers::create', [
            'roles' => $roles,
        ]);
    }

    public function store(CreateLecturersRequest $request)
    {
        \DB::beginTransaction();
        try {
            $transporter = new DataTransporter($request);
            if (isset($request->avatar)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'avatar', 'lecturers', StringLib::getClassNameFromString(Lecturers::class)]);
                if (!$image['error']) {
                    $transporter->avatar = $image['fileName'];
                }
            }
            $transporter->type = 2;
            $transporter->status = 2;
            $lecturers = Apiato::call('Lecturers@StoreNewLecturersAction', [$transporter]);
            if ($lecturers) {
                $transporter->lecturers_id = $lecturers->id;

                $lecturers->toArray();
                \DB::commit();
                return redirect()->back()->with([
                    'flash_level' => 'success',
                    'flash_message' => sprintf('Giảng Viên: %s đã được tạo', $lecturers->fullname),
                    'objectData' => $lecturers,
                    'viewRender' => 'lecturers::inc.item',
                ]);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function delete(DeleteLecturersRequest $request)
    {
        $row = Apiato::call('Lecturers@DeleteLecturersAction', [$request]);
        return $this->sendResponse($row, __('Xóa khách hàng thành công'));
    }

    public function edit(EditLecturersRequest $request)
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Giảng Viên', route('admin.lecturers.index'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Chỉnh sửa');
        $transporter = $request->toTransporter();
        $roles = Apiato::call('Authorization@GetAllRolesAction', [new DataTransporter([]), 0, true, ['lecturers']]);
        $lecturers = Apiato::call('Lecturers@FindLecturersByIdAction', [$transporter->id, []]);

        return view('lecturers::edit', [
            'roles' => $roles,
            'lecturers' => $lecturers,
        ]);
    }

    public function update(UpdateLecturersRequest $request)
    {
        \DB::beginTransaction();
        try {
            $transporter = new DataTransporter($request);
            if (isset($request->avatar)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'avatar', 'lecturers', StringLib::getClassNameFromString(Lecturers::class)]);
                if (!$image['error']) {
                    $transporter->avatar = $image['fileName'];
                }
            }
            $transporter->type = 2;

            $lecturers = Apiato::call('Lecturers@UpdateLecturersAction', [$transporter]);
            if ($lecturers) {
                $transporter->lecturers_id = $lecturers->id;
                \DB::commit();
                $lecturers = $lecturers->toArray();
                return redirect()->back()->with([
                    'flash_level' => 'success',
                    'flash_message' => sprintf('Giảng Viên: %s đã được cập nhật', $lecturers['fullname']),
                    'objectData' => $lecturers,
                    'viewRender' => 'lecturers::inc.item'
                ]);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function filter(FilterLecturersBySeachableRequest $request)
    {
        $transporter = $request->toTransporter();
        $lecturers = Apiato::call('Lecturers@FilterLecturersBySeachableAction', [
            $transporter,
            [],
            [
                'id',
                'fullname',
                'email',
                'phone'
            ]
        ]);
        return $this->sendResponse($lecturers);
    }
} // End class
