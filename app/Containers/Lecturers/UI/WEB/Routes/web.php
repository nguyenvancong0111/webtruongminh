<?php
// Lecturers
Route::group(['prefix' => 'lecturers'
    ], function ($router){
    $router->get('/', [
        'as'   => 'admin.lecturers.index',
        'uses' => 'LecturersController@index',
        'middleware' => [
            'auth:admin'
        ]
    ]);

    $router->get('/create', [
        'as'   => 'admin.lecturers.create',
        'uses' => 'LecturersController@create',
        'middleware' => [
            'auth:admin'
        ]
    ]);


    $router->post('/store', [
        'as'   => 'admin.lecturers.store',
        'uses' => 'LecturersController@store',
        'middleware' => [
            'auth:admin'
        ]
    ]);

    $router->delete('/{id}', [
        'as'   => 'admin.lecturers.delete',
        'uses' => 'LecturersController@delete',
        'middleware' => [
            'auth:admin'
        ]
    ]);

    $router->get('/{id}/edit', [
        'as'   => 'admin.lecturers.edit',
        'uses' => 'LecturersController@edit',
        'middleware' => [
            'auth:admin'
        ]
    ]);

    $router->put('/{id}', [
        'as'   => 'admin.lecturers.update',
        'uses' => 'LecturersController@update',
        'middleware' => [
            'auth:admin'
        ]
    ]);

    $router->post('/render-item', [
        'as'   => 'admin.lecturers.renderItem',
        'uses' => 'LecturersController@renderItem',
        'middleware' => [
            'cors'
        ]
    ]);

    $router->get('/filter', [
        'as' => 'admin.lecturers.filter',
        'uses' => 'LecturersController@filter',
        'middleware' => [
            'auth:admin'
        ]
    ]);
});
