@extends('basecontainer::admin.layouts.default')

@section('content')
    <div class="row" id="sectionContent">
        <div class="col-12">
            <div class="card card-accent-primary">
                <form action="{{ route('admin.lecturers.store') }}" method="POST">
                    @csrf
                    <div class="card-header d-flex">
                        <button class="btn btn-link">Tạo Giảng viên mới</button>
                        <div class="ml-auto">
                            <button type="button" class="btn btn-secondary mr-2" onclick='return closeFrame()'>Đóng lại</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lưu thông tin</button>
                        </div>
                    </div>

                    <div class="card-body">
                        <ul class="nav nav-tabs nav-underline nav-underline-primary">
                            <li class="nav-item">
                                <a class="nav-link active" href="#thong_tin_chung" data-toggle="tab" role="tab"
                                    aria-controls="thong_tin_chung">
                                    Thông tin chung
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="thong_tin_chung" role="tabpanel">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="name">Họ và tên</label>
                                                <input type="text" class="form-control{{ $errors->has('fullname') ? ' is-invalid' : '' }}" id="fullname" name="fullname" value="" />
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="" />
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Số điện thoại </label>
                                                <input type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" id="phone" name="phone" value="" maxlength="11" onkeypress="return shop.numberOnly()" />
                                            </div>

                                            <div class="form-group">
                                                <label for="password">Mật khẩu </label>
                                                <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"  id="password" name="password" />
                                            </div>

                                            <div class="form-group">
                                                <label for="password_confirm">Nhập lại mật khẩu </label>
                                                <input type="password" class="form-control{{ $errors->has('password_confirm') ? ' is-invalid' : '' }}"  id="password_confirm" name="password_confirm" />
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="date_of_birth">Ngày sinh</label>
                                                <input type="text" class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}" id="date_of_birth" name="date_of_birth" value=""  />
                                            </div>
                                            <div class="form-group">
                                                <label for="gender">Giới tính</label>
                                                <div class="d-flex" style="padding: 0.375rem 0.75rem;">
                                                    <div class="form-check m-auto">
                                                        <input class="form-check-input" type="radio" name="gender" id="male" value="1">
                                                        <label class="form-check-label" for="male">
                                                            Male
                                                        </label>
                                                    </div>
                                                    <div class="form-check m-auto">
                                                        <input class="form-check-input" type="radio" name="gender" id="female" value="2">
                                                        <label class="form-check-label" for="female">
                                                            Female
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="address">Địa chỉ </label>
                                                <input type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" name="address" value=""/>
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Chức vụ </label>
                                                <input type="text" class="form-control{{ $errors->has('position') ? ' is-invalid' : '' }}" id="position" name="position" value="{{ old('position') }}" />
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">% Lợi nhuận / Khóa học </label>
                                                <input type="text" class="form-control{{ $errors->has('percent') ? ' is-invalid' : '' }}" id="percent" name="percent" value="{{ old('percent') }}" onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()" />
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="phone">Mô tả ngắn </label>
                                                <textarea name="short_description" id="short_description" class="form-control{{ $errors->has('short_description') ? ' is-invalid' : '' }}"  cols="30" rows="4">{{ old('short_description') }}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="avartar">Avartar</label>
                                                <input type="file" id="avatar" name="avatar" class="dropify form-control {{ $errors->has('avatar') ? ' is-invalid' : '' }}" data-show-remove="false" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">

                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.End thong tin chung -->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
