@extends('basecontainer::admin.layouts.default')
@section('content')
    <div class="row" id="sectionContent">
        <div class="col-12">
            <div class="card mb-0">
                <form action="{{ route('admin.lecturers.update', ['id' => $lecturers->id]) }}" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="card-header d-flex">
                        <button class="btn btn-link">Sửa thông tin Giảng viên: {{ $lecturers->email }}</button>
                        <div class="d-flex ml-auto">
                            <button type="submit" class="btn btn-primary">Lưu thay đổi</button>
                        </div>
                    </div>

                    <div class="card-body">
                        <ul class="nav nav-tabs nav-underline nav-underline-primary">
                            <li class="nav-item">
                                <a class="nav-link active" href="#thong_tin_chung" data-toggle="tab" role="tab"
                                    aria-controls="thong_tin_chung">
                                    Thông tin chung
                                </a>
                            </li>

                             <li class="nav-item">
                                <a class="nav-link" href="#khoa-hoc-tham-gia" data-toggle="tab" role="tab" aria-controls="khoa-hoc-tham-gia">
                                    Khóa học đang tham gia
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#khoa-hoc-tao" data-toggle="tab" role="tab" aria-controls="khoa-hoc-tao">
                                    Khóa học đã tạo
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="thong_tin_chung" role="tabpanel">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="name">Họ và tên</label>
                                                <input type="text" class="form-control{{ $errors->has('fullname') ? ' is-invalid' : '' }}" id="fullname" name="fullname" value="{{ old('fullname', $lecturers->fullname ?? '') }}" />
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" {{ isset($editMode) && $editMode ? 'disabled' : '' }} value="{{ old('email', @$lecturers->email) }}" />
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Số điện thoại </label>
                                                <input type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" id="phone" name="phone" value="{{ old('phone', @$lecturers->phone) }}" required maxlength="11" onkeypress="return shop.numberOnly()" />
                                            </div>

                                            <div class="form-group">
                                                <label for="password">Mật khẩu </label>
                                                <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" {{ isset($editMode) && $editMode ? 'disabled' : '' }} id="password" name="password" />
                                            </div>

                                            <div class="form-group">
                                                <label for="password_confirm">Nhập lại mật khẩu </label>
                                                <input type="password" class="form-control{{ $errors->has('password_confirm') ? ' is-invalid' : '' }}" {{ isset($editMode) && $editMode ? 'disabled' : '' }} id="password_confirm" name="password_confirm" />
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="date_of_birth">Ngày sinh</label>
                                                <input type="text" class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}" id="date_of_birth" name="date_of_birth" value="{{ old('date_of_birth', $lecturers->date_of_birth ?? '') }}"  />
                                            </div>
                                            <div class="form-group">
                                                <label for="gender">Giới tính</label>
                                                <div class="d-flex" style="padding: 0.375rem 0.75rem;">
                                                    <div class="form-check m-auto">
                                                        <input class="form-check-input" type="radio" name="gender" id="male" value="1" {{ old('gender', @$lecturers->gender) == 1 ? 'checked' : '' }}>
                                                        <label class="form-check-label" for="male">
                                                            Male
                                                        </label>
                                                    </div>
                                                    <div class="form-check m-auto">
                                                        <input class="form-check-input" type="radio" name="gender" id="female" value="2" {{ old('gender', @$lecturers->gender) == 2 ? 'checked' : '' }}>
                                                        <label class="form-check-label" for="female">
                                                            Female
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="address">Địa chỉ </label>
                                                <input type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" name="address" value="{{ old('address', @$lecturers->address) }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Chức vụ </label>
                                                <input type="text" class="form-control{{ $errors->has('position') ? ' is-invalid' : '' }}" id="position" name="position" value="{{ old('position', @$lecturers->position) }}" />
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">% Lợi nhuận / Khóa học </label>
                                                <input type="text" class="form-control{{ $errors->has('percent') ? ' is-invalid' : '' }}" id="percent" name="percent" value="{{ old('percent', @$lecturers->percent) }}" onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="phone">Mô tả ngắn </label>
                                                <textarea name="short_description" id="short_description" class="form-control{{ $errors->has('short_description') ? ' is-invalid' : '' }}"  cols="30" rows="4">{{ old('short_description', @$lecturers->short_description) }}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="avartar">Avartar</label>
                                                <input type="file" id="avatar" name="avatar" class="dropify form-control {{ $errors->has('avatar') ? ' is-invalid' : '' }}" data-show-remove="false" data-default-file="{{ \ImageURL::getImageUrl(old('avatar',@$lecturers['avatar']), 'lecturers', 'original')    }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">

                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.End thong tin chung -->

                            <div class="tab-pane" id="khoa-hoc-tham-gia" role="tabpanel"></div>
                            <div class="tab-pane" id="khoa-hoc-tao" role="tabpanel"></div>
                        </div>

                    </div>


                </form>
            </div>
        </div>
    </div>
@endsection
