<?php

namespace App\Containers\Lecturers\UI\API\Transformers\FrontEnd;

use App\Containers\Course\Models\Course;
use App\Ship\Parents\Transformers\Transformer;


class ListLecturersCourseTransformer extends Transformer
{

    public function transform(Course $data)
    {
        $response = [
            'id' => $data->id,
            'image' => $data->getImageUrl('medium'),
            'link' => $data->link(),
            'link_edit' => route('web.lecturers.edit_course_view', ['id' => $data->id]),
            'lesson' => $data->lessons,
            'price' => \FunctionLib::priceFormat($data->price, ' đ'),
            'desc' => $data->relationLoaded('desc') ? $data->desc : $this->null(),
            'status' => $data->status == 2 ? 'Đã kích hoạt' : 'Chờ kiểm duyệt',
            'class' => $data->status == 1 ? 'text-danger' : 'text-success',
            'lecturers' => $data->relationLoaded('lecturers_course') ? $data->lecturers_course : $this->null()
        ];

        return $response;
    }
}
