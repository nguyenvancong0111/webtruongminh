<?php

use Apiato\Core\Foundation\Facades\Apiato;

Route::group(
[
    'middleware' => [
//        'auth:api-customer'
    ],
    'prefix' => '/lecturers-course',
],
function () use ($router) {
    $router->any('/participate', [
        'as' => 'api_list_lecturers_course_participate',
        'uses'       => 'FrontEnd\Controller@listLecturersCourseParticipate'
    ]);
    $router->any('/created', [
        'as' => 'api_list_lecturers_course_created',
        'uses'       => 'FrontEnd\Controller@listLecturersCourseCreated'
    ]);
});