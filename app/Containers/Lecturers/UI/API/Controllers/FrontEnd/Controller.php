<?php

namespace App\Containers\Lecturers\UI\API\Controllers\FrontEnd;

use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use App\Containers\Lecturers\UI\API\Controllers\FrontEnd\Features\ListLecturersCourseCreated;
use App\Containers\Lecturers\UI\API\Controllers\FrontEnd\Features\ListLecturersCourseParticipate;

class Controller extends BaseApiFrontController
{
    use ListLecturersCourseParticipate,
        ListLecturersCourseCreated;
}
