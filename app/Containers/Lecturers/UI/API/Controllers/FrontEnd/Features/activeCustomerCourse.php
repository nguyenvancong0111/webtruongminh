<?php

namespace App\Containers\Customer\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Customer\Actions\FrontEnd\ActiveCustomerCourseAction;
use App\Containers\Customer\Actions\FrontEnd\GetAllCustomerCourseAction;
use App\Containers\Customer\UI\API\Requests\FrontEnd\ActiveCustomerCourseRequest;
use App\Containers\Customer\UI\API\Transformers\FrontEnd\ListCustomerCourseTransformer;

trait activeCustomerCourse
{
    public function activeCustomerCourse(ActiveCustomerCourseRequest $request)
    {
        if(!empty($request->code)){
            $customer_course = app(ActiveCustomerCourseAction::class)->skipCache(true)->run(
                ['id' => (int)$request->cusCID, 'customer_id' => (int)$request->id, 'course_id' => (int)$request->course_id, 'active_code' => $request->code],
                ['is_active' => 1, 'start_time' => date(now())]
            );
            if($customer_course == null){
                return \FunctionLib::ajaxRespondV2(false, 'error', ['mess' => 'Mã kích hoạt không đúng. Vui lòng thử lại']);
            }

            return $this->transform($customer_course, new ListCustomerCourseTransformer());
        }

        return \FunctionLib::ajaxRespondV2(false, 'error', ['mess' => 'Mã kích hoạt không được để trống']);
    }
}
