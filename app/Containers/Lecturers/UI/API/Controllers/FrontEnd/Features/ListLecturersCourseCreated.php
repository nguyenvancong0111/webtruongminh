<?php

namespace App\Containers\Lecturers\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Lecturers\Actions\FrontEnd\GetAllLecturersCourseAction;
use App\Containers\Lecturers\UI\API\Requests\FrontEnd\ListLecturersCourseRequest;
use App\Containers\Lecturers\UI\API\Transformers\FrontEnd\ListLecturersCourseTransformer;

trait ListLecturersCourseCreated
{
    public function listLecturersCourseCreated(ListLecturersCourseRequest $request)
    {
        $lecturers_course_created = app(GetAllLecturersCourseAction::class)->skipCache(true)->run(
            ['lecturers' => $request->id, 'auth_type' => 1  ], false, 9, isset($request->page) && !empty($request->page) ? $request->page : 1
        );
        $html_paginate = '';
        if ($lecturers_course_created->total() > 0){
            $html_paginate = \View('ubacademy::pc.lecturers.components.paginate_render', ['data' => $lecturers_course_created, 'dom' => 2])->render();
        }
        return $this->transform($lecturers_course_created, new ListLecturersCourseTransformer(), [], ['paginate_html' => $html_paginate]);
    }
}
