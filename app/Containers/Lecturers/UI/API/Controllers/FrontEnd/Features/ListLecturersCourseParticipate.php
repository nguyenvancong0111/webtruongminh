<?php

namespace App\Containers\Lecturers\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Lecturers\Actions\FrontEnd\GetAllLecturersCourseAction;
use App\Containers\Lecturers\UI\API\Requests\FrontEnd\ListLecturersCourseRequest;
use App\Containers\Lecturers\UI\API\Transformers\FrontEnd\ListLecturersCourseTransformer;

trait ListLecturersCourseParticipate
{
    public function listLecturersCourseParticipate(ListLecturersCourseRequest $request)
    {
        $lecturers_course_participate = app(GetAllLecturersCourseAction::class)->skipCache(true)->run(
            ['lecturers' => $request->id, 'lecturers_create' => 0, 'status' => 2], false, 5, isset($request->page) && !empty($request->page) ? $request->page : 1
        );

        $html_paginate = '';
        if ($lecturers_course_participate->total() > 0){
            $html_paginate = \View('ubacademy::pc.lecturers.components.paginate_render', ['data' => $lecturers_course_participate, 'dom' => 1])->render();
        }

        return $this->transform($lecturers_course_participate, new ListLecturersCourseTransformer(), [], ['paginate_html' => $html_paginate]);
    }
}
