<?php
/**
 * Created by PhpStorm.
 * Filename: Filter.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 8/12/20
 * Time: 13:59
 */

namespace App\Containers\Filter\Models;


use App\Ship\Parents\Models\Model;

class Filter extends Model
{
    protected $table = 'filter';

    protected $primaryKey = 'filter_id';

    protected $fillable = [
        'filter_group_id',
        'image',
        'sort_order',
    ];

    public function desc()
    {
        return $this->hasOne(FilterDesc::class,'filter_id','filter_id');
    }

    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id',FilterDesc::class,'filter_id','filter_id');
    }

    public function filterGroup()
    {
        return $this->hasOne(FilterGroup::class,'filter_group_id','filter_group_id');
    }
}