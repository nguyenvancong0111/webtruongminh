<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:13:31
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

class FilterListingAction extends Action
{
    public function run($filter_group_id)
    {
        $data = Apiato::call(
            'Filter@FilterListingTask',
            [
                $filter_group_id
            ]
        );

        return $data;
    }
}
