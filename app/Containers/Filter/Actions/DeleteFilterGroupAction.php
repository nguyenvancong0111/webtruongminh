<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-12 12:09:53
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
class DeleteFilterGroupAction extends Action
{
    public function run($data)
    {
        Apiato::call('Filter@DeleteFilterGroupTask',[$data->id]);
        Apiato::call('Filter@DeleteFilterGroupDescTask', [$data->id]);
        $this->clearCache();
    }
}
