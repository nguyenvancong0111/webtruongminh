<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-12 12:10:04
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

class UpdateFilterGroupAction extends Action
{

    public function run($data)
    {
        $filterGroup = Apiato::call(
            'Filter@UpdateFilterGroupTask',
            [
                $data
            ]
        );

        if ($filterGroup) {
            $original_desc = Apiato::call('Filter@GetAllFilterGroupDescTask', [$filterGroup->filter_group_id]);
            Apiato::call('Filter@SaveFilterGroupDescTask', [
                $data,
                $original_desc,
                $filterGroup->filter_group_id
            ]);
            Apiato::call('Filter@SaveFilterTask', [
                $data->filter_value,
                $filterGroup->filter_group_id
            ]);
        }

        $this->clearCache();

        return $filterGroup;
    }
}
