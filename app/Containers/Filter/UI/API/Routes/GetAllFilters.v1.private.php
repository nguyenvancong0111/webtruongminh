<?php

$router->get('filter/getFilters', [
    'as' => 'api_filter_get_all',
    'uses'       => 'Controller@getFilters',
    'middleware' => [
        'auth:api',
    ],
]);