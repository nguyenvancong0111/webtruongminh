<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:19:15
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\UI\API\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Filter\UI\API\Requests\GetAllFilterGroupsRequest;
use App\Containers\Filter\UI\API\Transformers\FilterGroupsSelect2Transformer;
use App\Containers\Filter\UI\API\Transformers\FilterGroupsTransformer;
use App\Containers\Filter\UI\API\Transformers\FiltersSelect2Transformer;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Transporters\DataTransporter;

class Controller extends ApiController
{
    public function getFilterGroups(GetAllFilterGroupsRequest $request)
    {
        $transporter = new DataTransporter([
            'name' => $request->filter_group_name
        ]);
        $filterGroups = Apiato::call('Filter@FilterGroupListingAction', [$transporter]);
        $type = $request->type;
        if ($type && $type == 'select2') {
            return $this->transform($filterGroups, FilterGroupsSelect2Transformer::class, [], [], 'filtergroups');
        } else {
            return $this->transform($filterGroups, FilterGroupsTransformer::class, [], [], 'filtergroups');
        }
    }

    public function getFilters(GetAllFilterGroupsRequest $request)
    {
        $filters = Apiato::call('Filter@GetFilterByNameAction', [$request->name,10]);
        // return $filters;
        $type = $request->type;
        // if ($type && $type == 'select2') {
            return $this->transform(collect($filters), FiltersSelect2Transformer::class, [], [], 'filters');
        // } else {
        //     return $this->transform(collect($filters), FilterGroupsTransformer::class, [], [], 'filters');
        // }
    }
}
