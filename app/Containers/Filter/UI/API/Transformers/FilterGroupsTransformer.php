<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-20 03:49:45
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\UI\API\Transformers;

use App\Ship\Parents\Transformers\Transformer;
use Illuminate\Support\Facades\Config;

/**
 * Class FilterGroupsTransformer.
 *
 */
class FilterGroupsTransformer extends Transformer
{

    /**
     * @param $token
     *
     * @return  array
     */
    public function transform($filterGroup)
    {
        $response = [
            'filter_group_id' => $filterGroup->filter_group_id,
            'name' =>   $filterGroup->desc->name,
            'short_description' =>   $filterGroup->desc->short_description,
            'sort_order' => $filterGroup->sort_order,
            'created_at' => $filterGroup->created_at,
            'language_id' => $filterGroup->desc->language_id
        ];

        return $response;
    }
}
