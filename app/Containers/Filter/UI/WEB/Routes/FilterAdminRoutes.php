<?php
Route::group(
    [
        'prefix' => 'filters',
        'namespace' => '\App\Containers\Filter\UI\WEB\Controllers\Admin',
        'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
        'middleware' => [
            'auth:admin',
        ],
    ],
    function () use ($router) {
        $router->get('/', [
            'as'   => 'admin_filter_home_page',
            'uses' => 'Controller@index',
        ]);

        $router->get('/edit/{id}', [
            'as'   => 'admin_filters_edit_page',
            'uses' => 'Controller@edit',
        ]);

        $router->post('/edit/{id}', [
            'as'   => 'admin_filter_edit_page',
            'uses' => 'Controller@update',
        ]);

        $router->get('/add', [
            'as'   => 'admin_filter_add_page',
            'uses' => 'Controller@add',
        ]);

        $router->post('/add', [
            'as'   => 'admin_filter_add_page',
            'uses' => 'Controller@create',
        ]);

        $router->delete('/{id}', [
            'as' => 'admin_filter_delete',
            'uses'       => 'Controller@deleteFilter',
        ]);
    }
);
