<?php

namespace App\Containers\Filter\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class FilterGroupDescRepository.
 */
class FilterGroupDescRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Filter';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'filter_group_id',
        'language_id',
        'name',
    ];
}
