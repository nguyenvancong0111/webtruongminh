<?php

namespace App\Containers\Filter\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class FilterRepository.
 */
class FilterRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Filter';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'filter_id',
        'filter_group_id',
        'status',
        'created_at',
        'desc.name',
        'desc.language_id'
    ];
}
