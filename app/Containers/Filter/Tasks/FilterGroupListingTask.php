<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-08 09:58:41
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:15:43
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Tasks;

use App\Containers\Filter\Data\Repositories\FilterGroupRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class FilterGroupListingTask extends Task
{

    protected $repository;

    public function __construct(FilterGroupRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($filters = [], $orderBy = ['sort_order' => 'DESC', 'filter_group_id' =>  'DESC'], $defaultLanguage = null, $skipPagination = false, $limit = 10)
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;
        $this->repository->with(['desc' => function ($query) use ($language_id) {
            $query->activeLang($language_id);
        }, 'filter' => function ($query) use ($language_id) {
            $query->with('desc');
        }
        ]);

        if (isset($filters['name']) && $filters['name']) {
            $this->repository->whereHas('desc', function (Builder $query) use ($filters, $language_id) {
                $query->where('name', 'like', '%' . $filters['name'] . '%');
                $query->where('language_id', $language_id);
            });
        }
        foreach($orderBy as $column => $direction) {
            $this->repository->orderBy($column,$direction);
        }
        return !$skipPagination ? $this->repository->paginate($limit) : $this->repository->get();
    }
}
