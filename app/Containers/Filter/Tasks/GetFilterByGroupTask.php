<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:17:06
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Tasks;

use App\Containers\Filter\Data\Repositories\FilterGroupRepository;
use App\Ship\Parents\Tasks\Task;


class GetFilterByGroupTask extends Task
{

    protected $repository;

    public function __construct(FilterGroupRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($defaultLanguage = null,$language_id = '', $orderBy = ['sort_order' => 'ASC', 'filter_group_id' => 'DESC'])
    {
        $language_id = $language_id != '' ? $language_id : ($defaultLanguage ? $defaultLanguage->language_id : 1);

        $external_data['with_relationship'] = [
            'filter' => function ($qr) {
                $qr->select('filter_id', 'filter_group_id', 'sort_order');
            }, 'filter.desc' => function ($query) use ($language_id) {
                $query->select('filter_id', 'name');
                $query->activeLang($language_id);
            }
        ];
        $this->repository->with(array_merge(['desc' => function ($query) use ($language_id) {
            $query->activeLang($language_id);
        }], isset($external_data['with_relationship']) ? $external_data['with_relationship'] : []));

        foreach($orderBy as $column => $direction) {
            $this->repository->orderBy($column,$direction);
        }

        return $this->repository->all();
    }
}
