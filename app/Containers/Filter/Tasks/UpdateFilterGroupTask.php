<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-18 11:51:48
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Tasks;

use App\Containers\Filter\Data\Repositories\FilterGroupRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;


class UpdateFilterGroupTask extends Task
{

    protected $repository;

    public function __construct(FilterGroupRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data->toArray(), ['name', 'filter_value', '_token', '_headers']);

            $dataUpdate['min'] = intval(str_replace([',', '.'], '', $dataUpdate['min']));
            $dataUpdate['max'] = intval(str_replace([',', '.'], '', $dataUpdate['max']));

            $cate = $this->repository->update($dataUpdate, $data->id);

            return $cate;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
