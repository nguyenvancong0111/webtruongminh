<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 11:43:03
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Tasks;

use App\Containers\Filter\Data\Repositories\FilterGroupDescRepository;
use App\Ship\Parents\Tasks\Task;

class SaveFilterGroupDescTask extends Task
{

    protected $repository;

    public function __construct(FilterGroupDescRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data, $original_desc, $filter_group_id, $edit_id = null)
    {
        $description = isset($data['description']) ? (array)$data['description'] : null;
        if (is_array($description) && !empty($description)) {
            $updates = [];
            $inserts = [];
            $id_after_insert = 0;

            foreach ($description as $k => $v) {
                if (isset($original_desc[$k])) {
                    $updates['data'][$original_desc[$k]['filter_group_id'] . '_' . $k] = [
                        'name' => $v['name'],
                        'short_description' => $v['short_description'],
                    ];

                    $updates['conds'][$original_desc[$k]['filter_group_id'] . '_' . $k] = ['filter_group_id' => $original_desc[$k]['filter_group_id'], 'language_id' => $k];
                } else {
                    $inserts[] = [
                        'language_id' => $k,
                        'filter_group_id' => $filter_group_id,
                        'name' => $v['name'],
                        'short_description' => $v['short_description'],
                    ];
                }
            }
            if (!empty($updates)) {
                $this->repository->updateMultipleConditions($updates);
            }

            if (!empty($inserts)) {
                $this->repository->getModel()->insert($inserts);
            }
        }
    }
}
