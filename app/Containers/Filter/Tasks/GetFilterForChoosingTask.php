<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:17:26
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Tasks;

use App\Containers\Filter\Data\Repositories\FilterDescRepository;
use App\Containers\Filter\Data\Repositories\FilterGroupDescRepository;
use App\Containers\Filter\Data\Repositories\FilterRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Facades\DB;


class GetFilterForChoosingTask extends Task
{
    public $filterGroupDescRepository, $filterDescRepository;
    public function __construct(
        FilterRepository $repository, 
        FilterGroupDescRepository $filterGroupDescRepository,
        FilterDescRepository $filterDescRepository
    ){
        $this->repository = $repository;
        $this->filterGroupDescRepository = $filterGroupDescRepository;
        $this->filterDescRepository = $filterDescRepository;
    }

    public function run($language_id = '',$filters = [], $conds = [], $limit = 20, $defaultLanguage = null)
    {
        $language_id = $language_id != '' ? $language_id : ($defaultLanguage ? $defaultLanguage->language_id : 1);

        if (isset($filters['name']) && !empty($filters['name'])) {
            $conds[] = ['fd.name', 'LIKE', "%" . $filters['name'] . "%"];
        }
        $conds[] = ['fd.language_id', '=', $language_id];

        $wery = DB::table($this->repository->getModel()->getTable(), 'f');
        $wery->select('*');
        $wery->addSelect([
            'group' => DB::table($this->filterGroupDescRepository->getModel()->getTable(), 'fgd')
                ->select('fgd.name')
                ->whereRaw('f.filter_group_id = fgd.filter_group_id')
                ->where('fgd.language_id', $language_id)
        ]);
        $wery->leftJoin($this->filterDescRepository->getModel()->getTable() . ' as fd', 'f.filter_id', '=', 'fd.filter_id');


        //        $condOr[] = [['status', '=', 4],['status', '=', 2]];
        if (isset($condOr) && !empty($condOr)) {
            foreach ($condOr as $itm) {
                $wery->where(function ($q) use ($itm) {
                    $first = true;
                    foreach ($itm as $eachCond) {
                        if ($first) {
                            $q->where([$eachCond]);
                            $first = false;
                        } else {
                            $q->orWhere([$eachCond]);
                        }
                    }
                });
            }
        }

        $wery->where($conds);

        // DB::enableQueryLog();
        // $wery->get();
        // dd(DB::getQueryLog());

        if ($limit > 0) {
            return $wery->limit($limit)->get();
        } else {
            return $wery->get();
        }
    }
}
