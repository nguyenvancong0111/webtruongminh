<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-12 11:07:13
 * @ Description: Happy Coding!
 */


namespace App\Containers\Filter\Tasks;

use Apiato\Core\Foundation\StringLib;
use App\Containers\Filter\Data\Repositories\FilterDescRepository;
use App\Containers\Filter\Data\Repositories\FilterRepository;
use App\Ship\Parents\Tasks\Task;

class SaveFilterTask extends Task
{

    protected $repository, $filterDescRepository;

    public function __construct(FilterRepository $repository, FilterDescRepository $filterDescRepository)
    {
        $this->repository = $repository;
        $this->filterDescRepository = $filterDescRepository;
    }

    public function run($filter_values, $filter_group_id)
    {
        if (isset($filter_values['filter_id']) && !empty($filter_values)) {
            $updates = [];
            $inserts = [];

            $update_desc = [];
            $insert_desc = [];

            foreach ($filter_values['filter_id'] as $k_id => $it_id) {
                if ($it_id  && !StringLib::startsWith($it_id, '_')) {
                    //                    echo $it_id. ' | ';
                    $arr_not_delete[] = $it_id;
                    $updates[$it_id] = [
                        'image' => @$filter_values['image'][$k_id] ?? '',
                        'sort_order' => @$filter_values['sort_order'][$k_id] ?? 0,
                        'value_range' => @$filter_values['value_range'][$k_id] ?? 0,
                    ];

                    if (!empty($filter_values['filter_description'])) {
                        $reverse = true;
                        foreach ($filter_values['filter_description'] as $k_v_lang => &$it_v_desc) {
                            $fkc_key = array_key_first($it_v_desc);
                            $it_v_desc = array_reverse($it_v_desc, true);
                            $fuck = array_pop($it_v_desc);
                            $it_v_desc = array_reverse($it_v_desc, true);
                            //                            $reverse = !$reverse;

                            if (!StringLib::startsWith($fkc_key, '_')) {

                                $update_desc['data'][$it_id . '_' . $k_v_lang] = [
                                    'name' => $fuck,
                                ];

                                $update_desc['conds'][$it_id . '_' . $k_v_lang] = ['filter_id' => $it_id, 'filter_group_id' => $filter_group_id, 'language_id' => $k_v_lang];
                            } else {
                                $insert_desc[] = ['filter_id' => $it_id, 'filter_group_id' => $filter_group_id, 'language_id' => $k_v_lang, 'name' => $fuck/*, 'sort_order' => @$filter_values['sort_order'][$k_id] ?? 0*/];
                            }
                        }
                    }
                } else {
                    $inserts = [
                        'filter_group_id' => $filter_group_id,
                        'image' => @$filter_values['image'][$k_id] ?? '',
                        'sort_order' => @$filter_values['sort_order'][$k_id] ?? 0,
                        'value_range' => @$filter_values['value_range'][$k_id] ?? ''
                    ];
                    $id_return = $this->repository->getModel()->insertGetId($inserts);

                    if ($id_return) {
                        $arr_not_delete[] = $id_return;
                        $insert_new_desc = [];
                        foreach ($filter_values['filter_description'] as $k_v_lang => &$it_v_desc) {
                            $fkc_key = array_key_first($it_v_desc);
                            $it_v_desc = array_reverse($it_v_desc, true);
                            $fuck = array_pop($it_v_desc);
                            $it_v_desc = array_reverse($it_v_desc, true);

                            $insert_new_desc[] = ['filter_id' => $id_return, 'filter_group_id' => $filter_group_id, 'language_id' => $k_v_lang, 'name' => $fuck];
                        }

                        $this->filterDescRepository->getModel()->insert($insert_new_desc);
                    }
                }
            }

            if (!empty($updates)) {
                $this->repository->updateMultiple($updates, 'filter_id');
            }

            if (!empty($update_desc)) {
                $this->filterDescRepository->updateMultipleConditions($update_desc);
            }

            if (!empty($insert_desc)) {
                $this->filterDescRepository->getModel()->insert($insert_desc);
            }

            if (isset($arr_not_delete) && !empty($arr_not_delete)) {
                $this->repository->getModel()->where('filter_group_id', $filter_group_id)->whereNotIn('filter_id', $arr_not_delete)->delete();
                $this->filterDescRepository->getModel()->where('filter_group_id', $filter_group_id)->whereNotIn('filter_id', $arr_not_delete)->delete();
            }
        }
    }
}
