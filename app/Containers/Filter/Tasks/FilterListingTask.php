<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:16:05
 * @ Description: Happy Coding!
 */


namespace App\Containers\Filter\Tasks;

use App\Containers\Filter\Data\Repositories\FilterRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

class FilterListingTask extends Task
{

    protected $repository;

    public function __construct(FilterRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($filter_group_Id)
    {
        $this->repository->with(['all_desc']);

        $this->repository->pushCriteria(new ThisEqualThatCriteria('filter_group_id', $filter_group_Id));

        $this->repository->orderBy('sort_order');

        return $this->repository->all();
    }
}
