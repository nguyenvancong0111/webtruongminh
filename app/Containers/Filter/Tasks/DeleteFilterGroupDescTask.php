<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:15:01
 * @ Description: Happy Coding!
 */


namespace App\Containers\Filter\Tasks;

use App\Containers\Filter\Data\Repositories\FilterGroupDescRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteFilterGroupDescTask extends Task
{

    protected $repository;

    public function __construct(FilterGroupDescRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($filter_group_id)
    {
        try {
            $this->repository->getModel()->where('filter_group_id', $filter_group_id)->delete();
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
