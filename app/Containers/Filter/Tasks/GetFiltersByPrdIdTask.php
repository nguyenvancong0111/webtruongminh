<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:18:05
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Tasks;

use App\Containers\Filter\Data\Repositories\FilterDescRepository;
use App\Containers\Filter\Data\Repositories\FilterGroupDescRepository;
use App\Containers\Filter\Data\Repositories\FilterRepository;
use App\Containers\Product\Data\Repositories\ProductFilterRepository;
use App\Ship\Criterias\Eloquent\ThisInThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Facades\DB;

class GetFiltersByPrdIdTask extends Task
{

    protected $repository, $filterDescRepository, $filterGroupDescRepository, $productFilterRepository;

    public function __construct(
        FilterRepository $repository,
        FilterDescRepository $filterDescRepository,
        FilterGroupDescRepository $filterGroupDescRepository,
        ProductFilterRepository $productFilterRepository
    ) {
        $this->repository = $repository;
        $this->filterDescRepository = $filterDescRepository;
        $this->filterGroupDescRepository = $filterGroupDescRepository;
        $this->productFilterRepository = $productFilterRepository;
    }

    public function run($prd_id, $conds = [], $defaultLanguage = null)
    {
        $this->currentLang = $defaultLanguage ? $defaultLanguage->language_id : 1;

        $conds[] = ['fd.language_id', '=', $this->currentLang];
        $conds[] = ['pf.product_id', '=', $prd_id];

        $wery = DB::table($this->repository->getModel()->getTable(), 'f');
        $wery->select('*');
        $wery->addSelect([
            'group' => DB::table($this->filterGroupDescRepository->getModel()->getTable(), 'fgd')
                ->select('fgd.name')
                ->whereRaw('f.filter_group_id = fgd.filter_group_id')
                ->where('fgd.language_id', $this->currentLang)
        ]);
        $wery->leftJoin($this->filterDescRepository->getModel()->getTable() . ' as fd', 'f.filter_id', '=', 'fd.filter_id');
        $wery->join($this->productFilterRepository->getModel()->getTable() . ' as pf', 'f.filter_id', '=', 'pf.filter_id');

        $wery->where($conds);

        return $wery->get();
    }
}
