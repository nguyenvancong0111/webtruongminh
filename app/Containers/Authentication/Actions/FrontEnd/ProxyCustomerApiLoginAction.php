<?php

namespace App\Containers\Authentication\Actions\FrontEnd;

use App\Containers\Authentication\Tasks\CallOAuthServerTask;
use App\Containers\Authentication\Tasks\ExtractLoginCustomAttributeTask;
use App\Containers\Authentication\Tasks\MakeRefreshCookieTask;
use App\Ship\Parents\Actions\Action;

class ProxyCustomerApiLoginAction extends Action
{
    public function run(array $data): array
    {
        $loginCustomAttribute = app(ExtractLoginCustomAttributeTask::class)->run($data);

        $requestData = [
            'username' => $loginCustomAttribute['username'],
            'password' => $data['password'],
            'grant_type' => $data['grant_type'] ?? 'password',
            'client_id' => $data['client_id'],
            'client_secret' => $data['client_password'],
            'scope' => $data['scope'] ?? '',
        ];

        $responseContent = app(CallOAuthServerTask::class)->run($requestData);
        //dd($responseContent);

        // check if user email is confirmed only if that feature is enabled.
        /*Apiato::call('Authentication@CheckIfCustomerApiIsConfirmedTask', [],
            [['loginWithCredentials' => [
                $requestData['username'], $requestData['password'], $loginCustomAttribute['loginAttribute']]]
            ]);*/

        $refreshCookie = app(MakeRefreshCookieTask::class)->run($responseContent['refresh_token']);

        return [
            'response_content' => $responseContent,
            'refresh_cookie' => $refreshCookie,
            // 'access_token_cookie' => $accessTokenCookie
        ];
    }
}
