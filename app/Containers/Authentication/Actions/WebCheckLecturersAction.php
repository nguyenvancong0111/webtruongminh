<?php

namespace App\Containers\Authentication\Actions;

use App\Containers\Authentication\Tasks\WebCheckCustomerTask;
use App\Containers\Authentication\Tasks\WebCheckLecturersTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

class WebCheckLecturersAction extends Action
{
    public function run(DataTransporter $data)
    {
        $user = app(WebCheckLecturersTask::class)->run($data->username, $data->password, $data->rememberLogin);
        return $user;
    }
}
