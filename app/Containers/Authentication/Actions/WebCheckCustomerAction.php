<?php

namespace App\Containers\Authentication\Actions;

use App\Containers\Authentication\Tasks\WebCheckCustomerTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

class WebCheckCustomerAction extends Action
{
    public function run(DataTransporter $data)
    {
        $user = app(WebCheckCustomerTask::class)->run($data->username, $data->password, $data->type, $data->rememberLogin);
        return $user;
    }
}
