<?php

namespace App\Containers\Authentication\Tasks;

use App\Containers\Customer\Data\Repositories\CustomerRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Containers\Authentication\Exceptions\LoginFailedException;
use Illuminate\Support\Facades\Hash;

class WebCheckCustomerTask extends Task
{
    public function run(string $username, string $password, int $type, $remember = true)
    {
        $fieldName = filter_var( $username, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
        if (!$user = auth()->guard(config('auth.guard_for.frontend'))->attempt(['type' => $type, 'status' => 2, $fieldName => $username, 'password' => $password], $remember)) {
            return false;
        }

        return auth()->guard(config('auth.guard_for.frontend'))->user();


    }
}
