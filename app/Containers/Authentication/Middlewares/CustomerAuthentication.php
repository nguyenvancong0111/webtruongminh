<?php

namespace App\Containers\Authentication\Middlewares;


use App\Ship\Parents\Middlewares\Middleware;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class WebAuthentication
 *
 * @author  Ha Ly Manh  <halymanh@vccorp.com>
 */
class CustomerAuthentication extends Middleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth('customer')->check()) {
            return $next($request);
        } else {
            return redirect()->back()->with([
                'flash_title' => 'Thông báo',
                'flash_message' => 'Bạn cần đăng nhập để sử dụng chức năng này',
                'flash_level' => 'warning'
            ]);;
        }
    }
}
