<?php

/** @var Route $router */
$router->group([
    'domain' => parse_url(config('app.url'))['host'],
    'namespace' => '\App\Containers\Authentication\UI\API\Controllers\FrontEnd',
    'prefix' => 'customer'

], function () use ($router) {

    $router->post('/login', 'CustomerApiAuthController@customerApiLogin')->name('api.customer.login');

    $router->post('/refresh-token', 'CustomerApiAuthController@customerApiRefreshToken')->name('api.customer.refresh-token');

    $router->post('/logout', 'CustomerApiAuthController@customerApiLogout')->name('api_authentication_logout_customer');


    $router->get('/info', 'CustomerApiAuthController@customerApiInfo')->name('api.customer.info')->middleware(['auth:api-customer']);

});
