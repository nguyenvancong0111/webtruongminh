<?php

namespace App\Containers\Authentication\UI\API\Requests\FrontEnd;

use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Ship\Parents\Requests\ApiRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class CustomerApiRefreshRequest extends ApiRequest
{
    use ApiResTrait;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => null,
        'roles' => null,
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [

    ];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [

    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refresh_token' => 'required|string|max:1000',

        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->sendError('validation', Response::HTTP_UNPROCESSABLE_ENTITY, $validator->errors()->first()));
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
