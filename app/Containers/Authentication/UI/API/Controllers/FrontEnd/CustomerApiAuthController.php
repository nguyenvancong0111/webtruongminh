<?php

namespace App\Containers\Authentication\UI\API\Controllers\FrontEnd;

use App\Containers\Authentication\Actions\ApiLogoutAction;
use App\Containers\Authentication\Actions\ProxyApiRefreshAction;
use App\Containers\Authentication\Actions\FrontEnd\ProxyCustomerApiLoginAction;
use App\Containers\Authentication\Data\Transporters\ProxyRefreshTransporter;
use App\Containers\Authentication\UI\API\Requests\FrontEnd\CustomerApiLoginRequest;
use App\Containers\Authentication\UI\API\Requests\FrontEnd\CustomerApiRefreshRequest;
use App\Containers\Authentication\UI\API\Requests\LogoutCustomerRequest;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;

class CustomerApiAuthController extends ApiController
{
    public function customerApiLogin(CustomerApiLoginRequest $request)
    {
        $dataLogin = array_merge($request->all(), [
            'client_id' => Config::get('authentication-container.clients.web.customer.id'),
            'client_password' => Config::get('authentication-container.clients.web.customer.secret')
        ]);

        $result = app(ProxyCustomerApiLoginAction::class)->run($dataLogin);

        return $this->json([
            'success' => true,
            'message' => 'Đăng nhập thành công!',
            'code' => Response::HTTP_OK,
            'data' => $result['response_content'],
        ], Response::HTTP_OK)->withCookie($result['refresh_cookie']);
    }

    public function customerApiRefreshToken(CustomerApiRefreshRequest $request)
    {
        $dataTransporter = new ProxyRefreshTransporter(
            array_merge($request->all(), [
                'client_id' => Config::get('authentication-container.clients.web.customer.id'),
                'client_password' => Config::get('authentication-container.clients.web.customer.secret'),
                // use the refresh token sent in request data, if not exist try to get it from the cookie
                'refresh_token' => $request->refresh_token ?: $request->cookie('refreshToken'),
            ])
        );

        $result = app(ProxyApiRefreshAction::class)->run($dataTransporter);
        //dd($dataTransporter->toArray(), $result);

        return $this->json([
            'success' => true,
            'message' => 'Refresh token thành công!',
            'code' => Response::HTTP_OK,
            'data' => $result['response-content'],
        ], Response::HTTP_OK)->withCookie($result['refresh-cookie']);
    }

    public function customerApiInfo()
    {
        $authCustomerApiUser = auth()->guard(config('auth.guard_for.api-customer'))->user();
        $code = empty($authCustomerApiUser) ? Response::HTTP_UNAUTHORIZED : Response::HTTP_OK;

        return $this->json([
            'success' => empty($authCustomerApiUser) ? false : true,
            'message' => empty($authCustomerApiUser) ? 'Không tìm thấy!' : 'Thông tin Customer API!',
            'code' => $code,
            'data' => $authCustomerApiUser ?? [],
        ], $code);
    }

    public function customerApiLogout(LogoutCustomerRequest $request)
    {
        $dataTransporter = new DataTransporter($request);
        $dataTransporter->bearerToken = $request->bearerToken();

        app(ApiLogoutAction::class)->run($dataTransporter);

        return $this->accepted([
            'success' => true,
            'message' => 'Đăng xuất thành công!',
            'code' => 202,
            'data' => [],
        ])->withCookie(Cookie::forget('refreshToken'));
    }
}
