<?php
$router->get('/', [
    'as'   => 'get_admin_home_page',
    'uses'       => '\App\Containers\DashBoard\UI\WEB\Controllers\Admin\Controller@viewDashboardPage',
    'domain' => 'admin.'. parse_url(\Config::get('app.url'))['host'],
    'middleware' => [
        'auth:admin'
    ],
]);
