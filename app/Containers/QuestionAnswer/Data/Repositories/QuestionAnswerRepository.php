<?php

namespace App\Containers\QuestionAnswer\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class QuestionAnwserRepository.
 */
class QuestionAnswerRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'QuestionAnswer';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
    ];
}
