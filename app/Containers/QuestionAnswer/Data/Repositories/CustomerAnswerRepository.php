<?php

namespace App\Containers\QuestionAnswer\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class CustomerAnwserRepository.
 */
class CustomerAnswerRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'QuestionAnswer';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'question_id',
        'user_type',
        'user_id',
        'type',
        'answer',
        'status',
        'created_at',
        'update_at',
    ];
}
