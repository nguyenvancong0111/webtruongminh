<?php

namespace App\Containers\QuestionAnswer\UI\API\Controllers;

use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use App\Containers\QuestionAnswer\Actions\FrontEnd\CreateCustomerAnswerAction;
use App\Containers\QuestionAnswer\Actions\FrontEnd\GetAllQuestionAnswerByCourseIdAction;
use App\Containers\QuestionAnswer\UI\API\Requests\GetQuestionAnswerByCourseRequest;
use App\Containers\QuestionAnswer\UI\API\Requests\UpdateAnswerRequest;
use App\Containers\QuestionAnswer\UI\API\Transformers\CalculatorQuestionAnswerCourseTransformer;
use App\Containers\QuestionAnswer\UI\API\Transformers\LecturersAnswerCourseTransformer;
use App\Containers\QuestionAnswer\UI\API\Transformers\QuestionAnswerCourseTransformer;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Response;

/**
 * Class Controller.
 */
class Controller extends BaseApiFrontController
{
    public function getListQuestionAnswer(GetQuestionAnswerByCourseRequest $request){
        $question = app(GetAllQuestionAnswerByCourseIdAction::class)->skipCache(true)->run((int)$request->type, (int)$request->object, ['customer:id,fullname,avatar', 'answers', 'answers.lecturers:id,fullname,avatar', 'answers.customer:id,fullname,avatar'], true, 5, isset($request->page) && !empty($request->page) ? $request->page : 1);
        return $this->transform($question, new QuestionAnswerCourseTransformer());
    }

    public  function createAnswer(UpdateAnswerRequest $request){
        $answer = app(CreateCustomerAnswerAction::class)->skipCache()->run(
            [
                'object' => (int)$request->object,
                'question_id' => (int)$request->id,
                'user_type' => (int)$request->user_type,
                'user_id' => (int)$request->authID,
                'type' => (int)$request->type,
                'answer' => $request->answer,
                'status' => 2
            ]
        );
        if ($request->user_type == 1){
            return $this->transform($answer, new LecturersAnswerCourseTransformer());
        }
    }
}

