<?php

Route::group(
    [
        'middleware' => [
            'api',
        ],
        'prefix' => '/answer',
    ],
    function () use ($router) {
        $router->post('/', [
            'as' => 'api_post_answer_course',
            'uses' => 'Controller@createAnswer'
        ]);

    });