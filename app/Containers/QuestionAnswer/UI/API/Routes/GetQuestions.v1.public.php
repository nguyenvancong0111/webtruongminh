<?php

Route::group(
    [
        'middleware' => [
            'api',
        ],
        'prefix' => '/questions-answer',
    ],
    function () use ($router) {
        $router->any('/', [
            'as' => 'api_question_course',
            'uses' => 'Controller@getListQuestionAnswer'
        ]);

    });