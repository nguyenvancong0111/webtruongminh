<?php

namespace App\Containers\QuestionAnswer\UI\API\Transformers;

use App\Containers\QuestionAnswer\Models\QuestionAnswer;
use App\Ship\Parents\Transformers\Transformer;

class QuestionAnswerCourseTransformer extends Transformer
{
    /**
     * @param $token
     *
     * @return  array
     */
    public function transform(QuestionAnswer $data)
    {
        $answer = [];
        if ($data->relationLoaded('answers')){
            foreach ($data->answers as $itm){
                $answer[] = [
                    'id' => $itm->id,
                    'name' => $itm->user_type == 1 ? 'GV. '.$itm->lecturers->fullname : ($itm->user_type == 2 ? $itm->customer->fullname: 'Unknown'),
                    'avatar' => $itm->user_type == 1 ? (!empty($itm->lecturers->avatar) ? \ImageURL::getImageUrl($itm->lecturers->avatar, 'lecturers', 'small') : asset('template/images/user.png')) : ($itm->user_type == 2 ? (!empty($itm->customer->avatar) ? \ImageURL::getImageUrl($itm->customer->avatar, 'customer', 'small') : asset('template/images/user.png')) : asset('template/images/user.png')),
                    'content' => $itm->answer,
                    'created_at' => \FunctionLib::dateFormat($itm->created_at, 'd/m/Y H:i'),
                ];
            }
        }
        $response = [
            'id' => $data->id,
            'name' => $data->customer->fullname,
            'avatar' => \ImageURL::getImageUrl($data->customer->avatar, 'customer', 'small'),
            'content' => $data->message,
            'answer' => $answer,
            'created_at' => \FunctionLib::dateFormat($data->created_at, 'd/m/Y H:i'),
        ];
        return $response;
    }
}
