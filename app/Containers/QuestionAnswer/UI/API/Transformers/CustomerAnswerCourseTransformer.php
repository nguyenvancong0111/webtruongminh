<?php

namespace App\Containers\QuestionAnswer\UI\API\Transformers;

use App\Containers\QuestionAnswer\Models\CustomerAnswer;
use App\Ship\Parents\Transformers\Transformer;

class CustomerAnswerCourseTransformer extends Transformer
{
    /**
     * @param $token
     *
     * @return  array
     */
    public function transform(CustomerAnswer $data)
    {
        $response = [
            'id' => $data->id,
            'name' => $data->customer->fullname,
            'avatar' => \ImageURL::getImageUrl($data->customer->avatar, 'customer', 'small'),
            'content' => $data->message,
            'answer' => [],
            'created_at' => \FunctionLib::dateFormat($data->created_at, 'd/m/Y H:i'),
        ];
        return $response;
    }
}
