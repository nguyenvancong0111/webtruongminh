<?php

namespace App\Containers\QuestionAnswer\UI\API\Transformers;

use App\Containers\QuestionAnswer\Models\CustomerAnswer;
use App\Ship\Parents\Transformers\Transformer;

class LecturersAnswerCourseTransformer extends Transformer
{
    /**
     * @param $token
     *
     * @return  array
     */
    public function transform(CustomerAnswer $data)
    {

        $response = [
            'id' => $data->id,
            'name' => $data->user_type == 1 ? 'GV. '.$data->lecturers->fullname : $data->customer->fullname,
            'avatar' => $data->user_type == 1 ? (!empty($data->lecturers->avatar) ? \ImageURL::getImageUrl($data->lecturers->avatar, 'lecturers', 'small') : asset('template/images/user.png')) : (!empty($data->customer->avatar) ? \ImageURL::getImageUrl($data->customer->avatar, 'customer', 'small') : asset('template/images/user.png')) ,
            'content' => $data->answer,
            'created_at' => \FunctionLib::dateFormat($data->created_at, 'd/m/Y H:i'),
        ];
        return $response;
    }
}
