@extends('basecontainer::admin.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5"><h1>{{ $site_title }}</h1></div>

            @if( count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            {!! Form::open(['url' => route('admin_question_answer_home_page'), 'method' => 'get', 'id' => 'searchForm']) !!}
            <div class="card card-accent-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-bookmark-o"></i></span></div>
                                <input type="text" name="name" class="form-control" placeholder="Tên, Email, Nội dung"
                                       value="{{ $search_data->name }}">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-calendar"></i></span></div>
                                <input type="text" name="time_from" class="datepicker form-control"
                                       placeholder="Thời gian đánh giá từ" autocomplete="off"
                                       value="{{ $search_data->time_from }}">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-calendar"></i></span></div>
                                <input type="text" name="time_to" class="datepicker form-control"
                                       placeholder="Thời gian đánh giá đến" autocomplete="off"
                                       value="{{ $search_data->time_to }}">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-list"></i></span></div>
                                <select id="status" name="status" class="form-control">
                                    <option value="">-- Chọn trạng thái --</option>
                                    <option value="2"{{ $search_data->status == 2 ? ' selected="selected"' : '' }}>Đang
                                        hiển thị
                                    </option>
                                    <option value="1"{{ $search_data->status == 1 ? ' selected="selected"' : '' }}>Đang
                                        ẩn
                                    </option>
                                    <option value="-1"{{ $search_data->status == -1 ? ' selected="selected"' : '' }}>Đã
                                        xóa
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                    <a href="{{ route('admin_question_answer_home_page') }}" class="btn btn-sm btn-primary"><i
                            class="fa fa-refresh"></i> Reset</a>
                </div>
            </div>
            {!! Form::close() !!}

            <div class="card card-accent-primary">
                <div class="card-body p-0">
                    <table class="table table-bordered table-striped ">
                        <thead>
                        <tr>
                            <th width="55">#</th>
                            <th width="20%">Học viên</th>
                            <th>Nội dung câu hỏi</th>
                            <th width="25%">Khóa học</th>
                            <th width="100">Thời gian</th>
                            <th width="8%">Lệnh</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <td align="center">{{ $loop->index + 1 }}</td>
                                <td>
                                    <p><strong>Họ và Tên: </strong> {{$item->customer->fullname}}</p>
                                    <p><strong>Email: </strong> {{$item->customer->email}}</p>
                                </td>
                                <td>{!! nl2br(@$item->message) !!}</td>
                                <td>
                                    @if(!empty($item->course))
                                        <a href="{{$item->course->link()}}" target="_blank"
                                           class="">{{$item->course->desc->name}}</a>
                                    @endif
                                </td>
                                <td align="center">{!! $item->created_at ? Carbon\Carbon::parse($item->created_at)->format("d/m/Y H:i:s") : '---' !!} </td>
                                <td align>
                                    @if($item->status > 0)
                                        <div class="d-flex justify-content-center">
                                            @if($item->status != 2)
                                                <a href="javascript:void(0)" data-route="{{route('admin_question_answer_enable', $item->id)}}" class="text-danger" onclick="admin.updateStatus(this,{{$item->id}}, 2)" title="Đang ẩn, Click để hiển thị"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                            @else
                                                <a href="javasript:;" data-route="{{route('admin_question_answer_disable', $item->id)}}" class="text-success" onclick="admin.updateStatus(this,{{$item->id}}, 1)" title="Đang hiển thị. Click để ẩn"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            @endif
                                        </div>

                                        <div class="d-flex justify-content-center">
                                            @if($item->status != -1)
                                                <a data-href="{{ route('admin_question_answer_delete', $item->id) }}"
                                                   href="javascript:void(0)" class="text-danger" title="Xóa"
                                                   onclick="admin.delete_this(this);"><i class="fa fa-trash"></i></a>
                                            @endif
                                        </div>
                                    @endif
                                    <div class="d-flex justify-content-center">
                                        <a class="" data-toggle="collapse" href="#collapse-{{$item->id}}" aria-expanded="false" aria-controls="collapse-{{$item->id}}"><i class="fa fa-reply-all" aria-hidden="true"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <tr class="collapse" id="collapse-{{$item->id}}">
                                <td colspan="6">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr class="table-info">
                                            <th width="100" class="text-center">#</th>
                                            <th width="300">Tác giả</th>
                                            <th>Nội dung</th>
                                            <th width="150">Thời gian</th>
                                            <th width="100" class="text-center">Lệnh</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @if($item->answers->isNotEmpty())
                                            <tr>
                                                <td>1</td>
                                                <td>Phan Trung Tưởng</td>
                                                <td>T</td>
                                                <td>T</td>
                                                <td>T</td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td colspan="5" class="text-center">Chưa có câu trả lời cho câu hỏi.</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    @include('basecontainer::admin.inc.bottom-pager-infor')
                </div>
            </div>
        </div>
    </div>
@stop
@push('js_bot_all')
    <script>
        $('.datepicker').datetimepicker({
            format: 'd/m/Y',
            timepicker: false
        });
    </script>
@endpush
