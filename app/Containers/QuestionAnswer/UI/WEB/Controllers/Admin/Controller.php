<?php

namespace App\Containers\QuestionAnswer\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\StringLib;
use App\Containers\QuestionAnswer\Models\Question;
use App\Containers\QuestionAnswer\UI\WEB\Requests\Admin\FindQuestionAnswerRequest;
use App\Containers\QuestionAnswer\UI\WEB\Requests\GetAllQuestionAnswerRequest;
use App\Containers\QuestionAnswer\UI\WEB\Requests\UpdateQuestionAnswerRequest;
use App\Ship\Parents\Controllers\AdminController;
use Exception;

class Controller extends AdminController
{
    public function __construct()
    {
        $this->title = 'Hỏi - Đáp';

        parent::__construct();

    }
    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllQuestionAnswerRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $questions = Apiato::call('QuestionAnswer@QuestionAnswerListingAction', [$request, $this->perPage]);
//        dd($questions);
        return view('questionanswer::admin.index', [
            'search_data' => $request,
            'data' => $questions,
        ]);
    }

    public function edit(FindQuestionAnswerRequest $request)
    {
        $this->showEditForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['edit', $this->title, 'admin_question_home_page']);

        try{
            $question = Apiato::call('QuestionAnswer@Admin\FindQuestionAnswerByIdAction', [$request->id]);

        }catch(Exception $e){
            return redirect()->route('admin_question_answer_home_page', ['id' => $request->id])->with('status', 'Có lỗi');
        }
// dd(config('question-container.positions'));
        return view('question::admin.edit', [
            'data' => $question,
            'positions' => config('question-container.positions')
        ]);

        return $this->notfound($request->id);
    }

    public function add()
    {
        $this->showAddForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['add', $this->title, 'admin_question_answer_home_page']);

        return view('question::admin.edit', [
            'positions' => config('question-container.positions')
        ]);
    }

    public function update(UpdateQuestionAnswerRequest $request)
    {
        try {
            $data =$request->except('image');

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'question', StringLib::getClassNameFromString(Question::class)]);
                if (!$image['error']) {
                    $data['image'] = $image['fileName'];
                }
            }

            $question = Apiato::call('QuestionAnswer@UpdateQuestionAnswerAction', [$data]);

            if ($question) {
                return redirect()->route('admin_question_answer_edit_page', ['id' => $question->id])->with('status', 'Cập nhật question thành công');
            }
        } catch (\Exception $e) {
            $this->throwExceptionViaMess($e);
        }
    }

    public function create(CreateQuestionAnswerRequest $request)
    {
        try {
            $data =$request->except('image');

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'question', StringLib::getClassNameFromString(Question::class)]);
                if (!$image['error']) {
                    $data['image'] = $image['fileName'];
                }
            }

            $question = Apiato::call('QuestionAnswer@CreateQuestionAnswerAction', [$data]);
            if ($question) {
                return redirect()->route('admin_question_answer_home_page')->with('status', 'QuestionAnswer đã được thêm mới');
            }
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function delete(FindQuestionAnswerRequest $request)
    {
        try {
            Apiato::call('QuestionAnswer@DeleteQuestionAnswerAction', [$request]);
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function enable(FindQuestionAnswerRequest $request){
        try {
            Apiato::call('QuestionAnswer@EnableQuestionAnswerAction', [$request]);
            return FunctionLib::ajaxRespondV2(true,'Success');
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function disable(FindQuestionAnswerRequest $request){
        try {
            Apiato::call('QuestionAnswer@DisableQuestionAnswerAction', [$request]);
            return FunctionLib::ajaxRespondV2(true,'Success');
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }
}
