<?php

namespace App\Containers\QuestionAnswer\Tasks\FrontEnd;

use App\Containers\QuestionAnswer\Data\Repositories\QuestionAnswerRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class CreateBannerTask.
 */
class CreateQuestionAnswerTask extends Task
{

    protected $repository;

    public function __construct(QuestionAnswerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $data = $this->repository->create($data);

            return $data;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
