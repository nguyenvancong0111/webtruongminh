<?php

namespace App\Containers\QuestionAnswer\Tasks\FrontEnd;

use App\Containers\QuestionAnswer\Data\Repositories\QuestionAnswerRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetAllQuestionAnswerByCourseIdTask extends Task
{

    protected $repository;

    public function __construct(QuestionAnswerRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($type = 1, $courseId, $with = [], $skipPaginate= false, $limit = 10, $currentPage = 1): iterable {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('status', 2));
        $this->repository->pushCriteria(new ThisEqualThatCriteria('type', $type));
        $this->repository->pushCriteria(new ThisEqualThatCriteria('object_id', $courseId));
        $this->repository->with($with);
        if (isset($with['answers'])){
            $this->repository->whereHas('answers', function (Builder $q){
               $q->where('status', '=', 2);
            });
        }
        $this->repository->orderBy('id', 'desc');
        return $skipPaginate ? $this->repository->paginate($limit) : $this->repository->get();
    }
}
