<?php

namespace App\Containers\QuestionAnswer\Tasks;

use App\Containers\QuestionAnswer\Data\Criterias\AdminFilterCriteria;
use App\Containers\QuestionAnswer\Data\Criterias\HasNameCriteria;
use App\Containers\QuestionAnswer\Data\Criterias\OrderByCreatedCriteria;
use App\Containers\QuestionAnswer\Data\Criterias\OrderBySortCriteria;
use App\Containers\QuestionAnswer\Data\Criterias\WithAllDescriptionCriteria;
use App\Containers\QuestionAnswer\Data\Criterias\WithDescriptionCriteria;
use App\Containers\QuestionAnswer\Data\Repositories\QuestionAnswerRepository;
use App\Ship\Criterias\Eloquent\OrderByFieldCriteria;
use App\Ship\Criterias\Eloquent\OrderByFieldsCriteria;
use App\Ship\Parents\Tasks\Task;

class QuestionAnswerListingTask extends Task
{
    protected $repository;

    public function __construct(QuestionAnswerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($limit = 20)
    {
        return $this->repository->with(['course' => function($query){
            $query->with('desc');
        }, 'customer', 'answers' => function($ans){
            $ans->where('status', 2);
        }])->paginate($limit);
    }

    public function adminFilter($request): self
    {
        $this->repository->pushCriteria(new \App\Containers\QuestionAnswer\Data\Criterias\AdminFilterCriteria($request));
        return $this;
    }

    public function hasName($name): self
    {
        $this->repository->pushCriteria(new \App\Containers\QuestionAnswer\Data\Criterias\HasNameCriteria($name));
        return $this;
    }

    public function withDescription($language_id): self
    {
        $this->repository->pushCriteria(new \App\Containers\QuestionAnswer\Data\Criterias\WithDescriptionCriteria($language_id));
        return $this;
    }

    public function withAllDescription(): self
    {
        $this->repository->pushCriteria(new \App\Containers\QuestionAnswer\Data\Criterias\WithAllDescriptionCriteria());
        return $this;
    }

    public function ordereByCreated(): self
    {
        $this->repository->pushCriteria(new OrderByFieldsCriteria([
            ['created_at','desc'],
        ]));
        return $this;
    }

}
