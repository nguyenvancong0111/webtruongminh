<?php

namespace App\Containers\QuestionAnswer\Tasks;

use App\Containers\QuestionAnswer\Data\Repositories\QuestionAnswerRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteBannerTask.
 */
class EnableQuestionAnswerTask extends Task
{

    protected $repository;

    public function __construct(QuestionAnswerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($id)
    {
        try {
            $this->repository->getModel()->where('id', $id)->update(
                [
                    'status' => 2
                ]
            );

        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
