<?php

namespace App\Containers\QuestionAnswer\Tasks;

use App\Containers\QuestionAnswer\Data\Repositories\QuestionAnswerRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteQuestionAnswerTask.
 */
class DeleteQuestionAnswerTask extends Task
{

    protected $repository;

    public function __construct(QuestionAnswerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($question_id)
    {
        try {
          $data =  $this->repository->getModel()->where('id', $question_id)->update(
                [
                    'status' => -1
                ]
            );

        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
