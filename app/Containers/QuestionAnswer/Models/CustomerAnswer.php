<?php

namespace App\Containers\QuestionAnswer\Models;

use App\Containers\Customer\Models\Customer;
use App\Containers\Lecturers\Models\Lecturers;
use App\Containers\QuestionAnswer\Enums\QuestionStatus;
use App\Ship\Parents\Models\Model;

class CustomerAnswer extends Model
{
    protected $table = 'customer_question_answer';

    protected $fillable = [
        'object',
        'question_id',
        'user_type',
        'user_id',
        'type',
        'answer',
        'status',
        'created_at',
        'update_at',
    ];

    public function lecturers(){
        return $this->hasOne(Lecturers::class, 'id', 'user_id');
    }
    public function customer(){
        return $this->hasOne(Customer::class, 'id', 'user_id');
    }

}
