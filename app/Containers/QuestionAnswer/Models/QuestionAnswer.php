<?php

namespace App\Containers\QuestionAnswer\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Containers\Course\Models\Course;
use App\Containers\QuestionAnswer\Enums\QuestionStatus;
use App\Containers\Customer\Models\Customer;
use App\Containers\Product\Models\Product;
use App\Ship\Parents\Models\Model;
use Carbon\Carbon;

class QuestionAnswer extends Model
{
    protected $table = 'customer_question';

    protected $fillable = [
        'type',
        'object_id',
        'customer_id',
        'fullname',
        'email',
        'avatar',
        'message',
        'send_mail',
        'status',
        'created_at',
        'update_at',
    ];

    public function customer(){
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }

    public function course(){
        return $this->hasOne(Course::class,'id','object_id');
    }

    public function answers(){
        return $this->hasMany(CustomerAnswer::class, 'question_id', 'id');
    }


    public function getImageUrl($size = 'large')
    {
        return ImageURL::getImageUrl($this->desc->image, 'comment', $size);
    }

    public function getQuestionLink(): string
    {
        if (!empty($this->desc->link)) {
            return $this->desc->link;
        }

        return route('web_home_page');
    }
}
