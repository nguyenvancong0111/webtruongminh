<?php

namespace App\Containers\QuestionAnswer\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\QuestionAnswer\Tasks\QuestionAnswerListingTask;
use App\Ship\Parents\Actions\Action;

class QuestionAnswerListingAction extends Action
{
    public function run($filters, $limit = 10)
    {
        $defaultLanguage = Apiato::call('Localization@GetDefaultLanguageTask');

        $data = app(QuestionAnswerListingTask::class)->adminFilter($filters)->ordereByCreated();

        return $data->run($limit);
    }
}
