<?php

namespace App\Containers\QuestionAnswer\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\QuestionAnswer\Models\QuestionAnswer;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class UpdateQuestionAnswerAction.
 *
 */
class EnableQuestionAnswerAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $question = Apiato::call('QuestionAnswer@EnableQuestionAnswerTask', [$data->id]);
        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            ['status' => 1],
            ['status' => 2],
            'Hiển thị Câu hỏi',
            QuestionAnswer::class
        ]);

        $this->clearCache();

        return $question;
    }
}
