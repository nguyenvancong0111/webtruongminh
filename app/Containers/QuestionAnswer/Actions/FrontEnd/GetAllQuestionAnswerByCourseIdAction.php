<?php

namespace App\Containers\QuestionAnswer\Actions\FrontEnd;

use App\Ship\Parents\Actions\Action;

class GetAllQuestionAnswerByCourseIdAction extends Action
{
    public function run($type = 1, $courseId, $with = [], $skipPaginate= false, $limit = 10, $currentPage = 1): iterable {
        return $this->call('QuestionAnswer@FrontEnd\GetAllQuestionAnswerByCourseIdTask', [$type, $courseId, $with, $skipPaginate, $limit, $currentPage]);
    }
}
