<?php

namespace App\Containers\QuestionAnswer\Actions\FrontEnd;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\QuestionAnswer\Models\QuestionAnswer;
use App\Ship\Parents\Actions\Action;

/**
 * Class CreateQuestionAnswerAction.
 *
 */
class CreateQuestionAnswerAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        $data = Apiato::call('QuestionAnswer@FrontEnd\CreateQuestionAnswerTask',[$data]);

        if ($data) {

            Apiato::call('User@CreateUserLogSubAction', [
                $data->id,
                [],
                $data->toArray(),
                'Hỏi/Đáp',
                QuestionAnswer::class
            ]);
        }
        $this->clearCache();
        return $data;


    }
}
