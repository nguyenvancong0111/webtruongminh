<?php

namespace App\Containers\QuestionAnswer\Actions\FrontEnd;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\QuestionAnswer\Models\QuestionAnswer;
use App\Ship\Parents\Actions\Action;

/**
 * Class CreateCustomerAnswerAction.
 *
 */
class CreateCustomerAnswerAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        $data = Apiato::call('QuestionAnswer@FrontEnd\CreateCustomerAnswerTask',[$data]);

        if ($data) {

            Apiato::call('User@CreateUserLogSubAction', [
                $data->id,
                [],
                $data->toArray(),
                'Trả lời câu hỏi',
                QuestionAnswer::class
            ]);
        }
        $this->clearCache();
        return $data;


    }
}
