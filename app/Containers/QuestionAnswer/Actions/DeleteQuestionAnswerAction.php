<?php


namespace App\Containers\QuestionAnswer\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\QuestionAnswer\Models\QuestionAnswer;
use App\Ship\Parents\Actions\Action;

/**
 * Class DeleteQuestionAnswerAction.
 *
 */
class DeleteQuestionAnswerAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {

        Apiato::call('QuestionAnswer@DeleteQuestionAnswerTask', [$data->id]);

        $this->clearCache();

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            [],
            ['status' => -1],
            'Xóa QuestionAnswer',
            QuestionAnswer::class
        ]);
    }
}
