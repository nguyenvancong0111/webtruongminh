<?php

namespace App\Containers\QuestionAnswer\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\QuestionAnswer\Models\QuestionAnswer;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Exception;

/**
 * Class UpdateQuestionAction.
 *
 */
class DisableQuestionAnswerAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $question = Apiato::call('QuestionAnswer@DisableQuestionAnswerTask', [$data->id]);

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            ['status' => 2],
            ['status' => 1],
            'Ẩn QuestionAnswer',
            QuestionAnswer::class
        ]);

        $this->clearCache();

        return $question;
    }
}
