<div class="tab-pane" id="item">
    <div class="tabbable">

        <ul class="nav nav-tabs nav-underline nav-underline-primary mb-3" role="tablist">
            @foreach($langs as $it_lang)
                <li class="nav-item">
                    <a class="nav-link {{$loop->first ? 'active' : ''}}"
                       href="#layout_lang_{{$it_lang['language_id']}}">
                        <img src="{{ asset('admin/img/lang/'.$it_lang['image']) }}" title="{{$it_lang['name']}}"> {{$it_lang['name']}}
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content p-0">
            @foreach($langs as $it_lang)
                <div class="tab-pane {{$loop->first ? 'active' : ''}}" id="layout_lang_{{$it_lang['language_id']}}">

                    <table class="table-bordered mt-3 w-100 tab-table-holder">
                        <thead>
                        <tr>
                            <td class="py-2 pl-2" width="25%"><span class="px-1 py-2">Hình ảnh</span></td>
                            <td class="py-2 pl-2"><span class="px-1 py-2">Nội dung</span></td>
                            <td>
                                <span class="d-flex justify-content-end">
                                    <a href="javascript:void(0)" class="btn btn-outline-success" onclick="createATab(this)">Thêm mục</a>
                                </span>
                            </td>
                        </tr>
                        </thead>
                        <tbody class="tab-container data-item">
                            <template class="layout-item">
                                @include('staticpage::admin.edit_tabs.inc.tab_item', ['item' => null, 'key' => 0])
                            </template>
                            <?php $itemTabs = json_decode(@$data['all_desc'][$it_lang['language_id']]['item'], true); ?>
                            @if(empty($itemTabs))
                                @include('staticpage::admin.edit_tabs.inc.tab_item', ['item' => null, 'key' => 0])
                            @else
                                @foreach($itemTabs as $k => $item)
                                    @include('staticpage::admin.edit_tabs.inc.tab_item', ['item' => $item, 'key' => $k])
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            @endforeach
        </div>
    </div>
</div>
@push('js_bot_all')
    <script !src="">

        createATab = (thisA) => {
            let html = $(thisA).closest('.tab-table-holder').find('.tab-container').find('.layout-item').html();
            $(thisA).closest('.tab-table-holder').find('.tab-container').append(html);
            $('.js-row-item').each(function (index) {
                let lang = $(this).find('.js-input-file').attr('data-lang');
                $(this).find('.js-input-file').attr('data-index', index);
                $(this).find('.js-input-file').attr('id', 'item_image_'+lang+'_'+index);
                $(this).find('.for-label-input').attr('for', 'item_image_'+lang+'_'+index);
            })

        }
        deleteATab = (thisA) => {
            $(thisA).closest('tr').remove();
        }

        function getIMG(event, item, key){
            var input = event.target;
            if (input.files && input.files[0]) {
                var index = $(item).attr('data-index');
                var reader = new FileReader();
                reader.onload = (e) => {
                    $('#item_image_'+key+'_'+index).siblings('.dropify-wrapper').find('.dropify-preview img').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endpush


