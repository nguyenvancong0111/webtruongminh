<tr class="js-row-item">
    <td>
        <div class="input-group p-1">
            {{--@include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])--}}
            <div class="dropify-wrapper has-preview">
                <div class="dropify-loader" style="display: none;"></div>
                <div class="dropify-errors-container">
                    <ul></ul>
                </div>
                <div class="dropify-preview" style="display: block;">
                    <span class="dropify-render">
                        @if (!empty(@$item['item_image']))
                            <img src="{{ \ImageURL::getImageUrl($item['item_image'], 'staticpage', 'original') }}">
                        @else
                            <img src="{{ asset('upload/no_photo.jpg')}}">
                        @endif
                    </span>
                    <label class="for-label-input" for="item_image_{{ $it_lang['language_id'] }}_{{$key}}" style="cursor: pointer;">
                        <div class="dropify-infos">
                            <div class="dropify-infos-inner">
                                <p class="dropify-filename"><span class="file-icon"></span>
                                   {{-- <span v-if="item.img_advantages_name != ''" class="dropify-filename-inner">@{{ item.img_advantages_name }}</span>
                                    <span v-else class="dropify-filename-inner">@{{ item.image_advantages_edit }}</span>--}}
                                </p>
                                <i class="fa fa-cloud-upload fa-5x"></i>
                                <p class="">Click vào <i class="fa fa-cloud-upload fs-20"></i> và chọn ảnh </p>
                            </div>
                        </div>
                    </label>
                </div>
            </div>

            <input type="file" class="d-none js-input-file" id="item_image_{{ $it_lang['language_id'] }}_{{$key}}"  name="staticpage_description[{{ $it_lang['language_id'] }}][item][item_image][]" data-lang="{{ $it_lang['language_id'] }}" data-index="{{$key}}" onchange="getIMG(event, this, {{ $it_lang['language_id'] }})"/>
            @if (!empty(@$item['item_image']))
                <input type="hidden" name="staticpage_description[{{ $it_lang['language_id'] }}][item][check_item_image][]" value="{{ @$item['item_image'] }}" />
            @endif

            {{--<input type="file" class="form-control" name="staticpage_description[{{ $it_lang['language_id'] }}][item][item_image][]" id="name_{{ $it_lang['language_id'] }}" accept="image/png, image/gif, image/jpeg" value="{{ @$item['item_image'] }}">
            @if (!empty(@$item['item_image']))
                <input type="hidden" class="form-control" name="staticpage_description[{{ $it_lang['language_id'] }}][item][check_item_image][]" value="{{ @$item['item_image'] }}">
            @endif
            <div class="text-center"><img src="{{ asset('upload/staticpage/' . @$item['item_image']) }}" alt="" style="width:35%"></div>--}}
        </div>
	</td>

    <td>
        <div class="input-group p-1">
            @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
            <input type="text" class="form-control" placeholder="Tiêu đề" name="staticpage_description[{{ $it_lang['language_id'] }}][item][item_title][]" id="name_{{ $it_lang['language_id'] }}" value="{{ @$item['item_title'] }}">
        </div>
        <div class="input-group p-1">
            @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
            <input type="text" class="form-control" placeholder="Link" name="staticpage_description[{{ $it_lang['language_id'] }}][item][item_link][]" id="item_link_{{ $it_lang['language_id'] }}" value="{{ @$item['item_link'] }}">
        </div>
        <div class="input-group p-1">
            @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
            <textarea  class="form-control" rows="2" name="staticpage_description[{{ $it_lang['language_id'] }}][item][item_description][]" id="item_description_{{ $it_lang['language_id'] }}" placeholder="Nhập mô tả" value="">{!! @$item['item_description'] !!}</textarea>
        </div>
        <div class="input-group p-1">
            @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
            <input type="text" class="form-control" placeholder="Sắp xếp" name="staticpage_description[{{ $it_lang['language_id'] }}][item][item_sort][]" id="item_sort_{{ $it_lang['language_id'] }}" value="{{ @$item['item_sort'] }}">
        </div>
    </td>

    <td class="px-2 text-center" style="width: 80px">
        <a href="javascript:void(0)" class="btn btn-outline-danger" onclick="deleteATab(this)">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </a>
    </td>
</tr>
