<?php

namespace App\Containers\Gallery\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllGalleriesAction extends Action
{
    public function run(Request $request, $pid=0)
    {
        $galleries = Apiato::call('Gallery@GetAllGalleriesTask', [$pid], ['addRequestCriteria']);

        return $galleries;
    }
}
