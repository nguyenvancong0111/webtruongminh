<?php

namespace App\Containers\Gallery\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\core\Traits\HelpersTraits\RecursiveTrait;

class GetAllGalleryFolderAction extends Action
{
    use RecursiveTrait;

    public function run(bool $isBuildTree=false)
    {
        $folders = Apiato::call('Gallery@GetAllGalleryFolderTask', [], ['addRequestCriteria']);
        if ($isBuildTree) {
          $folderArray = $folders->toArray();
          $tree = $this->buildSelectTree($folderArray);
          return $tree;
        }
        return $folders;
    }
}
