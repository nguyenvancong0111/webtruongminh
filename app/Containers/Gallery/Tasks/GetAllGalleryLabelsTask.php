<?php

namespace App\Containers\Gallery\Tasks;

use App\Containers\Gallery\Data\Repositories\GalleryLabelRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllGalleryLabelsTask extends Task
{

    protected $repository;

    public function __construct(GalleryLabelRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->all();
    }
}
