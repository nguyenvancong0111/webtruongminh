<?php

/** @var Route $router */
$router->get('galleries', [
  'as' => 'admin.gallery.index',
  'uses'  => 'Controller@index',
  'middleware' => [
    'auth:admin',
  ],
]);

$router->delete('galleries/{id}', [
  'as' => 'admin.gallery.delete',
  'uses'  => 'Controller@delete',
  'middleware' => [
    'auth:admin',
  ],
]);


// Route for folder
$router->get('galleries-folders/create', [
  'as' => 'admin.gallery.folder.create',
  'uses'  => 'GalleryFolderController@create',
  'middleware' => [
    'auth:admin',
  ],
]);

$router->post('galleries-folders/store', [
  'as' => 'admin.gallery.folder.store',
  'uses'  => 'GalleryFolderController@store',
  'middleware' => [
    'auth:admin',
  ],
]);


// Route for labels
$router->get('galleries-labels/create', [
  'as' => 'admin.gallery.label.create',
  'uses'  => 'GalleryLabelController@create',
  'middleware' => [
    'auth:admin',
  ],
]);

$router->post('galleries-labels/store', [
  'as' => 'admin.gallery.label.store',
  'uses'  => 'GalleryLabelController@store',
  'middleware' => [
    'auth:admin',
  ],
]);

$router->delete('galleries-labels/{id}', [
  'as' => 'admin.gallery.label.delete',
  'uses'  => 'GalleryLabelController@delete',
  'middleware' => [
    'auth:admin',
  ],
]);

$router->get('galleries-labels/{id}/edit', [
  'as' => 'admin.gallery.label.edit',
  'uses'  => 'GalleryLabelController@edit',
  'middleware' => [
    'auth:admin',
  ],
]);

$router->put('galleries-labels/{id}', [
  'as' => 'admin.gallery.label.update',
  'uses'  => 'GalleryLabelController@update',
  'middleware' => [
    'auth:admin',
  ],
]);



