<p data-id="{{ $item['id'] }}" @if ($item['id'] == 1) id="firstObjective" @endif>
  @if ($item['id'] == 1)
    <a  href="{{ route('admin.gallery.index')}}"
        class="{{ empty($input['label_id']) ? 'font-weight-bold' : ''}}">
      Tất cả
    </a>
  @else
    <span class="d-flex">
      <a href="{{ route('admin.gallery.index', ['label_id' => $item['id']])}}"
         class="{{ $item['id'] == ($input['label_id'] ?? '') ? 'font-weight-bold' : '' }}">
        {{ $item['title'] }}
      </a>
      <span class="ml-auto">
        <input type="hidden" class="gallery-label-data" value='@bladeJson($item)'>
        <a  href="javascript:void(0)"
            class="mr-2"
            onclick="return loadIframe(this, '{{ route('admin.gallery.label.edit', ['id' => $item['id']]) }}')">
            <i class="fa fa-pencil"></i>
        </a>
        <a  href="javascript:void(0)"
            onclick="return deleteGalleryLabel(this, '.gallery-label-data', '{{ route('admin.gallery.label.delete', ['id' => $item['id']]) }}')">
            <i class="fa fa-trash"></i>
        </a>
      </span>
    </span>
  @endif
</p>
