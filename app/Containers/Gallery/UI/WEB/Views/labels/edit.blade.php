@extends('basecontainer::admin.layouts.default_for_iframe')
@section('content')
<div class="row" id="sectionContent">
  <div class="col-12">
    <div class="card mb-0">
      <form action="{{ route('admin.gallery.label.update', ['id' => $label->id]) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-header d-flex">
          <a href="javascript:void(0)" class="mt-2">Sửa nhãn: {{ $label->title }}</a>
          <div class="ml-auto">
            <button type="button" class="btn btn-secondary" onclick="return closeFrame()">Đóng lại</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lưu thông tin</button>
          </div>
        </div>

        <div class="card-bod">
          <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="title">Tên nhãn</label>
                        <input  type="text"
                                class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                id="title"
                                name="title"
                                value="{{ old('email',@$label->title) }}"
                                required
                        />
                    </div>
                </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                  <div class="form-group">
                      <label for="description">Mô tả nhãn</label>
                      <textarea name="description" id="description" cols="30" rows="3" class="form-control">{{ old('description',@$label->description) }}</textarea>
                  </div>
              </div>
          </div>
        </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
