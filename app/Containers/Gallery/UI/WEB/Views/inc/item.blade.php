<tr data-id="{{ $item['id'] }}">
    <td>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="{{ $item['id'] }}">
            <label class="form-check-label" for="{{ $item['id'] }}">
                #{{ $item['id'] }}
            </label>
        </div>
    </td>
    <td>
        @if ($item['type'] == 'folder')
            <a class="text-body" href="{{ route('admin.gallery.index', ['pid' => $item['id']]) }}">
                <i class="fa-13e {{ \FunctionLib::getIconGallery($item['type']) }}"></i> {{ $item['title'] }}
            </a>
        @else
            <a class="text-body" href="javascript:void(0);">
                <i class="fa-13e {{ \FunctionLib::getIconGallery($item['type']) }}"></i> {{ $item['title'] }}
            </a>
        @endif
    </td>
    <td>tôi</td>
    <td>{{ \FunctionLib::dateFromObj($item['updated_at']) }}</td>
    <td>{{ \FunctionLib::formatSizeUnits($item['size']) }}</td>
    <td>
        <div class="dropdown">
            <button class="border-0 dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <input type="hidden" class='delete-gallery' value='@bladeJson($item)'>
                <a class="dropdown-item" href="#">Sửa thông tin tài liệu</a>
                <a class="dropdown-item" href="#">Gắn nhãn</a>
                <a class="dropdown-item" href="#">Di chuyển</a>
                <a class="dropdown-item" href="javascript:void(0)"
                    onclick="return deleteGalleryLabel(this, '.delete-gallery', '{{ route('admin.gallery.delete', ['id' => $item['id']]) }}')">
                    Xóa tài liệu
                </a>
            </div>
        </div>
    </td>
</tr>
