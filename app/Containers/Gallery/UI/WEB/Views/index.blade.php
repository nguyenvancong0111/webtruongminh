@extends('basecontainer::admin.layouts.default')

@section('content')
<div class="row">
  <div class="col-3">
    <div class="card card-accent-primary">
      <div class="card-header d-flex">
        <input type="text" class="form-control mr-2">

        <button onclick="return loadIframe(this, '{{ route('admin.gallery.label.create')}}')"
                class="btn btn-secondary w-75"
                type="button">
          Tạo nhãn
       </button>

      </div>

      <div class="card-body" id="labelLeftSideBar">
        @forelse ($labels as $label)
          @include('gallery::labels.inc.item', ['item' => $label])
        @empty
        @endforelse
      </div>
    </div>
  </div>
  <div class="col-9">
    <div class="card card-accent-primary">
      <div class="card-header d-flex">
        <a href="javascript:void(0)" onclick="return window.history.back();"><i class="fa fa-reply" aria-hidden="true"></i> Quay lại</a>
        <div class="dropdown ml-auto">
          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-plus"></i> Tạo
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a  class="dropdown-item"
                href="javascript:void(0)"
                onclick="loadIframe(this, '{{ route('admin.gallery.folder.create', [
                    'label_id' => $input['label_id'] ?? ''
                ]) }}')">
                Thư mục mới
            </a>
            <a class="dropdown-item" href="#">Tạo tài liệu</a>
          </div>
        </div>
      </div>

      <table class="table table-hover table-bordered mb-0" id="tableCustomer">
          <thead>
            <tr>
              <th>#</th>
              <th>Tài liệu</th>
              <th>Chủ sở hữu</th>
              <th>Sửa đổi lần cuối</th>
              <th>Kích cỡ tệp</th>
              <th>Thao tác</th>
            </tr>
          </thead>

          <tbody>
            @forelse ($galleries as $gallery)
              @include('gallery::inc.item', ['item' => $gallery])
            @empty
              <tr>
                <td colspan="7">Không có tài liệu</td>
              </tr>
            @endforelse
          </tbody>
      </table>

    </div>
  </div>
</div>

@include('customer::inc.modal-iframe-edit')
@endsection
