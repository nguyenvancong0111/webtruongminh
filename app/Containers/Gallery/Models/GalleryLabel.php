<?php

namespace App\Containers\Gallery\Models;

use App\Ship\Parents\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GalleryLabel extends Model
{
    use SoftDeletes;

    protected $table = 'gallery_cats';

    protected $guarded = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'gallerylabels';

    public function isDefaultLabel(): bool {
      return $this->id == 1;
    }

    public function items() {
      return $this->hasMany(Gallery::class, 'cat_id', 'id');
    }
} // End class
