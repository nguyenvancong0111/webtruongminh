<?php

namespace App\Containers\File\Tasks;

use Apiato\Core\Foundation\ImageURL;
use App\Models\System\UploadFile;
use App\Ship\Parents\Tasks\Task;

/**
 * Class UploadFileTask.
 */
class UploadFileTask extends Task
{
    public function run($request, $fileField, $title, $folder_upload)
    {
        $errorMsg = null;
        if ($request->hasFile($fileField)) {
            $file = $request->file($fileField);
            $checkTypeFile = ImageURL::checkTypeFileOnly($file);
            if ($file->isValid()) {
                if ($checkTypeFile['accept'] == true) {
                    $fname = basename($file->getClientOriginalName());
                    $files = ImageURL::uploadFilesNotImage($file, $fname, $checkTypeFile['type']);
                    if (!$files['status']) {
                        $errorMsg = $files['message'];
                    }
                }else {
                    $errorMsg = 'File sai định dạng. .MP4, .PDF, .Docx';
                }
            }else {
                $errorMsg = 'Upload File thất bại!';
            }
        }
        return ['error' => !empty($errorMsg), 'msg' => $errorMsg, 'fileName' => @$fname, 'dir' => @$files['dir'], 'type' => @$checkTypeFile['type']];
    }
}
