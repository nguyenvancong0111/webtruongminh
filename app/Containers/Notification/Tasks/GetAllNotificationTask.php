<?php

namespace App\Containers\Notification\Tasks;

use Exception;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\OrderByFieldCriteria;
use App\Containers\Localization\Models\Language;
use App\Containers\Notification\Data\Repositories\NotificationRepository;
use DB;
use App\Containers\News\Enums\ServiceType;
use App\Containers\News\Enums\ServiceStatus;
use App\Containers\Notification\Enums\TypeNotification;

class GetAllNotificationTask extends Task
{

    protected $repository;

    public function __construct(NotificationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($notifiable_id,$notifiable_type, int $limit=10,$type,$isPanigate,Language $currentLang = null)
    {
        $language_id = $currentLang ? $currentLang->language_id : 1;
        if($type==TypeNotification::ORDER || empty($type)){
            $notiOrder=DB::table('notifications')->selectRaw("id,data,created_at,'' as image,'' as message,'' as slug,'order' as type")->where('notifiable_id',$notifiable_id)
            ->where('notifiable_type',$notifiable_type)->where('type',TypeNotification::ORDER);
        }
       if($type==TypeNotification::PROMOTION||$type==ServiceType::EVENT || empty($type)){
           if(empty($type)){
            $typeNews=[ServiceType::PROMOTION,ServiceType::EVENT];
           }
           else{
            $typeNews=[$type];
           }
           $notiNews=DB::table('news')->join('news_description','news_description.news_id','=','news.id')
           ->selectRaw("news.id,'' as data,news.created_at, image, name as message, slug,'news' as type")->where('language_id',$language_id)
           ->where('status',ServiceStatus::ACTIVE)->whereIn('type',$typeNews)->whereRaw("(DATEDIFF(now(),created_at)<30)");
       }
       if(empty($type)){

        $query=$notiNews->union($notiOrder)->orderBy("created_at",'desc')->paginate($limit);

       }
       else if($type==TypeNotification::ORDER){
        $query=$notiOrder->orderBy("created_at",'desc')->paginate($limit);
       }
       else {
        $query=$notiNews->orderBy("created_at",'desc')->paginate($limit);
       }

       return $query;

    }
}
