<?php

namespace App\Containers\Notification\Tasks;

use App\Containers\Notification\Data\Repositories\NotificationRepository;
use App\Containers\User\Models\User;
use App\Ship\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;
use App\Ship\Parents\Tasks\Task;

class CountMyUnreadNotificationTask extends Task
{

    protected $repository;

    public function __construct(NotificationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $userId, $type=User::class): int
    {

        return $this->repository->where('notifiable_id', $userId)
                                ->where('notifiable_type', $type)
                                ->whereNull('read_at')
                                ->count();
    }
}
