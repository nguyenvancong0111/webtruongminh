<?php

namespace App\Containers\Notification\Tasks;

use Exception;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Collection;
use App\Ship\Criterias\Eloquent\LimitCriteria;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Ship\Criterias\Eloquent\OrderByFieldCriteria;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Containers\Notification\Data\Repositories\NotificationRepository;

class GetMyNotificationTask extends Task
{

    protected $repository;

    public function __construct(NotificationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $where=[], int $limit=10,$type,$isPanigate)
    {
        if(!empty($type)){
            $this->repository->where('type',$type);
        }
        if($isPanigate){
            return $this->repository->where($where)->paginate($limit);
        }
        else{
            return $this->repository->where($where)->get();
        }
       
    }

    public function orderBy(array $orderBy=[]) {
        $orderByKey = key($orderBy);
        $orderByValue = $orderBy[$orderByKey];
        $this->repository->pushCriteria(new OrderByFieldCriteria($orderByKey, $orderByValue));
    }
}
