<?php

namespace App\Containers\Notification\Tasks;

use Exception;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Exceptions\NotFoundException;
use Illuminate\Database\Eloquent\Collection;
use App\Ship\Criterias\Eloquent\LimitCriteria;
use App\Ship\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;
use App\Containers\Notification\Data\Repositories\NotificationRepository;

class GetListNotificationByStatusTask extends Task
{

    protected $repository;

    public function __construct(NotificationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $where=[]): Collection
    {
        $this->repository->pushCriteria(new OrderByCreationDateDescendingCriteria());
        return $this->repository->findWhere($where);        
    }

    public function limit(int $limit=0) {
        if (!empty($limit)) {
            $this->repository->pushCriteria(new LimitCriteria($limit));
        }
    }
}
