<?php

namespace App\Containers\Notification\Tasks;

use App\Containers\Notification\Data\Repositories\NotificationRepository;
use App\Containers\Notification\Notifications\InteractiveNotification;
use App\Containers\Notification\Notifications\OrderNotification;
use App\Containers\Notification\Values\NotificationStructureValue;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;

class PushNotificationTask extends Task
{

    protected $repository;

    public function __construct(NotificationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Collection $notificationReceivers, NotificationStructureValue $notificationStructure,string $type='')
    {
        $result = Notification::send($notificationReceivers, $this->callClass($notificationStructure,$type));
        return $result;
    }
    public function callClass($notificationStructure, $type){
        if($type='order'){
            return new OrderNotification($notificationStructure);
        }
        else{
            return new InteractiveNotification($notificationStructure);
        }
    }
    
}
