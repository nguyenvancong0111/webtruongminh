<?php

namespace App\Containers\Notification\Notifications;

use App\Ship\Parents\Notifications\Notification;
use App\Containers\Notification\Values\NotificationStructureValue;

/**
 * Class OrderNotification
 */
class OrderNotification extends Notification
{
    public $notificationStructureValue;

    public function __construct(NotificationStructureValue $notificationStructureValue)
    {
        $this->notificationStructureValue = $notificationStructureValue;
    }

    public function toArray($notifiable)
    {
        return $this->notificationStructureValue->toArray();
    }
}
