<?php

namespace App\Containers\Notification\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Facades\Auth;

class GetMyReadNotificationAction extends Action
{
    public function run($currentObjectAuthen)
    {
        $myListReadNotification = Apiato::call('Notification@GetListNotificationByStatusTask', [
            [
                ['notifiable_id', '=', $currentObjectAuthen->id],
                ['read_at', '!=', null],
                ['notifiable_type', '=', get_class($currentObjectAuthen)]
            ]
        ], [
            ['limit' => [10]]
        ]);

        return $myListReadNotification;
    }
}
