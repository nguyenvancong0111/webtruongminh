<?php

namespace App\Containers\Notification\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\User\Models\User;
use App\Containers\Notification\Notifications\InteractiveNotification;

class GetMyNotificationAction extends Action
{
    public function run(int $userId, string $notifiable_type=User::class, int $limit=7,$type=InteractiveNotification::class, $isPanigate=true)
    {
        $myNotifications = Apiato::call('Notification@GetMyNotificationTask', [
            [
                'notifiable_id' => $userId,
                'notifiable_type' => $notifiable_type,
            ],
            $limit,
            $type,
            $isPanigate
        ], [
            [
                'orderBy' => [
                    ['created_at' => 'DESC']
                ]
            ]
        ]);

        return $myNotifications;
    }
}
