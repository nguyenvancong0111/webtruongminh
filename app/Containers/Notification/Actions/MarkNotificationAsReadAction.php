<?php

namespace App\Containers\Notification\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Notification\Values\NotificationStructureValue;
use App\Containers\User\Models\User;
use Illuminate\Database\Eloquent\Collection;

class MarkNotificationAsReadAction extends Action
{
    public function run($request)
    {
        $notificationData = $request->all();
        $notification = Apiato::call('Notification@MarkNotificationReadStatusTask', [
            $notificationData
        ]);
        
        return $notification;
    }
} // End class
