<?php

namespace App\Containers\Notification\Actions;

use Illuminate\Support\Collection;
use App\Containers\User\Models\User;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Notification\Values\NotificationStructureValue;

class PushNotificationAction extends Action
{
    public function run(Collection $notificationReceivers,  NotificationStructureValue $notificationContentStructure,string $type='')
    {
        $sendNotificationResult = Apiato::call('Notification@PushNotificationTask', [
            $notificationReceivers,
            $notificationContentStructure,
            $type
        ]);
        
        return $sendNotificationResult;
    }
} // End class
