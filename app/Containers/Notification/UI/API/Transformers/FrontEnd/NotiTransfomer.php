<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-07 13:09:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-30 14:47:45
 * @ Description: Happy Coding!
 */

namespace App\Containers\Notification\UI\API\Transformers\FrontEnd;

use App\Containers\Order\Enums\OrderStatus;
use App\Ship\Parents\Transformers\Transformer;
use Apiato\Core\Foundation\Facades\ImageURL;


class NotiTransfomer extends Transformer
{
    
    public function transform($notis)
    {
       if(@$notis->type=='order'){
        $datas=json_decode($notis->data);
        $data = [
            'message' => $datas->message,
            'link' => $datas->link,
            'image' => ImageURL::getImageUrl(@$datas->other_info->image, 'product', 'mediumx3'),
            'status' => empty($datas->other_info->status)?'':OrderStatus::TEXT[@$datas->other_info->status]
        ];
       }
       else{
        $data = [
            'message' =>$notis->message,
            'link' => route('web_news_detail_page',['slug'=>$notis->slug,'id'=>$notis->id]),
            'image' => ImageURL::getImageUrl(@$notis->image,'news','medium3'),
            'status' => ''
        ];
       }
       return $data;
       
    }

   
}
