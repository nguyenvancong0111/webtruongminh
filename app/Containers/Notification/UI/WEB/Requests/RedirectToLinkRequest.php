<?php

namespace App\Containers\Notification\UI\WEB\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateNotificationRequest.
 */
class RedirectToLinkRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    // protected $transporter = \App\Ship\Transporters\DataTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'redirect_link' => ['required', 'min:5']
        ];
    }

    public function messages() {
        return [
            'redirect_link.min' => 'Thông báo này không có link hoặc link không hợp lệ: ' . request('redirect_link')
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'read_at' => now()
        ]);
    }
}
