<?php

namespace App\Containers\Notification\UI\WEB\Controllers\Admin;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Containers\Notification\UI\WEB\Requests\RedirectToLinkRequest;
use App\Containers\Notification\UI\WEB\Requests\DeleteNotificationRequest;
use App\Containers\Notification\UI\WEB\Requests\GetAllNotificationsRequest;
use App\Containers\Notification\UI\WEB\Requests\MarkReadNotificationRequest;
use App\Containers\Notification\UI\WEB\Requests\MarkUnreadNotificationRequest;

/**
 * Class Controller
 *
 * @package App\Containers\Notification\UI\WEB\Controllers
 */
class NotificationController extends WebController
{
    use ApiResTrait;

    /**
     * Show all entities
     *
     * @param GetAllNotificationsRequest $request
     */
    public function getMyNotification(GetAllNotificationsRequest $request)
    {
        $currentUser = auth('admin')->user();

        $myTotalUnreadNotification = Apiato::call('Notification@CountMyUnreadNotificationAction', [
            $currentUser->id
        ]);
        
        $myNotifications = Apiato::call('Notification@GetMyNotificationAction', [
            $currentUser->id,
            get_class($currentUser),
            15
        ]);

        $responseData = [
            'unread' => [],
            'read' => $myNotifications->items(),
            'total_unread' => $myTotalUnreadNotification,
            'current_page' => $myNotifications->currentPage(),
            'has_more_page' => $myNotifications->hasMorePages()
        ];
        return $this->sendResponse($responseData);        
    }

    public function markReadAll() {
        $currentUser = auth('admin')->user();
        $currentUser->unreadNotifications->markAsRead();
        return $this->sendResponse(true);
    }

    public function redirectToLink(RedirectToLinkRequest $request): RedirectResponse  {
        $notificationAsRead = Apiato::call('Notification@MarkNotificationAsReadAction', [$request]);
        if ($notificationAsRead) {
            return redirect()->to($request->redirect_link);
        }

        return redirect()->back()->with([
            'flash_level' => 'error',
            'flash_message' => 'Mark Notification As Read Failure'
        ]);
    }

    public function markRead(MarkReadNotificationRequest $request) {
        $notification = Apiato::call('Notification@MarkNotificationAsReadAction', [$request]);
        return $this->sendResponse($notification);
    }

    public function markUnread(MarkUnreadNotificationRequest $request) {
        $notification = Apiato::call('Notification@MarkNotificationAsUnreadAction', [$request]);
        return $this->sendResponse($notification);
    }

    public function delete(DeleteNotificationRequest $request) {
        $notificationDeleteResult = Apiato::call('Notification@DeleteNotificationAction', [$request]);
        return $this->sendResponse($notificationDeleteResult);
    }
} // End class
