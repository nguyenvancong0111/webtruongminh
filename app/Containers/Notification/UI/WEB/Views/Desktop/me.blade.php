@extends("basecontainer::Desktop.layouts.default")

@section('content')
<main class="contact-page">
    <div class="page-title" style="background-image: url({{ asset('template/images/banner-page-title.jpg') }});">
        <div class="container page-title-wrap">
            <h2 class="title">Thông báo</h2>
            <span class="sub-title">Danh sách thông báo gửi cho bạn</span>
        </div>
    </div>
    <div class="support-box">
        <div class="container">
            <div class="support-box-bg">
                <div class="support-box-wrap">
                    <div class="box-info">
                        <h4 class="title-info">Chủ động liên hệ với <b>{{ @$settings['website']['site_name'] ? @$settings['website']['site_name'] : config('default-contact-config.config.site_name')}}</b></h4>
                        <div>
                            <p class="item">
                                <a href="tel:{{ @$settings['contact']['hotline'] ?? config('default-contact-config.config.hotline')}}">
                                    <img src="{{ asset('template/images/icon/phone-call-1.png') }}" alt="">
                                </a>
                                HOTLINE: <b>{{ @$settings['contact']['hotline'] ?? config('default-contact-config.config.hotline')}}</b>
                            </p>
                            <p class="item">
                                <a href="tel:{{ @$settings['contact']['tel'] ?? config('default-contact-config.config.tel')}}">
                                    <img src="{{ asset('template/images/icon/phone-call-1.png') }}" alt="">
                                </a>
                                Tel: {{ @$settings['contact']['tel'] ?? config('default-contact-config.config.tel')}}
                            </p>
                            <p class="item">
                                <a href="mailto:{{ @$settings['contact']['email'] ?? config('default-contact-config.config.email')}}">
                                    <img src="{{ asset('template/images/icon/envelope-1.png') }}" alt="">
                                </a>
                                {{ @$settings['contact']['email'] ?? config('default-contact-config.config.email')}}
                            </p>
                        </div>
                    </div>
                    <div class="form-support">
                        <h4 class="title-form">Danh sách</h4>
                        <div class="noti-page container">
                            <div class="drop-noti">
                                <ul>
                                    @forelse ($myNotifications as $notification)
                                        <li class="noti-type-{{ $notification->isUnread() ? '2' : '1'}}">
                                            <p class="img">
                                                <a href="{{ $notification->redirect_link }}">
                                                    <img src="{{ $notification->getObjectTriggerAvatar('small') }}" class="rounded-circle"/>
                                                </a>
                                            </p>

                                            <a  href="{{ $notification->redirect_link }}" 
                                                class="content text-left"
                                                @if(!$notification->hasRedirectLink())
                                                    onclick="return notifications.markAsRead('{{ $notification->id }}', this)"
                                                @endif
                                            >
                                                <span class="noti">{!! $notification->data['message'] !!}</span>
                                                <span class="time">{{ $notification->getCarbonDiffForHumans() }}</span>
                                            </a>
                                        </li>
                                    @empty
                                        <li class="noti-type-1">Bạn không có thông báo nào</li>
                                    @endforelse
                                </ul>
                            </div>

                            <div class="d-flex justify-content-center mt-2">
                                <center>
                                    {{ $myNotifications->links() }}
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
