<?php
$router->group([
  'prefix' => 'notifications',
  'middleware' => ['web', 'auth:customer'],
  'namespace' => '\App\Containers\Notification\UI\WEB\Controllers\Desktop',
  'domain' => parse_url(config('app.url'))['host']
], function () use ($router) {
  $router->get('/me', [
    'as' => 'notifications.me',
    'uses' => 'NotificationController@me'
  ]);

  $router->get('/redirect/{id}', [
    'as' => 'notifications.me.redirect',
    'uses' => 'NotificationController@redirect'
  ]);

  $router->get('/total-unread', [
    'as' => 'notifications.me.total_unread',
    'uses' => 'NotificationController@getTotalUnread'
  ]);

  $router->post('/mark-as-read', [
    'as' => 'notifications.me.mark-as-read',
    'uses' => 'NotificationController@markAsRead'
  ]);
});
