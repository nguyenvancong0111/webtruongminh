<?php

namespace App\Containers\Tags\UI\API\Transformers\FrontEnd;

use Apiato\Core\Foundation\ImageURL;
use App\Containers\Tags\Models\Tags;
use App\Ship\Parents\Transformers\Transformer;

class SpecialTagsTransfomer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [];

    /**
     * @var  array
     */
    protected $availableIncludes = [];

    public function transform(Tags $entity)
    {
        // dd($entity);
        $response = [
            "id" => $entity->id,
            "img" => ImageURL::getImageUrl($entity->image,'tags',''),
            "name" => @$entity->desc->name ?? '',
            "bg_color" => $entity->color,
            "type" => $entity->type,
            "cate" => $entity->cate,
        ];

        return $response;
    }
}
