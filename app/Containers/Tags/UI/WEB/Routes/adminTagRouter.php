<?php
Route::group(
[
    'prefix' => 'tags',
    'namespace' => '\App\Containers\Tags\UI\WEB\Controllers',
    'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
    'middleware' => [
        'auth:admin',
    ],
],function () use ($router) {
    $router->get('/{abc}', [
      'as'   => 'admin_tag_home_page',
      'uses' => 'Controller@index',
  ]);
    $router->get('/', [
      'as'   => 'admin_tag_home_page',
      'uses' => 'Controller@index',
  ]);

});
