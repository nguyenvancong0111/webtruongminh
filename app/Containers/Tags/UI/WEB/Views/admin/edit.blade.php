
@extends('basecontainer::admin.layouts.default')

@section('content')
  <div class="row" id="tagEdit">
  </div>
@endsection

@push('js_bot_all')
<script>
  window.tag_id = '{{ $tagID }}';
</script>
<script src="{{ asset('admin/tag/tag-edit.js') }}?v={{time()}}"></script>
@endpush
