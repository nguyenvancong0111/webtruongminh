<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-06 12:03:16
 * @ Description: Happy Coding!
 */

namespace App\Containers\Tags\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class TagCate extends BaseEnum
{
    const LABEL = 'label';
    const TAG = 'tag';
}
