<?php

namespace App\Containers\Tags\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Tags\Models\Tags;
use Exception;


class TagDetails extends Model
{
    protected $table = 'tag_details';

    protected $fillable = ['tag_id','object_id'];

    protected $attributes = [

    ];
    public $timestamps = false;
    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'tag_details';

    public function getByID($id,$input,$type,$cate){
        if($id==0 || empty($id))
          return false;

        return $this->with('tags')->whereHas('tags', function ($query) use($input,$type,$cate) {
          $query->where('title','like', '%'.$input.'%')->where('type',$type)->where('cate',$cate);
      })->where('object_id',$id)->get();
    }

    //$query->where('title','like', '%'.$input.'%');
    public function tags(){
      return $this->hasOne(Tags::class,'id','tag_id');
    }

    public function saveAndAddTag($object_tags,$obj_id,$type,$cate){
      try {
        $this->where('object_id',$obj_id)->delete();
        foreach ($object_tags as $key => $value) {
          if(isset($value['tag_id']) && isset($value['id'])){
              $data =[
                  'tag_id' =>  $value['tag_id'],
                  'object_id' =>  $obj_id,
              ];
              $this->create($data);
          }elseif(isset($value['id']) && !isset($value['tag_id'])){
            $data =[
                'tag_id' =>  $value['id'],
                'object_id' =>  $obj_id,
            ];
            $this->create($data);
          }elseif(!isset($value['id']) && !isset($value['tag_id'])){
                $checkIssetName = Tags::where('title',$value['tags']['title'])->first();
                if(empty($checkIssetName)){
                  $tag=Tags::addTags($value,$type,$cate);
                  $data =[
                    'tag_id' =>  $tag,
                    'object_id' =>  $obj_id,
                  ];
                }else{
                  $data =[
                    'tag_id' =>  $checkIssetName->id,
                    'object_id' =>  $obj_id,
                  ];
                }

                $this->create($data);
            }
          }
      } catch (Exception $exception) {

        throw $exception;
      }

    }

    public function saveAndAddTagLabel($object_tags,$obj_id){
      try {
        $this->where('object_id',$obj_id)->delete();
        foreach ($object_tags as $keyParrent => $valueParrent) {
          foreach ($valueParrent as $key => $value) {
            if(isset($value['tag_id']) && isset($value['id'])){
                $data =[
                    'tag_id' =>  $value['tag_id'],
                    'object_id' =>  $obj_id,
                ];
                $this->create($data);
            }elseif(isset($value['id']) && !isset($value['tag_id'])){
              $data =[
                  'tag_id' =>  $value['id'],
                  'object_id' =>  $obj_id,
              ];
              $this->create($data);
            }elseif(!isset($value['id']) && !isset($value['tag_id'])){
                  $checkIssetName = Tags::where('title',$value['tags']['title'])->first();
                  if(empty($checkIssetName)){
                    if($keyParrent == 'product_tags')
                      $tag=Tags::addTags($value,'product','tag');
                    elseif($keyParrent == 'product_labels')
                      $tag=Tags::addTags($value,'product','label');
                    $data =[
                      'tag_id' =>  $tag,
                      'object_id' =>  $obj_id,
                    ];
                  }else{
                    $data =[
                      'tag_id' =>  $checkIssetName->id,
                      'object_id' =>  $obj_id,
                    ];
                  }

                  $this->create($data);
              }
            }
        }
      } catch (Exception $exception) {

        throw $exception;
      }

    }
}
