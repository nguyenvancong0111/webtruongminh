<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:12:16
 * @ Description: Happy Coding!
 */

namespace App\Containers\Tags\Actions\TagFilter;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;


class GetFiltersByIdAction extends Action
{
    public function run($product_id)
    {
        $data = Apiato::call(
            'Tags@TagFilter\GetFiltersByIdTask',
            [
                $product_id,
                [],
                Apiato::call('Localization@GetDefaultLanguageTask')
            ]
        );

        return $data;
    }
}
