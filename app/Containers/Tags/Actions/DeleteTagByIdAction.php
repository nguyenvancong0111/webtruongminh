<?php

namespace App\Containers\Tags\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteTagByIdAction extends Action
{
    public function run($id)
    {
      $this->clearCache();
      return Apiato::call('Tags@DeleteTagByIdTask', [$id]);
    }
}
