<?php

namespace App\Containers\Tags\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class GetProductGalleryAction.
 *
 */
class GetTagsGalleryAction extends Action
{

    /**
     * @return mixed
     */
    public function run( $tag_id, $type = 'product', $json = false)
    {
        return $this->call('Tags@GetTagsGalleryTask', [
            $tag_id,
            $type,
            $json
        ]);
    }
}
