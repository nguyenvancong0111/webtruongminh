<?php

namespace App\Containers\Tags\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetTagsByInputAction extends Action
{
    public function run($input='', $cate='tag', $type='product')
    {
      $tags = Apiato::call('Tags@FindTagsByInputTask', [$input , $cate, $type]);
        return $tags;
    }
}
