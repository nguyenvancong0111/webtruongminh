<?php

namespace App\Containers\Tags\Tasks\TagFilter;

use App\Containers\Tags\Models\Tags;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SaveProductFilterTask.
 */
class SaveTagFilterTask extends Task
{
    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($tag_filter, Tags $current_tag_model)
    {
        if (!empty($tag_filter)) {
            if (is_array($tag_filter)) {
                $current_tag_model->filters()->sync($tag_filter);
            } else {
                $current_tag_model->filters()->attach($tag_filter);
            }
        } else {
            $current_tag_model->filters()->sync([]);
        }
    }
}
