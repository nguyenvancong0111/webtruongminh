<?php

namespace App\Containers\Tags\Tasks\TagDes;
use Illuminate\Support\Str;
use App\Containers\Tags\Data\Repositories\TagDescRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Apiato\Core\Foundation\Facades\Apiato;

class SaveTagDesTask extends Task
{

    protected $repository;

    public function __construct(TagDescRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id='', array $data)
    {
        try {
            if(!empty($data)){
              foreach ($data as $key => $item) {
                  $this->repository->getModel()->create($item);
              }
            }
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
