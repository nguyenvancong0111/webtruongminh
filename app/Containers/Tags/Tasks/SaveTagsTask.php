<?php

namespace App\Containers\Tags\Tasks;
use Illuminate\Support\Str;
use App\Containers\Tags\Data\Repositories\TagsRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
class SaveTagsTask extends Task
{

    protected $repository;

    public function __construct(TagsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id='', array $data)
    {
        try {
            unset($data['all_desc']);
            unset($data['tag_filter']);
            if($id!='undefined'){
                return $this->repository->update($data,$id);
            }
            else{
               $data['safe_title'] = Str::slug($data['title']);
               $data['created'] = time();
               $tags = $this->repository->getModel();
               $tags->title = $data['title'];
               $tags->safe_title = $data['safe_title'];
               $tags->image = $data['image'];
               $tags->created = $data['created'];
               $tags->type = $data['type'];
               $tags->primary_category_id = $data['primary_category_id'];
               return $tags->create($data);
            }


        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
