<?php

namespace App\Containers\Tags\Tasks;

use App\Containers\Tags\Data\Repositories\TagsRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class GetProductGalleryTask.
 */
class GetTagsGalleryTask extends Task
{

    protected $repository;

    public function __construct(TagsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($tag_id, $type = 'product', $json = false)
    {
        $img = $this->repository->getModel()->where('type', $type)->where('id', $tag_id)->first();
        // $images = $this->repository->getModel()->where('type', $type)->where('object_id', $product_id)->orderByRaw('sort desc,created desc')->get();
        $data = [];
        if(!empty($img)) {
                // $tmp = $img->toArray();
                $tmp['img'] = $img->image;
                $tmp['image_md'] = $img->image->getImageUrl('hotel_preview');
                array_push($data, $tmp);
        }
        return $json ? json_encode($data) : $data;
    }
}
