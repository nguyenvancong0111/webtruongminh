<?php

namespace App\Containers\Tags\Tasks\TagCategory;

use App\Containers\Tags\Models\Tags;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SaveProductCategoryTask.
 */
class SaveTagCategoryTask extends Task
{
    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($tag_category,Tags $current_product_model)
    {
        if (!empty($tag_category)) {
            if (is_array($tag_category)) {
                $current_product_model->categories()->sync($tag_category);
            } else {
                $current_product_model->categories()->attach($tag_category);
            }
        }else {
            $current_product_model->categories()->sync([]);
        }
    }
}
