<?php

namespace App\Containers\Tags\Tasks;

use App\Containers\Tags\Data\Repositories\TagDetailsRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindTagsByIdTask extends Task
{

    protected $repository;

    public function __construct(TagDetailsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id,$input,$type,$cate)
    {
        try {
            return $this->repository->getByID($id,$input,$type,$cate);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
