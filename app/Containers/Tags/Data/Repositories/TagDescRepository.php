<?php

namespace App\Containers\Tags\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class TagsRepository
 */
class TagDescRepository extends Repository
{

    /**
     * @var array
     */
    protected $container = 'Tags';
    protected $fieldSearchable = [
    ];

}
