<?php

namespace App\Containers\BaseContainer\UI\API\Transformers\FrontEnd;

use Apiato\Core\Foundation\ImageURL;
use App\Ship\Parents\Transformers\Transformer;
use Illuminate\Support\Str;

class BaseCategoryTransformer extends Transformer
{
    public $selectedValues;

    public function __construct(?array $selectedValues = null)
    {
        $this->selectedValues = $selectedValues;
        parent::__construct();
    }

    public function transform($category)
    {
        $response = [
            'category_id' => $category['category_id'],
            'icon' => ImageURL::getImageUrl($category['icon'], 'category', 'iconx2'),
            'icon2' => ImageURL::getImageUrl($category['icon2'], 'category', 'iconx2'),
            'hot' => (bool)$category['hot'],
            'top' => (bool)$category['top'],
            'is_good_price' => (bool)$category['is_good_price'],
            'show_freeship_page' => (bool)$category['show_freeship_page'],
            'show_sale_page' => (bool)$category['show_sale_page'],
            'primary_color' => $category['primary_color'],
            'second_color' => $category['second_color'],
            'name' =>   $category['desc']['name'],
            'slug' =>   $category['desc']['slug'],
            'parent_id' => $category['parent_id'],
            'selected' => !empty($this->selectedValues) ? in_array($category['category_id'],$this->selectedValues) : false,
            'products' => [],
            'link' => route('web.course.cate', ['cate_slug' => !empty($category['desc']['slug']) ? $category['desc']['slug'] : Str::slug($category['desc']['name']), 'cate_id' => $category['category_id']]),
            'count_course' => isset($category['course_count']) ? $category['course_count'] : 0
        ];

        return $response;
    }
}
