<div id="popupDoneRegister" class="popup popup--forgot-password">
    <div class="popup-overlay"></div>
    <div class="popup-absolute">
        <div class="popup-relative">
            <div class="popup-content">
                <div class="popup-login-regis--main position-relative">
                    <!-- button close popup -->

                    <a href="javascript:;" class="js-close-popup">
                        <img src="{{asset('template/images/ic-close-popup.png')}}" alt="ic-close-popup" />
                    </a>

                    <div class="style--radius-border">
                        <div class="box--login-regis-content">
                            <div class="box-title">
                                <h2 class="title">CHÚC MỪNG BẠN</h2>

                                <div class="sub">Đã đăng ký thành công tài khoản</div>
                            </div>

                            <a href="javascript:;" class="d-block btn-submit js-action-submit" data-tabpopup="popupLoginClient">
                                đăng nhập ngay
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>