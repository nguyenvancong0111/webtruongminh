<div id="popupRegisClient" class="popup">
    <div class="popup-overlay"></div>
    <div class="popup-absolute">
        <div class="popup-relative">
            <div class="popup-content">
                <div class="popup-login-regis--main position-relative">
                    <!-- button close popup -->

                    <a href="javascript:;" class="js-close-popup">
                        <img src="{{asset('template/images/ic-close-popup.png')}}" alt="ic-close-popup" />
                    </a>

                    <div class="style--radius-border">
                        <div class="navtab">
                            <a href="javascript:;" title="" class="navtab-item" data-tabpopup="popupLoginClient">ĐĂNG nhập</a>
                            <a href="javascript:;" title="" class="navtab-item active" data-tabpopup="popupRegisClient">ĐĂNG ký</a>
                        </div>

                        <div class="box--login-regis-content">
                            <h2 class="title--box">Khách hàng tạo tài khoản mới</h2>

                            {!! Form::open(['url' => '', 'id' => 'popup-register', 'class' => 'form--actions']) !!}
                            <span class="input-name"> Họ và tên <span class="red">*</span> </span>
                            <input type="text" name="regiter_popup_fullname" id="regiter_popup_fullname" class="{{$errors->has('regiter_popup_fullname') ? 'is-invalid' : 'reverse'}}" value="{{old('regiter_popup_fullname')}}" placeholder="Nhập họ và tên của bạn" />
                            <span class="error-message" id="regiter_popup_fullname_error">{{ $errors->has('regiter_popup_fullname') ? $errors->first('regiter_popup_fullname') : '' }}</span>

                            <span class="input-name"> Email/ Số điện thoại <span class="red">*</span> </span>
                            <input type="text" name="regiter_popup_email_phone" id="regiter_popup_email_phone" class="{{$errors->has('regiter_popup_email_phone') ? 'is-invalid' : 'reverse'}}" value="{{old('regiter_popup_email_phone')}}" placeholder="Nhập email hoặc số điện thoại" />
                            <span class="error-message" id="regiter_popup_email_phone_error">{{ $errors->has('regiter_popup_email_phone') ? $errors->first('regiter_popup_email_phone') : '' }}</span>

                            <span class="input-name"> Mật khẩu <span class="red">*</span> </span>
                            <div class="js-toggle-password position-relative">
                                <input type="password" name="regiter_popup_password" id="regiter_popup_password" class="mb-2 {{$errors->has('regiter_popup_password') ? 'is-invalid' : 'reverse'}}" value="{{old('regiter_popup_password')}}" placeholder="Thiết lập mật khẩu của bạn" />
                                <div class="password--show-hide">
                                    <a href="javascript:;" class="js-password-action hide active">
                                        <img src="{{asset('template/images/ic-hidepass.png')}}" alt="" />
                                    </a>
                                    <a href="javascript:;" class="js-password-action show">
                                        <img src="{{asset('template/images/ic-showpass.png')}}" alt="" />
                                    </a>
                                </div>
                            </div>
                            <span class="text-sub--gray">*Nhập mật khẩu gồm 6 ký tự gồm có: Chữ viết thường, in hoa, số</span>
                            <span class="error-message" id="regiter_popup_password_error">{{ $errors->has('regiter_popup_password') ? $errors->first('regiter_popup_password') : '' }}</span>

                            <div class="js-toggle-password position-relative">
                                <input type="password" name="regiter_popup_password_confirm" id="regiter_popup_password_confirm" class="{{$errors->has('regiter_popup_password_confirm') ? 'is-invalid' : 'reverse'}}" value="{{old('regiter_popup_password_confirm')}}" placeholder="Nhật mật khẩu của bạn vừa thiết lập" />

                                <div class="password--show-hide">
                                    <a href="javascript:;" class="js-password-action hide active">
                                        <img src="{{asset('template/images/ic-hidepass.png')}}" alt="ic-hidepass" />
                                    </a>
                                    <a href="javascript:;" class="js-password-action show">
                                        <img src="{{asset('template/images/ic-showpass.png')}}" alt="ic-showpass" />
                                    </a>
                                </div>
                            </div>
                            <span class="error-message" id="regiter_popup_password_confirm_error">{{ $errors->has('regiter_popup_password_confirm') ? $errors->first('regiter_popup_password_confirm') : '' }}</span>

                            <button type="button" class="btn-submit" id="popup-register-click" {{--data-tabpopup="popupDoneRegister"--}}>đăng ký</button>

                            <div class="line--text-center">
                                <span>Hoặc đăng nhập bằng :</span>
                            </div>

                            <div class="social--actions">
                                <a href="{{route('web.customer.facebook.login')}}" class="fb-login">
                                    <img src="{{asset('template/images/ic-fb-login.png')}}" alt="ic-fb-login" />
                                    Đăng ký bằng Facebook
                                </a>
                                <a href="{{route('web.customer.google.login')}}" class="gg-login">
                                    <img src="{{asset('template/images/ic-gg-login.png')}}" alt="ic-gg-login" />
                                    Đăng ký bằng Google
                                </a>
                            </div>
                            {!! Form::close() !!}

                            <div class="bottom--text-privacy">
                                Khi nhấn đăng ký tài khoản, đồng nghĩa bạn đã đồng ý tất cả
                                <a href="{{isset($settings['activity']['dieu_kien_chinh_sach']) && !empty($settings['activity']['dieu_kien_chinh_sach']) ? $settings['activity']['dieu_kien_chinh_sach'] : 'javascript:;'}}">điều kiện sử dụng và chính sách</a>
                                của HD Luật & Fdico
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('js_bot_all')
    <script>
        $('#popup-register-click').on('click', function(e) {
            e.preventDefault();
            var form = $('#popup-register');
            var data = $(form).serializeArray();
            var fd = new FormData();
            if (data.length > 0){
                $.each(data, function (key, value) {
                    fd.append(value.name, value.value);
                })
            }
            fd.append('type', 'home');
            $('#popup-register input').removeAttr('style').addClass('reverse');
            $('#popup-register textarea').removeAttr('style').addClass('reverse');
            $.ajax({
                type: 'POST',
                url: "{{route('web.customer.register')}}",
                data: fd,
                // dataType: 'html',
                contentType: false,
                processData: false,
                statusCode: {
                    422: function(response) {
                        var json = JSON.parse(response.responseText);
                        if (json) {
                            $('#popup-register span.error-message').empty();
                            $.each(json.errors, function(key, val) {
                                $('#popup-register #' + key).removeClass('reverse').css('border-bottom', '1px solid red' );
                                $('#popup-register #' + key + '_error').html(val[0]).css('display', 'block')
                            });
                        }
                    },
                    200: function(response) {
                        if (response.error === 1) {
                            Swal.fire(
                                response.data.title,
                                response.data.mess,
                                'error'
                            )
                        } else {

                            $('.popup').fadeOut();
                            $('.popup-content').removeClass('active');

                            $('#popupDoneRegister').fadeIn();
                            $('#popupDoneRegister').find('.popup-content').addClass('active');
                            $('#popup-register span.error-message').empty();
                            $('.popup-content').removeClass('active');
                            $('#popup-register input[type="text"]').val('');
                            $('#popup-register input[type="password"]').val('');
                            $('#popup-register textarea').val('');
                            $('#popup-register .text-danger').empty();
                        }

                    }
                },
                beforeSend: function() {

                }
            }).done(function(json) {

            });
        })
    </script>
@endpush