<div class="col-md-6 col-lg-3 col-xl-3">
    <h3>{{__('site.theodoichungtoi')}}</h3>
    <div class="socials">
        <a target="_blank" href="{{ $settings['social']['facebook_name'] ?? '#' }}"><img
                src="{{asset('template/images/icon-fb.png')}}" alt=""/></a>
        <a target="_blank" href="{{ $settings['social']['youtube'] ?? '#' }}"><img
                src="{{asset('template/images/icon-ytb.png')}}" alt=""/></a>
        <a target="_blank" href="{{ $settings['social']['messenger'] ?? '#' }}"><img
                src="{{asset('template/images/icon-mess.png')}}" alt=""/></a>
        <a target="_blank" href="{{ $settings['social']['instagram'] ?? '#' }}"><img
                src="{{asset('template/images/icon-in.png')}}" alt=""/></a>
    </div>
</div>