<div id="popupLoginClient" class="popup">
    <div class="popup-overlay"></div>
    <div class="popup-absolute">
        <div class="popup-relative">
            <div class="popup-content">
                <div class="popup-login-regis--main position-relative">
                    <!-- button close popup -->

                    <a href="javascript:;" class="js-close-popup">
                        <img src="{{asset('template/public/images/icon/ic-close-popup.png')}}" alt="ic-close-popup" />
                    </a>

                    <div class="style--radius-border">
                        <div class="navtab">
                            <a href="javascript:;" title="" class="navtab-item active" data-tabpopup="popupLoginClient">ĐĂNG nhập</a>
                        </div>

                        <div class="box--login-regis-content">
                            <h2 class="title--box">Đăng nhập để truy cập tài khoản {{env('APP_NAME')}} của bạn</h2>
                            {!! Form::open(['url' => '', 'id' => 'popup-login', 'class' => 'form--actions']) !!}
                                <span class="error-message" id="err_error">{{ $errors->has('err') ? $errors->first('err') : '' }}</span>

                                <span class="input-name"> Email<span class="red">*</span> </span>
                                <input type="text" name="login_email" id="login_email" class="{{$errors->has('login_email') ? 'is-invalid' : 'reverse'}}" value="{{old('login_email')}}" placeholder="" />
                                <span class="error-message" id="login_email_error">{{ $errors->has('login_email') ? $errors->first('login_email') : '' }}</span>

                                <span class="input-name"> Mật khẩu <span class="red">*</span> </span>
                                <div class="js-toggle-password position-relative">
                                    <input type="password" name="login_password" id="login_password" class="{{$errors->has('login_password') ? 'is-invalid' : 'reverse'}}"  placeholder="" />
                                </div>
                                <span class="error-message" id="login_password_error">{{ $errors->has('login_password') ? $errors->first('login_password') : '' }}</span>

                                <div class="actions-sub">
                                    <a href="{{route('web.customer.show_forget_password')}}" class="action-link js--action-forgot" data-tabpopup="popupForgotPassword">Quên mật khẩu?</a>
                                </div>

                                <button class="btn-submit" id="popup-login-click">đăng nhập</button>
                                <div class="line--text-center">
                                    <span>Hoặc đăng nhập bằng :</span>
                                </div>

                                <div class="d-block">
                                    <a href="{{route('web.customer.google.login')}}" class="btn btn-light btn-block mb-8pt text-danger">
                                        <span class="fab fa-google icon--left"></span>
                                        Continue with Google
                                    </a>
                                    <a href="{{route('web.customer.facebook.login')}}" class="btn btn-light btn-block mb-24pt" style="color: #2D88FF">
                                        <span class="fab fa-facebook-f icon--left"></span>
                                        Continue with Facebook
                                    </a>
                                </div>
                            {!! Form::close() !!}
                        </div>

                        <a href="{{route('web.customer.show_register')}}" class="bottom-methods">
                            Đăng ký ngay
                            <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.901855 0.0976561L0.901854 1.76982L5.67328 5.96661L0.901854 10.1634L0.901854 11.9011L6.90186 6.72072L6.90186 5.27807L0.901855 0.0976561Z" fill="#006AC5" />
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('js_bot_all')
    <script>
        $('#popup-login-click').on('click', function(e) {
            e.preventDefault();
            var form = $('#popup-login');
            var data = $(form).serializeArray();
            var fd = new FormData();
            if (data.length > 0){
                $.each(data, function (key, value) {
                    fd.append(value.name, value.value);
                })
            }
            fd.append('previous_url', '{{route('web.cart.index')}}' /*window.location.href*/)
            $('#popup-login input').removeAttr('style').addClass('reverse');
            $('#popup-login textarea').removeAttr('style').addClass('reverse');
            $.ajax({
                type: 'POST',
                url: "{{route('web.customer.login')}}",
                data: fd,
                // dataType: 'html',
                contentType: false,
                processData: false,
                statusCode: {
                    422: function(response) {
                        var json = JSON.parse(response.responseText);
                        if (json) {
                            $('#popup-login span.error-message').empty();
                            $.each(json.errors, function(key, val) {
                                $('#popup-login #' + key).removeClass('reverse').css('border-bottom', '1px solid red' );
                                $('#popup-login #' + key + '_error').html(val[0]).css('display', 'block')
                            });
                        }
                    },
                    200: function(response) {
                        if (response.error === 1) {
                            Swal.fire(
                                response.data.title,
                                response.data.mess,
                                'error'
                            )
                        } else {


                            $('.popup').fadeOut();
                            $('.popup-content').removeClass('active');

                            $('#popupDonelogin').fadeIn();
                            $('#popupDonelogin').find('.popup-content').addClass('active');
                            $('#popup-login span.error-message').empty();
                            $('#popup-login input[type="text"]').val('');
                            $('#popup-login textarea').val('');
                            $('#popup-login .text-danger').empty();

                            shop.redirect(response.data.url)
                        }

                    }
                },
                beforeSend: function() {

                }
            }).done(function(json) {

            });
        })
    </script>
@endpush