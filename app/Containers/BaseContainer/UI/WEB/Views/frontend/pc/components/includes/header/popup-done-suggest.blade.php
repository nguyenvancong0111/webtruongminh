<div id="popupDoneSuggest" class="popup popup--forgot-password">
    <div class="popup-overlay"></div>
    <div class="popup-absolute">
        <div class="popup-relative">
            <div class="popup-content">
                <div class="popup-login-regis--main position-relative">
                    <!-- button close popup -->

                    <a href="javascript:;" class="js-close-popup">
                        <img src="{{asset('template/images/ic-close-popup.png')}}" alt="ic-close-popup" />
                    </a>

                    <div class="style--radius-border">
                        <div class="box--login-regis-content">
                            <div class="box-title">
                                <img src="{{asset('template/images/ticket.svg')}}" class="d-block mx-auto mb-4 text-center" alt="ticket" />

                                <h2 class="title">GỬI YÊU CẦU THÀNH CÔNG</h2>

                                <div class="sub">
                                    Vui lòng kiểm tra lại email để thiết lập lại mật khẩu
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>