<div id="popupForgotPassword" class="popup popup--forgot-password">
    <div class="popup-overlay"></div>
    <div class="popup-absolute">
        <div class="popup-relative">
            <div class="popup-content">
                <div class="popup-login-regis--main position-relative">
                    <!-- button close popup -->

                    <a href="javascript:;" class="js-close-popup">
                        <img src="{{asset('template/images/ic-close-popup.png')}}" alt="ic-close-popup" />
                    </a>

                    <div class="style--radius-border">
                        <div class="box--login-regis-content">
                            <div class="box-title">
                                <h2 class="title">QUÊN MẬT KHẨU</h2>

                                <div class="sub">
                                    Nhập email của bạn để đặt lại mật khẩu
                                </div>
                            </div>
                            {!! Form::open(['url' => '', 'id' => 'popup-reset-password', 'class' => 'form--actions']) !!}
                                <span class="input-name"> Email <span class="red">*</span> </span>
                                <input type="text" name="popup_reset_password_email" id="popup_reset_password_email" class="{{$errors->has('popup_reset_password_email') ? 'is-invalid' : 'reverse'}}" value="{{old('popup_reset_password_email')}}" placeholder="Nhập email của bạn" />
                                <span class="error-message" id="popup_reset_password_email_error">{{ $errors->has('popup_reset_password_email') ? $errors->first('popup_reset_password_email') : '' }}</span>

                                <button type="button" class="btn-submit" id="popup-reset-click" {{--data-tabpopup="popupDoneSuggest"--}}>gửi yêu cầu</button>
                            {!! Form::close() !!}

                            <div class="sub-option">
                                Bạn đã nhớ lại mật khẩu ?
                                <a href="javascript:;" class="js--action-login" data-tabpopup="popupLoginClient">
                                    Đăng nhập ngay
                                    <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M0.901855 0.0976561L0.901854 1.76982L5.67328 5.96661L0.901854 10.1634L0.901854 11.9011L6.90186 6.72072L6.90186 5.27807L0.901855 0.0976561Z" fill="#006AC5" />
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('js_bot_all')
    <script>
        $('#popup-reset-click').on('click', function(e) {
            e.preventDefault();
            var form = $('#popup-reset-password');
            var data = $(form).serializeArray();
            var fd = new FormData();
            if (data.length > 0){
                $.each(data, function (key, value) {
                    fd.append(value.name, value.value);
                })
            }
            $('#popup-reset-password input').removeAttr('style').addClass('reverse');
            $('#popup-reset-password textarea').removeAttr('style').addClass('reverse');
            $.ajax({
                type: 'POST',
                url: "{{route('web.customer.reset_password')}}",
                data: fd,
                // dataType: 'html',
                contentType: false,
                processData: false,
                statusCode: {
                    422: function(response) {
                        var json = JSON.parse(response.responseText);
                        if (json) {
                            $('#popup-reset-password span.error-message').empty();
                            $.each(json.errors, function(key, val) {
                                $('#popup-reset-password #' + key).removeClass('reverse').css('border-bottom', '1px solid red' );
                                $('#popup-reset-password #' + key + '_error').html(val[0]).css('display', 'block')
                            });
                        }
                    },
                    200: function(response) {
                        if (response.error === 1) {
                            Swal.fire(
                                response.data.title,
                                response.data.mess,
                                'error'
                            )
                        } else {

                            $('.popup').fadeOut();
                            $('.popup-content').removeClass('active');

                            $('#popupDoneSuggest').fadeIn();
                            $('#popupDoneSuggest').find('.popup-content').addClass('active');
                            $('#popup-reset-password span.error-message').empty();
                            $('#popup-reset-password input[type="text"]').val('');
                            $('#popup-reset-password textarea').val('');
                            $('#popup-reset-password .text-danger').empty();
                        }

                    }
                },
                beforeSend: function() {

                }
            }).done(function(json) {

            });
        })
    </script>
@endpush