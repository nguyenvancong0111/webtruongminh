@php($def = isset($def) ? $def : FunctionLib::tplShareGlobal('web', $settings ?? [])) @php($seoDefault = FunctionLib::getSeo('web', $def)) @php($versionMedia = $settings['other']['version'] ?? 0) <!DOCTYPE html>
    <html lang="{{ $def['lang'] }}" dir="ltr">

    <head>
        @include('basecontainer::frontend.pc.layouts.includes.meta')
        @include('basecontainer::frontend.pc.layouts.includes.head_media')
    </head>

    <body class="home page-template page-template-index page-template-index-php page page-id-10">
        <div class="mdk-header-layout js-mdk-header-layout">
            <div class="mdk-header-layout__content page-content " style="padding-top: 65px;">
                @yield('content')
            </div>
            <div class="overlay-body"></div>
        </div>

        @include('basecontainer::frontend.pc.layouts.includes.bottom_media')

        @yield('js-bottom')
    </body>
    </html>
