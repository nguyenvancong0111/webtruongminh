<div class="bg-dark border-top-2 mt-auto" style="z-index: -1">
    <div class="container page__container page-section d-flex flex-column">
        <div class="row">
            <div class="col-lg-6">
                <p class="text-white-70 brand mb-24pt">
                    <img class="brand-icon" src="" width="100" alt="{{@$settings['website']['site_name']}}" width="30" >
                </p>
                <ul class="uk-list uk-list-large list-unstyled mb-4 mb-lg-5">
                   
                </ul>
            </div>
            @if (isset($footerMenus[0]))
                @foreach ($footerMenus[0] as $footerMenuLvl1)
                    <div class="col-lg-3">
{{--                        <h2 class="title-footer">{{ $footerMenuLvl1->desc_lang->name }}</h2>--}}
                        @if (isset($footerMenus[$footerMenuLvl1->id]))
                            <ul class="uk-list uk-list-large list-unstyled mb-4 mb-lg-5">
                                @foreach ($footerMenus[$footerMenuLvl1->id] as $footerMenuLvl2)
                                    <li><a href="{{ $footerMenuLvl2->menu_link }}" class="uk-link-border text-white font-bold" style="font-size: 16px;">{{ $footerMenuLvl2->desc_lang->name }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                @endforeach
            @endif
        </div>
        <p class="small mt-n1 mb-3 text-center text-uppercase text-danger">UBGROUP giữ bản quyền mọi nội dung trên UB.NET, nghiêm cấm sao chép, buôn bán tài liệu bản quyền dưới mọi hình thức.</p>
        <p class="text-white-50 small mt-n1 mb-16pt text-center">Copyright 2019 &copy; All rights reserved.</p>

        <ul class="list list-unstyled d-flex flex-wrap list-link">
            <li><a href="#" class="text-white-50 mr-3 btn-link">ôn thi vietcombank</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">thi viết vietcombank</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">bidv tuyển dụng</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">đề thi vào ngân hàng</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">thi vào bidv</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">đề thi bidv</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">tuyển dụng vietcombank</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">luyện phỏng vấn vào ngân hàng</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">giao dịch viên</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">quan hệ khách hàng</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">bidv</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">luyện thi ngân hàng</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">ôn luyện vietcombank</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">đề thi viết ngân hàng</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">ôn thi ngân hàng</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">vietcombank</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">câu hỏi phỏng vấn bidv</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">phỏng vấn vào bidv</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">thi ngân hàng</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">luyện thi công chức thuế</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">luyện thi hải quan</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">luyện thi samsung</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">thẩm định tín dụng</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">excel cơ bản</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">excel nâng cao</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">Excel VBA</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">SQL</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">tin học văn phòng</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">toeic 450</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">tiếng anh cơ bản</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">toeic 150</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">luyện thi giáo viên</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">kỹ năng bán hàng</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">kỹ năng viết cv</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">kỹ năng đàm phán</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">luyện thi Ngân hàng Nhà nước</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">Luyện thi bộ tài chính</a></li>
            <li><a href="#" class="text-white-50 mr-3 btn-link">Công chức nhà nước</a></li>
        </ul>
    </div>
</div>