<div id="popupLoginLoyal" class="popup">
    <div class="popup-overlay"></div>
    <div class="popup-absolute">
        <div class="popup-relative">
            <div class="popup-content">
                <div class="popup-login-regis--main position-relative">
                    <!-- button close popup -->

                    <a href="javascript:;" class="js-close-popup">
                        <img src="{{asset('template/images/ic-close-popup.png')}}" alt="ic-close-popup" />
                    </a>

                    <div class="style--radius-border">
                        <div class="navtab">
                            <a href="javascript:;" title="" class="navtab-item active">ĐĂNG nhập</a>
                        </div>

                        <div class="box--login-regis-content">
                            <h2 class="title--box">Luật sư đăng nhập</h2>
                            {!! Form::open(['url' => '', 'id' => 'popup-login-lawyer', 'class' => 'form--actions']) !!}
                                @csrf
                                <span class="error-message" id="err_error">{{ $errors->has('err') ? $errors->first('err') : '' }}</span>

                                <span class="input-name"> Email <span class="red">*</span> </span>
                                <input type="text" name="login_popup_email_lawyer" id="login_popup_email_lawyer" class="{{$errors->has('login_popup_email_lawyer') ? 'is-invalid' : 'reverse'}}" value="{{old('login_popup_email_lawyer')}}" placeholder="Nhập email của bạn" />
                                <span class="error-message" id="login_popup_email_lawyer_error">{{ $errors->has('login_popup_email_lawyer') ? $errors->first('login_popup_email_lawyer') : '' }}</span>

                                <span class="input-name"> Mật khẩu <span class="red">*</span> </span>
                                <div class="js-toggle-password position-relative">
                                    <input type="password" name="login_popup_password_lawyer" id="login_popup_password_lawyer" class="{{$errors->has('login_popup_password_lawyer') ? 'is-invalid' : 'reverse'}}"  placeholder="Nhập mật khẩu của bạn" />

                                    <div class="password--show-hide">
                                        <a href="javascript:;" class="js-password-action hide active">
                                            <img src="{{asset('template/images/ic-hidepass.png')}}" alt="ic-hidepass" />
                                        </a>
                                        <a href="javascript:;" class="js-password-action show">
                                            <img src="{{asset('template/images/ic-showpass.png')}}" alt="ic-showpass" />
                                        </a>
                                    </div>
                                </div>
                                <span class="error-message" id="login_popup_password_lawyer_error">{{ $errors->has('login_popup_password_lawyer') ? $errors->first('login_popup_password_lawyer') : '' }}</span>

                                <div class="actions-sub">
                                    <label for="rememberPasswordLawyer">
                                        <input type="checkbox" name="login_popup_remember_lawyer" id="rememberPasswordLawyer" value="1" style="display: inline;width: auto; margin: 0;"/>
                                        <span>Ghi nhớ đăng nhập</span>
                                    </label>

                                    <a href="javascript:;" class="action-link js--action-forgot-loyal" data-tabpopup="popupForgotPasswordLoyal">Bạn quên mật khẩu?</a>
                                </div>

                                <button class="btn-submit mb-4" id="popup-lawyer-login-click">đăng nhập</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('js_bot_all')
    <script>
        $('#popup-lawyer-login-click').on('click', function(e) {
            e.preventDefault();
            var form = $('#popup-login-lawyer');
            var data = $(form).serializeArray();
            var fd = new FormData();
            if (data.length > 0){
                $.each(data, function (key, value) {
                    fd.append(value.name, value.value);
                })
            }
            fd.append('previous_url', '{{route('web.home.index')}}')
            // $('#popup-login-lawyer input').removeAttr('style').addClass('reverse');
            // $('#popup-login-lawyer textarea').removeAttr('style').addClass('reverse');

            $.ajax({
                type: 'POST',
                url: "{{route('web.lawyer.login')}}",
                data: fd,
                // dataType: 'html',
                contentType: false,
                processData: false,
                statusCode: {
                    422: function(response) {
                        var json = JSON.parse(response.responseText);
                        if (json) {
                            $('#popup-login-lawyer span.error-message').empty();
                            $.each(json.errors, function(key, val) {
                                $('#popup-login-lawyer #' + key).removeClass('reverse').css('border-bottom', '1px solid red' );
                                $('#popup-login-lawyer #' + key + '_error').html(val[0]).css('display', 'block')
                            });
                        }
                    },
                    200: function(response) {
                        if (response.error === 1) {
                            Swal.fire(
                                response.data.title,
                                response.data.mess,
                                'error'
                            )
                        } else {


                            $('.popup').fadeOut();
                            $('.popup-content').removeClass('active');
                            $('#popup-login-lawyer span.error-message').empty();
                            $('#popup-login-lawyer input[type="text"]').val('');
                            $('#popup-login-lawyer textarea').val('');
                            $('#popup-login-lawyer .text-danger').empty();

                            shop.redirect(response.data.url)
                        }

                    }
                },
                beforeSend: function() {

                }
            }).done(function(json) {

            });
        })
    </script>
@endpush