<div id="popupForgotPasswordLoyal" class="popup popup--forgot-password">
    <div class="popup-overlay"></div>
    <div class="popup-absolute">
        <div class="popup-relative">
            <div class="popup-content">
                <div class="popup-login-regis--main position-relative">
                    <!-- button close popup -->

                    <a href="javascript:;" class="js-close-popup">
                        <img src="{{asset('template/images/ic-close-popup.png')}}" alt="" />
                    </a>

                    <div class="style--radius-border">
                        <div class="box--login-regis-content">
                            <div class="box-title">
                                <h2 class="title">QUÊN MẬT KHẨU</h2>

                                <div class="sub">
                                    Nhập email của bạn để đặt lại mật khẩu
                                </div>
                            </div>

                            <form action="" class="form--actions">
                                <span class="input-name"> Email <span class="red">*</span> </span>

                                <input type="mail" name="" id="" placeholder="Nhập email của bạn" />

                                <span class="error-message">Error !</span>

                                <button type="button" class="btn-submit js-action-submit" data-tabpopup="popupDoneSuggest">
                                    gửi yêu cầu
                                </button>
                            </form>

                            <div class="sub-option">
                                Bạn đã nhớ lại mật khẩu ?
                                <a href="javascript:;" class="js--action-login" data-tabpopup="popupLoginLoyal">
                                    Đăng nhập ngay
                                    <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M0.901855 0.0976561L0.901854 1.76982L5.67328 5.96661L0.901854 10.1634L0.901854 11.9011L6.90186 6.72072L6.90186 5.27807L0.901855 0.0976561Z" fill="#006AC5" />
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>