<div id="header" class="mdk-header js-mdk-header mb-0" data-fixed data-effects="waterfall">
    <div class="mdk-header__content">
        <div class="navbar navbar-expand navbar-light bg-white navbar-shadow" id="default-navbar" data-primary>
            <div class="container page__container">
                <!-- Navbar Brand -->
                <a href="{{ route('web.home.index') }}" class="navbar-brand mr-16pt">
                    <span class="navbar-brand-icon mr-0 mr-lg-8pt" style="width: 100px">
                        <img src="{{ ImageURL::getImageUrl(@$settings['website']['logo'], 'setting', 'original') }}"
                            alt="{{ @$settings['website']['site_name'] }}" class="img-fluid" />
                    </span>
                </a>

                <!-- Navbar toggler -->
                @if (isset($headerMenus[0]))
                    <ul class="nav navbar-nav d-none d-sm-flex flex justify-content-start ml-8pt">
                        @foreach ($headerMenus[0] as $headerMenuLvl1)
                            <li
                                class="nav-item {{ $headerMenuLvl1->getActiveMenuClass() }} @if (isset($headerMenus[$headerMenuLvl1->id])) dropdown @endif">
                                <a href="{{ $headerMenuLvl1->menu_link }}"
                                    @if (@$headerMenuLvl1->no_follow === 1) rel="nofollow" @endif
                                    @if (@$headerMenuLvl1->newtab === 1) target="_blank" @endif
                                    class="nav-link @if (isset($headerMenus[$headerMenuLvl1->id])) dropdown-toggle @endif"
                                    @if (isset($headerMenus[$headerMenuLvl1->id])) data-toggle="dropdown" data-caret="false" @endif>{{ $headerMenuLvl1->desc_lang->name }}</a>
                                @if (isset($headerMenus[$headerMenuLvl1->id]))
                                    <div class="dropdown-menu">
                                        @foreach ($headerMenus[$headerMenuLvl1->id] as $headerMenuLvl2)
                                            <a href="{{ $headerMenuLvl2->menu_link }}"
                                                @if (@$headerMenuLvl2->no_follow === 1) rel="nofollow" @endif
                                                @if (@$headerMenuLvl2->newtab === 1) target="_blank" @endif
                                                class="dropdown-item">{{ $headerMenuLvl2->desc_lang->name }}</a>
                                        @endforeach
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                @endif

                <form class="search-form form-control-rounded navbar-search d-none d-lg-flex mr-16pt position-relative" style="max-width: 230px;">
                    <button class="btn" type="submit" onclick="searchData('{{ route('web.search.index') }}?key='+$('#search_pc').val())"><i class="material-icons">search</i></button>
                    <input type="text" name="search" id="search_pc" class="form-control" placeholder="Tìm kiếm khóa học ..." />
                    <div class="result-search"></div>
                </form>

                <ul class="nav navbar-nav ml-auto mr-0">
                    <li class="nav-item dropdown">
                        @if (\Auth::guard('customer')->check())
                            <a href="javascript:;" class="nav-link dropdown-toggle" data-toggle="dropdown"
                                data-caret="false">
                                <img src="{{ !empty(auth('customer')->user()->avatar) ? \ImageURL::getImageUrl(auth('customer')->user()->avatar, 'customer', '70x70') : asset('template/images/user.png') }}"
                                    alt="" width="50" class="mr-2 rounded-circle">
                                <p>{{ \Auth::guard('customer')->user()->fullname }}</p>
                            </a>
                            <style>
                                .dropdown-menu .dropdown-item:hover {
                                    background-color: transparent;
                                    color: #272c33;
                                    font-weight: 700;
                                    position: relative;
                                    text-decoration: none;
                                }

                                .dropdown-menu .dropdown-item:hover:after {
                                    left: -1px;
                                    border-radius: 1px;
                                    background-color: #5567ff;
                                    position: absolute;
                                    top: 0;
                                    bottom: 0;
                                    width: 4px;
                                    content: "";
                                }
                            </style>
                            <div class="dropdown-menu">
                                <a href="{{ route('web.customer.profile') }}" class="dropdown-item"><i
                                        class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Thông tin tài
                                    khoản</a>
                                <a href="{{ route('web.customer.course_management_view') }}" class="dropdown-item"><i
                                        class="fa fa-list" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Danh sách khóa
                                    học</a>
                                <a href="{{ route('web.customer.coupons_view') }}" class="dropdown-item"><i
                                        class="fa fa-percent" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Danh sách
                                    Coupons</a>
                                <a href="{{ route('web.customer.transaction_history_view') }}" class="dropdown-item"><i
                                        class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Lịch sử
                                    giao dịch</a>
                                <a href="{{ route('web.customer.change_password') }}" class="dropdown-item"><i
                                        class="fa fa-lock" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Thay đổi mật
                                    khẩu</a>
                                <a href="{{ route('web.customer.logout') }}" class="dropdown-item">
                                    <svg width="17" height="18" viewBox="0 0 17 18" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <rect x="6.04688" y="0.0644531" width="4.0315" height="11" rx="2.01575"
                                            fill="#828282"></rect>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M4.03149 2.63477C1.62146 4.018 0 6.60338 0 9.56451C0 13.9828 3.60992 17.5645 8.06299 17.5645C12.5161 17.5645 16.126 13.9828 16.126 9.56451C16.126 6.60338 14.5045 4.018 12.0945 2.63477V9.03299C12.0945 11.2595 10.2895 13.0645 8.06299 13.0645C5.83646 13.0645 4.03149 11.2595 4.03149 9.03299V2.63477Z"
                                            fill="#828282"></path>
                                    </svg>
                                    &nbsp;&nbsp;&nbsp;Đăng xuất
                                </a>
                            </div>
                        @elseif(\Auth::guard('lecturers')->check())
                            <a href="javascript:;" class="nav-link dropdown-toggle" data-toggle="dropdown"
                                data-caret="false">
                                <img src="{{ !empty(auth('lecturers')->user()->avatar) ? \ImageURL::getImageUrl(auth('lecturers')->user()->avatar, 'lecturers', '70x70') : asset('template/images/user.png') }}"
                                    alt="" width="50" class="mr-2 rounded-circle">
                                <p>{{ \Auth::guard('lecturers')->user()->fullname }}</p>
                            </a>
                            <style>
                                .dropdown-menu .dropdown-item:hover {
                                    background-color: transparent;
                                    color: #272c33;
                                    font-weight: 700;
                                    position: relative;
                                    text-decoration: none;
                                }

                                .dropdown-menu .dropdown-item:hover:after {
                                    left: -1px;
                                    border-radius: 1px;
                                    background-color: #5567ff;
                                    position: absolute;
                                    top: 0;
                                    bottom: 0;
                                    width: 4px;
                                    content: "";
                                }
                            </style>
                            <div class="dropdown-menu">
                                <a href="{{ route('web.lecturers.profile') }}" class="dropdown-item"><i
                                        class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Thông tin tài
                                    khoản</a>
                                <a href="{{ route('web.lecturers.course_management_view') }}" class="dropdown-item"><i
                                        class="fa fa-list" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Danh sách khóa
                                    học</a>
                                <a href="{{ route('web.lecturers.income_statement_view') }}" class="dropdown-item"><i class="fa fa-percent"
                                        aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Báo cáo thu nhập</a>
                                <a href="{{ route('web.lecturers.change_password') }}" class="dropdown-item"><i
                                        class="fa fa-lock" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Thay đổi mật
                                    khẩu</a>
                                <a href="{{ route('web.lecturers.logout') }}" class="dropdown-item">
                                    <svg width="17" height="18" viewBox="0 0 17 18" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <rect x="6.04688" y="0.0644531" width="4.0315" height="11"
                                            rx="2.01575" fill="#828282"></rect>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M4.03149 2.63477C1.62146 4.018 0 6.60338 0 9.56451C0 13.9828 3.60992 17.5645 8.06299 17.5645C12.5161 17.5645 16.126 13.9828 16.126 9.56451C16.126 6.60338 14.5045 4.018 12.0945 2.63477V9.03299C12.0945 11.2595 10.2895 13.0645 8.06299 13.0645C5.83646 13.0645 4.03149 11.2595 4.03149 9.03299V2.63477Z"
                                            fill="#828282"></path>
                                    </svg>
                                    &nbsp;&nbsp;&nbsp;Đăng xuất
                                </a>
                            </div>
                        @else
                            <a href="javascript:;" class="nav-link dropdown-toggle" data-toggle="dropdown"
                                data-caret="false"><i class="material-icons">lock</i></a>
                            <div class="dropdown-menu">
                                <a href="{{ route('web.customer.show_register') }}" class="dropdown-item">Đăng ký</a>
                                <a href="{{ route('web.customer.show_login') }}" class="dropdown-item">Đăng nhập</a>
                            </div>
                        @endif
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@if (!\Auth::guard('customer')->check())
    @include('basecontainer::frontend.pc.components.includes.header.popup-login-customer')
@endif