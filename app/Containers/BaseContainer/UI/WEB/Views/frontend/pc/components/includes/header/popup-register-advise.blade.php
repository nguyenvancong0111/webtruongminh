<div id="dangkytuvan" class="popup">
    <div class="popup-overlay"></div>
    <div class="popup-absolute">
        <div class="popup-relative">
            <div class="popup-content">
                <div class="popup-login-regis--main position-relative">
                    <!-- button close popup -->

                    <a href="javascript:;" class="js-close-popup">
                        <img src="{{asset('template/images/ic-close-popup.png')}}" alt="ic-close-popup" />
                    </a>

                    <div class="style--radius-border">
                        <div class="section--form-contact p-0">
                            <div class="box-top-title">
                                <h2 class="title--top-main text-left text-blue">ĐĂNG KÝ TƯ VẤN</h2>

                                <div class="desc--top-main text-left">
                                    Hãy để lại thông tin yêu cầu nhận tư vấn
                                </div>
                            </div>

                            {!! Form::open(['url' => '', 'id' => 'contact-popup', 'class' => 'form--contact']) !!}
                            <div class="form-bg">
                                <h3 class="title-form">
                                    THÔNG TIN LIÊN HỆ
                                </h3>

                                <div class="form--starting form--flex">

                                    <div class="box-input">
                                        <span class="value-name">Họ và tên</span>
                                        <input type="text" name="contact_popup_fullname" id="contact_popup_fullname" class="{{$errors->has('contact_popup_fullname') ? 'is-invalid' : 'reverse'}}" value="{{old('contact_popup_fullname')}}" placeholder="Nhập họ và tên người cần tư vấn...">
                                            <small id="contact_popup_fullname_page" class="invalid-feedback text-danger"><i>{{ $errors->has('contact_popup_fullname') ? $errors->first('contact_popup_fullname') : '' }}</i></small>
                                    </div>

                                    <div class="box-input">
                                        <span class="value-name"> Số điện thoại </span>
                                        <input type="text" name="contact_popup_phone" id="contact_popup_phone" class="{{$errors->has('contact_popup_phone') ? 'is-invalid' : 'reverse'}}" value="{{old('contact_popup_phone')}}" placeholder="Nhập số điện thoại liên hệ" onkeypress="return shop.numberOnly()">
                                        <small id="contact_popup_phone_page" class="invalid-feedback text-danger"><i>{{ $errors->has('contact_popup_phone') ? $errors->first('contact_popup_phone') : '' }}</i></small>
                                    </div>

                                    <div class="box-input">
                                        <span class="value-name">Email</span>
                                        <input type="text"name="contact_popup_email" id="contact_popup_email" class="{{$errors->has('contact_popup_email') ? 'is-invalid' : 'reverse'}}" value="{{old('contact_popup_email')}}" placeholder="Nhập địa chỉ email liên hệ">
                                        <small id="contact_popup_email_page" class="invalid-feedback text-danger"><i>{{ $errors->has('contact_popup_email') ? $errors->first('contact_popup_email') : '' }}</i></small>
                                    </div>
                                </div>
                            </div>

                            <div class="form-bg">
                                <h3 class="title-form">THÔNG TIN TƯ VẤN</h3>
                                <div class="flex-box2">
                                    <div class="box--value-name">
                                        <span class="value-name">Chọn dịch vụ:</span>
                                    </div>
                                    <div class="box--options">
                                        <label for="onlineOptionFormPopup">
                                            <input type="radio" name="contact_popup_service_type" id="onlineOptionFormPopup" value="1" checked>
                                            <span class="checked-input"></span>
                                            <span>Tư vấn trực tuyến (online)</span>
                                        </label>

                                        <label for="offlineOptionFormPopup">
                                            <input type="radio" name="contact_popup_service_type" value="2" id="offlineOptionFormPopup">
                                            <span class="checked-input"></span>
                                            <span>Tư vấn trực tiếp (offline)</span>
                                        </label>
                                        <small id="contact_popup_service_type_page" class="invalid-feedback text-danger"><i>{{ $errors->has('contact_popup_service_type') ? $errors->first('contact_popup_service_type') : '' }}</i></small>

                                        <div class="div-content">
                                            <div class="address">
                                                Chọn địa điểm tư vấn
                                                <a href="javascript:;" class="hover-tooltip">
                                                    <img src="{{asset('template/images/infor-ic.png')}}" alt="">
                                                    <div class="div-hover">
                                                        Lựa chọn 1 địa điểm tư vấn trực tiếp
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="doing--at-office">
                                                <div class="title--offline">
                                                    Tại văn phòng của chúng tôi
                                                </div>

                                                <div class="box--label-flex">
                                                    <label for="hanoi-city-form-popup">
                                                        <input type="radio" name="contact_popup_consulting_office" id="hanoi-city-form-popup" value="Văn phòng Hà Nội">
                                                        <span class="checked-input"></span>

                                                        <div class="box-info">
                                                            <span class="title">Văn phòng Hà Nội</span>
                                                            <span class="sub">
                                                                    {{@$settings['contact']['address_hn']}}
                                                                </span>
                                                        </div>

                                                    </label>

                                                    <label for="hcm-city-form-popup">
                                                        <input type="radio" name="contact_popup_consulting_office" id="hcm-city-form-popup" value="Văn phòng TP. Hồ Chí Minh">
                                                        <span class="checked-input"></span>

                                                        <div class="box-info">
                                                            <span class="title">Văn phòng TP. Hồ Chí Minh</span>
                                                            <span class="sub">
                                                                    {{@$settings['contact']['address_hcm']}}
                                                                </span>
                                                        </div>

                                                    </label>
                                                </div>

                                            </div>

                                            <div class="doing--at-another">
                                                <div class="title--offline">Tại địa điểm khác</div>
                                                <input type="text" name="contact_popup_address_other" id="contact_popup_address_other" class="{{$errors->has('contact_popup_address_other') ? 'is-invalid' : 'reverse'}}" value="{{old('contact_popup_address_other')}}" placeholder="Nhập địa điểm hẹn tư vấn trực tiếp...">
                                                <small id="contact_popup_address_other_page" class="invalid-feedback text-danger"><i>{{ $errors->has('contact_popup_address_other_page') ? $errors->first('contact_popup_address_other_page') : '' }}</i></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <textarea name="contact_popup_content" id="contact_popup_content" class="{{$errors->has('contact_popup_content') ? 'is-invalid' : 'reverse'}}" placeholder="Nhập thêm thông tin mô tả về dịch vụ bạn cần tư vấn...">{{old('contact_popup_content')}}</textarea>
                            <small id="contact_popup_content_page" class="invalid-feedback text-danger"><i>{{ $errors->has('contact_popup_content') ? $errors->first('contact_popup_content') : '' }}</i></small>


                            <div class="d-block text-center mt-4">
                                <div class="button-bg-main">
                                    <a href="javascript:;" title="" class="btn-action" id="contact-popup-click">GỬI YÊU CẦU</a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('js_bot_all')
    <script>
        $('#contact-popup-click').on('click', function(e) {
            e.preventDefault();
            var form = $('#contact-popup');
            var data = $(form).serializeArray();
            var fd = new FormData();
            if (data.length > 0){
                $.each(data, function (key, value) {
                    fd.append(value.name, value.value);
                })
            }
            fd.append('type', 'home');
            $('#contact-popup input').removeAttr('style').addClass('reverse');
            $('#contact-popup textarea').removeAttr('style').addClass('reverse');
            $.ajax({
                type: 'POST',
                url: "{{route('web.ajax.popup')}}",
                data: fd,
                // dataType: 'html',
                contentType: false,
                processData: false,
                statusCode: {
                    422: function(response) {
                        var json = JSON.parse(response.responseText);
                        if (json) {
                            $('#contact-popup small.text-danger').empty();
                            $.each(json.errors, function(key, val) {
                                $('#contact-popup #' + key).removeClass('reverse').css('border-bottom', '1px solid red' );
                                $('#contact-popup #' + key + '_page').html(val[0]).css('display', 'block')
                            });
                        }
                    },
                    200: function(response) {
                        if (response.error === 1) {
                            Swal.fire(
                                response.data.title,
                                response.data.mess,
                                'error'
                            )
                        } else {
                            Swal.fire(
                                response.data.title,
                                response.data.mess,
                                'success'
                            ).then((result) => {
                                $('.popup').fadeOut();
                                $('.popup-content').removeClass('active');
                                $('#contact-popup input[type="text"]').val('');
                                $('#contact-popup textarea').val('');
                                $('#contact-popup .text-danger').empty();
                            });
                        }

                    }
                },
                beforeSend: function() {

                }
            }).done(function(json) {

            });
        })
    </script>
@endpush