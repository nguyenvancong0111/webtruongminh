@if(isset($breadcrumb) && !empty($breadcrumb))
    <div class="breadcrumb-inner">
        @foreach ($breadcrumb as $item)
            @if($item['isCurrent'])
                <a href="javascript:;" title="" class="breadcrumb-inner-item">{{ $item['title'] }}</a>
            @else
                <a href="{{ $item['link'] }}" title="" class="breadcrumb-inner-item">{{ $item['title'] }}</a>
            @endif
        @endforeach
    </div>
@endif

