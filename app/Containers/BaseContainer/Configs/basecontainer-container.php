<?php

return [
    'admin' => [
        'name' => "BigCore Admin UI",
        'css' => [
            'admin/css/jquery-ui.min.css',
            'admin/css/style.css',
            // Media
            'admin/css/font-awesome.min.css',
            'admin/css/jquery.datetimepicker.min.css',
            'admin/css/nprogress.css',
            // CSS
            'admin/css/select2.min.css',
            'admin/css/customize.css',
            'admin/css/dropify.min.css', // Upload and Preview Image
        ],
        'js' => [
            'admin/js/library/jquery-3.5.1.min.js',
            'admin/vendors/@coreui/coreui/js/coreui.bundle.min.js',
            'admin/vendors/@coreui/coreui/js/coreui.bundle.min.map',
            'admin/js/library/jquery-ui.min.js',
            'admin/js/library/jquery.datetimepicker.min.js',
            'admin/js/library/select2.min.js',
            'admin/js/library/sweetalert2@10.js',
            'admin/js/library/nprogress.min.js',
            'admin/js/library/dropify.min.js',
//            'admin/js/vue.js',
            'js/vue.js',
            'admin/js/library/jquery.i18n.min.js',
            'admin/js/library/jquery.i18n.messagestore.min.js',
            'js/core.js',
            'admin/js/tooltips.js',
            'admin/js/helpers.js',
            'admin/js/admin.js',
            'admin/js/lang.js',

            // Setup
            'admin/js/init/ajax-setup.js',
            'admin/js/init/sidebar-setup.js',
            'admin/js/init/plugin-setup.js',
            'admin/js/init/submit-setup.js',

            // Customize, hàm dùng chung
            'admin/js/customize.js',

            // Customer
            'admin/js/customer.js',
            'admin/js/customer-group.js',
            // 'admin/js/gallery.js',
            'admin/js/order.js',
            'admin/js/comment.js',
            'admin/js/discount.js',
            'admin/js/discount-list.js',
            'admin/js/iframe.js',
        ],
        'media' => [
            'admin/node_modules/font-awesome/css/font-awesome.min.css',
            // 'admin/node_modules/simple-line-icons/css/simple-line-icons.css',
            'admin/css/jquery.datetimepicker.min.css'
        ],
        'favicon' => 'template/images/logo.png'
    ],
    'web' => [
        'name' => "Website",
        'css' => [
            'template/public/css/boostrap.css',
            'template/public/css/swiper-bundle.css',
            'template/public/css/fontawesome.css',
            'template/public/css/style.css',
            // 'template/public/css/preloader.css',
            // 'template/public/css/app.css',
            // 'template/public/css/customs.css',
            // 'template/public/css/popup.css',
            // 'template/public/css/profile.css',
        ],
        'js' => [
            'template/public/js/popper.min.js',
            'template/public/js/bootstrap.min.js',
            'template/js/bootstrap-bundle.js',
            'template/public/js/meanmenu.js',
//            'template/public/vendor/dom-factory.js',
            'template/public/js/main.js',
            // 'template/public/js/app.js',
            // 'template/public/js/preloader.js',

            // // Customize, hàm dùng chung
            // 'js/vue.js',
            // 'js/lazyload.js',
            // 'js/sweetalert2.all.min.js',
            // 'js/URLSearchParams.js',
            // 'js/lodash.min.js',
            // 'js/core.js',
            // 'js/global.js',
        ],
        'media' => [],
        'override' => [
            'css' => [
//                'template/css/custom.css'
            ],
            'js' => [
                // 'template/js/customize.js'
            ]
        ],
        'favicon' => 'template/images/logo.png'
    ],
    'mobile' => [
        'name' => "Mobile",
        'css' => [
        ],
        'js' => [
        ],
        'media' => [],
        'override' => [
            'css' => [],
            'js' => []
        ],
        'favicon' => 'template/images/logo.png'
    ],
    'mobile_alias' => 'mobile',
    'desktop_alias' => 'pc'
];
