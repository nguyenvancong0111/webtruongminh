<?php

namespace App\Containers\Questions\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateViewItemAction extends Action
{
    public function run(int $id, array $data)
    {
        $data = Apiato::call('Question@UpdateViewItemTask', [$id, $data]);
        return $data;
    }
}
