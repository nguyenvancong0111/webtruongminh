<?php

namespace App\Containers\Questions\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetQuestionRelatedAction extends Action
{
    public function run(array $category =  [], array $filters = [], int $limit = 20, bool $isPanination = false, Language $currentLang = null, array $orderBy = ['created_at' => 'desc', 'id' => 'desc'], int $current_page): iterable {

        return $this->remember(function () use ($category, $filters, $limit, $isPanination, $currentLang,$orderBy, $current_page) {
            return $this->call('Question@FrontEnd\GetQuestionRelatedTask', [
                $category, $filters, $limit, $isPanination, $currentLang, $orderBy, $current_page
            ]);
        });
    }
}
