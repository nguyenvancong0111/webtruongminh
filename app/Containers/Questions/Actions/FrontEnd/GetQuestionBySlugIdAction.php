<?php

namespace App\Containers\Questions\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\Questions\Models\Questions;
use App\Containers\Questions\Tasks\GetQuestionBySlugIdTask;
use App\Ship\Parents\Actions\Action;

class GetQuestionBySlugIdAction extends Action
{
    protected $getQuestionBySlugIdTask;
    public function __construct(GetQuestionBySlugIdTask $getQuestionBySlugIdTask)
    {
        parent::__construct();
        $this->getQuestionBySlugIdTask = $getQuestionBySlugIdTask;
    }
    public function run(int $questionId, string $slug, Language $currentLang = null): ?Questions
    {
        return $this->remember(function () use ($questionId, $slug, $currentLang) {
            return $this->getQuestionBySlugIdTask->run($questionId, $slug, $currentLang);
        });
    }
}
