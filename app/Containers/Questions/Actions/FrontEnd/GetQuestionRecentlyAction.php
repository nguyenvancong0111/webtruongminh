<?php

namespace App\Containers\Questions\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetQuestionRecentlyAction extends Action
{
    public function run(
        array $filter = [],
        $type,
        int $limit = 20,
        bool $isPanination = false,
        Language $currentLang = null,
        array $orderBy = ['created_at' => 'desc', 'id' => 'desc']

    ): iterable {

        return $this->remember(function () use ($filter,$type, $limit, $isPanination, $currentLang,$orderBy) {
            return $this->call('Question@FrontEnd\GetQuestionRecentlyTask', [
                $filter,
                $type,
                $limit,
                $isPanination,
                $currentLang,
                $orderBy

            ]);
        }, null, [5]);
    }
}
