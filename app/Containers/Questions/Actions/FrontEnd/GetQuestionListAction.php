<?php


namespace App\Containers\Questions\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetQuestionListAction extends Action
{
    public function run(
        array $filters = [],
        array $orderBy = ['created_at' => 'desc', 'id' => 'desc'],
        int $limit = 20,
        bool $skipPagination = false,
        Language $currentLang = null,
        array $externalData = [],
        int $currentPage = 1
    ): iterable {
        return $this->remember(function () use ($filters, $orderBy, $limit, $skipPagination, $currentLang, $externalData, $currentPage) {
            return $this->call('Question@FrontEnd\GetQuestionListTask', [
                $filters,
                $orderBy,
                $limit,
                $skipPagination,
                $currentLang,
                $externalData,
                $currentPage
            ]);
        }, null, [5]);
    }
}
