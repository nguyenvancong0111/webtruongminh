<?php

namespace App\Containers\Questions\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class DeleteServiceAction.
 *
 */
class DeleteQuestionAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        Apiato::call('Questions@DeleteQuestionTask', [$data->id]);
        // Apiato::call('Question@DeleteQuestionDescTask', [$data->id]);

        $this->clearCache();
    }
}
