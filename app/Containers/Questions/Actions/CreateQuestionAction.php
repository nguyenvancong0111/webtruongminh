<?php

namespace App\Containers\Questions\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Questions\Tasks\SaveAnswerTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class CreateQuestionAction.
 *
 */
class CreateQuestionAction extends Action
{

    /**
     * @return mixed
     */
    public function run( $data)
    {
        // dd($data->answer);
        $question = Apiato::call(
            'Questions@CreateQuestionTask',
            [
                $data
            ]
        );

        if ($question) {
            app(SaveAnswerTask::class)->run( $data, [], $question->id);
        }

        $this->clearCache();

        return $question;
    }
}
