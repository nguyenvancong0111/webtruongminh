<?php

namespace App\Containers\Questions\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use App\Containers\Questions\Tasks\SaveAnswerTask;

/**
 * Class UpdateServiceAction.
 *
 */
class UpdateQuestionAction extends Action
{

    /**
     * @return mixed
     */
    public function run( $data)
    {
        $question = Apiato::call(
            'Questions@SaveQuestionTask',
            [
                $data
            ]
        );

        if($question) {
            $original_ans = Apiato::call('Questions@GetAllAnswerTask', [$question->id]);
            app(SaveAnswerTask::class)->run( $data, $original_ans,$question->id);
        }

        $this->clearCache();

        return $question;
    }
}
