<?php

namespace App\Containers\Questions\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class FindQuestionByIdAction.
 *
 */
class FindQuestionByIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($question_id, $withData = [])
    {
        $data = Apiato::call('Questions@FindQuestionByIdTask', [$question_id, $external_data = ['with_relationship' => array_merge([], $withData)]]);

        return $data;
    }
}
