<?php

namespace App\Containers\Questions\Actions\Admin;

use App\Containers\Questions\Tasks\Admin\ListQuestionInExamTask;
use App\Ship\Parents\Actions\Action;

/**
 * Class ListQuestionInExamAction.
 *
 */
class ListQuestionInExamAction extends Action
{

    /**
     * @return mixed
     */
    public function run($id, array $with = [])
    {
        $question = app(ListQuestionInExamTask::class)->run($id, $with);
        return $question;
    }
}
