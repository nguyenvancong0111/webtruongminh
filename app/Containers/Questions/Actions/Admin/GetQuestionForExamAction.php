<?php

namespace App\Containers\Questions\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Questions\Tasks\GetQuestionForExamTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class FindQuestionByIdAction.
 *
 */
class GetQuestionForExamAction extends Action
{

    /**
     * @return mixed
     */
    public function run(array $filter = [], int $limit = 10, array $rejectID = [])
    {
        $question = app(GetQuestionForExamTask::class)->run(
            $filter, $limit, $rejectID
        );
        return $question;
    }
}
