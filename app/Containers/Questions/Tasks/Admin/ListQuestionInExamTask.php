<?php

namespace App\Containers\Questions\Tasks\Admin;

use App\Containers\Questions\Data\Repositories\QuestionsRepository;
use App\Containers\Questions\Enums\QuestionStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class ListQuestionInExamTask extends Task
{

    protected $repository;

    public function __construct(QuestionsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($id, array $with = [])
    {
        $data = $this->repository->pushCriteria(new ThisEqualThatCriteria('status', QuestionStatus::ACTIVE));
        $data->with($with);
        $this->repository->whereHas('exam_question', function (Builder $q) use($id){
           $q->where('exam_question.exam_id', $id);
        });
        return $data->get();
    }
}
