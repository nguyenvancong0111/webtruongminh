<?php

namespace App\Containers\Questions\Tasks\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\Questions\Data\Repositories\QuestionsRepository;
use App\Containers\Questions\Enums\QuestionsStatus;
use App\Containers\Questions\Enums\ServiceStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetQuestionListTask extends Task
{

    protected $repository;

    public function __construct(QuestionsRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run(
        array $filters = [],
        array $orderBy = ['created_at' => 'desc', 'id' => 'desc'],
        int $limit = 20,
        bool $skipPagination = false,
        Language $currentLang = null,
        array $externalData = [],
        int $currentPage = 1
    ): iterable {
        $language_id = $currentLang ? $currentLang->language_id : 1;

        if (isset($filters['status']) && $filters['status'] != '') {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('status', $filters['status']));
        } else {
            $this->repository->pushCriteria(new ThisOperationThatCriteria('status', -1, '>'));
        }

        !isset($filters['is_hot']) || !$filters['is_hot'] ?: $this->repository->pushCriteria(new ThisEqualThatCriteria('is_hot', $filters['is_hot']));

        !isset($filters['is_home']) || !$filters['is_home'] ?: $this->repository->pushCriteria(new ThisEqualThatCriteria('is_home', $filters['is_home']));

        $this->repository->with(['desc' => function ($query) use ($language_id) {
            $query->select('id', 'question_id', 'language_id', 'name', 'short_description', 'slug', 'description');
            $query->activeLang($language_id);
        }, 'categories', 'categories.desc' => function ($query) use ($language_id) {
            $query->select('category_id', 'name');
            $query->activeLang($language_id);
        }]);
        if (isset($filters['cate_ids']) && !empty($filters['cate_ids'])) {
            $this->repository->whereHas('categories', function (Builder $q) use ($filters) {
                $q->whereIn('category.category_id', (array)$filters['cate_ids']);
            });
        }

        if (isset($filters['name']) && !empty($filters['name'])) {
            $this->repository->whereHas('desc', function (Builder $query) use ($filters) {
                $query->where('name', 'like', '%' . $filters['name'] . '%');
            });
        }

        if(isset($filters['reject_id'])){
            if(is_array($filters['reject_id'])){
                $this->repository->whereNotIn('id', $filters['reject_id']);
            }else{
                $this->repository->where('id', '!=' ,$filters['reject_id']);
            }
        }

        foreach ($orderBy as $column => $direction) {
            $this->repository->orderBy($column, $direction);
        }

        // \DB::enableQueryLog();

        return $skipPagination ? $this->repository->limit($limit) : $this->repository->paginate($limit);
        // dd(\DB::getQueryLog());
    }
}
