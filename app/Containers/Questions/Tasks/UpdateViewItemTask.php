<?php

namespace App\Containers\Questions\Tasks;

use App\Containers\Questions\Data\Repositories\QuestionSRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateViewItemTask extends Task
{

    protected $repository;

    public function __construct(QuestionSRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw $exception;
        }
    }
}
