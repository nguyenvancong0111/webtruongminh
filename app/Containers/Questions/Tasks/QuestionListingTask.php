<?php

namespace App\Containers\Questions\Tasks;

use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\Questions\Data\Repositories\QuestionsRepository;
use App\Containers\Questions\Enums\QuestionStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class questionListingTask.
 */
class QuestionListingTask extends Task
{

    protected $repository;

    public function __construct(QuestionsRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($filters = [], $orderBy = ['created_at' => 'desc', 'id' => 'desc'], $limit = 20, $noPaginate = true, $defaultLanguage = null, $external_data = [], $current_page = 1)
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;

        if (isset($filters['id']) && $filters['id'] != '') {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('id', $filters['id']));
        } else {
            if (isset($filters['status']) && $filters['status'] != '') {
                $this->repository->pushCriteria(new ThisEqualThatCriteria('status', $filters['status']));
            } else {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('status', QuestionStatus::NOT_DELETED, '>'));
            }
            if (isset($filters['is_hot']) && $filters['is_hot'] != '') {
                $this->repository->pushCriteria(new ThisEqualThatCriteria('is_hot', $filters['is_hot']));
            }
            if (isset($filters['is_home']) && $filters['is_home'] != '') {
                $this->repository->pushCriteria(new ThisEqualThatCriteria('is_home', $filters['is_home']));
            }
            if (isset($filters['time_from']) && !empty($filters['time_from']) && $filters['time_from'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('created_at', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['time_from'])), '>='));
            }
            if (isset($filters['time_to']) && !empty($filters['time_to']) && $filters['time_to'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('created_at', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['time_to'],true)), '<='));
            }

            if (isset($filters['publish_from']) && !empty($filters['publish_from']) && $filters['publish_from'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('published', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['publish_from'])), '>='));
            }
            if (isset($filters['publish_to']) && !empty($filters['publish_to']) && $filters['publish_to'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('published', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['publish_to'],true)), '<='));
            }
            if (isset($filters['type']) && $filters['type'] != '') {
                $this->repository->pushCriteria(new ThisEqualThatCriteria('type', $filters['type']));
            }
            if (isset($filters['cat_id']) && $filters['cat_id'] != '') {
                $this->repository->pushCriteria(new ThisEqualThatCriteria('category_id', $filters['cat_id']));
            }
            if (isset($filters['level']) && $filters['level'] != '') {
                $this->repository->pushCriteria(new ThisEqualThatCriteria('level_id', $filters['level']));
            }
        }

        $this->repository->with(['category' => function($cate){
            $cate->select('category_id');
        },'category.desc' => function ($query) use ($language_id) {
            $query->select('category_id', 'name');
        }, 'level' => function($lv){
            $lv->select('id');
        }, 'level.desc', 'course' => function($cour){
            $cour->select('id');
        }, 'course.desc' => function($course){
            $course->select('id', 'course_id', 'name');
        }]);

        if (isset($filters['name']) && !empty($filters['name'])) {
            $this->repository->whereHas('desc', function (Builder $query) use ($filters) {
                $query->where('name', 'like', '%' . $filters['name'] . '%');
            });
        }

        foreach ($orderBy as $column => $direction) {
            $this->repository->orderBy($column, $direction);
        }
        return $noPaginate ? $this->repository->take($limit)->get() :  $this->repository->paginate($limit);
    }
}
