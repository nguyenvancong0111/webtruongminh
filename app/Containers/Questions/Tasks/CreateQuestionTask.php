<?php

namespace App\Containers\Questions\Tasks;

use App\Containers\Questions\Data\Repositories\QuestionsRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class CreateQuestionTask.
 */
class CreateQuestionTask extends Task
{

    protected $repository;

    public function __construct(QuestionsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data->toArray(), ['question_description', '_token', '_headers','answer', 'radio_true', 'checkbox_true', 'video'. 'audio']);

            $dataUpdate['author'] = auth()->guard('admin')->user()->name;

            $question = $this->repository->create($dataUpdate);

            return $question;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
