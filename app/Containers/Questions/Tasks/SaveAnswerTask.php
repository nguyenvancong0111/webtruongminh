<?php

namespace App\Containers\Questions\Tasks;

use Apiato\Core\Foundation\StringLib;
use App\Containers\Questions\Data\Repositories\QuestionsAnswerRepository;
use App\Containers\Questions\Data\Repositories\QuestionsAnswerDescRepository;
use App\Ship\Parents\Tasks\Task;

class SaveAnswerTask extends Task
{

    protected $repository;

    public function __construct(QuestionsAnswerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data, $original_answer, $question_id)
    {
        $answer = $data->answer;
        $checkbox = isset($data->checkbox_true) ? $data->checkbox_true : [];
        $radio = isset($data->radio_true) ? $data->radio_true : [];
        $arr_ans_id = array_column($original_answer, 'id');

        $update_item = [];
        $insert_item = [];
        if (!empty($answer)){
            foreach ($answer as $key => $item){
                if (!empty($item['id']) && isset($original_answer[$item['id']])){
                    unset($arr_ans_id[array_search((int)$item['id'], $arr_ans_id)]);
                    $update_item[$item['id']] = [
                        'question_id' => $question_id,
                        'title' => $item['title'],
                        'sort' => $item['sort'],
                        'radio' => $radio == $key ? 1 : 0,
                        'checkbox' => in_array($key, $checkbox) ? 1 : 0
                    ];
                }else{
                    $insert_item[] = [
                        'question_id' => $question_id,
                        'title' => $item['title'],
                        'sort' => $item['sort'],
                        'radio' => $radio == $key ? 1 : 0,
                        'checkbox' => in_array($key, $checkbox) ? 1 : 0
                    ];
                }
            }

            if (!empty($update_item)) {
                $this->repository->updateMultiple($update_item);
            }

            if (!empty($insert_item)) {
                $this->repository->getModel()->insert($insert_item);
            }

            if(!empty($arr_ans_id)){
                $this->repository->whereIn('id', $arr_ans_id)->delete();
            }

        }
    }
}
