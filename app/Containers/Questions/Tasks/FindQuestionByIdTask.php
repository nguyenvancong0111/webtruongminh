<?php

namespace App\Containers\Questions\Tasks;

use App\Containers\Questions\Data\Repositories\QuestionsRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class FindquestionByIdTask.
 */
class FindquestionByIdTask extends Task
{

    protected $repository;

    public function __construct(QuestionsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($question_id, $external_data = ['with_relationship' => []])
    {
        try {
            $data = $this->repository->with($external_data['with_relationship'])->find($question_id);
        } catch (Exception $e) {
            //throw $th;
            dd($e->getMessage());
        }


        return $data;
    }
}
