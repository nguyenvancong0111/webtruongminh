<?php

namespace App\Containers\Questions\Tasks;

use App\Containers\Questions\Data\Repositories\QuestionsAnswerRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class GetAllAnswerTask.
 */
class GetAllAnswerTask extends Task
{

    protected $repository;

    public function __construct(QuestionsAnswerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($question_id)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('question_id', $question_id));
        $wery = $this->repository->all()->toArray();

        return $wery;
    }
}
