<?php

namespace App\Containers\Questions\Tasks;

use App\Containers\Localization\Models\Language;
use App\Containers\Questions\Data\Repositories\QuestionsRepository;
use App\Containers\Questions\Enums\QuestionStatus;
use App\Containers\Questions\Models\Questions;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisNotInThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetQuestionForExamTask extends Task
{

    protected $repository;

    public function __construct(QuestionsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run(array $filter = [], int $limit = 10, array $rejectID = [])
    {
        $data = $this->repository->pushCriteria(new ThisEqualThatCriteria('status', QuestionStatus::ACTIVE));
        if (!empty($filter)){
            foreach ($filter as $key => $val) {
                $data->pushCriteria(new ThisEqualThatCriteria($key, $val));
            }
        }
        if (!empty($rejectID)) {
            $data->whereNotIn('id', $rejectID);
        }
        $data->with(['answer', 'category' => function($cate){
            $cate->select('category_id');
        },'category.desc' => function ($query){
            $query->select('category_id', 'name');
        }, 'level' => function($lv){
            $lv->select('id');
        }, 'level.desc',
        /*'course' => function($cour){
            $cour->select('id');
        }, 'course.desc' => function($course){
            $course->select('id', 'course_id', 'name');
        },*/
        ]);
        return $data->orderByRaw("RAND()")->take($limit)->get();
    }
}
