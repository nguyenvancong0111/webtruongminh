<?php

namespace App\Containers\Questions\Tasks;

use App\Containers\Questions\Data\Repositories\QuestionsRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteQuestionTask.
 */
class DeleteQuestionTask extends Task
{

    protected $repository;

    public function __construct(QuestionsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($question_id)
    {
        try {
            $this->repository->update(['status' => -1],$question_id);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
