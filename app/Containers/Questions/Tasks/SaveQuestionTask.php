<?php

namespace App\Containers\Questions\Tasks;

use App\Containers\Questions\Data\Repositories\QuestionSRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class SavequestionTask.
 */
class SavequestionTask extends Task
{

    protected $repository;

    public function __construct(QuestionSRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data->toArray(), ['question_description', 'anwser', '_token', '_headers', 'video', 'audio']);
            $dataUpdate['author_updated'] = auth()->guard('admin')->user()->name;
            $question = $this->repository->update($dataUpdate, $data->id);

            return $question;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
