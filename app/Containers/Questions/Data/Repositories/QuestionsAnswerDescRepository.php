<?php

namespace App\Containers\Questions\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class QuestionsRepository
 */
class QuestionsAnswerDescRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
