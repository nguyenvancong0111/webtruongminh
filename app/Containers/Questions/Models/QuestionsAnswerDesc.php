<?php

namespace App\Containers\Questions\Models;

use App\Ship\Parents\Models\Model;

class QuestionsAnswerDesc extends Model
{
    protected $table = "question_answer_desc";
    protected $timestamp = false;
}
