<?php

namespace App\Containers\Questions\Models;

use App\Ship\Parents\Models\Model;

class QuestionsAnswer extends Model
{
    protected $table = "question_answer";
    protected $timestamp = false;
    public function desc()
    {
        return $this->hasOne(QuestionsAnswerDesc::class, 'question_answer_id', 'id');
    }
    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', QuestionsAnswerDesc::class, 'question_answer_id', 'id');
    }
}
