<?php

namespace App\Containers\Questions\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Containers\Category\Models\Category;
use App\Containers\Course\Models\Course;
use App\Containers\Exam\Models\ExamQuestion;
use App\Containers\Level\Models\Level;
use App\Ship\Parents\Models\Model;

class Questions extends Model
{


    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    protected $table = 'questions';



    protected $fillable = [
        'question_type',
        'question',
        'type',
        'category_id',
        'level_id',
        'course_id',
        'video',
        'audio',
        'image',
        'sort_order',
        'status',
    ];

    public function category()
    {
        return $this->hasOne(Category::class, 'category_id', 'category_id');
    }
    public function level()
    {
        return $this->hasOne(Level::class, 'id', 'level_id');
    }
    public function course()
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }
    public function answer()
    {
        return $this->hasMany(QuestionsAnswer::class, 'question_id', 'id');
    }

    public function exam_question(){
        return $this->hasMany(ExamQuestion::class, 'question_id', 'id');
    }
    public function getImageUrl($size = 'original')
    {
        return ImageURL::getImageUrl($this->image, 'questions', $size);
    }

    public function desc()
    {
        return $this->hasOne(QuestionsDesc::class, 'question_id', 'id');
    }

    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', QuestionsDesc::class, 'question_id', 'id');
    }



    public function getFormatDateAttribute()
    {
        return ($this->created_at) ? $this->created_at->format('d.m.Y') : null;
    }


    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'questions';
}
