<?php

/**
 * @apiGroup           Questions
 * @apiName            updateQuestions
 *
 * @api                {PATCH} /v1/questions/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('questions/{id}', [
    'as' => 'api_questions_update_questions',
    'uses'  => 'Controller@updateQuestions',
    'middleware' => [
      'auth:api',
    ],
]);
