<?php

/**
 * @apiGroup           Questions
 * @apiName            createQuestions
 *
 * @api                {POST} /v1/questions Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('questions', [
    'as' => 'api_questions_create_questions',
    'uses'  => 'Controller@createQuestions',
    'middleware' => [
      'auth:api',
    ],
]);
