<?php

/**
 * @apiGroup           Questions
 * @apiName            getAllQuestions
 *
 * @api                {GET} /v1/questions Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('questions', [
    'as' => 'api_questions_get_all_questions',
    'uses'  => 'Controller@getAllQuestions',
    'middleware' => [
      'auth:api',
    ],
]);
