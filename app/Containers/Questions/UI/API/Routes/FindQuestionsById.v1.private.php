<?php

/**
 * @apiGroup           Questions
 * @apiName            findQuestionsById
 *
 * @api                {GET} /v1/questions/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('questions/{id}', [
    'as' => 'api_questions_find_questions_by_id',
    'uses'  => 'Controller@findQuestionsById',
    'middleware' => [
      'auth:api',
    ],
]);
