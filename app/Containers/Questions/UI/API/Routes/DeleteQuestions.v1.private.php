<?php

/**
 * @apiGroup           Questions
 * @apiName            deleteQuestions
 *
 * @api                {DELETE} /v1/questions/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('questions/{id}', [
    'as' => 'api_questions_delete_questions',
    'uses'  => 'Controller@deleteQuestions',
    'middleware' => [
      'auth:api',
    ],
]);
