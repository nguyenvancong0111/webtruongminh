<?php

namespace App\Containers\Questions\UI\API\Controllers;

use App\Containers\Questions\UI\API\Requests\CreateQuestionsRequest;
use App\Containers\Questions\UI\API\Requests\DeleteQuestionsRequest;
use App\Containers\Questions\UI\API\Requests\GetAllQuestionsRequest;
use App\Containers\Questions\UI\API\Requests\FindQuestionsByIdRequest;
use App\Containers\Questions\UI\API\Requests\UpdateQuestionsRequest;
use App\Containers\Questions\UI\API\Transformers\QuestionsTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Questions\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateQuestionsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createQuestions(CreateQuestionsRequest $request)
    {
        $questions = Apiato::call('Questions@CreateQuestionsAction', [$request]);

        return $this->created($this->transform($questions, QuestionsTransformer::class));
    }

    /**
     * @param FindQuestionsByIdRequest $request
     * @return array
     */
    public function findQuestionsById(FindQuestionsByIdRequest $request)
    {
        $questions = Apiato::call('Questions@FindQuestionsByIdAction', [$request]);

        return $this->transform($questions, QuestionsTransformer::class);
    }

    /**
     * @param GetAllQuestionsRequest $request
     * @return array
     */
    public function getAllQuestions(GetAllQuestionsRequest $request)
    {
        $questions = Apiato::call('Questions@GetAllQuestionsAction', [$request]);

        return $this->transform($questions, QuestionsTransformer::class);
    }

    /**
     * @param UpdateQuestionsRequest $request
     * @return array
     */
    public function updateQuestions(UpdateQuestionsRequest $request)
    {
        $questions = Apiato::call('Questions@UpdateQuestionsAction', [$request]);

        return $this->transform($questions, QuestionsTransformer::class);
    }

    /**
     * @param DeleteQuestionsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteQuestions(DeleteQuestionsRequest $request)
    {
        Apiato::call('Questions@DeleteQuestionsAction', [$request]);

        return $this->noContent();
    }
}
