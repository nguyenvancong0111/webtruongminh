<?php

namespace App\Containers\Questions\UI\API\Transformers;

use App\Containers\Questions\Models\Questions;
use App\Ship\Parents\Transformers\Transformer;

class QuestionsTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Questions $entity
     *
     * @return array
     */
    public function transform(Questions $entity)
    {
        $response = [
            'object' => 'Questions',
            'id' => $entity->getHashedKey(),
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
