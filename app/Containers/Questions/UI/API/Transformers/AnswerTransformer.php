<?php

namespace App\Containers\Questions\UI\API\Transformers;

use App\Containers\Questions\Models\Questions;
use App\Ship\Parents\Transformers\Transformer;

class AnswerTransformer extends Transformer
{
    
    public function transform($entity)
    {
        $response = [
            'id' => $entity->id,
            'content' => $entity->desc ? $entity->desc->content : '',

        ];



        return $response;
    }
}
