<?php
Route::group(
    [
        'prefix' => 'questions',
        'namespace' => '\App\Containers\Questions\UI\WEB\Controllers\Admin',
        'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
        'middleware' => [
            'auth:admin',
        ],
    ],
    function () use ($router) {
        $router->get('/', [
            'as'   => 'admin_question_home_page',
            'uses' => 'Controller@index',
        ]);

        $router->get('/edit/{id}', [
            'as'   => 'admin_question_edit_page',
            'uses' => 'Controller@edit',
        ]);

        $router->post('/edit/{id}', [
            'as'   => 'admin_question_edit_page',
            'uses' => 'Controller@update',
        ]);

        $router->get('/add', [
            'as'   => 'admin_question_add_page',
            'uses' => 'Controller@add',
        ]);

        $router->post('/add', [
            'as'   => 'admin_question_add_page',
            'uses' => 'Controller@create',
        ]);

        $router->delete('/{id}', [
            'as' => 'admin_question_delete',
            'uses'       => 'Controller@delete',
        ]);

        $router->post('/status/{field}', [
            'as'   => 'admin_question_change_status',
            'uses' => 'Controller@updateSomeStatus',
        ]);
//        $router->post('/edit/{id}', [
//            'as'   => 'web.new.detail',
//            'uses' => 'Controller@edit',
//        ]);
    }
);
