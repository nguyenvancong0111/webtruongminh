@extends('basecontainer::admin.layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if (isset($editMode) && $editMode)
                {!! Form::open(['url' => route('admin_question_edit_page', $data->id), 'files' => true]) !!}
            @else
                {!! Form::open(['url' => route('admin_question_add_page'), 'files' => true]) !!}
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            <div class="card card-accent-primary">
                <div class="card-header">
                    <i class="fa fa-pencil"></i>THÔNG TIN
                </div>
                <div class="card-body">
                    <div class="tabbable boxed parentTabs">
                        <ul class="nav nav-tabs nav-underline nav-underline-primary mb-3">
                            <li class="nav-item">
                                <a class="nav-link active" href="#question_answer"><i class="fa fa-empire"></i> Câu hỏi & Câu trả lời</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#data"><i class="fa fa-deviantart"></i> Thông tin chi tiết</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#links"><i class="fa fa-link"></i> Liên kết</a>
                            </li>

                        </ul>

                        <div class="tab-content p-0">
                            @include('questions::admin.edit_tabs.question_answer')
                            @include('questions::admin.edit_tabs.data')
                            @include('questions::admin.edit_tabs.links')
{{--                            @include('questions::admin.edit_tabs.image')--}}
                        </div>
                    </div>
                </div>
            </div>


            <div class="mb-3">
                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Cập nhật</button>
                &nbsp;&nbsp;
                <a class="btn btn-sm btn-danger" href="{{ redirect()->back()->getTargetUrl() }}"><i class="fa fa-ban"></i>
                    Hủy bỏ</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')
    {!! \FunctionLib::addMedia('admin/js/library/tag/jquery.tag-editor.css') !!}
    {!! \FunctionLib::addMedia('admin/js/library/uploadifive/uploadifive.css') !!}
@stop

@push('js_bot_all')
    {!! \FunctionLib::addMedia('admin/js/library/tinymce/tinymce.min.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/tag/jquery.caret.min.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/tag/jquery.tag-editor.min.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/jquery.form.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/jquery.sortable.js') !!}

    <script type="text/javascript">
        $("ul.nav-tabs a").click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });

        // $(document).ready(function() {
        admin.system.tinyMCE('.__editor', plugins = '', menubar = '', toolbar = '', height = 300);
        // });

        function mixMoney(myfield) {
            var thousands_sep = '.',
                val = parseInt(myfield.value.replace(/[.*+?^${}()|[\]\\]/g, ''));
            myfield.value = shop.numberFormat(val, 0, '', thousands_sep);
        }

    </script>
@endpush
