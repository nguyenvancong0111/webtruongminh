@extends('basecontainer::admin.layouts.default')

@section('right-breads')
    {!! $data->appends($search_data->toArray())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5">
                <h1>{{ $site_title }}</h1>
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            {!! Form::open(['url' => route('admin_question_home_page'), 'method' => 'get', 'id' => 'searchForm']) !!}
            <div class="card card-accent-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-bookmark-o"></i></span></div>
                                <input type="text" name="name" class="form-control" placeholder="Tiêu đề"
                                       value="{{ $search_data->name }}">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-navicon"></i></span></div>
                                <select id="cat_id" name="cat_id" class="form-control">
                                    <option value="">-- Danh mục --</option>
                                    @foreach ($categories as $category)
                                        <option {{ $search_data->cat_id == $category->category_id ? 'selected' : '' }}
                                                value="{{ $category->category_id }}">{!! $category->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-navicon"></i></span></div>
                                <select id="level" name="level" class="form-control">
                                    <option value="">-- Level --</option>
                                    @foreach ($level as $lv)
                                        <option {{ $search_data->level == $lv->id ? 'selected' : '' }}
                                                value="{{ $lv->id }}">{!! $lv->desc->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-list"></i></span></div>
                                <select id="status" name="status" class="form-control">
                                    <option value="">-- Chọn trạng thái --</option>
                                    <option value="2" {{ $search_data->status == 2 ? ' selected="selected"' : '' }}>
                                        Đang hiển thị
                                    </option>
                                    <option value="1" {{ $search_data->status == 1 ? ' selected="selected"' : '' }}>
                                        Đang ẩn
                                    </option>
                                    <option value="-1" {{ $search_data->status == -1 ? ' selected="selected"' : '' }}>
                                        Đã xóa
                                    </option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-calendar"></i></span></div>
                                <input type="text" name="time_from" class="datepicker form-control"
                                       placeholder="Ngày tạo từ" autocomplete="off"
                                       value="{{ $search_data->time_from }}">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-calendar"></i></span></div>
                                <input type="text" name="time_to" class="datepicker form-control"
                                       placeholder="Ngày tạo đến" autocomplete="off"
                                       value="{{ $search_data->time_to }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                    <a href="{{ route('admin_question_home_page') }}" class="btn btn-sm btn-primary"><i
                            class="fa fa-refresh"></i>Reset</a>
                    @if ($user->can('create-question'))
                        <a href="{{ route('admin_question_add_page') }}" class="btn btn-sm btn-primary"><i
                                class="fa fa-plus"></i> Thêm mới</a>
                    @endif
                </div>
            </div>
            {!! Form::close() !!}

            <div class="card card-accent-primary">
                <div class="card-body p-0">
                    <table class="table table-bordered table-striped ">
                        <thead>
                        <tr>
                            <th width="55">ID</th>
                            <th>Thông tin</th>
                            <th width="85" align="center">Sắp xếp</th>
                            <th width="100">Ngày tạo</th>
                            @if (!isset(\request()->status) || \request()->status != -1)
                                <th width="55">Lệnh</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <td align="center">{{ $item->id }}</td>
                                <td>
                                    <p>
                                        <strong>Câu hỏi:</strong>&nbsp;<i>{{ @$item->question }}</i> <br>
                                        @if($item->question_type == 2)
                                            <strong>Câu hỏi dạng Video</strong> <i>[&nbsp;{{$item->video}}&nbsp;]</i>
                                        @endif
                                        @if($item->question_type == 3)
                                            <strong>Câu hỏi dạng Audio</strong>&nbsp;<i>[&nbsp;{{ @$item->audio }}&nbsp;]</i>
                                        @endif
                                        @if($item->question_type == 4)
                                            <strong>Câu hỏi dạng Image</strong>&nbsp;<i>[&nbsp;{{ @$item->image }}&nbsp;]</i>
                                        @endif
                                    </p>
                                    @if(!empty($item->category) && !empty($item->category->desc))
                                        <small><strong>Danh mục:</strong>&nbsp;{{$item->category->desc->name}}</small>
                                        <br>
                                    @endif
                                    @if(!empty($item->level) && !empty($item->level->desc))
                                        <small><strong>Level:</strong>&nbsp;{{$item->level->desc->name}} - ({{$item->level->desc->short_desc}})</small>
                                        <br>
                                    @endif
                                   {{-- @if(!empty($item->course) && !empty($item->course->desc))
                                        <small><strong>Khóa học:</strong>&nbsp;{{$item->course->desc->name}}</small>
                                        <br>
                                    @endif--}}
                                </td>

                                <td align="center">{{ @$item->sort_order }}</td>

                                <td align="center">{!! $item->created_at ? Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s') : '---' !!} </td>

                                @if (!isset(\request()->status) || \request()->status != -1)
                                    <td align="center">
                                        @if ($user->can('edit-question'))
                                            @if ($item->status == 2)
                                                <a href="javascript:void(0)" data-route="{{ route('admin_question_change_status', ['field' => 'status']) }}" class="btn text-primary" onclick="admin.updateStatus(this,{{ $item->id }},1)" title="Đang hiển thị, Click để ẩn"><i class="fa fa-eye"></i></a>
                                            @else
                                                <a href="javascript:void(0)" data-route="{{ route('admin_question_change_status', ['field' => 'status']) }}" class="btn text-danger" onclick="admin.updateStatus(this,{{ $item->id }}, 2)" title="Đang ẩn, Click để hiển thị"><i class="fa fa-eye"></i></a>
                                            @endif
                                            <a href="{{ route('admin_question_edit_page', $item->id) }}" class="btn text-primary"><i class="fa fa-pencil"></i></a>
                                        @endif

                                        @if ($user->can('delete-question'))
                                            @if ($item->status != -1)
                                                <a data-href="{{ route('admin_question_delete', $item->id) }}" class="btn text-danger" onclick="admin.delete_this(this);"><i class="fa fa-trash"></i></a>
                                            @endif
                                        @endif
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    @include('basecontainer::admin.inc.bottom-pager-infor')
                </div>
            </div>
        </div>
    </div>
@stop

@push('js_bot_all')
    <script>
        $('.datepicker').datetimepicker({
            format: 'd/m/Y',
            timepicker: false
        });
    </script>
@endpush
