<div class="tab-pane" id="links">
    <div class="tabbable">


        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="status">Danh mục</label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}" name="category_id"
                    id="category_id">
                    <option value="">--Chọn--</option>
                    @if (@$data->category_id > 0)
                        <option value="{{ $data->category_id }}" selected>{!! $data->category->desc->name !!}</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="level_id">Level câu hỏi</label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('level_id') ? ' is-invalid' : '' }}" name="level_id"
                    id="level_id">
                    <option value="">--Chọn--</option>
                    @if (@$data->level_id > 0)
                        <option value="{{ $data->level_id }}" selected>{!! $data->level->desc->name !!}</option>
                    @endif
                </select>
            </div>
        </div>
        {{--<div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="course_id">Khóa hoc</label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('course_id') ? ' is-invalid' : '' }}" name="course_id"
                    id="course_id">
                    <option value="">--Chọn--</option>
                    @if (@$data->course_id > 0)
                        <option value="{{ $data->course_id }}" selected>{!! $data->course->desc->name !!}</option>
                    @endif
                </select>
            </div>
        </div>--}}
    </div>
</div>

@push('js_bot_all')
    <script>
        $(document).ready(function() {

            $("#category_id").select2({
                width: '100%',
                tags: true,
                ajax: {
                    url: "/admin/ajax/category/controller/getCategories?type=select2",

                    headers: ENV.headerParams,
                    dataType: 'JSON',
                    delay: 800,
                    data: function(params) {
                        return {
                            cate_type: 1,
                            name: params.term, // search term
                            page: params.page,
                            _token: ENV.token
                        };
                    },
                    processResults: function(data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
            });
            $("#level_id").select2({
                width: '100%',
                tags: true,
                ajax: {
                    url: "{{ route('get_ajax_level') }}",

                    headers: ENV.headerParams,
                    dataType: 'JSON',
                    delay: 800,
                    data: function(params) {
                        return {

                            name: params.term, // search term
                            page: params.page,
                            _token: ENV.token
                        };
                    },
                    processResults: function(data, params) {
                        return {
                            results: $.map(data.data, function(item) {
                                console.log(item.name)
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };

                    },
                    cache: true
                },
            });
            $("#course_id").select2({
                width: '100%',
                tags: true,
                ajax: {
                    url: "{{ route('get_ajax_course') }}",

                    headers: ENV.headerParams,
                    dataType: 'JSON',
                    delay: 800,
                    data: function(params) {
                        return {
                            status: 2,
                            name: params.term, // search term
                            page: params.page,
                            _token: ENV.token
                        };
                    },
                    processResults: function(data, params) {
                        console.log(data.data);
                        return {
                            results: $.map(data.data, function(item) {
                                return {
                                    text: item.desc.name,
                                    id: item.id
                                }
                            })
                        };

                    },
                    cache: true
                },
            });
        });
    </script>
@endpush
