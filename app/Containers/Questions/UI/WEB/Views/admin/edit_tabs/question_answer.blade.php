<div class="tab-pane active" id="question_answer">
    <div class="tabbable">
        <div class="row form-group align-items-center">
            <label class="col-sm-1 control-label text-right mb-0" for="type_answer">Loại câu hỏi</label>
            <div class="col-sm-4">
                <select name="question_type" id="question_type" class="form-control" onchange="changeTypeQuestion($(this).val())">
                    <option value="0" @if(old('question_type', @$data->question_type) == 0) selected @endif> --- Chọn loại câu hỏi ---</option>
                    <option value="1" @if(old('question_type', @$data->question_type) == 1) selected @endif>Text</option>
                    <option value="2" @if(old('question_type', @$data->question_type) == 2) selected @endif>Video</option>
                    <option value="3" @if(old('question_type', @$data->question_type) == 3) selected @endif>Audio</option>
                    <option value="4" @if(old('question_type', @$data->question_type) == 4) selected @endif>Image</option>
                </select>
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-1 control-label text-right mb-0" for="type_answer">Câu hỏi</label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="5" name="question" id="question" >{{old('question', @$data->question)}}</textarea>
            </div>
        </div>

        <div class="quest-type type-video @if(old('question_type', @$data->question_type) == 2) d-block @else d-none @endif" >
            <div class="row form-group align-items-center">
                <label class="col-sm-1 control-label text-right mb-0" for="type_answer">Video</label>
                <div class="col-sm-4">
                    <div class="form-group">
                        <input type="file" id="video" name="video" class="dropify form-control {{ $errors->has('video') ? ' is-invalid' : '' }}" data-show-remove="false" data-default-file="{{ \ImageURL::getFileUrl(old('video',@$data['video']), 'questions', 'video')    }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="quest-type type-audio @if(old('question_type', @$data->question_type) == 3) d-block @else d-none @endif" >
            <div class="row form-group align-items-center">
                <label class="col-sm-1 control-label text-right mb-0" for="type_answer">Audio</label>
                <div class="col-sm-4">
                    <div class="form-group">
                        <input type="file" id="audio" name="audio" class="dropify form-control {{ $errors->has('audio') ? ' is-invalid' : '' }}" data-show-remove="false" data-default-file="{{ \ImageURL::getFileUrl(old('audio',@$data['audio']), 'questions', 'audio')    }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="quest-type type-image @if(old('question_type', @$data->question_type) == 4) d-block @else d-none @endif" >
            <div class="row form-group align-items-center">
                <label class="col-sm-1 control-label text-right mb-0" for="type_answer">Hình ảnh</label>
                <div class="col-sm-4">
                    <div class="form-group">
                        <input type="file" id="image" name="image" class="dropify form-control {{ $errors->has('image') ? ' is-invalid' : '' }}" data-show-remove="false" data-default-file="{{ \ImageURL::getImageUrl(old('image',@$data['image']), 'questions', 'original')    }}">
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <!--answer-->
        <div class="card card-accent-primary" id="answer">
            <div class="card-header">
                <div class="row">
                    <div class="col-sm-6">
                        <i class="fa fa-pencil"></i> Câu trả lời
                    </div>
                    <div class="col-sm-6">
                        <select class="form-control{{ $errors->has('type_answer') ? ' is-invalid' : '' }}" name="type" @change="changeType($event)">
                            <option value="">--- Phân loại ---</option>
                            <option v-for="(i, index) in data.type_select" :value="index" v-html="i" :selected="index == data.type"></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="tab-content p-0">
                <div class="row pl-1 pr-1">
                    <div class="col-sm-12">
                        <div v-for="(item, key) in data.val">
                            <input type="hidden" v-bind:name="'answer['+key+'][id]'" v-model="item.id" class="form-control js-input" >
                            <div class="row mt-3">
                                <div class="col-sm-1">
                                    <div class="form-group text-right p-2">
                                        <div class="checkbox ml-1 mb-1 form-check-inline radio-success js-input">
                                            <input v-if="data.type === 1" type="checkbox" :id="'answer_true_'+key" v-bind:name="'checkbox_true[]'" v-model="item.checkbox" :value="key" :checked="item.checkbox === 1">
                                            <input v-if="data.type === 2" type="radio" :id="'answer_true'" v-bind:name="'radio_true'" v-model="item.radio" :value="key" :checked="item.radio === 1">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input type="text" v-bind:name="'answer['+key+'][title]'" v-model="item.title" class="form-control js-input">
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <input type="text" v-bind:name="'answer['+key+'][sort]'" v-model="item.sort" class="form-control js-input"placeholder="Sort" >
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="input-group-append">
                                        <span class="btn btn-danger js-input" @click="trashAns(key)" style="cursor: pointer"><i class="fa fa-trash"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-7">
                                <a class="btn btn-outline-info  w-100" @click="addAns"  style="cursor: pointer">Thêm đáp án</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js_bot_all')
    <script>
        $('#published').datetimepicker({
            format: 'd/m/Y H:i',
        });

        //Question
        function changeTypeQuestion(val){
            var type = parseInt(val);
            $('.quest-type').removeClass('d-block').addClass('d-none');
            switch (type) {
                case 1:
                    $('.type-text').removeClass('d-none').addClass('d-block');
                    break;
                case 2:
                    $('.type-video').removeClass('d-none').addClass('d-block');
                    break
                case 3:
                    $('.type-audio').removeClass('d-none').addClass('d-block');
                    break
                case 4:
                    $('.type-image').removeClass('d-none').addClass('d-block');
                    break
            }
        }

        // Answer
        var answer_data = '{!! isset($answer_data) && !empty($answer_data) ? addslashes($answer_data) : '' !!}';
        var type_data = '{{ isset($answer_data) && !empty($answer_data) ? $type : 0}}';
        var answerVue = new Vue({
            el: '#answer',
            data() {
                return {
                    data: {
                        val: answer_data != '' ? JSON.parse(answer_data) : [{title: '', sort: ''}],
                        type_select: {
                            1: 'Checkbox - Chọn nhiều',
                            2: 'Radio - Chọn một',
                        },
                        type: type_data != '' ? parseInt(type_data) : 0

                    }
                }
            },
            mounted(){},
            methods: {
                addAns: function () {
                    var ans = {title: '', sort: ''};
                    return this.data.val.push(ans);
                },
                trashAns: function (key) {
                    if(this.data.val.length > 1) {
                        this.data.val.splice(key, 1);
                    }else {
                        this.data.val.splice(key, 1);
                        var ans = {title: '', sort: ''};
                        return this.data.val.push(ans);
                    }
                },

                changeType: function(e){
                    console.log(e.target.value)
                    this.data.type = parseInt( e.target.value)
                },
            },
        });
    </script>
@endpush
