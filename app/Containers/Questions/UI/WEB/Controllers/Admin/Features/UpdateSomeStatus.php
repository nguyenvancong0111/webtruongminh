<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 23:15:50
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-01 00:41:31
 * @ Description: Happy Coding!
 */

namespace App\Containers\question\UI\WEB\Controllers\Admin\Features;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\question\UI\WEB\Requests\Admin\UpdateSomeStatusRequest;

trait UpdateSomeStatus
{
    public function updateSomeStatus(UpdateSomeStatusRequest $request) {
        $result = Apiato::call('Questions@Admin\UpdateSomeStatusAction', [
            $request->field,
            $request->id,
            $request->status,
        ]);

        if($result) {
            return $this->sendResponse($result,'Success');
        }

        return $this->sendError('Không update được dữ liệu!');
    }
}
