<?php

namespace App\Containers\Questions\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Questions\Enums\QuestionAnswerType;
use App\Containers\Questions\Models\Questions;
use App\Containers\Questions\UI\WEB\Requests\Admin\FindQuestionRequest;
use App\Containers\Questions\UI\WEB\Requests\StoreQuestionRequest;
use App\Containers\Questions\UI\WEB\Requests\UpdateQuestionRequest;
use App\Containers\Questions\UI\WEB\Requests\GetAllQuestionRequest;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Http\Request;

class Controller extends AdminController
{
    // use UpdateSomeStatus;

    public function __construct()
    {
        $this->title = 'Câu hỏi';
        parent::__construct();
        \View::share('type_answer', QuestionAnswerType::TYPE_ANSWER);
    }
    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllQuestionRequest $request)
    {
        try {

            $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Câu hỏi', $this->form == 'list' ? '' : route('admin_question_home_page'));
            \View::share('breadcrumb', $this->breadcrumb);

            $question = Apiato::call('Questions@QuestionListingAction', [$request->all(), ['created_at' => 'desc'], $this->perPage, false, Apiato::call('Localization@GetDefaultLanguageAction'), [], $request->page ?? 1]);
//                dd($question);
            return view('questions::admin.index', [
                'search_data' => $request,
                'data' => $question,
                // 'typeOptions'=> questionType::TEXT,
                'categories' => Apiato::call('Category@Admin\GetAllCategoriesAction', [['cate_type' => 1]]),
                'level' => Apiato::call('Level@GetAllLevelsAction', [new DataTransporter(['status' => 2])])
            ]);
        } catch (Exception $e) {
            return redirect()->route('admin_question_home_page')->withErrors($e->getMessage());
        }
    }

    public function edit(Request $request)
    {
        $this->showEditForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Câu hỏi', $this->form == 'list' ? '' : route('admin_question_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Chỉnh sửa thông tin');
        \View::share('breadcrumb', $this->breadcrumb);

        $question = Apiato::call('Questions@Admin\FindQuestionByIdAction', [
            $request->id,
            ['answer', 'category', 'category.desc', 'level', 'level.desc', 'course', 'course.desc',]
        ]);

        $answer_data = [];
        if ($question->answer->isNotEmpty()){
            foreach ($question->answer as $ans){
                $answer_data[] = [
                    'id' => $ans->id,
                    'title' => $ans->title,
                    'sort' => $ans->sort,
                    'radio' => $ans->radio,
                    'checkbox' => $ans->checkbox
                ];
            }
        }

        $answer_data = !empty($answer_data) ? json_encode($answer_data,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE) : null;
        return view('questions::admin.edit', [
            'data' => $question,
            'answer_data' => $answer_data,
            'type' => $question->type,
        ]);

    }

    public function add(Request $request)
    {
        $this->showAddForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Câu hỏi', $this->form == 'list' ? '' : route('admin_question_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Thêm mới');
        \View::share('breadcrumb', $this->breadcrumb);
        return view('questions::admin.edit');
    }

    public function update(UpdateQuestionRequest $request)
    {

        try {
            $tranporter = new DataTransporter($request);

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'question', StringLib::getClassNameFromString(questions::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }
            if (isset($request->video)) {
                $file = Apiato::call('File@UploadFileAction', [$request, 'video', '', 'questions']);
                if (!$file['error']) {
                    $tranporter->video = $file['fileName'];
                }
            }
            if (isset($request->audio)) {
                $file = Apiato::call('File@UploadFileAction', [$request, 'audio', '', 'questions']);
                if (!$file['error']) {
                    $tranporter->audio = $file['fileName'];
                }
            }

            $question = Apiato::call('Questions@UpdateQuestionAction', [$tranporter]);

            if ($question) {
                return redirect()->route('admin_question_edit_page', ['id' => $question->id])->with('status', 'Câu hỏi đã được cập nhật');
            }
        } catch (\Exception $e) {
            throw $e;
            if (!$image['error'])
                return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
            else
                return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . $image['msg']]);
        }
    }

    public function create(StoreQuestionRequest $request)
    {

        try {
            $tranporter = new DataTransporter($request);

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'question', StringLib::getClassNameFromString(questions::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }
            if (isset($request->video)) {
                $file = Apiato::call('File@UploadFileAction', [$request, 'video', '', 'questions']);
                if (!$file['error']) {
                    $tranporter->file = $file['fileName'];
                }
            }
            if (isset($request->audio)) {
                $file = Apiato::call('File@UploadFileAction', [$request, 'audio', '', 'questions']);
                if (!$file['error']) {
                    $tranporter->file = $file['fileName'];
                }
            }
            $question = Apiato::call('Questions@CreateQuestionAction', [$tranporter]);
            if ($question) {
                return redirect()->route('admin_question_home_page')->with('status', 'Câu hỏi đã được thêm mới');
            }
        } catch (\Exception $e) {
            if (!isset($image['error']))
                return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! '. $e->getMessage()]);
            else
                return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . $image['msg']]);
        }
    }

    public function delete(FindQuestionRequest $request)
    {
        try {
            Apiato::call('Questions@DeleteQuestionAction', [$request]);
        } catch (\Exception $e) {
            // throw $e;
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
        }
    }
}
