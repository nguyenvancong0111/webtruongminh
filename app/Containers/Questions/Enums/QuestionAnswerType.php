<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-04 22:47:58
 * @ Description: Happy Coding!
 */

namespace App\Containers\Questions\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class QuestionAnswerType extends BaseEnum
{
    /**
     * Câu trả lời dạng text
     */
    const TEXT = 0;

    /**
     * Câu trả lời dạng checkbox
     */
    const CHECKBOX = 1;

    /**
     * Câu trả lời dạng radio(nhiều đáp án)
     */
    const RADIO = 2;

    const TYPE_ANSWER = [
//        self::TEXT => 'Text (Nhập câu trả lời)',
        self::CHECKBOX => 'Checkbox (Chọn nhiều)',
        self::RADIO => 'Radio (Chọn một)'
    ];
}
