<?php

namespace App\Containers\Feature\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Ship\Parents\Models\Model;

class Feature extends Model
{
    protected $table = 'feature';

    const OPTIONS = [
        'advantage' => 'Ưu điểm',
        'poster' => 'Poster Trang chủ',
        // 'product_list' => 'Banner sản phẩm',
        'testimonial' => 'Ý kiến khách hàng',
    ];

    const SIZE = [
        'advantage' => ['width' => 1300, 'height' => 611],
        'poster' => ['width' => 1300, 'height' => 611],
    ];

    public static function getSize($type = 'big_home'){
        if(isset(self::SIZE[$type])){
            return (object)self::SIZE[$type];
        }
        return false;
    }

    public function getImageUrl($size = 'original')
    {
        return ImageURL::getImageUrl($this->image, 'news', $size);
    }

    public function desc()
    {
        return $this->hasOne(FeatureDesc::class,'feature_id','id');
    }

    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id',FeatureDesc::class,'feature_id','id');
    }
}
