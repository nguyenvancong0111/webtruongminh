<?php

namespace App\Containers\Feature\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class FeatureRepository.
 */
class FeatureRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Feature';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'status',
        'published',
        'positions',
        'sort_order',
        'created_at',
        'desc.name',
        'desc.language_id'
    ];
}
