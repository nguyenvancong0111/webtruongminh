<?php

namespace App\Containers\Warehouse\Actions\WarehouseOrder;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateWarehouseOrderAction extends Action
{
    public function run(array $warehousesData,array $warehousesOrderItems)
    {
        $warehouses = Apiato::call('Warehouse@WarehouseOrder\CreateWarehouseOrderTask',[$warehousesData]);
        if($warehouses->id){
            $totalCount=0;
            foreach ($warehousesOrderItems as $key => $items){
                $orderItems[$key]=Apiato::call('Warehouse@WarehouseOrder\CreateWarehouseOrderItemsTask',[$items,$warehouses->id,$warehouses->type]);
                $totalCount +=   $orderItems[$key]->quantity;
            }
            Apiato::call('Warehouse@WarehouseOrder\UpdateWarehouseOrderTask',[$warehouses,$totalCount]);
        }
        $this->clearCache();
        return $warehouses;
    }
}
