<?php

namespace App\Containers\Warehouse\Actions\WarehouseOrder;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateOrderAction extends Action
{
    public function run($statusChange,$id,$order_detail)
    {
        $warehousesOld = Apiato::call('Warehouse@WarehouseOrder\FindWarehouseOrderByIdTask', [$id]);
        $warehouses = Apiato::call('Warehouse@WarehouseOrder\UpdateSomeStatusTask', [$id,$statusChange]);
        foreach($order_detail as $items){
            $variantsCountUpdate = Apiato::call('Product@ProductVariant\UpdateProductVariantTask', [$warehouses,$items['quantity'],$items['product_variants_id'],$warehousesOld->status,$items['product']['id']]);
        }
        $this->clearCache();
        return $warehouses;
    }
}
