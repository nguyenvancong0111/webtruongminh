<?php

namespace App\Containers\Warehouse\Actions\WarehouseOrder;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetWarehouseOrderDetailAction extends Action
{
    public function run($id)
    {
        $warehouses = Apiato::call('Warehouse@WarehouseOrder\GetWarehouseOrderDetailTask',[$id]);

        return $warehouses;
    }
}
