<?php

namespace App\Containers\Warehouse\Actions;

use App\Ship\Parents\Actions\Action;
use App\Containers\Warehouse\Tasks\DeleteWarehouseTask;
class DeleteWarehouseAction extends Action
{
    public function run(int $id)
    {
        return app(DeleteWarehouseTask::class)->run($id);
    }
}
