<?php
$router->get('warehouse', [
    'as' => 'admin_warehouse_home_page',
    'uses'  => 'Controller@index',
    'middleware' => [
      'auth:admin',
    ],
  ]);
$router->get('warehouse/order/{abc}', [
  'as' => 'admin_warehouse_order_page',
  'uses'  => 'Controller@warehouseOrder',
  'middleware' => [
    'auth:admin',
  ],
]);
$router->get('warehouse/order', [
  'as' => 'admin_warehouse_order_page',
  'uses'  => 'Controller@warehouseOrder',
  'middleware' => [
    'auth:admin',
  ],
]);