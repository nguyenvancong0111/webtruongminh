<?php

namespace App\Containers\Warehouse\UI\WEB\Controllers;

use App\Containers\Warehouse\UI\WEB\Requests\GetAllWarehousesItemRequest;
use App\Containers\Warehouse\UI\WEB\Requests\ProductWarehouseDetailRequest;
use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\StringLib;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Ship\Parents\Controllers\AdminController;
/**
 * Class Controller
 *
 * @package App\Containers\Warehouse\UI\WEB\Controllers
 */
class WarehouseOrderItemController extends AdminController
{
    use ApiResTrait;
    /**
     * Show all entities
     *
     * @param GetAllWarehousesRequest $request
     */

    public function ajaxWarehouseOrderItemData(GetAllWarehousesItemRequest $request){
        $warehouses = Apiato::call('Warehouse@WarehouseOrderItem\GetAllWarehousesOrderItemAction', [$request]);
        $warehouses->load([
            'product.desc',
            'product_variants',
            'product_variants.product_option_values',
            'product_variants.product_option_values.option',
            'product_variants.product_option_values.option_value',
        ]);
        return FunctionLib::ajaxRespond(true, 'Hoàn thành', $warehouses);
    }

    public function ajaxProductWarehouseDetail(ProductWarehouseDetailRequest $request){
        $warehouses = Apiato::call('Warehouse@WarehouseOrderItem\GetProductWarehouseDetailAction', [$request]);

        return FunctionLib::ajaxRespond(true, 'Hoàn thành', $warehouses);
    }
}
