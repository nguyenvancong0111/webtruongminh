<?php

namespace App\Containers\Warehouse\UI\WEB\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class GetAllWarehousesRequest.
 */
class SaveWarehouseRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\Warehouse\Data\Transporters\GetAllWarehousesTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'warehouseStoreName' => 'required|string',
            'warehouseAddress' => 'required|string',
            'city' => 'required',
            'district' => 'required',
            // '{user-input}' => 'required|max:255',
        ];
    }

      /**
     * @return  array
     */
    public function messages()
    {
        return [
            'warehouseStoreName.required' => 'Tên kho là trường bắt buộc',
            'warehouseAddress.required' => 'Địa chỉ là trường bắt buộc',
            'city.required' => 'Thành phố là trường bắt buộc',
            'district.required' => 'Quận huyện là trường bắt buộc',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
