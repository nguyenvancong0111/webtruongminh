<?php

namespace App\Containers\Warehouse\Tasks\WarehouseOrderItem;

use App\Containers\Warehouse\Data\Repositories\WarehouseOrderItemsRepository;
use App\Containers\Warehouse\Models\WarehouseOrderItems;
use App\Ship\Criterias\Eloquent\ByIdCriteria;
use App\Ship\Criterias\Eloquent\ByStatusCriteria;
use App\Ship\Criterias\Eloquent\ByIdWarehouseCriteria;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;

/**
 * Class GetAllProductssTask.
 */
class GetAllWarehousesOrderItemTask extends Task
{

    protected $repository;

    public function __construct(WarehouseOrderItemsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($filter, $skipPagination = false,int $limit = 10)
    {
        $result =  WarehouseOrderItems::select('warehouse_order.type','warehouse_order.status','warehouse_order.warehouse_id','warehouse_order.created','warehouse_order_items.id','warehouse_order_items.product_id','warehouse_order_items.product_variants_id','warehouse_order_items.warehouse_order_id','warehouse_order_items.created_at')
        ->selectRaw('SUM(warehouse_order_items.quantity_origin) as total_quantity')
        // ->join('warehouse_order','warehouse_order.id','=','warehouse_order_items.warehouse_order_id')
        ->join('warehouse_order', function ($join) {
          $join->on('warehouse_order.id','=','warehouse_order_items.warehouse_order_id')->orderBy('warehouse_order.created','ASC');
        })
        ->groupByRaw('product_variants_id')->orderBy('id','DESC');
            $result->whereNull('warehouse_order.cart_order_id');
        if(isset($filter['type']) && $filter['type']!=''){
            $result->where('warehouse_order.type', $filter['type']);
        }
        if(isset($filter['status']) && $filter['status']!=''){
            $result->where('warehouse_order.status', $filter['status']);
        }
        if(isset($filter['code']) && $filter['code']!=''){
            $result->where('warehouse_order.status', $filter['status']);
        }
        if(isset($filter['idWarehouse']) && $filter['idWarehouse']!=''){
            $result->where('warehouse_order.warehouse_id', $filter['idWarehouse']);
        }
        return  $skipPagination ? $result->all() :  $result->paginate($limit);
    }

    // public function equalTab($tab) {
    //     $this->repository->pushCriteria(new ByStatusCriteria($tab));
    // }

    // public function equalIdWarehouseOrder($id) {
    //     $this->repository->pushCriteria(new ByIdCriteria($id));
    // }

    // public function equalIdWarehouse($id){
    //     $this->repository->pushCriteria(new ByIdWarehouseCriteria($id));
    // }
}
