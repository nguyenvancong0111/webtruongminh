<?php

namespace App\Containers\Warehouse\Tasks\WarehouseOrder;

use App\Containers\Warehouse\Data\Repositories\WarehouseOrderRepository;

use App\Ship\Criterias\Eloquent\ByIdCriteria;
use App\Ship\Criterias\Eloquent\ByStatusCriteria;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;

/**
 * Class GetAllProductssTask.
 */
class GetWarehouseOrderDetailTask extends Task
{

    protected $repository;

    public function __construct(WarehouseOrderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($id)
    {   
        $data=$this->repository->with(['orderDetail' => function ($query) {
            $query->with('productDetail');
            
        },'warehouse'])->where('id',$id)->first();
        $data->load([
            'orderDetail.product.desc',
            'orderDetail.productDetail.product_option_values',
            'orderDetail.productDetail.product_option_values.option.desc',
            'orderDetail.productDetail.product_option_values.option_value.desc',
          ]);
        return $data ;
    }
}
