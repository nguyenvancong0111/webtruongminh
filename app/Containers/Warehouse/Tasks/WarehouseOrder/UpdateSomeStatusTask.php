<?php

namespace App\Containers\Warehouse\Tasks\WarehouseOrder;

use App\Containers\Warehouse\Data\Repositories\WarehouseOrderRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateSomeStatusTask extends Task
{

    protected $repository;

    public function __construct(WarehouseOrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
