<?php

namespace App\Containers\Warehouse\Tasks\WarehouseOrder;

use App\Containers\Warehouse\Data\Repositories\WarehouseOrderRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateWarehouseOrderTask extends Task
{

    protected $repository;

    public function __construct(WarehouseOrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            $dataFn = [
                'created' => time(),
                'warehouse_id' => $data['warehouseStore'],
                'created' => time(),
                'type' =>  $data['typeOrder'] == 1 ?  'In' : 'Out',
            ];
            return $this->repository->create($dataFn);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
