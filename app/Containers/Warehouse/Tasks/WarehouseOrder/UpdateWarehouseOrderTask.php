<?php

namespace App\Containers\Warehouse\Tasks\WarehouseOrder;

use App\Containers\Warehouse\Data\Repositories\WarehouseOrderRepository;
use App\Containers\Warehouse\Data\Repositories\WarehouseOrderItemsRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateWarehouseOrderTask extends Task
{

    protected $repository;

    public function __construct(WarehouseOrderRepository $repository, WarehouseOrderItemsRepository $repositoryWarehouseOrderItems)
    {
        $this->repository = $repository;
        $this->repositoryWarehouseOrderItems = $repositoryWarehouseOrderItems;
    }

    public function run($data, int $totalCount)
    {
        try {
            $dataFn=[
                'code' => $data['type'] == 'In' ?  'PN-'.$data->id : 'PX-'.$data->id,
                'quantity' => $totalCount ?? 0
            ];
            return $this->repository->update($dataFn, $data->id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
