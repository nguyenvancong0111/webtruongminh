<?php

namespace App\Containers\Warehouse\Models;

use App\Ship\Parents\Models\Model;

class WarehouseOrder extends Model
{   protected $table = 'warehouse_order';

    protected $fillable = [
        'type',
        'quantity',
        // 'title',
        'status',
        'created',
        'product_id',
        'warehouse_id',
        'code',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    
    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'warehouses';

    public function orderDetail(){
        return $this->hasMany(WarehouseOrderItems::class,'warehouse_order_id','id');

    }
    public function warehouse(){
        return $this->hasOne(Warehouse::class,'id','warehouse_id');

    }
}
