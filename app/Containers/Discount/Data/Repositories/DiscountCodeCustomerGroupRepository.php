<?php

namespace App\Containers\Discount\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class DiscountCodeCustomerGroupRepository
 */
class DiscountCodeCustomerGroupRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
