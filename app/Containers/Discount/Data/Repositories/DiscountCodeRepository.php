<?php

namespace App\Containers\Discount\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class DiscountRepository
 */
class DiscountCodeRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
