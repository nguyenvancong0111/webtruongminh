<?php

namespace App\Containers\Discount\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class DiscountCodeCustomerRepository
 */
class DiscountCodeCustomerRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
