<?php

namespace App\Containers\Discount\Tasks;

use Exception;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Containers\Discount\Data\Repositories\DiscountCodeRepository;

class StoreDiscountCodeTask extends Task
{

    protected $repository;

    public function __construct(DiscountCodeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw $exception;
        }
    }
}
