<?php

namespace App\Containers\Discount\Tasks;

use App\Containers\Discount\Data\Repositories\DiscountCodeRepository;
use App\Ship\Parents\Tasks\Task;

class GetDiscountCodeStatisticByTypeTask extends Task
{

    protected $repository;

    public function __construct(DiscountCodeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository
                    ->selectRaw('SUM(use_success) as total_use, discount_type')
                    ->groupBy('discount_type')
                    ->get();
    }
}
