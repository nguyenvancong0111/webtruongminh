<?php

namespace App\Containers\Discount\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Discount\Models\DiscountCode;
use App\Containers\Discount\Data\Repositories\DiscountRepository;
use App\Containers\Discount\Data\Repositories\DiscountCodeRepository;

class StoreDiscountCodeMetaTask extends Task
{

    protected $repository;

    public function __construct(DiscountCodeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(DiscountCode $discountCode, array $meta=[])
    {
      try {
        // Loại free ship
        if ($discountCode->isDiscountFreeShip()) {
          if ($discountCode->isFreeShipSpecialLocation()) {
            $discountCode->districts()->sync($meta['districtIds']);
          }
        }else {
          // Loại discount trên sản phẩm
          if ($discountCode->isApplyForCollection()) {
            $discountCode->productCollections()->sync($meta['productCollectionsIds']);
          }

          if ($discountCode->isApplyForProducts()) {
            $discountCode->products()->sync($meta['productsIds']);
          }
        }

        if ($discountCode->isUseForCustomers()) {
          $discountCode->customers()->sync($meta['customersIds']);
        }

        if ($discountCode->isUseForCustomerGroups()) {
          $discountCode->customerGroups()->sync($meta['customerGroupsIds']);
        }

        return $discountCode;
      }catch(\Exception $e) {
        throw $e;
      }
    }
}
