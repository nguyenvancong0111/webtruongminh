<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-15 15:44:49
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 15:16:31
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\Tasks\Getters;

use Exception;
use App\Containers\Discount\Data\Repositories\DiscountCodeRepository;
use App\Containers\Discount\Enums\DiscountCodeCustomerType;
use App\Ship\Criterias\Eloquent\ThisInThatCriteria;

class ListingDiscountCodeTask extends BaseGetCodeTask
{
    protected $repository;

    public function __construct(DiscountCodeRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    public function run(int $limit = 15)
    {
        try {
            // DB::enableQueryLog();
            return $this->skipPagin ? ($limit == 0 ? $this->repository->all() : $this->repository->limit($limit)) : $this->repository->paginate($limit);
            // $this->repository->paginate(15);
            // dd(DB::getQueryLog());
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    public function byListCode(array $code): self
    {
        $this->repository->pushCriteria(new ThisInThatCriteria('code', $code));
        return $this;
    }

    public function byCustomer(int $customer_id,string $type = DiscountCodeCustomerType::ONSAVE): self
    {
        $this->repository->with(['customers'])->whereHas('customers', function ($q) use ($customer_id,$type) {
            $q->where('customer_id', $customer_id);
//            $q->where('type',$type);
        });
        return $this;
    }
}
