<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-15 15:44:49
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 15:16:09
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\Tasks\Getters;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Discount\Enums\DiscountCodeStatus;
use App\Ship\Criterias\Eloquent\OrderByFieldsCriteria;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use Carbon\Carbon;

class BaseGetCodeTask extends Task
{
    protected $repository;

    public function discountType(string $type): self
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('discount_type', $type));
        return $this;
    }

    public function objectUse(string $objectUse):self {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('object_use',$objectUse));
        return $this;
    }

    public function isActive(): self
    {
        $now = Carbon::now();
        $this->repository->pushCriteria(new ThisEqualThatCriteria('status', DiscountCodeStatus::ACTIVE));

        $this->repository->pushCriteria(new ThisOperationThatCriteria('start_date', $now, '<='));
        $this->repository->pushCriteria(new ThisOperationThatCriteria('end_date', $now, '>'));
        
        return $this;
    }

    public function orderBy(array $orderByArr) :self{
        $this->repository->pushCriteria(new OrderByFieldsCriteria($orderByArr));
        return $this;
    }
}
