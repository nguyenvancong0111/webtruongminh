<?php

namespace App\Containers\Discount\Tasks;

use App\Containers\Discount\Data\Repositories\DiscountCodeRepository;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindDiscountByIdTask extends Task
{

  protected $repository;

  public function __construct(DiscountCodeRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run($id)
  {
    try {
      return $this->repository->find($id);
    } catch (Exception $exception) {
      throw new NotFoundException();
    }
  }

  public function selectFields(array $column = ['*'])
  {
    return $this->repository->pushCriteria(new SelectFieldsCriteria($column));
  }
} // End class
