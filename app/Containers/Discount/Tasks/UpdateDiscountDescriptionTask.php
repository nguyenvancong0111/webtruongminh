<?php

namespace App\Containers\Discount\Tasks;

use App\Containers\Discount\Data\Repositories\DiscountCodeDescriptionRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateDiscountDescriptionTask extends Task
{

    protected $repository;

    public function __construct(DiscountCodeDescriptionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            foreach ($data as $key => $value) {
                $checkIsset = $this->repository->where('language_id',$key)->where('discount_code_id',$id)->count();
                if($checkIsset == 0){ 
                    $this->repository->create($value);
                }else{
                    $this->repository->where('language_id',$key)->where('discount_code_id',$id)->update($value);
                }
            }
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
