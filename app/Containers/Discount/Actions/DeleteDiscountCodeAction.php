<?php

namespace App\Containers\Discount\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteDiscountCodeAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Discount@DeleteDiscountCodeTask', [$request->id]);
    }
}
