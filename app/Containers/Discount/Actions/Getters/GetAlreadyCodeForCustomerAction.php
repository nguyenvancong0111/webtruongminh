<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-15 16:20:37
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 15:18:55
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\Actions\Getters;

use App\Containers\Discount\Enums\DiscountCodeApplyForCustomer;
use App\Ship\Parents\Actions\Action;
use App\Containers\Discount\Tasks\Getters\ListingDiscountCodeTask;
use App\Containers\Localization\Models\Language;

class GetAlreadyCodeForCustomerAction extends Action
{
    public function run(int $customer_id,int $limit = 20,?Language $currentLang = null,int $curentPage = 1)
    {
        $coupons = app(ListingDiscountCodeTask::class)
            ->currentLang($currentLang)
            ->externalWith($this->externalWith)
            ->with([
                'desc'
            ])
            // ->discountType(CouponApplyForCustomer::ALL)
            // ->objectUse(DiscountCodeApplyForCustomer::ALL)
            ->isActive()
            ->byCustomer($customer_id)
            ->orderBy([
                ['end_date', 'asc']
            ])
            ->run($limit);
        // dd($coupons);
        return $coupons;
    }

    public function descDiscountCode(?Language $currentLang): self
    {
        $languageId = $currentLang ? $currentLang->language_id : 1;
        $this->externalWith = array_merge($this->externalWith, [
            'desc' => function ($query) use ($languageId) {
                $query->select('id', 'discount_code_id', 'name', 'short_description', 'description', 'image');
                $query->activeLang($languageId);
            }
        ]);
        return $this;
    }
}
