<?php

namespace App\Containers\Discount\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllDiscountsCodeAction extends Action
{
  public function run($transporter, array $with=[], array $column=['*'])
  {
    // $data = $transporter->toArray();
    $discounts = Apiato::call('Discount@GetAllDiscountsCodeTask', [], [
      ['filterDiscountCode' => [$transporter]],
      ['with' => [$with]],
      ['selectFields' => [$column]]
    ]);
    return $discounts;
  }
} // End class
