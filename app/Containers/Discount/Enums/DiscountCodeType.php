<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-18 14:37:13
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class DiscountCodeType extends BaseEnum
{
    /**
     * Miễn phí giao hàng
     */
    const FREESHIP = 'freeship';

    /**
     * Giảm theo phần trăm
     */
    const PERCENT = 'percent';

    /**
     * Giảm trực tiếp số tiền cụ thể
     */
    const AMOUNT = 'amount';

    const TEXT = [
        self::FREESHIP => 'Miễn phí giao hàng',
        self::PERCENT => 'Giảm %',
        self::AMOUNT => 'Giảm tiền'
    ];
}
