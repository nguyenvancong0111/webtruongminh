<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-18 14:38:10
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class DiscountCodeApplyForProduct extends BaseEnum
{
    /**
     * Tất cả sản phẩm
     */
    const ALL = "*";

    /**
     * Cho Bộ sưu tập sản phẩm     */
    const COLLECTION = 'collection';

    /**
     * Cho sản phẩm cụ thể
     */
    const SINGLE_PRODUCT = 'single_product';

    const TEXT = [
        self::ALL => 'Tất cả sản phẩm',
        self::COLLECTION => 'Bộ sưu tập',
        self::SINGLE_PRODUCT => 'Sản phẩm cụ thể'
    ];
}
