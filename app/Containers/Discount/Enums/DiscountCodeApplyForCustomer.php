<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-18 14:38:28
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class DiscountCodeApplyForCustomer extends BaseEnum
{
    /**
     * Tất cả mọi người
     */
    const ALL = "*";

    /**
     * Cho nhóm khách hàng cụ thể
     */
    const GROUP = 'group';

    /**
     * Cho cá nhân cụ thể
     */
    const PERSONAL = 'personal';

    const TEXT = [
        self::ALL => 'Tất cả mọi người',
        self::GROUP => 'Bộ sưu tập',
        self::PERSONAL => 'Cá nhân cụ thể'
    ];
}
