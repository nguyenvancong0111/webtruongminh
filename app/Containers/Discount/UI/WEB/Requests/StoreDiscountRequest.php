<?php

namespace App\Containers\Discount\UI\WEB\Requests;

use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Ship\Parents\Requests\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * Class StoreDiscountRequest.
 */
class StoreDiscountRequest extends Request
{
  use ApiResTrait;
  /**
   * The assigned Transporter for this Request
   *
   * @var string
   */
  protected $transporter = \App\Containers\Discount\Data\Transporters\CreateDiscountTransporter::class;

  /**
   * Define which Roles and/or Permissions has access to this request.
   *
   * @var  array
   */
  protected $access = [
    'permissions' => '',
    'roles'       => '',
  ];

  /**
   * Id's that needs decoding before applying the validation rules.
   *
   * @var  array
   */
  protected $decode = [
    // 'id',
  ];

  /**
   * Defining the URL parameters (e.g, `/user/{id}`) allows applying
   * validation rules on them and allows accessing them like request data.
   *
   * @var  array
   */
  protected $urlParameters = [
    // 'id',
  ];

  /**
   * @return  array
   */
  public function rules()
  {
    $input = request()->all();
    $rules =  [
      'discount.code' => 'required|min:6|unique:discount_code,code',
      'discount.discount_type' => 'required|in:percent,amount,freeship,geta2b',
      'discount.product_use' => 'required|in:*,collection,single_product',
      'discount.quantity' => 'required|numeric|min:1',
      'discount.start_date' => 'required',
      'discount.end_date' => 'required|after:discount.start_date',
      'discount.is_use_once' => 'required|in:1,0'
    ];
    $discount = $input['discount'];
    if ($discount['discount_type'] == 'freeship' && $discount['quanhuyen'] == 'single_quanhuyen') {
      $rules['meta.districtIds'] = 'required|array';
      $rules['meta.districtIds.*'] = 'numeric';
    }

    if($discount['discount_type'] == 'percent'){
      $rules['discount.discount_value'] = 'nullable|numeric|max:100';
    }

    if ($discount['discount_type'] == 'percent' || $discount['discount_type'] == 'amount') {
      if ($discount['product_use'] == 'collection') {
        $rules['meta.productCollectionsIds'] = 'required|array';
        $rules['meta.productCollectionsIds.*'] = 'numeric';
      }

      if ($discount['product_use'] == 'single_product') {
        $rules['meta.productsIds'] = 'required|array';
        $rules['meta.productsIds.*'] = 'numeric';
      }
    }

    if ($discount['object_use'] == 'personal') {
      $rules['meta.customersIds'] = 'required|array';
      $rules['meta.customersIds.*'] = 'numeric';
    }

    if ($discount['object_use'] == 'groups') {
      $rules['meta.customerGroupsIds'] = 'required|array';
      $rules['meta.customerGroupsIds.*'] = 'numeric';
    }
    return $rules;
  }

  public function messages() {
    return [
      'discount.code.required' => 'Mã giảm giá là bắt buộc',
      'discount.code.unique' => 'Mã Code đã tồn tại',
      'discount.code.min' => 'Mã giảm giá phải có độ dài tối thiểu 6 ký tự',
      'discount.discount_type.required' => 'Loại giảm giá là bắt buộc',
      'discount.discount_type.in' => 'Loại giảm giá không đúng định dạng',
      'discount.product_use.required' => 'Đối tượng áp dũng mã là bắt buộc',
      'discount.product_use.in' => 'Đối tượng áp dũng mã không đúng định dạng',
      'discount.quantity.required' => 'Số lượng mã giảm giá là bắt buộc',
      'discount.quantity.numeric' => 'Số lượng mã giảm giá phải là dạng số',
      'discount.quantity.min' => 'Số lượng mã giảm ít nhất là 1 mã',

      'discount.start_date.required' => 'Ngày bắt đầu sử dụng mã là bắt buộc',
      'discount.end_date.required' => 'Ngày kết thúc sử dụng mã là bắt buộc',

      'meta.districtIds.required' => 'Quận/huyện là bắt buộc',
      'meta.districtIds.array' => 'Quận/huyện phải là dạng mảng',
      'meta.districtIds.*.numeric' => 'Quận/huyện item phải là dạng số',

      'meta.productCollectionsIds.required' => 'Bộ sưu tập là bắt buộc',
      'meta.productCollectionsIds.array' => 'Bộ sưu tập phải là dạng mảng',
      'meta.productCollectionsIds.*.numeric' => 'Bộ sưu tập item phải là dạng số',

      'meta.productsIds.required' => 'Sản phẩm là bắt buộc',
      'meta.productsIds.array' => 'Sản phẩm phải là dạng mảng',
      'meta.productsIds.*.numeric' => 'Sản phẩm item phải là dạng số',

      'meta.customersIds.required' => 'Khách hàng là bắt buộc',
      'meta.customersIds.array' => 'Khách hàng phải là dạng mảng',
      'meta.customersIds.*.numeric' => 'Khách hàng item phải là dạng số',

      'meta.customerGroupsIds.required' => 'Nhóm khách hàng là bắt buộc',
      'meta.customerGroupsIds.array' => 'Nhóm khách hàng phải là dạng mảng',
      'meta.customerGroupsIds.*.numeric' => 'Nhóm khách hàng item phải là dạng số',
      'discount.end_date.after' => 'Ngày kết thúc phải sau ngày bắt đầu',
      'discount.is_use_once.required' => 'Sử dụng duy nhất là bắt buộc',
      'discount.is_use_once.in' => 'Sử dụng duy nhất đang có giá trị không đúng quy định',
      'discount.discount_value.max' => 'Giá trị giảm phải nhỏ hơn 100%',
      'discount.discount_value.numeric' => 'Giá trị giảm phải là số',
    ];
  }

  /**
   * @return  bool
   */
  public function authorize()
  {
    return $this->check([
      'hasAccess',
    ]);
  }

  protected function failedValidation(Validator $validator)
  {
    throw new HttpResponseException($this->sendError($validator->errors(), 422), 422);
  }

  protected function prepareForValidation()
  {
    $merge_data = [
      'status' => 1,
      'user_id' => auth()->id()
    ];

    $this->discount = array_merge($this->discount, $merge_data);
    $this->merge([
      'discount' => $this->discount
    ]);
  }
} // End class
