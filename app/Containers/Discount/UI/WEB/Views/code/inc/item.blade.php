<tr>
  <td>
    <img src="{{ $code->getDiscountTypeIcon() }}" alt="">
    {{ $code->getDiscountTypeText() }}
  </td>
  <td>{{ $code->code }}</td>
  <td>{{ $code->discount_value_text }}</td>
  <td><b class="text-success">{{ $code->use_success }}</b> / <b>{{ $code->quantity }}</b></td>
  <td>{{ $code->getObjectUseText() }}</td>
  <td>{{ $code->getObjectApplyText() }}</td>
  <td>
    @if ($code->isActive())
      <span class="badge badge-success">Đang hoạt động</span>
    @else
      <span class="badge badge-warning">Tạm ngưng</span>
    @endif
  </td>
  <td>{{ \FunctionLib::dateFormat($code->start_date, 'd/m/Y H:i') }} - {{ \FunctionLib::dateFormat($code->end_date, 'd/m/Y H:i') }}</td>
  <td>

    <div class="dropdown show">
      @dropdown('Chọn')

      <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
        {{-- <a  class="dropdown-item"
            href="javascript:void(0)"
            onclick="return loadIframe(this, '{{ route('admin.discount.code.history', ['id' => $code->id]) }}')">Lịch sử thay đổi mã</a> --}}

        <a  class="dropdown-item"
            href="javascript:void(0)"
            onclick="return loadIframe(this, '{{ route('admin.discount.code.show', ['id' => $code->id]) }}')">Xem thông tin chi tiết</a>

        <a  class="dropdown-item"
            href="{{ route('admin.discount.code.edit', ['id' => $code->id]) }}">
          Sửa
        </a>
        <a class="dropdown-item"
           href="javascript:void(0)"
           onclick="return toggleDiscountCodeStatus('{{ route('admin.discount.code.toggle-status', ['id' => $code->id]) }}', '{{ $code->code }}', '{{ $code->status }}', this)">
           @if ($code->isActive())
            Ngưng sử dụng
           @else
            Sử dụng trở lại
           @endif
        </a>
        <a class="dropdown-item"
           href="javascript:void(0)"
           onclick="return removeDiscountCode(this, '{{ route('admin.discount.code.delete', ['id' => $code->id])}}')">
           Xóa
        </a>
      </div>
    </div>
  </td>
</tr>
