<hr class="row">
<div class="form-group">
    <label for="" class="fa-13e">Loại mã</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="type" id="discountType1" value="percent"
               v-model="discount.discount_type">
        <label class="form-check-label" for="discountType1">
            Giảm %
        </label>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="type" id="discountType2" value="amount"
               v-model="discount.discount_type">
        <label class="form-check-label" for="discountType2">
            Giảm số tiền cụ thể
        </label>
    </div>
    {{--<div class="form-check">
      <input class="form-check-input" type="radio" name="type" id="discountType3" value="freeship" v-model="discount.discount_type">
      <label class="form-check-label" for="discountType3">
        Miễn phí giao hàng
      </label>
    </div>--}}

    {{-- <div class="form-check ">
      <input class="form-check-input" type="radio" name="type" id="discountType4" value="geta2b" v-model="discount.discount_type">
      <label class="form-check-label" for="discountType4">
        Mua sản phẩm A được tặng sản phẩm B
      </label>
    </div> --}}
</div>

<div class="form-group ml-4">
    <div v-if="discount.discount_type != 'freeship'">
        <label for="" class="fa-13e text-success" v-if="discount.discount_type != 'geta2b'">#1. Cách thức mã hoạt
            động</label>
        <div class="" v-if="discount.discount_type == 'percent'">
            <label for="">Giá trị được giảm</label>
            <input type="number" min="0" class="form-control" v-model="discount.discount_value" placeholder="Giảm ... %"
                   onkeypress="return shop.numberOnly()" onfocus="this.select()" onchange="checkNumber(this)"
            >
        </div>

        <div class="" v-if="discount.discount_type == 'amount'">
            <label for="">Giá trị được giảm</label>
            <input type="number" min="0" class="form-control" v-model="discount.discount_value" placeholder="Giảm ... đ"
                   onkeypress="return shop.numberOnly()" onfocus="this.select()" onchange="checkNumber(this)"
            >
        </div>
    </div>


    <!-- ÁP DỤNG -->
    <div id="ap-dung" v-if="discount.discount_type != 'freeship'">
        <label for="" class="fa-13e mt-2 text-success">#2. Áp dụng cho</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="product_use" id="productUse1" value="*"
                   v-model="discount.product_use">
            <label class="form-check-label" for="productUse1">
                Đơn hàng
            </label>
        </div>

        {{--<div class="form-check">
            <input class="form-check-input" type="radio" name="product_use" id="productUse2" value="collection" v-model="discount.product_use">
            <label class="form-check-label" for="productUse2">
                Bộ sưu tập sản phẩm
            </label>
        </div>--}}

        <div class="form-check">
            <input class="form-check-input" type="radio" name="product_use" id="productUse3" value="single_product" v-model="discount.product_use">
            <label class="form-check-label" for="productUse3">
                Các khóa học cụ thể
            </label>
        </div>

        {{--@include('discount::code.inc.modal-product-collection')--}}

        @include('discount::code.inc.modal-products')
    </div><!-- /.End áp dụng -->

    <!-- YÊU CẦU TỐI THIỂU -->
    <div id="dieu-kien-phu" v-if="discount.discount_type != 'freeship'">
        <label for="" class="fa-13e mt-2 text-success">#3. Điều kiện phụ</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="extra_rule" id="extraRule1" value="none" v-model="discount.extra_rule">
            <label class="form-check-label" for="extraRule1">
                Không có điều kiện
            </label>
        </div>

        <div class="form-check">
            <input class="form-check-input" type="radio" name="extra_rule" id="extraRule2" value="order_minimun" v-model="discount.extra_rule">
            <label class="form-check-label" for="extraRule2">
                Giá trị đơn hàng tối thiểu là:
            </label>

            <div class="row col-3" v-show="discount.extra_rule == 'order_minimun'">
                <input type="text" class="form-control mt-2" placeholder="đ 0" v-model="discount.extra_rule_value" onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()">
                <small class="text-info mt-1"><i class="fa fa-info-circle"></i> Áp dụng cho <u>các sản phẩm đã chọn #2</u></small>
            </div>
        </div>

        {{-- <div class="form-check">
          <input class="form-check-input" type="radio" name="extra_rule" id="extraRule3" value="quantity_minimum" v-model="discount.extra_rule">
          <label class="form-check-label" for="extraRule3">
            Số lượng sản phẩm trong đơn hàng là:
          </label>

          <div class="row col-3" v-show="discount.extra_rule == 'quantity_minimum'">
            <input type="text" class="form-control mt-2" placeholder="Nhập số lượng sản phẩm" v-model="discount.extra_rule_value">
            <small class="text-info mt-1"><i class="fa fa-info-circle"></i> Áp dụng cho <u>các sản phẩm đã chọn #2</u></small>
          </div>
        </div> --}}
    </div><!-- /.End dieu kien phu -->

    @include('discount::code.inc.modal-quan-huyen')
</div><!-- /.End cách thức hoạt động -->