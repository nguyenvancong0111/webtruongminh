<div class="row order-log-item">
  <!-- timeline item 1 left dot -->
  <div class="col-auto text-center flex-column d-none d-sm-flex">
    <div class="row h-50">
      <div class="col {{ $loop->even ? 'border-right' : '' }}">&nbsp;</div>
      <div class="col">&nbsp;</div>
    </div>
    <h5 class="m-2">
      <span class="badge badge-pill bg-light border">&nbsp;</span>
    </h5>
    <div class="row h-50">
      <div class="col border-right">&nbsp;</div>
      <div class="col">&nbsp;</div>
    </div>
  </div>

  <!-- timeline item 1 event content -->
  <div class="col">
    <div class="card mb-2 {{ $loop->even ? 'shadow' : '' }}">
      <div class="card-body p-2" ondblclick="return loadIframe(this, '{{ route('admin.discount.code.history-detail', ['version_id' => $version->version_id, 'id' => $discountCode->id]) }}')">
        <div class="float-right {{ $loop->even ? 'text-muted' : '' }}">{{ $version->created_at }}</div>
        <h5 class="card-title {{ $loop->even ? 'text-muted' : '' }}">
          Phiên bản: #{{ $version->version_id }}
        </h5>

        <div class="d-flex">
          <span>
            <i class="fa fa-user"></i> <i class="text-primary"> {{ $version->user->email }}</i>
            @if (!empty($version->user->name))
              ( {{ $version->user->name }} )
            @endif
          </span>

          <div class="card-text dropdown show ml-auto">
            @dropdown('Thao tác')
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a  class="dropdown-item"
                  href="javascript:void(0)"
                  onclick="return loadIframe(this, '{{ route('admin.discount.code.history-detail', ['version_id' => $version->version_id, 'id' => $discountCode->id]) }}')"
                  >
                  Xem chi tiết phiên bản
              </a>

              <a  class="dropdown-item"
                  href="javascript:void(0)"
                  >
                  Sử dụng phiên bản này thay cho phiên bản hiện tại
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--/row-->
