<div class="ml-auto">
  <a href="javascript:void(0)" data-toggle="modal" data-target="#boLocKhachHang">
    <i class="fa fa-filter" aria-hidden="true"></i> Bộ lọc
  </a>

  <a  href="{{ route('admin.discount.code.create') }}"
      class="ml-3">
    <i class="fa fa-plus" aria-hidden="true"></i> Tạo mã
  </a>
</div>

<!-- Modal -->
<div class="modal fade" id="boLocKhachHang" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg mw-80" role="document">
    <div class="modal-content">
      <form action="{{ route('admin.discount.code.index') }}" class="form-horizontal" method="GET">
        <input type="hidden" name="is_filter" value="1">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-filter"></i> Bộ lọc mã giảm giá</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
          <div class="row">
            <div class="col-3">
              <label for="">Loại mã</label>
              <select name="discount_type" class="form-control">
                <option value="">---Chọn---</option>
                @foreach ($config['discount_type'] as $type => $typeText)
                <option value="{{ $type }}" @if(isset($input['discount_type']) && $input['discount_type'] == $type) selected @endif>
                  {{ $typeText }}
                </option>
                @endforeach
              </select>
            </div>

            <div class="col-3">
              <label for="">Mã code</label>
              <input type="text" class="form-control" name="code" value="{{ $input['code'] ?? '' }}">
            </div>

            <div class="col-3">
              <label for="">Tình trạng hoạt động</label>
              <select name="status" class="form-control">
                <option value="">---Chọn---</option>
                <option value="1">Đang hoạt động</option>
                <option value="2">Tạm ngưng</option>
              </select>
            </div>

            <div class="col-3">
              <label for="">Tình trạng mã còn lại</label>
              <select name="conlai" class="form-control">
                <option value="">---Chọn---</option>
                <option value="1" @if(isset($input['conlai']) && $input['conlai'] == 1) selected @endif>Còn nhiều</option>
                <option value="2" @if(isset($input['conlai']) && $input['conlai'] == 2) selected @endif>Còn ít</option>
                <option value="3" @if(isset($input['conlai']) && $input['conlai'] == 3) selected @endif>Đã hết</option>
              </select>
            </div>
          </div>

          <div class="row mt-2">
            <div class="col-3">
              <label for="">Đối tượng dùng mã</label>
              <select name="object_use" class="form-control">
                <option value="">---Chọn---</option>
                @foreach ($config['object_use'] as $objectUse => $objectUseText)
                <option value="{{ $objectUse }}"  {{ @$input['object_use'] == $objectUse ? 'selected' : '' }}>
                  {{ $objectUseText }}
                </option>
                @endforeach
              </select>
            </div>

            <div class="col-3">
              <label for="">Đối tượng áp dụng mã</label>
              <select name="product_use" class="form-control">
                <option value="">---Chọn---</option>
                @foreach ($config['product_use'] as $productUse => $productUseText)
                <option value="{{ $productUse }}" {{ @$input['product_use'] == $productUse ? 'selected' : '' }}>
                  {{ $productUseText }}
                </option>
                @endforeach
              </select>
            </div>

            <div class="col-3">
              <label for="">Thời gian dùng mã từ</label>
              <input type="text" class="form-control dateptimeicker" name="start_date" value="{{ $input['start_date'] ?? '' }}">
            </div>
          </div>
        </div>

        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-sort"></i> Sắp xếp</h5>
        </div>

        <div class="modal-body">
          <div class="row">
            <div class="col-3">
              <label for="">Loại mã</label>
              <select name="orderByType" class="form-control">
                <option value="">---Chọn---</option>
                <option value="created_at DESC" @if (isset($input['orderByType']) && $input['orderByType'] == 'created_at DESC') selected @endif>
                  Thời gian tạo mã: gần đây -> cũ nhất
                </option>
                <option value="created_at ASC" @if (isset($input['orderByType']) && $input['orderByType'] == 'created_at ASC') selected @endif>
                  Thời gian tạo mã: cũ nhất -> gần đây
                </option>
                <option value="end_date DESC" @if (isset($input['orderByType']) && $input['orderByType'] == 'end_date DESC') selected @endif>
                  Thời gian kết thúc mã: gần đây -> cũ nhất
                </option>
                <option value="end_date ASC" @if (isset($input['orderByType']) && $input['orderByType'] == 'end_date ASC') selected @endif>
                  Thời gian kết thúc mã: cũ nhất -> gần đây
                </option>
                <option value="start_date DESC" @if (isset($input['orderByType']) && $input['orderByType'] == 'start_date DESC') selected @endif>
                  Thời gian bắt đầu dùng mã: gần đây -> cũ nhất
                </option>
                <option value="start_date ASC" @if (isset($input['orderByType']) && $input['orderByType'] == 'start_date ASC') selected @endif>
                  Thời gian bắt đầu dùng mã: cũ nhất -> gần đây
                </option>
              </select>
            </div>

            <div class="col-3">
              <label for="">Số lượng mã</label>
              <select name="orderByQty" class="form-control">
                <option value="">---Chọn---</option>
                <option value="quantity DESC"  @if (isset($input['orderByQty']) && $input['orderByQty'] == 'quantity DESC') selected @endif>Tổng số lượng mã: giảm dần</option>
                <option value="quantity ASC" @if (isset($input['orderByQty']) && $input['orderByQty'] == 'quantity ASC') selected @endif>Tổng số lượng mã: tăng dần</option>
                <option value="use_success DESC" @if (isset($input['orderByQty']) && $input['orderByQty'] == 'use_success DESC') selected @endif>Số lượng mã sử dụng thành công: giảm dần</option>
                <option value="use_success ASC" @if (isset($input['orderByQty']) && $input['orderByQty'] == 'use_success ASC') selected @endif>Số lượng mã sử dụng thành công: tăng dần</option>
              </select>
            </div>

            <div class="col-3">
              <label for="">Mã code</label>
              <select name="orderByCode" class="form-control">
                <option value="">---Chọn---</option>
                <option value="code ASC" @if (isset($input['orderByCode']) && $input['orderByCode'] == 'code ASC') selected @endif>A-Z</option>
                <option value="code DESC" @if (isset($input['orderByCode']) && $input['orderByCode'] == 'code DESC') selected @endif>Z-A</option>
              </select>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng lại</button>
          <button type="submit" class="btn btn-primary">Lọc kết quả</button>
        </div>
    </form>
    </div>
  </div>
</div>
