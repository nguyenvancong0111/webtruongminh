<div role="tabpanel" class="row tab-pane" id='info'>
  <div class="card-body">
    <div class="form-group">
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" v-for="(items,index) in lang">
          <a v-if="index==0" class="nav-link active" :href="'#'+ items.short_code" role="tab" data-toggle="tab" aria-selected="true">@{{items.name}}</a>
          <a v-else class="nav-link" :href="'#'+ items.short_code" role="tab" data-toggle="tab" aria-selected="true">@{{items.name}}</a>
        </li>
      </ul>
      <div class="tab-content">
          <div v-for="items in description" role="tabpanel" v-bind:class="getClass(items.language_id)" :id="items.short_code_lang" >
            <div class="form-group">
              <div class="d-flex">
                <label for="title">Tên <span class="small text-danger">( @{{items.name_lang}} )</span></label>
              </div>
              <div class="input-group">
                <div class="input-group-prepend"><span class=" input-group-text"><img :src="'/admin/img/lang/'+items.image_lang" :title="items.name_lang"></span></div>
                <input type="text" class="form-control" v-model="items.name">
              </div>
            </div>
            <div class="form-group">
              <div class="d-flex">
                <label for="title">Mô tả ngắn <span class="small text-danger">( @{{items.name_lang}} )</span></label>
              </div>
              <div class="input-group">
                <div class="input-group-prepend"><span class=" input-group-text"><img :src="'/admin/img/lang/'+items.image_lang" :title="items.name_lang"></span></div>
                <input type="text" class="form-control" v-model="items.short_description">
              </div>
            </div>
            <div class="form-group">
              <div class="d-flex">
                <label for="title">Mô tả dài <span class="small text-danger">( @{{items.name_lang}} )</span></label>
              </div>
              <div class="input-group">
                <div class="input-group-prepend"><span class=" input-group-text"><img :src="'/admin/img/lang/'+items.image_lang" :title="items.name_lang"></span></div>
                {{-- <textarea rows="5" cols="40" class="form-control editor_desc"></textarea> --}}
                <tinymce  v-model="items.description"></tinymce>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
