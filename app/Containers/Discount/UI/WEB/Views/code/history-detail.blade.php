@extends('basecontainer::admin.layouts.default_for_iframe')
@section('content')
    <div class="row" id="sectionContent">
        <div class="col-12">
            <div class="card mb-0">
                <div class="card-header d-flex">
                    <button class="btn btn-link">Thông tin mã giảm giá:
                        <strong>{{ $discountCode->code }}</strong></button>
                    <button type="button" class="btn btn-secondary ml-auto mr-2" onclick='return closeFrame()'>Đóng
                        lại</button>
                </div>

                <div class="card-body">
                    <ul class="nav nav-tabs nav-underline nav-underline-primary">
                        <li class="nav-item">
                            <a class="nav-link active" href="#mgg" data-toggle="tab" role="tab" aria-controls="mgg">
                                Mã giảm giá
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#object_use" data-toggle="tab" role="tab" aria-controls="object_use">
                                Thông tin mở rộng
                            </a>
                        </li>

                        <li class="nav-item">
                          <a class="nav-link" href="#version" data-toggle="tab" role="tab" aria-controls="version">
                              Thông tin phiên bản
                          </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="mgg" role="tabpanel" aria-labelledby="mgg">
                            <div class="mt-2">
                                <table class="table table-hover borderless">
                                    <tr>
                                        <th>Mã giảm giá</th>
                                        <td><code>{{ $discountCode->code }}</code></td>
                                    </tr>

                                    <tr>
                                        <th>Loại giảm giá</th>
                                        <td>
                                            <img src="{{ $discountCode->getDiscountTypeIcon() }}" alt="">
                                            {{ $discountCode->getDiscountTypeText() }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Giá trị giảm</th>
                                        <td>{{ $discountCode->discount_value_text }}</td>
                                    </tr>

                                    <tr>
                                        <th>Số lượng dùng mã đã dùng/tổng số</th>
                                        <td>0/{{ $discountCode->quantity }}</td>
                                    </tr>

                                    <tr>
                                        <th>Trạng thái hoạt động của mã</th>
                                        <td>
                                            @if ($discountCode->isActive())
                                                <span class="badge badge-success">Đang hoạt động</span>
                                            @else
                                                <span class="badge badge-danger">Tạm ngưng</span>
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Khung giờ sử dụng</th>
                                        <td>{{ \FunctionLib::dateFormat($discountCode->start_date, 'd/m/Y H:i') }} -
                                            {{ \FunctionLib::dateFormat($discountCode->end_date, 'd/m/Y H:i') }}</td>
                                    </tr>

                                    <tr>
                                      <th>Sử dụng duy nhất</th>
                                      <td>
                                          @if ($discountCode->isUseOncePerCustomer())
                                              <span class="text-info">Mỗi khách chỉ được dùng mã này <b>1 lần duy nhất</b></span>
                                          @else
                                              <span class="text-warning">Khách hàng có thể sử dụng mã này <b>nhiều lần</b></span>
                                          @endif
                                      </td>
                                    </tr>

                                    @if ($discountCode->user)
                                    <tr>
                                      <th>Người thao tác<span class="text-danger">*</span></th>
                                      <td>
                                         <code>{{ $discountCode->user->name }} - {{ $discountCode->user->email }}</code>
                                      </td>
                                    </tr>
                                    @endif
                                </table>
                            </div>
                        </div><!-- /.End ma giam gia -->

                        <div class="tab-pane" id="object_use" role="tabpanel" aria-labelledby="object_use">
                            <div class="mt-2">
                                <table class="table table-hover ">
                                    @if ($discountCode->isDiscountPercent() || $discountCode->isDiscountAmount())
                                        <tr>
                                            <th class="border-0">Ai có thể sử dụng mã này?</th>
                                            <td class="border-0"><code>{{ $discountCode->getObjectUseText() }}</code></td>
                                        </tr>

                                        <tr>
                                            <th><span class="pl-4">Chi tiết</span></th>
                                            <td>
                                                @if ($discountCode->isUseForAnyOne())
                                                    Bất kỳ ai
                                                @endif

                                                @if ($discountCode->isUseForCustomerGroups())
                                                    <small class="text-info"><i class="fa fa-info-circle"
                                                            aria-hidden="true"></i> KH nào thuộc <u>các nhóm</u> dưới đây sẽ
                                                        sử dụng được mã</small>
                                                    <ol class="pl-3">
                                                        @forelse ($discountCode->customerGroups as $group)
                                                            <li>{{ $group->title }}</li>
                                                        @empty
                                                            <li>Không có thông tin nhóm</li>
                                                @endforelse
                                                </ol>
                                    @endif

                                    @if ($discountCode->isUseForCustomers())
                                        <small class="text-info"><i class="fa fa-info-circle" aria-hidden="true"></i> KH nào
                                            thuộc <u>danh sách</u> dưới đây sẽ sử dụng được mã</small>
                                        <ol class="pl-3">
                                            @forelse ($discountCode->customers as $customer)
                                                <li>
                                                    <i class="fa fa-envelope-o"></i> <a
                                                        href="mailto:{{ $customer->email }}">{{ $customer->email }}</a>
                                                    {{-- <i class="fa fa-phone ml-auto" aria-hidden="true"></i> <a href="tel:{{ $customer->phone }}">{{ $customer->phone }}</a> --}}
                                                </li>
                                            @empty
                                                <li>Không có thông tin khách hàng</li>
                                    @endforelse
                                    </ol>
                                    @endif
                                    </td>
                                    </tr>

                                    <tr>
                                        <th>Mã này dùng cho?</th>
                                        <td><code>{{ $discountCode->getObjectApplyText() }}</code></td>
                                    </tr>

                                    <tr>
                                        <th><span class="pl-4">Chi tiết</span></th>
                                        <td>
                                            @if ($discountCode->isApplyForAllProduct())
                                                <span>Toàn bộ sản phẩm</span>
                                            @endif

                                            @if ($discountCode->isApplyForCollection())
                                                <small class="text-info"><i class="fa fa-info-circle"
                                                        aria-hidden="true"></i> Sản phẩm nào nằm trong các <u>bộ sưu tập</u>
                                                    dưới đây sẽ sử dụng được mã</small>
                                                <ol class="pl-3">
                                                    @forelse ($discountCode->productCollections as $collection)
                                                        <li>{{ $collection->desc->name }}</li>
                                                    @empty
                                                        <li>Không có thông tin nhóm</li>
                                            @endforelse
                                            </ol>
                                            @endif
                                        </td>
                                    </tr>
                                    @endif

                                    @if ($discountCode->isDiscountFreeShip())
                                        <tr>
                                            <th class="border-0">Quận huyện được hỗ trợ ship</th>
                                            <td class="border-0">
                                                <small class="text-info"><i class="fa fa-info-circle"
                                                        aria-hidden="true"></i> Các khu vực nhận hàng dưới đây sẽ sử dụng
                                                    được mã</small>
                                                <ol class="pl-3">
                                                    @forelse($discountCode->districts as $district)
                                                        <li>{{ $district->district_tag_name }}</li>
                                                    @empty
                                                        <li>Không có thông tin quận huyện</li>
                                    @endforelse
                                    </ol>
                                    </td>
                                    </tr>

                                    <tr>
                                        <th>Số tiền tối đa có thể hỗ trợ ship</th>
                                        <td>{{ \FunctionLib::priceFormat($discountCode->ship_rate) }}</td>
                                    </tr>
                                    @endif

                                    <tr>
                                        <th>Điều kiện phụ</th>
                                        <td>
                                            @if ($discountCode->hasExtraRule())
                                                <span>
                                                    {{ $discountCode->getExtraRuleText() }} từ:
                                                    <code>{{ $discountCode->getExtraRuleValueDisplayText() }}</code>
                                                </span>
                                            @else
                                                <span>Không có điều kiện phụ</span>
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div><!-- /.End tac nhan su dung -->

                        <div class="tab-pane" id="version" role="tabpanel" aria-labelledby="version">
                          <div class="mt-2">
                              <table class="table table-hover borderless">
                                  @forelse($versions as $version)
                                    <tr>
                                      <th>Phiên bản</th>
                                      <td class="text-primary">#{{ $version->version_id}}</td>
                                    </tr>

                                    <tr>
                                      <th>Ngày cập nhật</th>
                                      <td>{{ $version->created_at}}</td>
                                    </tr>
                                  @empty
                                    <tr>
                                      <td colspan="2">Không có thông tin</td>
                                    </tr>
                                  @endforelse
                              </table>
                          </div>
                      </div><!-- /.End ma giam gia -->
                    </div>
                </div><!-- /.Card-body -->
            </div><!-- /.End card -->
        </div>
    </div>
@endsection
