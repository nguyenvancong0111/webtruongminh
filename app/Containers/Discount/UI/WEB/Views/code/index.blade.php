@extends('basecontainer::admin.layouts.default')

@section('right-breads')
{!! $discountsCode->appends($input)->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12 position-relative">
            <div class="card card-accent-primary">
              <div class="card-header d-flex">
                <button class="btn btn-link p-0">Danh sách mã giảm giá</button>
                @include('discount::code.inc.filter')
              </div>

              <table class="table table-hover table-bordered mb-0">
                <thead>
                  <tr>
                    <th>Loại giảm giá</th>
                    <th>Mã giảm giá</th>
                    <th>Giá trị giảm</th>
                    <th>Mã đã dùng/Tổng số mã</th>
                    <th>Đối tượng sử dụng</th>
                    <th>Đối tượng áp dụng</th>
                    <th>Trạng thái mã</th>
                    <th>Thời gian áp dụng</th>
                    <th>Thao tác</th>
                  </tr>
                </thead>

                <tbody>
                  @forelse($discountsCode as $code)
                    @include('discount::code.inc.item', ['code' => $code])
                  @empty
                    <tr>
                      <td colspan="9">Không có thông tin mã giảm giá</td>
                    </tr>
                  @endforelse
                </tbody>
              </table>
            </div>
        </div>
    </div>
    </div>
@endsection
