<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 14:52:31
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\UI\API\Controllers\FrontEnd;

use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use App\Containers\Discount\UI\API\Controllers\FrontEnd\Features\AlreadyCoupons;
use App\Containers\Discount\UI\API\Controllers\FrontEnd\Features\CustomerAlreadyCoupons;
use App\Containers\Discount\UI\API\Controllers\FrontEnd\Features\CustomerSaveCoupon;

class Controller extends BaseApiFrontController
{
    use AlreadyCoupons,
        CustomerAlreadyCoupons,
        CustomerSaveCoupon;

    public function __construct()
    {
        parent::__construct();
    }
}
