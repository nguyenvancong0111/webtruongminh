<?php

namespace App\Containers\Discount\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Discount\Actions\Getters\GetAlreadyCodeForCustomerAction;
use App\Containers\Discount\UI\API\Requests\FrontEnd\CustomerAlreadyCouponsRequest;
use App\Containers\Discount\UI\API\Transformers\FrontEnd\ListCouponCodeTransfomer;

trait CustomerAlreadyCoupons
{
    public function customerAlreadyCoupons(CustomerAlreadyCouponsRequest $request)
    {
        $coupons = app(GetAlreadyCodeForCustomerAction::class)
            ->descDiscountCode($this->currentLang)
            ->run($request->id, 24, $this->currentLang, $request->page ?? 1);
        return $this->transform($coupons, new ListCouponCodeTransfomer, [], [], 'coupons');
    }
}
