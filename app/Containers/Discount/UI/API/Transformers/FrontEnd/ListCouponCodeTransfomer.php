<?php

namespace App\Containers\Discount\UI\API\Transformers\FrontEnd;

use App\Containers\Discount\Models\DiscountCode;
use App\Ship\Parents\Transformers\Transformer;
use Carbon\Carbon;

class ListCouponCodeTransfomer extends Transformer
{
    public function transform(DiscountCode $entity)
    {
        $data =[
            'id' => $entity->id,
            'code' => $entity->code,
            'type' => $entity->discount_type == 'percent' ? 1 : 0,
            'end_date' => Carbon::createFromDate($entity->end_date)->format('d/m/Y H:i:s'),
            'name' => $entity->desc->name,
            'short_description' => $entity->desc->short_description,
            'description' => $entity->desc->description
        ];

        return $data;
    }
}
