<?php

namespace App\Containers\Discount\Models;

use App\Ship\Parents\Models\Model;

class DiscountCodeDistrict extends Model
{
    protected $table = 'discount_code_district';

    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'discountcodedistricts';
}
