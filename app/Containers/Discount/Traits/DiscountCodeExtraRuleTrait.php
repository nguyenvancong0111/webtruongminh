<?php

namespace App\Containers\Discount\Traits;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Discount\Enums\DiscountCodeExtraRule;

trait DiscountCodeExtraRuleTrait
{
  // Điều kiện mã mở rộng
  public function hasExtraRule(): bool
  {
    return !empty($this->extra_rule) && $this->extra_rule != DiscountCodeExtraRule::NONE;
  }

  public function isExtraRuleMinimunOrderValue(): bool
  {
    return ($this->hasExtraRule() && $this->extra_rule == DiscountCodeExtraRule::ORDER_MINIMUM);
  }

  public function isExtraRuleMinimunQtyItem(): bool
  {
    return ($this->hasExtraRule() && $this->extra_rule == DiscountCodeExtraRule::QUANTITY_MINIMUM);
  }

  public function getExtraRuleText(): string
  {
    if ($this->isExtraRuleMinimunOrderValue()) {
      return 'Đơn hàng có giá trị tối thiểu';
    }

    if ($this->isExtraRuleMinimunQtyItem()) {
      return 'Đơn hàng số lượng sản phẩm';
    }

    return '';
  }

  public function getExtraRuleValueDisplayText(): string {
    if ($this->isExtraRuleMinimunOrderValue()) {
      return FunctionLib::priceFormat($this->extra_rule_value);
    }

    if ($this->isExtraRuleMinimunQtyItem()) {
      return sprintf('%s sản phẩm', $this->extra_rule_value);
    }

    return '';
  }
} // End class
