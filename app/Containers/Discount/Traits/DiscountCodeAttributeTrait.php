<?php

namespace App\Containers\Discount\Traits;

use Apiato\Core\Foundation\FunctionLib;


trait DiscountCodeAttributeTrait
{
  public function getDiscountTypeText() {
    if ($this->isDiscountPercent()) {
      return 'Giảm %';
    }

    if ($this->isDiscountAmount()) {
      return 'Giảm tiền';
    }

    if ($this->isDiscountFreeShip()) {
      return 'Miễn phí giao hàng';
    }

    if ($this->isDiscountGetA2B()) {
      return 'Qùa tặng đi kèm';
    }
  }

  public function getDiscountTypeIcon() {
    if ($this->isDiscountPercent()) {
      return asset('admin/img/discount/discount-percent.png');
    }

    if ($this->isDiscountAmount()) {
      return asset('admin/img/discount/discount-money.png');
    }

    if ($this->isDiscountFreeShip()) {
      return asset('admin/img/discount/discount-freeship.png');
    }

    if ($this->isDiscountGetA2B()) {
      return 'Qùa tặng đi kèm';
    }
  }

  public function getObjectApplyText(): string {
    if ($this->isApplyForAllProduct()) {
      return 'Sản phẩm bất kỳ';
    }

    if ($this->isApplyForCollection()) {
      return 'Bộ sưu tập';
    }

    if ($this->isApplyForProducts()) {
      return 'Sản phẩm cụ thể';
    }

    return '';
  }

  public function getObjectUseText(): string {
    if ($this->isUseForAnyOne()) {
      return 'Cho Bất kỳ ai';
    }

    if ($this->isUseForCustomerGroups()) {
      return 'Cho Nhóm khách hàng';
    }

    if ($this->isUseForCustomers()) {
      return 'Cho Khách hàng cụ thể';
    }

    return '';
  }

  public function getDiscountValueTextAttribute(): string {
    if ($this->isDiscountPercent()) {
      return $this->discount_value . '%';
    }

    if ($this->isDiscountAmount()) {
      return FunctionLib::priceFormat($this->discount_value);
    }

    return '';
  }
} // End class

