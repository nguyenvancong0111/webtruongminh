<?php

namespace App\Containers\Customer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindCustomerCourseAction extends Action
{
    public function run($filter)
    {
        $customer_course = Apiato::call('Customer@FindCustomerCourseTask', [$filter]);

        return $customer_course;
    }
}
