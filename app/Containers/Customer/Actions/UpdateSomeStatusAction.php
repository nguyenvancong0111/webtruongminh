<?php

namespace App\Containers\Customer\Actions;

use App\Ship\Parents\Actions\Action;


class UpdateSomeStatusAction extends Action
{
    public function run(string $field,int $id, int $status) :? bool
    {
        $result = $this->call('Customer@UpdateSomeStatusTask',[
            $field,
            $id,
            $status
        ]);

        $this->clearCache();

        return $result;
    }
}
