<?php

namespace App\Containers\Customer\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class FindCustomerByEmailAction extends Action
{
    public function run($customer_email)
    {
        $customer = Apiato::call('Customer@FindCustomerByEmailTask', [$customer_email]);

        return $customer;
    }
}
