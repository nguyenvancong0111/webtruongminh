<?php

namespace App\Containers\Customer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class ImportCustomerAction extends Action
{
    public function run(array $insert, array $update)
    {
        $import = Apiato::call('Customer@ImportCustomerTask', [$insert, $update]);
        return $import;
    }
}
