<?php

namespace App\Containers\Customer\Actions;

use App\Containers\Customer\Actions\FrontEnd\ActiveCustomerCourseAction;
use App\Ship\Parents\Actions\Action;


class UpdateCustomerCourseAction extends Action
{
    public function run($filter, $dataUpdate)
    {
        $customer_course = $this->call('Customer@UpdateCustomerCourseTask', [$filter, $dataUpdate]);

        return $customer_course;
    }
}
