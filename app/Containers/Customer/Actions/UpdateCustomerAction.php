<?php

namespace App\Containers\Customer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class UpdateCustomerAction extends Action
{
    public function run(DataTransporter $transporter)
    {
      if ($transporter->password) {
        $transporter->password = bcrypt($transporter->password);
        $transporter->password_encode = bcrypt($transporter->password);
      }

      $customerPostData = $transporter->toArray();
      if (empty($customerPostData['password'])){
          unset($customerPostData['password']);
      }
      $customer = Apiato::call('Customer@UpdateCustomerTask', [$transporter->id, $customerPostData]);
      return $customer;
    }
}
