<?php

namespace App\Containers\Customer\Actions\FrontEnd;

use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\ImageURL;
use App\Ship\Parents\Actions\Action;
use Carbon\Carbon;

class CheckCustomerCourseAction extends Action
{
    public function run(array $filters = [])
    {
        $customer_course = $this->call('Customer@CheckCustomerCourseTask', [$filters]);

        if (!empty($customer_course)){
            $current_time = strtotime(Carbon::now('Asia/Ho_Chi_Minh')->toDateTimeString());
            $end_time = strtotime('+'.$customer_course->available_time.' day', strtotime($customer_course->start_time));
            if ($end_time > $current_time || $customer_course->available_time == 0){
                return true;
            }
            return false;
        }
        return false;
    }
}