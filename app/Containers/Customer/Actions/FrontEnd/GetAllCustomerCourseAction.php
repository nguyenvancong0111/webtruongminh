<?php

namespace App\Containers\Customer\Actions\FrontEnd;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllCustomerCourseAction extends Action
{
    public function run(array $filters = [], bool $skipPaginate = true, int $limit = 10, int $currentPage = 1, array $with = [])
    {
        $customer_course = Apiato::call('Customer@GetAllCustomerCourseTask', [$filters, $skipPaginate, $limit, $currentPage], [[$with]]);

        return $customer_course;
    }
}
