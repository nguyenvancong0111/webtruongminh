<?php

namespace App\Containers\Customer\Actions\FrontEnd;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class StoreCustomerCourseAction extends Action
{
    public function run(DataTransporter $dataTransporter)
    {
        $customer_course = Apiato::call('Customer@StoreCustomerCourseTask', [$dataTransporter->toArray()]);
        return $customer_course;
    }
}
