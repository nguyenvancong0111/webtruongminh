<?php

namespace App\Containers\Customer\Actions\FrontEnd;

use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\ImageURL;
use App\Ship\Parents\Actions\Action;
use Carbon\Carbon;

class ActiveCustomerCourseAction extends Action
{
    public function run(array $filtes = [], array $dataUpdate = [], $with = [])
    {
        $customer_course = $this->call('Customer@UpdateCustomerCourseTask', [$filtes, $dataUpdate, $with]);
        return $customer_course;
    }
}