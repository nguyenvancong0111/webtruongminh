<?php

namespace App\Containers\Customer\Actions\FrontEnd;

use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\ImageURL;
use App\Ship\Parents\Actions\Action;
use Carbon\Carbon;

class CheckCustomerCourseActiveAction extends Action
{
    public function run(array $filters = [])
    {
        $customer_course = $this->call('Customer@CheckCustomerCourseActiveTask', [$filters]);

        return $customer_course;
    }
}