<?php

namespace App\Containers\Customer\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Customer\Models\Customer;

class AttachCustomerCourseAction extends Action
{
    public function run(Customer $customer, array $course=[])
    {
        $customer->courses()->detach(array_keys($course));
        return $customer->courses()->attach($course);
    }
}
