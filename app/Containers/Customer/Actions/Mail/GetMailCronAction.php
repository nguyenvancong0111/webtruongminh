<?php

namespace App\Containers\Customer\Actions\Mail;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetMailCronAction extends Action
{
    public function run(array $where=[])
    {
        $mailcrons = Apiato::call('Customer@Mail\GetMailCronTask', [$where], [
            ['limit' => [10] ]
        ]);
        return $mailcrons;
    }
}
