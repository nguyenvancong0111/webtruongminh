<?php

namespace App\Containers\Customer\Actions\Mail;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateMailCronAction extends Action
{
    public function run(array $ids, array $updateFields=[])
    {
        $mailcron = Apiato::call('Customer@Mail\UpdateMailCronTask', [$ids, $updateFields]);
        return $mailcron;
    }
}
