<?php

namespace App\Containers\Customer\Actions\Mail;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Customer\Tasks\Mail\StoreMailCronTask;

class StoreMailCronAction extends Action
{
    public function run(array $data=[])
    {
        $mailcron = app(StoreMailCronTask::class)->run($data);
        return $mailcron;
    }
}
