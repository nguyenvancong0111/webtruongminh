<?php

namespace App\Containers\Customer\Events;

use App\Containers\Authentication\Models\PasswordReset;
use App\Containers\Customer\Models\Customer;
use App\Containers\Settings\Actions\GetAllSettingsAction;
use App\Ship\Parents\Events\Event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordEvent extends Event implements ShouldQueue
{
    use SerializesModels;

    public $passwordReset;
    public $customer;
    public $settings;


    public function __construct(PasswordReset $passwordReset, Customer $customer)
    {
        $this->passwordReset = $passwordReset;
        $this->customer = $customer;
        $this->settings = app(GetAllSettingsAction::class)->run('Array', true);
    }

    public function handle()
    {
        // Do some handling here
    }

    public function broadcastOn()
    {
        return [];
    }
}
