<?php

namespace App\Containers\Customer\Events\Handlers;

use App\Containers\BaseContainer\Events\Handlers\BaseFrontEventHandler;
use App\Containers\Customer\Events\ChangePasswordSuccessEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class ChangePasswordSuccessEmailHandler extends BaseFrontEventHandler implements ShouldQueue
{
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(ChangePasswordSuccessEvent $event)
    {
        $messageBody = view('ubacademy::pc.customer.email.change-password-success', ['customer_name' => $event->customer->fullname, 'settings' => $event->settings])->render();
        Mail::html($messageBody, function ($message) use($event) {
            $message->to($event->customer->email);
            $message->subject('Thay đổi mật khẩu thành công');
        });
    }
}
