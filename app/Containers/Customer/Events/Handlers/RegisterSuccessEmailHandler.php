<?php

namespace App\Containers\Customer\Events\Handlers;

use App\Containers\BaseContainer\Events\Handlers\BaseFrontEventHandler;
use App\Containers\Customer\Events\RegisterSuccessEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class RegisterSuccessEmailHandler extends BaseFrontEventHandler implements ShouldQueue
{
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(RegisterSuccessEvent $event)
    {
        $messageBody = view('ubacademy::pc.customer.email.register-success', ['customer_name' => $event->customer->fullname, 'settings' => $event->settings])->render();
        Mail::html($messageBody, function ($message) use($event) {
            $message->to($event->customer->email);
            $message->subject('Tạo tài khoản thành công');
        });
    }
}
