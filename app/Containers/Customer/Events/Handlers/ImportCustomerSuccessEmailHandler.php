<?php

namespace App\Containers\Customer\Events\Handlers;

use App\Containers\BaseContainer\Events\Handlers\BaseFrontEventHandler;
use App\Containers\Customer\Actions\Mail\StoreMailCronAction;
use App\Containers\Customer\Events\ImportCustomerSuccessEvent;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class ImportCustomerSuccessEmailHandler extends BaseFrontEventHandler implements ShouldQueue
{
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(ImportCustomerSuccessEvent $event)
    {

        /*$messageBody = view('ubacademy::pc.customer.email.register-success', ['customer_name' => $event->customer->fullname, 'settings' => $event->settings])->render();
        Mail::html($messageBody, function ($message) use($event) {
            $message->to($event->customer->email);
            $message->subject('Tạo tài khoản thành công');
        });*/

        if (!empty($event->customer)){
            foreach ($event->customer as $cus){
                $mailSubject = '['.env('APP_NAME').'] Tạo tài khoản thành công';
                $mailContent = view('customer::email.import-customer', ['customer_name' => $cus['fullname'], 'password' => $cus['password'], 'settings' => $event->settings])->render();
                $mailCronData[] = [
                    'email' => $cus['email'],
                    'subject' => $mailSubject,
                    'html' => preg_replace('~[\r\n]+~', '', $mailContent),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
                app(StoreMailCronAction::class)->run($mailCronData);
            }
        }

    }
}
