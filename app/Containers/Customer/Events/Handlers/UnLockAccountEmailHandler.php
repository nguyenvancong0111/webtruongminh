<?php

namespace App\Containers\Customer\Events\Handlers;

use App\Containers\BaseContainer\Events\Handlers\BaseFrontEventHandler;
use App\Containers\Customer\Events\UnLockAccountEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class UnLockAccountEmailHandler extends BaseFrontEventHandler implements ShouldQueue
{
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(UnLockAccountEvent $event)
    {
        $messageBody = view('ubacademy::pc.customer.email.unlock-account', ['delete_time' => date(now()), 'settings' => $event->settings])->render();
        Mail::html($messageBody, function ($message) use($event) {
            $message->to($event->customer->email);
            $message->subject('Thông báo mở tài khoản');
        });
    }
}
