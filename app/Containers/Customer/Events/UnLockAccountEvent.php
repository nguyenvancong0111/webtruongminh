<?php

namespace App\Containers\Customer\Events;

use App\Containers\Customer\Models\Customer;
use App\Containers\Settings\Actions\GetAllSettingsAction;
use App\Ship\Parents\Events\Event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class  UnLockAccountEvent extends Event implements ShouldQueue
{
    use SerializesModels;

    public $customer;
    public $settings;


    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
        $this->settings = app(GetAllSettingsAction::class)->run('Array', true);
    }

    public function handle()
    {
        // Do some handling here
    }

    public function broadcastOn()
    {
        return [];
    }
}
