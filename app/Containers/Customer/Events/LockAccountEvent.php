<?php

namespace App\Containers\Customer\Events;

use Apiato\Core\Abstracts\Events\Interfaces\ShouldHandleNow;
use App\Containers\Authentication\Models\PasswordReset;
use App\Containers\Customer\Models\Customer;
use App\Containers\Settings\Actions\GetAllSettingsAction;
use App\Ship\Parents\Events\Event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class  LockAccountEvent extends Event implements ShouldQueue
{
    use SerializesModels;

    public $customer;
    public $settings;


    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
        $this->settings = app(GetAllSettingsAction::class)->run('Array', true);
    }

    public function handle()
    {
        // Do some handling here
    }

    public function broadcastOn()
    {
        return [];
    }
}
