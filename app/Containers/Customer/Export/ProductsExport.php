<?php

namespace App\Containers\Product\Export;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Category\Actions\GetCategoryByIdAction;
use App\Containers\Product\Data\Repositories\ProductRepository;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProductsExport implements FromView
{
    public $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
        $products = Apiato::call('Product@Admin\GetAllProductsAction', [$this->request->all(), ['with_relationship' => ['all_desc', 'categories', 'categories.desc', 'filters', 'filters.desc', 'filters.filterGroup', 'filters.filterGroup.desc', 'images']], 0, true]);
        $dataExport = $this->dataExport($products);

        return view('product::admin.export.product_export', ['data' => $dataExport]);
    }

    public function dataExport($dataExport){
        $data = [];
        if ($dataExport->isNotEmpty()){
            foreach ($dataExport as $key => $val){
                $dataCate = [];
                $dataFilter = [];
                $dataImageSlideUrl = [];
                if ($val->categories->isNotEmpty()){
                    $category = array_column($val->categories->toArray(), 'category_id');
                    $cate = app(GetCategoryByIdAction::class)->skipCache(true)->run($category);
                    if (!empty($cate)){
                        foreach ($cate as $itm){
                            $path = [];
                            if (!empty($itm->path)){
                                $path[] = $itm->path;
                            }
                            $path[] = $itm->name;
                            $dataCate[] = implode(' > ', $path);
                        }
                    }
                }
                if ($val->filters->isNotEmpty()){
                    foreach ($val->filters as $itm){
                        $path = [$itm->filterGroup->desc->name, $itm->desc->name];
                        $dataFilter[] = implode(' > ', $path);
                    }
                }
                if ($val->images->isNotEmpty()){
                    foreach ($val->images as $itm){
                        $dataImageSlideUrl[] = \ImageURL::getImageUrl($itm->image, 'product', 'original');
                    }
                }

                $data_desc = [];
                if ($val->all_desc->isNotEmpty()){
                    foreach ($val->all_desc as $item){
                        $parameter = json_decode($item->parameter, true);
                        if (!empty($parameter)){
                            $p_key = implode('|', array_column($parameter, 'key'));
                            $p_val = implode('|', array_column($parameter, 'val'));
                        }
                        $data_desc[$item->language_id] = [
                            'name' => $item->name,
                            'key' => isset($p_key) && !empty($p_key) ? $p_key : '',
                            'val' => isset($p_val) && !empty($p_val) ? $p_val : '',
                            'meta_title' => $item->meta_title,
                            'meta_description' => $item->meta_description,
                            'meta_keyword' => $item->meta_keyword,
                        ];
                    }
                }
                $data[$key] = [
                    'ma_sp' => $val->code,
                    'ten_tiengviet' => isset($data_desc[1]) ? $data_desc[1]['name'] : '',
                    'ten_tienganh' => isset($data_desc[1]) ? $data_desc[2]['name'] : '',
                    'key_thuoctinh_tiengviet' => isset($data_desc[1]) ? $data_desc[1]['key'] : '',
                    'giatri_thuoctinh_tiengviet' => isset($data_desc[1]) ? $data_desc[1]['val'] : '',
                    'key_thuoctinh_tienganh' => isset($data_desc[2]) ? $data_desc[2]['key'] : '',
                    'giatri_thuoctinh_tienganh' => isset($data_desc[2]) ? $data_desc[2]['val'] : '',
                    'danh_muc' => isset($dataCate) && !empty($dataCate) ? implode(' | ', $dataCate) : '',
                    'bo_loc' => isset($dataFilter) && !empty($dataFilter) ? implode(' | ', $dataFilter) : '',
                    'image_slide_url' => isset($dataImageSlideUrl) && !empty($dataImageSlideUrl) ? implode(' , ', $dataImageSlideUrl) : '',
                    'image_url' => \ImageURL::getImageUrl($val->image, 'product', 'original'),
                    'gia_gach' => !empty($val->global_price) ? FunctionLib::priceFormat($val->global_price,  '') : '',
                    'gia_ban' => !empty($val->price) ? FunctionLib::priceFormat($val->price, '') : '',
                    'ton_kho' => $val->stock,
                    'new' => $val->new_old,
                    'home' => $val->is_home,
                    'hot' => $val->hot,
                    'meta_title_tiengviet' => isset($data_desc[1]) ? $data_desc[1]['meta_title'] : '',
                    'meta_description_tiengviet' => isset($data_desc[1]) ? $data_desc[1]['meta_description'] : '',
                    'meta_keyword_tiengviet' => isset($data_desc[1]) ? $data_desc[1]['meta_keyword'] : '',
                    'meta_title_tienganh' => isset($data_desc[2]) ? $data_desc[2]['meta_title'] : '',
                    'meta_description_tienganh' => isset($data_desc[2]) ? $data_desc[2]['meta_description'] : '',
                    'meta_keyword_tienganh' => isset($data_desc[2]) ? $data_desc[2]['meta_keyword'] : '',
                ];
            }
        }
        return $data;
    }
}
