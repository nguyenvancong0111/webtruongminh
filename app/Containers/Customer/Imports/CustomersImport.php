<?php

namespace App\Containers\Customer\Imports;

use App\Containers\Customer\Data\Repositories\CustomerRepository;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CustomersImport implements  ToModel,WithHeadingRow
{
    public $customer;

    public function __construct(CustomerRepository $customer)
    {
        $this->customer = $customer;
    }

    public function model(array $row)
    {
        return true;
    }

}
