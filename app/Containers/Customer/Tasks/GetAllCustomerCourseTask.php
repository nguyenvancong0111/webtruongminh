<?php

namespace App\Containers\Customer\Tasks;

use App\Containers\Customer\Data\Repositories\CustomerCourseRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllCustomerCourseTask extends Task
{

    protected $repository;

    public function __construct(CustomerCourseRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $filters = [], bool $skipPaginate = true, int $limit = 10, int $currentPage = 1)
    {
        $this->repository->with(['course.desc' => function($q){
            $q->select('id', 'course_id', 'name', 'slug');
        }]);
        if (isset($filters['customer_id'])  && !empty($filters['customer_id'])){
            $this->repository->where('customer_id', $filters['customer_id']);
        }

        $this->repository->select('id', 'customer_id', 'course_id', 'is_active', 'start_time', 'available_time', 'percent_view');

        return $skipPaginate ? ($limit > 0 ? $this->repository->limit($limit) : $this->repository->all()) : $this->repository->paginate($limit);

    }
} // End class
