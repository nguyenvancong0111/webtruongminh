<?php

namespace App\Containers\Customer\Tasks;

use App\Containers\Customer\Data\Repositories\CustomerCourseRepository;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Exceptions\UpdateResourceFailedException;

class CheckCustomerCourseActiveTask extends Task
{

    protected $repository;

    public function __construct(CustomerCourseRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $filters = [])
    {
        try {
            if (!empty($filters)){
                foreach ($filters as $key => $val){
                    $this->repository = $this->repository->where($key, $val);
                }
                $this->repository->whereHas('order_item.order', function (Builder $q){
                    $q->where('orders.payment_status', '=', 0);
                });
                return $this->repository->first();
            }
            return null;
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
