<?php

namespace App\Containers\Customer\Tasks;

use App\Containers\Customer\Data\Repositories\CustomerCourseRepository;
use App\Containers\Customer\Data\Repositories\CustomerExamRepository;
use Exception;
use Illuminate\Support\Arr;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Exceptions\UpdateResourceFailedException;

class UpdateCustomerExamTask extends Task
{

    protected $repository;

    public function __construct(CustomerExamRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $filters = [], array $dataUpdate = [])
    {
        $this->repository->updateOrCreate($filters, $dataUpdate);
    }
}
