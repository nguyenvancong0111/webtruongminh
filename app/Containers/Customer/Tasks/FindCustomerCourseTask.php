<?php

namespace App\Containers\Customer\Tasks;

use App\Containers\Customer\Data\Repositories\CustomerCourseRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindCustomerCourseTask extends Task
{

    protected $repository;

    public function __construct(CustomerCourseRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($filter)
    {
        try {
            if (is_array($filter) && !empty($filter)){
                $data = $this->repository;
                foreach ($filter as $key => $val){
                    $data= $data->where($key, $val);
                }
                return $data->first();
            }
            return null;
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
