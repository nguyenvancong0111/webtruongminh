<?php

namespace App\Containers\Customer\Tasks;

use Apiato\Core\Foundation\StringLib;
use App\Containers\Customer\Data\Repositories\CustomerRepository;
use App\Containers\Questions\Data\Repositories\QuestionsAnswerRepository;
use App\Containers\Questions\Data\Repositories\QuestionsAnswerDescRepository;
use App\Ship\Parents\Exceptions\Exception;
use App\Ship\Parents\Tasks\Task;

class ImportCustomerTask extends Task
{

    protected $repository;

    public function __construct(CustomerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $import, array $update)
    {
        try {
            if (!empty($update)) {
                $this->repository->updateMultiple($update);
            }

            if (!empty($import)) {
                $this->repository->getModel()->insert($import);
            }
        }catch (Exception $e){
            return $e->getMessage();
        }

    }
}
