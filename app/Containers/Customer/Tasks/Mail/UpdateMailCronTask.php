<?php

namespace App\Containers\Customer\Tasks\Mail;

use App\Containers\Customer\Data\Repositories\MailCronRepository;
use App\Ship\Parents\Tasks\Task;

class UpdateMailCronTask extends Task
{

    protected $repository;

    public function __construct(MailCronRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $ids, array $fields)
    {
        return $this->repository->whereIn('id', $ids)->update($fields);
    }
}
