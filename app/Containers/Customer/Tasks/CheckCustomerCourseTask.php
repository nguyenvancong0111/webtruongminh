<?php

namespace App\Containers\Customer\Tasks;

use App\Containers\Customer\Data\Repositories\CustomerCourseRepository;
use Exception;
use Illuminate\Support\Arr;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Exceptions\UpdateResourceFailedException;

class CheckCustomerCourseTask extends Task
{

    protected $repository;

    public function __construct(CustomerCourseRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $filters = [])
    {
        try {
            if (!empty($filters)){
                foreach ($filters as $key => $val){
                    $this->repository = $this->repository->where($key, $val);
                }
            }
            return $this->repository->first();
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
