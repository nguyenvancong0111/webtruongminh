<?php

namespace App\Containers\Customer\Tasks;

use App\Containers\Customer\Data\Repositories\CustomerRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class FindUserByEmailTask
 *
 * @author  Sebastian Weckend
 */
class FindCustomerByEmailTask extends Task
{

    protected $repository;

    public function __construct(CustomerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $email
     *
     * @return User
     * @throws NotFoundException
     */
    public function run($email)
    {
        try {
            if (is_array($email) && !empty($email)){
                return $this->repository->whereIn('email', $email)->select('id', 'email')->get()->keyBy('email')->toArray();
            }
            return null;
        } catch (Exception $e) {
            throw new NotFoundException();
        }
    }
}
