<?php

namespace App\Containers\Customer\Tasks;

use App\Containers\Customer\Data\Repositories\CustomerCourseRepository;
use Exception;
use Illuminate\Support\Arr;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Exceptions\UpdateResourceFailedException;

class UpdateCustomerCourseTask extends Task
{

    protected $repository;

    public function __construct(CustomerCourseRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $filters = [], array $dataUpdate = [], $with = [])
    {
        try {
            $customer = $this->repository->with($with);
            if (!empty($filters)){
                foreach ($filters as $key => $val){
                    $customer->where($key, $val);
                }
            }

            if (!empty($customer->first())){
                return $customer->update($dataUpdate, $filters['id']);
            }

            return null;
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
