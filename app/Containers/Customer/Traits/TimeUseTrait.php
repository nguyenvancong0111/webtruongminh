<?php

namespace App\Containers\Customer\Traits;
use Apiato\Core\Foundation\FunctionLib;

trait TimeUseTrait
{
  public function startTime() {
    return FunctionLib::dateFormat($this->start_time, 'd/m/Y H:i');
  }

  public function endTime(){
      return $this->available_time == 0 ? 'Vô thời hạn' : FunctionLib::dateFormat(date('Y-m-d H:i:s', strtotime('+'.$this->available_time.' day', strtotime($this->start_time))), 'd/m/Y H:i');
  }

} // End class

