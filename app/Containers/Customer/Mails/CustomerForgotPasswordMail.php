<?php

namespace App\Containers\Customer\Mails;

use App\Containers\Customer\Models\Customer;
use App\Ship\Parents\Mails\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class UserForgotPasswordMail
 *
 * @author  Sebastian Weckend
 */
class CustomerForgotPasswordMail extends Mail implements ShouldQueue
{

    use Queueable;

    /**
     * @var  App\Containers\Customer\Models\Customer
     */
    protected $recipient;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var string
     */
    protected $pass_reset;

    /**
     * UserForgotPasswordMail constructor.
     *
     * @param App\Containers\Customer\Models\Customer $recipient
     * @param                                  $token
     * @param                                  $reseturl
     */
    public function __construct(Customer $recipient, $token, $pass_reset)
    {
        $this->recipient = $recipient;
        $this->token = $token;
        $this->pass_reset = $pass_reset;
    }

    /**
     * @return  $this
     */
    public function build()
    {
        return $this->view('customer::forgot-password-passwordReset')
            ->to($this->recipient->email, $this->recipient->fullname)
            ->with(['token'  => $this->token, 'password' => $this->pass_reset, 'email'=> 'xxx',]);
    }
}
