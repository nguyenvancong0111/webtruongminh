<?php

namespace App\Containers\Customer\UI\API\Transformers\FrontEnd;

use App\Ship\Parents\Transformers\Transformer;
use App\Containers\Product\UI\API\Transformers\FrontEnd\ProductListTransformer;
use Apiato\Core\Foundation\Facades\ImageURL;
use Apiato\Core\Foundation\FunctionLib;

class ListCustomerCourseTransformer extends Transformer
{

    public function transform($data)
    {
        $response = [
            'id' => $data->id,
            'course_id' => $data->course_id,
            'is_active' => $data->is_active,
            'start_time' => $data->startTime(),
            'end_time' => $data->endTime(),
            'percent' => $data->percent_view > 100 ? 100 : round($data->percent_view),
            'courseDesc' => !empty($data->course) && !empty($data->course->desc) ? $data->course->desc : null,
            'link' => !empty($data->course) ? $data->course->link() : 'javascript:;',
            'link_study' => !empty($data->course) ? $data->course->linkStudy() : 'javascript:;',

        ];

        return $response;
    }
}
