<?php

namespace App\Containers\Customer\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Customer\Actions\FrontEnd\GetAllCustomerCourseAction;
use App\Containers\Customer\UI\API\Requests\FrontEnd\ListCustomerCourseRequest;
use App\Containers\Customer\UI\API\Transformers\FrontEnd\ListCustomerCourseTransformer;
use App\Containers\Customer\UI\API\Transformers\FrontEnd\WishListTransformer;
use App\Containers\Customer\Actions\FrontEnd\WishList\GetAllWishListByCustomerAction;
use App\Containers\Exam\Actions\GetExamsByCourseCustomerAction;

trait ListCustomerCourse
{
    public function listCustomerCourse(ListCustomerCourseRequest $request)
    {
        $customer_course = app(GetAllCustomerCourseAction::class)->skipCache(true)->run(
            ['customer_id' => $request->id], false, 1, isset($request->page) && !empty($request->page) ? $request->page : 1
        );
        return $this->transform($customer_course, new ListCustomerCourseTransformer());
    }
}
