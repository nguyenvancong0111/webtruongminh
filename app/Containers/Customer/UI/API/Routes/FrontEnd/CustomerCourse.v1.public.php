<?php

use Apiato\Core\Foundation\Facades\Apiato;

Route::group(
[
    'middleware' => [
//        'auth:api-customer'
    ],
    'prefix' => '/customer-course',
],
function () use ($router) {
    $router->any('/', [
        'as' => 'api_list_customer_course',
        'uses'       => 'FrontEnd\Controller@listCustomerCourse'
    ]);
    $router->any('/active', [
       'as' => 'api_active_customer_course',
       'uses' => 'FrontEnd\Controller@activeCustomerCourse'
    ])->where(['id' => '[0-9]+', 'code' => '[a-zA-Z0-9_\-]+']);
});