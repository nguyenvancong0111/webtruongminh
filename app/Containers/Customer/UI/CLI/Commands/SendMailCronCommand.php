<?php

namespace App\Containers\Customer\UI\CLI\Commands;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Monolog\Handler\TelegramBotHandler;

class SendMailCronCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customer_send_mailcron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mail cron';

    public $cronEnv;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->cronEnv = config('app.env');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $mailCrons = Apiato::call('Customer@Mail\GetMailCronAction', [
                ['is_sent' => 0]
            ]);

            if ($mailCrons->isNotEmpty()) {
                $successCronIds = [];

                foreach ($mailCrons as $mailCron) {
                    Mail::html($mailCron->html, function ($message) use($mailCron) {
                        $message->to($mailCron->email);
                        $message->subject($mailCron->subject);
                    });

                    $successCronIds[] = $mailCron->id;
                }

                if ( !empty($successCronIds) ) {
                    Apiato::call('Customer@Mail\UpdateMailCronAction', [
                        $successCronIds,
                        ['is_sent' => 1]
                    ]);
                }
            }
        }catch(\Exception $e) {
            throw $e;
        }
    }
}
