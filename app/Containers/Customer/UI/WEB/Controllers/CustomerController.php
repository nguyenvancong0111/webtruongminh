<?php

namespace App\Containers\Customer\UI\WEB\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\FunctionLib as FoundationFunctionLib;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Customer\Events\UnLockAccountEvent;
use App\Containers\Customer\UI\WEB\Requests\UpdateSomeStatusRequest;
use App\Containers\Customer\Actions\FindCustomerByEmailAction;
use App\Containers\Customer\Actions\FindCustomerByIdAction;
use App\Containers\Customer\Actions\ImportCustomerAction;
use App\Containers\Customer\Events\ImportCustomerSuccessEvent;
use App\Containers\Customer\Events\LockAccountEvent;
use App\Containers\Customer\Imports\CustomersImport;
use App\Containers\Customer\Models\Customer;
use App\Containers\Customer\UI\WEB\Requests\CreateCustomerRequest;
use App\Containers\Customer\UI\WEB\Requests\DeleteCustomerRequest;
use App\Containers\Customer\UI\WEB\Requests\EditCustomerRequest;
use App\Containers\Customer\UI\WEB\Requests\FilterCustomerBySeachableRequest;
use App\Containers\Customer\UI\WEB\Requests\GetAllCustomerRequest;
use App\Containers\Customer\UI\WEB\Requests\UpdateCustomerRequest;
use App\Containers\Product\Export\ProductsExport;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;
use Carbon\Carbon;

/**
 * Class Controller
 *
 * @author  Mahmoud Zalt  <mahmoud@zalt.me>
 */
class CustomerController extends AdminController
{
    use ApiResTrait;

    public function __construct()
    {
        if (FoundationFunctionLib::isDontUseShareData(['edit', 'store', 'update', 'delete', 'create'])) {
            $this->dontUseShareData = false;
        }

        parent::__construct();
    }

    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllCustomerRequest $request)
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Học viên', route('admin.customers.index'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách');
        \View::share('breadcrumb', $this->breadcrumb);

        $transporter = new DataTransporter($request);
        $transporter->type = 1;
        $customers = Apiato::call('Customer@GetAllCustomersAction', [
            $transporter,
            true,
            [],
            ['id', 'email', 'phone', 'fullname', 'created_at', 'status']
        ]);

        if ($request->ajax()) {
            return $this->sendResponse($customers);
        }

        $roles = Apiato::call('Authorization@GetAllRolesAction', [new DataTransporter([]), 0, true, ['customers']]);
        return view('customer::index', [
            'customers' => $customers,
            'input' => $request->all(),
            'roles' => $roles,
        ]);
    }

    public function create()
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Học viên', route('admin.customers.index'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Tạo Học viên', route('admin.customers.create'));
        \View::share('breadcrumb', $this->breadcrumb);

        $roles = Apiato::call('Authorization@GetAllRolesAction', [new DataTransporter([]), 0, true, ['customers']]);
        $all_perms = Apiato::call('Authorization@GetAllPermToGroupAction', [['customers']]);
//    $groups = Apiato::call('Customer@GetAllCustomerGroupsAction', []);

        return view('customer::create', [
            'roles' => $roles,
            'all_perms' => $all_perms,
//      'groups' => $groups
        ]);
    }

    public function store(CreateCustomerRequest $request)
    {
        \DB::beginTransaction();
        try {
            $transporter = new DataTransporter($request);
            $transporter->status = 2;
            $transporter->type = 1;
            if (isset($request->avatar)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'avatar', 'avatar', StringLib::getClassNameFromString(Customer::class)]);
                if (!$image['error']) {
                    $transporter->avatar = $image['fileName'];
                }
            }
            $customer = Apiato::call('Customer@StoreNewCustomerAction', [$transporter]);
            if ($customer) {
                $transporter->customer_id = $customer->id;

                if (!empty($transporter->roles_ids)) {
                    $role = Apiato::call('Authorization@SyncCustomerRolesAction', [$transporter]);
                }

                if (!empty($transporter->permissions_ids)) {
                    $perm = Apiato::call('Authorization@SyncCustomerPermissionsAction', [$transporter]);
                }

                if (!empty($transporter->customer_group_ids)) {
                    $customerGroups = Apiato::call('Customer@SyncCustomerToGroupAction', [$customer, $transporter->customer_group_ids]);
                }

                $customer->load(['roles:id,display_name', 'groups:id,title']);
                $customer->toArray();
                \DB::commit();
                return redirect()->back()->with([
                    'flash_level' => 'success',
                    'flash_message' => sprintf('Học viên: %s đã được tạo', $customer->fullname),
                    'objectData' => $customer,
                    'viewRender' => 'customer::inc.item',
                ]);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function delete(DeleteCustomerRequest $request)
    {
        //send mail
        $customer = app(FindCustomerByIdAction::class)->skipCache(true)->run($request->id, [], []);
        LockAccountEvent::dispatch($customer);
        $row = Apiato::call('Customer@DeleteCustomerAction', [$request]);


        return $this->sendResponse($row, __('Xóa Học viên thành công'));
    }

    public function edit(EditCustomerRequest $request)
    {
        $transporter = $request->toTransporter();
        $roles = Apiato::call('Authorization@GetAllRolesAction', [new DataTransporter([]), 0, true, ['customers']]);
        $all_perms = Apiato::call('Authorization@GetAllPermToGroupAction', [['customers']]);
        $groups = Apiato::call('Customer@GetAllCustomerGroupsAction', []);
        $customer = Apiato::call('Customer@FindCustomerByIdAction', [$transporter->id, ['groups']]);

        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Học viên', route('admin.customers.index'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Thông tin Học viên');
        \View::share('breadcrumb', $this->breadcrumb);
        return view('customer::edit', [
            'roles' => $roles,
            'all_perms' => $all_perms,
            'groups' => $groups,
            'customer' => $customer
        ]);
    }

    public function update(UpdateCustomerRequest $request)
    {
        \DB::beginTransaction();
        try {
            $transporter = new DataTransporter($request);
            if (isset($request->avatar)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'avatar', 'avatar', StringLib::getClassNameFromString(Customer::class)]);
                if (!$image['error']) {
                    $transporter->avatar = $image['fileName'];
                }
            }

            $customer = Apiato::call('Customer@UpdateCustomerAction', [$transporter]);
            if ($customer) {
                $transporter->customer_id = $customer->id;
                if (!empty($transporter->roles_ids)) {
                    $role = Apiato::call('Authorization@SyncCustomerRolesAction', [$transporter]);
                }

                if (!empty($transporter->permissions_ids)) {
                    $perm = Apiato::call('Authorization@SyncCustomerPermissionsAction', [$transporter]);
                }

                if (!empty($transporter->customer_group_ids)) {
                    $customerGroups = Apiato::call('Customer@SyncCustomerToGroupAction', [$customer, $transporter->customer_group_ids]);
                }

                \DB::commit();
                $customer->load(['roles:id,display_name', 'groups:id,title']);
                $customer = $customer->toArray();
                return redirect()->back()->with([
                    'flash_level' => 'success',
                    'flash_message' => sprintf('Học viên: %s đã được cập nhật', $customer['fullname']),
                    'objectData' => $customer,
                    'viewRender' => 'customer::inc.item'
                ]);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function filter(FilterCustomerBySeachableRequest $request)
    {
        $transporter = $request->toTransporter();
        $customers = Apiato::call('Customer@FilterCustomerBySeachableAction', [
            $transporter,
            [],
            [
                'id',
                'fullname',
                'email',
                'phone'
            ]
        ]);
        return $this->sendResponse($customers);
    }

    public function importExcel(Request $request, Excel $excel, CustomersImport $import){
        if($request->hasFile('file')) {
            $excelData = $excel->toArray($import, request()->file('file'));
            if (!empty($excelData) && is_array($excelData)){
                foreach ($excelData as $arr_item){
                    if (!empty($arr_item)){
                        $send_mail = [];
                        $update = [];
                        $insert = [];

                        $arr_email = array_column($arr_item, 'email');
                        $customer_isset = app(FindCustomerByEmailAction::class)->skipCache(true)->run($arr_email);
                        foreach ($arr_item as $item){
                            if (!empty($customer_isset) && isset($customer_isset[$item['email']])){
                                $update[$customer_isset[$item['email']]['id']] = [
                                    'fullname' => $item['ho_va_ten'],
                                    'phone' => $item['so_dien_thoai'],
                                ];
                            }else{
                                $password = $this->randomPassword(8);
                                $insert[] = [
                                    'type' => 1,
                                    'email' => $item['email'],
                                    'fullname' => $item['ho_va_ten'],
                                    'phone' => $item['so_dien_thoai'],
                                    'password' => bcrypt($password)
                                ];
                                $send_mail [] = [
                                    'email' => $item['email'],
                                    'fullname' => $item['ho_va_ten'],
                                    'password' => $password,
                                    'status' => 2
                                ];
                            }
                        }
                        $import = app(ImportCustomerAction::class)->skipCache(true)->run($insert, $update);
                        if (!$import){
                            ImportCustomerSuccessEvent::dispatch($send_mail);
                        }
                    }
                }
                return FunctionLib::ajaxRespond(true, 'Success', ['title' => 'Thành công', 'mess' => 'Import thành công']);
            }
            return FunctionLib::ajaxRespond(false, 'Error', ['title' => 'Thất bại', 'mess' => 'Không tìm thấy dữ liệu']);
        }
        return FunctionLib::ajaxRespond(false, 'Error', ['title' => 'Thất bại', 'mess' =>  'File không đúng định dạng']);
    }

    private function randomPassword($length = 10){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#%^&.*';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function exportExcel(Request $request, Excel $excel){
        return $excel->download(new ProductsExport($request), 'Export Sản phẩm '.FunctionLib::dateFormat(time(), '\( \l\ú\c H\hi  d-m-Y \)').'.xlsx');

    }

    public function updateSomeStatus(UpdateSomeStatusRequest $request) {
        //send mail
        $customer = app(FindCustomerByIdAction::class)->skipCache(true)->run($request->id, [], []);
        if ($request->status == 1){
            LockAccountEvent::dispatch($customer);
        }elseif($request->status == 2){
            UnLockAccountEvent::dispatch($customer);
        }
        $result = Apiato::call('Customer@UpdateSomeStatusAction', [
            $request->field,
            $request->id,
            $request->status,
        ]);


        if($result) {
            return $this->sendResponse($result,'Success');
        }

        return $this->sendError('Không update được dữ liệu!');
    }
} // End class
