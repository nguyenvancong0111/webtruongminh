<?php

// User
Route::group([
        'prefix' => 'customers',
    ],
    function () use ($router) {
        $router->get('/', [
            'as'   => 'admin.customers.index',
            'uses' => 'CustomerController@index',
            'middleware' => [
                'auth:admin'
            ]
        ]);

        $router->get('/create', [
            'as'   => 'admin.customers.create',
            'uses' => 'CustomerController@create',
            'middleware' => [
                'auth:admin'
            ]
        ]);


        $router->post('/store', [
            'as'   => 'admin.customers.store',
            'uses' => 'CustomerController@store',
            'middleware' => [
                'auth:admin'
            ]
        ]);

        $router->delete('/{id}', [
            'as'   => 'admin.customers.delete',
            'uses' => 'CustomerController@delete',
            'middleware' => [
                'auth:admin'
            ]
        ]);

        $router->get('/{id}/edit', [
            'as'   => 'admin.customers.edit',
            'uses' => 'CustomerController@edit',
            'middleware' => [
                'auth:admin'
            ]
        ]);

        $router->put('/{id}', [
            'as'   => 'admin.customers.update',
            'uses' => 'CustomerController@update',
            'middleware' => [
                'auth:admin'
            ]
        ]);

        $router->post('/render-item', [
            'as'   => 'admin.customers.renderItem',
            'uses' => 'CustomerController@renderItem',
            'middleware' => [
                'cors'
            ]
        ]);

        $router->get('/filter', [
            'as' => 'admin.customers.filter',
            'uses' => 'CustomerController@filter',
            'middleware' => [
                'auth:admin'
            ]
        ]);
        $router->post('/import-excel', [
            'as' => 'admin.customers.import',
            'uses' => 'CustomerController@importExcel'
        ]);
        $router->any('/export-excel', [
            'as' => 'admin.customers.export',
            'uses' => 'CustomerController@exportExcel'
        ]);
        $router->get('/run_cron_sendmail', [
            'as' => 'web_customer_run_cron_sendmail',
            'uses'  => function(){
                \Artisan::call('customer_send_mailcron');
                echo 'Done Cron';
            },
            'middleware' => [
                'auth:admin',
            ],
        ]);
        $router->post('/status/{field}', [
            'as'   => 'admin_customer_change_status',
            'uses' => 'CustomerController@updateSomeStatus',
        ]);
    });

// Customer groups
/*Route::group([
        'prefix' => 'customers-groups',
    ], function ($router){
        $router->get('/create', [
            'as' => 'admin.customers-groups.create',
            'uses' => 'CustomerGroupController@create',
            'middleware' => [
                'auth:admin'
            ]
        ]);

        $router->get('/', [
            'as' => 'admin.customers-groups.index',
            'uses' => 'CustomerGroupController@index',
            'middleware' => [
                'auth:admin'
            ]
        ]);

        $router->post('/store', [
            'as' => 'admin.customers-groups.store',
            'uses' => 'CustomerGroupController@store',
            'middleware' => [
                'auth:admin'
            ]
        ]);

        $router->get('/{id}/edit', [
            'as' => 'admin.customers-groups.edit',
            'uses' => 'CustomerGroupController@edit',
            'middleware' => [
                'auth:admin'
            ]
        ]);

        $router->delete('/{id}', [
            'as' => 'admin.customers-groups.delete',
            'uses' => 'CustomerGroupController@delete',
            'middleware' => [
                'auth:admin'
            ]
        ]);

        $router->put('/{id}', [
            'as' => 'admin.customers-groups.update',
            'uses' => 'CustomerGroupController@update',
            'middleware' => [
                'auth:admin'
            ]
        ]);
    });*/




