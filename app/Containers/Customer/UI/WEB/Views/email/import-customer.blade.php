<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email Template</title>
    <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@400;500;600&display=swap" rel="stylesheet">
    <style>
        @charset "UTF-8";

        .main-wrap {
            background: #F5F5F5;
            font-family: 'Exo 2', sans-serif;
            font-weight: normal;
            font-size: 14px;
            color: #333;
            max-width: 600px;
            margin-left: auto;
            margin-right: auto;
            padding-bottom: 0px;
        }

        *,
        *::before,
        *::after {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        .head {
            text-align: center;
            padding-top: 66px;
            padding-bottom: 36px;
        }

        .body {
            background-color: #fff;
            padding: 25px 30px;
        }

        .row{
            display: flex;
        }

        @media (max-width: 500px) {
            .row {
                -webkit-box-orient: vertical;
                -webkit-box-direction: normal;
                -ms-flex-direction: column;
                flex-direction: column;
            }

            .footer-left {
                -webkit-box-flex: 0;
                -ms-flex: 0 0 100%;
                flex: 0 0 100%;
                max-width: 100%;
            }

            .footer-right {
                -webkit-box-flex: 0;
                -ms-flex: 0 0 100%;
                flex: 0 0 100%;
                max-width: 100%;
                padding-left: 0;
                margin-top: 20px;
            }

            .footer-right::before {
                display: none;
            }
        }

        .footer{
            padding: 20px 20px 30px;
            margin-top: 30px;
        }

        .footer-left {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .footer-right {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .title-ft {
            font-weight: 500;
            font-size: 20px;
            line-height: 25px;
            color: #FFFFFF;
            margin: 0;
            margin-bottom: 25px;
        }

        .footer-left a {
            display: block;
            color: #fff;
            text-decoration: none;
            margin-bottom: 10px;
            font-size: 14px;
        }

        .footer-left a img {
            margin-right: 15px;
            vertical-align: -4px;
        }
        .footer-right{
            padding-left: 28px;
            position: relative;
        }
        .footer-right .footer-right-title {
            color: #CCCCCC;
            font-weight: 500;
            font-size: 14px;
            margin: 0;
            margin-bottom: 17px;
        }
        .footer-right .social-link{
            text-decoration: none;
            margin-right: 16px;
        }
        .footer-right::before{
            content: '';
            position: absolute;
            height: 81px;
            border-left: 1px solid #FFFFFF;
            opacity: 0.2;
            left: 0;
            top: 0;
        }
        .ft-note{
            color: #888888;
            font-size: 12px;
            text-align: center;
        }
        .ft-note a{
            color: #888888;
            font-weight: bold;
        }
        .ft-note a:hover{
            color: #f00;
        }

        @media (max-width: 500px) {
            .row{
                -webkit-box-orient: vertical;
                -webkit-box-direction: normal;
                -ms-flex-direction: column;
                flex-direction: column;
            }
            .footer-left {
                -webkit-box-flex: 0;
                -ms-flex: 0 0 100%;
                flex: 0 0 100%;
                max-width: 100%;
            }

            .footer-right {
                -webkit-box-flex: 0;
                -ms-flex: 0 0 100%;
                flex: 0 0 100%;
                max-width: 100%;
                padding-left: 0;
                margin-top: 20px;
            }
            .footer-right::before{
                display: none;
            }
        }
    </style>
</head>
<body>
<div class="main-wrap">
    <div class="header" style="background-color: #fff; padding: 20px 10px 30px;">
        <a href="#">
            <img src="{{asset('template/images/logo-head.png')}}" style="width: 200px; max-width: 100%;margin: 0 auto;display: block;" alt="">
        </a>
    </div>
    <div class="main" style="padding: 0 20px;">
        <h3 style="font-weight: normal; font-size: 22px;margin-bottom: 30px;">Xin chào! <strong>{{$customer_name}}</strong></h3>
        <span style="display: block;font-size: 18px;margin-bottom: 9px;">Email của bạn đã được Admin <strong>{{env('APP_NAME')}}</strong> tạo tài khoản trên <strong><a href="{{env('APP_URL')}}" target="_blank">{{env('APP_URL')}}</a></strong>.</span>
        <span style="display: block;font-size: 18px;margin-bottom: 9px;">Bạn có thể sử dụng email của bạn để sự dụng dịch vụ của <strong>{{env('APP_NAME')}}</strong></span>
    </div>
    <div class="main" style="padding: 0 20px;">
        <span style="display: block;font-size: 18px;margin-bottom: 9px;">Mật khẩu  của bạn là:</span>
        <span style="color: #343E76;font-weight: 500;font-size: 50px;margin-bottom: 20px; display: inline-block;border: 1px solid #D5D9F3;border-radius: 8px;padding: 7px 21px; width: 100%; text-align: center">{{$password}}</span>
        <span class="note" style="display: block;">
            <span style="color: #CA1F27;">*</span> Lưu ý: Vui lòng không chia sẻ mã này cho người khác.
        </span>
    </div>

    <div class="footer" style="background-image: url('{{asset('template/images/bg-footer-email.jpeg')}}');">
        <h3 class="title-ft">
            Bạn cần hỗ trợ, tư vấn thêm?
        </h3>
        <div class="row">
            <div class="footer-left">
                <a href="tel:19001000" class="phone" style="font-size: 12px;">
                    <img src="{{assert('template/images/icon-phone-tpl_e.png')}}" alt="">
                    HOTLINE: <b style="font-size: 16px;">{{$settings['contact']['hotline']}}</b>
                </a>
                <a href="tel:0979173626" class="phone">
                    <img src="{{assert('template/images/icon-phone-tpl_e.png')}}" alt="">
                    Tel: <b style="font-weight: 500;">{{$settings['contact']['tel']}}</b>
                </a>
                <a href="mailto:contact@ub.net">
                    <img src="{{assert('template/images/icon-mail-tpl_e.png')}}" alt="">
                    {{$settings['contact']['email']}}
                </a>
            </div>
            <div class="footer-right">
                <h5 class="footer-right-title">
                    Kết nối với chúng tôi
                </h5>
                <a href="#" class="social-link">
                    <img src="{{asset('template/images/icon-facebook-tpl_e.png')}}" alt="">
                </a>
                <a href="#" class="social-link">
                    <img src="{{asset('template/images/icon-instagram-tpl_e.pn')}}g" alt="">
                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>