@extends('basecontainer::admin.layouts.default')

@section('right-breads')
    {!! $customers->appends($input)->links('order::inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-accent-primary">
                <div class="card-header d-flex py-0">
                    <ul class="nav nav-tabs nav-underline nav-underline-primary card-header-tabs"
                        style="border-bottom: unset;border-color:unset;">
                        <li class="nav-item">
                            <a class="nav-link {{ empty($input['group_id']) ? 'active' : '' }}"
                                href="{{ route('admin.customers.index') }}">
                                Tất cả
                            </a>
                        </li>


                        @include('customer::inc.filter')
                    </ul>
                </div>


                <table class="table table-hover table-bordered mb-0" id="tableCustomer">
                    <thead>
                        <tr>
                            <th>
                                ID
                                <a href="javascript:void(0)"
                                    onclick="return addQueryParams('sort[id]', {{ !(!empty($input['sort']['id']) ? 1 : '0') }})">
                                    <i class="fa fa-long-arrow-{{ empty($input['sort']['id']) ? 'down' : 'up' }}"
                                        aria-hidden="true"></i>
                                </a>
                            </th>
                            <th>
                                Tên
                                <a href="javascript:void(0)"
                                    onclick="return addQueryParams('sort[fullname]', {{ !(!empty($input['sort']['fullname']) ? 1 : '0') }})">
                                    <i class="fa fa-long-arrow-{{ empty($input['sort']['fullname']) ? 'down' : 'up' }}"
                                        aria-hidden="true"></i>
                                </a>
                            </th>
                            <th>
                                Email
                                <a href="javascript:void(0)"
                                    onclick="return addQueryParams('sort[email]', {{ !(!empty($input['sort']['email']) ? 1 : '0') }})">
                                    <i class="fa fa-long-arrow-{{ empty($input['sort']['email']) ? 'down' : 'up' }}"
                                        aria-hidden="true"></i>
                                </a>
                            </th>
                            <th>
                                SĐT
                                <a href="javascript:void(0)"
                                    onclick="return addQueryParams('sort[phone]', {{ !(!empty($input['sort']['phone']) ? 1 : '0') }})">
                                    <i class="fa fa-long-arrow-{{ empty($input['sort']['phone']) ? 'down' : 'up' }}"
                                        aria-hidden="true"></i>
                                </a>
                            </th>
                            <th>
                                Ngày tạo
                                <a href="javascript:void(0)"
                                    onclick="return addQueryParams('sort[created_at]', {{ !(!empty($input['sort']['created_at']) ? 1 : '0') }})">
                                    <i class="fa fa-long-arrow-{{ empty($input['sort']['created_at']) ? 'down' : 'up' }}"
                                        aria-hidden="true"></i>
                                </a>
                            </th>
                            <th>Chi tiết</th>
                        </tr>

                        @if (isset($input['is_filter']))
                            <tr class="table-info">
                                <td colspan="8">
                                    <center class="font-weight-bold text-info">
                                        <i class="fa fa-search"></i> Tìm thấy
                                        @if ($customers->total() > 0)
                                            {{ $customers->firstItem() }} - {{ $customers->lastItem() }} trong số
                                            {{ $customers->total() }} kết quả.
                                        @else
                                            0 kết quả.
                                        @endif
                                    </center>
                                </td>
                            </tr>
                        @endif
                    </thead>

                    <tbody>
                        @forelse ($customers as $customer)
                            @include('customer::inc.item', ['item' => $customer])
                        @empty
                            <tr class="table-warning">
                                <td colspan="8">
                                    Không có dữ liệu.
                                    @if (!empty($input))
                                        <a href="{{ route('admin.customers.index') }}">Hủy bộ lọc</a>
                                    @endif
                                </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
@push('js_bot_all')
    <script>
        $("#import_excel").change(function() {
            var files = $(this)[0].files;
            var fd = new FormData();
            fd.append('file', files[0]);
            fd.append('_token', ENV.token);

            $.ajax({
                type: 'POST',
                url: "{{route('admin.customers.import')}}",
                data: fd,
                contentType: false,
                processData: false,
                statusCode: {
                    422: function(response) {
                        var json = JSON.parse(response.responseText);
                        if (json) {

                            $.each(json.errors, function(key, val) {
                                Swal.fire(
                                    'Thất bại',
                                    val[0],
                                    'error'
                                )
                            });
                        }
                    },
                    200: function(response) {
                        if (response.error === 1) {
                            Swal.fire(
                                response.data.title,
                                response.data.mess,
                                'error'
                            )
                        } else {
                            Swal.fire(
                                response.data.title,
                                response.data.mess,
                                'success'
                            )
                            shop.reload();
                        }

                    }
                },
                beforeSend: function() {

                }
            }).done(function(json) {

            });
        });
        function exportExcel() {
            var result = get_query(false);
            var url = "{{route('admin.customers.export')}}"+'?'+result
            $.ajax({
                type: 'POST',
                url: url,
            }).done(function(json) {
                if (json.error == 1){
                    Swal.fire(
                        'Error!',
                        json.code,
                        'error'
                    )
                }else{
                    window.location.href = url;
                }
            });
        }
        function get_query(object_param = true){
            var url = document.location.href;

            if(object_param) {
                var qs = url.substring(url.indexOf('?') + 1).split('&');

                for(var i = 0, result = {}; i < qs.length; i++){
                    qs[i] = qs[i].split('=');
                    result[qs[i][0]] = decodeURIComponent(qs[i][1]);
                }
                return result;
            }else{
                return url.substring(url.indexOf('?') + 1)
            }
        }
    </script>
@endpush
