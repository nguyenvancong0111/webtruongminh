<?php

namespace App\Containers\Customer\Models;

use App\Containers\Course\Models\Course;
use App\Containers\Customer\Traits\TimeUseTrait;
use App\Containers\Order\Models\OrderItem;
use App\Ship\Parents\Models\Model;

class CustomerCourse extends Model
{
    use TimeUseTrait;
    protected $table = 'customer_course';
    protected $fillable = ['id', 'customer_id', 'course_id', 'active_code', 'is_active', 'start_time', 'end_time', 'available_time', 'percent_view'];
    public $timestamps = false;
    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'customercourse';

    public function course(){
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function order_item(){
        return $this->hasOne(OrderItem::class, 'id', 'order_item_id');
    }

} // End class
