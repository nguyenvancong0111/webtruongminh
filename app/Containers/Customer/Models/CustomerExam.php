<?php

namespace App\Containers\Customer\Models;

use App\Containers\Course\Models\Course;
use App\Containers\Customer\Traits\TimeUseTrait;
use App\Containers\Exam\Models\Exam;
use App\Ship\Parents\Models\Model;

class CustomerExam extends Model
{
    use TimeUseTrait;
    protected $table = 'customer_exam';
    protected $fillable = ['id', 'customer_id', 'course_id', 'exam_id', 'rework', 'times', 'start_time', 'end_time', 'handling_time', 'score', 'result', 'count_true'];
    public $timestamps = true;
    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    public function exam(){
        return $this->hasMany(Exam::class,'id','exam_id');
    }
    protected $resourceKey = 'customercourse';
} // End class
