<?php

namespace App\Containers\Customer\Providers;

use App\Containers\Customer\Events\ChangePasswordSuccessEvent;
use App\Containers\Customer\Events\ForgotPasswordEvent;
use App\Containers\Customer\Events\Handlers\ChangePasswordSuccessEmailHandler;
use App\Containers\Customer\Events\Handlers\ForgotPasswordEmailHandler;
use App\Containers\Customer\Events\Handlers\ImportCustomerSuccessEmailHandler;
use App\Containers\Customer\Events\Handlers\LockAccountEmailHandler;
use App\Containers\Customer\Events\Handlers\RegisterSuccessEmailHandler;
use App\Containers\Customer\Events\Handlers\UnLockAccountEmailHandler;
use App\Containers\Customer\Events\ImportCustomerSuccessEvent;
use App\Containers\Customer\Events\LockAccountEvent;
use App\Containers\Customer\Events\RegisterSuccessEvent;
use App\Containers\Customer\Events\UnLockAccountEvent;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        ForgotPasswordEvent::class => [
            ForgotPasswordEmailHandler::class
        ],
        RegisterSuccessEvent::class => [
            RegisterSuccessEmailHandler::class
        ],
        ChangePasswordSuccessEvent::class => [
            ChangePasswordSuccessEmailHandler::class
        ],
        LockAccountEvent::class => [
            LockAccountEmailHandler::class
        ],
        UnLockAccountEvent::class => [
            UnLockAccountEmailHandler::class
        ],

        ImportCustomerSuccessEvent::class => [
            ImportCustomerSuccessEmailHandler::class
        ]
    ];

    public function boot() {
        parent::boot();
    }
}
