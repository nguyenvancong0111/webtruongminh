<?php

namespace App\Containers\General\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class StockStatusRepository.
 */
class StockStatusRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'General';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'stock_status_id',
        'language_id',
        'name',
        'created_at',
        'status',
    ];
}
