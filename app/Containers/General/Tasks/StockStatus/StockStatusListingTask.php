<?php

namespace App\Containers\General\Tasks\StockStatus;

use App\Containers\General\Data\Repositories\StockStatusRepository;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class StocStatusListingTask.
 */
class StockStatusListingTask extends Task
{

    protected $repository;

    public function __construct(StockStatusRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *  Bắt buộc Conds phải đủ 3 tham số: field, operation, value cho điều kiện
     * @return  mixed
     */
    public function run($conds = [['status','>',-1]],$limit = 20, $defaultLanguage = null)
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;

        $this->repository->whereHas('language', function (Builder $q) use($language_id) {
            $q->where('language_id', $language_id);
        });

        if(!empty($conds)) {
            foreach($conds as $item) {
                $this->repository->pushCriteria(new ThisOperationThatCriteria($item[0],$item[2],$item[1]));
            }
        }

        $this->repository->orderBy('created_at','desc')->orderBy('stock_status_id', 'desc');

        if ($limit > 0) {
            return $this->repository->paginate($limit);
        }
        return $this->repository->all();
    }
}
