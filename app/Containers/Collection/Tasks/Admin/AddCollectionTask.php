<?php

namespace App\Containers\Collection\Tasks\Admin;

use App\Containers\Collection\Data\Repositories\CollectionRepository;
use App\Ship\Parents\Tasks\Task;

class AddCollectionTask extends Task
{
    protected $repository;

    public function __construct(CollectionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($collection_data, $collection_id)
    {
        $data = [
            'created' => time(),
        ];
        if (isset($collection_id) && !empty($collection_id)) {
            $data['id'] = $collection_id;
            $collection = $this->repository->where('id', $collection_id)->first();

            return $collection->id;
        } else {
            $collection = $this->repository->create($data);
        }

        return $collection->id;
    }
}
