<?php

namespace App\Containers\Collection\Tasks\Admin;

use App\Containers\Collection\Data\Repositories\CollectionImageRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class GetProductGalleryTask.
 */
class GetCollectionGalleryTask extends Task
{

    protected $repository;

    public function __construct(CollectionImageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($collection_id, $type = 'collection', $json = false)
    {
        $images = $this->repository->getModel()->where('type', $type)->where('object_id', $collection_id)->orderByRaw('id asc')->get();
        // $images = $this->repository->getModel()->where('type', $type)->where('object_id', $collection_id)->orderByRaw('sort desc,created desc')->get();
        $data = [];
        if(!$images->isEmpty()) {
            foreach ($images as $image) {
                $tmp = $image->toArray();
                $tmp['img'] = $image->image;
                $tmp['image_sm'] = $image->getImageUrl('hotel_small');
                $tmp['image_md'] = $image->getImageUrl('hotel_preview');
                $tmp['image'] = $image->getImageUrl('hotel_large');
                $tmp['image_hover'] = $images->image_hover;
                $tmp['image_org'] = $image->getImageUrl();
                array_push($data, $tmp);
            }
        }
        return $json ? json_encode($data) : $data;
    }
}
