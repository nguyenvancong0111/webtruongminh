<?php

namespace App\Containers\Collection\Tasks\Admin;

use App\Containers\Collection\Data\Repositories\CollectionDetailRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ByObjectIDCriteria;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Containers\Localization\Models\Language;

class GetCollectionPrdTask extends Task
{

    protected $repository;

    public function __construct(CollectionDetailRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($request){
        if(!empty($request->prd_id)){
          $this->repository->pushCriteria(new ByObjectIDCriteria($request->prd_id));
        }
        if( !empty($request->id)){
          $this->repository->pushCriteria(new ThisEqualThatCriteria('collection_id', $request->id));
        }
        $this->repository->pushCriteria(new WithCriteria(['product'=> function($query) use($request)
        {
          $query->select('id', 'price', 'type', 'status', 'image')->with(['all_desc' => function($q) use($request) {
              $q->activeLang(1);
              $q->where('name',  'LIKE', '%'.$request->inputTitle.'%');
        }]);
        }]));
        $result = $this->repository->get();
        $data=[];
        foreach ($result as $item){
            if(isset($item->product->all_desc[0]->name)){
              $data[$item->object_id]['id'] = $item->object_id;
              $data[$item->object_id]['description'] = $item->product->all_desc[0]->description;
              $data[$item->object_id]['short_description'] = $item->product->all_desc[0]->short_description;
              $data[$item->object_id]['image'] = $item->product->image;
              $data[$item->object_id]['imageUrl'] = $item->product->image_url;
              $data[$item->object_id]['name'] =$item->product->all_desc[0]->name;
              $data[$item->object_id]['price'] = $item->product->price;
            }else{
              unset($data[$item->object_id]);
            }
        }
        return $data;
    }
    
}
