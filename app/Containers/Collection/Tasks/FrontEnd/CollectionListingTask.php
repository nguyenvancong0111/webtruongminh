<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 16:49:08
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-20 16:50:58
 * @ Description: Happy Coding!
 */

namespace App\Containers\Collection\Tasks\FrontEnd;

use App\Containers\Collection\Data\Repositories\CollectionRepository;
use App\Containers\Collection\Enums\CollectionStatus;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;

class CollectionListingTask extends Task
{
    protected $repository;

    public function __construct(CollectionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $limit = 10, int $limitProduct = 6)
    {
        $language_id = $this->currentLang ? $this->currentLang->language_id : 1;

        $this->repository->with([
            'desc' => function ($query) use ($language_id) {
                    $query->activeLang($language_id);
            },
            'products' => function ($query) use ($language_id,$limitProduct) {
                $query->where('status',2)->orderBy('sort_order','asc')->orderBy('created_at','desc')->limit($limitProduct);
            },
            'products.desc' => function ($query) use ($language_id) {
                $query->activeLang($language_id);
              },
            'products.images' => function($query){
                    $query->orderBy('sort','desc')->limit(1);
              }
            ]);

        return $this->skipPagin ? ($limit == 0 ? $this->repository->all() : $this->repository->limit($limit)) : $this->repository->paginate($limit);
    }


    public function isActive(bool $bool = true): self
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('status', $bool ? CollectionStatus::ACTIVE : CollectionStatus::DE_ACTIVE));
        return $this;
    }

    public function inRandomOrder(): self
    {
        $this->repository->inRandomOrder();
        return $this;
    }
}
