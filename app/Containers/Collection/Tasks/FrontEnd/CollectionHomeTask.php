<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 16:49:08
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-20 16:50:58
 * @ Description: Happy Coding!
 */

namespace App\Containers\Collection\Tasks\FrontEnd;

use App\Containers\Collection\Data\Repositories\CollectionRepository;
use App\Ship\Parents\Tasks\Task;

class CollectionHomeTask extends Task
{
    protected $repository;

    public function __construct(CollectionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $limit = 10)
    {
        $language_id = $this->currentLang ? $this->currentLang->language_id : 1;

        $this->repository->with([
            'desc' => function ($query) use ($language_id) {
                    $query->select('id', 'collection_id', 'name', 'language_id', 'short_description', 'meta_title');
                    $query->activeLang($language_id);
            }]);
        return $this->skipPagin ? ($limit == 0 ? $this->repository->all() : $this->repository->limit($limit)) : $this->repository->paginate($limit);
    }
}
