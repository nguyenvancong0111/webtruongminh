<?php

namespace App\Containers\Collection\Actions\Admin;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class GetCollectionByIdAction extends Action
{
    public function run($collection_id, $with = ['all_desc'], $where = [], $selectFields = ['*'])
    {
        return  Apiato::call('Collection@Admin\GetCollectionByIdTask', [$collection_id], [
            ['with' => [$with]],
            ['where' => [$where]],
            ['selectFields' => [$selectFields]],
        ]);
    }
}
