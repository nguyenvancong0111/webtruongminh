<?php

namespace App\Containers\Collection\Actions\Admin;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteCollectionByIdAction extends Action
{
    public function run($id)
    {
      $this->clearCache();

      return Apiato::call('Collection@Admin\DeleteCollectionByIdTask', [$id]);
    }
}
