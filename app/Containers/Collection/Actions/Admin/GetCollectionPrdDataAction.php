<?php

namespace App\Containers\Collection\Actions\Admin;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetCollectionPrdDataAction extends Action
{
    public function run($request)
    {   $data=[];
        $collection = Apiato::call('Collection@Admin\GetCollectionPrdTask', [$request]);
        foreach ($collection as $key => $task){
            $collection[$key]['categories'] = Apiato::call('Category@GetCategoryByIdAction', [Apiato::call('Product@CateIdsDirectFromPrdIdAction', [$task['id']])]);
            if(isset($request->cate_id) && !empty($request->cate_id)){
                foreach ($collection[$key]['categories'] as $k => $v){
                    # code..
                    if($v->category_id == $request->cate_id){
                        $data[$key] = $collection[$key];
                    }
                } 
            }
        }
        if(isset($request->cate_id) && !empty($request->cate_id))
            return $data;
        return $collection;
    }
}
