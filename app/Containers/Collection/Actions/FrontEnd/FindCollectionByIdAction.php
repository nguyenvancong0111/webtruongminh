<?php

namespace App\Containers\Collection\Actions\FrontEnd;

use App\Containers\Collection\Tasks\Admin\GetCollectionByIdTask;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class FindCollectionByIdAction extends Action
{
    public function run($id, ?array $with, ?Language $currentLang, $conditions = [], $selectFields = ['*'])
    {
        return $this->remember(function () use ($id, $with, $currentLang, $conditions, $selectFields) {
            return app(GetCollectionByIdTask::class)->currentLang($currentLang)->where($conditions)->withDescLang()->run($id);
        }, null, [], 0, true);
    }
}
