<?php


namespace App\Containers\Collection\Actions\FrontEnd;

use App\Containers\Collection\Tasks\FrontEnd\CollectionHomeTask;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetCollectionHomeAction extends Action
{
    public function run(?array $filters = [], ?array $with, ?Language $currentLang, $skipPagination = false, $limit = 10, $where = [], $orderBy = ['sort_order' => 'ASC', 'id' => 'DESC'], $limitProduct = 6)
    {

        return $this->remember(function () use ($filters, $with, $currentLang, $skipPagination, $limit, $where, $orderBy, $limitProduct) {
            $collections = app(CollectionHomeTask::class)->currentLang($currentLang)->where($where)->with($with)->mustHaveDescByLang();

            if (!empty($orderBy) && is_array($orderBy)) {
                $collections = $collections->orderBy($orderBy);
            } elseif (!empty($orderBy) && is_string($orderBy) && $orderBy === 'inRandomOrder') {
                $collections = $collections->inRandomOrder();
            }

            $collections = $this->detectTaskCriteriaFunc($filters, $collections);

            if ($skipPagination) {
                $collections->skipPagin();
            }

            return $collections->run($limit);
        }, null, [], 0, $this->skipCache);
    }
}
