<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 17:09:00
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-01 11:29:58
 * @ Description: Happy Coding!
 */

namespace App\Containers\Collection\Actions\FrontEnd;

use App\Containers\Collection\Tasks\FrontEnd\GetProductByCollectionTask;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetProductByCollectionAction extends Action
{
    protected $externalWith = [];

    public function run(?array $filters = [], ?array $with, ?Language $currentLang, $skipPagination = false, $limit = 10)
    {
        return $this->remember(function () use ($filters, $with, $currentLang, $skipPagination, $limit) {
            $products = app(GetProductByCollectionTask::class)->externalWith($this->externalWith)->with($with)->currentLang($currentLang);

            $products = $this->detectTaskCriteriaFunc($filters, $products);

            if ($skipPagination) {
                $products->skipPagin();
            }

            return $products->run($limit)->pluck('product');
        }, null, [], 0, $this->skipCache);
    }

    public function descSpecialOfffer(?Language $currentLang): self
    {
        $languageId = $currentLang ? $currentLang->language_id : 1;

        $this->externalWith = array_merge($this->externalWith, [
            'product.specialOffers.desc' => function ($query) use ($languageId) {
                $query->select('id', 'special_offer_id', 'name');
                $query->whereHas('language', function ($q) use ($languageId) {
                    $q->where('language_id', $languageId);
                });
            }
        ]);

        return $this;
    }
}
