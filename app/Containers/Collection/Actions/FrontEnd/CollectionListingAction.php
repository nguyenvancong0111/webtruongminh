<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 17:09:00
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-20 16:54:27
 * @ Description: Happy Coding!
 */

namespace App\Containers\Collection\Actions\FrontEnd;

use App\Containers\Collection\Tasks\FrontEnd\CollectionListingTask;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class CollectionListingAction extends Action
{
    public function run(?array $filters = [], ?array $with, ?Language $currentLang, $skipPagination = false, $limit = 10, $where = [], $orderBy = ['sort_order' => 'ASC', 'id' => 'DESC'], $limitProduct = 6)
    {
        return $this->remember(function () use ($filters, $with, $currentLang, $skipPagination, $limit, $where, $orderBy, $limitProduct) {
            $collections = app(CollectionListingTask::class)->currentLang($currentLang)->where($where)->mustHaveDescByLang();

            if (!empty($orderBy) && is_array($orderBy)) {
                $collections = $collections->orderBy($orderBy);
            } elseif (!empty($orderBy) && is_string($orderBy) && $orderBy === 'inRandomOrder') {
                $collections = $collections->inRandomOrder();
            }

            $collections = $this->detectTaskCriteriaFunc($filters, $collections);

            if ($skipPagination) {
                $collections->skipPagin();
            }

            return $collections->run($limit,$limitProduct);
        }, null, [], 0, $this->skipCache);
    }
}
