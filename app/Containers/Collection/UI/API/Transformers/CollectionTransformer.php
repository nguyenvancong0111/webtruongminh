<?php

namespace App\Containers\Collection\UI\API\Transformers;

use App\Containers\Collection\Models\Collection;
use App\Ship\Parents\Transformers\Transformer;

class CollectionTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Collection $entity
     *
     * @return array
     */
    public function transform(Collection $entity)
    {
        $response = [
            'object' => 'Collection',
            'id' => $entity->getHashedKey(),
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
