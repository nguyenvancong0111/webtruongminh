<?php

namespace App\Containers\Collection\UI\WEB\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class GetAllCollectionsRequest.
 */
class ajaxCollectionSaveRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\Collection\Data\Transporters\GetAllCollectionsTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles'       => 'admin',
        'permissions' => 'collection-manager',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'content.1.name'           => 'required|max:255',
            'content.2.name'           => 'required|max:255',
            'content.3.name'           => 'required|max:255',
            'file' => 'bail|mimes:jpeg,jpg,png,gif',
        ];
    }

     /**
     * Custom message for validation
     *
     * @return array
     */

    public function messages()
    {
        return [
            'content.1.name.required' => 'Tên Tiếng Việt không được bỏ trống',
            'content.2.name.required' => 'Tên Tiếng Anh không được bỏ trống',
            'content.3.name.required' => 'Tên Tiếng Trung không được bỏ trống',
            'content.1.name.max' => 'Tên Tiếng Việt tối đa 255 ký tự',
            'content.2.name.max' => 'Tên Tiếng Anh tối đa 255 ký tự',
            'content.3.name.max' => 'Tên Tiếng Trung tối đa 255 ký tự',
            'file.mimes' => 'Ảnh không đúng định dạng (jpeg, jpg, png, gif)',
        ];
    }


    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
