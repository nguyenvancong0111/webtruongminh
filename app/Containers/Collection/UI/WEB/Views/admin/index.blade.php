@extends('basecontainer::admin.layouts.default')

@section('content')
    <div id="collectionApp"></div>
@endsection

@push('js_bot_all')
    {!! FunctionLib::addMedia('admin/js/library/tinymce/tinymce.min.js') !!}

    <script>
        window.object_id = '{{ @$input['id'] }}';
    </script>
    <script src="{{ asset('admin/GroupJS/collection/collection.js') }}?v={{time()}}"></script>
@endpush
