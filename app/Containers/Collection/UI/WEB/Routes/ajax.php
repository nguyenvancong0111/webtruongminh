<?php

use Illuminate\Support\Facades\Route;

Route::namespace('\App\Containers\Collection\UI\WEB\Controllers\Admin')->prefix('admin/ajax')->group(function () {
  Route::prefix('collection')->group(function () {
    Route::any('/controller/collectionList', [
      'as' => 'admin.collection.collectionList',
      'uses' => 'Controller@ajaxCollectionList',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/{id}/collectionDelete',[
      'as' => 'admin.collection.controller.collectionDelete',
      'uses' => 'Controller@ajaxDeleteCollection',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::get('/{collection_id}-{lang_id}/controller/', [
      'as' => 'admin.collection.controller.collectionByID',
      'uses' => 'Controller@ajaxCollectionByID',
       'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::post('controller/imgUpload', [
      'as' => 'admin.collection.controller.imgUpload',
      'uses' => 'Controller@ajaxImageUpload',
       'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::post('controller/collectionSave', [
      'as' => 'admin.collection.controller.collectionSave',
      'uses' => 'Controller@ajaxCollectionSave',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::get('/{id}-{img_id}/controller/itemImgDel',[
      'as' => 'admin.collection.controller.itemImgDel',
      'uses' => 'Controller@ajaxItemImgDel',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::get('/{id}/controller/imgData', [
      'as' => 'admin.collection.controller.imgData',
      'uses' => 'Controller@ajaxImgData',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::get('/{id}/controller/dataCollection', [
      'as' => 'admin.collection.controller.dataCollection',
      'uses' => 'Controller@ajaxDataCollection',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::POST('/controller/searchProduct', [
      'as' => 'admin.collection.searchProduct',
      'uses' => 'Controller@ajaxSearchProduct',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/collectionByID/{id}',[
      'as' => 'admin.collection.controller.collectionByID',
      'uses' => 'Controller@ajaxCollectionID',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::post('/controller/itemImgUpdate',[
      'as' => 'admin.collection.controller.itemImgUpdate',
      'uses' => 'Controller@ajaxItemImgUpdate',
       'middleware' => [
        'auth:admin',
      ]
    ]);
      Route::GET('/controller/getCollection', [
          'as' => 'admin.collection.getCollection',
          'uses' => 'Controller@ajaxGetCollection',
          'middleware' => [
              'auth:admin',
          ]
      ]);

      Route::get('/{product_id}/controller/collectionData', [
          'as' => 'admin.collection.controller.collectionData',
          'uses' => 'Controller@ajaxCollectionDataByID',
          'middleware' => [
              'auth:admin',
          ],
      ]);
  });
});
