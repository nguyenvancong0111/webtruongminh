<?php

namespace App\Containers\Collection\Models;
use App\Containers\Product\Models\Product;
use App\Ship\Parents\Models\Model;

class CollectionDetail extends Model
{
    const KEY = 'collection_details';
    protected $fillable = [ 'collection_id','object_id'
    ];
    public $timestamps = false;

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [

    ];
    public function product()
    {
        return $this->hasOne(Product::class,'id','object_id');
    }
    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
}
