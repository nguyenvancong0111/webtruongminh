<?php

namespace App\Containers\Collection\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Models\ProductCategory;
use App\Containers\Tags\Enums\TagType;
use App\Containers\Tags\Models\TagDetails;
use App\Containers\Tags\Models\Tags;
use App\Ship\Parents\Models\Model;
use Illuminate\Support\Str;

class Collection extends Model
{
    // use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;
    const KEY = 'collection';

    protected $fillable = [
        'title', 'created', 'image', 'is_good_price', 'color', 'status', 'category_ids', 'sort_order'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $resourceKey = 'collections';

    public function getById($id)
    {
        return $this->with('all_desc')->find($id);
    }

    public function images()
    {
        return $this->hasMany(CollectionImage::class, 'object_id', 'id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tags::class, TagDetails::getTableName(), 'object_id', 'tag_id')->where('type', TagType::COLLECTION);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, CollectionDetail::getTableName(), 'collection_id', 'object_id');
    }

    public function productsDetail()
    {
        return $this->belongsToMany(Product::class, CollectionDetail::getTableName(), 'collection_id', 'object_id')
            ->where('status',2)->orderBy('sort_order','asc')->orderBy('created_at','desc')->limit(12);
    }

    public function desc()
    {
        return $this->hasOne(CollectionDesc::class, 'collection_id', 'id') ;
    }

    public function all_desc()
    {
        return $this->hasMany(CollectionDesc::class, 'collection_id', 'id');
    }

    public function getImageUrl($size = 'social')
    {
        return ImageURL::getImageUrl($this->image, 'collection', $size);
    }

    /*routes*/
    public function getRouteDetailAttribute()
    {
        return $this->routeCollectionDetail();
    }

    public function routeCollectionDetail()
    {
        if (!empty($this->desc->name)) {
            if($this->desc->language_id === 3){
               $slug =  Str::slug($this->desc->name, '-', 'cn');
            }else{
                $slug =  Str::slug($this->desc->name);
            }
           return route('web.collection.detail', [$slug, $this->id]);
        }
        return null;
    }


}
