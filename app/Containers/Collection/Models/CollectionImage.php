<?php
/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-01-31 18:03:27
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-04-15 19:54:46
 * @ Description: Happy Coding!
 */

namespace App\Containers\Collection\Models;

use Apiato\Core\Foundation\Facades\ImageURL;
use App\Ship\Parents\Models\Model;

class CollectionImage extends Model
{
    protected $table = 'collection_img';

    public $timestamps = false;

    public static $step = 1;

    protected $fillable = ['image', 'object_id', 'type', 'sort', 'created'];

    protected $appends = ['img', 'imglarge'];

    public function collection()
    {
        return $this->hasOne(Collection::class, 'id', 'objet_id')->where('type', 'collection');
    }

    public function getImageUrl($size = 'original')
    {
        return ImageURL::getImageUrl($this->image, Collection::KEY, $size);
    }

    public static function getSortInsert($lang = 'vi')
    {
        return self::max('sort') + self::$step;
    }

    public function getImgAttribute()
    {
        return $this->getImageUrl();
    }

    public function getImglargeAttribute()
    {
        return $this->getImageUrl('social');
    }
}