<?php

namespace App\Containers\Collection\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class CollectionRepository
 */
class CollectionDescRepository extends Repository
{

    /**
     * @var array
     */
    protected $container = 'collection';

    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
