<?php

namespace App\Containers\Course\Models;

use App\Ship\Parents\Models\Model;

class CourseLessonAttached extends Model
{
    protected $table = 'course_lesson_attached';

    protected $fillable = [
        'lesson_id',
        'file_id',
    ];


}

