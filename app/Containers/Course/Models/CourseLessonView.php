<?php

namespace App\Containers\Course\Models;

use App\Containers\Exam\Models\Exam;
use App\Ship\Parents\Models\Model;

class CourseLessonView extends Model
{
    protected $table = 'course_lesson_view';

    protected $fillable = [
        'customer_id',
        'course_id',
        'lesson_id',
        'percent',
        'time_view'
    ];
}

