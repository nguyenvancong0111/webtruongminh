<?php

namespace App\Containers\Course\Models;

use Apiato\Core\Foundation\Facades\ImageURL;
use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Category\Enums\CategoryStatus;
use App\Containers\Category\Models\Category;
use App\Containers\Comment\Models\Comment;
use App\Containers\Course\Traits\CourseFormTrait;
use App\Containers\Lecturers\Models\Lecturers;
use App\Containers\Option\Models\Option;
use App\Containers\Option\Models\OptionValue;
use App\Ship\Parents\Models\Model;
use Illuminate\Support\Str;

class Course extends Model
{
    use CourseFormTrait;
    protected $table = 'course';

    protected $appends = ['format_date'];

    protected $fillable = [
        'option_gr',
        'code',
        'price',
        'global_price',
        'image',
        'image_detail',
        'icon',
        'icon_hover',
        'views',
        'is_hot',
        'is_home',
        'lecturers',
        'auth_type',
        'author',
        'author_updated',
        'type',
        'form',
        'cate',
        'student',
        'video_intro',
        'video_intro_dir',
        'video_intro_youtube',
        'sort_order',
        'status',
        'published',
        'created_at',
        'updated_at',
    ];

    public function category()
    {
        return $this->hasOne(Category::class, 'category_id', 'cate');
    }

    public function getImageUrl($size = 'original')
    {
        return ImageURL::getImageUrl($this->image, 'course', $size);
    }

    public function desc()
    {
        return $this->hasOne(CourseDesc::class, 'course_id', 'id');
    }

    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', CourseDesc::class, 'course_id', 'id');
    }

    public function getFormatDateAttribute()
    {
        return ($this->created_at) ? $this->created_at->format('d.m.Y') : null;
    }

    public function course_group(){
        return $this->hasMany(CourseLessonGroup::class, 'course_id', 'id');
    }

    public function count_item(){
        return $this->hasMany(CourseLessonItemGroup::class, 'course_id', 'id');
    }
    public function count_courseInCombo(){
        return $this->hasMany(CourseCombo::class, 'combo_id', 'id');
    }

    public function lecturers_course(){
        return $this->hasOne(Lecturers::class, 'id', 'lecturers');
    }
    public function comment(){
        return $this->hasMany(Comment::class, 'object_id', 'id')->where('status', '=',2);
    }

    public function combo(){
        return $this->belongsToMany(Course::class, CourseCombo::getTableName(), 'course_id', 'combo_id')->where('course.status', '=', 2);
    }

    public function course_option(){
        return $this->hasMany(CourseOption::class, 'course_id', 'id');
    }

    public function option(){
        return $this->hasOne(Option::class, 'id', 'option_gr');
    }


    public function link()
    {
        $slug = !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name);
        if (!empty($slug)) {
            return route('web.course.detail', ['course_slug' => $slug, 'course_id' => $this->id]);
        } else {
            return 'javascript:;';
        }
    }
    public function linkStudy()
    {
        $slug = !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name);
        if (!empty($slug)) {
            return route('web.course.studying', ['c_slug' => $slug, 'c_id' => $this->id]);
        } else {
            return 'javascript:;';
        }
    }
    public function linkStudying()
    {
        $slug = !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name);
        if (!empty($slug)) {
            return route('web.course.studying', ['c_slug' => $slug, 'c_id' => $this->id]);
        } else {
            return 'javascript:;';
        }
    }
    public function getAvgRatingCourse(){
        $total = $this->comment()->count();
        if ($total > 0){
            $total_rating = $this->comment()->sum('rating');
            $avg = round($total_rating / $total, 1);
            return (int)$avg;
        }
        return 0;
   }
}

