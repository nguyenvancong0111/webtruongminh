<?php

namespace App\Containers\Course\Models;

use Apiato\Core\Foundation\MyStorage;
use App\Ship\Parents\Models\Model;
use Illuminate\Support\Arr;

class CourseLessonFile extends Model
{
    protected $table = 'course_lesson_item_file';

    protected $fillable = [
        'auth_type',
        'auth_id',
        'type',
        'mine_type',
        'dir',
        'name',
        'file_format',
        'duration',
        'playtime_string',
        'size',
        'created_at',
        'updated_at'
    ];


    public static function video_stream($path, $object, $user){
        return [
            'type' => 'application/x-mpegURL',
            'rel' => '720',
            'label' => 'HD',
            'title' => 'HD',
            'src' => url('/') . $path . '/master.m3u8',
        ];
    }

    public function get_stream_link($options = []){
        $options['id'] = $this->id;
        $options['secure'] = true;
        $options['version'] = 'hd';

        $streams = [];
        $streams[] = self::get_video_stream_link($this->dir, $options);

        /*if($this->video_hd_path != ''){
            $options['version'] = 'hd';
            $streams[] = self::get_video_stream_link($this->dir, $options);
        }
        if($this->video_sd_path != ''){
            $options['version'] = 'sd';
            $streams[] = self::get_video_stream_link($this->dir, $options);
        }
        if(count($streams) == 0){
            $streams[] = self::get_video_stream_link($this->dir, $options);
        }*/
        return $streams;
    }

    public function get_download_link($options = []){
        if(!$this->exists) return "";
        $options['secure'] = true;
        $options['document_id'] = $this->id;
        return MyStorage::get_document_download_link('local', $this->dir, $options);
    }

    public static function get_video_stream_link($path, $options){
        $version = Arr::get($options, 'version', 'origin');

        if(app()->environment() == 'local'){
            $storage_stream_config = config('flysystem.connections.'.env('DEFAULT_DISK'));
            if(!Arr::get($storage_stream_config, 'video_streaming_support', false)){
                return [];
            }else{
                try{
                    $closure = Arr::get($storage_stream_config, 'get_video_stream_closure');
//                    $stream = call_user_func($closure,  $path, null, null );
                    $stream = MyStorage::video_stream('/'.$path);
                    if($version == 'hd'){
                        $stream['rel'] = '720';
                        $stream['label'] = 'HD';
                        $stream['title'] = 'HD';
                    }elseif($version == 'sd'){
                        $stream['rel'] = '360';
                        $stream['label'] = 'SD';
                        $stream['title'] = 'SD';
                    }else{
                        $stream['rel'] = '1000';
                        $stream['label'] = 'Origin';
                        $stream['title'] = 'Origin';
                    }

                    return $stream;
                }catch (\Exception $ex){
                    return [];
                }

            }
        }
        return "";
    }
}

