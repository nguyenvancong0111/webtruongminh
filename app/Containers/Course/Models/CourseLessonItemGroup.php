<?php

namespace App\Containers\Course\Models;

use App\Containers\Exam\Models\Exam;
use App\Ship\Parents\Models\Model;

class CourseLessonItemGroup extends Model
{
    protected $table = 'course_lesson_item_group';

    protected $fillable = [
        'group_id',
        'course_id',
        'title',
        'type',
        'description',
        'file',
        'quiz',
        'preview',
        'sort',
        'status',
        'created_at',
        'updated_at',
        'attached',
        'session'
    ];

    public function attached_file()
    {
        return $this->belongsToMany(CourseLessonFile::class, CourseLessonAttached::getTableName(), 'lesson_id', 'file_id');
    }

    public function file_primary(){
        return $this->hasOne(CourseLessonFile::class, 'id', 'file');
    }
    public function quiz_re(){
        return $this->hasOne(Exam::class, 'id', 'quiz');
    }


}

