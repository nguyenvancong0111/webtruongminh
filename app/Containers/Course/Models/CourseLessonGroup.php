<?php

namespace App\Containers\Course\Models;

use App\Ship\Parents\Models\Model;

class CourseLessonGroup extends Model
{
    protected $table = 'course_lesson_group';

    protected $fillable = [
        'course_id',
        'title',
        'sort',
        'status',
        'created_at',
        'updated_at'
    ];

    public function course_item_group(){
        return $this->hasMany(CourseLessonItemGroup::class, 'group_id', 'id');
    }
}

