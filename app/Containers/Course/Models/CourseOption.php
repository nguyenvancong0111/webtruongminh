<?php

namespace App\Containers\Course\Models;

use App\Containers\Exam\Models\Exam;
use App\Containers\Option\Models\Option;
use App\Containers\Option\Models\OptionValue;
use App\Ship\Parents\Models\Model;

class CourseOption extends Model
{
    protected $table = 'course_option';

    protected $fillable = [
        'course_id',
        'option_id',
        'value',
        'price',
        'is_active',
    ];

    public $timestamps = true;

    public  function option(){
        return $this->hasOne(Option::class, 'id', 'option_id');
    }

    public function option_value(){
        return $this->hasOne(OptionValue::class, 'id', 'option_id');
    }
}


