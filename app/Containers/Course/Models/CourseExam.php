<?php

namespace App\Containers\Course\Models;

use App\Containers\Exam\Models\Exam;
use App\Ship\Parents\Models\Model;

class CourseExam extends Model
{
    protected $table = 'course_exam';

    protected $fillable = [
        'course_id',
        'exam_id'
    ];

    public function exam(){
        return $this->hasMany(Exam::class, 'id', 'exam');
    }
}

