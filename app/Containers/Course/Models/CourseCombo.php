<?php

namespace App\Containers\Course\Models;

use App\Containers\Exam\Models\Exam;
use App\Ship\Parents\Models\Model;

class CourseCombo extends Model
{
    protected $table = 'course_combo';

    protected $fillable = [
        'combo_id',
        'course_id',
    ];

    public function course(){
        return $this->hasMany(Course::class, 'id', 'combo_id');
    }
}

