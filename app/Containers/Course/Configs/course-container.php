<?php

return [
    'sort_by' => [
        'asc' => ['price' => 'ASC'],
        'desc' => ['price' => 'DESC'],
    ],
    'filters' => [
        'free' => ['price' => 0]
    ]

];
