<?php

namespace App\Containers\Course\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class CourseType extends BaseEnum
{
    const TEXT = [
        0 => 'Khóa học',
        1 => 'Combo'
    ];

}
