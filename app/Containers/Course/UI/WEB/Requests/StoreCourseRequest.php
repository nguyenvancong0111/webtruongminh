<?php

namespace App\Containers\Course\UI\WEB\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class CreateCourseRequest.
 *
 */
class StoreCourseRequest extends Request
{

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles'       => 'admin',
        'permissions' => 'create-course',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            // 'id'           => 'required|exists:course,id',
//            'course_category' => 'required'
            'course_description*name' => 'required',
            'image' => 'bail|mimes:jpeg,jpg,png,gif',
        ];

    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'course_description.1.name' => 'Tên Tiếng Việt không được bỏ trống',
            'image.mimes' => 'Ảnh đại diện không đúng định dạng (jpeg, jpg, png, gif)',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
