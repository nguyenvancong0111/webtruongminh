<?php

namespace App\Containers\Course\UI\WEB\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class UpdateCourseRequest.
 *
 */
class UpdateCourseRequest extends Request
{

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles'       => 'admin',
        'permissions' => 'edit-course',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'id'           => 'required|exists:course,id',
            'course_description.*.name' => 'required',
            'image' => 'bail|nullable|mimes:jpeg,jpg,png,gif',
//            'course_category' => 'required'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required'  => 'ID không tồn tại',
            'id.exists'  => 'ID không tồn tại',
//            'course_category.required' => 'Danh mục không được bỏ trống',
            'course_description.1.name.required' => 'Tên không được bỏ trống',
            'course_description.2.name.required' => 'Tên Tiếng Anh không được bỏ trống',
            'course_description.3.name.required' => 'Tên Tiếng Trung không được bỏ trống',
            'image.mimes' => 'Ảnh đại diện không đúng định dạng (jpeg, jpg, png, gif)',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
