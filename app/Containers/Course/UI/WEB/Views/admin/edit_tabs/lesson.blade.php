<div class="tab-pane " id="lesson">
    <div class="row">
        <div class="col-sm-12">
            <div v-for="(group, key_group) in data.val">
                <input type="hidden" v-bind:name="'lesson[group]['+key_group+'][id]'" v-model="group.id" class="form-control js-input" >
                <div class="row mt-3">
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" v-bind:name="'lesson[group]['+key_group+'][title]'" v-model="group.title_group" class="form-control js-input" data-noti="Nhóm bài học " placeholder="e.g: Phần 1: Giới thiệu">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <input type="text" v-bind:name="'lesson[group]['+key_group+'][sort]'" v-model="group.sort" class="form-control js-input" data-noti="Sắp xếp từ bé tới lớn" placeholder="Sort" >
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="input-group-append">
                            <span class="btn btn-danger js-input" data-noti="Xóa nhóm bài học, toàn bộ bài học sẽ bị xóa" @click="trashGroup(key_group)" style="cursor: pointer"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
                <div class="row" v-for="(item, key_item) in group.props">
                    <input type="hidden" v-bind:name="'lesson[item]['+key_group+']['+key_item+'][id]'" v-model="item.id"  class="form-control js-input">
                    <div class="col-sm-1 w-100 text-center">
                        <div class="checkbox ml-1 mb-1 form-check-inline radio-success js-input" data-noti="Tích chọn sẽ cho phép học thử bài học">
                            <input type="checkbox" :id="'option_view_'+key_group+'_'+key_item" v-bind:name="'lesson[item]['+key_group+']['+key_item+'][preview]'" v-model="item.preview" value="1" :checked="item.preview == 1">
                            <label :for="'option_view_'+key_group+'_'+key_item" ></label>
                        </div>
                    </div>
                    <div class="col-sm-4 mb-2" >
                        <div class="input-group">
                            <input type="text" v-bind:name="'lesson[item]['+key_group+']['+key_item+'][title]'" v-model="item.title"  class="form-control js-input" data-noti="Tiêu đề bài học" placeholder="Tiêu đề bài học">
                        </div>
                    </div>
                    <div class="col-sm-2 mb-2" >
                        <div class="input-group">
                            <select v-bind:name="'lesson[item]['+key_group+']['+key_item+'][type]'" class="form-control" @change="changeType($event, item, 'type')">
                                <option value="">Định dạng bài học</option>
                                <option v-for="(i, index) in data.type" :value="index" v-html="i" :selected="item.type === index"></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2 mb-2">
                        <div class="input-group" v-if="typeof item.type != 'undefined' && item.type">
                            <span  title="Upload File"  style="cursor: pointer" class="pr-2" v-if="['video', 'audio', 'pdf'].includes(item.type)"  @click="loadModalUploadListFilePrimary($event, item.type, key_group, key_item)">
                                <label :for="'upload-file-'+key_group+'-'+key_item" class="m-1" style="cursor: pointer" title="Upload">
                                    <i class="fa fa-cloud-upload fa-2x text-success" aria-hidden="true"></i>
                                    <input type="text" style="display: none" v-bind:name="'lesson[item]['+key_group+']['+key_item+'][file]'" :id="item.type+'-file-'+key_group+'-'+key_item" :value="item.file_id">
                                </label>
                            </span>
                            <span  title="Nội dung"  style="cursor: pointer" class="pr-2" v-if="['text', 'video', 'audio', 'pdf' ].includes(item.type)"  @click="loadModalText($event, item.type, key_group, key_item)">
                                <label style="cursor: pointer" :for="'text-'+key_group+'-'+key_item" class="m-1">
                                    <i class="fa fa-pencil-square-o fa-2x text-info" aria-hidden="true"></i>
                                    <input type="text" style="display: none" v-bind:name="'lesson[item]['+key_group+']['+key_item+'][description]'" :id="item.type+'-text-'+key_group+'-'+key_item" :value="item.description">
                                </label>
                            </span>

                            <span title="File đính kèm" style="cursor: pointer" class="pr-2" v-if="['text', 'video', 'audio', 'pdf' ].includes(item.type)" @click="loadModalUploadListFile($event, item.type, key_group, key_item)" >
                                <label style="cursor: pointer" :for="'list-file-server-'+key_group+'-'+key_item" class="m-1">
                                    <i class="fa fa-paperclip fa-2x text-success"></i>
                                    <input type="text" style="display: none" v-bind:name="'lesson[item]['+key_group+']['+key_item+'][attached]'" :id="item.type+'-attached-'+key_group+'-'+key_item" :value="item.attached_str">
                                </label>
                            </span>
                            <span title="Bài Quiz" style="cursor: pointer" class="pr-2" v-if="['quiz'].includes(item.type)" @click="loadModalQuiz($event, item.type, key_group, key_item, item.attached, item.file)">
                                <label style="cursor: pointer" :for="'quiz-'+key_group+'-'+key_item" class="m-1">
                                    <i class="fa fa-link fa-2x text-success" aria-hidden="true"></i>
                                    <input type="text" style="display: none" v-bind:name="'lesson[item]['+key_group+']['+key_item+'][quiz]'" :id="item.type+'-quiz-'+key_group+'-'+key_item" :value="item.quiz_id">
                                </label>
                            </span>
                            <span title="File đã chọn" style="cursor: pointer" class="pr-2" v-if="['text', 'video', 'audio', 'pdf' ,'quiz'].includes(item.type)" @click="loadModalListFileSelected($event, item.type, key_group, key_item, item)">
                                <label style="cursor: pointer" :for="'list-file-seleted-'+key_group+'-'+key_item" class="m-1">
                                    <i class="fa fa-bars fa-2x text-info" aria-hidden="true"></i>
                                </label>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-1 mb-2">
                        <div class="input-group">
                            <input type="text"  v-bind:name="'lesson[item]['+key_group+']['+key_item+'][sort]'" v-model="item.sort" class="form-control js-input" data-noti="Sắp xếp từ bé tới lớn" placeholder="e.g: 1">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="input-group-append">
                            <span class="btn btn-danger js-input" style="color: #e55353; background-color: unset" data-noti="Xóa bài học" @click="trashItemGroup(group.props, key_item)"  style="cursor: pointer"><i class="fa fa-trash"  ></i></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-5">
                        <div class="input-group-append">
                            <a class="btn btn-outline-success w-100" @click="addItemGroup(group)" style="cursor: pointer">Thêm bài học mới</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-4"></div>
                <div class="col-sm-7">
                    <a class="btn btn-outline-info  w-100" @click="addGroup"  style="cursor: pointer">Thêm nhóm bài học mới</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Text-->
    <div v-if="Object.keys(data.text).length !== 0" class="modal fade" :id="'modal-'+data.text.type+'-text-'+data.text.group+'-'+data.text.item" tabindex="-1" role="dialog" aria-labelledby="modalTextTitle" aria-hidden="true">
        <div class="modal-dialog modal-info modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTextTitle">Nội dung</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="closeModalText($event)">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" :id="'editor-'+data.text.times" v-model="data.text.content">@{{data.text.content}}</textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="closeModalText($event)">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" @click="updateDataModalText($event, data.text.group, data.text.item, data.text.content, data.text.type)">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal upload and list file -->
    <div v-if="Object.keys(data.fileList).length !== 0" class="modal fade" :id="'modal-'+data.fileList.item.type+'-attached-'+data.fileList.item.group+'-'+data.fileList.item.item" tabindex="-1" role="dialog" aria-labelledby="modalUploadListFileTitle" aria-hidden="true">
        <div class="modal-dialog modal-info modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalUploadListFileTitle">File đính kèm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="closeModalFile($event)">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input class="form-control mr-2" :id="'name_attached_'+data.fileList.item.group+'_'+data.fileList.item.item" type="text" placeholder="Lọc tên file">
                                    <a href="javascript:;" class="btn btn-success" type="button" @click="searchFileName($event, data.fileList.item.group,  data.fileList.item.item, '#name_attached_'+data.fileList.item.group+'_'+data.fileList.item.item, 1)">Lọc</a>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label :for="'upload-'+data.fileList.item.group+'-'+data.fileList.item.item" class="btn btn-outline-success" style="cursor: pointer" title="Upload">
                                    <i class="fa fa-cloud-upload text-success" aria-hidden="true"></i>
                                    <input type="file" style="display: none" name="upload" :id="'upload-'+data.fileList.item.group+'-'+data.fileList.item.item" @change="uploadFile($event)">
                                    <span>Upload file mới</span>
                                </label>
                            </div>
                            <div class="col-sm-4 loading-process d-none">
                                <div class="time-percent" style="opacity: 1;">
                                    <div class="time" v-html="'0%'"></div>
                                </div>

                                <div class="progress">
                                    <div class="progress-before" :style="'width: 10%'"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-12">
                                <table class="sortable">
                                    <thead>
                                    <tr>
                                        <th>Filename</th>
                                        <th>Type</th>
                                        <th>Size</th>
                                        <th>Date Created</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="file" v-if="typeof data.fileList.list != 'undefined' && data.fileList.list.length > 0" v-for="itm in data.fileList.list">
                                            <td><span class="name" v-html="itm.name"></span></td>
                                            <td><span v-html="itm.mine_type"></span></td>
                                            <td><span v-html="itm.size"></span></td>
                                            <td><span v-html="itm.created_at"></span></td>
                                            <td>
                                                <input type="checkbox" :id="'check_attached_'+data.fileList.item.group+'_'+data.fileList.item.item" v-model="data.fileListSelected.attached" :value="itm">
                                            </td>
                                        </tr>

                                        <tr style="height: 40px" v-if="typeof data.fileList.list == 'undefined' || data.fileList.list.length === 0">
                                            <td colspan="5" class="text-center">Empty Data</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="closeModalFile($event)">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" @click="updateDataAttachedModal($event, data.fileList.item.group, data.fileList.item.item, data.fileListSelected.attached, data.fileList.item.type)">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal upload and list file Primary -->
    <div v-if="Object.keys(data.fileList).length !== 0" class="modal fade" :id="'modal-'+data.fileList.item.type+'-file-'+data.fileList.item.group+'-'+data.fileList.item.item" tabindex="-1" role="dialog" aria-labelledby="modalUploadListFilePrimaryTitle" aria-hidden="true">
        <div class="modal-dialog modal-info modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalUploadListFilePrimaryTitle">File Video/Audio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="closeModalFilePrimary($event)">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input class="form-control mr-2" :id="'name_primary_'+data.fileList.item.group+'_'+data.fileList.item.item" type="text" placeholder="Lọc tên file">
                                    <a href="javascript:;" class="btn btn-success" type="button" @click="searchFileName($event, data.fileList.item.group,  data.fileList.item.item, '#name_primary_'+data.fileList.item.group+'_'+data.fileList.item.item)">Lọc</a>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label :for="'upload-'+data.fileList.item.group+'-'+data.fileList.item.item" class="btn btn-outline-success" style="cursor: pointer" title="Upload">
                                    <i class="fa fa-cloud-upload text-success" aria-hidden="true"></i>
                                    <input type="file" style="display: none" name="upload" :id="'upload-'+data.fileList.item.group+'-'+data.fileList.item.item" @change="uploadFile($event)">
                                    <span>Upload file mới</span>
                                </label>

                            </div>
                            <div class="col-sm-4 loading-process d-none">
                                <div class="time-percent" style="opacity: 1;">
                                    <div class="time" v-html="'0%'"></div>
                                </div>

                                <div class="progress">
                                    <div class="progress-before" :style="'width: 10%'"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-12">
                                <table class="sortable">
                                    <thead>
                                    <tr>
                                        <th>Filename</th>
                                        <th>Type</th>
                                        <th>Size</th>
                                        <th>Date Created</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="file" v-if="typeof data.fileList.list != 'undefined' && data.fileList.list.length > 0" v-for="itm in data.fileList.list">
                                        <td><span class="name" v-html="itm.name"></span></td>
                                        <td><span v-html="itm.mine_type"></span></td>
                                        <td><span v-html="itm.size"></span></td>
                                        <td><span v-html="itm.created_at"></span></td>
                                        <td>
                                            <input type="radio" :id="'check_file_primary_'+data.fileList.item.group+'_'+data.fileList.item.item" v-model="data.fileListSelected.file_primary" :value="itm">
                                        </td>
                                    </tr>

                                    <tr style="height: 40px" v-if="typeof data.fileList.list == 'undefined' || data.fileList.list.length == 0">
                                        <td colspan="5" class="text-center">Empty Data</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="closeModalFilePrimary($event)">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" @click="updateDataFilePrimaryModal($event, data.fileList.item.group, data.fileList.item.item, data.fileListSelected.file_primary, data.fileList.item.type)">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Quiz -->
    <div v-if="Object.keys(data.fileList).length !== 0" class="modal fade" :id="'modal-'+data.fileList.item.type+'-quiz-'+data.fileList.item.group+'-'+data.fileList.item.item" tabindex="-1" role="dialog" aria-labelledby="modalListQuizTitle" aria-hidden="true">
        <div class="modal-dialog modal-info modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalListQuizTitle">Danh sách bài Quiz</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="closeModalQuiz($event)">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row" >
                            <div class="col-sm-12">
                                <table class="sortable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Quiz Name</th>
                                        <th>Date Created</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="file" v-if="typeof data.fileList.quiz != 'undefined' && data.fileList.quiz.length > 0" v-for="(itm, indexQ) in data.fileList.quiz">
                                        <td><span class="name" v-html="Number(indexQ)  + Number(1) "></span></td>
                                        <td><span class="name" v-html="itm.name"></span></td>
                                        <td><span v-html="itm.created_at"></span></td>
                                        <td>
                                            <input type="radio" :id="'check_file_primary_'+data.fileList.item.group+'_'+data.fileList.item.item" v-model="data.fileListSelected.quiz" :value="itm">
                                        </td>
                                    </tr>

                                    <tr style="height: 40px" v-if="typeof data.fileList.quiz == 'undefined' || data.fileList.quiz.length === 0">
                                        <td colspan="5" class="text-center">Empty Data</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="closeModalQuiz($event)">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" @click="updateDataQuizModal($event, data.fileList.item.group, data.fileList.item.item, data.fileListSelected.quiz, data.fileList.item.type)">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal List File Selected-->
    <div v-if="Object.keys(data.fileListSelected).length !== 0" class="modal fade" :id="'modal-'+data.fileListSelected.item.type+'-show-attached-'+data.fileListSelected.item.group+'-'+data.fileListSelected.item.item" tabindex="-1" role="dialog" aria-labelledby="modalFileSelectedTitle" aria-hidden="true">
        <div class="modal-dialog modal-info modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalFileSelectedTitle">Danh sách File</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="closeModalFileSelected($event)">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" v-if="['video', 'audio', 'pdf'].includes(data.fileListSelected.item.type)">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <h5>File Primary</h5>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <table class="sortable">
                                <thead>
                                <tr>
                                    <th>Filename</th>
                                    <th>Type</th>
                                    <th>Size</th>
                                    <th>Date Created</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="file" v-if="typeof data.fileListSelected.file_primary != 'undefined' && Object.keys(data.fileListSelected.file_primary).length !== 0 ">
                                    <td><span class="name" v-html="data.fileListSelected.file_primary.name"></span></td>
                                    <td><span v-html="data.fileListSelected.file_primary.mine_type"></span></td>
                                    <td><span v-html="data.fileListSelected.file_primary.size"></span></td>
                                    <td><span v-html="data.fileListSelected.file_primary.created_at"></span></td>
                                    <td>
                                        <span @click="trashItemFilePrimary($event)">
                                            <i class="fa fa-times text-danger" aria-hidden="true"></i>
                                        </span>
                                    </td>
                                </tr>

                                <tr style="height: 40px" v-if="typeof data.fileListSelected.file_primary == 'undefined' || Object.keys(data.fileListSelected.file_primary).length === 0">
                                    <td colspan="5" class="text-center">Empty Data</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="row" v-if="['text', 'video', 'audio', 'pdf'].includes(data.fileListSelected.item.type)">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <h5>File đính kèm</h5>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <table class="sortable">
                                <thead>
                                <tr>
                                    <th>Filename</th>
                                    <th>Type</th>
                                    <th>Size</th>
                                    <th>Date Created</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="file" v-if="typeof data.fileListSelected.attached != 'undefined' && data.fileListSelected.attached.length > 0" v-for="(itmA, indexA) in data.fileListSelected.attached">
                                    <td><span class="name" v-html="itmA.name"></span></td>
                                    <td><span v-html="itmA.mine_type"></span></td>
                                    <td><span v-html="itmA.size"></span></td>
                                    <td><span v-html="itmA.created_at"></span></td>
                                    <td>
                                        <span @click="trashItemAttached(data.fileListSelected.attached, indexA)">
                                            <i class="fa fa-times text-danger" aria-hidden="true"></i>
                                        </span>
                                    </td>
                                </tr>

                                <tr style="height: 40px" v-if="typeof data.fileListSelected.attached == 'undefined' || data.fileListSelected.attached.length === 0">
                                    <td colspan="5" class="text-center">Empty Data</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="row" v-if="['quiz'].includes(data.fileListSelected.item.type)">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <h5>Bài Quiz đã chọn</h5>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <table class="sortable">
                                <thead>
                                <tr>
                                    <th>Quiz Name</th>
                                    <th>Date Created</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="file" v-if="typeof data.fileListSelected.quiz != 'undefined' && Object.keys(data.fileListSelected.quiz).length !== 0" >
                                    <td><span class="name" v-html="data.fileListSelected.quiz.name"></span></td>
                                    <td><span v-html="data.fileListSelected.quiz.created_at"></span></td>
                                    <td>
                                        <span @click="trashItemQuiz()">
                                            <i class="fa fa-times text-danger" aria-hidden="true"></i>
                                        </span>
                                    </td>
                                </tr>

                                <tr style="height: 40px" v-if="typeof data.fileListSelected.quiz == 'undefined' || Object.keys(data.fileListSelected.quiz).length === 0">
                                    <td colspan="2" class="text-center">Empty Data</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="closeModalFileSelected($event)">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" @click="updateDataSelectedModal($event, data.fileListSelected.item.group, data.fileListSelected.item.item, data.fileListSelected.item.type, data.fileListSelected.attached, data.fileListSelected.file_primary)">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .time-percent {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        margin-bottom: 5px;
    }
    .time-percent .time {
        display: block;
        font-style: italic;
        font-weight: 500;
        font-size: 13px;
        line-height: 19px;
        color: #848383;
        margin-left: auto;
    }
    .progress {
        display: block;
        background: #e0e0e0;
        border: 1px solid #e0e0e0;
        border-radius: 8px;
        height: 7px;
        margin-bottom: 8px;
        box-shadow: inset 0 0.1rem 0.1rem rgb(39 44 51 / 10%);
        overflow: hidden;
        line-height: 0;
        font-size: .609375rem;
    }
    .progress .progress-before {
        position: relative;
        height: 100%;
        width: 100%;
    }
    .progress .progress-before::before {
        content: "";
        position: absolute;
        background: #006ac5;
        height: 100%;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
    }
    #container {
        box-shadow: 0 5px 10px -5px rgba(0,0,0,0.5);
        position: relative;
        background: white;
    }

    table {
        background-color: #F3F3F3;
        border-collapse: collapse;
        width: 100%;
        margin: 0 0 15px 0;
    }

    th {
        background-color: #FE4902;
        color: #FFF;
        cursor: pointer;
        padding: 5px 10px;
    }

    th small {
        font-size: 9px;
    }

    td, th {
        text-align: left;
    }

    span {
        text-decoration: none;
    }

    td span {
        color: #663300;
        display: block;
        padding: 5px 10px;
    }

    th span {
        padding-left: 0
    }

    td:first-of-type span {
        background: url(./.images/file.png) no-repeat 10px 50%;
        padding-left: 35px;
    }

    th:first-of-type {
        padding-left: 35px;
    }

    td:not(:first-of-type) a {
        background-image: none !important;
    }

    tr:nth-of-type(odd) {
        background-color: #E6E6E6;
    }

    tr:hover td {
        background-color:#CACACA;
    }

    tr:hover td span {
        color: #000;
    }

    /* icons for file types (icons by famfamfam) */

    /* images */
    table tr td:first-of-type a[href$=".jpg"],
    table tr td:first-of-type a[href$=".png"],
    table tr td:first-of-type a[href$=".gif"],
    table tr td:first-of-type a[href$=".svg"],
    table tr td:first-of-type a[href$=".jpeg"] {
        background-image: url(./.images/image.png);
    }

    /* zips */
    table tr td:first-of-type a[href$=".zip"] {
        background-image: url(./.images/zip.png);
    }

    /* css */
    table tr td:first-of-type a[href$=".css"] {
        background-image: url(./.images/css.png);
    }

    /* docs */
    table tr td:first-of-type a[href$=".doc"],
    table tr td:first-of-type a[href$=".docx"],
    table tr td:first-of-type a[href$=".ppt"],
    table tr td:first-of-type a[href$=".pptx"],
    table tr td:first-of-type a[href$=".pps"],
    table tr td:first-of-type a[href$=".ppsx"],
    table tr td:first-of-type a[href$=".xls"],
    table tr td:first-of-type a[href$=".xlsx"] {
        background-image: url(./.images/office.png)
    }

    /* videos */
    table tr td:first-of-type a[href$=".avi"],
    table tr td:first-of-type a[href$=".wmv"],
    table tr td:first-of-type a[href$=".mp4"],
    table tr td:first-of-type a[href$=".mov"],
    table tr td:first-of-type a[href$=".m4a"] {
        background-image: url(./.images/video.png);
    }

    /* audio */
    table tr td:first-of-type a[href$=".mp3"],
    table tr td:first-of-type a[href$=".ogg"],
    table tr td:first-of-type a[href$=".aac"],
    table tr td:first-of-type a[href$=".wma"] {
        background-image: url(./.images/audio.png);
    }

    /* web pages */
    table tr td:first-of-type a[href$=".html"],
    table tr td:first-of-type a[href$=".htm"],
    table tr td:first-of-type a[href$=".xml"] {
        background-image: url(./.images/xml.png);
    }

    table tr td:first-of-type a[href$=".php"] {
        background-image: url(./.images/php.png);
    }

    table tr td:first-of-type a[href$=".js"] {
        background-image: url(./.images/script.png);
    }

    /* directories */
    table tr.dir td:first-of-type a {
        background-image: url(./.images/folder.png);
    }
</style>
@push('js_bot_all')
    <script !src="">
        var i = 0;
        var lesson_data = '{!! isset($data) && !empty($data['lesson_data']) ? addslashes($data['lesson_data']) : '' !!}';
        var lessonVue = new Vue({
            el: '#lesson',
            data() {
                return {
                    data: {
                        val: lesson_data != '' ? JSON.parse(lesson_data) : [/*{title_group: '', sort: '', props: [{}]}*/],
                        text: {},
                        fileListSelected:{
                            item:{},
                            file_primary: {},
                            attached:[],
                            quiz:{}
                        },
                        fileList:{
                            item:{},
                            list:[],
                            quiz:[],
                        },
                        type: {
                            text: 'Text',
                            video: 'Video',
                            audio: 'Audio',
                            pdf: 'View PDF',
                            quiz: 'Quiz'
                        }

                    }
                }
            },
            mounted(){},
            methods: {
                addItemGroup: function (group) {
                    this.isShow = true;
                    return group.props.push({});
                },
                addGroup: function () {
                    this.isShow = true;
                    var group = {title_group: '', sort: '', props: [{}]};
                    return this.data.val.push(group);
                },
                trashItemGroup: function (props,index) {
                    if(props.length > 1) {
                        props.splice(index, 1);
                    }else {
                        props.splice(index, 1);
                        return item.push({});
                    }
                },
                trashGroup: function (key) {
                    if(this.data.val.length > 1) {
                        this.data.val.splice(key, 1);
                    }else {
                        this.data.val.splice(key, 1);
                        // var group = {title_group: '', sort: '', props: [{}]};
                        // return this.data.val.push(group);
                    }
                },

                changeType: function(e, item, key){
                    this.$set( item, key, e.target.value);
                },

                /*Start text*/
                loadModalText: function(e, type, key_group, key_item){
                    i++;
                    var vm = this;
                    this.$set(this.data.text, 'times', i);
                    this.$set(this.data.text, 'type', type);
                    this.$set(this.data.text, 'group', key_group);
                    this.$set(this.data.text, 'item', key_item);
                    this.$set(this.data.text, 'content', typeof vm.data.val[key_group].props[key_item].description != 'undefined' ? vm.data.val[key_group].props[key_item].description : null );
                    setTimeout(function () {
                        tinymce.init({
                            selector: '#editor-'+i,
                            height: 600,
                            setup: function(editor) {
                                // when typing keyup event
                                editor.on('keyup', function() {
                                    var new_value = tinymce.get('editor-'+i).getContent(this.value);
                                    vm.data.text.content = new_value
                                });
                            }
                        });
                        $('#modal-'+type+'-text-'+key_group+'-'+key_item).modal('show');
                    }, 1000)
                },
                updateDataModalText: function(e, key_group, key_item, val, type){
                    $('#'+type+'-text-'+key_group+'-'+key_item).val(val);
                    this.data.val[key_group].props[key_item].description = val;
                    this.closeModalText();
                },
                closeModalText: function(e){
                    this.data.text = {};
                },
                /*End text*/

                /*Start File Primary*/
                loadModalUploadListFilePrimary: function(e, type, key_group, key_item){
                    var vm = this;
                    vm.$set(vm.data.fileList.item, 'type', type);
                    vm.$set(vm.data.fileList.item, 'group', key_group);
                    vm.$set(vm.data.fileList.item, 'item', key_item);
                    setTimeout(function () {
                        $('#modal-'+type+'-file-'+key_group+'-'+key_item).modal('show');
                    }, 1000);
                    vm.loadFile(key_group, key_item)
                },
                updateDataFilePrimaryModal: function(e, key_group, key_item, val, type){
                    /*$('#'+type+'-file-'+key_group+'-'+key_item).val(Object.keys(val).length !== 0 ? val.id : '');
                    console.log($('#'+type+'-file-'+key_group+'-'+key_item).val())*/
                    this.data.val[key_group].props[key_item].file_id = Object.keys(val).length !== 0 ? val.id : null;
                    this.data.val[key_group].props[key_item].file = val;
                    this.closeModalFilePrimary()
                },
                closeModalFilePrimary: function(e){
                    this.data.fileListSelected.file_primary = {};
                },
                /*End File Primary*/

                /*Start Attached*/
                loadModalUploadListFile: function(e, type, key_group, key_item){
                    var vm = this;
                    vm.$set(vm.data.fileList.item, 'type', type);
                    vm.$set(vm.data.fileList.item, 'group', key_group);
                    vm.$set(vm.data.fileList.item, 'item', key_item);
                    setTimeout(function () {
                        $('#modal-'+type+'-attached-'+key_group+'-'+key_item).modal('show');
                    }, 1000);

                    var fd = new FormData();
                    fd.append('type', type);
                    this.loadFile(key_group, key_item, '', 1)

                },
                updateDataAttachedModal: function(e, key_group, key_item, val, type){
                    var arrAttchedID = [];
                    if(val.length > 0) {
                        $.each(val, function (key, value) {
                            arrAttchedID.push(value.id)
                        })
                    }
                    // $('#'+type+'-attached-'+key_group+'-'+key_item).val(arrAttchedID);
                    this.data.val[key_group].props[key_item].attached_str = arrAttchedID;
                    this.data.val[key_group].props[key_item].attached = val;
                    this.closeModalFile()
                },
                closeModalFile: function(e){
                    this.data.fileListSelected.attached = [];
                },
                /*End Attached*/

                /*Show Attached, File Primary*/
                loadModalListFileSelected: function(e, type, key_group, key_item, item){
                    this.$set(this.data.fileListSelected.item, 'type', type);
                    this.$set(this.data.fileListSelected.item, 'group', key_group);
                    this.$set(this.data.fileListSelected.item, 'item', key_item);
                    this.data.fileListSelected.attached = item.attached;
                    this.data.fileListSelected.file_primary = item.file;
                    this.data.fileListSelected.quiz = item.quiz;
                    setTimeout(function () {
                        $('#modal-'+type+'-show-attached-'+key_group+'-'+key_item).modal('show');
                    }, 1000)
                },
                trashItemFilePrimary: function(e){
                    this.data.fileListSelected.file_primary = {};
                },
                trashItemAttached: function (attached,index) {
                    if(attached.length > 1) {
                        attached.splice(index, 1);
                    }else {
                        attached.splice(index, 1);
                    }
                },
                updateDataSelectedModal: function(e, key_group, key_item, type, val, val_primary){
                    var arrAttchedID = [];
                    if(val.length > 0){
                        $.each(val, function (key, value) {
                            arrAttchedID.push(value.id)
                        })
                    }
                    // $('#'+type+'-attached-'+key_group+'-'+key_item).val(arrAttchedID);
                    // $('#'+type+'-file-'+key_group+'-'+key_item).val(typeof val_primary != 'undefined' && Object.keys(val_primary).length !== 0 ? val_primary.id : '');
                    this.data.val[key_group].props[key_item].attached_str = arrAttchedID;
                    this.data.val[key_group].props[key_item].attached = val;
                    this.data.val[key_group].props[key_item].file_id = typeof val_primary != 'undefined' && Object.keys(val_primary).length !== 0 ? val_primary.id : null;
                    this.data.val[key_group].props[key_item].file = val_primary;
                    this.closeModalFileSelected()
                },
                closeModalFileSelected: function(e){
                    this.data.fileListSelected.attached = [];
                    this.data.fileListSelected.file_primary = {};
                },
                /*End Attached, File Primary*/


                /*Start Quiz*/
                loadModalQuiz: function(e, type, key_group, key_item){
                    var vm = this;
                    vm.$set(vm.data.fileList.item, 'type', type);
                    vm.$set(vm.data.fileList.item, 'group', key_group);
                    vm.$set(vm.data.fileList.item, 'item', key_item);
                    setTimeout(function () {
                        $('#modal-'+type+'-quiz-'+key_group+'-'+key_item).modal('show');
                    }, 1000);

                    var fd = new FormData();
                    fd.append('type', type);
                    $.ajax({
                        type: 'GET',
                        url: "{{route('admin_list_quiz')}}",
                        data: {},
                        contentType: false,
                        processData: false,
                        statusCode: {
                            422: function(response) {
                                var json = JSON.parse(response.responseText);
                                if (json) {

                                    $.each(json.errors, function(key, val) {
                                        Swal.fire(
                                            'Thất bại',
                                            val[0],
                                            'error'
                                        )
                                    });
                                }
                            },
                            200: function(response) {
                                if (response.error === 0) {
                                    vm.data.fileList.quiz = response.data.data
                                }
                            }
                        },
                        beforeSend: function() {

                        }
                    }).done(function(json) {

                    });

                },
                trashItemQuiz: function(e){
                    this.data.fileListSelected.quiz = {};
                },
                updateDataQuizModal: function(e, key_group, key_item, val, type){

                    // $('#'+type+'-quiz-'+key_group+'-'+key_item).val(typeof val != 'undefined'  && Object.keys(val).length !== 0 ? val.id : '');
                    this.data.val[key_group].props[key_item].quiz_id = typeof val != 'undefined'  && Object.keys(val).length !== 0 ? val.id : null;
                    this.data.val[key_group].props[key_item].quiz = val;
                    this.closeModalQuiz()
                },
                closeModalQuiz: function(e){
                    this.data.fileListSelected.quiz = {};
                },
                /*End Quiz*/


                uploadFile: function (e, index_group, index_item) {
                    var vm = this;
                    var process_line = $('.progress-before');
                    var percent = $('.time');
                    var dom = e.target;
                    var files = $(dom)[0].files;
                    var fd = new FormData();
                    fd.append('file', files[0]);
                    fd.append('_token', ENV.token);

                    $.ajax({
                        xhr: function() {
                            var xhr = new window.XMLHttpRequest();

                            // Upload progress
                            xhr.upload.addEventListener("progress", function(evt){
                                if (evt.lengthComputable) {
                                    var percentComplete = Math.floor((evt.loaded / evt.total) * 100);
                                    console.log(percentComplete)
                                    process_line.width(percentComplete+'%');
                                    percent.html(percentComplete+'%');
                                }
                            }, false);

                            // Download progress
                            xhr.addEventListener("progress", function(evt){
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    // Do something with download progress
                                    console.log(percentComplete);
                                }
                            }, false);

                            return xhr;
                        },
                        type: 'POST',
                        url: "{{route('admin_course_upload')}}",
                        data: fd,
                        contentType: false,
                        processData: false,
                        statusCode: {
                            422: function(response) {
                                var json = JSON.parse(response.responseText);
                                if (json) {

                                    $.each(json.errors, function(key, val) {
                                        Swal.fire(
                                            'Thất bại',
                                            val[0],
                                            'error'
                                        )
                                    });
                                }
                            },
                            413: function (response) {
                                Swal.fire(
                                    'Thất bại',
                                    'File có dung lượng quá lớn',
                                    'error'
                                )
                            },
                            200: function(response) {
                                if (response.error === 1) {
                                    Swal.fire(
                                        response.data.title,
                                        response.data.mess,
                                        'error'
                                    )
                                } else {
                                    Swal.fire(
                                        'Thành công',
                                        '',
                                        'success'
                                    )

                                    // vm.data.fileListSelected.attached = vm.data.fileListSelected.attached.concat(response.data);
                                    vm.data.fileList.list = [response.data.data, ...vm.data.fileList.list];
                                    // shop.reload();
                                    $('.loading-process').removeClass('d-block').addClass('d-none');
                                }

                            }
                        },
                        beforeSend: function() {
                            // status.empty();
                            var percentVal = '0%';
                            process_line.width(percentVal);
                            percent.html(percentVal);
                            $('.loading-process').removeClass('d-none').addClass('d-block');
                        },
                        complete: function (){
                            $('.loading-process').removeClass('d-block').addClass('d-none');
                        }
                    }).done(function(json) {
                        $('.loading-process').removeClass('d-block').addClass('d-none');
                    });
                },

                loadFile: function(group, item, name = '', attached = 0){
                    var vm = this;
                    var type = '';
                    if(attached === 0){
                        type = typeof vm.data.val[group].props[item].type != 'undefined' && ['video', 'audio', 'pdf'].includes(vm.data.val[group].props[item].type) ? vm.data.val[group].props[item].type :  '';
                    }else{
                        type = 'pdf, doc, docx';
                    }
                    $.ajax({
                        type: 'GET',
                        url: "{{route('admin_list_file')}}",
                        data: {
                            name: name,
                            type: type
                        },
                        statusCode: {
                            422: function(response) {
                                var json = JSON.parse(response.responseText);
                                if (json) {

                                    $.each(json.errors, function(key, val) {
                                        Swal.fire(
                                            'Thất bại',
                                            val[0],
                                            'error'
                                        )
                                    });
                                }
                            },
                            200: function(response) {
                                if (response.error === 0) {
                                    vm.data.fileList.list = response.data.data
                                }
                            }
                        },
                        beforeSend: function() {

                        }
                    }).done(function(json) {

                    });
                },

                searchFileName: function (e, group, item, dom, attached = 0) {
                    var name = $(dom).val();
                    this.loadFile(group, item, name, attached)
                }

            },
        });
    </script>
@endpush