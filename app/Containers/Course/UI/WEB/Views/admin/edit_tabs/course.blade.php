<div class="tab-pane " id="course">
    <div class="tabbable">


        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="input-category"><span data-toggle="tooltip" title="Bộ lọc">Khóa học</span></label>
            <div class="col-sm-6">
                <input type="text" placeholder="Nhập để tìm kiếm khóa học" id="input-course" class="form-control"/>
            </div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <div id="news_course" class="row mx-0 list-group flex-row">
                    @if(isset($course_combo))
                        @foreach($course_combo as $item)
                            @if(@$item->id)
                                <a href="#" id="combo-course-{{$item->id}}"
                                   class="col-3 list-group-item list-group-item-action">
                                    <i class="fa fa-close"></i> {!! @$item->desc->name  !!}
                                    <input type="hidden" name="combo_course[]" value="{{$item->id}}"/>
                                </a>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@push('js_bot_all')
    <script>
        $('input#input-course').autocomplete({
            classes: {
                "ui-autocomplete": "dropdown-menu"
            },
            'source': function (request, response) {
                $.ajax({
                    global: false,
                    url: "{{route('api_course_get_all')}}",
                    dataType: 'json',
                    headers: ENV.headerParams,
                    data: {type: 'select2', course_type: 0, name: request.term, _token: ENV.token},
                    success: function (json) {
                        response($.map(json.data, function (item) {
                            return {
                                label: item.text,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            'select': function (event, ui) {
                var item = ui.item;

                $('#category-' + item.value).remove();

                var html = '<a href="#" id="course-' + item.value + '" class="col-3 list-group-item list-group-item-action">\n' +
                    '                        <i class="fa fa-close"></i> ' + item.label + '\n' +
                    '                        <input type="hidden" name="combo_course[]" value="' + item.value + '" />\n' +
                    '                    </a>';

                $('#news_course').append(html);

                $('input#input-course').val('');
                return false;
            }
        });

        $('#news_course').delegate('.fa-close', 'click', function () {
            $(this).parent().remove();
        });
    </script>
@endpush