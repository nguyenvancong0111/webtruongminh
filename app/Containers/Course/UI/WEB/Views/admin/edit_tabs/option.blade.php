<div class="tab-pane" id="option">
    <div class="tabbable">
        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="status">Options Group</label>
            <div class="col-sm-6">
                <select class="form-control" name="option_gr" id="option_gr" @change="changeType($event)">
                    <option value="0" :selected="type === 0">--Chọn--</option>
                    <option v-if="typeof option != 'undefined'" v-for="gr in option" :value="gr.id" v-html="gr.name" :selected="type == gr.id"></option>
                </select>
            </div>
        </div>
        <div class="row pl-1 pr-1" v-if="typeof option[type] != 'undefined'">
            <div class="col-sm-12">
                <div v-for="(item, key) in option[type].option_values">
                    <input type="hidden" v-bind:name="'option['+key+'][id]'" v-model="item.id" class="form-control js-input" >
                    <div class="row mt-3">
                        <div class="col-sm-1">
                            <div class="form-group text-right p-2">
                                <div class="checkbox ml-1 mb-1 form-check-inline radio-success js-input">
                                    {{--<input v-if="data.type === 1" type="checkbox" :id="'answer_true_'+key" v-bind:name="'checkbox_true[]'" v-model="item.checkbox" :value="key" :checked="item.checkbox === 1">
                                    <input v-if="data.type === 2" type="radio" :id="'answer_true'" v-bind:name="'radio_true'" v-model="item.radio" :value="key" :checked="item.radio === 1">--}}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <input type="text" v-bind:name="'option['+key+'][title]'" v-model="item.title" class="form-control js-input">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <input type="text" v-bind:name="'option['+key+'][value]'" v-model="item.value" class="form-control js-input" placeholder="Giá trị áp dụng">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <input type="text" v-bind:name="'option['+key+'][price]'" v-model="item.price" class="form-control js-input" placeholder="Giá tiền áp dụng" @keypress="isNumber($event)" >
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="c-switch c-switch-label c-switch-outline-primary">
                                    <input class="c-switch-input" v-bind:name="'option['+key+'][is_active]'" v-model="item.is_active" type="checkbox" :checked="item.is_active == true">
                                    <span class="c-switch-slider" data-checked="On" data-unchecked="Off"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js_bot_all')
    <script !src="">
        var url_option = '{{route('admin.ajax.option.all', ['course_id' => $data->id])}}';
        var type = '{{ !empty($data->option_gr) ? $data->option_gr : 0 }}';
        var OptionVue = new Vue({
            el: '#option',
            data: {
                url_option: url_option,
                option: [],
                type: parseInt(type),
            },
            mounted: function () {
                this.loadOption();
            },
            methods: {
                loadOption: function(){
                    $.get(this.url_option).then(json => {
                        this.option = json.data;
                    });
                },
                changeType: function(e){
                    this.type = parseInt( e.target.value)
                },
                isNumber: function(evt) {
                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
                        evt.preventDefault();;
                    } else {
                        return true;
                    }
                },
                formatNumber: function(e, value) {
                    let val = (value/1).toFixed(0).replace('.', ',');
                    e.target.value = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                },

            },
        });
    </script>
@endpush