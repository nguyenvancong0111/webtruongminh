<div class="tab-pane" id="image">
    <div class="tabbable">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="image">Ảnh đại diện</label>
                    <input type="file" id="image" name="image" class="dropify form-control {{ $errors->has('image') ? ' is-invalid' : '' }}" data-show-remove="false" data-default-file="{{ \ImageURL::getImageUrl(old('image',@$data['image']), 'course', 'original')    }}">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="image">Ảnh đại diện <span>(Trang chi tiết - 700x400 px) </span></label>
                    <input type="file" id="image_detail" name="image_detail" class="dropify form-control {{ $errors->has('image_detail') ? ' is-invalid' : '' }}" data-show-remove="false" data-default-file="{{ \ImageURL::getImageUrl(old('image_detail',@$data['image_detail']), 'course', 'original')    }}">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="image">Video giới thiệu <small class="text-danger">(Ưu tiên)</small></label>
                    <input type="file" id="video_intro" name="video_intro" class="dropify form-control {{ $errors->has('video_intro') ? ' is-invalid' : '' }}" data-show-remove="false" data-default-file="{{ \ImageURL::getFileUrl(old('video_intro',@$data['video_intro']), 'course', 'video')    }}">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="image">Video giới thiệu <small class="text-danger">(Sử dụng Link Youtube)</small></label>
                    <input type="text" id="youtube_id" name="video_intro_youtube" class="form-control {{ $errors->has('video_intro_youtube') ? ' is-invalid' : '' }}" value="{{ old('video_intro_youtube',@$data['video_intro_youtube']) }}" onchange="admin.previewVideo($(this).val())">
                    <div class="form-group mt-2">
                        <div class="embed-responsive embed-responsive-16by9 @if(isset($data) && !empty($data['video_intro_youtube'])) d-block @else d-none @endif">
                            <iframe class="embed-responsive-item" id="preview_videos" src="@if(isset($data) && !empty($data['video_intro_youtube']))https://www.youtube.com/embed/{{ @$data['video_intro_youtube'] }} @endif"></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>