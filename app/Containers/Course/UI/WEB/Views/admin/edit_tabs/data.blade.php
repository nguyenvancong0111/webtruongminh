<div class="tab-pane" id="data">
    <div class="tabbable">
        {{-- <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="published">Ngày hiệu lực <span class="d-block small text-danger">(Ngày mà Khóa học chính thức được đăng)</span></label>
            <div class="col-sm-6">
                <input type="text" name="published" readonly value="{{ old('published',@$data['published'] ? Carbon\Carbon::parse($data['published'])->format('d/m/Y H:i') : '') }}" placeholder="dd/mm/YY H:i"
                       id="published" class="form-control">
            </div>
        </div> --}}

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="status">Phân loại</label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" id="status" onchange="getCateByType($(this).val(), '#cate')">
                    <option value="0" {{@$data['type'] == 0 ? 'selected' : ''}}>Khóa học</option>
                    <option value="1" {{@$data['type'] == 1 ? 'selected' : ''}}>Combo</option>
                </select>
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="status">Hiển thị ở trang chủ <span
                    class="d-block small text-danger">(Hiển thị hay không)</span></label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('is_home') ? ' is-invalid' : '' }}" name="is_home" id="status">
                    <option value="0" {{@$data['is_home'] == 0 ? 'selected' : ''}}>Ẩn</option>
                    <option value="1" {{@$data['is_home'] == 1 ? 'selected' : ''}}>Hiển thị</option>
                </select>
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="status">Trạng thái <span
                    class="d-block small text-danger">(Hiển thị hay không)</span></label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" id="status">
                    <option value="1" {{@$data['status'] == 1 ? 'selected' : ''}}>Ẩn</option>
                    <option value="2" {{@$data['status'] == 2 ? 'selected' : ''}}>Hiển thị</option>
                </select>
            </div>
        </div>
        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="type">Danh mục <span class="d-block small text-danger"></span></label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('cate') ? ' is-invalid' : '' }}" name="cate" id="cate">
                    <option value="">--- Lựa chọn --- </option>
                    @foreach($cate as $key => $item)
                        <option value="{{ $item->category_id }}" @if($item->category_id == (@$data['cate'] ?? null)) selected @endif>
                            {!!  $item->name  !!}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="type-combo @if(@$data->type == 1)  d-none @else d-block @endif">
            <div class="row form-group align-items-center">
                <label class="col-sm-2 control-label text-right mb-0" for="type">Giảng viên <span class="d-block small text-danger"></span></label>
                <div class="col-sm-6">
                    <select class="form-control {{ $errors->has('lecturers') ? ' is-invalid' : '' }} select2" style="width: 100%" name="lecturers" id="lecturers">
                        <option value="">--- Lựa chọn --- </option>
                        @foreach($lecturers as $key => $item)
                            <option value="{{ $item->id }}" @if($item->id == (@$data['lecturers'] ?? null)) selected @endif>
                                {!!  $item->fullname  !!}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="type">Giá gạch <span class="d-block small text-danger"> nếu có - Có giá trị hiển thị </span></label>
            <div class="col-sm-6">
                <input type="text" name="global_price" value="{{ old('global_price', @$data['global_price'])}}" placeholder="" id="global_price" class="form-control" onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()">
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="type">Giá <span class="d-block small text-danger"> Là giá trị thanh toán</span></label>
            <div class="col-sm-6">
                <input type="text" name="price" value="{{ old('price', \Apiato\Core\Foundation\FunctionLib::numberFormat(@$data['price']))}}" placeholder="" id="price" class="form-control" onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()">
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="sort_order">Sắp xếp <span
                    class="d-block small text-danger">(Vị trí, càng nhỏ thì càng lên đầu)</span></label>
            <div class="col-sm-3">
                <input type="text" name="sort_order" value="{{ old('sort_order', \Apiato\Core\Foundation\FunctionLib::numberFormat(@$data['sort_order']))}}" placeholder="Vị trí sắp xếp" id="sort_order" class="form-control" onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()">
            </div>
        </div>
        <div class="type-combo @if(@$data->type == 1)  d-none @else d-block @endif">
            <div class="row form-group align-items-center">
                <label class="col-sm-2 control-label text-right mb-0" for="sort_order">Học viên<span
                        class="d-block small text-danger">(Số học viên đang theo học)</span></label>
                <div class="col-sm-3">
                    <input type="text" name="student" value="{{ old('student', \Apiato\Core\Foundation\FunctionLib::numberFormat(@$data['student']))}}" placeholder="" id="student" class="form-control" onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()">
                </div>
            </div>
        </div>
    </div>
</div>

@push('js_bot_all')
    <script>
        $('#published').datetimepicker({format: 'd/m/Y H:i',});
        $(document).ready(function() {
            $(".select2").select2({ width: 'resolve' });
        });
    </script>
@endpush
