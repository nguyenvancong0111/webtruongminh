@extends('basecontainer::admin.layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if (isset($editMode) && $editMode)
                {!! Form::open(['url' => route('admin_course_edit_page', $data->id), 'files' => true]) !!}
            @else
                {!! Form::open(['url' => route('admin_course_add_page'), 'files' => true]) !!}
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif
            @if (session('status'))
                <!-- Flexbox container for aligning the toasts -->
                <div class="position-fixed bottom-0 right-0 p-3" style="z-index: 5; right: 0; bottom: 0;">
                    <div id="themmoi" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000">
                        <div class="toast-header">
                            <svg class="bd-placeholder-img rounded mr-2" width="20" height="20" xmlns="http://www.w3.org/2000/svg" role="img" aria-label=" :  " preserveAspectRatio="xMidYMid slice" focusable="false"><title> </title><rect width="100%" height="100%" fill="#007aff"></rect><text x="50%" y="50%" fill="#dee2e6" dy=".3em"> </text></svg>

                            <strong class="mr-auto">Thành công</strong>
                            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="toast-body">
                            {!! session('status') !!}
                        </div>
                    </div>
                </div>
            @endif

            <div class="card card-accent-primary">
                <div class="card-header">
                    <i class="fa fa-pencil"></i>THÔNG TIN
                </div>
                <div class="card-body">
                    <div class="tabbable boxed parentTabs">
                        <ul class="nav nav-tabs nav-underline nav-underline-primary mb-3">
                            <li class="nav-item">
                                <a class="nav-link active" href="#general"><i class="fa fa-empire"></i> Chung</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#data"><i class="fa fa-deviantart"></i> Thông tin chi tiết</a>
                            </li>

                            <li class="nav-item">
                                @if (isset($editMode) && $editMode)
                                    @if(@$data->type == 0)
                                        <a class="nav-link type-tab" data-combo="#course" data-course="#lesson" href="#lesson"><i class="fa fa-deviantart"></i> <span class="type-text" data-combo="Danh sách khóa học" data-course="Danh sách bài học">Danh sách bài học</span></a>
                                    @elseif(@$data->type == 1)
                                        <a class="nav-link type-tab" data-combo="#course" data-course="#lesson" href="#course"><i class="fa fa-deviantart"></i> <span class="type-text" data-combo="Danh sách khóa học" data-course="Danh sách bài học">Danh sách khóa học</span></a>
                                    @endif
                                @else
                                    <a class="nav-link" href="javascript:;" style="opacity: 0.5; cursor: not-allowed"><i class="fa fa-deviantart"></i> <span class="type-text" data-combo="Danh sách khóa học" data-course="Danh sách bài học">Danh sách bài học</span></a>
                                @endif
                            </li>
                            <li class="nav-item option @if(@$data->type == 1) d-none @endif" >
                                @if (isset($editMode) && $editMode)
                                    <a class="nav-link" href="#option"><i class="fa fa-deviantart"></i> <span >Biến thể</span></a>
                                @else
                                    <a class="nav-link" href="javascript:;" style="opacity: 0.5; cursor: not-allowed"><i class="fa fa-deviantart"></i> <span>Biến thể</span></a>
                                @endif
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#image"><i class="fa fa-image"></i> Hình ảnh & Video</a>
                            </li>
                        </ul>

                        <div class="tab-content p-0">
                            @include('course::admin.edit_tabs.general')

                            @include('course::admin.edit_tabs.data')

                            @if (isset($editMode) && $editMode)
                                @include('course::admin.edit_tabs.lesson')
                                @include('course::admin.edit_tabs.course')
                                @include('course::admin.edit_tabs.option')
                            @endif
                            @include('course::admin.edit_tabs.image')
                        </div>
                    </div>
                </div>
            </div>


            <div class="mb-3">
                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Cập nhật</button>
                &nbsp;&nbsp;
                <a class="btn btn-sm btn-danger" href="{{ redirect()->back()->getTargetUrl() }}"><i class="fa fa-ban"></i>
                    Hủy bỏ</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')
    {!! \FunctionLib::addMedia('admin/js/library/tag/jquery.tag-editor.css') !!}
    {!! \FunctionLib::addMedia('admin/js/library/uploadifive/uploadifive.css') !!}
@stop

@push('js_bot_all')
    {!! \FunctionLib::addMedia('admin/js/library/tinymce/tinymce.min.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/tag/jquery.caret.min.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/tag/jquery.tag-editor.min.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/jquery.form.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/jquery.sortable.js') !!}
    <script type="text/javascript">
        $('#themmoi').toast('show');

        $("ul.nav-tabs a").click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
        admin.system.tinyMCE('.__editor', plugins = '', menubar = '', toolbar = '', height = 300);

        function getCateByType(type, outputElement) {
            var textCombo = $('.type-text').attr('data-combo');
            var textCourse = $('.type-text').attr('data-course');

            var tabCombo = $('.type-tab').attr('data-combo')
            var tabCourse = $('.type-tab').attr('data-course')
            if(parseInt(type) === 1){
                $('.type-tab').attr('href', tabCombo);
                $('.type-text').empty().html(textCombo);
                $('.type-combo').removeClass('d-block').addClass('d-none');
                $('.option').removeClass('d-block').addClass('d-none');
            }else{
                $('.type-tab').attr('href', tabCourse);
                $('.type-text').empty().html(textCourse);
                $('.type-combo').removeClass('d-none').addClass('d-block');
                $('.option').removeClass('d-none').addClass('d-block');
            }
            // $.get(`/category/type/${type}`).then(res => {
            //     if (res.success) {
            //         Swal.close();
            //         var html = '<option value="0">-- Chọn Menu cha --</option>'
            //         $(outputElement).html(html + res.data);
            //     }
            // });
        }

        function generateSKUCode(dom, codeLength = 6, lang = 1) {
            var skuCode = Math.random().toString(36).substring(2, codeLength+2).toUpperCase();
            $(dom).val(skuCode);
        }

        function mixMoney(myfield) {
            var thousands_sep = '.',
                val = parseInt(myfield.value.replace(/[.*+?^${}()|[\]\\]/g, ''));
            myfield.value = shop.numberFormat(val, 0, '', thousands_sep);
        }

    </script>
@endpush
