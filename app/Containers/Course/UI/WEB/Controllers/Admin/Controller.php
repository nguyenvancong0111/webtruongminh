<?php

namespace App\Containers\Course\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Category\Actions\Admin\GetAllCategoriesAction;
use App\Containers\Course\Actions\Admin\GetLessonFileListAction;
use App\Containers\Course\Actions\CourseComboListingAction;
use App\Containers\Course\Actions\CreateCourseLessonFileAction;
use App\Containers\Course\Actions\FrontEnd\UpdateLessonCourseAction;
use App\Containers\Course\Models\Course;
use App\Containers\Course\UI\WEB\Controllers\Admin\Features\UpdateSomeStatus;
use App\Containers\Course\UI\WEB\Requests\Admin\FindCourseRequest;
use App\Containers\Course\UI\WEB\Requests\CreateCourseRequest;
use App\Containers\Course\UI\WEB\Requests\EditCourseRequest;
use App\Containers\Course\UI\WEB\Requests\StoreCourseRequest;
use App\Containers\Course\UI\WEB\Requests\UpdateCourseRequest;
use App\Containers\Course\UI\WEB\Requests\GetAllCourseRequest;
use App\Containers\Course\UI\WEB\Transformers\CourseLessonFileTransformer;
use App\Containers\Course\UI\WEB\Transformers\ListQuizTransformer;
use App\Containers\Exam\Actions\ExamListingAction;
use App\Containers\Lecturers\Actions\GetAllLecturersAction;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use FFMpeg\FFMpeg;
use Illuminate\Http\Request;
use App\Containers\Course\Enums\CourseType;
use ProtoneMedia\LaravelFFMpeg\FFMpeg\FFProbe;

class Controller extends AdminController
{
    use UpdateSomeStatus;

    public function __construct()
    {
        $this->title = 'Khóa học';
        parent::__construct();
    }
    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllCourseRequest $request)
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Khóa Học ', $this->form == 'list' ? '' : route('admin_course_home_page'));
        \View::share('breadcrumb', $this->breadcrumb);

        $course = Apiato::call('Course@CourseListingAction', [$request->all(),['id' => 'desc'], $this->perPage, false,Apiato::call('Localization@GetDefaultLanguageAction'),[],$request->page ?? 1]);

        return view('course::admin.index', [
            'search_data' => $request,
            'data' => $course,
            'typeOptions'=> CourseType::TEXT,
        ]);
    }

    public function edit(Request $request)
    {
        $this->showEditForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Khóa Học', $this->form == 'list' ? '' : route('admin_course_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Chỉnh sửa thông tin');
        \View::share('breadcrumb', $this->breadcrumb);

        $course = Apiato::call('Course@Admin\FindCourseByIdAction', [$request->id, ['course_group', 'course_group.course_item_group', 'course_group.course_item_group.attached_file'
            , 'course_group.course_item_group.file_primary', 'course_group.course_item_group.quiz_re']]);
        $lesson_data = [];
//        dd($course);
        if (!empty($course) && $course->course_group->isNotEmpty()){
            foreach ($course->course_group as $group){
                $props = [];
                if ($group->course_item_group->isNotEmpty()){
                    foreach ($group->course_item_group as $item){
                        $file_primary = [];
                        $quiz = [];
                        if (!empty($item->file_primary)){
                            $file_primary['id'] = $item->file_primary->id;
                            $file_primary['name'] = $item->file_primary->name;
                            $file_primary['type'] = $item->file_primary->type;
                            $file_primary['mine_type'] = $item->file_primary->mine_type;
                            $file_primary['size'] = \FunctionLib::formatBytes($item->file_primary->size, 2);
                            $file_primary['created_at'] = \Apiato\Core\Foundation\FunctionLib::dateFormat($item->file_primary->created_at, 'd/m/Y H:i');
                        }
                        if (!empty($item->quiz_re)){
                            $quiz['id'] = $item->quiz_re->id;
                            $quiz['name'] = $item->quiz_re->title;
                            $quiz['created_at'] = \Apiato\Core\Foundation\FunctionLib::dateFormat($item->quiz_re->created_at, 'd/m/Y H:i');
                        }
                        $props[] = [
                            'id' => $item->id,
                            'group_id' => $item->group_id,
                            'title' => str_replace('"','\'', $item->title),
                            'type' => $item->type,
                            'description' => $item->description,
                            'attached' => $this->transform($item->attached_file, new CourseLessonFileTransformer())['data'] ,
                            'file' => isset($file_primary) ? $file_primary : '',
                            'attached_str' => $item->attched,
                            'file_id' => $item->file,
                            'quiz_id' => $item->quiz,
                            'quiz' => isset($quiz) ? $quiz : '',
                            'preview' => $item->preview,
                            'sort' => $item->sort,
                            'status' => 2
                        ];
                    }
                }
                $lesson_data[] = [
                    'id' => $group->id,
                    'title_group' => $group->title,
                    'sort' => $group->sort,
                    'props' => $props,
                ];
            }
        }
        $course->lesson_data = !empty($lesson_data) ? json_encode($lesson_data,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE) : null;

        $course_combo = app(CourseComboListingAction::class)->skipCache(true)->run($course->id);
//        dd($course_combo);


        $cate = app(GetAllCategoriesAction::class)->skipCache(true)->run(
            ['cate_type' => 1, 'status' => 2], 10, true, 1
        );
        $lecturers = app(GetAllLecturersAction::class)->skipCache(true)->run(
            new DataTransporter(['status' => 2, 'limit' => 100]), false, [], ['*']
        );
        return view('course::admin.edit', [
            'data' => $course,
            'typeOptions'=> CourseType::TEXT,
            'cate' => $cate,
            'lecturers' => $lecturers,
            'course_combo' => $course_combo,
        ]);

        return $this->notfound($request->id);
    }

    public function add(Request $request)
    {
        $this->showAddForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Khóa Học', $this->form == 'list' ? '' : route('admin_course_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Thêm mới');
        \View::share('breadcrumb', $this->breadcrumb);

        $cate = app(GetAllCategoriesAction::class)->skipCache(true)->run(
            ['cate_type' => 1, 'status' => 2], 10, true, 1
        );
        $lecturers = app(GetAllLecturersAction::class)->skipCache(true)->run(
            new DataTransporter(['status' => 2, 'limit' => 100]), false, [], ['*']
        );
        return view('course::admin.edit', [
            'typeOptions'=> CourseType::TEXT,
            'cate' => $cate,
            'lecturers' => $lecturers
        ]);
    }

    public function update(UpdateCourseRequest $request)
    {
        try {
            $tranporter = new DataTransporter($request);

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'course', StringLib::getClassNameFromString(Course::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }
            if (isset($request->image_detail)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image_detail', 'course', StringLib::getClassNameFromString(Course::class)]);
                if (!$image['error']) {
                    $tranporter->image_detail = $image['fileName'];
                }
            }
            if (isset($request->video_intro)) {
                $file = Apiato::call('File@UploadFileAction', [$request, 'video_intro', '', 'course']);
                if (!$file['error']) {
                    $tranporter->video_intro = $file['fileName'];
                    $tranporter->video_intro_dir = $file['dir'];
                }
            }
            if (isset($request->icon_hover)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'icon_hover', 'icon-hover-course', StringLib::getClassNameFromString(Course::class)]);
                if (!$image['error']) {
                    $tranporter->icon_hover = $image['fileName'];
                }
            }

            if (isset($request->price)){
                $tranporter->price = intval(str_replace([',', '.'], '', $request->price));
            }
            if (isset($request->global_price)){
                $tranporter->global_price = intval(str_replace([',', '.'], '', $request->global_price));
            }
            if (isset($request->student)){
                $tranporter->student = intval(str_replace([',', '.'], '', $request->student));
            }

            $tranporter->status = isset($request['lesson']) && !empty($request['lesson']['item']) && !empty($request['lesson']['item'][0][0]['title']) ? $request->status : 1;


            $course = Apiato::call('Course@UpdateCourseAction', [$tranporter]);

            if ($course) {
                return redirect()->route('admin_course_edit_page', ['id' => $course->id])->with('status', 'Khóa Học đã được cập nhật');
            }
        } catch (\Exception $e) {
            throw $e;
            if (isset($images) && isset($image['error']))
              return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
            else
              return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . isset($image) && isset($image['msg']) ? $image['msg'] : '']);
        }
    }

    public function create(StoreCourseRequest $request)
    {
        try {
            $tranporter = new DataTransporter($request);

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'image-course', StringLib::getClassNameFromString(Course::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }

            if (isset($request->image_detail)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image_detail', 'course', StringLib::getClassNameFromString(Course::class)]);
                if (!$image['error']) {
                    $tranporter->image_detail = $image['fileName'];
                }
            }

            if (isset($request->video_intro)) {
                $file = Apiato::call('File@UploadFileAction', [$request, 'video_intro', '', 'course']);
                if (!$file['error']) {
                    $tranporter->video_intro = $file['fileName'];
                    $tranporter->video_intro_dir = $file['dir'];
                }
            }
            if (isset($request->icon_hover)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'icon_hover', 'icon-hover-course', StringLib::getClassNameFromString(Course::class)]);
                if (!$image['error']) {
                    $tranporter->image = $image['fileName'];
                }
            }

            if (isset($request->price)){
                $tranporter->price = intval(str_replace([',', '.'], '', $request->price));
            }
            if (isset($request->global_price)){
                $tranporter->global_price = intval(str_replace([',', '.'], '', $request->global_price));
            }

            $tranporter->status = 1;

            $course = Apiato::call('Course@CreateCourseAction', [$tranporter]);
            if ($course) {
                return redirect()->route('admin_course_edit_page', ['id' => $course->id])->with('status', 'Khóa Học đã được thêm mới. Bạn có thể chỉnh sửa khóa học');
            }
        } catch (\Exception $e) {
          if (isset($image['error']))
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
          else
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . @$image['msg']]);
        }
    }

    public function delete(FindCourseRequest $request)
    {
        try {
            Apiato::call('Course@DeleteCourseAction', [$request]);
        } catch (\Exception $e) {
            // throw $e;
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
        }
    }
    public function getAjaxCourse(GetAllCourseRequest $request)
    {
        $course = Apiato::call('Course@CourseListingAction', [$request->all(),['id' => 'desc'], 0, true,Apiato::call('Localization@GetDefaultLanguageAction'),[],$request->page ?? 1]);
        return FunctionLib::ajaxRespond(true, 'Hoàn thành', $course);
    }

    public function upload(Request $request){
        if (isset($request->file)) {

            $getID3 = new \getID3;
            $data_file = $getID3->analyze($request->file);

            $file = Apiato::call('File@UploadFileAction', [$request, 'file', '', 'course']);

            $dataCreate = [
                'auth_type' => 1,
                'auth_id' => auth()->guard('admin')->user()->id,
                'type' => $file['type'],
                'mine_type' => $data_file['mime_type'],
                'name' => $file['fileName'],
                'dir' => $file['dir'],
                'file_format' => $data_file['fileformat'],
                'size' => $data_file['avdataend'],
                'duration' => in_array($data_file['fileformat'], ['mp4', 'mp3']) ? $data_file['playtime_seconds'] : null,
                'playtime_string' => in_array($data_file['fileformat'], ['mp4', 'mp3']) ? $data_file['playtime_string'] : null,
            ];
            if ($file['error']) {
                return \FunctionLib::ajaxRespondV2(false, 'error', ['title' => 'Thất bại', 'mess' => $file['msg']]);
            }

            $data = app(CreateCourseLessonFileAction::class)->skipCache(true)->run(new DataTransporter($dataCreate));
            if ($data){
                return \FunctionLib::ajaxRespondV2(true, 'success', $this->transform($data, new CourseLessonFileTransformer()));
            }
            return \FunctionLib::ajaxRespondV2(false, 'error', ['title' => 'Thất bại', 'mess' => 'Upload File Thất bại']);
        }
    }

    public function fileList(Request $request){
        $filter = [];
        if (isset($request->name) && !empty($request->name)){
            $filter['name'] = $request->name;
        }
        if (isset($request->type) && !empty($request->type)){
            $filter['type'] = explode(',', $request->type);
        }
//        dd($filter);
        $data = app(GetLessonFileListAction::class)->skipCache(true)->run(
            $filter, 0, false, 1
        );
        return \FunctionLib::ajaxRespondV2(true, 'success', $this->transform($data, new CourseLessonFileTransformer()));
    }

    public function fileListQuiz(Request $request){
        $data = app(ExamListingAction::class)->skipCache(true)->run(
            new DataTransporter([]), ['id' => 'desc'], []
        );
        return \FunctionLib::ajaxRespondV2(true, 'success', $this->transform($data, new ListQuizTransformer()));
    }

}
