<?php

namespace App\Containers\Course\UI\WEB\Controllers\Admin\Features;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Course\UI\WEB\Requests\Admin\UpdateSomeStatusRequest;

trait UpdateSomeStatus
{
    public function updateSomeStatus(UpdateSomeStatusRequest $request) {
        $result = Apiato::call('Course@Admin\UpdateSomeStatusAction', [
            $request->field,
            $request->id,
            $request->status,
        ]);

        if($result) {
            return $this->sendResponse($result,'Success');
        }

        return $this->sendError('Không update được dữ liệu!');
    }
}
