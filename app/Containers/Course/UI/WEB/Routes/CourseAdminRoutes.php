<?php
Route::group(
    [
        'prefix' => 'course',
        'namespace' => '\App\Containers\Course\UI\WEB\Controllers\Admin',
        'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
        'middleware' => [
            'auth:admin',
        ],
    ],
    function () use ($router) {
        $router->get('/', [
            'as'   => 'admin_course_home_page',
            'uses' => 'Controller@index',
        ]);

        $router->get('/edit/{id}', [
            'as'   => 'admin_course_edit_page',
            'uses' => 'Controller@edit',
        ]);

        $router->post('/edit/{id}', [
            'as'   => 'admin_course_edit_page',
            'uses' => 'Controller@update',
        ]);

        $router->get('/add', [
            'as'   => 'admin_course_add_page',
            'uses' => 'Controller@add',
        ]);

        $router->post('/add', [
            'as'   => 'admin_course_add_page',
            'uses' => 'Controller@create',
        ]);

        $router->delete('/{id}', [
            'as' => 'admin_course_delete',
            'uses'       => 'Controller@delete',
        ]);

        $router->post('/status/{field}', [
            'as'   => 'admin_course_change_status',
            'uses' => 'Controller@updateSomeStatus',
        ]);
        $router->get('/getAjaxCourse', [
            'as'   => 'get_ajax_course',
            'uses' => 'Controller@getAjaxCourse',
        ]);
        $router->post('/upload', [
            'as'   => 'admin_course_upload',
            'uses' => 'Controller@upload',
        ]);
        $router->get('/load-file', [
            'as'   => 'admin_list_file',
            'uses' => 'Controller@fileList',
        ]);
        $router->get('/load-quiz', [
            'as'   => 'admin_list_quiz',
            'uses' => 'Controller@fileListQuiz',
        ]);
    }
);
