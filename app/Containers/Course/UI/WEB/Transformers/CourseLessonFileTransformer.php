<?php

namespace App\Containers\Course\UI\WEB\Transformers;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Course\Models\Course;
use App\Containers\Course\Models\CourseLessonFile;
use App\Ship\Parents\Transformers\Transformer;

class CourseLessonFileTransformer extends Transformer
{
    /**
     * @param $token
     *
     * @return  array
     */
    public function transform(CourseLessonFile $data)
    {
        $response = [
            'id' => $data->id,
            'name' => $data->name,
            'type' => $data->type,
            'mine_type' => $data->mine_type,
            'size' => \FunctionLib::formatBytes($data->size, 2),
            'created_at' => FunctionLib::dateFormat($data->created_at, 'd/m/Y H:i')
        ];
        return $response;
    }
}
