<?php

namespace App\Containers\Course\UI\WEB\Transformers;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Course\Models\Course;
use App\Containers\Course\Models\CourseLessonFile;
use App\Containers\Exam\Models\Exam;
use App\Ship\Parents\Transformers\Transformer;

class ListQuizTransformer extends Transformer
{
    /**
     * @param $token
     *
     * @return  array
     */
    public function transform(Exam $data)
    {
        $response = [
            'id' => $data->id,
            'name' => $data->title,
            'created_at' => FunctionLib::dateFormat($data->created_at, 'd/m/Y H:i')
        ];
        return $response;
    }
}
