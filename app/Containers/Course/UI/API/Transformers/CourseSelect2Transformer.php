<?php

namespace App\Containers\Course\UI\API\Transformers;

use App\Ship\Parents\Transformers\Transformer;
use Illuminate\Support\Facades\Config;

/**
 * Class CourseSelect2Transformer.
 *
 */
class CourseSelect2Transformer extends Transformer
{

    /**
     * @param $token
     *
     * @return  array
     */
    public function transform($data)
    {
        $response = [
            'id' => $data->id,
            'text' => html_entity_decode($data->desc->name),
        ];

        return $response;
    }
}
