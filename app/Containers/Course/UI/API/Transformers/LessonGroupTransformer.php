<?php

namespace App\Containers\Course\UI\API\Transformers;

use App\Containers\Course\Models\CourseLessonGroup;
use App\Ship\Parents\Transformers\Transformer;

class LessonGroupTransformer extends Transformer
{
    /**
     * @param $token
     *
     * @return  array
     */
    public function transform(CourseLessonGroup $data)
    {
        $response = [
            'id' => $data->id,
            'title' => $data->title,
            'item' => $data->relationLoaded('course_item_group') ? $data->course_item_group : $this->null(),
        ];
        return $response;
    }
}
