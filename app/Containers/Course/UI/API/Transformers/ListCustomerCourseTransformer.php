<?php

namespace App\Containers\Course\UI\API\Transformers;

use App\Containers\Customer\Models\Customer;
use App\Ship\Parents\Transformers\Transformer;
use App\Containers\Product\UI\API\Transformers\FrontEnd\ProductListTransformer;
use Apiato\Core\Foundation\Facades\ImageURL;
use Apiato\Core\Foundation\FunctionLib;
use Carbon\Carbon;

class ListCustomerCourseTransformer extends Transformer
{
    public function transform(Customer $data)
    {
        $response = [
            'id' => $data->id,
            'fullname' => $data->fullname,
            'email' => $data->email,
            'phone' => $data->phone,
            'avatar' => \ImageURL::getImageUrl($data->avatar, 'customer', '70x70'),
            'date_of_birth' => FunctionLib::dateFormat($data->date_of_birth),
            'is_active' => $data->relationLoaded('customerCourse') ? $data->customerCourse->is_active : $this->null(),
            'active' => $data->relationLoaded('customerCourse') ? ($data->customerCourse->is_active == 0 ? 'Chưa kích hoạt' : 'Đã kích hoạt') : $this->null(),
            'start_time' => $data->relationLoaded('customerCourse') ? ($data->customerCourse->is_active == 1 ? FunctionLib::dateFormat($data->customerCourse->start_time, 'd/m/Y H:i') : '---' ) : $this->null(),
            'end_time' => $data->relationLoaded('customerCourse') ? ($data->customerCourse->is_active == 1 ? (empty($data->customerCourse->available_time) || $data->customerCourse->available_time == 0 ? 'Vô thời hạn' : FunctionLib::dateFormat(date('Y-m-d H:i:s', strtotime('+'.$data->customerCourse->available_time.' day', strtotime($data->customerCourse->start_time))), 'd/m/Y H:i')) : '---' ) : $this->null(),
            'percent' => $data->relationLoaded('customerCourse') ? ( (int)$data->customerCourse->percent_view > 100 ? 100 : round($data->customerCourse->percent_view) )  : 0,
        ];

        return $response;
    }
}
