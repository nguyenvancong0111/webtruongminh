<?php

namespace App\Containers\Course\UI\API\Transformers;

use App\Containers\Course\Models\Course;
use App\Ship\Parents\Transformers\Transformer;

class CourseOtherTransformer extends Transformer
{
    /**
     * @param $token
     *
     * @return  array
     */
    public function transform(Course $data)
    {
        $response = [
            'id' => $data->id,
            'image' => $data->getImageUrl('40x40'),
            'link' => $data->link(),
            'desc' => $data->relationLoaded('desc') ? $data->desc : $this->null(),
            'rate' => (int)$data->getAvgRatingCourse()
        ];
        return $response;
    }
}
