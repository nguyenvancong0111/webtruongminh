<?php

namespace App\Containers\Course\UI\API\Controllers;

use Apiato\Core\Abstracts\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\FunctionLib;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use App\Containers\Category\Actions\FrontEnd\GetAllCateChillByIDAction;
use App\Containers\Category\UI\API\Transformers\AllCategoriesTransformer;
use App\Containers\Category\UI\API\Transformers\CategoriesSelect2Transformer;
use App\Containers\Course\Actions\Admin\GetLessonFileListAction;
use App\Containers\Course\Actions\CreateCourseLessonFileAction;
use App\Containers\Course\UI\API\Transformers\CourseSelect2Transformer;
use App\Containers\Course\Actions\Admin\FindCourseByIdAction;
use App\Containers\Course\Actions\CourseListingAction;
use App\Containers\Course\Actions\FrontEnd\FindLessonByCourseIdAction;
use App\Containers\Course\Actions\FrontEnd\GetListCustomerCourseAction;
use App\Containers\Course\UI\API\Requests\GetCourseAllRequest;
use App\Containers\Course\UI\API\Requests\GetCourseByCategoryRequest;
use App\Containers\Course\UI\API\Requests\GetCourseByKeywordRequest;
use App\Containers\Course\UI\API\Requests\GetCourseOtherRequest;
use App\Containers\Course\UI\API\Requests\GetLessonByCourseRequest;
use App\Containers\Course\UI\API\Requests\GetListCustomerRequest;
use App\Containers\Course\UI\API\Requests\UpdateCourseRequest;
use App\Containers\Course\UI\API\Requests\UpdateCustomerViewLessonRequest;
use App\Containers\Course\UI\API\Transformers\CourseOtherTransformer;
use App\Containers\Course\UI\API\Transformers\LessonGroupTransformer;
use App\Containers\Course\UI\API\Transformers\ListCustomerCourseTransformer;
use App\Containers\Course\UI\WEB\Transformers\CourseLessonFileTransformer;
use App\Containers\Course\UI\WEB\Transformers\ListQuizTransformer;
use App\Containers\Exam\Actions\ExamListingAction;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Response;

/**
 * Class Controller.
 */
class Controller extends BaseApiFrontController
{
    public function updateCourse(UpdateCourseRequest $request) {
        $tranporter = new DataTransporter($request);
        $course = Apiato::call('Course@UpdateCourseAction', [$tranporter]);

        if ($course) {
            return FunctionLib::ajaxRespondV2(true,'Success');
        }
        return FunctionLib::ajaxRespondV2(false, 'Fail',[],Response::HTTP_NOT_ACCEPTABLE);
    }

    public function getCourseByCategory(GetCourseByCategoryRequest $request){
        $html = '';
        if(isset($request->category) && !empty($request->category)){
            $arr_cate_item = app(GetAllCateChillByIDAction::class)->skipCache(true)->run([(int)$request->category]);
            if(!empty($arr_cate_item)){
                $course = app(CourseListingAction::class)->skipCache(true)->run(
                    ['status' => 2, 'is_home' => 1, 'cat_id' => $arr_cate_item], ['sort_order' => 'asc','id' => 'desc'], 8, true, $this->currentLang, [], 1
                );
                $html = \View::make('ubacademy::pc.home.components.list-course',['data' => $course] )->render();
            }
        }
        return FunctionLib::ajaxRespondV2(true,'Success', $html);
    }

    public function getLessonByCourse(GetLessonByCourseRequest $request){
        $lesson_group = app(FindLessonByCourseIdAction::class)->skipCache(true)->run($request->id, []);
        return $this->transform($lesson_group, new LessonGroupTransformer());
    }

    public function getCourseOther(GetCourseOtherRequest $request){
        $course = app(CourseListingAction::class)->skipCache(true)->run(
            ['status' => 2, 'reject' => isset($request->reject) && !empty($request->reject) ? [(int)$request->reject] : [], 'cat_id' => isset($request->cate) && !empty($request->cate) ? $request->cate : ''], ['sort_order' => 'ASC','id' => 'desc'], 10, true, 1, [], 1
        );
        return $this->transform($course, new CourseOtherTransformer());
    }

    function listCustomer(GetListCustomerRequest $request){
        $customer = app(GetListCustomerCourseAction::class)->skipCache(true)->run(['status' => 2, 'courseID' => $request->object], false, 1, isset($request->page) && !empty($request->page) ? $request->page : 1);
//        dd($customer);
        return $this->transform($customer, new ListCustomerCourseTransformer());
    }

    public function getCourseByKeyword(GetCourseByKeywordRequest $request){
        $course = app(CourseListingAction::class)->skipCache(true)->run(
            ['status' => 2, 'name' => trim($request->keyword)], ['sort_order' => 'desc', 'id' => 'desc'], 12, false, $this->currentLang, [], isset($request->page) && !empty($request->page) ? $request->page : 1
        );
        $html = \View('ubacademy::pc.search.components.search_data', ['data' => $course])->render();
        return FunctionLib::ajaxRespondV2(true,'Success', $html);
    }

    public function updateCustomerViewLesson(UpdateCustomerViewLessonRequest $request){
        dd($request->all());
    }
    public  function getCourseAll(GetCourseAllRequest $request){
        $filter = [
            'status' => 2,
            'type' => 0
        ];
        if (isset($request->name) && !empty($request->name)){
            $filter['name'] = $request->name;
        }
        $course = app(CourseListingAction::class)->skipCache(true)->run(
            $filter, ['created_at' => 'desc','id' => 'desc'], 0, true, $this->currentLang, [], 1
        );

        $type = $request->type;
        if($type && $type == 'select2') {
            return $this->transform($course, CourseSelect2Transformer::class, [], [], 'course');
        }else {
            return $this->transform($course, AllCategoriesTransformer::class,[],[],'category');
        }
    }


    public function fileList(\Illuminate\Http\Request $request){
        $filter = [];
        if (isset($request->name) && !empty($request->name)){
            $filter['name'] = $request->name;
        }
        if (isset($request->type) && !empty($request->type)){
            $filter['type'] = explode(',', $request->type);
        }

        $filter['auth_type'] = 2;
        $filter['auth_id'] = \Auth::guard('lecturers')->user()->id;

//        dd($filter);
        $data = app(GetLessonFileListAction::class)->skipCache(true)->run(
            $filter, 0, false, 1
        );
        return \FunctionLib::ajaxRespondV2(true, 'success', $this->transform($data, new CourseLessonFileTransformer()));
    }

    public function fileListQuiz(\Illuminate\Http\Request $request){
        $data = app(ExamListingAction::class)->skipCache(true)->run(
            new DataTransporter([]), ['id' => 'desc'], []
        );
        return \FunctionLib::ajaxRespondV2(true, 'success', $this->transform($data, new ListQuizTransformer()));
    }
    public function upload(\Illuminate\Http\Request $request){
        if (isset($request->file)) {

            $getID3 = new \getID3;
            $data_file = $getID3->analyze($request->file);

            $file = Apiato::call('File@UploadFileAction', [$request, 'file', '', 'course']);

            $dataCreate = [
                'auth_type' => 2,
                'auth_id' => auth()->guard('lecturers')->user()->id,
                'type' => $file['type'],
                'mine_type' => $data_file['mime_type'],
                'dir' => $file['dir'],
                'name' => $file['fileName'],
                'file_format' => $data_file['fileformat'],
                'size' => $data_file['avdataend'],
                'duration' => in_array($data_file['fileformat'], ['mp4', 'mp3']) ? $data_file['playtime_seconds'] : null,
                'playtime_string' => in_array($data_file['fileformat'], ['mp4', 'mp3']) ? $data_file['playtime_string'] : null,
            ];
            if ($file['error']) {
                return \FunctionLib::ajaxRespondV2(false, 'error', ['title' => 'Thất bại', 'mess' => $file['msg']]);
            }

            $data = app(CreateCourseLessonFileAction::class)->skipCache(true)->run(new DataTransporter($dataCreate));
            if ($data){
                return \FunctionLib::ajaxRespondV2(true, 'success', $this->transform($data, new CourseLessonFileTransformer()));
            }
            return \FunctionLib::ajaxRespondV2(false, 'error', ['title' => 'Thất bại', 'mess' => 'Upload File Thất bại']);
        }
    }


}

