<?php

Route::group(
    [
        'middleware' => [
             'api',
        ],
        'prefix' => '/course-lesson-view',
    ],
    function () use ($router) {
        $router->any('/update-percent-lesson', [
            'as' => 'api_customer_courser_lesson_percent',
            'uses'       => 'Controller@updateCustomerViewLesson'
        ]);

    });