<?php

$router->post('course/updateCourse', [
    'as' => 'api_course_edit',
    'uses'       => 'Controller@updateCourse',
    'middleware' => [
        'auth:api',
    ],
]);
