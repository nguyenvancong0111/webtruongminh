<?php

Route::group(
    [
        'middleware' => [
            'api',
        ],
        'prefix' => '/course',
    ],
    function () use ($router) {
        $router->any('/lesson', [
            'as' => 'api_lesson_course',
            'uses' => 'Controller@getLessonByCourse'
        ]);
        $router->any('/other', [
            'as' => 'api_course_other',
            'uses' => 'Controller@getCourseOther'
        ]);
        $router->get('/load-file', [
            'as'   => 'api_list_file',
            'uses' => 'Controller@fileList',
        ]);
        $router->get('/load-quiz', [
            'as'   => 'api_list_quiz',
            'uses' => 'Controller@fileListQuiz',
        ]);
        $router->post('/upload', [
            'as'   => 'api_course_upload',
            'uses' => 'Controller@upload',
        ]);
    });