<?php

Route::group(
    [
        'middleware' => [
             'api',
        ],
        'prefix' => '/course',
    ],
    function () use ($router) {
        $router->any('/category', [
            'as' => 'api_course_category',
            'uses'       => 'Controller@getCourseByCategory'
        ]);
        $router->any('/search', [
            'as' => 'api_course_search',
            'uses'       => 'Controller@getCourseByKeyword'
        ]);
        $router->any('/list', [
            'as' => 'api_course_get_all',
            'uses'       => 'Controller@getCourseAll'
        ]);

    });