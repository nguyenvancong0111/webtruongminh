<?php

use Apiato\Core\Foundation\Facades\Apiato;

Route::group(
[
    'middleware' => [
//        'auth:api-customer'
    ],
    'prefix' => '/list-customer',
],
function () use ($router) {
    $router->any('/', [
        'as' => 'api_list_customer',
        'uses'       => 'Controller@listCustomer'
    ]);
});