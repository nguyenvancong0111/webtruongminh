<?php

namespace App\Containers\Course\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class CreateNewsAction.
 *
 */
class CreateCourseAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $course = Apiato::call('Course@CreateCourseTask', [$data]);
        if ($course) {
            Apiato::call('Course@SaveCourseDescTask', [$data, [], $course->id]);
            if(isset($data->toArray()['lesson'])){
                Apiato::call('Course@SaveCourseLessonTask', [$data, [], $course->id]);
            }
        }

        $this->clearCache();

        return $course;
    }
}
