<?php

namespace App\Containers\Course\Actions\FrontEnd;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetListCustomerCourseAction extends Action
{
    public function run(array $filters = [], bool $skipPaginate = true, int $limit = 10, int $currentPage = 1, array $with = [])
    {
        $customer = Apiato::call('Course@FrontEnd\GetListCustomerCourseTask', [$filters, $skipPaginate, $limit, $currentPage], [[$with]]);

        return $customer;
    }
}
