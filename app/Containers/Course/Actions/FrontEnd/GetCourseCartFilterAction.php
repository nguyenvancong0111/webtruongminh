<?php

namespace App\Containers\Course\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetCourseCartFilterAction extends Action
{
    public function run(array $filters = []): iterable {
        return $this->remember(function () use ($filters) {
            return $this->call('Course@FrontEnd\GetCourseCartFilterTask', [$filters]);
        }, null, [5]);
    }
}
