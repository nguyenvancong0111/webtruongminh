<?php

namespace App\Containers\Course\Actions\FrontEnd;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class FindCustomerLessonViewAction.
 *
 */
class FindCustomerLessonViewAction extends Action
{

    /**
     * @return mixed
     */
    public function run($filters)
    {
        $data = Apiato::call('Course@FrontEnd\FindCustomerLessonViewTask', [$filters]);
        return $data;
    }
}
