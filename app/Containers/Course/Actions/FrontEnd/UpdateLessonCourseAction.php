<?php

namespace App\Containers\Course\Actions\FrontEnd;

use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\ImageURL;
use App\Ship\Parents\Actions\Action;
use Carbon\Carbon;

class UpdateLessonCourseAction extends Action
{
    public function run(array $filtes = [], array $dataUpdate = [])
    {
        $customer_course = $this->call('Course@FrontEnd\UpdateLessonCourseTask', [$filtes, $dataUpdate]);
        return $customer_course;
    }
}