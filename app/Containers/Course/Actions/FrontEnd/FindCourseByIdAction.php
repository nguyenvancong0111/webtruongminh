<?php

namespace App\Containers\Course\Actions\FrontEnd;

use App\Containers\Course\Tasks\FindCourseByIdTask;
use App\Containers\Localization\Models\Language;
use App\Containers\News\Models\News;
use App\Containers\News\Tasks\GetNewsBySlugIdTask;
use App\Containers\Course\Models\Course;
use App\Containers\Course\Tasks\GetCourseBySlugIdTask;
use App\Ship\Parents\Actions\Action;

class FindCourseByIdAction extends Action
{
    protected $findCourseByIdTask;
    public function __construct(FindCourseByIdTask $findCourseByIdTask)
    {
        parent::__construct();
        $this->findCourseByIdTask = $findCourseByIdTask;
    }
    public function run(int $courseId, array $with = []): ?Course
    {
        return $this->findCourseByIdTask->run($courseId, $with);
    }
}
