<?php

namespace App\Containers\Course\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetCourseListAction extends Action
{
    public function run(
        array $filters = [],
        array $orderBy = ['created_at' => 'desc', 'id' => 'desc'],
        int $limit = 20,
        bool $skipPagination = false,
        Language $currentLang = null,
        array $externalData = [],
        int $currentPage = 1
    ): iterable {
        return $this->remember(function () use ($filters, $orderBy, $limit, $skipPagination, $currentLang, $externalData, $currentPage) {
            return $this->call('Course@FrontEnd\GetCourseListTask', [
                $filters,
                $orderBy,
                $limit,
                $skipPagination,
                $currentLang,
                $externalData,
                $currentPage
            ]);
        }, null, [5]);
    }
}
