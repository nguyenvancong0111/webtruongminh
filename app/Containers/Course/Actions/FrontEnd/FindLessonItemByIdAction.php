<?php

namespace App\Containers\Course\Actions\FrontEnd;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class FindNewsByIdAction.
 *
 */
class FindLessonItemByIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($id)
    {
        $data = Apiato::call('Course@FrontEnd\FindLessonItemByIdTask', [$id]);
        return $data;
    }
}
