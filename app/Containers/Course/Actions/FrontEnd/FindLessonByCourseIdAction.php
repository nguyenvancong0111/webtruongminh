<?php

namespace App\Containers\Course\Actions\FrontEnd;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class FindNewsByIdAction.
 *
 */
class FindLessonByCourseIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($course_id, $withData = [])
    {
        $data = Apiato::call('Course@FrontEnd\FindLessonByCourseIdTask', [
            $course_id, $external_data = ['with_relationship' => array_merge([], $withData)]
        ]);
        return $data;
    }
}
