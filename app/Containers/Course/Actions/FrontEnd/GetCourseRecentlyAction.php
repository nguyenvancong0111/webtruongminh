<?php

namespace App\Containers\Course\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetCourseRecentlyAction extends Action
{
    public function run($type, array $filter = [], int $limit = 20, bool $isPanination = false, Language $currentLang = null, array $orderBy = ['created_at' => 'desc', 'id' => 'desc']): iterable {
//        return $this->remember(function () use ($type, $filter, $limit, $isPanination, $currentLang,$orderBy) {
            return $this->call('Course@FrontEnd\GetCourseRecentlyTask', [
                $type,
                $filter,
                $limit,
                $isPanination,
                $currentLang,
                $orderBy

            ]);
//        }, null, [5]);
    }
}
