<?php

namespace App\Containers\Course\Actions\FrontEnd;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class FindCourseLessonFileByIdAction.
 *
 */
class FindCourseLessonFileByIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($id)
    {
        $data = Apiato::call('Course@FrontEnd\FindCourseLessonFileByIdTask', [$id]);
        return $data;
    }
}
