<?php

namespace App\Containers\Course\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\News\Models\News;
use App\Containers\News\Tasks\GetNewsBySlugIdTask;
use App\Containers\Course\Models\Course;
use App\Containers\Course\Tasks\GetCourseBySlugIdTask;
use App\Ship\Parents\Actions\Action;

class GetCourseBySlugIdAction extends Action
{
    protected $getCourseBySlugIdTask;
    public function __construct(GetCourseBySlugIdTask $getCourseBySlugIdTask)
    {
        parent::__construct();
        $this->getCourseBySlugIdTask = $getCourseBySlugIdTask;
    }
    public function run(int $courseId, string $slug, Language $currentLang = null): ?Course
    {
//        return $this->remember(function () use ($courseId, $slug, $currentLang) {
            return $this->getCourseBySlugIdTask->run($courseId, $slug, $currentLang);
//        });
    }
}
