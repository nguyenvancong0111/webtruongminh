<?php

namespace App\Containers\Course\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\Course\Models\Course;
use App\Containers\Course\Tasks\FrontEnd\GetCourseListTask;
use App\Ship\Parents\Actions\Action;

class GetCourseByArrIdAction extends Action
{
    protected $getCourseListTask;
    public function __construct(GetCourseListTask $getCourseListTask)
    {
        parent::__construct();
        $this->getCourseListTask = $getCourseListTask;
    }
    public function run(array $filters = [], array $orderBy = ['created_at' => 'desc', 'id' => 'desc'], int $limit = 20, bool $skipPagination = false, Language $currentLang = null, array $externalData = [], int $currentPage = 1)
    {
//        return $this->remember(function () use ($filters, $orderBy, $limit, $skipPagination, $currentLang, $externalData, $currentPage) {
            return $this->getCourseListTask->run($filters, $orderBy, $limit, $skipPagination, $currentLang, $externalData, $currentPage);
//        });
    }
}
