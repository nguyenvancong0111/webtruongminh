<?php

namespace App\Containers\Course\Actions\FrontEnd;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class UpdateCustomerLessonViewAction.
 *
 */
class UpdateCustomerLessonViewAction extends Action
{

    /**
     * @return mixed
     */
    public function run($filters, $value)
    {
        $data = Apiato::call('Course@FrontEnd\UpdateCustomerLessonViewTask', [$filters, $value]);
        $this->clearCache();
        return $data;
    }
}
