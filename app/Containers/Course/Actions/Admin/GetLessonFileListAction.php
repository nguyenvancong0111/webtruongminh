<?php

namespace App\Containers\Course\Actions\Admin;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetLessonFileListAction extends Action
{
    public function run(array $filters = [], int $limit = 20, bool $skipPagination = false, int $currentPage = 1): iterable {
//        return $this->remember(function () use ($filters, $limit, $skipPagination, $currentPage) {
            return $this->call('Course@Admin\GetLessonFileListTask', [$filters, $limit, $skipPagination, $currentPage
            ]);
//        }, null, [5]);
    }
}
