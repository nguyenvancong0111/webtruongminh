<?php

namespace App\Containers\Course\Actions\Admin;

use App\Ship\Parents\Actions\Action;


class UpdateSomeStatusAction extends Action
{
    public function run(string $field,int $id, int $status) :? bool
    {
        $result = $this->call('Course@Admin\UpdateSomeStatusTask',[
            $field,
            $id,
            $status
        ]);

        $this->clearCache();

        return $result;
    }
}
