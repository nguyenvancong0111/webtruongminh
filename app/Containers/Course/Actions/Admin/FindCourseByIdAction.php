<?php

namespace App\Containers\Course\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class FindNewsByIdAction.
 *
 */
class FindCourseByIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($course_id, $withData = [])
    {

        $data = Apiato::call('Course@FindCourseByIdTask', [
            $course_id,
            $external_data = array_merge(['all_desc'], $withData)
        ]);

        if ($data) {
            $data->setRelation('all_desc', $data->all_desc->keyBy('language_id'));
        }

        return $data;
    }
}
