<?php

namespace App\Containers\Course\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class CreateCourseLessonFileAction.
 *
 */
class CreateCourseLessonFileAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $course = Apiato::call('Course@CreateCourseLessonCourseTask', [$data]);

        $this->clearCache();

        return $course;
    }
}
