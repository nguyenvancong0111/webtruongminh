<?php

namespace App\Containers\Course\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class DeleteCourseAction.
 *
 */
class DeleteCourseAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        Apiato::call('Course@DeleteCourseTask', [$data->id]);
        // Apiato::call('News@DeleteNewsDescTask', [$data->id]);

        $this->clearCache();
    }
}
