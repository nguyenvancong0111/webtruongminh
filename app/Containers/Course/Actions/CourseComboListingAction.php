<?php

namespace App\Containers\Course\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class CourseComboListingAction.
 *
 */
class CourseComboListingAction extends Action
{

    /**
     * @return mixed
     */
    public function run($combo_id = 0)
    {
        $data = Apiato::call(
            'Course@CourseComboListingTask', [$combo_id]
        );

        return $data;
    }
}
