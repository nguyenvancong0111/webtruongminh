<?php

namespace App\Containers\Course\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Course\Tasks\Admin\FindLessonItemBySessionTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Str;

/**
 * Class UpdateCourseAction.
 *
 */
class UpdateCourseAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $course = Apiato::call(
            'Course@SaveCourseTask',
            [
                $data
            ]
        );

        if($course) {
            $session = $course->id.'_'.strtotime(now());
            $original_desc = Apiato::call('Course@GetAllCourseDescTask', [$course->id]);
            Apiato::call('Course@SaveCourseDescTask', [$data, $original_desc, $course->id]);

            if ($course->type == 0){
                if ($course->option_gr != 0){
                    $original_course_option = Apiato::call('Course@GetAllOptionCourseByIdTask', [$course->id]);
                    Apiato::call('Course@SaveCourseOptionTask', [$data, $original_course_option, $course->id]);
                }
                $original_course = Apiato::call('Course@GetAllLessonByCourseIdTask', [$course->id]);
                Apiato::call('Course@SaveCourseLessonTask', [$data, $original_course, $course->id, $session]);

                $lesson_session = app(FindLessonItemBySessionTask::class)->run($session);
                if ($lesson_session->isNotEmpty()){
                    $lesson_attached = [];
                    $lesson_quiz = [];
                    $arr_lesson_remove = [];
                    foreach ($lesson_session as $itm){
                        $arr_lesson_remove[] = $itm->id;
                        if(!empty($itm->quiz)){
                            $lesson_quiz[] = ['course_id' => $course->id, 'exam_id' => $itm->quiz];
                        }
                        if (!empty($itm->attached)){
                            $attached = explode(',', $itm->attached);
                            foreach ($attached as $i){
                                $lesson_attached[] = ['lesson_id' => $itm->id, 'file_id' => $i];
                            }
                        }
                    }
                    if (!empty($arr_lesson_remove)){
                        Apiato::call('Course@DeleteLessonAttachedTask',[$arr_lesson_remove]);
                    }
                    if (!empty($lesson_attached)){
                        Apiato::call('Course@SaveLessonAttachedTask',[$lesson_attached]);
                    }

                    Apiato::call('Course@DeleteCourseExamTask',[$course->id]);
                    if (!empty($lesson_quiz)){
                        Apiato::call('Course@SaveCourseExamTask',[$lesson_quiz]);
                    }
                }
            }elseif ($course->type == 1){
                Apiato::call('Course@SaveCourseComboTask',[$data, $course]);
            }




        }

        $this->clearCache();

        return $course;
    }
}
