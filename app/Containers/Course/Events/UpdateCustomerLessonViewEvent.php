<?php

namespace App\Containers\Course\Events;

use Apiato\Core\Abstracts\Events\Interfaces\ShouldHandleNow;
use App\Containers\Authentication\Models\PasswordReset;
use App\Containers\Customer\Models\Customer;
use App\Containers\Settings\Actions\GetAllSettingsAction;
use App\Ship\Parents\Events\Event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class UpdateCustomerLessonViewEvent extends Event implements ShouldQueue
{
    use SerializesModels;

    public $customer_id;
    public $course_id;
    public $lesson_id;
    public $percent;
    public $time_view;


    public function __construct($customer_id, $course_id, $lesson_id, $percent, $time_view)
    {
        $this->customer_id = $customer_id;
        $this->course_id = $course_id;
        $this->lesson_id = $lesson_id;
        $this->percent = $percent;
        $this->time_view = $time_view;

    }

    public function handle()
    {
        // Do some handling here
    }

    public function broadcastOn()
    {
        return [];
    }
}
