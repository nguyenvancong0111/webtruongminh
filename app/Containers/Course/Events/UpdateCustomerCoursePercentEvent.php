<?php

namespace App\Containers\Course\Events;

use Apiato\Core\Abstracts\Events\Interfaces\ShouldHandleNow;
use App\Containers\Authentication\Models\PasswordReset;
use App\Containers\Customer\Models\Customer;
use App\Containers\Settings\Actions\GetAllSettingsAction;
use App\Ship\Parents\Events\Event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class UpdateCustomerCoursePercentEvent extends Event implements ShouldQueue
{
    use SerializesModels;

    public $customer_id;
    public $course_id;


    public function __construct($customer_id, $course_id)
    {
        $this->customer_id = $customer_id;
        $this->course_id = $course_id;
    }

    public function handle()
    {
        // Do some handling here
    }

    public function broadcastOn()
    {
        return [];
    }
}
