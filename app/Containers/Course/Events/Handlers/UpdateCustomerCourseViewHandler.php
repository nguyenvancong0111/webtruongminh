<?php

namespace App\Containers\Course\Events\Handlers;

use App\Containers\BaseContainer\Events\Handlers\BaseFrontEventHandler;
use App\Containers\Course\Actions\FrontEnd\FindCustomerLessonViewByFilterAction;
use App\Containers\Course\Actions\FrontEnd\GetCustomerLessonViewAction;
use App\Containers\Course\Actions\FrontEnd\UpdateCustomerLessonViewAction;
use App\Containers\Course\Events\UpdateCustomerCoursePercentEvent;
use App\Containers\Course\Events\UpdateCustomerLessonViewEvent;
use App\Containers\Customer\Actions\FindCustomerCourseAction;
use App\Containers\Customer\Actions\UpdateCustomerCourseAction;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateCustomerCourseViewHandler extends BaseFrontEventHandler implements ShouldQueue
{
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(UpdateCustomerCoursePercentEvent $event)
    {
        if ($event->customer_id != 0){
            $customer_lesson = app(GetCustomerLessonViewAction::class)->skipCache()->run(
                ['customer_id' => (int)$event->customer_id, 'course_id' => (int)$event->course_id]
            );

            $customer_course = app(FindCustomerCourseAction::class)->skipCache(true)->run(
                ['customer_id' => (int)$event->customer_id, 'course_id' => (int)$event->course_id, 'is_active' => 1]
            );
            if(!empty($customer_course)){
                $sum_percent = $customer_lesson->isNotEmpty() ? array_sum(array_column($customer_lesson->toArray(), 'percent')) : 0;
                app(UpdateCustomerCourseAction::class)->skipCache(true)->run(
                    ['id' => $customer_course->id, 'customer_id' => $event->customer_id, 'course_id' => $event->course_id], ['percent_view' => $sum_percent]
                );
            }
        }
    }
}
