<?php

namespace App\Containers\Course\Events\Handlers;

use App\Containers\BaseContainer\Events\Handlers\BaseFrontEventHandler;
use App\Containers\Course\Actions\FrontEnd\FindCustomerLessonViewByFilterAction;
use App\Containers\Course\Actions\FrontEnd\UpdateCustomerLessonViewAction;
use App\Containers\Course\Events\UpdateCustomerLessonViewEvent;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateCustomerLessonViewHandler extends BaseFrontEventHandler implements ShouldQueue
{
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(UpdateCustomerLessonViewEvent $event)
    {
        if ($event->customer_id != 0){
            //update Lesson
            app(UpdateCustomerLessonViewAction::class)->skipCache(true)->run(
                ['customer_id' => (int)$event->customer_id, 'course_id' => (int)$event->course_id, 'lesson_id' => $event->lesson_id], ['percent' => $event->percent, 'time_view' => $event->time_view]
            );
        }
    }
}
