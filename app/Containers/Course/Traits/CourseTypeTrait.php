<?php


namespace App\Containers\Course\Traits;

use App\Containers\Order\Enums\OrderStatus;
use App\Containers\Course\Enums\CourseType;
use App\Containers\Settings\Enums\PaymentStatus;

trait CourseTypeTrait
{
  public function isOnline(): bool
  {
    return $this->type == CourseType::ONLINE;
  }

  public function isOffile(): bool
  {
    return $this->type == CourseType::OFFLINE;
  }

  public function getCourseTypeText(): string
  {
    $text = isset(CourseType::TEXT[$this->type]) ? CourseType::TEXT[$this->type] : '';
    return $text ? $text : 'Không xác định';
  }
} // End class
