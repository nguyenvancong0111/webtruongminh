<?php

namespace App\Containers\Course\Traits;

use App\Containers\Order\Enums\OrderStatus;
use App\Containers\Course\Enums\CourseForm;
use App\Containers\Course\Enums\CourseType;
use App\Containers\Settings\Enums\PaymentStatus;

trait CourseFormTrait
{
  public function isHour(): bool
  {
    return $this->type == CourseForm::HOUR;
  }

  public function isCase(): bool
  {
    return $this->type == CourseForm::CASE;
  }

  public function getCourseFormText(): string
  {
    $text = isset(CourseForm::TEXT[$this->type]) ? CourseForm::TEXT[$this->type] : '';
    return $text ? $text : 'Không xác định';
  }

    public function getPopupForm(): string
    {
        if ($this->isHour()) {
            return 'popupTimeline';
        } elseif ($this->isCase()) {
            return 'popupTimelineCase';
        }
        else {
            return 'text-light';
        }
    }
} // End class
