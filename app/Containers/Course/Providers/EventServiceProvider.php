<?php

namespace App\Containers\Course\Providers;

use App\Containers\Course\Events\Handlers\UpdateCustomerCourseVideoAudioViewHandler;
use App\Containers\Course\Events\Handlers\UpdateCustomerCourseViewHandler;
use App\Containers\Course\Events\Handlers\UpdateCustomerLessonViewHandler;
use App\Containers\Course\Events\UpdateCustomerCoursePercentEvent;
use App\Containers\Course\Events\UpdateCustomerCourseViewEvent;
use App\Containers\Course\Events\UpdateCustomerLessonViewEvent;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        UpdateCustomerCoursePercentEvent::class => [
            UpdateCustomerCourseViewHandler::class
        ],
        /*UpdateCustomerCourseViewEvent::class => [
            UpdateCustomerCourseVideoAudioViewHandler::class
        ]*/
    ];

    public function boot() {
        parent::boot();
    }
}
