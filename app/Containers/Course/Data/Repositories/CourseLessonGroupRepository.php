<?php

namespace App\Containers\Course\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class CourseRepository.
 */
class CourseLessonGroupRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Course';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
    ];
}
