<?php

namespace App\Containers\Course\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class CourseExamRepository.
 */
class CourseExamRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Course';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
    ];
}
