<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Localization\Models\Language;
use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Containers\Course\Models\Course;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetCourseBySlugIdTask extends Task
{

    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run(int $courseId, string $slug, Language $currentLang = null): ?Course
    {
        $language_id = $currentLang ? $currentLang->language_id : 1;

        $data = $this->repository->where('status','=', 2)->with([
            'desc' => function($query) use($language_id) {
                $query->activeLang($language_id);
            }, 'category', 'category.desc', 'lecturers_course', 'option']);
        $data->whereHas('desc', function (Builder $query) use ($slug) {
            $query->where('slug', '=', $slug);
        });
        $data->withCount('count_item as lessons');
        $data->withCount('count_courseInCombo as count_course');
        // $this->repository->pushCriteria(new ThisEqualThatCriteria('path_id',$category_id));

        return $data->where('id',$courseId)->first();
    }
}
