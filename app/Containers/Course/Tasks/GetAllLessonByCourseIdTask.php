<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseDescRepository;
use App\Containers\Course\Data\Repositories\CourseLessonGroupRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class GetAllLessonByCourseIdTask.
 */
class GetAllLessonByCourseIdTask extends Task
{

    protected $repository;

    public function __construct(CourseLessonGroupRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($course_id)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('course_id', $course_id));
        $wery = $this->repository->with('course_item_group')->get()->keyBy('id');

        if($wery->isNotEmpty()){
            foreach ($wery as &$item){
                $item->lesson_item = $item->course_item_group->keyBy('id')->toArray();
            }
        }

        return $wery->toArray();
    }
}
