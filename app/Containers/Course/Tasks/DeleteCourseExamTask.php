<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseExamRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteLessonAttachedTask.
 */
class DeleteCourseExamTask extends Task
{

    protected $repository;

    public function __construct(CourseExamRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $this->repository->where('course_id', $data)->delete();
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
