<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseExamRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class SaveCourseExamTask.
 */
class SaveCourseExamTask extends Task
{

    protected $repository;

    public function __construct(CourseExamRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $this->repository->getModel()->insert($data);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
