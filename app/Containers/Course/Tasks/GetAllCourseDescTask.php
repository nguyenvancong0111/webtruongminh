<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseDescRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class GetAllCourseDescTask.
 */
class GetAllCourseDescTask extends Task
{

    protected $repository;

    public function __construct(CourseDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($course_id)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('course_id', $course_id));
        $wery = $this->repository->all()->keyBy('language_id')->toArray();

        return $wery;
    }
}
