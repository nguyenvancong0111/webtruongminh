<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Ship\Parents\Tasks\Task;
/**
 * Class FindCourseByIdTask.
 */
class FindCourseByIdTask extends Task
{

    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run(int $course_id, array $with = [])
    {

        $data = $this->repository->with($with)->withCount('count_item as lessons')->find($course_id);

        return $data;
    }
}