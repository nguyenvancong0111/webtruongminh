<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseLessonAttachedRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteLessonAttachedTask.
 */
class DeleteLessonAttachedTask extends Task
{

    protected $repository;

    public function __construct(CourseLessonAttachedRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $this->repository->whereIn('lesson_id', $data)->delete();;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
