<?php

namespace App\Containers\Course\Tasks;

use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Containers\Course\Enums\CourseStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CourseComboListingTask.
 */
class CourseComboListingTask extends Task
{

    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($combo_id = 0)
    {
        $this->repository->with(['desc' => function ($query) {
            $query->select('id', 'course_id', 'language_id', 'name', 'short_description', 'benefit');
        }]);
        $this->repository->withCount('count_item as lessons');
        $this->repository->whereHas('combo', function (Builder $q) use ($combo_id){
           $q->where('course_combo.combo_id', '=', $combo_id);
        });

        return $this->repository->get();
    }
}
