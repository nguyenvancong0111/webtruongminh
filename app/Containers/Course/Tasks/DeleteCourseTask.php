<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class DeleteNewsTask.
 */
class DeleteCourseTask extends Task
{

    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($news_id)
    {
        try {
            $this->repository->update(['status' => -1],$news_id);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
