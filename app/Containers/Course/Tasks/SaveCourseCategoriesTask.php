<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class SaveCourseCategoriesTask.
 */
class SaveCourseCategoriesTask extends Task
{

    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data, $course)
    {
        try {
            $news_category = $data['course_category'] ?? [];

            if (!empty($news_category)) {
                // dd($news,$news_category->toArray());
                // if (is_array($news_category)) {
                    $course->categories()->sync($news_category->toArray());
                // } else {
                //     $news->categories()->attach($news_category);
                // }
            }else {
                $course->categories()->sync([]);
            }
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
