<?php

namespace App\Containers\Course\Tasks;

use Apiato\Core\Foundation\Facades\StringLib;
use App\Containers\Course\Data\Repositories\CourseDescRepository;
use App\Containers\Course\Data\Repositories\CourseLessonGroupRepository;
use App\Containers\Course\Data\Repositories\CourseLessonItemGroupRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SaveCourseDescTask.
 */
class SaveCourseLessonTask extends Task
{

    protected $lessonItemRepository, $lessonGroupRepository;

    public function __construct(CourseLessonItemGroupRepository $lessonItemGroupRepository, CourseLessonGroupRepository $lessonGroupRepository)
    {
        $this->lessonItemRepository = $lessonItemGroupRepository;
        $this->lessonGroupRepository = $lessonGroupRepository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data, $original_course, $course_id, $session = null)
    {
        $data = $data->toArray()['lesson'];
        $arr_group_id = array_column($original_course, 'id');
        $arr_item_id = [];
        foreach ($original_course as $itm){
            $arr_item_id = array_merge($arr_item_id, array_column($itm['course_item_group'], 'id'));
        }
        $update_group = [];
        $update_item = [];
        $insert_item = [];
        if (!empty($data['group'])){
            foreach ($data['group'] as $key => $item){
                if (!empty($item['id']) && isset($original_course[$item['id']])){
                    unset($arr_group_id[array_search((int)$item['id'], $arr_group_id)]);
                    $update_group[$item['id']] = [
                        'course_id' => $course_id,
                        'title' => $item['title'],
                        'sort' => $item['sort']
                    ];
                    foreach ($data['item'][$key] as $i){
                        if (!empty($i['id']) && isset($original_course[$item['id']]['lesson_item'][$i['id']])){
                            unset($arr_item_id[array_search((int)$i['id'], $arr_item_id)]);
                            $update_item[$i['id']] = [
                                'title' => str_replace('"','\'', $i['title']),
                                'type' => isset($i['type']) ? $i['type'] : null,
                                'description' => isset($i['description']) ? $i['description'] : null,
                                'file' => isset($i['file']) ? $i['file'] : null,
                                'quiz' => isset($i['quiz']) ? $i['quiz'] : null,
                                'attached' => isset($i['attached']) ? $i['attached'] : null,
                                'preview' => isset($i['preview']) ? $i['preview'] : null,
                                'sort' => $i['sort'],
                                'status' => 2,
                                'session' => $session
                            ];
                        }else{
                            $insert_item[] = [
                                'course_id' => $course_id,
                                'group_id' => $item['id'],
                                'title' => str_replace('"','\'', $i['title']),
                                'type' => isset($i['type']) ? $i['type'] : null,
                                'description' => isset($i['description']) ? $i['description'] : null,
                                'file' => isset($i['file']) ? $i['file'] : null,
                                'quiz' => isset($i['quiz']) ? $i['quiz'] : null,
                                'attached' => isset($i['attached']) ? $i['attached'] : null,
                                'preview' => isset($i['preview']) ? $i['preview'] : null,
                                'sort' => $i['sort'],
                                'status' => 2,
                                'session' => $session

                            ];
                        }
                    }
                }else{
                    $group = $this->lessonGroupRepository->create(['course_id' => $course_id,'title' => $item['title'],'sort' => $item['sort'],'status' => 2]);
                    foreach ($data['item'][$key] as $i){
                        $insert_item[] = [
                            'course_id' => $course_id,
                            'group_id' => $group['id'],
                            'title' => str_replace('"','\'', $i['title']),
                            'type' => isset($i['type']) ? $i['type'] : null,
                            'description' => isset($i['description']) ? $i['description'] : null,
                            'file' => isset($i['file']) ? $i['file'] : null,
                            'quiz' => isset($i['quiz']) ? $i['quiz'] : null,
                            'attached' => isset($i['attached']) ? $i['attached'] : null,
                            'preview' => isset($i['preview']) ? $i['preview'] : null,
                            'sort' => $i['sort'],
                            'status' => 2,
                            'session' => $session

                        ];
                    }
                }
            }


            if (!empty($update_group)) {
                $this->lessonGroupRepository->updateMultiple($update_group);
            }
            if (!empty($update_item)) {
                $this->lessonItemRepository->updateMultiple($update_item);
            }

            if (!empty($insert_item)) {
                 $this->lessonItemRepository->getModel()->insert($insert_item);
            }
            if(!empty($arr_group_id)){
                $this->lessonGroupRepository->whereIn('id', $arr_group_id)->delete();
            }
            if(!empty($arr_item_id)){
                $this->lessonItemRepository->whereIn('id', $arr_item_id)->delete();
            }

        }
    }

    public function to_slug($string, $separator = '-') {
        $re = "/(\\s|\\".$separator.")+/mu";
        $str = @trim($string);
        $subst = $separator;
        $result = preg_replace($re, $subst, $str);

        return $result;
    }
}
