<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseOptionRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class GetAllOptionCourseByIdTask.
 */
class GetAllOptionCourseByIdTask extends Task
{

    protected $repository;

    public function __construct(CourseOptionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($course_id)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('course_id', $course_id));
        $wery = $this->repository->get()->keyBy('option_id');

        return $wery->toArray();
    }
}
