<?php

namespace App\Containers\Course\Tasks;

use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Containers\Course\Enums\CourseStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CourseListingTask.
 */
class CourseListingTask extends Task
{

    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($filters = [], $orderBy = ['created_at' => 'desc', 'id' => 'desc'], $limit = 20, $noPaginate = true, $defaultLanguage = null, $external_data = [], $current_page = 1)
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;

        if (isset($filters['id']) && $filters['id'] != '') {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('id', $filters['id']));
        } else {
            if (isset($filters['status']) && $filters['status'] != '') {
                $this->repository->pushCriteria(new ThisEqualThatCriteria('status', $filters['status']));
            } else {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('status', CourseStatus::NOT_DELETED, '>'));
            }
            if (isset($filters['is_hot']) && $filters['is_hot'] != '') {
                $this->repository->pushCriteria(new ThisEqualThatCriteria('is_hot', $filters['is_hot']));
            }
            if (isset($filters['price']) && $filters['price'] != '') {
                $this->repository->pushCriteria(new ThisEqualThatCriteria('price', $filters['price']));
            }
            if (isset($filters['is_home']) && $filters['is_home'] != '') {
                $this->repository->pushCriteria(new ThisEqualThatCriteria('is_home', $filters['is_home']));
            }
            if (isset($filters['type']) && $filters['type'] != '') {
                $this->repository->pushCriteria(new ThisEqualThatCriteria('type', $filters['type']));
            }
            if (isset($filters['time_from']) && !empty($filters['time_from']) && $filters['time_from'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('created_at', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['time_from'])), '>='));
            }
            if (isset($filters['time_to']) && !empty($filters['time_to']) && $filters['time_to'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('created_at', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['time_to'], true)), '<='));
            }

            if (isset($filters['publish_from']) && !empty($filters['publish_from']) && $filters['publish_from'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('published', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['publish_from'])), '>='));
            }
            if (isset($filters['publish_to']) && !empty($filters['publish_to']) && $filters['publish_to'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('published', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['publish_to'], true)), '<='));
            }
            if (isset($filters['cat_id']) && is_array($filters['cat_id']) && !empty($filters['cat_id'])) {
                $this->repository->whereIn('cate', $filters['cat_id']);
            } elseif (isset($filters['cat_id']) && !empty($filters['cat_id'])) {
                $this->repository->where('cate', $filters['cat_id']);
            }
            if (isset($filters['reject']) && is_array($filters['reject']) && !empty($filters['reject'])) {
                $this->repository->whereNotIn('id', $filters['reject']);
            }elseif(isset($filters['reject']) && !empty($filters['reject'])){
                $this->repository->where('id', '!=', $filters['reject']);
            }
        }

        $this->repository->with(['desc' => function ($query) use ($language_id) {
            $query->select('id', 'course_id', 'language_id', 'name', 'short_description', 'benefit');
            $query->activeLang($language_id);
        }, 'category.desc', 'lecturers_course']);

        $this->repository->withCount('count_item as lessons');
        $this->repository->withCount('count_courseInCombo as combo');

//        $this->repository->whereHas('count_item', function (Builder $query) {
//            $query->whereNotNull('course_id');
//        });

        if (isset($filters['name']) && !empty($filters['name'])) {
            $this->repository->whereHas('desc', function (Builder $query) use ($filters) {
                $query->where('name', 'like', '%' . $filters['name'] . '%');
            });
        }

        foreach ($orderBy as $column => $direction) {
            $this->repository->orderBy($column, $direction);
        }

        // \DB::enableQueryLog();
        return $noPaginate ? ($limit == 0 ? $this->repository->get() : $this->repository->limit($limit)) :  $this->repository->paginate($limit);
        // dd(\DB::getQueryLog());
    }
}
