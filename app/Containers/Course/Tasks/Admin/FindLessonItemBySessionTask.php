<?php

namespace App\Containers\Course\Tasks\Admin;

use App\Containers\Course\Data\Repositories\CourseLessonGroupRepository;
use App\Containers\Course\Data\Repositories\CourseLessonItemGroupRepository;
use App\Ship\Parents\Tasks\Task;
/**
 * Class FindLessonItemBySessionTask.
 */
class FindLessonItemBySessionTask extends Task
{

    protected $repository;

    public function __construct(CourseLessonItemGroupRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($session)
    {
        return $this->repository->where('session', $session)->get();
    }
}