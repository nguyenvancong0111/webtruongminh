<?php

namespace App\Containers\Course\Tasks\Admin;

use App\Containers\Course\Data\Repositories\CourseLessonFileRepository;
use App\Containers\Course\Enums\CourseStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetLessonFileListTask extends Task
{

    protected $repository;

    public function __construct(CourseLessonFileRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run(array $filters = [], int $limit = 20, bool $skipPagination = false, int $currentPage = 1): iterable {

        $data = $this->repository;
        if (isset($filters['auth_type']) && $filters['auth_type'] != '') {
            $data = $data->pushCriteria(new ThisEqualThatCriteria('auth_type', $filters['auth_type']));
        }
        if(isset($filters['auth_id']) && $filters['auth_id'] != '') {
            $data = $data->pushCriteria(new ThisEqualThatCriteria('auth_id', $filters['auth_id']));
        }
        if(isset($filters['name']) && $filters['name'] != ''){
            $data = $data->where('name', 'like', '%'.$filters['name'].'%');
        }
        if (isset($filters['type']) && is_array($filters['type']) && !empty($filters['type'])){
            $data = $data->whereIn('type', $filters['type']);
        }
        $data = $data->orderBy('id', 'DESC');
        if ($limit == 0){
            return $data->get();
        }
        return $skipPagination ? $data->limit($limit) : $data->paginate($limit);
    }
}
