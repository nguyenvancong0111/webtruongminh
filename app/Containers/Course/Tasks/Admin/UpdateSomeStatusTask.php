<?php

namespace App\Containers\Course\Tasks\Admin;

use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Ship\core\Traits\HelpersTraits\UpdateDynamicStatusTrait;
use App\Ship\Parents\Tasks\Task;

class UpdateSomeStatusTask extends Task
{
    use UpdateDynamicStatusTrait;

    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(string $field,int $id, int $status) :? bool
    {
        return $this->updateDynamicStatus($field,$id,$status);
    }
}
