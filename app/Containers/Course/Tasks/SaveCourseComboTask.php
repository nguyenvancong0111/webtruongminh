<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class SaveCourseComboTask.
 */
class SaveCourseComboTask extends Task
{

    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data, $course)
    {
        try {
            $combo_course = $data['combo_course'] ?? [];

            if (!empty($combo_course)) {
                $course->combo()->sync($combo_course->toArray());
            }else {
                $course->combo()->sync([]);
            }
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
