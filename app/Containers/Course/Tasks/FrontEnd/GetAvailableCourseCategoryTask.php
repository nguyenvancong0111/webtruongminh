<?php

namespace App\Containers\Course\Tasks\FrontEnd;

use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Tasks\Task;

class GetAvailableCourseCategoryTask extends Task
{
    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $pagination = 12, Language $currentLang = null, array $withNew = [], array $where = [], array $notWhere = [])
    {
        $language_id = $currentLang->language_id ? $currentLang->language_id : 1;

        if (!empty($where['category_id'])) {
            $this->repository->whereHas('categories', function ($query) use ($where) {
                $query->where('category.category_id', $where['category_id']);
            });
        }
        if (!empty($where['position'])) {
            $this->repository->whereHas('categories', function ($query) use ($where) {
                $query->where('category.position', $where['position']);
            });
        }

        if (!empty($notWhere['news'])) {
            foreach ($notWhere['news'] as $column => $field) {
                $this->repository->where($column, '!=', $field);
            }
        }

        $data = $this->repository->where('status', 2)->with([
                'desc' => function ($query) use ($language_id) {
                    $query->where('language_id', $language_id);
                },
                'categories' => function ($query) use ($where, $language_id) {
                    if (!empty($where['category_id'])) {
                        $query->where('category.category_id', $where['category_id'])
                            ->with(['desc' => function ($query) use ($language_id) {
                                $query->where('language_id', $language_id);
                            }]);
                    }
                }
            ]
        );

        return ($pagination != 0) ? $data->paginate($pagination) : $data->get();
    }
}
