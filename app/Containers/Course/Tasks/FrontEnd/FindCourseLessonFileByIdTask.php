<?php

namespace App\Containers\Course\Tasks\FrontEnd;

use App\Containers\Course\Data\Repositories\CourseLessonFileRepository;
use App\Ship\Parents\Tasks\Task;
/**
 * Class FindCourseLessonFileByIdTask.
 */
class FindCourseLessonFileByIdTask extends Task
{

    protected $repository;

    public function __construct(CourseLessonFileRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($id)
    {
        return $this->repository->find($id);
    }
}