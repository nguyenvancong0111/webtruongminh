<?php

namespace App\Containers\Course\Tasks\FrontEnd;

use App\Containers\Course\Data\Repositories\CourseLessonViewRepository;
use App\Ship\Parents\Tasks\Task;
/**
 * Class GetCustomerLessonViewTask.
 */
class GetCustomerLessonViewTask extends Task
{

    protected $repository;

    public function __construct(CourseLessonViewRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($filter)
    {
        if (is_array($filter) && !empty($filter)){
            foreach ($filter as $key => $val){
                $this->repository->where($key, $val);
            }
            return $this->repository->get();
        }
        return null;
    }
}