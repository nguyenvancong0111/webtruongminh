<?php

namespace App\Containers\Course\Tasks\FrontEnd;

use App\Containers\Course\Data\Repositories\CourseLessonGroupRepository;
use App\Containers\Course\Data\Repositories\CourseLessonItemGroupRepository;
use App\Ship\Parents\Tasks\Task;
/**
 * Class FindCourseByIdTask.
 */
class FindLessonItemByIdTask extends Task
{

    protected $repository;

    public function __construct(CourseLessonItemGroupRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($id)
    {
        return $this->repository->with(['attached_file', 'file_primary'])->find($id);
    }
}