<?php

namespace App\Containers\Course\Tasks\FrontEnd;

use App\Containers\Course\Data\Repositories\CourseLessonViewRepository;
use App\Ship\Parents\Tasks\Task;
/**
 * Class FindCustomerLessonViewTask.
 */
class FindCustomerLessonViewTask extends Task
{

    protected $repository;

    public function __construct(CourseLessonViewRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($filter)
    {
        $data = $this->repository;
        if (is_array($filter) && !empty($filter)){
            foreach ($filter as $key => $val){
                $data = $data->where($key, $val);
            }
        }
        return $data->first();
    }
}