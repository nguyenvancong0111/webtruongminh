<?php

namespace App\Containers\Course\Tasks\FrontEnd;

use App\Containers\Course\Data\Repositories\CourseLessonGroupRepository;
use App\Ship\Parents\Tasks\Task;
/**
 * Class FindCourseByIdTask.
 */
class FindLessonByCourseIdTask extends Task
{

    protected $repository;

    public function __construct(CourseLessonGroupRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($course_id, $external_data = [])
    {
        $data = $this->repository->with(array_merge($external_data['with_relationship'],[]))
            ->with(['course_item_group' => function($q){
                $q->select('id', 'group_id', 'title', 'type', 'file', 'quiz');
            }])
            ->where('course_id', $course_id)
            ->orderBy('sort', 'asc');

        return $data->get();
    }
}