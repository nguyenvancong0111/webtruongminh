<?php

namespace App\Containers\Course\Tasks\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Containers\Course\Enums\CourseStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetCourseCartFilterTask extends Task
{

    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run(array $filters = []): iterable {
        $language_id = 1;

        if (isset($filters['status']) && $filters['status'] != '') {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('status', $filters['status']));
        }
        $this->repository->with(['desc' => function ($query) use ($language_id) {
            $query->select('id', 'course_id', 'language_id', 'name', 'short_description', 'slug', 'description');
            $query->activeLang($language_id);
        }, 'option' => function($q){
            $q->where('status', '=', 2);
            $q->select('id', 'type', 'name');
        }]);

        if (isset($filters['arr_id']) && is_array($filters['arr_id'])){
            $this->repository->whereIn('id', $filters['arr_id']);
        }


        // \DB::enableQueryLog();
        return $this->repository->get();
        // dd(\DB::getQueryLog());
    }
}
