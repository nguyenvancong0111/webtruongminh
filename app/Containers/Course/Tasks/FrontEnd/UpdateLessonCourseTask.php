<?php

namespace App\Containers\Course\Tasks\FrontEnd;

use App\Containers\Course\Data\Repositories\CourseLessonItemGroupRepository;
use App\Containers\Customer\Data\Repositories\CustomerCourseRepository;
use Exception;
use Illuminate\Support\Arr;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Exceptions\UpdateResourceFailedException;

class UpdateLessonCourseTask extends Task
{

    protected $repository;

    public function __construct(CourseLessonItemGroupRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $filters = [], array $dataUpdate = [])
    {
        try {
            $customer = $this->repository;
            if (!empty($filters)){
                foreach ($filters as $key => $val){
                    $customer->where($key, $val);
                }
            }
            if (!empty($customer->first())){
                return $customer->update($dataUpdate, $filters['id']);
            }

            return null;
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
