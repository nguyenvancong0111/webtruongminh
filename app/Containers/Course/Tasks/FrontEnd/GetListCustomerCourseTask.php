<?php

namespace App\Containers\Course\Tasks\FrontEnd;

use App\Containers\Customer\Data\Repositories\CustomerCourseRepository;
use App\Containers\Customer\Data\Repositories\CustomerRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetListCustomerCourseTask extends Task
{

    protected $repository;

    public function __construct(CustomerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $filters = [], bool $skipPaginate = true, int $limit = 10, int $currentPage = 1)
    {
        if (isset($filters['status']) && !empty($filters['status'])){
            $this->repository->where('status', $filters['status']);
        }
        $this->repository->with(['customerCourse' => function($q){
            $q->select('id', 'customer_id', 'course_id', 'is_active', 'start_time', 'available_time', 'percent_view');
        }]);

        $this->repository->whereHas('customerCourse', function (Builder $query) use ($filters) {
            $query->where('course_id', $filters['courseID']);
        });

        $this->repository->select('id', 'email', 'fullname', 'phone', 'avatar', 'date_of_birth');

        return $skipPaginate ? ($limit > 0 ? $this->repository->limit($limit) : $this->repository->all()) : $this->repository->paginate($limit);

    }
} // End class
