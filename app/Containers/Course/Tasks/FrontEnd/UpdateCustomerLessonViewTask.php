<?php

namespace App\Containers\Course\Tasks\FrontEnd;

use App\Containers\Course\Data\Repositories\CourseLessonViewRepository;
use App\Ship\Parents\Tasks\Task;
/**
 * Class UpdateCustomerLessonViewTask.
 */
class UpdateCustomerLessonViewTask extends Task
{

    protected $repository;

    public function __construct(CourseLessonViewRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($filter, $value)
    {
        $this->repository->updateOrCreate($filter, $value);
    }
}