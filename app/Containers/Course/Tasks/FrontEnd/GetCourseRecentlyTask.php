<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 01:33:06
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-05 15:52:43
 * @ Description: Happy Coding!
 */

namespace App\Containers\Course\Tasks\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Containers\Course\Enums\CourseStatus;
use App\Containers\Course\Enums\CourseType;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisInThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

class GetCourseRecentlyTask extends Task
{

    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($type, array $filter = [], int $limit = 20, bool $isPanination = false, Language $currentLang = null, array $orderBy = ['created_at' => 'desc', 'id' => 'desc']): iterable {
        $language_id = $currentLang ? $currentLang->language_id : 1;

        $this->repository->pushCriteria(new ThisEqualThatCriteria('status', CourseStatus::ACTIVE));

        if(!empty($type)){
            $this->repository->pushCriteria(new ThisEqualThatCriteria('type', $type));
        }
        if (!empty($filter['id_skip'])){
            $this->repository->where('id', '<>', $filter['id_skip']);
        }
//        dd($this->repository->all());
//        else{
//            $this->repository->pushCriteria(new ThisInThatCriteria('type',[CourseType::PROMOTION,CourseType::EVENT] ));
//        }
        $this->repository->with(['desc' => function ($query) use ($language_id) {
            $query->select('id', 'course_id', 'language_id', 'name', 'short_description', 'slug');
            $query->activeLang($language_id);
        }]);
//        $this->repository->whereRaw("(DATEDIFF('".Carbon::now()."',created_at)<30)");

        foreach ($orderBy as $column => $direction) {
            $this->repository->orderBy($column, $direction);
        }
//         \DB::enableQueryLog();

        return $isPanination ?  $this->repository->paginate($limit): $this->repository->limit($limit);
        // dd(\DB::getQueryLog());
    }
}
