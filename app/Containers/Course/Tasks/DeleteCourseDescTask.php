<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseDescRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class DeleteCourseDescTask.
 */
class DeleteCourseDescTask extends Task
{

    protected $repository;

    public function __construct(CourseDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($course_id)
    {
        try {
            $this->repository->getModel()->where('course_id', $course_id)->delete();
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
