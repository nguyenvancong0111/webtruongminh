<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseLessonAttachedRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class SaveLessonAttachedTask.
 */
class SaveLessonAttachedTask extends Task
{

    protected $repository;

    public function __construct(CourseLessonAttachedRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $this->repository->getModel()->insert($data);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
