<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class CreateCourseTask.
 */
class CreateCourseTask extends Task
{

    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data->toArray(), [
                'group_lesson_title',
                'group_lesson_sort',
                'option_show',
                'title_lesson',
                'value_lesson',
                'value_lesson_sort',
                'course_description',
                '_token',
                '_headers'
            ]);
            $dataUpdate['auth_type'] = isset($dataUpdate['author']) && !empty($dataUpdate['author']) ? 1 : 0;
            $dataUpdate['author'] = isset($dataUpdate['author']) && !empty($dataUpdate['author']) ? $dataUpdate['author'] : auth()->guard('admin')->user()->name;

            $course = $this->repository->create($dataUpdate);


            return $course;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
