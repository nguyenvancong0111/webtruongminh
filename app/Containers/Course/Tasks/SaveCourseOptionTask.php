<?php

namespace App\Containers\Course\Tasks;

use Apiato\Core\Foundation\StringLib;
use App\Containers\Course\Data\Repositories\CourseOptionRepository;
use App\Containers\Questions\Data\Repositories\QuestionsAnswerRepository;
use App\Containers\Questions\Data\Repositories\QuestionsAnswerDescRepository;
use App\Ship\Parents\Tasks\Task;

class SaveCourseOptionTask extends Task
{

    protected $repository;

    public function __construct(CourseOptionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data, $original_course_option, $course_id)
    {
        $option = $data->option;
        $arr_co_id = array_column($original_course_option, 'id');

        $update_item = [];
        $insert_item = [];
        if (!empty($option)){
            foreach ($option as $key => $item){
                if (!empty($item['id']) && isset($original_course_option[$item['id']])){
                    unset($arr_co_id[array_search((int)$original_course_option[$item['id']]['id'], $arr_co_id)]);
                    $update_item[$original_course_option[$item['id']]['id']] = [
                        'course_id' => $course_id,
                        'option_id' => $item['id'],
                        'value' => @$item['value'],
                        'price' => @$item['price'],
                        'is_active' => isset($item['is_active']) ? 1 : 0
                    ];
                }else{
                    $insert_item[] = [
                        'course_id' => $course_id,
                        'option_id' => $item['id'],
                        'value' => $item['value'],
                        'price' => $item['price'],
                        'is_active' => isset($item['is_active']) ? 1 : 0
                    ];
                }
            }

            if (!empty($update_item)) {
                $this->repository->updateMultiple($update_item);
            }

            if (!empty($insert_item)) {
                $this->repository->getModel()->insert($insert_item);
            }

            if(!empty($arr_co_id)){
                $this->repository->whereIn('id', $arr_co_id)->delete();
            }

        }
    }
}
