<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class SaveCourseTask.
 */
class SaveCourseTask extends Task
{

    protected $repository;

    public function __construct(CourseRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data->toArray(), [
                'option',
                'lesson',
                'combo_course',
                'course_description',
                '_token',
                '_headers'
            ]);
            $dataUpdate['auth_type'] = isset($dataUpdate['author']) && !empty($dataUpdate['author']) ? 1 : 0;
            $dataUpdate['author'] = isset($dataUpdate['author']) && !empty($dataUpdate['author']) ? $dataUpdate['author'] : auth()->guard('admin')->user()->name;

            $course = $this->repository->update($dataUpdate, $data->id);

            return $course;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
