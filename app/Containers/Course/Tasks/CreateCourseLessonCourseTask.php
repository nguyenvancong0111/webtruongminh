<?php

namespace App\Containers\Course\Tasks;

use App\Containers\Course\Data\Repositories\CourseLessonFileRepository;
use App\Containers\Course\Data\Repositories\CourseRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class CreateCourseLessonCourseTask.
 */
class CreateCourseLessonCourseTask extends Task
{

    protected $repository;

    public function __construct(CourseLessonFileRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data->toArray(), ['_token', '_headers']);

            $course = $this->repository->create($dataUpdate);


            return $course;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
